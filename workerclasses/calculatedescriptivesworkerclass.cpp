#include "calculatedescriptivesworkerclass.h"

CalculateDescriptivesWorkerClass::CalculateDescriptivesWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CalculateDescriptivesWorkerClass::Parameters *>(parameters))
{

}

bool CalculateDescriptivesWorkerClass::checkInputParameters()
{

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected");

        return false;

    }

    return true;

}

void CalculateDescriptivesWorkerClass::doWork()
{
    emit processStarted("calculate descriptives");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Calculate descriptives"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining item subsets: \"" + d_parameters.annotationLabelDefiningItemSubsets + "\"");

        emit appendLogItem(LogListModel::PARAMETER, "item subset identifiers: " + ConvertFunctions::toString(d_parameters.itemSubsetIdentifiers));

    } else
        d_parameters.itemSubsetIdentifiers << QString();

    emit appendLogItem(LogListModel::PARAMETER, "descriptive selected: " + ConvertFunctions::toString(d_parameters.descriptives));

    if (!d_parameters.selectedValues.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "values selected: " + ConvertFunctions::toString(d_parameters.selectedValues));

    if (!d_parameters.annotationLabelsDefiningAnnotationValuesForVariables.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation labels defining annotation values for selected variables = " + ConvertFunctions::toString(d_parameters.annotationLabelsDefiningAnnotationValuesForVariables));

    if (!d_parameters.exportDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");


    emit startProgress(0, "Collecting data", 0, 0);

    d_itemSubsetIdentifierToIndexes = d_parameters.baseDataset->annotationValueToIndexes<QString>(d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.annotationLabelDefiningItemSubsets, QSet<QString>(d_parameters.itemSubsetIdentifiers.begin(), d_parameters.itemSubsetIdentifiers.end()), BaseMatrix::switchOrientation(d_parameters.orientation));

    emit stopProgress(0);

    switch (d_parameters.baseDataset->baseMatrix()->metaTypeIdentifier()) {

        case QMetaType::Short: this->_doWork<short>(); break;

        case QMetaType::UShort: this->_doWork<unsigned short>(); break;

        case QMetaType::Int: this->_doWork<int>(); break;

        case QMetaType::UInt: this->_doWork<unsigned int>(); break;

        case QMetaType::LongLong: this->_doWork<long long>(); break;

        case QMetaType::ULongLong: this->_doWork<unsigned long long>(); break;

        case QMetaType::Float: this->_doWork<float>(); break;

        case QMetaType::Double: this->_doWork<double>(); break;

        case QMetaType::UChar: this->_doWork<unsigned char>(); break;

        case QMetaType::QChar: this->_doWork<QChar>(); break;

        case QMetaType::QString: this->_doWork<QString>(); break;

        case QMetaType::Bool: this->_doWork<bool>(); break;

        default: break;

    }

    QThread::currentThread()->quit();

}
