#include "liptakstrendmethod.h"

QStringList LiptaksTrendMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "Z-transformed ( Significance (2-tailed) )", "Weight", "Sign of statistic", "Sample size"};

QStringList LiptaksTrendMethod::d_metaAnalysisTestDescriptiveLabels = {"Z", "Significance (2-tailed)"};

QString LiptaksTrendMethod::d_nameOfMetaAnalysisTest = "Liptak's trend method";


LiptaksTrendMethod::LiptaksTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

LiptaksTrendMethod::~LiptaksTrendMethod()
{

}

QStringList LiptaksTrendMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList LiptaksTrendMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> LiptaksTrendMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    boost::math::normal dist(0.0, 1.0);

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << -1.0 * boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0) << std::sqrt(baseStatisticalTest->sampleSize()) << (baseStatisticalTest->isSignOfStatisticNegative() ? "-" : "+") << baseStatisticalTest->sampleSize();

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> LiptaksTrendMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void LiptaksTrendMethod::calculateStatistic()
{

    boost::math::normal dist(0.0, 1.0);

    d_statistic = 0.0;

    double w = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        if (baseStatisticalTest->isSignOfStatisticNegative())
            d_statistic -= -1.0 *boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0) * std::sqrt(baseStatisticalTest->sampleSize());
        else
            d_statistic += -1.0 *boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0) * std::sqrt(baseStatisticalTest->sampleSize());

        w += baseStatisticalTest->sampleSize();

    }

    d_statistic /= std::sqrt(w);

}

void LiptaksTrendMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, std::fabs(d_statistic)));

}

void LiptaksTrendMethod::_reset()
{

}
