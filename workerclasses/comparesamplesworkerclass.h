#ifndef COMPARESAMPLESWORKERCLASS_H
#define COMPARESAMPLESWORKERCLASS_H

#include <QString>
#include <QSharedPointer>

#include <tuple>
#include <algorithm>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"
#include "statistics/basestatisticaltest.h"
#include "base/convertfunctions.h"

#include "statistics/basestatisticaltest.h"
#include "statistics/fishersmethod.h"
#include "statistics/fisherstrendmethod.h"
#include "statistics/stouffersmethod.h"
#include "statistics/stoufferstrendmethod.h"
#include "statistics/liptaksmethod.h"
#include "statistics/liptakstrendmethod.h"
#include "statistics/lancastersmethod.h"
#include "statistics/lancasterstrendmethod.h"
#include "statistics/genericinversemethodwithfixedeffectmodel.h"
#include "statistics/genericinversemethodwithrandomeffectmodel.h"

#include "statistics/studentt.h"
#include "statistics/welcht.h"
#include "statistics/levenef.h"
#include "statistics/mannwhitneyu.h"
#include "statistics/kolmogorovsmirnovz.h"
#include "statistics/kruskalwallischisquared.h"
#include "statistics/brownforsythef.h"
#include "statistics/bartlettchisquared.h"
#include "statistics/ftestf.h"
#include "statistics/vanderwaerdent.h"

class CompareSamplesWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString annotationLabelDefiningItemSamples;

        QString annotationLabelDefiningPairedItems;

        QString annotationLabelDefiningItemSubsets;

        QString annotationLabelDefiningStrata;

        QStringList annotationLabelsDefiningAnnotationValuesForVariables;

        QSharedPointer<BaseDataset> baseDataset;

        QSharedPointer<BaseStatisticalTest> baseStatisticalTest;

        QString comparisonDescription;

        int confidenceLevel;

        QString exportDirectory;

        int falseDiscoveryRate;

        QStringList itemSampleIdentifiers;

        QStringList itemSubsetIdentifiers;

        QString nameOfMetaAnalysisApproach;

        QString nameOfStatisticalTest;

        int numberOfPermutations;

        BaseMatrix::Orientation orientation;

        bool performMultivariatePermutationTest;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        QStringList strataIdentifiers;

        QStringList categoryIdentifiers;

    };

    CompareSamplesWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CompareSamplesWorkerClass::Parameters d_parameters;

    QHash<QString, std::tuple<QList<int>, QVector<unsigned int>, QVector<unsigned int>, QVector<unsigned int> > > d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers;

    bool checkInputParameters();

    template <typename T> void _doWork();

    double numberOfPossiblePermutations(const QVector<unsigned int> &itemGroupCodedIdentifiers, const QVector<unsigned int> &itemPairCodedIdentifiers);

};

template <typename T>
void CompareSamplesWorkerClass::_doWork()
{

    QSharedPointer<Dataset<T> > dataSet = qSharedPointerCast<Dataset<T> >(d_parameters.baseDataset);

    QVector<QVector<QSharedPointer<BaseStatisticalTest> > > statisticalTests;

    QVector<QSharedPointer<BaseMetaAnalysisTest> >  metaAnalysisTests;

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
        emit startProgress(0, "Processing item subsets", 0, d_parameters.itemSubsetIdentifiers.size());

    QVector<QVector<QVector<QVariant> > > resultTablePerItemSubset;

    QVector<QVector<QVariant> > metaAnalysisResultTable;

    for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

        QVector<QVector<QVariant> > resultTable(d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

        QVector<QSharedPointer<BaseStatisticalTest> > _statisticalTests;

        emit startProgress((d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? 0 : 1), "Performing statistical tests", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

            int indexOfVariable = d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i);

            if (!d_parameters.annotationLabelsDefiningAnnotationValuesForVariables.isEmpty())
                resultTable[i] = dataSet->annotationValues(indexOfVariable, d_parameters.annotationLabelsDefiningAnnotationValuesForVariables, d_parameters.orientation).toVector();

            if (d_parameters.nameOfStatisticalTest == "Student T")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new StudentT<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "Welch T")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new WelchT<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "Levene F")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new LeveneF<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "Mann-Whitney U")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new MannWhitneyU<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "Kolmogorov-Smirnov Z")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new KolmogorovsmirnovZ<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "Kruskal-Wallis chi-squared")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new KruskalWallisChiSquared<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "Brown-Forsythe F")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new BrownForsytheF<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "Bartlett chi-squared")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new BarlettChiSquared<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "F-test F")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new FTestF<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));
            else if (d_parameters.nameOfStatisticalTest == "van de Waerden T")
                _statisticalTests << QSharedPointer<BaseStatisticalTest>(new VanDerWaerdenT<double>(dataSet->matrix()->vectorOfDouble(indexOfVariable, std::get<0>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier))), d_parameters.orientation), std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)])));

            resultTable[i] << _statisticalTests.last()->samplesDescriptiveValues() << _statisticalTests.last()->testDescriptiveValues();

            emit updateProgress((d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? 0 : 1), i + 1);

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

        }

        emit stopProgress((d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? 0 : 1));

        statisticalTests << _statisticalTests;

        resultTablePerItemSubset << resultTable;

        if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
            emit updateProgress(0, indexOfItemSubsetIdentifier + 1);

    }

    MatrixOperations::transpose_inplace(statisticalTests);

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
        emit stopProgress(0);

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

        emit startProgress(0, "Performing meta-analysis tests", 0, statisticalTests.size());

        for (int indexOfStatisticalTest = 0; indexOfStatisticalTest < statisticalTests.size(); ++indexOfStatisticalTest) {

            const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTestGroupedPerSubset(statisticalTests.at(indexOfStatisticalTest));

            if (d_parameters.nameOfMetaAnalysisApproach == "Fisher's method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new FishersMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Fisher's trend method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new FishersTrendMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Stouffer's method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new StouffersMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Stouffer's trend method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new StouffersTrendMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Liptak's method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new LiptaksMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Liptak's trend method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new LiptaksTrendMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Lancaster's method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new LancastersMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Lancaster's trend method")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new LancastersTrendMethod(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Generic inverse method with fixed effect model")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new GenericInverseMethodWithFixedEffectModel(baseStatisticalTestGroupedPerSubset));
            else if (d_parameters.nameOfMetaAnalysisApproach == "Generic inverse method with fixed random model")
                metaAnalysisTests << QSharedPointer<BaseMetaAnalysisTest>(new GenericInverseMethodWithRandomEffectModel(baseStatisticalTestGroupedPerSubset));

            if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
                metaAnalysisResultTable[indexOfStatisticalTest] << dataSet->annotationValues(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(indexOfStatisticalTest), d_parameters.annotationLabelsDefiningAnnotationValuesForVariables, d_parameters.orientation).toVector();

            metaAnalysisResultTable[indexOfStatisticalTest] << metaAnalysisTests.last()->baseStatisticalTestDescriptiveValues() << metaAnalysisTests.last()->metaAnalysisTestDescriptiveValues();

            emit updateProgress(0, indexOfStatisticalTest + 1);

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

        }

        emit stopProgress(0);

    }

    if (d_parameters.performMultivariatePermutationTest) {

        std::random_device rd;

        std::mt19937 g(rd());

        emit startProgress(0, "Preparing for permutations", 0, 0);

        QVector<QVector<double> > observedPValuesPerItemSubset(d_parameters.itemSubsetIdentifiers.size());

        QVector<QVector<double> > sortedObservedPValuesPerItemSubset(d_parameters.itemSubsetIdentifiers.size());

        for (int indexOfStatisticalTest = 0; indexOfStatisticalTest < statisticalTests.size(); ++indexOfStatisticalTest) {

           for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

               double pValue = statisticalTests.at(indexOfStatisticalTest).at(indexOfItemSubsetIdentifier)->pValue();

               sortedObservedPValuesPerItemSubset[indexOfItemSubsetIdentifier] << pValue;

               observedPValuesPerItemSubset[indexOfItemSubsetIdentifier] << pValue;

           }

        }

        for (QVector<double> &sortedObservedPValues : sortedObservedPValuesPerItemSubset)
            std::sort(sortedObservedPValues.begin(), sortedObservedPValues.end());

        QVector<double> observedPValuesForMetaAnalysisTests;

        QVector<double> sortedObservedPValuesForMetaAnalysisTests;

        if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

            for (const QSharedPointer<BaseMetaAnalysisTest> &baseMetaAnalysisTest : metaAnalysisTests) {

                observedPValuesForMetaAnalysisTests << baseMetaAnalysisTest->pValue();

                sortedObservedPValuesForMetaAnalysisTests << baseMetaAnalysisTest->pValue();

            }

            std::sort(sortedObservedPValuesForMetaAnalysisTests.begin(), sortedObservedPValuesForMetaAnalysisTests.end());

        }

        QVector<QVector<double> > multivariatePermutationThresholdsPerItemSubset(d_parameters.itemSubsetIdentifiers.size());

        QVector<double> multivariatePermutationThresholdsForMetaAnalysis;

        QVector<bool> performMultivariatePermutationTestWithExactModePerItemSubset(d_parameters.itemSubsetIdentifiers.size(), false);

        QVector<QVector<QVector<unsigned int *> > > itemSubsetIdentifierToAuxiliaryMatrixForPermutatingPairedSamples;

        QVector<QVector<QVector<unsigned int> > > itemSubsetIdentifierToOriginalOfAuxiliaryMatrixForPermutatingPairedSamples;

        QVector<QVector<unsigned int> > itemSubsetIdentifierToOriginalOfItemGroupCodedIdentifiers;


        for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

            QString itemSubsetIdentifier = d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier);


            QVector<unsigned int> &itemGroupCodedIdentifiers(std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[itemSubsetIdentifier]));

            itemSubsetIdentifierToOriginalOfItemGroupCodedIdentifiers << itemGroupCodedIdentifiers;

            QVector<unsigned int> const &itemPairCodedIdentifiers(std::get<2>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[itemSubsetIdentifier]));

            QVector<QVector<unsigned int *> > auxiliaryMatrixForPermutatingPairedSamples;

            QVector<QVector<unsigned int> > originalOfAuxiliaryMatrixForPermutatingPairedSamples;

            if (!itemPairCodedIdentifiers.isEmpty()) {

                auxiliaryMatrixForPermutatingPairedSamples.resize(static_cast<int>(*std::max_element(itemPairCodedIdentifiers.begin(), itemPairCodedIdentifiers.end()) + 1));

                for (int i = 0; i < itemGroupCodedIdentifiers.size(); ++i) {

                    auxiliaryMatrixForPermutatingPairedSamples[static_cast<int>(itemPairCodedIdentifiers.at(i))] << &itemGroupCodedIdentifiers[i];

                    originalOfAuxiliaryMatrixForPermutatingPairedSamples[static_cast<int>(itemPairCodedIdentifiers.at(i))] << itemGroupCodedIdentifiers.at(i);

                }

                itemSubsetIdentifierToAuxiliaryMatrixForPermutatingPairedSamples << auxiliaryMatrixForPermutatingPairedSamples;

                itemSubsetIdentifierToOriginalOfAuxiliaryMatrixForPermutatingPairedSamples << originalOfAuxiliaryMatrixForPermutatingPairedSamples;

            }

            double numberOfPossiblePermutations = this->numberOfPossiblePermutations(itemGroupCodedIdentifiers, itemPairCodedIdentifiers);

            if (numberOfPossiblePermutations <= d_parameters.numberOfPermutations) {

                emit appendLogItem(LogListModel::WARNING, (d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? QString() : "for item subset \"" + itemSubsetIdentifier + "\": ") + "possible number of permutations (" + QString::number(numberOfPossiblePermutations) + ") is smaller than the requested number of permutations (" + QString::number(d_parameters.numberOfPermutations) + ")");

                emit appendLogItem(LogListModel::WARNING, "setting number of permutations to " + QString::number(numberOfPossiblePermutations));

                d_parameters.numberOfPermutations = numberOfPossiblePermutations;

                performMultivariatePermutationTestWithExactModePerItemSubset[indexOfItemSubsetIdentifier] = true;

                if (itemPairCodedIdentifiers.isEmpty())
                    std::sort(itemGroupCodedIdentifiers.begin(), itemGroupCodedIdentifiers.end());
                else {

                    for (QVector<unsigned int *> &vectorOfPairedGroupCodedIdentifiers : auxiliaryMatrixForPermutatingPairedSamples) {

                        std::sort(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), [](const unsigned int *value1, const unsigned int *value2) {return value1 < value2;});

                        for (int i = 0; i < vectorOfPairedGroupCodedIdentifiers.size(); ++i)
                            *vectorOfPairedGroupCodedIdentifiers[i] = static_cast<unsigned int>(i);

                    }

                }

            } else {

                if (itemPairCodedIdentifiers.isEmpty()) {

                    QVector<unsigned int> const &originalOfItemGroupCodedIdentifiers = itemSubsetIdentifierToOriginalOfItemGroupCodedIdentifiers.at(indexOfItemSubsetIdentifier);

                    std::shuffle(itemGroupCodedIdentifiers.begin(), itemGroupCodedIdentifiers.end(), g);

                    while (originalOfItemGroupCodedIdentifiers == itemGroupCodedIdentifiers)
                        std::shuffle(itemGroupCodedIdentifiers.begin(), itemGroupCodedIdentifiers.end(), g);

                } else {

                    for (int i = 0; i < auxiliaryMatrixForPermutatingPairedSamples.size(); ++i) {

                        QVector<unsigned int *> &vectorOfPairedGroupCodedIdentifiers = auxiliaryMatrixForPermutatingPairedSamples[i];

                        QVector<unsigned int > const &originalOfVectorOfPairedGroupCodedIdentifiers = originalOfAuxiliaryMatrixForPermutatingPairedSamples.at(i);

                        std::shuffle(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), g);

                        while (std::equal(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), originalOfVectorOfPairedGroupCodedIdentifiers.begin(), originalOfVectorOfPairedGroupCodedIdentifiers.end(), [](const unsigned int* value1, const unsigned int value2){ return *value1 == value2; }))
                            std::shuffle(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), g);

                        for (int j = 0; j < vectorOfPairedGroupCodedIdentifiers.size(); ++j)
                            *vectorOfPairedGroupCodedIdentifiers[j] = static_cast<unsigned int>(j);

                    }

                }

            }

        }

        auto getPermutationPValuesFromStatisticalTests = [&](const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) {

            QVector<double> pValues;

            for (const QSharedPointer<BaseStatisticalTest> &baseStatisticalTest : baseStatisticalTests) {

                baseStatisticalTest->reset();

                pValues << baseStatisticalTest->pValue();

            }

            return pValues;

        };

        auto getPermutationPValuesFromMetaAnalysisTest = [&](const QSharedPointer<BaseMetaAnalysisTest> &baseMetaAnalysisTest) {

            baseMetaAnalysisTest->reset();

            return baseMetaAnalysisTest->pValue();

        };

        auto getMultivariatePermutationThreshold = [&](const QVector<double> &sortedObservedPValues, const QVector<double> &sortedPermutationPValues) {

            for (int i = 0; i < sortedObservedPValues.size(); ++i) {

                int index = static_cast<int>(d_parameters.falseDiscoveryRate / 100.0) * (i + 1);

                if (!(sortedObservedPValues.at(i) < sortedPermutationPValues.at(index)) && !((i != 0) && (index > int(i * d_parameters.falseDiscoveryRate / 100.0))))
                    return sortedPermutationPValues.at(index);

            }

            return sortedPermutationPValues.last();

        };

        auto updateCountForPermutationPValues = [&](QVector<int> &countForPermutationPValues, const QVector<double> &observedPValues, const QVector<double> &permutationPValues) {

            for (int i = 0; i < countForPermutationPValues.size(); ++i) {

                if (permutationPValues.at(i) < observedPValues.at(i))
                    ++countForPermutationPValues[i];

            }

        };

        emit stopProgress(0);

        QVector<QVector<int> > countForPermutationPValuesPerItemSubset(d_parameters.itemSubsetIdentifiers.size(), QVector<int>(statisticalTests.size(), 0));

        QVector<int> countForPermutationPValuesForMetaAnalysisTests(statisticalTests.size(), 0);

        emit startProgress(0, "Performing permutations", 0, d_parameters.numberOfPermutations);

        for (int permutationRound = 0; permutationRound < d_parameters.numberOfPermutations; ++permutationRound) {

            QVector<QVector<double> > permutationPValuesPerItemSubset = QtConcurrent::blockingMapped<QVector<QVector<double> > >(statisticalTests, std::function<QVector<double>(const QVector<QSharedPointer<BaseStatisticalTest> > &)>(getPermutationPValuesFromStatisticalTests));

            MatrixOperations::transpose_inplace(permutationPValuesPerItemSubset);

            for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

                updateCountForPermutationPValues(countForPermutationPValuesPerItemSubset[indexOfItemSubsetIdentifier], observedPValuesPerItemSubset.at(indexOfItemSubsetIdentifier), permutationPValuesPerItemSubset.at(indexOfItemSubsetIdentifier));

            }

            QVector<QVector<double> > sortedPermutationPValuesPerItemSubset = permutationPValuesPerItemSubset;

            for (QVector<double> &sortedPermutationPValues : sortedPermutationPValuesPerItemSubset)
                std::sort(sortedPermutationPValues.begin(), sortedPermutationPValues.end());

            for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

                multivariatePermutationThresholdsPerItemSubset[indexOfItemSubsetIdentifier] << getMultivariatePermutationThreshold(sortedObservedPValuesPerItemSubset.at(indexOfItemSubsetIdentifier), sortedPermutationPValuesPerItemSubset.at(indexOfItemSubsetIdentifier));

                if (d_parameters.annotationLabelDefiningPairedItems.isEmpty()) {

                    QVector<unsigned int> &itemGroupCodedIdentifiers(std::get<1>(d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers[d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)]));

                    QVector<unsigned int> const &originalOfItemGroupCodedIdentifiers = itemSubsetIdentifierToOriginalOfItemGroupCodedIdentifiers.at(indexOfItemSubsetIdentifier);

                    if (performMultivariatePermutationTestWithExactModePerItemSubset.at(indexOfItemSubsetIdentifier))
                        std::next_permutation(itemGroupCodedIdentifiers.begin(), itemGroupCodedIdentifiers.end());
                    else {

                        std::shuffle(itemGroupCodedIdentifiers.begin(), itemGroupCodedIdentifiers.end(), g);

                        while (originalOfItemGroupCodedIdentifiers == itemGroupCodedIdentifiers)
                            std::shuffle(itemGroupCodedIdentifiers.begin(), itemGroupCodedIdentifiers.end(), g);


                    }

                } else {

                    QVector<QVector<unsigned int> > const &originalOfAuxiliaryMatrixForPermutatingPairedSamples = itemSubsetIdentifierToOriginalOfAuxiliaryMatrixForPermutatingPairedSamples.at(indexOfItemSubsetIdentifier);

                    QVector<QVector<unsigned int *> > &auxiliaryMatrixForPermutatingPairedSamples = itemSubsetIdentifierToAuxiliaryMatrixForPermutatingPairedSamples[indexOfItemSubsetIdentifier];

                    for (int i = 0; i < auxiliaryMatrixForPermutatingPairedSamples.size(); ++i) {

                        QVector<unsigned int *> &vectorOfPairedGroupCodedIdentifiers = auxiliaryMatrixForPermutatingPairedSamples[i];

                        QVector<unsigned int > const &originalOfVectorOfPairedGroupCodedIdentifiers = originalOfAuxiliaryMatrixForPermutatingPairedSamples.at(i);

                        if (performMultivariatePermutationTestWithExactModePerItemSubset.at(indexOfItemSubsetIdentifier))
                            std::next_permutation(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), [](const unsigned int *value1, const unsigned int *value2) {return value1 < value2;});
                        else {

                            std::shuffle(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), g);

                            while (std::equal(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), originalOfVectorOfPairedGroupCodedIdentifiers.begin(), originalOfVectorOfPairedGroupCodedIdentifiers.end(), [](const unsigned int* value1, const unsigned int value2){ return *value1 == value2; }))
                                std::shuffle(vectorOfPairedGroupCodedIdentifiers.begin(), vectorOfPairedGroupCodedIdentifiers.end(), g);

                        }

                        for (int j = 0; j < vectorOfPairedGroupCodedIdentifiers.size(); ++j)
                            *vectorOfPairedGroupCodedIdentifiers[j] = static_cast<unsigned int>(j);

                    }

                }

            }

            if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

                QVector<double> permutationPValuesForMetaAnalysisTests = QtConcurrent::blockingMapped<QVector<double> >(metaAnalysisTests, std::function<double (const QSharedPointer<BaseMetaAnalysisTest> &)>(getPermutationPValuesFromMetaAnalysisTest));

                updateCountForPermutationPValues(countForPermutationPValuesForMetaAnalysisTests, observedPValuesForMetaAnalysisTests, permutationPValuesForMetaAnalysisTests);

                QVector<double> sortedPermutationPValuesForMetaAnalysisTests = permutationPValuesForMetaAnalysisTests;

                std::sort(sortedPermutationPValuesForMetaAnalysisTests.begin(), sortedPermutationPValuesForMetaAnalysisTests.end());

                multivariatePermutationThresholdsForMetaAnalysis << getMultivariatePermutationThreshold(sortedObservedPValuesForMetaAnalysisTests, sortedPermutationPValuesForMetaAnalysisTests);

            }


            emit updateProgress(0, permutationRound + 1);

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

        }

        emit stopProgress(0);

        QVector<double> finalMultivariatePermutationThresholdPerItemSubset(d_parameters.itemSubsetIdentifiers.size());

        for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

            std::sort(multivariatePermutationThresholdsPerItemSubset[indexOfItemSubsetIdentifier].begin(), multivariatePermutationThresholdsPerItemSubset[indexOfItemSubsetIdentifier].end());

            finalMultivariatePermutationThresholdPerItemSubset[indexOfItemSubsetIdentifier] = MathDescriptives::percentileOfPresortedVector(multivariatePermutationThresholdsPerItemSubset.at(indexOfItemSubsetIdentifier), double(100.0 - d_parameters.confidenceLevel) / 100.0);

            emit appendLogItem(LogListModel::MESSAGE, (d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? QString() : "for item subset \"" + d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier) + "\": ") + "multivariate permutation threshold = "+ QString::number(finalMultivariatePermutationThresholdPerItemSubset.last()));

        }

        double finalMultivariatePermutationThresholdForMetaAnalysis = std::numeric_limits<double>::quiet_NaN();

        if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

            std::sort(multivariatePermutationThresholdsForMetaAnalysis.begin(), multivariatePermutationThresholdsForMetaAnalysis.end());

            finalMultivariatePermutationThresholdForMetaAnalysis = MathDescriptives::percentileOfPresortedVector(multivariatePermutationThresholdsForMetaAnalysis, double(100.0 - d_parameters.confidenceLevel) / 100.0);

            emit appendLogItem(LogListModel::MESSAGE, "multivariate permutation threshold for meta-analysis = "+ QString::number(finalMultivariatePermutationThresholdForMetaAnalysis));

        }

        for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

            QVector<QVector<QVariant> > &resultTable(resultTablePerItemSubset[indexOfItemSubsetIdentifier]);

            const QVector<double> &observedPValues(observedPValuesPerItemSubset.at(indexOfItemSubsetIdentifier));

            double finalMultivariatePermutationThreshold = finalMultivariatePermutationThresholdPerItemSubset.at(indexOfItemSubsetIdentifier);

            const QVector<int> &countForPermutationPValues(countForPermutationPValuesPerItemSubset.at(indexOfItemSubsetIdentifier));

            bool performMultivariatePermutationTestWithExactMode = performMultivariatePermutationTestWithExactModePerItemSubset.at(indexOfItemSubsetIdentifier);

            for (int i = 0; i < resultTable.size(); ++i) {

                double permutationPValue;

                if (performMultivariatePermutationTestWithExactMode)
                    permutationPValue = static_cast<double>(countForPermutationPValues.at(i) - 1) / static_cast<double>(d_parameters.numberOfPermutations);
                else
                    permutationPValue = static_cast<double>(countForPermutationPValues.at(i)) / static_cast<double>(d_parameters.numberOfPermutations);

                resultTable[i] << (permutationPValue == 0.0 ? "<" + QString::number(1.0 / static_cast<double>(d_parameters.numberOfPermutations)) : QString::number(permutationPValue));

                if (observedPValues.at(i) <= finalMultivariatePermutationThreshold)
                    resultTable[i] << "true";
                else
                    resultTable[i] << "false";

            }

        }

        for (int i = 0; i < metaAnalysisResultTable.size(); ++i) {

            double permutationPValue = static_cast<double>(countForPermutationPValuesForMetaAnalysisTests.at(i)) / static_cast<double>(d_parameters.numberOfPermutations);

            metaAnalysisResultTable[i] << (permutationPValue == 0.0 ? "<" + QString::number(1.0 / static_cast<double>(d_parameters.numberOfPermutations)) : QString::number(permutationPValue));

            if (observedPValuesForMetaAnalysisTests.at(i) <= finalMultivariatePermutationThresholdForMetaAnalysis)
                metaAnalysisResultTable[i] << "true";
            else
                metaAnalysisResultTable[i] << "false";

        }

    }

    if (d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

        if (d_parameters.performMultivariatePermutationTest) {

            if (!this->processTableResult(d_parameters.nameOfStatisticalTest, QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << statisticalTests.first().first()->sampleDescriptiveLabels(d_parameters.itemSampleIdentifiers) << statisticalTests.first().first()->testDescriptiveLabels() << "Permutation p-value" << QString("MVP reject H0 (FDR " + QString::number(d_parameters.falseDiscoveryRate) + "%, CL " + QString::number(d_parameters.confidenceLevel) + "%, nPerm " + QString::number(d_parameters.numberOfPermutations) + ")"), d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.first(), QString()))
                return;

        } else {

            if (!this->processTableResult(d_parameters.nameOfStatisticalTest, QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << statisticalTests.first().first()->sampleDescriptiveLabels(d_parameters.itemSampleIdentifiers) << statisticalTests.first().first()->testDescriptiveLabels(), d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.first(), QString()))
                return;

        }

    } else {

        for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

            if (d_parameters.performMultivariatePermutationTest) {

                if (!this->processTableResult("Item subset - " + d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier), QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << statisticalTests.first().first()->sampleDescriptiveLabels(d_parameters.itemSampleIdentifiers) << statisticalTests.first().first()->testDescriptiveLabels() << "Permutation p-value" << QString("MVP reject H0 (FDR " + QString::number(d_parameters.falseDiscoveryRate) + "%, CL " + QString::number(d_parameters.confidenceLevel) + "%, nPerm " + QString::number(d_parameters.numberOfPermutations) + ")"), d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.at(indexOfItemSubsetIdentifier), QString()))
                    return;

            } else {

                if (!this->processTableResult("Item subset - " + d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier), QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << statisticalTests.first().first()->sampleDescriptiveLabels(d_parameters.itemSampleIdentifiers) << statisticalTests.first().first()->testDescriptiveLabels() , d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.at(indexOfItemSubsetIdentifier), QString()))
                    return;

            }

        }

        if (d_parameters.performMultivariatePermutationTest) {

            if (!this->processTableResult("Meta-analysis", QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << metaAnalysisTests.first()->metaAnalysisTestDescriptiveLabels() << "Permutation p-value" << QString("MVP reject H0 (FDR " + QString::number(d_parameters.falseDiscoveryRate) + "%, CL " + QString::number(d_parameters.confidenceLevel) + "%, nPerm " + QString::number(d_parameters.numberOfPermutations) + ")"), d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.first(), QString()))
                return;

        } else {

            if (!this->processTableResult("Meta-analysis", QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << metaAnalysisTests.first()->metaAnalysisTestDescriptiveLabels() , d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.first(), QString()))
                return;

        }

    }

}

#endif // COMPARESAMPLESWORKERCLASS_H
