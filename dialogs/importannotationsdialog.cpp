#include "importannotationsdialog.h"

ImportAnnotationsDialog::ImportAnnotationsDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings)
{

    d_parameters = new ImportAnnotationsWorkerClass::Parameters;

    this->setupWidgets();

}

ImportAnnotationsDialog::~ImportAnnotationsDialog()
{

    delete static_cast<ImportAnnotationsWorkerClass::Parameters *>(d_parameters);

}

void ImportAnnotationsDialog::initializeWidgets()
{

    d_selectFileBuildingBlockWidget = new SelectFileBuildingBlockWidget(this, "selectFileBuildingBlockWidget", "Select file", QFileDialog::AcceptOpen, false);

    d_gridLayout->addWidget(d_selectFileBuildingBlockWidget, 0, 0);

    d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget = new SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget(this, "selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget, 1, 0);

    d_comboBoxBuildingBlockWidget_headerLine = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_headerLine", "Select header line that contains column labels");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_headerLine, 2, 0);

    d_comboBoxBuildingBlockWidget_firstDataLine = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_firstDataLine", "Select first data");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_firstDataLine, 3, 0);

    d_comboBoxBuildingBlockWidget_lastDataLine = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_lastDataLine", "Select last data");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_lastDataLine, 4, 0);

    d_selectDelimiterBuildingBlockWidget = new SelectDelimiterBuildingBlockWidget(this, "selectDelimiterBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectDelimiterBuildingBlockWidget, 5, 0);

    d_selectHeaderLabelsBuildingBlockWidget = new SelectHeaderLabelsBuildingBlockWidget(this, "selectHeaderLabelsBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectHeaderLabelsBuildingBlockWidget, 6, 0);

    this->connect(d_selectFileBuildingBlockWidget, SIGNAL(pathChanged(QString)), this, SLOT(pathChanged(QString)));

    this->connect(d_selectFileBuildingBlockWidget, SIGNAL(invalidPath()), d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox(), SLOT(clear()));

    this->connect(d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(headerLineChanged(int)));

    this->connect(d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(firstDataLineChanged(int)));

    this->connect(d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(lastDataLineChanged(int)));

    this->connect(d_selectDelimiterBuildingBlockWidget, SIGNAL(delimiterChanged()), this, SLOT(updateHeaderLabelsWidget()));

}

void ImportAnnotationsDialog::firstDataLineChanged(int index)
{

    if (index == -1) {

        d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->clear();

        d_lineNumberOfFirstDataLine = -1;

        return;

    }

    d_lineNumberOfFirstDataLine = d_lineNumberOfHeaderLine + index + 1;

    d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->clear();

    int lineNumberStart = d_lineNumberOfFirstDataLine - d_offSetLineNumber;

    if (lineNumberStart + 1 == d_listOfLineInTail.size())
        d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInTail.mid(d_lineNumberOfFirstDataLine - 1, 1));
    else
        d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInTail.mid(((lineNumberStart <= 0) ? 0 : lineNumberStart + 1)));

    d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->setCurrentIndex(d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->count() - 1);

}

void ImportAnnotationsDialog::headerLineChanged(int index)
{

    if (index == -1) {

        d_lineNumberOfHeaderLine = -1;

        d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox()->clear();

        d_selectHeaderLabelsBuildingBlockWidget->headerLineChanged(QStringList());

        return;

    }

    d_lineNumberOfHeaderLine = index + 1;

    d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInHead.mid(index + 1));

    this->updateHeaderLabelsWidget();

}

void ImportAnnotationsDialog::initializeParameters()
{

    ImportAnnotationsWorkerClass::Parameters *parameters = static_cast<ImportAnnotationsWorkerClass::Parameters *>(d_parameters);

    parameters->pathToFile = d_selectFileBuildingBlockWidget->pointerToInternalLineEdit()->text();

    parameters->lineNumberHeaderLine = d_lineNumberOfHeaderLine;

    parameters->lineNumberFirstDataLine = d_lineNumberOfFirstDataLine;

    parameters->lineNumberLastDataLine = d_lineNumberOfLastDataLine;

    parameters->delimiterType = static_cast<SelectDelimiterBuildingBlockWidget::DelimiterType>(d_selectDelimiterBuildingBlockWidget->pointerToInternalComboBox()->currentIndex());

    parameters->custumDelimiter = d_selectDelimiterBuildingBlockWidget->delimiter();

    parameters->caseSensitivityForCustumDelimiter = d_selectDelimiterBuildingBlockWidget->pointerToInternalCheckBox()->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive;

    parameters->indexAndLabelForColumnDefiningRowIdentifiers = d_selectHeaderLabelsBuildingBlockWidget->indexAndLabelForColumnDefiningRowIdentifiers();

    parameters->indexAndLabelForColumnDefiningFirstDataColumn = d_selectHeaderLabelsBuildingBlockWidget->indexAndLabelForColumnDefiningFirstDataColumn();

    parameters->indexAndLabelForColumnDefiningSecondDataColumn = d_selectHeaderLabelsBuildingBlockWidget->indexAndLabelForColumnDefiningSecondDataColumn();

}

void ImportAnnotationsDialog::lastDataLineChanged(int index)
{

    if (index == -1)
        return;

    int lineNumberStart = d_lineNumberOfFirstDataLine - d_offSetLineNumber;

    if (lineNumberStart + 1 == d_listOfLineInTail.size())
        d_lineNumberOfLastDataLine = d_lineNumberOfFirstDataLine;
    else
        d_lineNumberOfLastDataLine = (lineNumberStart <= 0) ? d_offSetLineNumber + index : d_lineNumberOfFirstDataLine + index + 1;

}

void ImportAnnotationsDialog::pathChanged(const QString &path)
{

    QFile file(path);

    if (path.isEmpty() || !file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->clear();

        return;

    }

    int numberOfLinesToReadFromHeadOfFile = d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget->pointerToInternalSpinBoxNumberOfLinesFromHeadOfFile()->value();

    int numberOfLinesToReadFromTailOfFile = d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget->pointerToInternalSpinBoxNumberOfLinesFromTailOfFile()->value();

    int lineCounter = 0;

    QList<qint64> filePositions;

    while (!file.atEnd()) {

        if (++lineCounter > numberOfLinesToReadFromHeadOfFile)
            break;

        filePositions << file.pos();

        d_listOfLineInHead << QString(file.readLine()).remove(QRegExp("[\r\n]"));

        if (d_listOfLineInHead.last().size() > 150)
            d_listOfTrimmedLineInHead << d_listOfLineInHead.last().left(75).simplified() + " ...]----[... " + d_listOfLineInHead.last().right(75).simplified();
        else
            d_listOfTrimmedLineInHead << d_listOfLineInHead.last().simplified();

    }

    while (!file.atEnd()) {

        filePositions << file.pos();

        file.readLine();

    }

    d_offSetLineNumber = filePositions.size() - numberOfLinesToReadFromTailOfFile + 1;

    if (d_offSetLineNumber < 1)
        d_offSetLineNumber = 1;

    file.seek(filePositions[d_offSetLineNumber - 1]);

    while (!file.atEnd()) {

        d_listOfLineInTail << QString(file.readLine()).remove(QRegExp("[\r\n]"));

        if (d_listOfLineInTail.last().size() > 150)
            d_listOfTrimmedLineInTail << d_listOfLineInTail.last().left(75).simplified() + " ...]----[... " + d_listOfLineInTail.last().right(75).simplified();
        else
            d_listOfTrimmedLineInTail << d_listOfLineInTail.last().simplified();

    }

    file.close();

    d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInHead);

}

void ImportAnnotationsDialog::updateHeaderLabelsWidget()
{

    if (d_listOfLineInHead.isEmpty())
        return;

    if (d_selectDelimiterBuildingBlockWidget->pointerToInternalComboBox()->currentIndex() == SelectDelimiterBuildingBlockWidget::CUSTUM)
        d_selectHeaderLabelsBuildingBlockWidget->headerLineChanged(d_listOfLineInHead.at(d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->currentIndex()).split(d_selectDelimiterBuildingBlockWidget->delimiter(), Qt::KeepEmptyParts, (d_selectDelimiterBuildingBlockWidget->pointerToInternalCheckBox()->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive)));
    else
        d_selectHeaderLabelsBuildingBlockWidget->headerLineChanged(d_listOfLineInHead.at(d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->currentIndex()).split(QRegExp(d_selectDelimiterBuildingBlockWidget->delimiter()), Qt::KeepEmptyParts));

}
