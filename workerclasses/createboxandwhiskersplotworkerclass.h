#ifndef CREATEBOXANDWHISKERSPLOTWORKERCLASS_H
#define CREATEBOXANDWHISKERSPLOTWORKERCLASS_H

#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QAbstractSeries>
#include <QDir>
#include <QList>
#include <QHash>

#include "base/basedataset.h"
#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/exportplottofile.h"
#include "charts/boxandwhiskerschart.h"
#include "charts/openplotfromfile.h"
#include "charts/copychartparameters.h"

using namespace QtCharts;


class CreateBoxAndWhiskersPlotWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString annotationLabelDefiningAlternativeVariableIdentifier;

        QString annotationLabelDefiningSubsets;

        QSharedPointer<BaseDataset> baseDataset;

        int dpiOfPlot;

        QString exportDirectory;

        QString exportTypeOfPlot;

        int heightOfPlot;

        BaseMatrix::Orientation orientation;

        QString pathToTemplatePlot;

        QString plotMode;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        QStringList subsetIdentifiers;

        int widthOfPlot;

    };

    CreateBoxAndWhiskersPlotWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CreateBoxAndWhiskersPlotWorkerClass::Parameters d_parameters;

    QSharedPointer<QChart> d_templatePlot;

};

#endif // CREATEBOXANDWHISKERSPLOTWORKERCLASS_H
