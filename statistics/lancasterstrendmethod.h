#ifndef LANCASTERSTRENDMETHOD_H
#define LANCASTERSTRENDMETHOD_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include <cmath>
#include <limits>

#include "boost/math/distributions/chi_squared.hpp"

#include "statistics/basemetaanalysistest.h"

class LancastersTrendMethod : public BaseMetaAnalysisTest
{

public:

    LancastersTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    ~LancastersTrendMethod();

    QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const;

    const QStringList metaAnalysisTestDescriptiveLabels() const;

protected:

    void _reset();

    static QStringList d_baseStatisticalTestDescriptiveLabels;

    double d_degreeOfFreedom;

    static QStringList d_metaAnalysisTestDescriptiveLabels;

    static QString d_nameOfMetaAnalysisTest;

    QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues();

    QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues();

    void calculateStatistic();

    void calculatedPValue();

};

#endif // LANCASTERSTRENDMETHOD_H
