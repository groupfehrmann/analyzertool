#ifndef ITEMSSELECTORBUILDINGBLOCKWIDGET_H
#define ITEMSSELECTORBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QAbstractListModel>
#include <QVariant>
#include <QMimeData>
#include <QSortFilterProxyModel>
#include <QSettings>

#include "dialogs/basedialog.h"

class ItemSelectorModel : public QAbstractListModel
{

public:

    explicit ItemSelectorModel(QObject *parent = nullptr, const QString &communicationIdentifier = QString());

    void clear();

    QVariant data(const QModelIndex &index, int role) const;

    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    const QPair<QStringList, QList<int> > &itemsIdentifierAndNumber();

    QMimeData *mimeData(const QModelIndexList &indexes) const;

    QStringList mimeTypes() const;

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    void setItemsIdentifierAndNumber(const QStringList &identifiers, const QList<int> &numbers);

    void setItemsIdentifier(const QStringList &identifiers);

    Qt::DropActions supportedDropActions() const;

private:

    QPair<QStringList, QList<int> > d_itemsIdentifierAndNumber;

    QString d_communicationIdentifier;

};

namespace Ui {

class ItemsSelectorBuildingBlockWidget;

}

class ItemsSelectorBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit ItemsSelectorBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), const QStringList &itemsIdentifiers = QStringList(), const QList<int> &itemsNumbers = QList<int>());

    ~ItemsSelectorBuildingBlockWidget();

    const QPair<QStringList, QList<int> > &selectedItemsIdentifierAndNumber();

    void setItemsIdentifier(const QStringList &identifiers);

    void setItemsIdentifierAndNumber(const QPair<QStringList, QList<int> > &itemsIdentifierAndNumber);

    void setItemsIdentifierAndNumber(const QStringList &identifiers, const QList<int> &numbers);

    void clear();

private:

    Ui::ItemsSelectorBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

    ItemSelectorModel *d_itemSelectorModel_notSelected;

    ItemSelectorModel *d_itemSelectorModel_selected;

signals:

    void selectionChanged(const QStringList &selectedItemsIdentifier, const QStringList &notSelectedItemsIdentifier);

};


#endif // ITEMSSELECTORBUILDINGBLOCKWIDGET_H
