#ifndef CALCULATEDESCRIPTIVESWORKERCLASS_H
#define CALCULATEDESCRIPTIVESWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QVariantList>

#include <tuple>
#include <algorithm>
#include <cmath>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"
#include "base/convertfunctions.h"

class CalculateDescriptivesWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString annotationLabelDefiningItemSubsets;

        QStringList annotationLabelsDefiningAnnotationValuesForVariables;

        QSharedPointer<BaseDataset> baseDataset;

        QStringList selectedValues;

        QString exportDirectory;

        QStringList descriptives;

        QStringList itemSubsetIdentifiers;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

    };

    CalculateDescriptivesWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CalculateDescriptivesWorkerClass::Parameters d_parameters;

    QHash<QString, QList<int> > d_itemSubsetIdentifierToIndexes;

    bool checkInputParameters();

    template <typename T> void _doWork();

};

template <typename T>
void CalculateDescriptivesWorkerClass::_doWork()
{

    QSharedPointer<Dataset<T> > dataSet = qSharedPointerCast<Dataset<T> >(d_parameters.baseDataset);

    QList<std::function<QVector<QVariant> (int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation)> > descriptiveFunctions;

    QStringList descriptiveColumnLabels;

    QVector<T> selectedValues = ConvertFunctions::convertVector<T, QString>(d_parameters.selectedValues.toVector());

    for (const QString &descriptive : d_parameters.descriptives) {

        if (descriptive == "mean") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::mean(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "mean";

        } else if (descriptive == "mean of absolute values") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::meanOfAbsoluteValues(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "mean of absolute values";

        } else if (descriptive == "mean of squared values") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::meanOfSquaredValues(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "mean of squared values";

        } else if (descriptive == "standard deviation") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::standardDeviation(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "standard deviation";

        } else if (descriptive == "variance") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::variance(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "variance";

        } else if (descriptive == "median") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::median(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "median";

        } else if (descriptive == "sum") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::sum(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "sum";

        } else if (descriptive == "sum of absolute values") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::sumOfAbsoluteValues(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "sum of absolute values";

        } else if (descriptive == "sum of squared values") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::sumOfSquaredValues(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "sum of squared values";

        } else if (descriptive == "minimum") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::minimum(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "minimum";

        } else if (descriptive == "maximum") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::maximum(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "maximum";

        } else if (descriptive == "minimum of absolute values") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::minimumOfAbsoluteValues(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "minimum of absolute values";

        } else if (descriptive == "maximum of absolute values") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::maximumOfAbsoluteValues(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation));};

            descriptiveColumnLabels << "maximum of absolute values";

        } else if (descriptive == "one-sample wilcoxon signed rank - deviation from zero - -log10(p-value)") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << MathDescriptives::OneSampleWilcoxonSignedRankMinusLog10PValue(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation), 0.0);};

            descriptiveColumnLabels << "one-sample wilcoxon signed rank - deviation from zero - -log10(p-value)";

        } else if (descriptive == "count") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << ConvertFunctions::convertVector<QVariant, unsigned long long int>(MathDescriptives::counts(dataSet->matrix()->vector(index_orientation, indexes_oppositeOrientation, orientation), selectedValues));};

            for (const QString &selectedValue : d_parameters.selectedValues)
                descriptiveColumnLabels << "count (\"" + selectedValue + "\")";

        } else if (descriptive == "percentage") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { return QVector<QVariant>() << ConvertFunctions::convertVector<QVariant, double>(MathDescriptives::percentages(dataSet->matrix()->vector(index_orientation, indexes_oppositeOrientation, orientation), selectedValues));};

            for (const QString &selectedValue : d_parameters.selectedValues)
                descriptiveColumnLabels << "percentage (\"" + selectedValue + "\")";

        } else if (descriptive == "coefficient of variation") {

            descriptiveFunctions << [&](int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) { QPair<double, double> meanAndVariance = MathDescriptives::meanAndVariance(dataSet->matrix()->vectorOfDouble(index_orientation, indexes_oppositeOrientation, orientation)); return QVector<QVariant>() << std::sqrt(meanAndVariance.second) / meanAndVariance.first;};

            descriptiveColumnLabels << "coefficient of variation";

        }

    }

    QVector<QVector<QVariant> > resultTable(d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

        if (!d_parameters.annotationLabelsDefiningAnnotationValuesForVariables.isEmpty()) {

            emit startProgress(0, "Collecting annotation values", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

            for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

                resultTable[i] = dataSet->annotationValues(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelsDefiningAnnotationValuesForVariables, d_parameters.orientation).toVector();

                emit updateProgress(0, i + 1);

                if (QThread::currentThread()->isInterruptionRequested()) {

                    QThread::currentThread()->quit();

                    return;

                }

            }

            emit stopProgress(0);

        }

        emit startProgress(0, "Processing item subsets", 0, d_parameters.itemSubsetIdentifiers.size());

    }

    QVector<QVector<QVector<QVariant> > > resultTablePerItemSubset;

    for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

        QVector<QVector<QVariant> > resultTableForCurrentItemSubset(d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

        emit startProgress((d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? 0 : 1), "Calculating descriptives", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

            int indexOfVariable = d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i);

            if (!d_parameters.annotationLabelsDefiningAnnotationValuesForVariables.isEmpty())
                resultTableForCurrentItemSubset[i] = dataSet->annotationValues(indexOfVariable, d_parameters.annotationLabelsDefiningAnnotationValuesForVariables, d_parameters.orientation).toVector();

            for (const std::function<QVector<QVariant> (int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation)> &descriptiveFunction : descriptiveFunctions) {

                QVector<QVariant> temp = descriptiveFunction(indexOfVariable, d_itemSubsetIdentifierToIndexes.value(d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier)), d_parameters.orientation);

                resultTableForCurrentItemSubset[i] << temp;

                if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
                    resultTable[i] << temp;

            }

            emit updateProgress((d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? 0 : 1), i + 1);

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

        }

        emit stopProgress((d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? 0 : 1));

        resultTablePerItemSubset << resultTableForCurrentItemSubset;

        if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
            emit updateProgress(0, indexOfItemSubsetIdentifier + 1);

    }

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
        emit stopProgress(0);

    if (d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

        if (!this->processTableResult("Descriptives", QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << descriptiveColumnLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.first(), d_parameters.exportDirectory))
            return;

    } else {

        for (int indexOfItemSubsetIdentifier = 0; indexOfItemSubsetIdentifier < d_parameters.itemSubsetIdentifiers.size(); ++indexOfItemSubsetIdentifier) {

            if (!this->processTableResult("Descriptives - item subset - " + d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier), QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << descriptiveColumnLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTablePerItemSubset.at(indexOfItemSubsetIdentifier), d_parameters.exportDirectory))
            return;

        }

        QStringList temp;

        for (const QString &itemSubsetIdentifier : d_parameters.itemSubsetIdentifiers) {

            for (const QString &descriptiveColumnLabel : descriptiveColumnLabels)
                temp << (descriptiveColumnLabel + " \"" + itemSubsetIdentifier + "\"");

        }

        if (!this->processTableResult("Descriptives - all item subsets", QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << temp, d_parameters.selectedVariableIdentifiersAndIndexes.first, resultTable, d_parameters.exportDirectory))
            return;

    }

}

#endif // CALCULATEDESCRIPTIVESWORKERCLASS_H
