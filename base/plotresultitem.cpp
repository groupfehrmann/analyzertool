#include "plotresultitem.h"

PlotResultItem::PlotResultItem(QObject *parent, const QString &name, const QSharedPointer<QChart> &chart) :
    BaseResultItem(parent, name), d_chart(chart)
{

    BaseResultItem::d_data->d_type = BaseResultItem::PLOT;

}

void PlotResultItem::setChart(const QSharedPointer<QChart> &chart)
{

    d_chart = chart;

}

const QSharedPointer<QChart> &PlotResultItem::chart()
{

    return d_chart;

}
