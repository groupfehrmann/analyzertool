#ifndef TRANSFORMSEQUENCEBUILDINGBLOCKWIDGET_H
#define TRANSFORMSEQUENCEBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QStringList>
#include <QSettings>
#include <QValidator>
#include <QIntValidator>
#include <QDoubleValidator>
#include <QCompleter>
#include <QTableWidgetItem>
#include <QLineEdit>

#include "dialogs/basedialog.h"
#include "math/mathdescriptives.h"
#include "math/vectoroperations.h"
#include "math/matrixoperations.h"
#include "math/statfunctions.h"

namespace TransformSequenceUtils {

    class TransformFunction : public QObject
    {

        Q_OBJECT

    public:

        TransformFunction(QObject *parent = nullptr, const QString &functionDescription = QString(), const QStringList &parameterDescriptions = QStringList(), const QList<QValidator *> &parameterValidators = QList<QValidator *>());

        QString const &functionDescriptionWithoutParametersReplacement() const;

        QString functionDescriptionWithParametersReplacement() const;

        int numberOfParameters() const;

        const QString &parameterDescription(int index) const;

        const QValidator *parameterValidator(int index);

        virtual std::function<void (QVector<double> &)> func() = 0;

    protected:

        QString d_functionDescription;

        QStringList d_parameterDescriptions;

        QList<QValidator *> d_parameterValidators;

        QVector<QVariant> d_parameterValues;

    public slots:

        void parameterValueChanged(int index, const QString &stringRepresentationOfValue);

    };

    class VectorScalarAdd : public TransformFunction
    {

    public:

        VectorScalarAdd(QObject *parent = nullptr);

        ~VectorScalarAdd();

        std::function<void (QVector<double> &)> func();

    };

    class VectorScalarMin : public TransformFunction
    {

    public:

        VectorScalarMin(QObject *parent = nullptr);

        ~VectorScalarMin();

        std::function<void (QVector<double> &)> func();

    };

    class ScalarVectorMin : public TransformFunction
    {

    public:

        ScalarVectorMin(QObject *parent = nullptr);

        ~ScalarVectorMin();

        std::function<void (QVector<double> &)> func();

    };

    class VectorScalarMul : public TransformFunction
    {

    public:

        VectorScalarMul(QObject *parent = nullptr);

        ~VectorScalarMul();

        std::function<void (QVector<double> &)> func();

    };

    class VectorScalarDiv : public TransformFunction
    {

    public:

        VectorScalarDiv(QObject *parent = nullptr);

        ~VectorScalarDiv();

        std::function<void (QVector<double> &)> func();

    };

    class ScalarVectorDiv : public TransformFunction
    {

    public:

        ScalarVectorDiv(QObject *parent = nullptr);

        ~ScalarVectorDiv();

        std::function<void (QVector<double> &)> func();

    };

    class CenterVectorToMean : public TransformFunction
    {

    public:

        CenterVectorToMean(QObject *parent = nullptr);

        ~CenterVectorToMean();

        std::function<void (QVector<double> &)> func();

    };

    class StandardizeVector : public TransformFunction
    {

    public:

        StandardizeVector(QObject *parent = nullptr);

        ~StandardizeVector();

        std::function<void (QVector<double> &)> func();

    };

    class TransformFunctionList : public QList<TransformFunction *>
    {

    public:

        TransformFunctionList();

        ~TransformFunctionList();

        QStringList functionDescriptions() const;

    };

    class NormalizeVector : public TransformFunction
    {

    public:

        NormalizeVector(QObject *parent = nullptr);

        ~NormalizeVector();

        std::function<void (QVector<double> &)> func();

    };

    class RescaleVectorWithTwoPercentiles : public TransformFunction
    {

    public:

        RescaleVectorWithTwoPercentiles(QObject *parent = nullptr);

        ~RescaleVectorWithTwoPercentiles();

        std::function<void (QVector<double> &)> func();

    };

    class LogTransformVector : public TransformFunction
    {

    public:

        LogTransformVector(QObject *parent = nullptr);

        ~LogTransformVector();

        std::function<void (QVector<double> &)> func();

    };

    class RankVector : public TransformFunction
    {

    public:

        RankVector(QObject *parent = nullptr);

        ~RankVector();

        std::function<void (QVector<double> &)> func();

    };

    class RecodeVectorEqualOrLess : public TransformFunction
    {

    public:

        RecodeVectorEqualOrLess(QObject *parent = nullptr);

        ~RecodeVectorEqualOrLess();

        std::function<void (QVector<double> &)> func();

    };

    class RecodeVectorEqualOrGreater : public TransformFunction
    {

    public:

        RecodeVectorEqualOrGreater(QObject *parent = nullptr);

        ~RecodeVectorEqualOrGreater();

        std::function<void (QVector<double> &)> func();

    };

    class RecodeVectorUnequal : public TransformFunction
    {

    public:

        RecodeVectorUnequal(QObject *parent = nullptr);

        ~RecodeVectorUnequal();

        std::function<void (QVector<double> &)> func();

    };

    class DoubleMedianAbsoluteDeviation : public TransformFunction
    {

    public:

        DoubleMedianAbsoluteDeviation(QObject *parent = nullptr);

        ~DoubleMedianAbsoluteDeviation();

        std::function<void (QVector<double> &)> func();

    };

    class ResolutionBasedOutlierScore : public TransformFunction
    {

    public:

        ResolutionBasedOutlierScore(QObject *parent = nullptr);

        ~ResolutionBasedOutlierScore();

        std::function<void (QVector<double> &)> func();

    };

    class ConvertToAbsoluteValues : public TransformFunction
    {

    public:

        ConvertToAbsoluteValues(QObject *parent = nullptr);

        ~ConvertToAbsoluteValues();

        std::function<void (QVector<double> &)> func();

    };

}

namespace Ui {

class TransformSequenceBuildingBlockWidget;

}

class TransformSequenceBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit TransformSequenceBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString());

    ~TransformSequenceBuildingBlockWidget();

    QList<std::function<void (QVector<double> &)> > formatedTransformFunctionList() const;

    QStringList functionDescriptionsWithParametersReplacement() const;

private:

    Ui::TransformSequenceBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

    TransformSequenceUtils::TransformFunctionList d_transformFunctionList;

    QList<std::function<void (QVector<double> &)> > d_formatedTransformFunctionList;

private slots:

    void comboBox_comboBox_transformFunctions_currentIndexChanged(int index);

    void toolButton_add_clicked();

    void toolButton_remove_clicked();

};

#endif // TRANSFORMSEQUENCEBUILDINGBLOCKWIDGET_H
