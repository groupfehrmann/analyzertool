#include "calculatedistancematrixworkerclass.h"

CalculateDistanceMatrixWorkerClass::CalculateDistanceMatrixWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CalculateDistanceMatrixWorkerClass::Parameters *>(parameters))
{

}

bool CalculateDistanceMatrixWorkerClass::checkInputParameters()
{

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexesDataset1.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected for dataset 1");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexesDataset1.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected for dataset 1");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexesDataset2.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected for dataset 2");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexesDataset2.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected for dataset 2");

        return false;

    }

    return true;

}

void CalculateDistanceMatrixWorkerClass::doWork()
{

    emit processStarted("calculate distance matrix");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Calculate distance matrix"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset 1: \"" + d_parameters.dataset1->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation dataset 1: " + BaseMatrix::orientationToString(d_parameters.orientationDataset1));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected for dataset 1: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexesDataset1.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected for dataset 1: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexesDataset1.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "dataset 2: \"" + d_parameters.dataset2->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation dataset 2: " + BaseMatrix::orientationToString(d_parameters.orientationDataset2));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected for dataset 2: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexesDataset2.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected for dataset 2: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexesDataset2.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "distance function: " + d_parameters.distanceFunction);

    emit appendLogItem(LogListModel::PARAMETER, "calculate p-values: " + (d_parameters.calculatePValues ? QString("enabled") : QString("disabled")));

    if (d_parameters.calculatePValues && ((d_parameters.distanceFunction == "Distance covariance") || (d_parameters.distanceFunction == "Distance correlation") || (d_parameters.distanceFunction == "Distance covariance (pairwise complete method)") || (d_parameters.distanceFunction == "Distance correlation (pairwise complete method)")))
        emit appendLogItem(LogListModel::PARAMETER, "number of permutations: " + QString::number(d_parameters.numberOfPermutations));

    if (!d_parameters.exportDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    emit startProgress(0, "Matching item identifiers between datasets", 0, 0);

    QMultiHash<QString, int> itemIdentifierToIndexDataset1;

    QMultiHash<QString, int> itemIdentifierToIndexDataset2;

    emit startProgress(1, "Creating hash for dataset 1", 0, d_parameters.selectedItemIdentifiersAndIndexesDataset1.second.size());

    for (int i = 0; i < d_parameters.selectedItemIdentifiersAndIndexesDataset1.second.size(); ++i) {

        itemIdentifierToIndexDataset1.insert(d_parameters.selectedItemIdentifiersAndIndexesDataset1.first.at(i), d_parameters.selectedItemIdentifiersAndIndexesDataset1.second.at(i));

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(1, i + 1);

    }

    emit stopProgress(1);

    emit startProgress(1, "Creating hash for dataset 2", 0, d_parameters.selectedItemIdentifiersAndIndexesDataset2.second.size());

    for (int i = 0; i < d_parameters.selectedItemIdentifiersAndIndexesDataset2.second.size(); ++i) {

        itemIdentifierToIndexDataset2.insert(d_parameters.selectedItemIdentifiersAndIndexesDataset2.first.at(i), d_parameters.selectedItemIdentifiersAndIndexesDataset2.second.at(i));

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(1, i + 1);

    }

    QStringList matchedItemIdentifiers;

    QList<int> alignedIndexesForDataset1;

    QList<int> alignedIndexesForDataset2;

    emit startProgress(1, "Aligning item identifiers", 0, d_parameters.selectedItemIdentifiersAndIndexesDataset1.second.size());

    for (int i = 0; i < d_parameters.selectedItemIdentifiersAndIndexesDataset1.second.size(); ++i) {

        QString currentItemIdentifier = d_parameters.selectedItemIdentifiersAndIndexesDataset1.first.at(i);

        if ((itemIdentifierToIndexDataset1.values(currentItemIdentifier).size() > 1) && (itemIdentifierToIndexDataset2.contains(currentItemIdentifier))) {

            emit appendLogItem(LogListModel::ERROR, "item identifier \"" + currentItemIdentifier + "\" is encountered " + QString::number(itemIdentifierToIndexDataset1.values(currentItemIdentifier).size()) + " times in dataset \"" + d_parameters.dataset1->name() + "\"");

            QThread::currentThread()->quit();

            return;

        }

        if (itemIdentifierToIndexDataset2.values(currentItemIdentifier).size() > 1) {

            emit appendLogItem(LogListModel::ERROR, "item identifier \"" + currentItemIdentifier + "\" is encountered " + QString::number(itemIdentifierToIndexDataset2.values(currentItemIdentifier).size()) + " times in dataset \"" + d_parameters.dataset2->name() + "\"");

            QThread::currentThread()->quit();

            return;

        }

        if (itemIdentifierToIndexDataset2.contains(currentItemIdentifier)) {

            alignedIndexesForDataset1 << itemIdentifierToIndexDataset1.value(currentItemIdentifier);

            alignedIndexesForDataset2 << itemIdentifierToIndexDataset2.value(currentItemIdentifier);

            matchedItemIdentifiers << currentItemIdentifier;

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(1, i + 1);

    }

    if (matchedItemIdentifiers.size() < 3) {

        emit appendLogItem(LogListModel::ERROR, "number of matched item identifiers is to small : " + QString::number(matchedItemIdentifiers.size()));

        QThread::currentThread()->quit();

        return;

    } else
        emit appendLogItem(LogListModel::MESSAGE, "number of matched item identifiers : " + QString::number(matchedItemIdentifiers.size()));

    emit stopProgress(1);

    emit stopProgress(0);

    QVector<QVector<double> > allignedDataFromDataset2;

    emit startProgress(0, "Collecting alligned data from dataset 2", 0, alignedIndexesForDataset2.size());

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexesDataset2.second.size(); ++i) {

        allignedDataFromDataset2 << d_parameters.dataset2->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexesDataset2.second.at(i), alignedIndexesForDataset2, d_parameters.orientationDataset2);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    std::function<QPair<double, double> (const QVector<double> &vector1, QVector<double> &vector2)> distanceFunction;

    if (d_parameters.calculatePValues) {

        if (d_parameters.distanceFunction == "Pearson")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {PearsonR<double> pearsonR(vector1, vector2); return QPair<double, double>(pearsonR.statistic(), pearsonR.pValue());};
        else if (d_parameters.distanceFunction == "Spearman")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {SpearmanR<double> spearmanR(vector1, vector2); return QPair<double, double>(spearmanR.statistic(), spearmanR.pValue());};
        else if (d_parameters.distanceFunction == "Distance covariance")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCovariance<double> distanceCovariance(vector1, vector2, d_parameters.numberOfPermutations); return QPair<double, double>(distanceCovariance.distanceCovarianceValue(), distanceCovariance.pValue());};
        else if (d_parameters.distanceFunction == "Distance correlation")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCorrelation<double> distanceCorrelation(vector1, vector2, d_parameters.numberOfPermutations); return QPair<double, double>(distanceCorrelation.distanceCorrelationValue(), distanceCorrelation.pValue());};
        else if (d_parameters.distanceFunction == "Pearson (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {PearsonR<double> pearsonR(vector1, vector2, true); return QPair<double, double>(pearsonR.statistic(), pearsonR.pValue());};
        else if (d_parameters.distanceFunction == "Spearman (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {SpearmanR<double> spearmanR(vector1, vector2, false, 0, true); return QPair<double, double>(spearmanR.statistic(), spearmanR.pValue());};
        else if (d_parameters.distanceFunction == "Distance covariance (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCovariance<double> distanceCovariance(vector1, vector2, d_parameters.numberOfPermutations, true); return QPair<double, double>(distanceCovariance.distanceCovarianceValue(), distanceCovariance.pValue());};
        else if (d_parameters.distanceFunction == "Distance correlation (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCorrelation<double> distanceCorrelation(vector1, vector2, d_parameters.numberOfPermutations, true); return QPair<double, double>(distanceCorrelation.distanceCorrelationValue(), distanceCorrelation.pValue());};

    } else {

        if (d_parameters.distanceFunction == "Pearson")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {PearsonR<double> pearsonR(vector1, vector2); return QPair<double, double>(pearsonR.statistic(), 0.0);};
        else if (d_parameters.distanceFunction == "Spearman")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {SpearmanR<double> spearmanR(vector1, vector2); return QPair<double, double>(spearmanR.statistic(), 0.0);};
        else if (d_parameters.distanceFunction == "Distance covariance")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCovariance<double> distanceCovariance(vector1, vector2); return QPair<double, double>(distanceCovariance.distanceCovarianceValue(), 0.0);};
        else if (d_parameters.distanceFunction == "Distance correlation")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCorrelation<double> distanceCorrelation(vector1, vector2); return QPair<double, double>(distanceCorrelation.distanceCorrelationValue(), 0.0);};
        else if (d_parameters.distanceFunction == "Pearson (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {PearsonR<double> pearsonR(vector1, vector2, true); return QPair<double, double>(pearsonR.statistic(), 0.0);};
        else if (d_parameters.distanceFunction == "Spearman (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {SpearmanR<double> spearmanR(vector1, vector2, false, 0, true); return QPair<double, double>(spearmanR.statistic(), 0.0);};
        else if (d_parameters.distanceFunction == "Distance covariance (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCovariance<double> distanceCovariance(vector1, vector2, true); return QPair<double, double>(distanceCovariance.distanceCovarianceValue(), 0.0);};
        else if (d_parameters.distanceFunction == "Distance correlation (pairwise complete method)")
            distanceFunction = [&](const QVector<double> &vector1, QVector<double> &vector2) {DistanceCorrelation<double> distanceCorrelation(vector1, vector2, true); return QPair<double, double>(distanceCorrelation.distanceCorrelationValue(), 0.0);};

    }

    QMutex mutex;

    double progressValue = 0.0;

    auto mapFunctor = [&](const int &index) {

        QVector<double> currentVectorOfDataset1 = d_parameters.dataset1->baseMatrix()->vectorOfDouble(index, alignedIndexesForDataset1, d_parameters.orientationDataset1);

        QVector<double> tempStatistic;

        QVector<double> tempPValue;

        tempStatistic.reserve(allignedDataFromDataset2.size());

        tempPValue.reserve(allignedDataFromDataset2.size());

        for (int i = 0; i < allignedDataFromDataset2.size(); ++i) {

            QPair<double, double> temp = distanceFunction(allignedDataFromDataset2.at(i), currentVectorOfDataset1);

            tempStatistic << temp.first;

            tempPValue << temp.second;

            mutex.lock();

            progressValue += 1.0;

            mutex.unlock();

        }

        if (d_parameters.calculatePValues)
            return QPair<QVector<double>, QVector<double> >(tempStatistic, tempPValue);
        else
            return QPair<QVector<double>, QVector<double> >(tempStatistic, QVector<double>());

    };

    QFuture<QPair<QVector<double>, QVector<double> > > future = QtConcurrent::mapped(d_parameters.selectedVariableIdentifiersAndIndexesDataset1.second, std::function<QPair<QVector<double>, QVector<double> >(const int &)>(mapFunctor));

    this->monitorFutureAndWaitForFinish(future, 0, "Calculating distances", progressValue, 0.0, static_cast<double>(d_parameters.selectedVariableIdentifiersAndIndexesDataset1.second.size()) * static_cast<double>(d_parameters.selectedVariableIdentifiersAndIndexesDataset2.second.size()));

    if (QThread::currentThread()->isInterruptionRequested()) {

        QThread::currentThread()->quit();

        return;

    }

    QVector<QVector<double> > statisticsMatrix;

    statisticsMatrix.reserve(future.resultCount());

    emit startProgress(0, "Collecting distances (statistics) ", 0, future.resultCount());

    for (int i = 0; i < future.resultCount(); ++i) {

        statisticsMatrix << future.resultAt(i).first;

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(1, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Exporting distances", 0, 0);

    if (!this->processTableResult("Distance - statistics - " + d_parameters.distanceFunction, d_parameters.selectedVariableIdentifiersAndIndexesDataset2.first, d_parameters.selectedVariableIdentifiersAndIndexesDataset1.first, statisticsMatrix, d_parameters.exportDirectory))
        return;

    emit stopProgress(0);

    if (d_parameters.calculatePValues) {

        statisticsMatrix.clear();

        QVector<QVector<double> > pValueMatrix;

        pValueMatrix.reserve(future.resultCount());

        emit startProgress(0, "Collecting distances (p-values) ", 0, future.resultCount());

        for (int i = 0; i < future.resultCount(); ++i) {

            pValueMatrix << future.resultAt(i).second;

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(1, i + 1);

        }

        emit stopProgress(0);

        emit startProgress(0, "Exporting p-values", 0, 0);

        if (!this->processTableResult("Distance - p-values - " + d_parameters.distanceFunction, d_parameters.selectedVariableIdentifiersAndIndexesDataset2.first, d_parameters.selectedVariableIdentifiersAndIndexesDataset1.first, pValueMatrix, d_parameters.exportDirectory))
            return;

        emit stopProgress(0);

    }

    QThread::currentThread()->quit();

}
