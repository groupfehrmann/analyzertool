#ifndef CREATEPIEPLOTSWORKERCLASS_H
#define CREATEPIEPLOTSWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QAbstractSeries>
#include <QDir>

#include <functional>
#include <utility>
#include <algorithm>

#include "base/basedataset.h"
#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/exportplottofile.h"
#include "charts/piechart.h"
#include "base/convertfunctions.h"
#include "charts/openplotfromfile.h"
#include "charts/copychartparameters.h"

using namespace QtCharts;


class CreatePiePlotsWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        bool addPercentageToLabel;

        bool addValueToLabel;

        QString annotationLabelDefiningAlternativeVariableIdentifier;

        QString annotationLabelDefiningAlternativeItemIdentifier;

        QString annotationLabelDefiningSubsets;

        QSharedPointer<BaseDataset> baseDataset;

        int dpiOfPlot;

        QString exportDirectory;

        QString exportTypeOfPlot;

        int heightOfPlot;

        BaseMatrix::Orientation orientation;

        QString pathToTemplatePlot;

        QString plotMode;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        int widthOfPlot;

    };

    CreatePiePlotsWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CreatePiePlotsWorkerClass::Parameters d_parameters;

    QSharedPointer<QChart> d_templatePlot;

};

#endif // CREATEPIEPLOTSWORKERCLASS_H
