#ifndef BARTLETTCHISQUARED_H
#define BARTLETTCHISQUARED_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/fisher_f.hpp"

template <typename T>
class BarlettChiSquared : public BaseStatisticalTest
{

public:

    BarlettChiSquared();

    BarlettChiSquared(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~BarlettChiSquared();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;

    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    QVector<QVector<T> > d_structuredMeasurements;

    QVector<QPair<double, double> > d_sampleMeanAndVariance;

    double d_df;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation BarlettChiSquared<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool BarlettChiSquared<T>::d_hasEffectSize = false;

template <typename T>
QStringList BarlettChiSquared<T>::d_sampleDescriptiveLabels = {"Count", "Mean", "Variance"};

template <typename T>
int BarlettChiSquared<T>::d_maximumNumberOfSamples = std::numeric_limits<int>::max();

template <typename T>
int BarlettChiSquared<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int BarlettChiSquared<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString BarlettChiSquared<T>::d_nameOfEffectSize = "NA";

template <typename T>
QString BarlettChiSquared<T>::d_nameOfTransformedEffectSize = "NA";

template <typename T>
QString BarlettChiSquared<T>::d_nameOfStatisticalTest = "Barlett chi-squared";

template <typename T>
bool BarlettChiSquared<T>::d_rankedBased = false;

template <typename T>
QStringList BarlettChiSquared<T>::d_testDescriptiveLabels = {"Barlett chi-squared", "Degree of freedom", "Significance (2-tailed)"};

template <typename T>
BarlettChiSquared<T>::BarlettChiSquared() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
BarlettChiSquared<T>::BarlettChiSquared(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    this->createStructuredMeasurements();

}

template <typename T>
BarlettChiSquared<T>::~BarlettChiSquared()
{

}

template <typename T>
QVector<QVariant> BarlettChiSquared<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i)
        sampleDescriptiveValues << d_structuredMeasurements.at(i).size() << d_sampleMeanAndVariance.at(i).first << d_sampleMeanAndVariance.at(i).second;

    return sampleDescriptiveValues;

}

template <typename T>
void BarlettChiSquared<T>::calculateStatistic()
{

    double k = d_structuredMeasurements.size();

    double n = d_measurements.size();

    double varp = 0.0;

    double chi1 = 0.0;

    double chi2 = 0.0;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i) {

        d_sampleMeanAndVariance[i] = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(i));

        double vari = d_sampleMeanAndVariance.at(i).second;

        double ni = d_structuredMeasurements.at(i).size();

        varp += ((ni - 1.0) * vari);

        chi1 += (ni - 1.0) * std::log(vari);

        chi2 += 1.0 / (ni - 1.0);

    }

    varp /=  (n - k);

    double chi = ((n - k) * std::log(varp) - chi1) / (1.0 + (1.0 / (3.0 * (k - 1.0))) * (chi2 - (1.0 / (n - k))));

    d_statistic = (chi < 0.0) ? 0.0 : chi;

}

template <typename T>
void BarlettChiSquared<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    d_df = d_structuredMeasurements.size() - 1.0;

    boost::math::chi_squared dist(d_df);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void BarlettChiSquared<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void BarlettChiSquared<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> BarlettChiSquared<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_statistic << d_df << d_pValue;

    return descriptiveValues;

}

template <typename T>
void BarlettChiSquared<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize((*std::max_element(d_itemSampleCodedIdentifiers.begin(), d_itemSampleCodedIdentifiers.end())) + 1);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

    d_sampleMeanAndVariance.resize(d_structuredMeasurements.size());
}

template <typename T>
void BarlettChiSquared<T>::_reset()
{

    this->createStructuredMeasurements();

}

#endif // BARTLETTCHISQUARED_H
