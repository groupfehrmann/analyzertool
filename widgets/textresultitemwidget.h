#ifndef TEXTRESULTITEMWIDGET_H
#define TEXTRESULTITEMWIDGET_H

#include <QWidget>
#include <QTextEdit>

#include "base/textresultitem.h"

namespace Ui {

class TextResultItemWidget;

}

class TextResultItemWidget : public QWidget
{

    Q_OBJECT

public:

    explicit TextResultItemWidget(QWidget *parent = nullptr, QSharedPointer<TextResultItem> textResultItem = QSharedPointer<TextResultItem>());

    ~TextResultItemWidget();

private:

    Ui::TextResultItemWidget *ui;

    QSharedPointer<TextResultItem> d_textResultItem;

};

#endif // TEXTRESULTITEMWIDGET_H
