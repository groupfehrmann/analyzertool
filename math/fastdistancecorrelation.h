#ifndef FASTDISTANCECORRELATION_H
#define FASTDISTANCECORRELATION_H

#include <QVector>

#include <tuple>
#include <cmath>
#include <algorithm>
#include <vector>

#include "math/mathdescriptives.h"

namespace FastDistanceCorrelation {

    double fastDistanceCorrelation(const QVector<double> &x, const QVector<double> &y, bool unbiased = false);

    QVector<double> fastDistanceCovariance(const QVector<double> &x, const QVector<double> &y, bool unbiased = false, bool allParameters = false);

    QVector<double> fastDistanceCovarianceSums(const QVector<double> &x, const QVector<double> &y, bool allSums = false);

    QVector<double> partialSum2D(const QVector<double> &x, const QVector<double> &y, const QVector<double> &c, const QVector<std::tuple<double, int, int, double> > &x_orderStatistics_orderIndices_rank_cummulativeSum, const QVector<std::tuple<double, int, int, double> > &y_orderStatistics_orderIndices_rank_cummulativeSum);

    QVector<double> binaryTreeSums(const QVector<double> &y, const QVector<double> &c);

    bool sortAscendingOnFirstElementOfTupleAndThenOnSecondElementOfTuple(const std::tuple<double, int, int, double> &element1, const std::tuple<double, int, int, double> &element2);

    QVector<std::tuple<double, int, int, double> > createVector_orderStatistics_orderIndices_rank_cummulativeSum(const QVector<double> vector);

}

#endif // FASTDISTANCECORRELATION_H
