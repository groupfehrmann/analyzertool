#ifndef DEDUPLICATEIDENTIFIERSWORKERCLASS_H
#define DEDUPLICATEIDENTIFIERSWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QStringList>

#include <random>
#include <chrono>

#include "base/baseworkerclass.h"
#include "base/convertfunctions.h"

class DeduplicateIdentifiersWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    enum Mode{

        KEEPRANDOM = 0,

        KEEPFIRST = 1,

        KEEPLAST = 2

    };

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedIdentifiersAndIndexes;

        Mode mode;

    };

    DeduplicateIdentifiersWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    DeduplicateIdentifiersWorkerClass::Parameters d_parameters;

};

#endif // DEDUPLICATEIDENTIFIERSWORKERCLASS_H
