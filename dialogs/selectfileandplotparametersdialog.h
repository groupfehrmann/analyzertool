#ifndef SELECTFILEANDPLOTPARAMETERSDIALOG_H
#define SELECTFILEANDPLOTPARAMETERSDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QSettings>
#include <QString>

namespace Ui {

class SelectFileAndPlotParametersDialog;

}

class SelectFileAndPlotParametersDialog : public QDialog
{

    Q_OBJECT

public:

    explicit SelectFileAndPlotParametersDialog(QWidget *parent = 0);

    ~SelectFileAndPlotParametersDialog();

    int dpiOfPlot() const;

    int heigthOfPlot() const;

    QString pathOfPlot() const;

    int widthOfPlot() const;

    void setDPIOfPlot(int dpi);

    void setHeightOfPlot(int height);

    void setWidthOfPlot(int width);

private:

    Ui::SelectFileAndPlotParametersDialog *ui;

private slots:

    void pushButton_browseClicked();

};

#endif // SELECTFILEANDPLOTPARAMETERSDIALOG_H
