#include "selectmultiplefilesbuildingblockwidget.h"
#include "ui_selectmultiplefilesbuildingblockwidget.h"

SelectMultipleFilesBuildingBlockWidget::SelectMultipleFilesBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, QFileDialog::AcceptMode acceptMode, bool rememberSettings) :
    QWidget(parent), ui(new Ui::SelectMultipleFilesBuildingBlockWidget), d_acceptMode(acceptMode), d_rememberSettings(rememberSettings)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/selectFileBuildingBlockWidget");

        if (settings.contains("plainTextEdit"))
            ui->plainTextEdit->setPlainText(settings.value("plainTextEdit").toString());

        settings.endGroup();

    }

    this->connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(pushButton_clicked()));

}

SelectMultipleFilesBuildingBlockWidget::~SelectMultipleFilesBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/selectFileBuildingBlockWidget");

        settings.setValue("plainTextEdit", ui->plainTextEdit->toPlainText());

        settings.endGroup();

    }

    delete ui;

}

QLabel *SelectMultipleFilesBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}

QPlainTextEdit *SelectMultipleFilesBuildingBlockWidget::pointerToInternalPlainTextEdit()
{

    return ui->plainTextEdit;

}

QPushButton *SelectMultipleFilesBuildingBlockWidget::pointerToInternalPushButton()
{

    return ui->pushButton;

}

void SelectMultipleFilesBuildingBlockWidget::pushButton_clicked()
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - Select file");

    fileDialog.setAcceptMode(d_acceptMode);

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    settings.endGroup();

    if (fileDialog.exec()) {

        for (const QString &path : fileDialog.selectedFiles())
            ui->plainTextEdit->appendPlainText(path);

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/fileDialog");

        settings.setValue("state", fileDialog.saveState());

        settings.setValue("geometry", fileDialog.saveGeometry());

        settings.setValue("lastDirectory", fileDialog.directory().path());

        settings.endGroup();

    }

}
