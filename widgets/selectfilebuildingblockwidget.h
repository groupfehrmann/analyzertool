#ifndef SELECTFILEBUILDINGBLOCKWIDGET_H
#define SELECTFILEBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFile>
#include <QFileDialog>

#include "dialogs/basedialog.h"

namespace Ui {

class SelectFileBuildingBlockWidget;

}

class SelectFileBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectFileBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), QFileDialog::AcceptMode = QFileDialog::AcceptOpen, bool rememberSettings = false);

    ~SelectFileBuildingBlockWidget();

    QLabel *pointerToInternalLabel();

    QLineEdit *pointerToInternalLineEdit();

    QPushButton *pointerToInternalPushButton();

private:

    Ui::SelectFileBuildingBlockWidget *ui;

    QFileDialog::AcceptMode d_acceptMode;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

    void evaluatePath();

private slots:

    void lineEdit_edited(const QString &text);

    void pushButton_clicked();


signals:

    void pathChanged(const QString &path);

    void invalidPath();
};

#endif // SELECTFILEBUILDINGBLOCKWIDGET_H
