#include "lancasterstrendmethod.h"

QStringList LancastersTrendMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "Chi-squared", "Degree of freedom", "Sign of statistic", "Sample size"};

QStringList LancastersTrendMethod::d_metaAnalysisTestDescriptiveLabels = {"Chi-squared", "Degree of freedom", "Significance (2-tailed)"};

QString LancastersTrendMethod::d_nameOfMetaAnalysisTest = "Lancaster's trend method";

LancastersTrendMethod::LancastersTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

LancastersTrendMethod::~LancastersTrendMethod()
{

}

QStringList LancastersTrendMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList LancastersTrendMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> LancastersTrendMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        boost::math::chi_squared dist(baseStatisticalTest->sampleSize());

        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << boost::math::quantile(complement(dist, (baseStatisticalTest->pValue()))) << baseStatisticalTest->sampleSize() << (baseStatisticalTest->isSignOfStatisticNegative() ? "-" : "+") << baseStatisticalTest->sampleSize();

    }

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> LancastersTrendMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_degreeOfFreedom << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void LancastersTrendMethod::calculateStatistic()
{

    d_statistic = 0.0;

    d_degreeOfFreedom = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        boost::math::chi_squared dist(baseStatisticalTest->sampleSize());

        if (baseStatisticalTest->isSignOfStatisticNegative())
            d_statistic -= boost::math::quantile(complement(dist, (baseStatisticalTest->pValue())));
        else
            d_statistic += boost::math::quantile(complement(dist, (baseStatisticalTest->pValue())));

        d_degreeOfFreedom += baseStatisticalTest->sampleSize();

    }

}

void LancastersTrendMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_degreeOfFreedom);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

}

void LancastersTrendMethod::_reset()
{

    d_degreeOfFreedom = std::numeric_limits<double>::quiet_NaN();

}
