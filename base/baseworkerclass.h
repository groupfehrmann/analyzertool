#ifndef BASEWORKERCLASS_H
#define BASEWORKERCLASS_H

#include <QObject>
#include <QSharedPointer>
#include <QUuid>
#include <QString>
#include <QTimer>
#include <QThread>

#include "base/basedataset.h"
#include "base/project.h"
#include "base/result.h"
#include "base/baseresultitem.h"
#include "base/loglistmodel.h"
#include "base/tableresultitem.h"
#include "base/exporttofilefunctions.h"

class BaseWorkerClass : public QObject
{

    Q_OBJECT

public:

    enum ValidDataType {

        NUMERICAL,

        CATEGORICAL,

        ALL

    };

    explicit BaseWorkerClass(QObject *parent = nullptr);

    virtual ~BaseWorkerClass();

protected:

    QTimer *d_timer;

    QThread d_timerThread;

    template <typename T>
    bool processTableResult(const QString &name, const QStringList &horizontalHeaderLabels, const QStringList &verticalHeaderLabels, const QVector<QVector<T> > &data, const QString &outputDirectory);

    template<typename T>
    void monitorFutureAndWaitForFinish(const QFuture<T> &future, int progressIdentifier, const QString &progressDescription);

    template<typename T>
    void monitorFutureAndWaitForFinish(const QFuture<T> &future, int progressIdentifier, const QString &progressDescription, double &progressValue, const double &minimumValue, const double &maximumValue);

public slots:

    virtual void doWork() = 0;

signals:

    void addToRecentlyOpenedFiles(const QString &name, const QString &path);

    void annotationsBeginChange(const QUuid &uuid, BaseMatrix::Orientation orientation);

    void annotationsEndChange(const QUuid &uuid, BaseMatrix::Orientation orientation);

    void appendLogItem(LogListModel::Type type, const QString &text);

    void datasetAvailable(BaseDataset *dataset);

    void datasetBeginChange(const QUuid &uuid);

    void datasetEndChange(const QUuid &uuid);

    void matrixDataBeginChange(const QUuid &uuid);

    void matrixDataEndChange(const QUuid &uuid);

    void processStarted(const QString &processTitle);

    void processStopped();

    void projectAvailable(Project *project);

    void resultAvailable(Result *result);

    void resultItemAvailable(BaseResultItem *resultItem);

    void startProgress(int index, const QString &label, const double &progressMinimum, const double &progressMaximum);

    void stopProgress(int index);

    void updateProgress(int index, const double &value);

};

template <typename T>
bool BaseWorkerClass::processTableResult(const QString &name, const QStringList &horizontalHeaderLabels, const QStringList &verticalHeaderLabels, const QVector<QVector<T> > &data, const QString &outputDirectory)
{

    if (outputDirectory.isEmpty()) {

        TableResultItem *tableResultItem = new TableResultItem(nullptr, name);

        tableResultItem->setTableData(horizontalHeaderLabels, verticalHeaderLabels, data);

        emit resultItemAvailable(tableResultItem);

        return true;

    } else {

        QString errorMessage;

        if (!ExportToFileFunctions::exportToFile_tabDelimited(outputDirectory + "/" + name.simplified() + ".txt", horizontalHeaderLabels, verticalHeaderLabels, data, errorMessage)) {

            emit appendLogItem(LogListModel::ERROR, errorMessage);

            QThread::currentThread()->quit();

            return false;

        }

        return true;

    }

}

template<typename T>
void BaseWorkerClass::monitorFutureAndWaitForFinish(const QFuture<T> &future, int progressIdentifier, const QString &progressDescription)
{

    emit startProgress(progressIdentifier, progressDescription, future.progressMinimum(), future.progressMaximum());

    QFutureWatcher<T> futureWatcher;

    this->connect(d_timer, &QTimer::timeout, this, [=, &future](){ emit updateProgress(progressIdentifier, future.progressValue());}, Qt::DirectConnection);

    d_timerThread.start();

    futureWatcher.setFuture(future);

    futureWatcher.waitForFinished();

    d_timerThread.quit();

    this->disconnect(d_timer, nullptr, nullptr, nullptr);

    emit stopProgress(progressIdentifier);

}

template<typename T>
void BaseWorkerClass::monitorFutureAndWaitForFinish(const QFuture<T> &future, int progressIdentifier, const QString &progressDescription, double &progressValue, const double &progressMinimum, const double &progressMaximum)
{

    emit startProgress(progressIdentifier, progressDescription, progressMinimum, progressMaximum);

    QFutureWatcher<T> futureWatcher;

    this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(progressIdentifier, progressValue);}, Qt::DirectConnection);

    d_timerThread.start();

    futureWatcher.setFuture(future);

    futureWatcher.waitForFinished();

    d_timerThread.quit();

    this->disconnect(d_timer, nullptr, nullptr, nullptr);

    emit stopProgress(progressIdentifier);

}

#endif // BASEWORKERCLASS_H
