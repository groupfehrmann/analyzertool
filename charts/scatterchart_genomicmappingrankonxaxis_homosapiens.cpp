#include "scatterchart_genomicmappingrankonxaxis_homosapiens.h"

ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens::ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens(QObject *parent, const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis, QChart::ChartType chartType) :
    BaseChart(parent), d_chartType(chartType)
{

    this->connect(this, SIGNAL(initializeChart(QString,QVector<int>,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)), SLOT(_initializeChart(QString,QVector<int>,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)));

    emit initializeChart(title, chromosomeMappingsForAllSubsets, subsetIdentifiers, ranks_xAxis, values_yAxis);

    d_eventLoop.exec();

}

ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens::ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens(QObject *parent, const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QVector<int> &ranks_xAxis, const QVector<double> &values_yAxis, QChart::ChartType chartType) :
    BaseChart(parent), d_chartType(chartType)
{

    this->connect(this, SIGNAL(initializeChart(QString,QVector<int>,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)), SLOT(_initializeChart(QString,QVector<int>,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)));

    emit initializeChart(title, chromosomeMappingsForAllSubsets, {QString()}, QVector<QVector<int> >() << ranks_xAxis, QVector<QVector<double> >() << values_yAxis);

    d_eventLoop.exec();

}

void ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens::_initializeChart(const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis)
{

    QChart *chart;

    if (d_chartType == QChart::ChartTypePolar)
        chart = new QPolarChart;
    else
        chart = new QChart;

    chart->setTitle(title);


    QCategoryAxis *xAxis = new QCategoryAxis;

    xAxis->setMin(1);

    xAxis->setMax(chromosomeMappingsForAllSubsets.size());

    xAxis->setShadesColor(QColor(245, 245, 245));

    xAxis->setShadesVisible(true);

    xAxis->setShadesBorderColor(Qt::transparent);

    xAxis->setGridLineVisible(false);

    xAxis->setTitleText("Genomic mapping");

    xAxis->setGridLineVisible(false);

    for (int i = 1; i < chromosomeMappingsForAllSubsets.size() - 1; ++i) {

        if (chromosomeMappingsForAllSubsets.at(i - 1) != chromosomeMappingsForAllSubsets.at(i)) {

            int currentChromosomeNumber = chromosomeMappingsForAllSubsets.at(i - 1);

            if (currentChromosomeNumber == 23)
                xAxis->append("X", i + 1);
            else if (currentChromosomeNumber == 24)
                xAxis->append("X", i + 1);
            else
                xAxis->append(QString::number(chromosomeMappingsForAllSubsets.at(i - 1)), i + 1);

        }

    }

    xAxis->append(QString::number(chromosomeMappingsForAllSubsets.last()), chromosomeMappingsForAllSubsets.size());


    QValueAxis *yAxis = new QValueAxis();


    if (d_chartType == QChart::ChartTypePolar) {

        static_cast<QPolarChart *>(chart)->addAxis(xAxis, QPolarChart::PolarOrientationAngular);

        static_cast<QPolarChart *>(chart)->addAxis(yAxis, QPolarChart::PolarOrientationRadial);

    } else {

        chart->addAxis(xAxis, Qt::AlignBottom);

        chart->addAxis(yAxis, Qt::AlignLeft);

    }


    if (values_yAxis.size() == 1)
        chart->legend()->setVisible(false);


    double yMinimumValue = std::numeric_limits<double>::max();

    double yMaximumValue = std::numeric_limits<double>::lowest();


    for (int i = 0; i < subsetIdentifiers.size(); ++i) {

        QScatterSeries *scatterSeries = new QScatterSeries;

        if (!subsetIdentifiers.at(i).isEmpty())
            scatterSeries->setName(subsetIdentifiers.at(i));

        const QVector<int> &current_ranks_xAxis(ranks_xAxis.at(i));

        const QVector<double> &current_values_yAxis(values_yAxis.at(i));


        for (int j = 0; j < current_ranks_xAxis.size(); ++j) {

            double current_value_yAxis = current_values_yAxis.at(j);

            scatterSeries->append(current_ranks_xAxis.at(j), current_value_yAxis);

            yMinimumValue = std::min(yMinimumValue, current_value_yAxis);

            yMaximumValue = std::max(yMaximumValue, current_value_yAxis);

        }

        chart->addSeries(scatterSeries);

        scatterSeries->attachAxis(xAxis);

        scatterSeries->attachAxis(yAxis);

        scatterSeries->setMarkerSize(5);

        scatterSeries->setBorderColor(Qt::transparent);

        scatterSeries->setOpacity(0.5);

    }

    yAxis->setRange(yMinimumValue, yMaximumValue);

    yAxis->applyNiceNumbers();


    d_chart = QSharedPointer<QChart>(chart);


    emit finishedInitializationOfChart();

}
