#ifndef OPENDATASETWORKERCLASS_H
#define OPENDATASETWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QFile>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"

class OpenDatasetWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

    };

    OpenDatasetWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    OpenDatasetWorkerClass::Parameters d_parameters;

    QFile d_fileIn;

private slots:

    void updateFileProgress();

};

#endif // OPENDATASETWORKERCLASS_H
