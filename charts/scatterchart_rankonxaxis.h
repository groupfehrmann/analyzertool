#ifndef SCATTERCHART_RANKONXAXIS_H
#define SCATTERCHART_RANKONXAXIS_H

#include <QObject>
#include <QPolarChart>
#include <QStringList>
#include <QEventLoop>
#include <QScatterSeries>
#include <QCategoryAxis>

#include "charts/exportplottofile.h"
#include "charts/basechart.h"

using namespace QtCharts;

class ScatterChart_RankOnXAxis : public BaseChart
{

    Q_OBJECT

public:

    explicit ScatterChart_RankOnXAxis(QObject *parent = nullptr, const QString &title = QString(), const QStringList &subsetIdentifiers = QStringList(), const QVector<QVector<int> > &ranks_xAxis = QVector<QVector<int> >(), const QVector<QVector<double> > &values_yAxis = QVector<QVector<double> >(), QChart::ChartType chartType = QChart::ChartTypeCartesian);

    explicit ScatterChart_RankOnXAxis(QObject *parent = nullptr, const QString &title = QString(), const QVector<int> &ranks_xAxis = QVector<int>(), const QVector<double> &values_yAxis = QVector<double> (), QChart::ChartType chartType = QChart::ChartTypeCartesian);

private:

    QChart::ChartType d_chartType;

signals:

    void initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis);

private slots:

    void _initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis);

};

#endif // SCATTERCHART_RANKONXAXIS_H
