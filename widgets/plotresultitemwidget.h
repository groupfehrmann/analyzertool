#ifndef PLOTRESULTITEMWIDGET_H
#define PLOTRESULTITEMWIDGET_H

#include <QWidget>
#include <QChartView>
#include <QVBoxLayout>

#include "base/plotresultitem.h"

namespace Ui {

class PlotResultItemWidget;

}

class PlotResultItemWidget : public QWidget
{

    Q_OBJECT

public:

    explicit PlotResultItemWidget(QWidget *parent = nullptr, QSharedPointer<PlotResultItem> plotResultItem = QSharedPointer<PlotResultItem>());

    ~PlotResultItemWidget();

    QChartView *chartView();

private:

    Ui::PlotResultItemWidget *ui;

    QChartView *d_chartView;

    QSharedPointer<PlotResultItem> d_plotResultItem;

    QSharedPointer<QChart> d_chart;

};

#endif // PLOTRESULTITEMWIDGET_H
