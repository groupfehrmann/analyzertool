#ifndef SELECTPENCAPSTYLEWIDGET_H
#define SELECTPENCAPSTYLEWIDGET_H

#include <QWidget>
#include <QPen>
#include <QPixmap>
#include <QPainter>
#include <QPointF>

namespace Ui {

class SelectPenCapStyleWidget;

}

class SelectPenCapStyleWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectPenCapStyleWidget(QWidget *parent = 0, Qt::PenCapStyle defaultPenCapStyle = Qt::FlatCap);

    ~SelectPenCapStyleWidget();

    Qt::PenCapStyle currentPenCapStyle();

public slots:

    void setCurrentPenCapStyle(Qt::PenCapStyle penCapStyle);

private:

    Ui::SelectPenCapStyleWidget *ui;

    QPixmap createPixmap(Qt::PenCapStyle penCapStyle);

private slots:

    void currentIndexOfComboBoxChanged(int index);

signals:

    void currentPenCapStyleChanged(Qt::PenCapStyle penCapStyle);

};

#endif // SELECTPENCAPSTYLEWIDGET_H
