#include "comparesamplescontinuousdialog.h"

CompareSamplesContinuousDialog::CompareSamplesContinuousDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new CompareSamplesWorkerClass::Parameters;

    this->setupWidgets();

}

CompareSamplesContinuousDialog::~CompareSamplesContinuousDialog()
{

   delete static_cast<CompareSamplesWorkerClass::Parameters *>(d_parameters);

}

void CompareSamplesContinuousDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 1, 1);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples", "Select annotation label defining samples");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples, 3, 0);

    d_itemsSelectorBuildingBlockWidget_itemSampleIdentifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_itemSampleIdentifiers", "Select sample identifiers");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_itemSampleIdentifiers, 4, 0);

    d_comboBoxBuildingBlockWidget_nameOfStatisticalTest = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_nameOfStatisticalTest", "Select statistical test", {"Student T", "Welch T", "Levene F", "Mann-Whitney U", "Kolmogorov-Smirnov Z", "Kruskal-Wallis chi-squared", "Bartlett chi-squared", "F-test F", "van der Waerden T"});

    QCompleter *completer = new QCompleter({"Student T", "Welch T", "Levene F", "Mann-Whitney U", "Kolmogorov-Smirnov Z", "Kruskal-Wallis chi-squared", "Brown-Forsythe F", "Bartlett chi-squared", "F-test F", "van der Waerden T"}, this);

    completer->setCaseSensitivity(Qt::CaseInsensitive);

    completer->setFilterMode(Qt::MatchContains);

    d_comboBoxBuildingBlockWidget_nameOfStatisticalTest->pointerToInternalComboBox()->setEditable(true);

    d_comboBoxBuildingBlockWidget_nameOfStatisticalTest->pointerToInternalComboBox()->setCompleter(completer);

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_nameOfStatisticalTest, 5, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets", "Select annotation label defining item subsets (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets, 3, 1);

    d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers", "Select item subset identifiers (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers, 4, 1);

    d_comboBoxBuildingBlockWidget_nameOfMetaAnalysisApproach = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_nameOfMetaAnalysisApproach", "Select meta-analysis approach", {"Fisher's method", "Fisher's trend method", "Stouffer's method", "Stouffer's trend method", "Liptak's method", "Liptak's trend method", "Lancaster's method", "Lancaster's trend method", "Generic inverse method with fixed effect model", "Generic inverse method with fixed random model"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_nameOfMetaAnalysisApproach, 8, 0);

    d_permutationOptionsBuildingBlockWidget = new PermutationOptionsBuildingBlockWidget(this, "permutationOptionsBuildingBlockWidget");

    d_gridLayout->addWidget(d_permutationOptionsBuildingBlockWidget, 9, 0);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables", "Select annotation labels (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables, 10, 0);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory (optional)", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 11, 0);

    d_gridLayout->setColumnStretch(0, 1);

    d_gridLayout->setColumnStretch(1, 1);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->connect(d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples->pointerToInternalComboBox(), SIGNAL(currentTextChanged(QString)), this, SLOT(annotationLabelDefiningItemSamplesChanged(QString)));

    this->connect(d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox(), SIGNAL(currentTextChanged(QString)), this, SLOT(annotationLabelDefiningItemSubsetsChanged(QString)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void CompareSamplesContinuousDialog::initializeParameters()
{

    CompareSamplesWorkerClass::Parameters *parameters = static_cast<CompareSamplesWorkerClass::Parameters *>(d_parameters);

    parameters->annotationLabelDefiningItemSamples = d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningItemSubsets = d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelsDefiningAnnotationValuesForVariables = d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables->selectedItemsIdentifierAndNumber().first;

    parameters->baseDataset = d_baseDataset;

    parameters->confidenceLevel = d_permutationOptionsBuildingBlockWidget->pointerToInternalSpinBox_CL()->value();

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->falseDiscoveryRate = d_permutationOptionsBuildingBlockWidget->pointerToInternalSpinBox_FDR()->value();

    parameters->itemSampleIdentifiers = d_itemsSelectorBuildingBlockWidget_itemSampleIdentifiers->selectedItemsIdentifierAndNumber().first;

    parameters->itemSubsetIdentifiers = d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers->selectedItemsIdentifierAndNumber().first;

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->nameOfMetaAnalysisApproach = d_comboBoxBuildingBlockWidget_nameOfMetaAnalysisApproach->pointerToInternalComboBox()->currentText();

    parameters->nameOfStatisticalTest = d_comboBoxBuildingBlockWidget_nameOfStatisticalTest->pointerToInternalComboBox()->currentText();

    parameters->numberOfPermutations = d_permutationOptionsBuildingBlockWidget->pointerToInternalSpinBox_Permutations()->value();

    parameters->performMultivariatePermutationTest = d_permutationOptionsBuildingBlockWidget->pointerToInternalCheckBox_enable()->isChecked();

    if (parameters->nameOfStatisticalTest == "Student T")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new StudentT<double>());
    else if (parameters->nameOfStatisticalTest == "Welch T")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new WelchT<double>());
    else if (parameters->nameOfStatisticalTest == "Levene F")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new LeveneF<double>());
    else if (parameters->nameOfStatisticalTest == "Mann-Whitney U")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new MannWhitneyU<double>());
    else if (parameters->nameOfStatisticalTest == "Kolmogorov-Smirnov Z")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new KolmogorovsmirnovZ<double>());
    else if (parameters->nameOfStatisticalTest == "Kruskal-Wallis chi-squared")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new KruskalWallisChiSquared<double>());
    else if (parameters->nameOfStatisticalTest == "Brown-Forsythe F")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new BrownForsytheF<double>());
    else if (parameters->nameOfStatisticalTest == "Bartlett chi-squared")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new BarlettChiSquared<double>());
    else if (parameters->nameOfStatisticalTest == "F-test F")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new FTestF<double>());
    else if (parameters->nameOfStatisticalTest == "van der Waerden T")
        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new VanDerWaerdenT<double>());

}

void CompareSamplesContinuousDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples->pointerToInternalComboBox()->addItems(QStringList() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables->setItemsIdentifier(d_baseDataset->annotations(orientation).labels());

}

void CompareSamplesContinuousDialog::annotationLabelDefiningItemSamplesChanged(const QString &currentAnnotationLabel)
{

    if (currentAnnotationLabel.isEmpty()) {

        d_itemsSelectorBuildingBlockWidget_itemSampleIdentifiers->clear();

        return;

    }

    QStringList itemSampleIdentifiers = d_baseDataset->uniqueAnnotationValues<QString>(currentAnnotationLabel, BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget->selectedOrientation()), d_selectedOnly).values();

    itemSampleIdentifiers.removeAll(QString());

    std::sort(itemSampleIdentifiers.begin(), itemSampleIdentifiers.end());

    d_itemsSelectorBuildingBlockWidget_itemSampleIdentifiers->setItemsIdentifier(itemSampleIdentifiers);

}

void CompareSamplesContinuousDialog::annotationLabelDefiningItemSubsetsChanged(const QString &currentAnnotationLabel)
{

    if (currentAnnotationLabel.isEmpty()) {

        d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers->clear();

        return;

    }

    QStringList itemSubsetIdentifiers = d_baseDataset->uniqueAnnotationValues<QString>(currentAnnotationLabel, BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget->selectedOrientation()), d_selectedOnly).values();

    itemSubsetIdentifiers.removeOne(QString());

    std::sort(itemSubsetIdentifiers.begin(), itemSubsetIdentifiers.end());

    d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers->setItemsIdentifier(itemSubsetIdentifiers);

}
