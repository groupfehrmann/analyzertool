#ifndef SETMARGINSPARAMETERSTREEWIDGETITEM_H
#define SETMARGINSPARAMETERSTREEWIDGETITEM_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QMargins>
#include <QSpinBox>

#include "charts/selectpenstylewidget.h"
#include "charts/selectpencapstylewidget.h"
#include "charts/selectpenjoinstylewidget.h"
#include "charts/selectcolorwidget.h"

class SetMarginsParametersTreeWidgetItem : public QObject, QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetMarginsParametersTreeWidgetItem(QTreeWidget *parent = nullptr, const QString &label = "Margins", const QMargins &defaultMargins = QMargins());

    explicit SetMarginsParametersTreeWidgetItem(QTreeWidgetItem *parent = nullptr, const QString &label = "Margins", const QMargins &defaultMargins = QMargins());

    ~SetMarginsParametersTreeWidgetItem();

    QMargins currentMargins();

public slots:

    void setCurrentMargins(const QMargins &margins);

private:

    void initialize(const QString &label, const QMargins &margins);

    QMargins d_margins;

    QSpinBox *d_spinBoxLeftMarginWidget;

    QSpinBox *d_spinBoxTopMarginWidget;

    QSpinBox *d_spinBoxRightMarginWidget;

    QSpinBox *d_spinBoxBottomMarginWidget;

private slots:

    void setLeftMargin(int leftMargin);

    void setTopMargin(int topMargin);

    void setRightMargin(int rightMargin);

    void setBottomMargin(int bottomMargin);

signals:

    void currentMarginsChanged(const QMargins &margins);

};

#endif // SETMARGINSPARAMETERSTREEWIDGETITEM_H
