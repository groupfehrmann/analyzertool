#include "importannotationsworkerclass.h"

ImportAnnotationsWorkerClass::ImportAnnotationsWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<ImportAnnotationsWorkerClass::Parameters *>(parameters))
{

}

void ImportAnnotationsWorkerClass::doWork()
{

    emit processStarted("import " + BaseMatrix::orientationToString(d_parameters.orientation) + " annotations");

    emit appendLogItem(LogListModel::PARAMETER, "import " + BaseMatrix::orientationToString(d_parameters.orientation) + " annotations for dataset \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    emit appendLogItem(LogListModel::PARAMETER, "line number of header containing annotation labels: " + QString::number(d_parameters.lineNumberHeaderLine));

    emit appendLogItem(LogListModel::PARAMETER, "line number of first annotations line: " + QString::number(d_parameters.lineNumberFirstDataLine));

    emit appendLogItem(LogListModel::PARAMETER, "line number of last annotations line: " + QString::number(d_parameters.lineNumberLastDataLine));

    emit appendLogItem(LogListModel::PARAMETER, "column defining row identifiers: index " + QString::number(d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first) + ", label \"" + d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.second + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "column defining first annotation column: index " + QString::number(d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first) + ", label \"" + d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.second + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "column defining second annotation column: index " + QString::number(d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.first) + ", label \"" + d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.second + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "delimiter type: \"" + SelectDelimiterBuildingBlockWidget::delimiterTypeToString(d_parameters.delimiterType) + "\"");

    if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
        emit appendLogItem(LogListModel::PARAMETER, "custum delimiter \"" + d_parameters.custumDelimiter + "\", case sensitive: " + (d_parameters.caseSensitivityForCustumDelimiter == Qt::CaseSensitive ? "true" : "false"));

    Annotations &annotations(d_parameters.baseDataset->annotations(d_parameters.orientation));

    const Header &header(d_parameters.baseDataset->header(d_parameters.orientation));

    d_fileIn.setFileName(d_parameters.pathToFile);

    if (!d_fileIn.exists() || !d_fileIn.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\" -> " + d_fileIn.errorString());

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Importing file", 0, d_fileIn.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    int numberOfLinesRead = 0;

    for (int i = 0; i < d_parameters.lineNumberHeaderLine - 1; ++i) {

        d_fileIn.readLine();

        ++numberOfLinesRead;

    }

    QStringList tokens;

    if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
        tokens = QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(d_parameters.custumDelimiter, Qt::KeepEmptyParts, d_parameters.caseSensitivityForCustumDelimiter);
    else
        tokens = QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(QRegExp(d_parameters.custumDelimiter), Qt::KeepEmptyParts);

    if (tokens.size() < 2) {

        emit appendLogItem(LogListModel::ERROR, "header line needs at least two columns");

        QThread::currentThread()->quit();

        return;

    }

    ++numberOfLinesRead;

    int numberOfTokensInHeaderLine = tokens.size();

    int deltaIndex = (d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.first != -1) ? d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.first - d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first : tokens.size();

    QStringList annotationLabels;

    emit annotationsBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    for (int i = d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first; i < tokens.size(); i += deltaIndex) {

        if (!annotations.appendLabel(tokens.at(i)))
            emit appendLogItem(LogListModel::WARNING, tr("annotation label \"") + tokens.at(i) + "\" is allready present in " + BaseMatrix::orientationToString(d_parameters.orientation) + " annotations");
        else
            annotationLabels << tokens.at(i);

    }

    for (int i = 0; i < d_parameters.lineNumberFirstDataLine - d_parameters.lineNumberHeaderLine - 1; ++i) {

        d_fileIn.readLine();

        ++numberOfLinesRead;

    }

    while (!d_fileIn.atEnd() && (numberOfLinesRead != d_parameters.lineNumberLastDataLine)) {

        if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
            tokens = QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(d_parameters.custumDelimiter, Qt::KeepEmptyParts, d_parameters.caseSensitivityForCustumDelimiter);
        else
            tokens = QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(QRegExp(d_parameters.custumDelimiter), Qt::KeepEmptyParts);

        ++numberOfLinesRead;

        if ((tokens.size() > d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first) && header.contains(tokens.at(d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first))) {

            if (tokens.size() != numberOfTokensInHeaderLine)
                emit appendLogItem(LogListModel::WARNING, "the number of tokens read at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInHeaderLine) + " tokens detected in the header line");

            QVector<QString> vectorOfStrings;

            for (int i = d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first; i < tokens.size(); i += deltaIndex)
                vectorOfStrings << tokens.at(i);

            if (vectorOfStrings.size() != annotationLabels.count()) {

                emit appendLogItem(LogListModel::ERROR, "the number of annotation tokens " + QString::number(vectorOfStrings.size()) + " is unequal to the " + QString::number(annotationLabels.size()) + " annotation labels detected in the header line");

                annotations.removeLabels(annotationLabels);

                emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

                QThread::currentThread()->quit();

                return;

            }

            for (int i = 0; i < annotationLabels.size(); ++i) {

                if (!annotations.appendValue(tokens.at(d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first), annotationLabels.at(i), vectorOfStrings.at(i)))
                    emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + tokens.at(d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first) + "\", label \"" + annotationLabels.at(i) + "\", value to set \"" + vectorOfStrings.at(i) + "\", current value \"" + annotations.value(tokens.at(d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first), annotationLabels.at(i)).toString() + "\"");

            }

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            annotations.removeLabels(annotationLabels);

            emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

            QThread::currentThread()->quit();

            return;

        }

    }


    if (numberOfLinesRead != d_parameters.lineNumberLastDataLine)
        emit appendLogItem(LogListModel::WARNING, "could not read until last data line requested");

    emit stopProgress(0);

    emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    QThread::currentThread()->quit();

}

void ImportAnnotationsWorkerClass::updateFileProgress()
{

    emit updateProgress(0, d_fileIn.pos());

}
