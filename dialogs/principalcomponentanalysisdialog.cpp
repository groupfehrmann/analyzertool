#include "principalcomponentanalysisdialog.h"

PrincipalComponentAnalysisDialog::PrincipalComponentAnalysisDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new PrincipalComponentAnalysisWorkerClass::Parameters;

    this->setupWidgets();

}

PrincipalComponentAnalysisDialog::~PrincipalComponentAnalysisDialog()
{

   delete static_cast<PrincipalComponentAnalysisWorkerClass::Parameters *>(d_parameters);

}

void PrincipalComponentAnalysisDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 2, 0);

    d_comboBoxBuildingBlockWidget_mode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_mode", "Select mode", {"Covariance", "Correlation", "Distance covariance", "Distance correlation"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_mode, 3, 0);

    d_spinBoxBuildingBlockWidget_numberOfComponentsToExtract = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_numberOfComponentsToExtract", "Select number of components to extract", false, 10, 0, 100000, 0, 1);

    this->connect(d_itemsSelectorBuildingBlockWidget_variables, SIGNAL(selectionChanged(QStringList,QStringList)), this, SLOT(selectionOfVariableIdentifiersAndIndexesChanged(QStringList,QStringList)));

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_numberOfComponentsToExtract, 4, 0);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory (optional)", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 5, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

    selectionOfVariableIdentifiersAndIndexesChanged(d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber().first, QStringList());

}

void PrincipalComponentAnalysisDialog::initializeParameters()
{

    PrincipalComponentAnalysisWorkerClass::Parameters *parameters = static_cast<PrincipalComponentAnalysisWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->outputDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->mode = (d_comboBoxBuildingBlockWidget_mode->pointerToInternalComboBox()->currentText() == "Covariance") ? PrincipalComponentAnalysisWorkerClass::COVARIANCE : PrincipalComponentAnalysisWorkerClass::CORRELATION;

    QString currentModeInStringFormat = d_comboBoxBuildingBlockWidget_mode->pointerToInternalComboBox()->currentText();

    if (currentModeInStringFormat == "Covariance")
        parameters->mode = PrincipalComponentAnalysisWorkerClass::COVARIANCE;
    else if (currentModeInStringFormat == "Correlation")
        parameters->mode = PrincipalComponentAnalysisWorkerClass::CORRELATION;
    else if (currentModeInStringFormat == "Distance covariance")
        parameters->mode = PrincipalComponentAnalysisWorkerClass::DISTANCECOVARIANCE;
    else if (currentModeInStringFormat == "Distance correlation")
        parameters->mode = PrincipalComponentAnalysisWorkerClass::DISTANCECORRELATION;

    parameters->numberOfComponentsToExtract = d_spinBoxBuildingBlockWidget_numberOfComponentsToExtract->pointerToInternalSpinBox()->value();

}

void PrincipalComponentAnalysisDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

}

void PrincipalComponentAnalysisDialog::selectionOfVariableIdentifiersAndIndexesChanged(const QStringList &selectedItemsIdentifier, const QStringList &notSelectedItemsIdentifier)
{

    Q_UNUSED(notSelectedItemsIdentifier);

    d_spinBoxBuildingBlockWidget_numberOfComponentsToExtract->pointerToInternalSpinBox()->setMaximum(selectedItemsIdentifier.size());

    d_spinBoxBuildingBlockWidget_numberOfComponentsToExtract->pointerToInternalSpinBox()->setValue(selectedItemsIdentifier.size());

}
