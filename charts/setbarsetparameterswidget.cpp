#include "setbarsetparameterswidget.h"

SetBarSetParametersWidget::SetBarSetParametersWidget(QTreeWidget *parent, const QString &label, QBarSet *barSet) :
    QObject(), QTreeWidgetItem(parent), d_barSet(barSet), d_initialized(false)
{

    if (barSet) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetBarSetParametersWidget::SetBarSetParametersWidget(QTreeWidgetItem *parent, const QString &label, QBarSet *barSet) :
   QObject(), QTreeWidgetItem(parent), d_barSet(barSet), d_initialized(false)
{

    if (barSet) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetBarSetParametersWidget::~SetBarSetParametersWidget()
{

}

void SetBarSetParametersWidget::initialize(const QString &label)
{

    this->setText(0, label);


    d_setLabelWidget = new QLineEdit(d_barSet->label());

    this->connect(d_setLabelWidget, SIGNAL(textEdited(QString)), this, SLOT(setLabel(QString)));

    this->treeWidget()->setItemWidget(this, 1, d_setLabelWidget);


    d_setBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(this, "Brush", d_barSet->brush());

    this->connect(d_setBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setBrush(QBrush)));


    QTreeWidgetItem *labelTreeWidgetItem = new QTreeWidgetItem(this, {"Label"});


    d_selectLabelColorWidget = new SelectColorWidget(nullptr, d_barSet->labelColor());

    this->connect(d_selectLabelColorWidget, SIGNAL(colorSelected(QColor)), this, SLOT(setLabelColor(QColor)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelTreeWidgetItem, {"Color"}), 1, d_selectLabelColorWidget);


    d_selectLabelFontWidget = new SelectFontWidget(nullptr, d_barSet->labelFont());

    this->connect(d_selectLabelFontWidget, SIGNAL(currentFontChanged(QFont)), this, SLOT(setLabelFont(QFont)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelTreeWidgetItem, {"Font"}), 1, d_selectLabelFontWidget);

    d_setPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(this, "Pen", d_barSet->pen());

    this->connect(d_setPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setPen(QPen)));

}

void SetBarSetParametersWidget::setBrush(const QBrush &brush)
{

    d_barSet->setBrush(brush);

}

void SetBarSetParametersWidget::setLabel(const QString label)
{

    d_barSet->setLabel(label);

}

void SetBarSetParametersWidget::setLabelColor(QColor color)
{

    d_barSet->setLabelColor(color);

}

void SetBarSetParametersWidget::setLabelFont(const QFont &font)
{

    d_barSet->setLabelFont(font);

}

void SetBarSetParametersWidget::setPen(const QPen &pen)
{

    d_barSet->setPen(pen);

}

void SetBarSetParametersWidget::setBarSet(const QString &label, QBarSet *barSet)
{

    d_barSet = barSet;

    qDeleteAll(this->takeChildren());

    this->initialize(label);

    d_setBrushParametersTreeWidgetItem->setCurrentBrush(barSet->brush());

    d_setLabelWidget->setText(barSet->label());

    d_selectLabelColorWidget->setCurrentColor(barSet->labelColor());

    d_selectLabelFontWidget->setCurrentFont(barSet->labelFont());

    d_setPenParametersTreeWidgetItem->setCurrentPen(barSet->pen());

}
