#ifndef COPYCHARTPARAMETERS_H
#define COPYCHARTPARAMETERS_H

#include <QObject>
#include <QEventLoop>
#include <QSharedPointer>
#include <QApplication>
#include <QAbstractAxis>
#include <QBarCategoryAxis>
#include <QDateTimeAxis>
#include <QDateTime>
#include <QLogValueAxis>
#include <QValueAxis>
#include <QCategoryAxis>
#include <QAbstractSeries>
#include <QAbstractBarSeries>
#include <QBarSeries>
#include <QHorizontalBarSeries>
#include <QHorizontalPercentBarSeries>
#include <QHorizontalStackedBarSeries>
#include <QPercentBarSeries>
#include <QStackedBarSeries>
#include <QCandlestickSeries>
#include <QAreaSeries>
#include <QBoxPlotSeries>
#include <QPieSeries>
#include <QXYSeries>
#include <QLineSeries>
#include <QSplineSeries>
#include <QScatterSeries>
#include <QBarSet>
#include <QBoxSet>
#include <QPieSlice>
#include <QChart>
#include <QEasingCurve>

using namespace QtCharts;

namespace CopyParameterSettingsFunctions {

    void copyParameterSettings(QAbstractAxis &abstractAxisTo, const QAbstractAxis &abstractAxisFrom);

    void copyParameterSettings(QBarCategoryAxis &barCategoryAxisTo, const QBarCategoryAxis &barCategoryAxisFrom);

    void copyParameterSettings(QDateTimeAxis &dateTimeAxisTo, const QDateTimeAxis &dateTimeAxisFrom);

    void copyParameterSettings(QLogValueAxis &logValueAxisTo, const QLogValueAxis &logValueAxisFrom);

    void copyParameterSettings(QValueAxis &valueAxisTo, const QValueAxis &valueAxisFrom);

    void copyParameterSettings(QCategoryAxis &categoryAxisTo, const QCategoryAxis &categoryAxisFrom);

    void copyParameterSettings(QAbstractSeries &abstractSeriesTo, const QAbstractSeries &abstractSeriesFrom);

    void copyParameterSettings(QAbstractBarSeries &abstractBarSeriesTo, const QAbstractBarSeries &abstractBarSeriesFrom);

    void copyParameterSettings(QBarSeries &barSeriesTo, const QBarSeries &barSeriesFrom);

    void copyParameterSettings(QHorizontalBarSeries &horizontalBarSeriesTo, const QHorizontalBarSeries &horizontalBarSeriesFrom);

    void copyParameterSettings(QHorizontalPercentBarSeries &horizontalPercentBarSeriesTo, const QHorizontalPercentBarSeries &horizontalPercentBarSeriesFrom);

    void copyParameterSettings(QHorizontalStackedBarSeries &horizontalStackedBarSeriesTo, const QHorizontalStackedBarSeries &horizontalStackedBarSeriesFrom);

    void copyParameterSettings(QPercentBarSeries &percentBarSeriesTo, const QPercentBarSeries &percentBarSeriesFrom);

    void copyParameterSettings(QStackedBarSeries &stackedBarSeriesTo, const QStackedBarSeries &stackedBarSeriesFrom);

    void copyParameterSettings(QAreaSeries &areaSeriesTo, const QAreaSeries &areaSeriesFrom);

    void copyParameterSettings(QBoxPlotSeries &boxPlotSeriesTo, const QBoxPlotSeries &boxPlotSeriesFrom);

    void copyParameterSettings(QPieSeries &pieSeriesTo, const QPieSeries &pieSeriesFrom);

    void copyParameterSettings(QXYSeries &xySeriesTo, const QXYSeries &xySeriesFrom);

    void copyParameterSettings(QLineSeries &lineSeriesTo, const QLineSeries &lineSeriesFrom);

    void copyParameterSettings(QSplineSeries &splineSeriesTo, const QSplineSeries &splineSeriesFrom);

    void copyParameterSettings(QScatterSeries &scatterSeriesTo, const QScatterSeries &scatterSeriesFrom);

    void copyParameterSettings(QCandlestickSeries &candlestickSeriesTo, const QCandlestickSeries &candlestickSeriesFrom);

    void copyParameterSettings(QBarSet &barSetTo, const QBarSet &barSetFrom);

    void copyParameterSettings(QBoxSet &boxSetTo, const QBoxSet &boxSetFrom);

    void copyParameterSettings(QPieSlice &pieSliceTo, const QPieSlice &pieSliceFrom);

    void copyParameterSettings(QChart &chartTo, const QChart &chartFrom);

    void copyParameterSettings(QLegend &legendTo, const QLegend &legendFrom);

}

class CopyChartParameters : public QObject
{

    Q_OBJECT

public:

    explicit CopyChartParameters(QObject *parent = 0, QChart *chartTo = nullptr, QChart *chartFrom = nullptr);

private:

    QEventLoop d_eventLoop;

signals:

    void startCopy(QChart *chartTo, QChart *chartFrom);

    void finishedCopy();

private slots:

    void _startCopy(QChart *chartTo, QChart *chartFrom);

};

#endif // COPYCHARTPARAMETERS_H
