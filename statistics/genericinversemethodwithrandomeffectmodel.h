#ifndef GENERICINVERSEMETHODWITHRANDOMEFFECTMODEL_H
#define GENERICINVERSEMETHODWITHRANDOMEFFECTMODEL_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include <cmath>
#include <limits>

#include "boost/math/distributions/normal.hpp"
#include "boost/math/distributions/chi_squared.hpp"

#include "statistics/basemetaanalysistest.h"

class GenericInverseMethodWithRandomEffectModel : public BaseMetaAnalysisTest
{

public:

    GenericInverseMethodWithRandomEffectModel(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    ~GenericInverseMethodWithRandomEffectModel();

    QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const;

    const QStringList metaAnalysisTestDescriptiveLabels() const;

protected:

    void _reset();

    static QString d_nameOfMetaAnalysisTest;

    double d_sumOfInverseVarianceWeights;

    double d_sumOfSquaredInverseVarianceWeights;

    double d_sumOfInverseVarianceWeightedEffectSizes;

    double d_sumOfInverseVarianceWeightedSquaredEffectSizes;

    double d_meanEffectSize;

    double d_q;

    double d_standardErrorEffectSize;

    QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues();

    QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues();

    void calculateStatistic();

    void calculatedPValue();

};

#endif // GENERICINVERSEMETHODWITHRANDOMEFFECTMODEL_H
