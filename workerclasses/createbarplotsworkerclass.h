#ifndef CREATEBARPLOTSWORKERCLASS_H
#define CREATEBARPLOTSWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QAbstractSeries>
#include <QDir>

#include "base/basedataset.h"
#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/barchart.h"
#include "charts/exportplottofile.h"
#include "charts/openplotfromfile.h"
#include "charts/copychartparameters.h"

using namespace QtCharts;

class CreateBarPlotsWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString annotationLabelDefiningAlternativeBarSetLabels;

        QString annotationLabelDefiningAlternativeBarCategoryLabels;

        QPair<QStringList, QList<int> > barCategoryLabels;

        QPair<QStringList, QList<int> > barSetLabels;

        QSharedPointer<BaseDataset> baseDataset;

        QString exportDirectory;

        QString exportTypeOfPlot;

        int dpiOfPlot;

        int heightOfPlot;

        BaseMatrix::Orientation orientation;

        QString pathToTemplatePlot;

        QString plotMode;

        QAbstractSeries::SeriesType seriesType;

        int widthOfPlot;

    };

    CreateBarPlotsWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CreateBarPlotsWorkerClass::Parameters d_parameters;

    QSharedPointer<QChart> d_templatePlot;

};

#endif // CREATEBARPLOTSWORKERCLASS_H
