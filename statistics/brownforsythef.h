#ifndef BROWNFORSYTHEF_H
#define BROWNFORSYTHEF_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/fisher_f.hpp"

template <typename T>
class BrownForsytheF : public BaseStatisticalTest
{

public:

    BrownForsytheF();

    BrownForsytheF(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~BrownForsytheF();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;



    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    QVector<QVector<T> > d_structuredMeasurements;

    QVector<QPair<double, double> > d_sampleMeanAndVariance;

    double d_df1;

    double d_df2;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation BrownForsytheF<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool BrownForsytheF<T>::d_hasEffectSize = false;

template <typename T>
QStringList BrownForsytheF<T>::d_sampleDescriptiveLabels = {"Count", "Mean", "Variance"};

template <typename T>
int BrownForsytheF<T>::d_maximumNumberOfSamples = std::numeric_limits<int>::max();

template <typename T>
int BrownForsytheF<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int BrownForsytheF<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString BrownForsytheF<T>::d_nameOfEffectSize = "NA";

template <typename T>
QString BrownForsytheF<T>::d_nameOfTransformedEffectSize = "NA";

template <typename T>
QString BrownForsytheF<T>::d_nameOfStatisticalTest = "Brown-Forsythe F";

template <typename T>
bool BrownForsytheF<T>::d_rankedBased = false;

template <typename T>
QStringList BrownForsytheF<T>::d_testDescriptiveLabels = {"Brown-Forsythe F", "Degree of freedom 1", "Degree of freedom 2" "Significance (2-tailed)"};

template <typename T>
BrownForsytheF<T>::BrownForsytheF() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
BrownForsytheF<T>::BrownForsytheF(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    this->createStructuredMeasurements();

}

template <typename T>
BrownForsytheF<T>::~BrownForsytheF()
{

}

template <typename T>
QVector<QVariant> BrownForsytheF<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i) {

        QPair<double, double> meandAndVariance = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(i));

        sampleDescriptiveValues << d_structuredMeasurements.at(0).size() << meandAndVariance.first << meandAndVariance.second;

    }

    return sampleDescriptiveValues;

}

template <typename T>
void BrownForsytheF<T>::calculateStatistic()
{

    double w1 = 0.0;

    double z_ = 0.0;

    QVector<double> mean_i;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i) {

        double median = MathDescriptives::median(d_structuredMeasurements.at(i));

        for (T &measurement: d_structuredMeasurements[i])
            measurement = std::fabs(measurement - median);

        double z_i = MathDescriptives::sum(d_structuredMeasurements.at(i));

        z_ += z_i;

        z_i = z_i / d_structuredMeasurements.at(i).size();

        mean_i << z_i;

        for (const T &measurement: d_structuredMeasurements.at(i))
            w1 += (measurement - z_i) * (measurement - z_i);

    }

    z_ = z_ / double(d_measurements.size());

    double w2 = 0.0;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i)
        w2 += d_structuredMeasurements.at(i).size() * ((mean_i.at(i) - z_) * (mean_i.at(i) - z_));

    d_statistic = ((double(d_measurements.size()) - double(d_structuredMeasurements.size())) / double(d_structuredMeasurements.size() - 1.0)) * (w2 / w1);

}

template <typename T>
void BrownForsytheF<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    d_df1 = d_structuredMeasurements.size() - 1.0;

    d_df2 = d_measurements.size() - d_structuredMeasurements.size();

    boost::math::fisher_f dist(d_df1, d_df2);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void BrownForsytheF<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void BrownForsytheF<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> BrownForsytheF<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_statistic << d_df1 << d_df2 << d_pValue;

    return descriptiveValues;

}

template <typename T>
void BrownForsytheF<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize((*std::max_element(d_itemSampleCodedIdentifiers.begin(), d_itemSampleCodedIdentifiers.end())) + 1);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

    d_sampleMeanAndVariance.resize(d_structuredMeasurements.size());
}

template <typename T>
void BrownForsytheF<T>::_reset()
{

    this->createStructuredMeasurements();

}

#endif // BROWNFORSYTHEF_H
