#ifndef SELECTHEADERLABELSBUILDINGBLOCKWIDGET_H
#define SELECTHEADERLABELSBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QSettings>
#include <QComboBox>
#include <QPair>

#include "dialogs/basedialog.h"

namespace Ui {

class SelectHeaderLabelsBuildingBlockWidget;

}

class SelectHeaderLabelsBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectHeaderLabelsBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString());

    ~SelectHeaderLabelsBuildingBlockWidget();

    QPair<int, QString> indexAndLabelForColumnDefiningRowIdentifiers();

    QPair<int, QString> indexAndLabelForColumnDefiningFirstDataColumn();

    QPair<int, QString> indexAndLabelForColumnDefiningSecondDataColumn();

    QComboBox *pointerToInternalComboBoxLabelDefiningRowIdentifiers();

    QComboBox *pointerToInternalComboBoxLabelDefiningFirstDataColumn();

    QComboBox *pointerToInternalComboBoxLabelDefiningSecondDataColumn();

private:

    Ui::SelectHeaderLabelsBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    QStringList d_tokensOfHeaderLine;

    int d_indexOfLabelDefiningRowIdentifiers;

    int d_indexOfLabelDefiningFirstDataColumn;

public slots:

    void comboBoxLabelDefiningRowIdentifiersCurrentIndexChanged(int index);

    void comboBoxLabelDefiningFirstDataColumnCurrentIndexChanged(int index);

    void headerLineChanged(const QStringList &tokensOfHeaderLine);

};

#endif // SELECTHEADERLABELSBUILDINGBLOCKWIDGET_H
