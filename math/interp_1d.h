#ifndef FITAB_H
#define FITAB_H

#include <QVector>

#include <cmath>

struct Base_interp
{
    int n, mm, jsav, cor, dj;
    const double *xx, *yy;

    Base_interp( QVector<double> &x, const double *y, int m );

    double interp( double x );

    int locate( const double x );
    int hunt( const double x );

    double virtual rawinterp( int jlo, double x ) = 0;
};


struct Linear_interp : Base_interp
{
    Linear_interp( QVector<double> &xv, QVector<double> &yv);

    double rawinterp( int j, double x);
};

struct Poly_interp : Base_interp
{
    double dy;

    Poly_interp( QVector<double> &xv, QVector<double> &yv, int m );

    double rawinterp( int jl, double x );
};

struct Rat_interp : Base_interp
{
    double dy;

    Rat_interp( QVector<double> &xv, QVector<double> &yv, int m );

    double rawinterp( int jl, double x );
};

struct Spline_interp : Base_interp
{
    QVector<double> y2;

    Spline_interp( QVector<double> &xv, QVector<double> &yv, double yp1 = 1.e99, double ypn = 1.e99 );

    Spline_interp( QVector<double> &xv, const double *yv, double yp1 = 1.e99, double ypn = 1.e99 );

    void sety2( const double *xv, const double *yv, double yp1, double ypn );

    double rawinterp( int jl, double xv );
};

struct BaryRat_interp : Base_interp
{
    QVector<double> w;
    int d;

    BaryRat_interp( QVector<double> &xv, QVector<double> &yv, int dd );

    double rawinterp( int jl, double x );
    double interp( double x );
};

#endif
