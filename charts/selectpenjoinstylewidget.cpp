#include "selectpenjoinstylewidget.h"
#include "ui_selectpenjoinstylewidget.h"

SelectPenJoinStyleWidget::SelectPenJoinStyleWidget(QWidget *parent, Qt::PenJoinStyle defaultPenJoinStyle) :
    QWidget(parent), ui(new Ui::SelectPenJoinStyleWidget)
{

    ui->setupUi(this);

    ui->comboBox->addItem(this->createPixmap(Qt::MiterJoin), "The outer edges of the lines are extended to meet at an angle, and this area is filled", static_cast<int>(Qt::MiterJoin));

    ui->comboBox->addItem(this->createPixmap(Qt::BevelJoin), "The triangular notch between the two lines is filled", static_cast<int>(Qt::BevelJoin));

    ui->comboBox->addItem(this->createPixmap(Qt::RoundJoin), "A circular arc between the two lines is filled", static_cast<int>(Qt::RoundJoin));

    this->connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexOfComboBoxChanged(int)));

    this->setCurrentPenJoinStyle(defaultPenJoinStyle);

}

SelectPenJoinStyleWidget::~SelectPenJoinStyleWidget()
{

    delete ui;

}

QPixmap SelectPenJoinStyleWidget::createPixmap(Qt::PenJoinStyle PenJoinStyle)
{

    QPixmap pixmap(100, 100);

    pixmap.fill(Qt::transparent);

    QPen pen;

    pen.setWidth(10);

    pen.setStyle(Qt::SolidLine);

    pen.setJoinStyle(PenJoinStyle);

    QPainter painter(&pixmap);

    painter.setPen(pen);

    QPointF points[5] = { QPointF(10.0, 90.0), QPointF(10.0, 10.0), QPointF(90.0, 10.0), QPointF(90.0, 50.0), QPointF(30.0, 50.0) };

    painter.drawPolyline(points, 5);

    return pixmap;

}

void SelectPenJoinStyleWidget::currentIndexOfComboBoxChanged(int index)
{

    Q_UNUSED(index);

    emit currentPenJoinStyleChanged(this->currentPenJoinStyle());

}

Qt::PenJoinStyle SelectPenJoinStyleWidget::currentPenJoinStyle()
{

    return static_cast<Qt::PenJoinStyle>(ui->comboBox->currentData().toInt());

}

void SelectPenJoinStyleWidget::setCurrentPenJoinStyle(Qt::PenJoinStyle PenJoinStyle)
{

    switch (PenJoinStyle) {

        case Qt::MiterJoin: ui->comboBox->setCurrentIndex(0); break;

        case Qt::BevelJoin: ui->comboBox->setCurrentIndex(1); break;

        case Qt::RoundJoin: ui->comboBox->setCurrentIndex(2); break;

        default : ui->comboBox->setCurrentIndex(-1);

    }

}
