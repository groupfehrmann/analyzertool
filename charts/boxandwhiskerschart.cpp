#include "boxandwhiskerschart.h"

BoxAndWhiskersChart::BoxAndWhiskersChart(QObject *parent, const QString &title, const QStringList &variableIdentifiers, const QStringList &subsetIdentifiers, const QVector<QVector<QVector<double> > > &data) :
    BaseChart(parent)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QStringList,QVector<QVector<QVector<double> > >)), this, SLOT(_initializeChart(QString,QStringList,QStringList,QVector<QVector<QVector<double> > >)));

    emit initializeChart(title, variableIdentifiers, subsetIdentifiers, data);

    d_eventLoop.exec();

}



BoxAndWhiskersChart::BoxAndWhiskersChart(QObject *parent, const QString &title, const QVector<double> &data) :
    BaseChart(parent)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList, QStringList,QVector<QVector<QVector<double> > >)), this, SLOT(_initializeChart(QString,QStringList, QStringList,QVector<QVector<QVector<double> > >)));

    emit initializeChart(title, {QString()}, {QString()}, QVector<QVector<QVector<double> > >() << (QVector<QVector<double> >() << data));

    d_eventLoop.exec();

}

void BoxAndWhiskersChart::_initializeChart(const QString &title, const QStringList &variableIdentifiers, const QStringList &subsetIdentifiers, const QVector<QVector<QVector<double> > > &data)
{

    d_chart = QSharedPointer<QChart>(new QChart);

    d_chart->setTitle(title);

    QValueAxis *yAxis = new QValueAxis();

    d_chart->addAxis(yAxis, Qt::AlignLeft);

    QBarCategoryAxis *xAxis = new QBarCategoryAxis();

    d_chart->addAxis(xAxis, Qt::AlignBottom);

    xAxis->append(subsetIdentifiers);

    xAxis->setGridLineVisible(false);


    if (variableIdentifiers.size() == 1)
        d_chart->legend()->setVisible(false);


    double maximum = std::numeric_limits<double>::lowest();

    double minimum = std::numeric_limits<double>::max();


    QScatterSeries *scatterSeriesOutliers = new QScatterSeries;

    scatterSeriesOutliers->setName("outliers");

    scatterSeriesOutliers->setMarkerSize(5);

    scatterSeriesOutliers->setBorderColor(Qt::black);

    scatterSeriesOutliers->setColor(Qt::black);


    QScatterSeries *scatterSeriesSuspectedOutliers = new QScatterSeries;

    scatterSeriesSuspectedOutliers->setName("suspected outliers");

    scatterSeriesSuspectedOutliers->setMarkerSize(5);

    scatterSeriesSuspectedOutliers->setBorderColor(Qt::black);

    scatterSeriesSuspectedOutliers->setColor(Qt::white);


    QVector<double> shiftForOutliers;

    if (data.size() % 2 == 0) {

        for (int i = 0; i < data.size(); ++i)
            shiftForOutliers << (-0.5 + i * 1.0 / static_cast<double>(data.size())) + 1.0 / static_cast<double>(data.size()) / 2.0;

    } else {

        for (int i = 0; i < data.size(); ++i)
            shiftForOutliers <<  (-0.5 + (1.0 + i) * 1.0 / static_cast<double>(data.size())) - 1.0 / static_cast<double>(data.size()) / 2.0;

    }

    for (int i = 0; i < data.size(); ++i) {

        const QVector<QVector<double> > &subsetVectors(data.at(i));

        QBoxPlotSeries *boxPlotSeries = new QBoxPlotSeries;

        boxPlotSeries->setName(variableIdentifiers.at(i) + "- box and whiskers");

        for (int j = 0; j < subsetIdentifiers.size(); ++j) {

            QVector<double> sortedSubsetVector = subsetVectors.at(j);

            std::sort(sortedSubsetVector.begin(), sortedSubsetVector.end());

            QVector<long double> percentiles_25_50_75 = MathDescriptives::percentilesOfPresortedVector(sortedSubsetVector, {0.25, 0.5, 0.75});

            double interQuartileRange = static_cast<double>(percentiles_25_50_75.last() - percentiles_25_50_75.first());

            double lowerOutlierRange = static_cast<double>(percentiles_25_50_75.first() - static_cast<long double>(3.0 * interQuartileRange));

            double upperOutlierRange = static_cast<double>(percentiles_25_50_75.last() + static_cast<long double>(3.0 * interQuartileRange));

            double lowerSuspectedOutlierRange = static_cast<double>(percentiles_25_50_75.first() - static_cast<long double>(1.5 * interQuartileRange));

            double upperSuspectedOutlierRange = static_cast<double>(percentiles_25_50_75.last() + static_cast<long double>(1.5 * interQuartileRange));

            for (const double &value : sortedSubsetVector) {

                if ((value < lowerOutlierRange) || (value > upperOutlierRange))
                    scatterSeriesOutliers->append(j + shiftForOutliers.at(i), value);
                else if ((value < lowerSuspectedOutlierRange) || (value > upperSuspectedOutlierRange))
                    scatterSeriesSuspectedOutliers->append(j + shiftForOutliers.at(i), value);

            }

            if (sortedSubsetVector.last() > maximum)
                maximum = sortedSubsetVector.last();

            if (sortedSubsetVector.first() < minimum)
                minimum = sortedSubsetVector.first();

            QBoxSet *set = new QBoxSet(sortedSubsetVector.first(), static_cast<double>(percentiles_25_50_75.at(0)), static_cast<double>(percentiles_25_50_75.at(1)), static_cast<double>(percentiles_25_50_75.at(2)), sortedSubsetVector.last(), subsetIdentifiers.at(j));

            boxPlotSeries->append(set);

        }

        boxPlotSeries->attachAxis(xAxis);

        boxPlotSeries->attachAxis(yAxis);

        d_chart->addSeries(boxPlotSeries);

//        d_chart->addAxis(xAxis, Qt::AlignBottom);

//        d_chart->addAxis(yAxis, Qt::AlignLeft);

    }

    scatterSeriesOutliers->attachAxis(xAxis);

    scatterSeriesOutliers->attachAxis(yAxis);

    d_chart->addSeries(scatterSeriesOutliers);


    scatterSeriesSuspectedOutliers->attachAxis(xAxis);

    scatterSeriesSuspectedOutliers->attachAxis(yAxis);

    d_chart->addSeries(scatterSeriesSuspectedOutliers);


 //   d_chart->addAxis(xAxis, Qt::AlignBottom);

 //   d_chart->addAxis(yAxis, Qt::AlignLeft);


//    d_chart->addAxis(xAxis, Qt::AlignBottom);

//    d_chart->addAxis(yAxis, Qt::AlignLeft);

/*
    d_chart->setAxisX(xAxis, scatterSeriesOutliers);

    d_chart->setAxisY(yAxis, scatterSeriesOutliers);


    d_chart->setAxisX(xAxis, scatterSeriesSuspectedOutliers);

    d_chart->setAxisY(yAxis, scatterSeriesSuspectedOutliers);
*/

    yAxis->setRange(minimum, maximum);

    yAxis->applyNiceNumbers();


    emit finishedInitializationOfChart();

}
