#ifndef REPLACEIDENTIFIERSWORKERCLASS_H
#define REPLACEIDENTIFIERSWORKERCLASS_H

#include "base/baseworkerclass.h"

class ReplaceIdentifiersWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        QString annotationLabelDefiningNewIdentifiers;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedIdentifiersAndIndexes;

    };

    ReplaceIdentifiersWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    ReplaceIdentifiersWorkerClass::Parameters d_parameters;

};

#endif // REPLACEIDENTIFIERSWORKERCLASS_H
