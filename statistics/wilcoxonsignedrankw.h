#ifndef WILCOXONSIGNEDRANKW_H
#define WILCOXONSIGNEDRANKW_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/vectoroperations.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/normal.hpp"

template <typename T>
class WilcoxonSignedRankW : public BaseStatisticalTest
{

public:

    WilcoxonSignedRankW();

    WilcoxonSignedRankW(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers, QVector<unsigned int> &itemPairCodedIdentifiers);

    ~WilcoxonSignedRankW();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;


    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<unsigned int> &d_itemPairCodedIdentifiers;

    QVector<unsigned int> _itemPairCodedIdentifiers;

    QVector<T> d_measurements;

    double d_tiesCorrectionFactor;

    double d_n;

    double d_countNegatives;

    double d_countPositives;

    double d_sumNegatives;

    double d_sumPositives;

    double d_z;

    double d_meanDifference;

    double d_standardErrorDifference;

    QVector<T> d_structuredMeasurements;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation WilcoxonSignedRankW<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool WilcoxonSignedRankW<T>::d_hasEffectSize = false;

template <typename T>
QStringList WilcoxonSignedRankW<T>::d_sampleDescriptiveLabels = {"Count", "Minimum", "25th Percentile", "Median", "75th Percentile", "Maximum"};

template <typename T>
int WilcoxonSignedRankW<T>::d_maximumNumberOfSamples = 2;

template <typename T>
int WilcoxonSignedRankW<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int WilcoxonSignedRankW<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString WilcoxonSignedRankW<T>::d_nameOfEffectSize = "Not applicable";

template <typename T>
QString WilcoxonSignedRankW<T>::d_nameOfTransformedEffectSize = "Not applicable";

template <typename T>
QString WilcoxonSignedRankW<T>::d_nameOfStatisticalTest = "Wilcoxon signed rank W";

template <typename T>
bool WilcoxonSignedRankW<T>::d_rankedBased = true;

template <typename T>
QStringList WilcoxonSignedRankW<T>::d_testDescriptiveLabels = {"Count of negative ranks", "Mean of negative ranks", "Sum of negative ranks", "Count of positive ranks", "Mean of positive ranks", "Sum of positive ranks", "Count of ties", "R", "Wilcoxon signed-rank W", "Z", "Significance (2-tailed)"};


template <typename T>
WilcoxonSignedRankW<T>::WilcoxonSignedRankW() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers), d_itemPairCodedIdentifiers(_itemPairCodedIdentifiers)
{

}

template <typename T>
WilcoxonSignedRankW<T>::WilcoxonSignedRankW(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers, QVector<unsigned int> &itemPairCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_itemPairCodedIdentifiers(itemPairCodedIdentifiers), d_measurements(measurements)
{

    this->createStructuredMeasurements();

}

template <typename T>
WilcoxonSignedRankW<T>::~WilcoxonSignedRankW()
{

}

template <typename T>
QVector<QVariant> WilcoxonSignedRankW<T>::calculateSamplesDescriptivesValues()
{
    QVector<QVector<T> > structuredMeasurements;

    structuredMeasurements.clear();

    structuredMeasurements.resize(2);

    for (int i = 0; i < d_measurements.size(); ++i)
        structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

    QVector<QVariant> sampleDescriptiveValues;

    std::sort(structuredMeasurements[0].begin(), structuredMeasurements[0].end());

    std::sort(structuredMeasurements[1].begin(), structuredMeasurements[1].end());

    QVector<long double> percentilesSampleOne = MathDescriptives::percentilesOfPresortedVector(structuredMeasurements.at(0), {0.0, 0.25, 0.5, 0.75, 1.0});

    QVector<long double> percentilesSampleTwo = MathDescriptives::percentilesOfPresortedVector(structuredMeasurements.at(1), {0.0, 0.25, 0.5, 0.75, 1.0});

    sampleDescriptiveValues << structuredMeasurements.at(0).size() << percentilesSampleOne.at(0) << percentilesSampleOne.at(1) << percentilesSampleOne.at(2) << percentilesSampleOne.at(3) << percentilesSampleOne.at(4);

    sampleDescriptiveValues << structuredMeasurements.at(1).size() << percentilesSampleTwo.at(0) << percentilesSampleTwo.at(1) << percentilesSampleTwo.at(2) << percentilesSampleTwo.at(3) << percentilesSampleTwo.at(4);

    return sampleDescriptiveValues;

}

template <typename T>
void WilcoxonSignedRankW<T>::calculateStatistic()
{

    d_n = d_structuredMeasurements.size();

    d_countNegatives = 0.0;

    d_countPositives = 0.0;

    d_sumNegatives = 0.0;

    d_sumPositives = 0.0;

    for (int i = 0; i < d_n; ++i) {

        if (d_structuredMeasurements.at(i) > 0) {

            ++d_countPositives;

            d_sumPositives += d_structuredMeasurements.at(i);

        } else if (d_structuredMeasurements.at(i) < 0) {

            ++d_countNegatives;

            d_sumNegatives += d_structuredMeasurements.at(i);

        }

    }

    d_statistic = d_sumNegatives + d_sumPositives;

    d_n = d_countNegatives + d_countPositives;

    if (d_n == 0.0)
        d_statistic = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void WilcoxonSignedRankW<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    if (d_sumPositives < std::fabs(d_sumNegatives))
        d_z = (d_sumPositives - (d_n * (d_n + 1.0) / 4.0)) / sqrt(((d_n * (d_n + 1.0) * (2.0 * d_n + 1.0)) / 24.0) - (d_tiesCorrectionFactor / 48.0));
    else
        d_z = -1.0 * ((std::fabs(d_sumNegatives) - (d_n * (d_n + 1.0) / 4.0)) / sqrt(((d_n * (d_n + 1.0) * (2.0 * d_n + 1.0)) / 24.0) - (d_tiesCorrectionFactor / 48.0)));

    if (d_n == 0.0)
        d_z = 0.0;

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, fabs(d_z)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void WilcoxonSignedRankW<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void WilcoxonSignedRankW<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> WilcoxonSignedRankW<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_countNegatives << ((d_countNegatives == 0) ? 0 : (d_sumNegatives / d_countNegatives)) << d_sumNegatives << d_countPositives << ((d_countPositives == 0) ? 0 : (d_sumPositives / d_countPositives)) << d_sumPositives << d_structuredMeasurements.size() - d_countNegatives - d_countPositives << d_z / std::sqrt(2.0 * d_structuredMeasurements.size()) << d_statistic << d_z << d_pValue;

    return descriptiveValues;

}

template <typename T>
void WilcoxonSignedRankW<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    QVector<QVector<int> > pairedItemIndexes(int(d_itemPairCodedIdentifiers.size() / 2.0), QVector<int>(2));

    for (int i = 0; i < d_itemSampleCodedIdentifiers->size(); ++i)
        pairedItemIndexes[d_itemPairCodedIdentifiers.at(i)][d_itemSampleCodedIdentifiers.at(i)] = i;

    QVector<T> signs;

    QVector<T*> pointersToStructuredMeasurements;

    d_structuredMeasurements.resize(pairedIndexes.size());

    for (int i = 0; i < pairedItemIndexes.size(); ++i) {

        T difference = d_measurements.at(pairedItemIndexes.at(i).at(0)) - d_measurements.at(pairedItemIndexes.at(i).at(1));

        d_structuredMeasurements[i] = std::fabs(difference);

        signs << ((difference < 0.0) ? -1 : 1);

        if (difference != 0.0)
            pointersToStructuredMeasurements << &d_structuredMeasurements[i];

    }

    d_tiesCorrectionFactor = VectorOperations::crank_ptrs(pointersToStructuredMeasurements);

    for (int i = 0; i < d_structuredMeasurements.size(); ++i)
        d_structuredMeasurements[i] *= signs.at(i);

}

template <typename T>
void WilcoxonSignedRankW<T>::_reset()
{

    this->createStructuredMeasurements();

}


#endif // WILCOXONSIGNEDRANKW_H
