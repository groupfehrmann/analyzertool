#include "selectwithregularexpressiondialog.h"

SelectWithRegularExpressionDialog::SelectWithRegularExpressionDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new SelectWithRegularExpressionWorkerClass::Parameters;

    this->setupWidgets();

}

SelectWithRegularExpressionDialog::~SelectWithRegularExpressionDialog()
{

   delete static_cast<SelectWithRegularExpressionWorkerClass::Parameters *>(d_parameters);

}

void SelectWithRegularExpressionDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_identifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_identifiers", "Select identifiers");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_identifiers, 1, 0);

    d_selectRegularExpressionFiltersBuildingBlockWidget = new SelectRegularExpressionFiltersBuildingBlockWidget(this, "selectRegularExpressionFiltersBuildingBlockWidget", "Select regular expression filters", d_baseDataset);

    d_gridLayout->addWidget(d_selectRegularExpressionFiltersBuildingBlockWidget, 2, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void SelectWithRegularExpressionDialog::initializeParameters()
{

    SelectWithRegularExpressionWorkerClass::Parameters *parameters = static_cast<SelectWithRegularExpressionWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->selectedIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_identifiers->selectedItemsIdentifierAndNumber();

    parameters->filters = d_selectRegularExpressionFiltersBuildingBlockWidget->getFilters();

}

void SelectWithRegularExpressionDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > identifiersAndIndexes = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_identifiers->setItemsIdentifierAndNumber(identifiersAndIndexes.first, identifiersAndIndexes.second);

    d_selectRegularExpressionFiltersBuildingBlockWidget->setOrientation(orientation);

}
