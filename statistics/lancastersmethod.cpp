#include "lancastersmethod.h"

QStringList LancastersMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "Chi-squared", "Degree of freedom", "Sample size"};

QStringList LancastersMethod::d_metaAnalysisTestDescriptiveLabels = {"Chi-squared", "Degree of freedom", "Significance (2-tailed)"};

QString LancastersMethod::d_nameOfMetaAnalysisTest = "Lancaster's method";

LancastersMethod::LancastersMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

LancastersMethod::~LancastersMethod()
{

}

QStringList LancastersMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList LancastersMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> LancastersMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        boost::math::chi_squared dist(baseStatisticalTest->sampleSize());

        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << boost::math::quantile(complement(dist, (baseStatisticalTest->pValue()))) << baseStatisticalTest->sampleSize() << baseStatisticalTest->sampleSize();

    }

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> LancastersMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_degreeOfFreedom << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void LancastersMethod::calculateStatistic()
{

    d_statistic = 0.0;

    d_degreeOfFreedom = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        boost::math::chi_squared dist(baseStatisticalTest->sampleSize());

        d_statistic += boost::math::quantile(complement(dist, (baseStatisticalTest->pValue())));

        d_degreeOfFreedom += baseStatisticalTest->sampleSize();

    }

}

void LancastersMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_degreeOfFreedom);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

}

void LancastersMethod::_reset()
{

    d_degreeOfFreedom = std::numeric_limits<double>::quiet_NaN();

}
