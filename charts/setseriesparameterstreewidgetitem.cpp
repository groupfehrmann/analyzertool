#include "setseriesparameterstreewidgetitem.h"

SetSeriesParametersTreeWidgetItem::SetSeriesParametersTreeWidgetItem(QTreeWidget *parent, const QString &label, QAbstractSeries *series) :
    QTreeWidgetItem(parent), d_series(series), d_initialized(false)
{

    if (series) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetSeriesParametersTreeWidgetItem::SetSeriesParametersTreeWidgetItem(QTreeWidgetItem *parent, const QString &label, QAbstractSeries *series) :
   QTreeWidgetItem(parent), d_series(series), d_initialized(false)
{

    if (series) {

        this->initialize(label);

        d_initialized = true;

    }

}

void SetSeriesParametersTreeWidgetItem::initialize(const QString &label)
{

    this->setText(0, label);


    d_setNameWidget = new QLineEdit(d_series->name());

    this->connect(d_setNameWidget, SIGNAL(textEdited(QString)), this, SLOT(setName(QString)));

    this->treeWidget()->setItemWidget(this, 1, d_setNameWidget);

    if ((  d_series->type() == QAbstractSeries::SeriesTypeBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeStackedBar)
       || (d_series->type() == QAbstractSeries::SeriesTypePercentBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeHorizontalBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeHorizontalStackedBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeHorizontalPercentBar)) {

        QTreeWidgetItem *barSetsTreeWidgetItem = new QTreeWidgetItem(this, {"Bar sets"});

        for (int i = 0; i < static_cast<QAbstractBarSeries *>(d_series)->barSets().size(); ++i)
            new SetBarSetParametersWidget(barSetsTreeWidgetItem, "Bar set " + QString::number(i + 1), static_cast<QAbstractBarSeries *>(d_series)->barSets().at(i));


        d_selectBarWidthWidget = new QDoubleSpinBox;

        d_selectBarWidthWidget->setRange(0, 1.0);

        d_selectBarWidthWidget->setDecimals(2);

        d_selectBarWidthWidget->setSingleStep(0.01);

        d_selectBarWidthWidget->setValue(static_cast<QAbstractBarSeries *>(d_series)->barWidth());

        this->connect(d_selectBarWidthWidget, SIGNAL(valueChanged(double)), this, SLOT(setBarWidth(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Bar width"}), 1, d_selectBarWidthWidget);

    }

    if (d_series->type() == QAbstractSeries::SeriesTypeBoxPlot) {

        QTreeWidgetItem *boxTreeWidgetItem = new QTreeWidgetItem(this, {"Box"});


        d_selectBoxOutlineVisibleWidget = new QCheckBox;

        d_selectBoxOutlineVisibleWidget->setChecked(static_cast<QBoxPlotSeries *>(d_series)->boxOutlineVisible());

        this->connect(d_selectBoxOutlineVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setBoxOutlineVisible(bool)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(boxTreeWidgetItem, {"Outline visible"}), 1, d_selectBoxOutlineVisibleWidget);


        d_selectBoxWidthWidget = new QDoubleSpinBox;

        d_selectBoxWidthWidget->setRange(0, 1000.0);

        d_selectBoxWidthWidget->setDecimals(2);

        d_selectBoxWidthWidget->setSingleStep(0.01);

        d_selectBoxWidthWidget->setValue(static_cast<QBoxPlotSeries *>(d_series)->boxWidth());

        this->connect(d_selectBoxWidthWidget, SIGNAL(valueChanged(double)), this, SLOT(setBoxWidth(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(boxTreeWidgetItem, {"Width"}), 1, d_selectBoxWidthWidget);


        QTreeWidgetItem *boxSetsTreeWidgetItem = new QTreeWidgetItem(this, {"Box sets"});

        for (int i = 0; i < static_cast<QBoxPlotSeries *>(d_series)->boxSets().size(); ++i)
            new SetBoxSetParametersWidget(boxSetsTreeWidgetItem, "Box set " + QString::number(i + 1), static_cast<QBoxPlotSeries *>(d_series)->boxSets().at(i));

    }

    if ((  d_series->type() == QAbstractSeries::SeriesTypeLine)
       || (d_series->type() == QAbstractSeries::SeriesTypeScatter)
       || (d_series->type() == QAbstractSeries::SeriesTypeSpline)) {

        d_setBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(this, "Brush", static_cast<QXYSeries *>(d_series)->brush());

        this->connect(d_setBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setBrush(QBrush)));

    }

    if (d_series->type() == QAbstractSeries::SeriesTypeBoxPlot) {

        d_setBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(this, "Brush", static_cast<QBoxPlotSeries *>(d_series)->brush());

        this->connect(d_setBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setBrush(QBrush)));

    }

    if ((  d_series->type() == QAbstractSeries::SeriesTypeBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeStackedBar)
       || (d_series->type() == QAbstractSeries::SeriesTypePercentBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeHorizontalBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeHorizontalStackedBar)
       || (d_series->type() == QAbstractSeries::SeriesTypeHorizontalPercentBar)) {

        QTreeWidgetItem *labelsTreeWidgetItem = new QTreeWidgetItem(this, {"Labels"});


        d_selectLabelsAngleWidget = new QSpinBox;

        d_selectLabelsAngleWidget->setRange(-360, 360);

        d_selectLabelsAngleWidget->setValue(static_cast<QAbstractBarSeries *>(d_series)->labelsAngle());

        this->connect(d_selectLabelsAngleWidget, SIGNAL(valueChanged(int)), this, SLOT(setLabelsAngle(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Angle"}), 1, d_selectLabelsAngleWidget);


        d_selectLabelsFormatWidget = new QLineEdit(static_cast<QAbstractBarSeries *>(d_series)->labelsFormat());

        this->connect(d_selectLabelsFormatWidget, SIGNAL(textEdited(QString)), this, SLOT(setLabelsFormat(QString)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Format"}), 1, d_selectLabelsFormatWidget);


        d_selectLabelsPositionWidget = new QComboBox();

        d_selectLabelsPositionWidget->addItem("Label is in the center of the bar", QAbstractBarSeries::LabelsCenter);

        d_selectLabelsPositionWidget->addItem("Label is inside the bar at the high end of it", QAbstractBarSeries::LabelsInsideEnd);

        d_selectLabelsPositionWidget->addItem("Label is inside the bar at the low end of it", QAbstractBarSeries::LabelsInsideBase);

        d_selectLabelsPositionWidget->addItem("Label is outside the bar at the high end of it", QAbstractBarSeries::LabelsOutsideEnd);

        d_selectLabelsPositionWidget->setCurrentIndex(static_cast<QAbstractBarSeries *>(d_series)->labelsPosition());

        this->connect(d_selectLabelsPositionWidget, SIGNAL(currentIndexChanged(int)), this, SLOT(setLabelsPosition(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Position"}), 1, d_selectLabelsPositionWidget);


        d_selectLabelsVisibleWidget = new QCheckBox;

        d_selectLabelsVisibleWidget->setChecked(static_cast<QAbstractBarSeries *>(d_series)->isLabelsVisible());

        this->connect(d_selectLabelsVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setLabelsVisible(bool)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Visible"}), 1, d_selectLabelsVisibleWidget);

    }


    if (d_series->type() == QAbstractSeries::SeriesTypeScatter) {

        QTreeWidgetItem *markerTreeWidgetItem = new QTreeWidgetItem(this, {"Marker"});


        d_selectMarkerShapeWidget = new QComboBox();

        d_selectMarkerShapeWidget->addItem("Circle", QScatterSeries::MarkerShapeCircle);

        d_selectMarkerShapeWidget->addItem("Rectangle", QScatterSeries::MarkerShapeRectangle);

        d_selectMarkerShapeWidget->setCurrentIndex(static_cast<QScatterSeries *>(d_series)->markerShape());

        this->connect(d_selectMarkerShapeWidget, SIGNAL(currentIndexChanged(int)), this, SLOT(setMarkerShape(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(markerTreeWidgetItem, {"Shape"}), 1, d_selectMarkerShapeWidget);


        d_selectMarkerSizeWidget = new QDoubleSpinBox;

        d_selectMarkerSizeWidget->setRange(0, 1000.0);

        d_selectMarkerSizeWidget->setDecimals(2);

        d_selectMarkerSizeWidget->setSingleStep(0.01);

        d_selectMarkerSizeWidget->setValue(static_cast<QScatterSeries *>(d_series)->markerSize());

        this->connect(d_selectMarkerSizeWidget, SIGNAL(valueChanged(double)), this, SLOT(setMarkerSize(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(markerTreeWidgetItem, {"Width"}), 1, d_selectMarkerSizeWidget);

    }


    d_selectOpacityWidget = new QDoubleSpinBox;

    d_selectOpacityWidget->setRange(0, 1.0);

    d_selectOpacityWidget->setDecimals(2);

    d_selectOpacityWidget->setSingleStep(0.01);

    d_selectOpacityWidget->setValue(d_series->opacity());

    this->connect(d_selectOpacityWidget, SIGNAL(valueChanged(double)), this, SLOT(setOpacity(double)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Opacity"}), 1, d_selectOpacityWidget);


    if ((  d_series->type() == QAbstractSeries::SeriesTypeLine)
       || (d_series->type() == QAbstractSeries::SeriesTypeScatter)
       || (d_series->type() == QAbstractSeries::SeriesTypeSpline)) {


        d_setPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(this, "Pen", static_cast<QXYSeries *>(d_series)->pen());

        this->connect(d_setPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setPen(QPen)));

    }

    if (d_series->type() == QAbstractSeries::SeriesTypeBoxPlot) {

        d_setPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(this, "Pen", static_cast<QBoxPlotSeries *>(d_series)->pen());

        this->connect(d_setPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setPen(QPen)));
    }

    if (d_series->type() == QAbstractSeries::SeriesTypePie) {


        QTreeWidgetItem *AngleTreeWidgetItem = new QTreeWidgetItem(this, {"Pie angle"});


        d_selectPieEndAngleWidget = new QDoubleSpinBox;

        d_selectPieEndAngleWidget->setRange(0, 360);

        d_selectPieEndAngleWidget->setDecimals(2);

        d_selectPieEndAngleWidget->setSingleStep(0.01);

        d_selectPieEndAngleWidget->setValue(static_cast<QPieSeries *>(d_series)->pieEndAngle());

        this->connect(d_selectPieEndAngleWidget, SIGNAL(valueChanged(double)), this, SLOT(setPieEndAngle(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(AngleTreeWidgetItem, {"End"}), 1, d_selectPieEndAngleWidget);


        d_selectPieStartAngleWidget = new QDoubleSpinBox;

        d_selectPieStartAngleWidget->setRange(0, 360);

        d_selectPieStartAngleWidget->setDecimals(2);

        d_selectPieStartAngleWidget->setSingleStep(0.01);

        d_selectPieStartAngleWidget->setValue(static_cast<QPieSeries *>(d_series)->pieStartAngle());

        this->connect(d_selectPieStartAngleWidget, SIGNAL(valueChanged(double)), this, SLOT(setPieStartAngle(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(AngleTreeWidgetItem, {"Start"}), 1, d_selectPieStartAngleWidget);


        d_selectHoleSizeWidget = new QDoubleSpinBox;

        d_selectHoleSizeWidget->setRange(0, 1.0);

        d_selectHoleSizeWidget->setDecimals(2);

        d_selectHoleSizeWidget->setSingleStep(0.01);

        d_selectHoleSizeWidget->setValue(static_cast<QPieSeries *>(d_series)->holeSize());

        this->connect(d_selectHoleSizeWidget, SIGNAL(valueChanged(double)), this, SLOT(setHoleSize(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Pie hole size"}), 1, d_selectHoleSizeWidget);


        QTreeWidgetItem *piePositionTreeWidgetItem = new QTreeWidgetItem(this, {"Pie position"});


        d_selectHorizontalPositionWidget = new QDoubleSpinBox;

        d_selectHorizontalPositionWidget->setRange(0, 1);

        d_selectHorizontalPositionWidget->setDecimals(2);

        d_selectHorizontalPositionWidget->setSingleStep(0.01);

        d_selectHorizontalPositionWidget->setValue(static_cast<QPieSeries *>(d_series)->horizontalPosition());

        this->connect(d_selectHorizontalPositionWidget, SIGNAL(valueChanged(double)), this, SLOT(setHorizontalPosition(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(piePositionTreeWidgetItem, {"Horizontal"}), 1, d_selectHorizontalPositionWidget);


        d_selectVerticalPositionWidget = new QDoubleSpinBox;

        d_selectVerticalPositionWidget->setRange(0, 1);

        d_selectVerticalPositionWidget->setDecimals(2);

        d_selectVerticalPositionWidget->setSingleStep(0.01);

        d_selectVerticalPositionWidget->setValue(static_cast<QPieSeries *>(d_series)->verticalPosition());

        this->connect(d_selectVerticalPositionWidget, SIGNAL(valueChanged(double)), this, SLOT(setVerticalPosition(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(piePositionTreeWidgetItem, {"Vertical"}), 1, d_selectVerticalPositionWidget);


        d_selectPieSizeWidget = new QDoubleSpinBox;

        d_selectPieSizeWidget->setRange(0, 1.0);

        d_selectPieSizeWidget->setDecimals(2);

        d_selectPieSizeWidget->setSingleStep(0.01);

        d_selectPieSizeWidget->setValue(static_cast<QPieSeries *>(d_series)->pieSize());

        this->connect(d_selectPieSizeWidget, SIGNAL(valueChanged(double)), this, SLOT(setPieSize(double)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Pie size"}), 1, d_selectPieSizeWidget);

        new SetPieSliceParametersWidget(this, "Settings for all pie slices in this series", static_cast<QPieSeries *>(d_series)->slices());

        QTreeWidgetItem *pieSlicesTreeWidgetItem = new QTreeWidgetItem(this, {"Pie slices"});

        for (int i = 0; i < static_cast<QPieSeries *>(d_series)->slices().size(); ++i)
            new SetPieSliceParametersWidget(pieSlicesTreeWidgetItem, "Pie slice " + QString::number(i + 1), {static_cast<QPieSeries *>(d_series)->slices().at(i)});

    }

    if ((  d_series->type() == QAbstractSeries::SeriesTypeLine)
       || (d_series->type() == QAbstractSeries::SeriesTypeScatter)
       || (d_series->type() == QAbstractSeries::SeriesTypeSpline)) {

        QTreeWidgetItem *pointLabelsTreeWidgetItem = new QTreeWidgetItem(this, {"Point labels"});


        d_selectPointLabelsClippingWidget = new QCheckBox;

        d_selectPointLabelsClippingWidget->setChecked(static_cast<QXYSeries *>(d_series)->pointLabelsClipping());

        this->connect(d_selectPointLabelsClippingWidget, SIGNAL(toggled(bool)), this, SLOT(setPointLabelsClipping(bool)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(pointLabelsTreeWidgetItem, {"Clipping"}), 1, d_selectPointLabelsClippingWidget);


        d_selectPointLabelsColorWidget = new SelectColorWidget(nullptr, static_cast<QXYSeries *>(d_series)->pointLabelsColor());

        this->connect(d_selectPointLabelsColorWidget, SIGNAL(colorSelected(QColor)), this, SLOT(setPointLabelsColor(QColor)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(pointLabelsTreeWidgetItem, {"Color"}), 1, d_selectPointLabelsColorWidget);


        d_selectPointLabelsFontWidget = new SelectFontWidget(nullptr, static_cast<QXYSeries *>(d_series)->pointLabelsFont());

        this->connect(d_selectPointLabelsFontWidget, SIGNAL(currentFontChanged(QFont)), this, SLOT(setPointLabelsFont(QFont)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(pointLabelsTreeWidgetItem, {"Font"}), 1, d_selectPointLabelsFontWidget);


        d_selectPointLabelsFormatWidget = new QLineEdit(static_cast<QXYSeries *>(d_series)->pointLabelsFormat());

        this->connect(d_selectPointLabelsFormatWidget, SIGNAL(textEdited(QString)), this, SLOT(setPointLabelsFormat(QString)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(pointLabelsTreeWidgetItem, {"Format"}), 1, d_selectPointLabelsFormatWidget);


        d_selectPointLabelsVisibleWidget = new QCheckBox;

        d_selectPointLabelsVisibleWidget->setChecked(static_cast<QXYSeries *>(d_series)->pointLabelsVisible());

        this->connect(d_selectPointLabelsVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setPointLabelsVisible(bool)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(pointLabelsTreeWidgetItem, {"Visible"}), 1, d_selectPointLabelsVisibleWidget);

    }

}

void SetSeriesParametersTreeWidgetItem::setOpacity(const double &opacity)
{

    d_series->setOpacity(opacity);

}

void SetSeriesParametersTreeWidgetItem::setBarWidth(const double &width)
{

    static_cast<QAbstractBarSeries *>(d_series)->setBarWidth(width);

}

void SetSeriesParametersTreeWidgetItem::setLabelsAngle(int angle)
{

    static_cast<QAbstractBarSeries *>(d_series)->setLabelsAngle(angle);

}

void SetSeriesParametersTreeWidgetItem::setLabelsFormat(const QString &format)
{

    static_cast<QAbstractBarSeries *>(d_series)->setLabelsFormat(format);

}

void SetSeriesParametersTreeWidgetItem::setLabelsPosition(int index)
{

    static_cast<QAbstractBarSeries *>(d_series)->setLabelsPosition(static_cast<QAbstractBarSeries::LabelsPosition>(index));

}

void SetSeriesParametersTreeWidgetItem::setLabelsVisible(bool visible)
{

    static_cast<QAbstractBarSeries *>(d_series)->setLabelsVisible(visible);

}

void SetSeriesParametersTreeWidgetItem::setName(const QString &name)
{

    d_series->setName(name);

}

void SetSeriesParametersTreeWidgetItem::setBrush(const QBrush &brush)
{

    switch (d_series->type()) {

        case QAbstractSeries::SeriesTypeLine: static_cast<QXYSeries *>(d_series)->setBrush(brush); break;

        case QAbstractSeries::SeriesTypeScatter: static_cast<QXYSeries *>(d_series)->setBrush(brush); break;

        case QAbstractSeries::SeriesTypeSpline: static_cast<QXYSeries *>(d_series)->setBrush(brush); break;

        case QAbstractSeries::SeriesTypeBoxPlot: static_cast<QBoxPlotSeries *>(d_series)->setBrush(brush); break;

        default: break;

    }

}

void SetSeriesParametersTreeWidgetItem::setPen(const QPen &pen)
{

    switch (d_series->type()) {

        case QAbstractSeries::SeriesTypeLine: static_cast<QXYSeries *>(d_series)->setPen(pen); break;

        case QAbstractSeries::SeriesTypeScatter: static_cast<QXYSeries *>(d_series)->setPen(pen); break;

        case QAbstractSeries::SeriesTypeSpline: static_cast<QXYSeries *>(d_series)->setPen(pen); break;

        case QAbstractSeries::SeriesTypeBoxPlot: static_cast<QBoxPlotSeries *>(d_series)->setPen(pen); break;

        default: break;

    }

}

void SetSeriesParametersTreeWidgetItem::setPointLabelsClipping(bool enabled)
{

    static_cast<QXYSeries *>(d_series)->setPointLabelsClipping(enabled);

}

void SetSeriesParametersTreeWidgetItem::setPointLabelsColor(const QColor &color)
{

    static_cast<QXYSeries *>(d_series)->setPointLabelsColor(color);

}

void SetSeriesParametersTreeWidgetItem::setPointLabelsFont(const QFont &font)
{

    static_cast<QXYSeries *>(d_series)->setPointLabelsFont(font);

}

void SetSeriesParametersTreeWidgetItem::setPointLabelsFormat(const QString &format)
{

    static_cast<QXYSeries *>(d_series)->setPointLabelsFormat(format);

}

void SetSeriesParametersTreeWidgetItem::setPointLabelsVisible(bool visible)
{

    static_cast<QXYSeries *>(d_series)->setPointLabelsVisible(visible);

}

void SetSeriesParametersTreeWidgetItem::setMarkerShape(int shape)
{

    static_cast<QScatterSeries *>(d_series)->setMarkerShape(static_cast<QScatterSeries::MarkerShape>(shape));

}

void SetSeriesParametersTreeWidgetItem::setMarkerSize(const double &size)
{

    static_cast<QScatterSeries *>(d_series)->setMarkerSize(size);

}

void SetSeriesParametersTreeWidgetItem::setBoxOutlineVisible(bool visible)
{

    static_cast<QBoxPlotSeries *>(d_series)->setBoxOutlineVisible(visible);

}

void SetSeriesParametersTreeWidgetItem::setBoxWidth(const double &width)
{

    static_cast<QBoxPlotSeries *>(d_series)->setBoxWidth(width);

}

void SetSeriesParametersTreeWidgetItem::setHoleSize(const double &holeSize)
{

    static_cast<QPieSeries *>(d_series)->setHoleSize(holeSize);

}

void SetSeriesParametersTreeWidgetItem::setHorizontalPosition(const double &relativePosition)
{

    static_cast<QPieSeries *>(d_series)->setHorizontalPosition(relativePosition);

}

void SetSeriesParametersTreeWidgetItem::setPieEndAngle(const double &angle)
{

    static_cast<QPieSeries *>(d_series)->setPieEndAngle(angle);

}

void SetSeriesParametersTreeWidgetItem::setPieSize(const double &relativeSize)
{

    static_cast<QPieSeries *>(d_series)->setPieSize(relativeSize);

}

void SetSeriesParametersTreeWidgetItem::setPieStartAngle(const double &startAngle)
{

    static_cast<QPieSeries *>(d_series)->setPieStartAngle(startAngle);

}

void SetSeriesParametersTreeWidgetItem::setVerticalPosition(const double &relativePosition)
{

    static_cast<QPieSeries *>(d_series)->setVerticalPosition(relativePosition);

}

void SetSeriesParametersTreeWidgetItem::setSeries(const QString &label, QAbstractSeries *series)
{

    d_series = series;

    if (!d_initialized)
        this->initialize(label);
    else {

        qDeleteAll(this->takeChildren());

        this->initialize(label);

    }

    d_initialized = true;

}
