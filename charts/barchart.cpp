#include "barchart.h"

BarChart::BarChart(QObject *parent, const QString &title, const QStringList &barCategoryLabels, const QStringList &barSetLabels, const QVector<QVector<double> > &values, QAbstractSeries::SeriesType seriesType) :
    BaseChart(parent)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QStringList,QVector<QVector<double> >,QAbstractSeries::SeriesType)), this, SLOT(_initializeChart(QString,QStringList,QStringList,QVector<QVector<double> >,QAbstractSeries::SeriesType)));

    emit initializeChart(title, barCategoryLabels, barSetLabels, values, seriesType);

    d_eventLoop.exec();

}

BarChart::BarChart(QObject *parent, const QString &title, const QStringList &barCategoryLabels, const QString &barSetLabel, const QVector<double> &values, QAbstractSeries::SeriesType seriesType) :
    BaseChart(parent)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QStringList,QVector<QVector<double> >,QAbstractSeries::SeriesType)), this, SLOT(_initializeChart(QString,QStringList,QStringList,QVector<QVector<double> >,QAbstractSeries::SeriesType)));

    emit initializeChart(title, barCategoryLabels, QStringList() << barSetLabel, QVector<QVector<double> >() << values, seriesType);

    d_eventLoop.exec();

}

void BarChart::_initializeChart(const QString &title, const QStringList &barCategoryLabels, const QStringList &barSetLabels, const QVector<QVector<double> > &values, QAbstractSeries::SeriesType seriesType)
{

    d_chart = QSharedPointer<QChart>(new QChart);

    if (barSetLabels.size() == 1)
        d_chart->legend()->setVisible(false);

    QAbstractBarSeries *series;

    switch (seriesType) {

        case QAbstractSeries::SeriesTypeBar: series = new QBarSeries; break;

        case QAbstractSeries::SeriesTypeStackedBar: series = new QStackedBarSeries; break;

        case QAbstractSeries::SeriesTypePercentBar: series = new QPercentBarSeries; break;

        case QAbstractSeries::SeriesTypeHorizontalBar: series = new QHorizontalBarSeries; break;

        case QAbstractSeries::SeriesTypeHorizontalStackedBar: series = new QHorizontalStackedBarSeries; break;

        case QAbstractSeries::SeriesTypeHorizontalPercentBar: series = new QHorizontalPercentBarSeries; break;

        default: series = new QBarSeries; break;

    }

    for (int i = 0; i < barSetLabels.size(); ++i) {

        QBarSet *set = new QBarSet(barSetLabels.at(i));

        for (const double &value: values.at(i))
            *set << value;

        series->append(set);

    }

    d_chart->addSeries(series);

    d_chart->createDefaultAxes();

    QBarCategoryAxis *barCategoryAxis = new QBarCategoryAxis();

    barCategoryAxis->append(barCategoryLabels);

    switch (seriesType) {

        case QAbstractSeries::SeriesTypeBar: d_chart->addAxis(barCategoryAxis, Qt::AlignBottom); break;

        case QAbstractSeries::SeriesTypeStackedBar: d_chart->addAxis(barCategoryAxis, Qt::AlignBottom); break;

        case QAbstractSeries::SeriesTypePercentBar: d_chart->addAxis(barCategoryAxis, Qt::AlignBottom); break;

        case QAbstractSeries::SeriesTypeHorizontalBar: d_chart->addAxis(barCategoryAxis, Qt::AlignLeft); break;

        case QAbstractSeries::SeriesTypeHorizontalStackedBar: d_chart->addAxis(barCategoryAxis, Qt::AlignLeft); break;

        case QAbstractSeries::SeriesTypeHorizontalPercentBar: d_chart->addAxis(barCategoryAxis, Qt::AlignLeft); break;

        default: break;

    }


    d_chart->setTitle(title);

    emit finishedInitializationOfChart();

}
