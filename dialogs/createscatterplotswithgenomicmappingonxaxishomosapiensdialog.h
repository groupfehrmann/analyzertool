#ifndef CREATESCATTERPLOTSWITHGENOMICMAPPINGONXAXISHOMOSAPIENSDIALOG_H
#define CREATESCATTERPLOTSWITHGENOMICMAPPINGONXAXISHOMOSAPIENSDIALOG_H

#include <QString>
#include <QSharedPointer>

#include <algorithm>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/plotexportparametersbuildingblockwidget.h"
#include "widgets/selectfilebuildingblockwidget.h"
#include "widgets/lineeditbuildingblockwidget.h"
#include "workerclasses/createscatterplotswithgenomicmappingonxaxishomosapiensworkerclass.h"

using namespace QtCharts;

class CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog : public BaseDialog
{

    Q_OBJECT

public:

    CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningChromosomeMapping;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningBasepairMapping;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_subsetIdentifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_chartType;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_plotMode;

    PlotExportParametersBuildingBlockWidget *d_plotExportParametersBuildingBlockWidget;

    LineEditBuildingBlockWidget *d_lineEditBuildingBlockWidget_prefixStringToUseInPlotName;

    SelectFileBuildingBlockWidget *d_selectFileBuildingBlockWidget_templatePlot;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void annotationLabelDefiningSubsetChanged(const QString &currentAnnotationLabel);

    void plotModeChanged(const QString &plotMode);

};

#endif // CREATESCATTERPLOTSWITHGENOMICMAPPINGONXAXISHOMOSAPIENSDIALOG_H
