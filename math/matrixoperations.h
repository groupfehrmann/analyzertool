#ifndef MATRIXOPERATIONS_H
#define MATRIXOPERATIONS_H

#include <QVector>
#include <QtConcurrent>
#include <QFutureSynchronizer>
#include <QPair>
#include <QThread>
#include <QMutex>
#include <QSemaphore>
#include <QReadWriteLock>

#include <functional>
#include <utility>
#include <random>
#include <chrono>
#include <vector>

#include "math/vectoroperations.h"
#include "math/functionsformultithreading.h"

#ifdef Q_OS_MAC
#include <Accelerate/Accelerate.h>
#endif

#ifdef Q_OS_LINUX
//#include <lapacke.h>
#include <mkl/mkl_lapacke.h>
#endif

namespace MatrixOperations {

    QVector<double> columnVector(const QVector<QVector<double> > &matrix, int columnIndex);

    template <typename T>
    QVector<QVector<T> > &transpose_inplace(QVector<QVector<T> > &matrix)
    {

        int numberOfRows = matrix.size();

        int numberOfColumns = matrix.isEmpty() ? 0 : matrix.first().size();

        if (matrix.isEmpty() || ((numberOfRows == 1) && (numberOfColumns == 1)))
            return matrix;

        if (numberOfRows == numberOfColumns) {

            for (int i = 0; i < numberOfRows; ++i) {

                QVector<T> &currentRowVector(matrix[i]);

                for (int j = i + 1; j < numberOfColumns; ++j)
                    std::swap(currentRowVector[j], matrix[j][i]);

            }

        } else if (numberOfRows < numberOfColumns) {

            matrix << QVector<QVector<T> >(numberOfColumns - numberOfRows, QVector<T>(numberOfRows));

            for (int i = 0; i < numberOfRows; ++i) {

                QVector<T> &currentRowVector(matrix[i]);

                for (int j = i + 1; j < numberOfRows; ++j)
                    std::swap(currentRowVector[j], matrix[j][i]);

                for (int j = numberOfRows; j < numberOfColumns; ++j)
                    matrix[j][i] = currentRowVector.at(j);

                currentRowVector.resize(numberOfRows);

            }

        } else {

            for (int i = 0; i < numberOfColumns; ++i) {

                QVector<T> &currentRowVector(matrix[i]);

                for (int j = i + 1; j < numberOfColumns; ++j)
                    std::swap(currentRowVector[j], matrix[j][i]);

                currentRowVector.resize(numberOfRows);

            }

            for (int i = numberOfColumns; i < numberOfRows; ++i) {

                QVector<T> &currentRowVector(matrix[i]);

                for (int j = 0; j < numberOfColumns; ++j)
                    matrix[j][i] = currentRowVector.at(j);

            }

            matrix.resize(numberOfColumns);

        }

        return matrix;

    }

    template <typename T>
    QVector<QVector<T> > transpose(const QVector<QVector<T> > &matrix)
    {

        QVector<QVector<double> > result;

        result = matrix;

        return MatrixOperations::transpose_inplace(result);

    }

    template <typename T>
    QVector<QVector<T> > &transpose_inplace_multiThreaded(QVector<QVector<T> > &matrix)
    {

        int rowCount = matrix.size();

        int columnCount = matrix.isEmpty() ? 0 : matrix.first().size();

        if (matrix.isEmpty() || ((rowCount == 1) && (columnCount == 1)))
            return matrix;

        if (rowCount == columnCount) {

            FunctionsForMultiThreading::detachMatrix(matrix);

            auto processor = [&](const QVector<int> &indexesToProcess) {

                for (const int &indexToProcess : indexesToProcess) {

                    QVector<T> &currentRowVector(matrix[indexToProcess]);

                    for (int j = indexToProcess + 1; j < currentRowVector.size(); ++j)
                        std::swap(matrix[j][indexToProcess], currentRowVector[j]);

                }

            };

            QFutureSynchronizer<void> futureSynchronizer;

            for (const QVector<int> &indexesToProcess : FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(rowCount, QThreadPool::globalInstance()->maxThreadCount()))
                futureSynchronizer.addFuture(QtConcurrent::run(processor, indexesToProcess));

            futureSynchronizer.waitForFinished();

            return matrix;

        } else if (rowCount < columnCount) {

            for (int i = 0; i < (columnCount - rowCount); ++i)
                matrix.append(QVector<T>(rowCount));

            FunctionsForMultiThreading::detachMatrix(matrix);

            auto processor = [&](const QVector<int> &indexesToProcess) {

                for (const int &indexToProcess : indexesToProcess) {

                    QVector<T> &currentRowVector(matrix[indexToProcess]);

                    for (int j = indexToProcess + 1; j < rowCount; ++j)
                        std::swap(currentRowVector[j], matrix[j][indexToProcess]);

                    for (int j = rowCount; j < columnCount; ++j)
                        matrix[j][indexToProcess] = currentRowVector.at(j);

                }

            };

            QFutureSynchronizer<void> futureSynchronizer;

            for (const QVector<int> &indexesToProcess : FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(rowCount, QThreadPool::globalInstance()->maxThreadCount()))
                futureSynchronizer.addFuture(QtConcurrent::run(processor, indexesToProcess));

            futureSynchronizer.waitForFinished();

            for (int i = 0; i < rowCount; ++i)
                matrix[i].resize(rowCount);

            return matrix;

        } else {

            for (int i = 0; i < columnCount; ++i)
                matrix[i].resize(rowCount);

            FunctionsForMultiThreading::detachMatrix(matrix);

            auto processor = [&](const QVector<int> &indexesToProcess) {

                for (const int &indexToProcess : indexesToProcess) {

                    QVector<T> &currentRowVector(matrix[indexToProcess]);

                    if (indexToProcess < columnCount) {

                        for (int j = indexToProcess + 1; j < columnCount; ++j)
                            std::swap(currentRowVector[j], matrix[j][indexToProcess]);

                    } else {

                        for (int j = 0; j < columnCount; ++j)
                            matrix[j][indexToProcess] = currentRowVector.at(j);

                    }

                }

            };

            QFutureSynchronizer<void> futureSynchronizer;

            for (const QVector<int> &indexesToProcess : FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(rowCount, QThreadPool::globalInstance()->maxThreadCount()))
                futureSynchronizer.addFuture(QtConcurrent::run(processor, indexesToProcess));

            futureSynchronizer.waitForFinished();

            matrix.resize(columnCount);

            return matrix;

        }

    }

    template <typename T>
    QVector<QVector<T> > transpose_multiThreaded(const QVector<QVector<T> > &matrix)
    {

        QVector<QVector<T> > result;

        for (int i = 0; i < (matrix.isEmpty() ? 0 : matrix.first().size()); ++i)
            result << QVector<double>(matrix.size());

        auto processor = [&](const QPair<int, int> &block) {

            for (int i = block.first; i < block.second; ++i) {

                const QVector<T> &currentRowVector(matrix.at(i));

                for (int j = 0; j < currentRowVector.size(); ++j)
                    result[j][i] = currentRowVector.at(j);

            }

        };

        QFutureSynchronizer<void> futureSynchronizer;

        for (const QPair<int, int> &block : FunctionsForMultiThreading::createIndexBlocksForMultiThreading(matrix.size(), QThreadPool::globalInstance()->maxThreadCount()))
            futureSynchronizer.addFuture(QtConcurrent::run(processor, block));

        futureSynchronizer.waitForFinished();

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        QVector<QVector<double> > result(matrix.size());

        for (int i = 0; i < matrix.size(); ++i)
            result[i] = VectorOperations::applyFunctor_elementWise(matrix.at(i), functor);

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise_multiThreaded(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        auto mapFunctor = [&functor](const QVector<double> &vector) {

            return VectorOperations::applyFunctor_elementWise(vector, functor);

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix, std::function<QVector<double>(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        for (QVector<double> &vector : matrix)
            VectorOperations::applyFunctor_elementWise_inplace(vector, functor);

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace_multiThreaded(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        auto mapFunctor = [&functor](QVector<double> &vector) {

            VectorOperations::applyFunctor_elementWise_inplace(vector, functor);

        };

        QtConcurrent::blockingMap(matrix, mapFunctor);

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise_horizontal(const QVector<QVector<double> > &matrix, const QVector<double> &rowVector, const Functor &functor)
    {

        QVector<QVector<double> > result;

        result.reserve(matrix.size());

        for (const QVector<double> &vector : matrix)
            result << VectorOperations::applyFunctor_elementWise(vector, rowVector, functor);

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise_multiThreaded_horizontal(const QVector<QVector<double> > &matrix, const QVector<double> &rowVector, const Functor &functor)
    {

        auto mapFunctor = [&rowVector, &functor](const QVector<double> &vector) {

            return VectorOperations::applyFunctor_elementWise(vector, rowVector, functor);

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix, std::function<QVector<double>(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace_horizontal(QVector<QVector<double> > &matrix, const QVector<double> &rowVector, const Functor &functor)
    {

        for (QVector<double> &vector : matrix)
            VectorOperations::applyFunctor_elementWise_inplace(vector, rowVector, functor);

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace_multiThreaded_horizontal(QVector<QVector<double> > &matrix, const QVector<double> &rowVector, const Functor &functor)
    {

        auto mapFunctor = [&rowVector, &functor](QVector<double> &vector) {

            VectorOperations::applyFunctor_elementWise_inplace(vector, rowVector, functor);

        };

        QtConcurrent::blockingMap(matrix, mapFunctor);

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise_vertical(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor &functor)
    {

        QVector<QVector<double> > result(matrix.size());

        for (int i = 0; i < matrix.size(); ++i) {

            double temp = columnVector.at(i);

            auto functorWrapper = [&functor, &temp](const double &value) {

                return functor(value, temp);

            };

            result[i] = VectorOperations::applyFunctor_elementWise(matrix.at(i), functorWrapper);

        }

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise_multiThreaded_vertical(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor &functor)
    {

        auto mapFunctor = [&columnVector, &functor, &matrix](const int &index) {

            double temp = columnVector.at(index);

            auto functorWrapper = [&functor, &temp](const double &value) {

                return functor(value, temp);

            };

            return VectorOperations::applyFunctor_elementWise(matrix.at(index), functorWrapper);

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace_vertical(QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor &functor)
    {

        for (int i = 0; i < matrix.size(); ++i) {

            double temp = columnVector.at(i);

            auto functorWrapper = [&functor, &temp](double &value) {

                functor(value, temp);

            };

            for (double &value : matrix[i])
                functorWrapper(value);

        }

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace_multiThreaded_vertical(QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor &functor)
    {

        FunctionsForMultiThreading::detachMatrix(matrix);

        auto mapFunctor = [&columnVector, &functor, &matrix](int &index) {

            double temp = columnVector.at(index);

            auto functorWrapper = [&functor, &temp](double &value) {

                functor(value, temp);

            };

            VectorOperations::applyFunctor_elementWise_inplace(matrix[index], functorWrapper);

        };

        QtConcurrent::blockingMap(FunctionsForMultiThreading::createSequenceOfIndexes(matrix.size(), 0, 1), mapFunctor);

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        QVector<QVector<double> > result;

        result.reserve(matrix1.size());

        for (int i = 0; i < matrix1.size(); ++i)
            result << VectorOperations::applyFunctor_elementWise(matrix1.at(i), matrix2.at(i), functor);

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_elementWise_multiThreaded(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        auto mapFunctor = [&matrix1, &matrix2, &functor](const int &index) {

            return VectorOperations::applyFunctor_elementWise(matrix1.at(index), matrix2.at(index), functor);

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix1.size(), 0, 1), std::function<QVector<double>(const int &index)>(mapFunctor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        for (int i = 0; i < matrix1.size(); ++i)
            VectorOperations::applyFunctor_elementWise_inplace(matrix1[i], matrix2.at(i), functor);

        return matrix1;

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_elementWise_inplace_multiThreaded(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        FunctionsForMultiThreading::detachMatrix(matrix1);

        auto mapFunctor = [&matrix1, &matrix2, &functor](int &index) {

            VectorOperations::applyFunctor_elementWise_inplace(matrix1[index], matrix2.at(index), functor);

        };

        QVector<int> sequenceOfIndex = FunctionsForMultiThreading::createSequenceOfIndexes(matrix1.size(), 0, 1);

        QtConcurrent::blockingMap(sequenceOfIndex, mapFunctor);

        return matrix1;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_horizontal(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        QVector<QVector<double> > result(matrix.size());

        for (int i = 0; i < matrix.size(); ++i)
            result[i] << functor(matrix.at(i));

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_multiThreaded_horizontal(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix, std::function<QVector<double>(const QVector<double> &)>(functor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_vectorWise_inplace_horizontal(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        for (QVector<double> &vector : matrix)
            functor(vector);

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_vectorWise_inplace_multiThreaded_horizontal(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        QtConcurrent::blockingMap<QVector<QVector<double> > >(matrix, functor);

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_vertical(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        QVector<QVector<double> > result(matrix.isEmpty() ? 0 : matrix.first().size());

        for (int i = 0; i < result.size(); ++i)
            result[i] = functor(MatrixOperations::columnVector(matrix, i));

        return MatrixOperations::transpose_inplace(result);

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_multiThreaded_vertical(QVector<QVector<double> > matrix, const Functor &functor)
    {

        auto mapFunctor = [&matrix, &functor](const int &index) {

            return functor(MatrixOperations::columnVector(matrix, index));

        };

        QVector<QVector<double> > temp = QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix.size(), 0, 1), std::function<QVector<double>(const int &index)>(mapFunctor));

        return MatrixOperations::transpose_inplace_multiThreaded(temp);

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_vectorWise_inplace_vertical(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        return MatrixOperations::transpose_inplace(MatrixOperations::applyFunctor_vectorWise_inplace_horizontal(MatrixOperations::transpose_inplace(matrix), functor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_vectorWise_inplace_multiThreaded_vertical(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        return MatrixOperations::transpose_inplace_multiThreaded(MatrixOperations::applyFunctor_vectorWise_inplace_multiThreaded_horizontal(MatrixOperations::transpose_inplace_multiThreaded(matrix), functor));

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_horizontal(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        QVector<QVector<double> > result(matrix1.size());

        for (int i = 0; i < matrix1.size(); ++i)
            result[i] = functor(matrix1.at(i), matrix2.at(i));

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_multiThreaded_horizontal(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        auto mapFunctor = [&functor, &matrix1, &matrix2](const int &index) {

            return functor(matrix1.at(index), matrix2.at(index));

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix1.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_vectorWise_inplace_horizontal(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        for (int i = 0; i < matrix1.size(); ++i)
            functor(matrix1[i], matrix2.at(i));

        return matrix1;

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_vectorWise_inplace_multiThreaded_horizontal(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        FunctionsForMultiThreading::detachMatrix(matrix1);

        auto mapFunctor = [&functor, &matrix1, &matrix2](int &index) {

            functor(matrix1[index], matrix2.at(index));

        };

        QtConcurrent::blockingMap(FunctionsForMultiThreading::createSequenceOfIndexes(matrix1.size(), 0, 1), mapFunctor);

        return matrix1;

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_vertical(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        QVector<QVector<double> > result(matrix1.isEmpty() ? 0 : matrix1.first().size());;

        for (int i = 0; i < result.size(); ++i)
            result[i] = functor(MatrixOperations::columnVector(matrix1, i), MatrixOperations::columnVector(matrix2, i));

        return MatrixOperations::transpose_inplace(result);

    }

    template <typename Functor>
    QVector<QVector<double> > applyFunctor_vectorWise_multiThreaded_vertical(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        auto mapFunctor = [&functor, &matrix1, &matrix2](const int &index) {

            return functor(MatrixOperations::columnVector(matrix1, index), MatrixOperations::columnVector(matrix2, index));

        };

        QVector<QVector<double> > temp = QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix1.size(), 0, 1), std::function<QVector<double>(const int &index)>(mapFunctor));

        return MatrixOperations::transpose_inplace_multiThreaded(temp);

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunction_vectorWise_inplace_vertical(QVector<QVector<double> > &matrix1, QVector<QVector<double> > matrix2, const Functor &functor)
    {

        return MatrixOperations::transpose_inplace(MatrixOperations::applyFunctor_vectorWise_inplace_horizontal(MatrixOperations::transpose_inplace(matrix1), MatrixOperations::transpose_inplace(matrix2), functor));

    }

    template <typename Functor>
    QVector<QVector<double> > &applyFunctor_vectorWise_inplace_multiThreaded_vertical(QVector<QVector<double> > &matrix1, QVector<QVector<double> > matrix2, const Functor &functor)
    {

        return MatrixOperations::transpose_inplace_multiThreaded(MatrixOperations::applyFunctor_vectorWise_inplace_multiThreaded_horizontal(MatrixOperations::transpose_inplace_multiThreaded(matrix1), MatrixOperations::transpose_inplace_multiThreaded(matrix2), functor));

    }

    QVector<double> rowVector_x_matrix(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix);

    QVector<double> rowVector_x_matrix_multiThreaded(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix);

    QVector<double> &rowVector_x_matrix_inplace(QVector<double> &rowVector, const QVector<QVector<double> > &matrix);

    QVector<double> &rowVector_x_matrix_inplace_multiThreaded(QVector<double> &rowVector, const QVector<QVector<double> > &matrix);

    template <typename Functor>
    QVector<double> rowVector_x_matrix_applyFunctorToRowVectorElementWise(const Functor &functor, const QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
    {

        QVector<double> result(matrix.first().size(), 0.0);

        for (int i = 0; i < rowVector.size(); ++i) {

            double temp = functor(rowVector.at(i));

            const QVector<double> &currentRowVectorOfMatrix(matrix.at(i));

            for (int j = 0; j < currentRowVectorOfMatrix.size(); ++j)
                result[j] += currentRowVectorOfMatrix.at(j) * temp;

        }

        return result;

    }

    template <typename Functor>
    QVector<double> rowVector_x_matrix_applyFunctorToRowVectorElementWise_multiThreaded(const Functor &functor, const QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
    {

        auto mapFunctor = [&matrix, &functor, &rowVector](const int &index) {

            return VectorOperations::vsMul(matrix.at(index), functor(rowVector.at(index)));

        };

        auto reduceFunctor = [](QVector<double> &result, const QVector<double> &intermediateResult) {

            VectorOperations::vsMul_vsMul_vvAdd_inplace(result, 1.0, intermediateResult, 1.0);

        };

       return QtConcurrent::blockingMappedReduced<QVector<double> >(FunctionsForMultiThreading::createSequenceOfIndexes(rowVector.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor), std::function<void(QVector<double> &, const QVector<double> &)>(reduceFunctor));

    }

    template <typename Functor>
    QVector<double> &rowVector_x_matrix_applyFunctorToRowVectorElementWise_inplace(const Functor &functor, QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
    {

        rowVector = MatrixOperations::rowVector_x_matrix_applyFunctorToRowVectorElementWise(functor, rowVector, matrix);

        return rowVector;

    }

    template <typename Functor>
    QVector<double> &rowVector_x_matrix_applyFunctorToRowVectorElementWise_inplace_multiThreaded(const Functor &functor, QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
    {

        rowVector = MatrixOperations::rowVector_x_matrix_applyFunctorToRowVectorElementWise_multiThreaded(functor, rowVector, matrix);

        return rowVector;

    }

    template <typename Functor>
    QVector<double> rowVector_x_matrix_applyFunctorOnResultingVectorElementWise(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        QVector<double> result(matrix.first().size(), 0.0);

        for (int i = 0; i < rowVector.size(); ++i) {

            const double &temp(rowVector.at(i));

            const QVector<double> &currentRowVectorOfMatrix(matrix.at(i));

            for (int j = 0; j < currentRowVectorOfMatrix.size(); ++j)
                result[j] += currentRowVectorOfMatrix.at(j) * temp;

        }

        return VectorOperations::applyFunctor_elementWise_inplace(result, functor);

    }

    template <typename Functor>
    QVector<double> rowVector_x_matrix_applyFunctorOnResultingVectorElementWise_multiThreaded(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        auto mapFunctor = [&matrix, &rowVector](const int &index) {

            return VectorOperations::vsMul(matrix.at(index), rowVector.at(index));

        };

        auto reduceFunctor = [](QVector<double> &result, const QVector<double> &intermediateResult) {

            VectorOperations::vsMul_vsMul_vvAdd_inplace(result, 1.0, intermediateResult, 1.0);

        };

       return VectorOperations::applyFunctor_elementWise(QtConcurrent::blockingMappedReduced<QVector<double> >(FunctionsForMultiThreading::createSequenceOfIndexes(rowVector.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor), std::function<void(QVector<double> &, const QVector<double> &)>(reduceFunctor)), functor);

    }

    template <typename Functor>
    QVector<double> &rowVector_x_matrix_applyFunctorOnResultingVectorElementWise_inplace(QVector<double> &rowVector, const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        rowVector = MatrixOperations::rowVector_x_matrix_applyFunctorOnResultingVectorElementWise(rowVector, matrix, functor);

        return rowVector;

    }

    template <typename Functor>
    QVector<double> &rowVector_x_matrix_applyFunctorOnResultingVectorElementWise_inplace_multiThreaded(QVector<double> &rowVector, const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        rowVector = MatrixOperations::rowVector_x_matrix_applyFunctorOnResultingVectorElementWise_multiThreaded(rowVector, matrix, functor);

        return rowVector;

    }

    template <typename Functor1, typename Functor2>
    QVector<double> rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise(Functor1 functor1, const QVector<double> &rowVector, const QVector<QVector<double> > &matrix, Functor2 functor2)
    {

        QVector<double> result(matrix.first().size(), 0.0);

        for (int i = 0; i < rowVector.size(); ++i) {

            double temp = functor1(rowVector.at(i));

            const QVector<double> &currentRowVectorOfMatrix(matrix.at(i));

            for (int j = 0; j < currentRowVectorOfMatrix.size(); ++j)
                result[j] += currentRowVectorOfMatrix.at(j) * temp;

        }

        return VectorOperations::applyFunctor_elementWise_inplace(result, functor2);

    }

    template <typename Functor1, typename Functor2>
    QVector<double> rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise_multiThreaded(Functor1 functor1, const QVector<double> &rowVector, const QVector<QVector<double> > &matrix, Functor2 functor2)
    {

        auto mapFunctor = [&matrix, &functor1, &rowVector](const int &index) {

            return VectorOperations::vsMul(matrix.at(index), functor1(rowVector.at(index)));

        };

        auto reduceFunctor = [](QVector<double> &result, const QVector<double> &intermediateResult) {

            VectorOperations::vsMul_vsMul_vvAdd_inplace(result, 1.0, intermediateResult, 1.0);

        };

        return VectorOperations::applyFunctor_elementWise(QtConcurrent::blockingMappedReduced<QVector<double> >(FunctionsForMultiThreading::createSequenceOfIndexes(rowVector.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor), std::function<void(QVector<double> &, const QVector<double> &)>(reduceFunctor)), functor2);

    }

    template <typename Functor1, typename Functor2>
    QVector<double> &rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise_inplace(Functor1 functor1, QVector<double> &rowVector, const QVector<QVector<double> > &matrix, Functor2 functor2)
    {

        rowVector = MatrixOperations::rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise(functor1, rowVector, matrix, functor2);

        return rowVector;

    }

    template <typename Functor1, typename Functor2>
    QVector<double> &rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise_inplace_multiThreaded(Functor1 functor1, QVector<double> &rowVector, const QVector<QVector<double> > &matrix, Functor2 functor2)
    {

        rowVector = MatrixOperations::rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise_multiThreaded(functor1, rowVector, matrix, functor2);

        return rowVector;

    }

    QVector<double> matrix_x_columnVector(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector);

    QVector<double> matrix_x_columnVector_multiThreaded(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector);

    QVector<double> &matrix_x_columnVector_inplace(const QVector<QVector<double> > &matrix, QVector<double> &columnVector);

    QVector<double> &matrix_x_columnVector_inplace_multiThreaded(const QVector<QVector<double> > &matrix, QVector<double> &columnVector);

    template <typename Functor>
    QVector<double> matrix_x_columnVector_applyFunctorToMatrixElementWise(const Functor &functor, const QVector<QVector<double> > &matrix, const QVector<double> &columnVector)
    {

        QVector<double> result;

        result.reserve(matrix.size());

        for (const QVector<double> &currentRowVector : matrix) {

            double dotProduct = 0.0;

            for (int i = 0; i < currentRowVector.size(); ++i)
                dotProduct += functor(currentRowVector.at(i)) * columnVector.at(i);

            result << dotProduct;

        }

        return result;

    }

    template <typename Functor>
    QVector<double> matrix_x_columnVector_applyFunctorToMatrixElementWise_multiThreaded(const Functor &functor, const QVector<QVector<double> > &matrix, const QVector<double> &columnVector)
    {

        auto mapFunctor = [&functor, &columnVector](const QVector<double> &vector) {

            double dotProduct = 0.0;

            for (int i = 0; i < vector.size(); ++i)
                dotProduct += functor(vector.at(i)) * columnVector.at(i);

            return dotProduct;

        };

        return QtConcurrent::blockingMapped<QVector<double> >(matrix, std::function<double(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<double> &matrix_x_columnVector_applyFunctorToMatrixElementWise_inplace(const Functor &functor, const QVector<QVector<double> > &matrix, QVector<double> &columnVector)
    {

        columnVector = MatrixOperations::matrix_x_columnVector_applyFunctorToMatrixElementWise(functor, matrix, columnVector);

        return columnVector;

    }

    template <typename Functor>
    QVector<double> &matrix_x_columnVector_applyFunctorToMatrixElementWise_inplace_multiThreaded(const Functor &functor, const QVector<QVector<double> > &matrix, QVector<double> &columnVector)
    {

        columnVector = MatrixOperations::matrix_x_columnVector_applyFunctorToMatrixElementWise_multiThreaded(functor, matrix, columnVector);

        return columnVector;

    }

    template <typename Functor>
    QVector<double> matrix_x_columnVector_applyFunctorOnResultingVectorElementWise(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor &functor)
    {

        QVector<double> result;

        result.reserve(matrix.size());

        for (const QVector<double> &currentRowVector : matrix)
            result << functor(VectorOperations::dotProduct(currentRowVector, columnVector));

        return result;

    }

    template <typename Functor>
    QVector<double> matrix_x_columnVector_applyFunctorOnResultingVectorElementWise_multiThreaded(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor &functor)
    {

        auto mapFunctor = [&columnVector, &functor](const QVector<double> &vector) {

            double dotProduct = 0.0;

            for (int i = 0; i < vector.size(); ++i)
                dotProduct += vector.at(i) * columnVector.at(i);

            return functor(dotProduct);

        };

        return QtConcurrent::blockingMapped<QVector<double> >(matrix, std::function<double(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<double> &matrix_x_columnVector_applyFunctorOnResultingVectorElementWise_inplace(const QVector<QVector<double> > &matrix, QVector<double> &columnVector, const Functor &functor)
    {

        columnVector = MatrixOperations::matrix_x_columnVector_applyFunctorOnResultingVectorElementWise(matrix, columnVector, functor);

        return columnVector;

    }

    template <typename Functor>
    QVector<double> &matrix_x_columnVector_applyFunctorOnResultingVectorElementWise_inplace_multiThreaded(const QVector<QVector<double> > &matrix, QVector<double> &columnVector, const Functor &functor)
    {

        columnVector = MatrixOperations::matrix_x_columnVector_applyFunctorOnResultingVectorElementWise_multiThreaded(matrix, columnVector, functor);

        return columnVector;

    }

    template <typename Functor1, typename Functor2>
    QVector<double> matrix_x_columnVector_applyFunctorToMatrixElementWise_applyFunctor2OnResultingVectorElementWise(const Functor1 &functor1, const QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor2 &functor2)
    {

        QVector<double> result;

        result.reserve(matrix.size());

        for (const QVector<double> &currentRowVector : matrix) {

            double dotProduct = 0.0;

            for (int i = 0; i < currentRowVector.size(); ++i)
                dotProduct += functor1(currentRowVector.at(i)) * columnVector.at(i);

            result << functor2(dotProduct);

        }

        return result;

    }

    template <typename Functor1, typename Functor2>
    QVector<double> matrix_x_columnVector_applyFunctorToMatrixElementWise_applyFunctor2OnResultingVectorElementWise_multiThreaded(const Functor1 &functor1, const QVector<QVector<double> > &matrix, const QVector<double> &columnVector, const Functor2 &functor2)
    {

        auto mapFunctor = [&functor1, &columnVector, &functor2](const QVector<double> &vector) {

            double dotProduct = 0.0;

            for (int i = 0; i < vector.size(); ++i)
                dotProduct += functor1(vector.at(i)) * columnVector.at(i);

            return functor2(dotProduct);

        };

        return QtConcurrent::blockingMapped<QVector<double> >(matrix, std::function<double(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor1, typename Functor2>
    QVector<double> &matrix_x_columnVector_applyFunctorToMatrixElementWise_applyFunctor2OnResultingVectorElementWise_inplace(const Functor1 &functor1, const QVector<QVector<double> > &matrix, QVector<double> &columnVector, const Functor2 &functor2)
    {

        columnVector = MatrixOperations::matrix_x_columnVector_applyFunctorToMatrixElementWise_applyFunctor2OnResultingVectorElementWise(functor1, matrix, columnVector, functor2);

        return columnVector;

    }

    template <typename Functor1, typename Functor2>
    QVector<double> &matrix_x_columnVector_applyFunctorToMatrixElementWise_applyFunctor2OnResultingVectorElementWise_inplace_multiThreaded(const Functor1 &functor1, const QVector<QVector<double> > &matrix, QVector<double> &columnVector, const Functor2 &functor2)
    {

        columnVector = MatrixOperations::matrix_x_columnVector_applyFunctorToMatrixElementWise_applyFunctor2OnResultingVectorElementWise_multiThreaded(functor1, matrix, columnVector, functor2);

        return columnVector;

    }

    QVector<QVector<double> > matrix1_x_matrix2(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2);

    QVector<QVector<double> > matrix1_x_matrix2_multiThreaded(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2);

    QVector<QVector<double> > &matrix1_x_matrix2_inplace(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2);

    QVector<QVector<double> > &matrix1_x_matrix2_inplace_multiThreaded(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2);

    template <typename Functor>
    QVector<QVector<double> > matrix1_x_matrix2_applyFunctorToMatrix1ElementWise(const Functor &functor, const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
    {

        QVector<QVector<double> > result;

        result.reserve(matrix1.size());

        for (const QVector<double> &vector : matrix1)
            result << MatrixOperations::rowVector_x_matrix(VectorOperations::applyFunctor_elementWise(vector, functor), matrix2);

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded(const Functor &functor, const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
    {

        auto mapFunctor = [&functor, &matrix2](const QVector<double> &vector) {

            return MatrixOperations::rowVector_x_matrix(VectorOperations::applyFunctor_elementWise(vector, functor), matrix2);

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix1, std::function<QVector<double>(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<QVector<double> > &matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_inplace(const Functor &functor, QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
    {

        for (QVector<double> &currentRowVectorOfMatrix1 : matrix1)
            MatrixOperations::rowVector_x_matrix_applyFunctorToRowVectorElementWise_inplace(functor, currentRowVectorOfMatrix1, matrix2);

        return matrix1;

    }

    template <typename Functor>
    QVector<QVector<double> > &matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_inplace_multiThreaded(const Functor &functor, QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
    {

        auto mapFunctor = [&functor, &matrix2](QVector<double> &vector) {

            MatrixOperations::rowVector_x_matrix_applyFunctorToRowVectorElementWise_inplace(functor, vector, matrix2);

        };

        QtConcurrent::blockingMap(matrix1, mapFunctor);

        return matrix1;

    }

    template <typename Functor>
    QVector<QVector<double> > matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        QVector<QVector<double> > result;

        result.reserve(matrix1.size());

        for (const QVector<double> &vector : matrix1)
            result << VectorOperations::applyFunctor_elementWise(MatrixOperations::rowVector_x_matrix(vector, matrix2), functor);

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        auto mapFunctor = [&functor, &matrix2](const QVector<double> &vector) {

            return VectorOperations::applyFunctor_elementWise(MatrixOperations::rowVector_x_matrix(vector, matrix2), functor);

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix1, std::function<QVector<double>(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor>
    QVector<QVector<double> > &matrix1_x_matrix2_applyFunctorOnResultingMatrix_inplace(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        for (QVector<double> &currentRowVectorOfMatrix1 : matrix1)
            MatrixOperations::rowVector_x_matrix_applyFunctorOnResultingVectorElementWise_inplace(currentRowVectorOfMatrix1, matrix2, functor);

        return matrix1;

    }

    template <typename Functor>
    QVector<QVector<double> > &matrix1_x_matrix2_applyFunctorOnResultingMatrix_inplace_multiThreaded(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor)
    {

        auto mapFunctor = [&matrix2, &functor](QVector<double> &vector) {

            MatrixOperations::rowVector_x_matrix_applyFunctorOnResultingVectorElementWise_inplace(vector, matrix2, functor);

        };

        QtConcurrent::blockingMap(matrix1, mapFunctor);

        return matrix1;

    }

    template <typename Functor1, typename Functor2>
    QVector<QVector<double> > matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise(const Functor1 &functor1, const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor2 &functor2)
    {

        QVector<QVector<double> > result;

        result.reserve(matrix1.size());

        for (const QVector<double> &vector : matrix1)
            result << VectorOperations::applyFunctor_elementWise(MatrixOperations::rowVector_x_matrix(VectorOperations::applyFunctor_elementWise(vector, functor1), matrix2), functor2);

        return result;

    }

    template <typename Functor1, typename Functor2>
    QVector<QVector<double> > matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise_multiThreaded(const Functor1 &functor1, const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor2 &functor2)
    {

        auto mapFunctor = [&](const QVector<double> &vector) {

            return VectorOperations::applyFunctor_elementWise(MatrixOperations::rowVector_x_matrix(VectorOperations::applyFunctor_elementWise(vector, functor1), matrix2), functor2);

        };

        return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix1, std::function<QVector<double>(const QVector<double> &)>(mapFunctor));

    }

    template <typename Functor1, typename Functor2>
    QVector<QVector<double> > &matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise_inplace(const Functor1 &functor1, QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor2 &functor2)
    {

        for (QVector<double> &currentRowVectorOfMatrix1 : matrix1)
            MatrixOperations::rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise_inplace(functor1, currentRowVectorOfMatrix1, matrix2, functor2);

        return matrix1;

    }

    template <typename Functor1, typename Functor2>
    QVector<QVector<double> > &matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise_inplace_multiThreaded(const Functor1 &functor1, QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor2 &functor2)
    {

        auto mapFunctor = [&](QVector<double> &vector) {

            MatrixOperations::rowVector_x_matrix_applyFunctor1ToRowVectorElementWise_applyFunctor2OnResultingVectorElementWise_inplace(functor1, vector, matrix2, functor2);

        };

        QtConcurrent::blockingMap(matrix1, mapFunctor);

        return matrix1;

    }

    QVector<QVector<double> > matrix_x_diagonalizedScalars_x_matrixTranspose(const QVector<QVector<double> > &matrix, const QVector<double> &scalars);

    QVector<QVector<double> > matrix_x_diagonalizedScalars_x_matrixTranspose_multiThreaded(const QVector<QVector<double> > &matrix, const QVector<double> &scalars);

    QVector<QVector<double> > &matrix_x_diagonalizedScalars_x_matrixTranspose_inplace(QVector<QVector<double> > &matrix, const QVector<double> &scalars);

    QVector<QVector<double> > &matrix_x_diagonalizedScalars_x_matrixTranspose_inplace_multiThreaded(QVector<QVector<double> > &matrix, const QVector<double> &scalars);


    template <typename Functor>
    QVector<QVector<double> > createSymmetricMatrix(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        QVector<QVector<double> > result(matrix.size(), QVector<double>(matrix.size()));

        for (int i = 0; i < matrix.size(); ++i) {

            const QVector<double> &currentRowVectorInMatrix(matrix.at(i));

            QVector<double> &currentRowVectorInResult(result[i]);

            currentRowVectorInResult[i] = functor(currentRowVectorInMatrix, currentRowVectorInMatrix);

            for (int j = i + 1; j < matrix.size(); ++j)
                result[j][i] = currentRowVectorInResult[j] = functor(currentRowVectorInMatrix, matrix.at(j));

        }

        return result;
    }

    template <typename Functor>
    QVector<QVector<double> > createSymmetricMatrix_multiThreaded(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        auto mapFunctor = [&](const int &index) {

            const QVector<double> &currentRowVectorInMatrix(matrix.at(index));

            QVector<double> result(matrix.size());

            result[index] = functor(currentRowVectorInMatrix, matrix.at(index));

            for (int j = index + 1; j < matrix.size(); ++j)
                result[j] = functor(currentRowVectorInMatrix, matrix.at(j));

            return result;

        };

        QVector<QVector<double> > result = QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor));

        auto mirrorFunctor = [&](int &index) {

            QVector<double> &currentRowInResult(result[index]);

            for (int i = 0; i < index; ++i)
                currentRowInResult[i] = result.at(i).at(index);

        };

        QVector<int> sequenceOfIndexes = FunctionsForMultiThreading::createSequenceOfIndexes(result.size(), 0, 1);

        QtConcurrent::blockingMap(sequenceOfIndexes, mirrorFunctor);

        return result;

    }

    template <typename Functor>
    QVector<QVector<double> > &createSymmetricMatrix_inplace(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        for (int i = 0; i < matrix.size(); ++i) {

            QVector<double> intermediateVector(matrix.size());

            const QVector<double> &currentRowVectorInMatrix(matrix.at(i));

            for (int j = 0; j < i; ++j)
                intermediateVector[j] = matrix.at(j).at(i);

            intermediateVector[i] = functor(currentRowVectorInMatrix, currentRowVectorInMatrix);

            for (int j = i + 1; j < matrix.size(); ++j)
                intermediateVector[j] = functor(currentRowVectorInMatrix, matrix.at(j));

            matrix[i] = intermediateVector;

        }

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > &createSymmetricMatrix_inplace_multiThreaded(QVector<QVector<double> > &matrix, const Functor &functor)
    {

        FunctionsForMultiThreading::detachMatrix(matrix);

        QSemaphore semaphore(1);

        QReadWriteLock readWriteLock;

        auto mapFunction = [&](const QVector<int> &indexesToProcess) {

            for (const int &indexToProcess : indexesToProcess) {

                semaphore.acquire(indexToProcess + 1);

                readWriteLock.lockForRead();

                QVector<double> intermediateVector(matrix.size());

                const QVector<double> &currentRowVectorInMatrix(matrix.at(indexToProcess));

                intermediateVector[indexToProcess] = functor(currentRowVectorInMatrix, currentRowVectorInMatrix);

                if (indexToProcess + 1 < matrix.size()) {

                    intermediateVector[indexToProcess + 1] = functor(currentRowVectorInMatrix, matrix.at(indexToProcess + 1));

                    semaphore.release(indexToProcess + 2);

                }

                for (int i = indexToProcess + 2; i < matrix.size(); ++i)
                    intermediateVector[i] = functor(currentRowVectorInMatrix, matrix.at(i));

                readWriteLock.unlock();

                readWriteLock.lockForWrite();

                matrix[indexToProcess] = intermediateVector;

                readWriteLock.unlock();

            }

        };


        QFutureSynchronizer<void> futureSynchronizer;

        QVector<QVector<int> > schedule = FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(matrix.size(), QThreadPool::globalInstance()->maxThreadCount());

        for (const QVector<int> &indexesToProcess : schedule)
            futureSynchronizer.addFuture(QtConcurrent::run(mapFunction, indexesToProcess));

        futureSynchronizer.waitForFinished();

        futureSynchronizer.clearFutures();

        auto mirrorFunction = [&](const QVector<int> &indexesToProcess) {

            for (const int &indexToProcess : indexesToProcess) {

                QVector<double> &currentRowVector(matrix[indexToProcess]);

                for (int i = 0; i < indexToProcess; ++i)
                    currentRowVector[i] = matrix.at(i).at(indexToProcess);

            }

        };

        for (const QVector<int> &indexesToProcess : schedule)
            futureSynchronizer.addFuture(QtConcurrent::run(mirrorFunction, indexesToProcess));

        futureSynchronizer.waitForFinished();

        return matrix;

    }

    template <typename Functor>
    QVector<QVector<double> > &createSymmetricMatrix_inplace_multiThreaded(QVector<QVector<double> > &matrix, const Functor &functor, double &progressValue)
    {

        FunctionsForMultiThreading::detachMatrix(matrix);

        progressValue = 0.0;

        QSemaphore semaphore(1);

        QReadWriteLock readWriteLock;

        auto mapFunction = [&](const QVector<int> &indexesToProcess) {

            for (const int &indexToProcess : indexesToProcess) {

                semaphore.acquire(indexToProcess + 1);

                readWriteLock.lockForRead();

                QVector<double> intermediateVector(matrix.size());

                const QVector<double> &currentRowVectorInMatrix(matrix.at(indexToProcess));

                intermediateVector[indexToProcess] = functor(currentRowVectorInMatrix, currentRowVectorInMatrix);

                if (indexToProcess + 1 < matrix.size()) {

                    intermediateVector[indexToProcess + 1] = functor(currentRowVectorInMatrix, matrix.at(indexToProcess + 1));

                    semaphore.release(indexToProcess + 2);

                }

                for (int i = indexToProcess + 2; i < matrix.size(); ++i)
                    intermediateVector[i] = functor(currentRowVectorInMatrix, matrix.at(i));

                readWriteLock.unlock();

                readWriteLock.lockForWrite();

                matrix[indexToProcess] = intermediateVector;

                progressValue += matrix.size() - indexToProcess - 1;

                readWriteLock.unlock();

            }

        };


        QFutureSynchronizer<void> futureSynchronizer;

        QVector<QVector<int> > schedule = FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(matrix.size(), QThreadPool::globalInstance()->maxThreadCount());

        for (const QVector<int> &indexesToProcess : schedule)
            futureSynchronizer.addFuture(QtConcurrent::run(mapFunction, indexesToProcess));

        futureSynchronizer.waitForFinished();

        futureSynchronizer.clearFutures();

        auto mirrorFunction = [&](const QVector<int> &indexesToProcess) {

            for (const int &indexToProcess : indexesToProcess) {

                QVector<double> &currentRowVector(matrix[indexToProcess]);

                for (int i = 0; i < indexToProcess; ++i)
                    currentRowVector[i] = matrix.at(i).at(indexToProcess);

            }

        };

        for (const QVector<int> &indexesToProcess : schedule)
            futureSynchronizer.addFuture(QtConcurrent::run(mirrorFunction, indexesToProcess));

        futureSynchronizer.waitForFinished();

        return matrix;
    }

    int eigenSym(QVector<QVector<double> > &symmetricMatrixOnInput_eigenvectorsOnOutput, QVector<double> &eigenvalues);

    int eigenSym_multiThreaded(QVector<QVector<double> > &symmetricMatrixOnInput_eigenvectorsOnOutput, QVector<double> &eigenvalues);

    int eigenvaluesAndEigenVectorsOfRealTridiagonalMatrix(QVector<double> &d, QVector<double> &e, int numberOfComponents, QVector<double> &eigenvalues, QVector<QVector<double> > &realSymmetricTridiagonalMatrixOnInput_eigenvectorsOnOutput);

    QVector<QVector<double> > &fillMatrixWithRandomNumbersFromUniformRealDistribution(QVector<QVector<double> > &vector, const double &minimumValue, const double &maximumValue);

    QVector<QVector<double> > &mPower_inplace(QVector<QVector<double> > &symmetricRealMatrix, const double &power);

    QVector<QVector<double> > &mPower_inplace_multiThreaded(QVector<QVector<double> > &symmetricRealMatrix, const double &power);


    int singularValueDecomposition(const QVector<QVector<double> > &matrix, QVector<QVector<double> > &u, QVector<double> &s, QVector<QVector<double> > &vTransposed);

    QVector<QVector<double> > subMatrixBasedOnColumnIndexes(const QVector<QVector<double> > &matrix, QList<int> columnIndexex);

    void tred2(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e);

    void tred2(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e, double &progressValue);

    void tred2_multiThreaded(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e);

    void tred2_multiThreaded(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e, double &progressValue);

    template <typename T>
    QVector<double> matrixToColumnMajorOrder(const QVector<QVector<T> > &matrix)
    {

        QVector<double> result;

        result.reserve(matrix.size() * (matrix.isEmpty() ? 0 : matrix.first().size()));

        for (const QVector<double> &vector : matrix)
            result << vector;

        return result;

    }

    template <typename T>
    QVector<double> matrixToRowMajorOrder(const QVector<QVector<T> > &matrix)
    {

        QVector<double> result;

        result.reserve(matrix.size() * (matrix.isEmpty() ? 0 : matrix.first().size()));

        for (const QVector<double> &vector : matrix)
            result << vector;

        return result;

    }

    template <typename T>
    QVector<QVector<double> > rowMajorOrderToMatrix(QVector<T> &rowMajorOrder, int numberOfRows, int numberOfColumns) {

        QVector<QVector<double> > result(numberOfRows);

        auto itBegin = rowMajorOrder.begin();

        for (int i = 0; i < numberOfRows; ++i) {

            auto itEnd = std::next(itBegin, numberOfColumns);

            std::move(itBegin, itEnd, std::back_inserter(result[i]));

            itBegin = itEnd;

        }

        return result;

    }

    template <typename T>
    T *matrixToColumnMajorOrderArray(const QVector<QVector<T> > &matrix)
    {
        int numberOfRows = matrix.size();

        int numberOfColumns = (matrix.isEmpty() ? 0 : matrix.at(0).size());

        double *array;

        array = new double[numberOfRows * numberOfColumns];

        for (int i = 0; i < numberOfRows; ++i) {

            const QVector<T> &currentVector(matrix.at(i));

            for (int j = 0; j < numberOfColumns; ++j)
                array[i + j * numberOfRows] = currentVector.at(j);

        }

        return array;

    }

    template <typename T>
    QVector<QVector<T> > columnMajorOrderArrayToMatrix(T *array, int numberOfRows, int numberOfColumns)
    {
        QVector<QVector<T> > matrix(numberOfRows, QVector<T>(numberOfColumns));

        for (int i = 0; i < numberOfRows; ++i) {

            QVector<T> &currentVector(matrix[i]);

            for (int j = 0; j < numberOfColumns; ++j)
                currentVector[j] = array[i + j * numberOfRows];

        }

        return matrix;

    }

    QVector<QVector<double> > &orthogonalizeMatrix_inplace(QVector<QVector<double> > &matrix);

}

#endif // MATRIXOPERATIONS_H
