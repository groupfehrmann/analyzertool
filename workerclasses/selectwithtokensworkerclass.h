#ifndef SELECTWITHTOKENSWORKERCLASS_H
#define SELECTWITHTOKENSWORKERCLASS_H

#include <QString>

#include "base/baseworkerclass.h"

class SelectWithTokensWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    enum Mode{

        CONTAINS = 0,

        EXACTMATCH = 1

    };

    struct Parameters {

        QStringList applyToAnnotationLabels;

        QSharedPointer<BaseDataset> baseDataset;

        Qt::CaseSensitivity caseSensitivity;

        bool applyToIdentifiers;

        Mode mode;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedIdentifiersAndIndexes;

        QStringList tokensToMatch;

    };

    SelectWithTokensWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    SelectWithTokensWorkerClass::Parameters d_parameters;

};

#endif // SELECTWITHTOKENSWORKERCLASS_H
