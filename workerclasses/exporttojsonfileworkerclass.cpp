#include "exporttojsonfileworkerclass.h"


ExportToJSONFileWorkerClass::ExportToJSONFileWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<ExportToJSONFileWorkerClass::Parameters *>(parameters))
{

}

bool ExportToJSONFileWorkerClass::checkInputParameters()
{

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected");

        return false;

    }

    return true;

}

void ExportToJSONFileWorkerClass::doWork()
{
    emit processStarted("export to JSON file");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Export to JSON file"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "export format: \"" + d_parameters.exportFormat + "\"");

    if (!d_parameters.annotationLabelsForVariables.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotations labels used for variables: " + ConvertFunctions::toString(d_parameters.annotationLabelsForVariables));

    if (!d_parameters.annotationLabelsForItems.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotations labels used for items: " + ConvertFunctions::toString(d_parameters.annotationLabelsForItems));

    if (!d_parameters.annotationIdentifiersForItems.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotations identifiers used for items: " + ConvertFunctions::toString(d_parameters.annotationIdentifiersForItems));

    if (!d_parameters.exportDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    if (d_parameters.exportFormat == "json for distroChart")
        this->processExportFormat_forDistroChart();
    else if (d_parameters.exportFormat == "json for JEasyUI DataGrid")
        this->processExportFormat_forJEasyUIDataGrid();

    QThread::currentThread()->quit();

}

void ExportToJSONFileWorkerClass::processExportFormat_forDistroChart()
{

    if (!this->exportSearchableKeysForVariables())
        return;

    QHash<QString, int> itemIdentifierToRank;

    for (int i = 0; i < d_parameters.annotationIdentifiersForItems.size(); ++i)
        itemIdentifierToRank.insert(d_parameters.annotationIdentifiersForItems.at(i), i);

    emit startProgress(0, "Exporting variables", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        QVector<QPair<QString, QPair<int, double> > > itemIdentifier_rank_value;

        QVector<double> currentDataVector = d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        for (int j = 0; j < d_parameters.selectedItemIdentifiersAndIndexes.second.size(); ++j) {

            QList<QVariant> annotationValues;

            if (d_parameters.annotationLabelsForItems.isEmpty())
                annotationValues << d_parameters.selectedItemIdentifiersAndIndexes.first.at(j);
            else
                annotationValues = d_parameters.baseDataset->annotationValues(d_parameters.selectedItemIdentifiersAndIndexes.second.at(j), d_parameters.annotationLabelsForItems, BaseMatrix::switchOrientation(d_parameters.orientation));

            QString temp = annotationValues.first().toString();

            for (int k = 1; k < annotationValues.size(); ++k)
                temp += " \\\\\\\\\\\\ " + annotationValues.at(k).toString();

            temp.remove('"');

            if (itemIdentifierToRank.contains(temp))
                itemIdentifier_rank_value.append(QPair<QString, QPair<int, double> >(temp, QPair<int, double>(itemIdentifierToRank.value(temp), currentDataVector.at(j))));

            if (QThread::currentThread()->isInterruptionRequested())
                return;

        }

        std::sort(itemIdentifier_rank_value.begin(), itemIdentifier_rank_value.end(), [](const QPair<QString, QPair<int, double > > &element1, const QPair<QString, QPair<int, double > > &element2) {

            if (element1.second.first < element2.second.first)
                return true;
            else if (element1.second.first == element2.second.first)
                return (element1.second. second < element2.second.second);

            return false;

        });

        QFile fileOut(d_parameters.exportDirectory + "/" + QString::number(i + 1) + "_variableDistroChart.json");

        if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/" + QString::number(i) + "_variableDistroChart.json" + "\"");

            QThread::currentThread()->quit();

            return;

        }

        QTextStream out(&fileOut);

        out << "[" << Qt::endl;

        bool first = true;

        for (int j = 0; j < itemIdentifier_rank_value.size(); ++j) {

            QPair<QString, QPair<int, double> > currentItem(itemIdentifier_rank_value.at(j));

            if (std::isfinite(currentItem.second.second)) {

                if (first) {

                    out << "\t{\"value\":" << currentItem.second.second << ",\"label\":\"" << currentItem.first << "\"}";

                    first = false;

                } else
                    out << "," << Qt::endl << "\t{\"value\":" << currentItem.second.second << ",\"label\":\"" << currentItem.first << "\"}";

            }

        }

        out << Qt::endl << "]";

        fileOut.close();

        if (QThread::currentThread()->isInterruptionRequested()) {

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

}

void ExportToJSONFileWorkerClass::processExportFormat_forJEasyUIDataGrid()
{

    QFile fileOut(d_parameters.exportDirectory + "/JEasyUIDataGrid.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/JEasyUIDataGrid.json" + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Constructing column labels", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

    QStringList columnLabels;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        QList<QVariant> annotationValues;

        if (d_parameters.annotationLabelsForVariables.isEmpty())
            annotationValues << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i);
        else
            annotationValues = d_parameters.baseDataset->annotationValues(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelsForVariables, d_parameters.orientation);

        QString temp = annotationValues.first().toString();

        for (int j = 1; j < annotationValues.size(); ++j)
            temp += " \\\\\\\\\\\\ " + annotationValues.at(j).toString();

        temp.remove('"');

        columnLabels.append(temp);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Constructing column labels", 0, d_parameters.selectedItemIdentifiersAndIndexes.second.size());

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (int i = 0; i < d_parameters.selectedItemIdentifiersAndIndexes.second.size(); ++i) {

        QList<QVariant> annotationValues;

        if (d_parameters.annotationLabelsForItems.isEmpty())
            annotationValues << d_parameters.selectedItemIdentifiersAndIndexes.first.at(i);
        else
            annotationValues = d_parameters.baseDataset->annotationValues(d_parameters.selectedItemIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelsForItems, BaseMatrix::switchOrientation(d_parameters.orientation));

        QString temp = annotationValues.first().toString();

        for (int j = 1; j < annotationValues.size(); ++j)
            temp += " \\\\\\\\\\\\ " + annotationValues.at(j).toString();

        temp.remove('"');

        QVector<QVariant> currentDataVector = d_parameters.baseDataset->baseMatrix()->vectorOfQVariant(d_parameters.selectedItemIdentifiersAndIndexes.second.at(i), d_parameters.selectedVariableIdentifiersAndIndexes.second, BaseMatrix::switchOrientation(d_parameters.orientation));

        double value;

        if (first) {

            out << "\t{\"" + columnLabels.at(0) + "\":";

            if (ConvertFunctions::canConvert(currentDataVector.at(0), value))
                out << value;
            else
                out << currentDataVector.at(0).toString();

            for (int j = 1; j < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++j) {

                out << ",\"" + columnLabels.at(j) + "\":";

                if (ConvertFunctions::canConvert(currentDataVector.at(j), value))
                    out << value;
                else
                    out << currentDataVector.at(j).toString();

            }

            out << "}";

            first = false;

        } else {

            out << "," << Qt::endl << "\t{\"" + columnLabels.at(0) + "\":";

            if (ConvertFunctions::canConvert(currentDataVector.at(0), value))
                out << value;
            else
                out << currentDataVector.at(0).toString();

            for (int j = 1; j < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++j) {

                out << ",\"" + columnLabels.at(j) + "\":";

                if (ConvertFunctions::canConvert(currentDataVector.at(j), value))
                    out << value;
                else
                    out << currentDataVector.at(j).toString();

            }

            out << "}";

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        emit updateProgress(0, i + 1);

    }

    out << Qt::endl << "]}";

    emit stopProgress(0);

}

bool ExportToJSONFileWorkerClass::exportSearchableKeysForVariables()
{

    emit startProgress(0, "Constructing searchable keys for variables", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

    QStringList searchableKeysForVariables;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        QList<QVariant> annotationValues;

        if (d_parameters.annotationLabelsForVariables.isEmpty())
            annotationValues << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i);
        else
            annotationValues = d_parameters.baseDataset->annotationValues(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelsForVariables, d_parameters.orientation);

        QString temp = annotationValues.first().toString();

        for (int j = 1; j < annotationValues.size(); ++j)
            temp += " \\\\\\\\\\\\ " + annotationValues.at(j).toString();

        temp.remove('"');

        searchableKeysForVariables.append(temp);

        if (QThread::currentThread()->isInterruptionRequested()) {

            return false;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    QFile fileOut(d_parameters.exportDirectory + "/variableLabelToIndex.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/variableLabelToIndex.json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "[";

    out << Qt::endl << "\t" << "{" << Qt::endl;

    out << "\t\t" << "\"value\":" << 1 << "," << Qt::endl;

    out << "\t\t" << "\"label\":\"";

    out << searchableKeysForVariables.first();

    out << "\"" << Qt::endl;

    out << "\t}";

    for (int i = 1; i < searchableKeysForVariables.size(); ++i) {

        out << "," << Qt::endl << "\t" << "{" << Qt::endl;

        out << "\t\t" << "\"value\":" << i + 1 << "," << Qt::endl;

        out << "\t\t" << "\"label\":\"";

        out << searchableKeysForVariables.at(i);

        out << "\"" << Qt::endl;

        out << "\t}";

    }

    out << Qt::endl << "]";

    fileOut.close();

    return true;

}
