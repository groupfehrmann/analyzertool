#include "transposedatasetworkerclass.h"

TransposeDatasetWorkerClass::TransposeDatasetWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<TransposeDatasetWorkerClass::Parameters *>(parameters))
{

}

void TransposeDatasetWorkerClass::doWork()
{
    emit processStarted("Transpose dataset");

    emit appendLogItem(LogListModel::PARAMETER, "dataset to transpose: \"" + d_parameters.datasetToTranspose->name() +"\"");

    emit startProgress(0, "Transposing", 0, 0);

    emit datasetBeginChange(d_parameters.datasetToTranspose->universallyUniqueIdentifier());

    d_parameters.datasetToTranspose->transpose();

    emit datasetEndChange(d_parameters.datasetToTranspose->universallyUniqueIdentifier());

    emit stopProgress(0);

    QThread::currentThread()->quit();

}
