#ifndef LANCASTERSMETHOD_H
#define LANCASTERSMETHOD_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include <cmath>
#include <limits>

#include "boost/math/distributions/chi_squared.hpp"

#include "statistics/basemetaanalysistest.h"

class LancastersMethod : public BaseMetaAnalysisTest
{

public:

    LancastersMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    ~LancastersMethod();

    QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const;

    const QStringList metaAnalysisTestDescriptiveLabels() const;

protected:

    void _reset();

    static QStringList d_baseStatisticalTestDescriptiveLabels;

    double d_degreeOfFreedom;

    static QStringList d_metaAnalysisTestDescriptiveLabels;

    static QString d_nameOfMetaAnalysisTest;

    QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues();

    QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues();

    void calculateStatistic();

    void calculatedPValue();

};

#endif // LANCASTERSMETHOD_H
