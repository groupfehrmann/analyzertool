#ifndef CALCULATEMODULESCORESWORKERCLASS_H
#define CALCULATEMODULESCORESWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QVariantList>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"

class CalculateModuleScoresWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QStringList annotationLabelsDefiningAnnotationValuesForVariables;

        QSharedPointer<BaseDataset> baseDataset;

        QString exportDirectory;

        QString fileDefiningModules;

        QString formatOfFileDefiningModules;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

    };


    CalculateModuleScoresWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

    void updateFileProgress();

private:

    CalculateModuleScoresWorkerClass::Parameters d_parameters;

    QStringList d_moduleIdentifiers;

    QVector<QVector<QPair<QString, double> > > d_moduleToMembersAndWeights;

    QFile d_file;

    bool checkInputParameters();

    bool readModules_moduleDefinedByGeneMembershipOnly();

    bool readModules_moduleDefinedByGeneMembershipAndWeight();

    bool readModules_moduleDefinedByWeightAndGeneMembership();

};

#endif // CALCULATEMODULESCORESWORKERCLASS_H
