#include "math/functionsformultithreading.h"

QList<QPair<int, int> > FunctionsForMultiThreading::createIndexBlocksForMultiThreading(int sizeOfSequence, int numberOfThreads)
{

    QList<QPair<int, int> > blocks;

    std::div_t divResult = std::div(sizeOfSequence, numberOfThreads);

    if (divResult.quot == 0) {

        for (int i = 0; i < divResult.rem; ++i)
            blocks << QPair<int, int>(i, i + 1);

    } else {

        int counter = 0;

        int remainder = divResult.rem;

        for (int i = 0; i < numberOfThreads; ++i) {

            QPair<int, int> block;

            block.first = counter;

            for (int j = 0; j < divResult.quot - 1; ++j)
                ++counter;

            if (remainder != 0) {

                ++counter;

                --remainder;

            }

            block.second = counter + 1;

            ++counter;

            blocks << block;

        }

    }

    return blocks;

}

QVector<QVector<int> > FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(int sizeOfSequence, int numberOfThreads) {

    QVector<QVector<int> > scheduler(numberOfThreads);

    for (int i = 0; i < sizeOfSequence; ++i)
        scheduler[(i % numberOfThreads)] << i;

    return scheduler;

}

QVector<int> FunctionsForMultiThreading::createSequenceOfIndexes(int numberOfIndexes, int startIndex, int stride, bool shuffle)
{

    QVector<int> indexes;

    if (stride == 1) {

        indexes.resize(numberOfIndexes);

        std::iota(indexes.begin(), indexes.end(), startIndex);

    } else {

        for (int i = startIndex; i < numberOfIndexes; i += stride)
            indexes << i;

    }

    if (shuffle) {

        std::random_device rd;

        std::mt19937 g(rd());

        std::shuffle(indexes.begin(), indexes.end(), g);

    }

    return indexes;

}
