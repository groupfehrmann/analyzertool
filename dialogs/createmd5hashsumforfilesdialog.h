#ifndef CREATEMD5HASHSUMFORFILESDIALOG_H
#define CREATEMD5HASHSUMFORFILESDIALOG_H

#include "dialogs/basedialog.h"
#include "workerclasses/createmd5hashsumforfilesworkerclass.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "widgets/lineeditbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"

class CreateMD5HashSumForFilesDialog : public BaseDialog
{

    Q_OBJECT

public:

    CreateMD5HashSumForFilesDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString());


    ~CreateMD5HashSumForFilesDialog();

private:

    SelectDirectoryBuildingBlockWidget *d_selectImportDirectory;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_copyUniqueFilesToExportDirectory;

    LineEditBuildingBlockWidget *d_lineEditBuildingBlockWidget_patternToFilterValidFiles;

    SelectDirectoryBuildingBlockWidget *d_selectExportDirectory;

    void initializeWidgets();

    void initializeParameters();

};

#endif // CREATEMD5HASHSUMFORFILESDIALOG_H
