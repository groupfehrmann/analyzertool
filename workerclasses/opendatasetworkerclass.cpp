#include "opendatasetworkerclass.h"

OpenDatasetWorkerClass::OpenDatasetWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<OpenDatasetWorkerClass::Parameters *>(parameters))
{

}

void OpenDatasetWorkerClass::doWork()
{
    emit processStarted("Open dataset");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    d_fileIn.setFileName(d_parameters.pathToFile);

    if (!d_fileIn.open(QIODevice::ReadOnly)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Opening dataset", 0, d_fileIn.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    QDataStream in(&d_fileIn);

    BaseDataset *baseDataset = new BaseDataset();

    in >> *baseDataset;

    emit stopProgress(0);

    emit appendLogItem(LogListModel::MESSAGE, "number of rows: " + QString::number(baseDataset->header(BaseMatrix::ROW).count()));

    emit appendLogItem(LogListModel::MESSAGE, "number of columns: " + QString::number(baseDataset->header(BaseMatrix::COLUMN).count()));

    emit appendLogItem(LogListModel::MESSAGE, "number row annotation labels: " + QString::number(baseDataset->annotations(BaseMatrix::ROW).labels().count()));

    emit appendLogItem(LogListModel::MESSAGE, "number column annotation labels: " + QString::number(baseDataset->annotations(BaseMatrix::COLUMN).labels().count()));

    emit datasetAvailable(baseDataset);

    emit addToRecentlyOpenedFiles(baseDataset->name(), d_parameters.pathToFile);

    QThread::currentThread()->quit();

}

void OpenDatasetWorkerClass::updateFileProgress()
{

    emit updateProgress(0, d_fileIn.pos());

}
