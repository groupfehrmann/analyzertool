#ifndef CREATESCATTERPLOTSWITHRANKONXAXISWORKERCLASS_H
#define CREATESCATTERPLOTSWITHRANKONXAXISWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QAbstractSeries>
#include <QDir>

#include <functional>
#include <utility>
#include <algorithm>

#include "base/basedataset.h"
#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/exportplottofile.h"
#include "charts/scatterchart_rankonxaxis.h"
#include "base/convertfunctions.h"
#include "charts/openplotfromfile.h"
#include "charts/copychartparameters.h"

using namespace QtCharts;


class CreateScatterPlotsWithRankOnXAxisWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString annotationLabelDefiningAlternativeVariableIdentifier;

        QString annotationLabelDefiningRank;

        QString annotationLabelDefiningSubsets;

        QSharedPointer<BaseDataset> baseDataset;

        QChart::ChartType chartType;

        int dpiOfPlot;

        QString exportDirectory;

        QString exportTypeOfPlot;

        int heightOfPlot;

        BaseMatrix::Orientation orientation;

        QString pathToTemplatePlot;

        QString plotMode;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        QStringList subsetIdentifiers;

        int widthOfPlot;

    };

    CreateScatterPlotsWithRankOnXAxisWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CreateScatterPlotsWithRankOnXAxisWorkerClass::Parameters d_parameters;

    QSharedPointer<QChart> d_templatePlot;

};

#endif // CREATESCATTERPLOTSWITHRANKONXAXISWORKERCLASS_H
