#ifndef MERGEDATASETSDIALOG_H
#define MERGEDATASETSDIALOG_H

#include "base/projectlist.h"
#include "dialogs/basedialog.h"
#include "workerclasses/mergedatasetsworkerclass.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "base/projectlistmodel.h"

class MergeDatasetsDialog : public BaseDialog
{

    Q_OBJECT

public:

    MergeDatasetsDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), const ProjectListModel *projectListModel = nullptr);


    ~MergeDatasetsDialog();

private:

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_basedatasets;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_mergedDatasetDataType;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_mergeRowAnnotations;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_mergeColumnAnnotations;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_createUniqueIdentifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_createUniqueAnnotationLabels;

    const ProjectListModel * d_projectListModel;

    QList<QSharedPointer<BaseDataset> > d_baseDatasets;

    QStringList d_baseDatasetDescriptions;

    QList<bool> d_selectedOnlyStatusOfBaseDatasets;

    void initializeWidgets();

    void initializeParameters();

};

#endif // MERGEDATASETSDIALOG_H
