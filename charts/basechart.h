#ifndef BASECHART_H
#define BASECHART_H

#include <QObject>
#include <QEventLoop>
#include <QSharedPointer>
#include <QChart>
#include <QApplication>

using namespace QtCharts;

class BaseChart : public QObject
{

    Q_OBJECT

public:

    explicit BaseChart(QObject *parent = 0);

    virtual ~BaseChart();

    const QSharedPointer<QChart> &chart();

protected:

    QSharedPointer<QChart> d_chart;

    QEventLoop d_eventLoop;

signals:

    void finishedInitializationOfChart();

    void deleteChart();

    void finishedDeletingChart();

private slots:

    void _deleteChart();

};

#endif // BASECHART_H
