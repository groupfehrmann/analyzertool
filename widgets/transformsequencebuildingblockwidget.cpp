#include "transformsequencebuildingblockwidget.h"
#include "ui_transformsequencebuildingblockwidget.h"

TransformSequenceUtils::TransformFunction::TransformFunction(QObject *parent, const QString &functionDescription, const QStringList &parameterDescriptions, const QList<QValidator *> &parameterValidators) :
    QObject(parent), d_functionDescription(functionDescription), d_parameterDescriptions(parameterDescriptions), d_parameterValidators(parameterValidators), d_parameterValues(parameterDescriptions.size())
{

}

const QString &TransformSequenceUtils::TransformFunction::functionDescriptionWithoutParametersReplacement() const
{

    return d_functionDescription;

}

QString TransformSequenceUtils::TransformFunction::functionDescriptionWithParametersReplacement() const
{

    QString _functionDescription = d_functionDescription;

    for (int i = 0; i < d_parameterValues.size(); ++i)
        _functionDescription = _functionDescription.replace(QString("#") + QChar('a' + i), d_parameterValues.at(i).toString());

    return _functionDescription;

}

int TransformSequenceUtils::TransformFunction::numberOfParameters() const
{

    return d_parameterValues.size();

}

const QString &TransformSequenceUtils::TransformFunction::parameterDescription(int index) const
{

    return d_parameterDescriptions.at(index);

}

const QValidator *TransformSequenceUtils::TransformFunction::parameterValidator(int index)
{

    return d_parameterValidators.at(index);

}

void TransformSequenceUtils::TransformFunction::parameterValueChanged(int index, const QString &stringRepresentationOfValue)
{

    d_parameterValues[index] = QVariant(stringRepresentationOfValue);

}

TransformSequenceUtils::VectorScalarAdd::VectorScalarAdd(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "X = X + #a", {"#a"}, {new QDoubleValidator})
{

}

TransformSequenceUtils::VectorScalarAdd::~VectorScalarAdd()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::VectorScalarAdd::func()
{

    double scalar = d_parameterValues.at(0).toDouble();

    auto lambda = [scalar](QVector<double> &vector) {

        for (double &value : vector)
            value += scalar;

    };

    return lambda;

}

TransformSequenceUtils::VectorScalarMin::VectorScalarMin(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "X = X - #a", {"#a"}, {new QDoubleValidator})
{

}

TransformSequenceUtils::VectorScalarMin::~VectorScalarMin()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::VectorScalarMin::func()
{

    double scalar = d_parameterValues.at(0).toDouble();

    auto lambda = [scalar](QVector<double> &vector) {

        for (double &value : vector)
            value -= scalar;

    };

    return lambda;

}

TransformSequenceUtils::ScalarVectorMin::ScalarVectorMin(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "X = #a - X", {"#a"}, {new QDoubleValidator})
{

}

TransformSequenceUtils::ScalarVectorMin::~ScalarVectorMin()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::ScalarVectorMin::func()
{

    double scalar = d_parameterValues.at(0).toDouble();

    auto lambda = [scalar](QVector<double> &vector) {

        for (double &value : vector)
            value = scalar - value;

    };

    return lambda;

}

TransformSequenceUtils::VectorScalarMul::VectorScalarMul(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "X = #a * X", {"#a"}, {new QDoubleValidator})
{

}

TransformSequenceUtils::VectorScalarMul::~VectorScalarMul()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::VectorScalarMul::func()
{

    double scalar = d_parameterValues.at(0).toDouble();

    auto lambda = [scalar](QVector<double> &vector) {

        for (double &value : vector)
            value *= scalar;

    };

    return lambda;

}

TransformSequenceUtils::VectorScalarDiv::VectorScalarDiv(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "X = X / #a", {"#a"}, {new QDoubleValidator})
{

}

TransformSequenceUtils::VectorScalarDiv::~VectorScalarDiv()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::VectorScalarDiv::func()
{

    double scalar = d_parameterValues.at(0).toDouble();

    auto lambda = [scalar](QVector<double> &vector) {

        for (double &value : vector)
            value /= scalar;

    };

    return lambda;

}

TransformSequenceUtils::ScalarVectorDiv::ScalarVectorDiv(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "X = #a / X", {"#a"}, {new QDoubleValidator})
{

}

TransformSequenceUtils::ScalarVectorDiv::~ScalarVectorDiv()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::ScalarVectorDiv::func()
{

    double scalar = d_parameterValues.at(0).toDouble();

    auto lambda = [scalar](QVector<double> &vector) {

        for (double &value : vector)
            value = scalar / value;

    };

    return lambda;

}

TransformSequenceUtils::CenterVectorToMean::CenterVectorToMean(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Center vector to mean")
{

}

TransformSequenceUtils::CenterVectorToMean::~CenterVectorToMean()
{

}

std::function<void (QVector<double> &)> TransformSequenceUtils::CenterVectorToMean::func()
{

    auto lambda = [](QVector<double> &vector) {

        double _mean = MathDescriptives::mean(vector);

        for (double &value : vector)
            value -= _mean;

    };

    return lambda;

}

TransformSequenceUtils::StandardizeVector::StandardizeVector(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Standardize vector")
{

}

TransformSequenceUtils::StandardizeVector::~StandardizeVector()
{

}

std::function<void (QVector<double> &)> TransformSequenceUtils::StandardizeVector::func()
{

    auto lambda = [](QVector<double> &vector) {

        VectorOperations::standardizeVector_inplace(vector);

    };

    return lambda;

}

TransformSequenceUtils::NormalizeVector::NormalizeVector(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Normalize vector")
{

}

TransformSequenceUtils::NormalizeVector::~NormalizeVector()
{

}

std::function<void (QVector<double> &)> TransformSequenceUtils::NormalizeVector::func()
{

    auto lambda = [](QVector<double> &vector) {

        VectorOperations::normalizeVector_inplace(vector);

    };

    return lambda;

}

TransformSequenceUtils::RescaleVectorWithTwoPercentiles::RescaleVectorWithTwoPercentiles(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Rescale vector with two percentiles: lower percentile = #a; upper percentile = #b, new value of lower percentile = #c, new value of upper percentile = #d", {"lower percentile #a", "upper percentile #b", "new value for lower percentile #c", "new value for upper percentile #d"}, {new QDoubleValidator(0.0, 1.0, 2), new QDoubleValidator(0.0, 1.0, 2), new QDoubleValidator, new QDoubleValidator})
{

}

TransformSequenceUtils::RescaleVectorWithTwoPercentiles::~RescaleVectorWithTwoPercentiles()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::RescaleVectorWithTwoPercentiles::func()
{

    double lowerPercentile = d_parameterValues.at(0).toDouble();

    double upperPercentile = d_parameterValues.at(1).toDouble();

    double newValueOfLowerPercentile = d_parameterValues.at(2).toDouble();

    double newValueOfUpperPercentile = d_parameterValues.at(3).toDouble();

    double delta2 = std::fabs(newValueOfUpperPercentile - newValueOfLowerPercentile);

    auto lambda = [lowerPercentile, upperPercentile, newValueOfLowerPercentile, delta2](QVector<double> &vector) {

        QVector<double> copyOfVector = vector;

        std::sort(copyOfVector.begin(), copyOfVector.end());

        double currentValueOfLowerPercentile = MathDescriptives::percentileOfPresortedVector(copyOfVector, lowerPercentile);

        double delta1 = std::fabs(MathDescriptives::percentileOfPresortedVector(copyOfVector, upperPercentile) - currentValueOfLowerPercentile);

        double scaleFactor = delta1 / delta2;

        for (double &value : vector)
            value = value / scaleFactor;

        double shiftFactor = newValueOfLowerPercentile - (currentValueOfLowerPercentile / scaleFactor);

        for (double &value : vector)
            value = value + shiftFactor;

    };

    return lambda;

}

TransformSequenceUtils::LogTransformVector::LogTransformVector(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "X = Log#a(X)", {"#a"}, {new QDoubleValidator})
{

}

TransformSequenceUtils::LogTransformVector::~LogTransformVector()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::LogTransformVector::func()
{

    double base = d_parameterValues.at(0).toDouble();

    auto lambda = [base](QVector<double> &vector) {

        for (double &value : vector)
            value = std::log(value) / std::log(base);

    };

    return lambda;

}

TransformSequenceUtils::RankVector::RankVector(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Rank vector")
{

}

TransformSequenceUtils::RankVector::~RankVector()
{

}

std::function<void (QVector<double> &)> TransformSequenceUtils::RankVector::func()
{

    auto lambda = [](QVector<double> &vector) {

        VectorOperations::crank(vector);

    };

    return lambda;

}


TransformSequenceUtils::RecodeVectorEqualOrLess::RecodeVectorEqualOrLess(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "if (X <= #a) then X = #b", {"#a", "#b"}, {new QDoubleValidator, new QDoubleValidator})
{

}

TransformSequenceUtils::RecodeVectorEqualOrLess::~RecodeVectorEqualOrLess()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::RecodeVectorEqualOrLess::func()
{

    double threshold = d_parameterValues.at(0).toDouble();

    double recodedValue = d_parameterValues.at(1).toDouble();

    auto lambda = [threshold, recodedValue](QVector<double> &vector) {

        for (double &value : vector) {

            if (value <= threshold)
                value = recodedValue;

        }

    };

    return lambda;

}

TransformSequenceUtils::RecodeVectorEqualOrGreater::RecodeVectorEqualOrGreater(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "if (X >= #a) then X = #b", {"#a", "#b"}, {new QDoubleValidator, new QDoubleValidator})
{

}

TransformSequenceUtils::RecodeVectorEqualOrGreater::~RecodeVectorEqualOrGreater()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::RecodeVectorEqualOrGreater::func()
{

    double threshold = d_parameterValues.at(0).toDouble();

    double recodedValue = d_parameterValues.at(1).toDouble();

    auto lambda = [threshold, recodedValue](QVector<double> &vector) {

        for (double &value : vector) {

            if (value >= threshold)
                value = recodedValue;

        }

    };

    return lambda;

}

TransformSequenceUtils::RecodeVectorUnequal::RecodeVectorUnequal(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "if (X != #a) then X = #b", {"#a", "#b"}, {new QDoubleValidator, new QDoubleValidator})
{

}

TransformSequenceUtils::RecodeVectorUnequal::~RecodeVectorUnequal()
{

    for (QValidator *parameterValidator : d_parameterValidators)
        delete static_cast<QDoubleValidator *>(parameterValidator);

}

std::function<void (QVector<double> &)> TransformSequenceUtils::RecodeVectorUnequal::func()
{

    double valueToCompareWith = d_parameterValues.at(0).toDouble();

    double recodedValue = d_parameterValues.at(1).toDouble();

    auto lambda = [valueToCompareWith, recodedValue](QVector<double> &vector) {

        for (double &value : vector) {

            if (!(std::fabs(value - valueToCompareWith) <= std::numeric_limits<double>::epsilon()))
                value = recodedValue;

        }

    };

    return lambda;

}

TransformSequenceUtils::DoubleMedianAbsoluteDeviation::DoubleMedianAbsoluteDeviation(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Convert to double median absolute deviation")
{

}

TransformSequenceUtils::DoubleMedianAbsoluteDeviation::~DoubleMedianAbsoluteDeviation()
{

}

std::function<void (QVector<double> &)> TransformSequenceUtils::DoubleMedianAbsoluteDeviation::func()
{

    auto lambda = [](QVector<double> &vector) {

        double _median = MathDescriptives::median(vector);

        QVector<double> absoluteDeviationsFromMedian;

        QVector<double> leftVector;

        QVector<double> rightVector;

        for (const double &value : vector) {

            double absoluteDeviationFromMedian = std::fabs(value - _median);

            absoluteDeviationsFromMedian << absoluteDeviationFromMedian;

            if (value <= _median)
                leftVector << absoluteDeviationFromMedian;
            else if (value >= _median)
                rightVector << absoluteDeviationFromMedian;

        }

        double leftMAD = MathDescriptives::median(leftVector);

        double rightMAD = MathDescriptives::median(rightVector);

        for (int i = 0; i < vector.size(); ++i) {

            double &value(vector[i]);

            if (value <= _median)
                value = absoluteDeviationsFromMedian.at(i) / leftMAD;
            else
                value = absoluteDeviationsFromMedian.at(i) / rightMAD;

        }

    };

    return lambda;

}

TransformSequenceUtils::ResolutionBasedOutlierScore::ResolutionBasedOutlierScore(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Convert to resolution based outlier score")
{

}

TransformSequenceUtils::ResolutionBasedOutlierScore::~ResolutionBasedOutlierScore()
{

}

std::function<void (QVector<double> &)> TransformSequenceUtils::ResolutionBasedOutlierScore::func()
{

    auto lambda = [](QVector<double> &vector) {

        QVector<QVector<double> > distanceMatrix(vector.size(), QVector<double>(vector.size(), 0.0));

        for (int i = 0; i < vector.size(); ++i) {

            for (int j = i + 1; j < vector.size(); ++j) {

                double distance = std::fabs(vector.at(i) - vector.at(j));

                distanceMatrix[i][j] = distance;

                distanceMatrix[j][i] = distance;

            }

        }

        for (QVector<double> &distanceVector: distanceMatrix)
            std::sort(distanceVector.begin(), distanceVector.end());

        MatrixOperations::transpose_inplace(distanceMatrix);

        QVector<double> minVector;

        QVector<double> maxVector;

        for (int i = 1; i < distanceMatrix.size(); ++i) {

            QPair<double, double> minAndMax = MathDescriptives::minimumAndMaximum(distanceMatrix.at(i));

            minVector << minAndMax.first;

            maxVector << minAndMax.second;

        }

        double minRadius = MathDescriptives::minimum(minVector);

        double maxRadius = MathDescriptives::maximum(maxVector);

        QVector<double> radiusVector;

        for (double radius = minRadius; radius <= maxRadius; radius = radius + (maxRadius - minRadius) / static_cast<double>(distanceMatrix.size()))
            radiusVector << radius;

        MatrixOperations::transpose_inplace(distanceMatrix);

        QVector<double> rofScores;

        for (int i = 0; i < distanceMatrix.size(); ++i) {

            int count = 0;

            QVector<int> countVector(radiusVector.size(), distanceMatrix.size());

            const QVector<double> &currentDistanceVector(distanceMatrix.at(i));

            int currentIndexInRadiusVector = 0;

            int currentIndexInDistanceVector = 0;

            while ((currentIndexInDistanceVector < currentDistanceVector.size()) && (currentIndexInRadiusVector < radiusVector.size())) {

                if (currentDistanceVector.at(currentIndexInDistanceVector) <= radiusVector.at(currentIndexInRadiusVector)) {

                    ++count;

                    ++currentIndexInDistanceVector;

                    if ((currentIndexInRadiusVector + 1) == radiusVector.size())
                       countVector[currentIndexInRadiusVector] = count;

                } else {

                    countVector[currentIndexInRadiusVector] = count;

                    ++currentIndexInRadiusVector;

                }

            }

            double rofScore = 0.0;

            for (int j = 1; j < countVector.size(); ++j)
                rofScore += ((static_cast<double>(countVector.at(j - 1)) - 1.0) / static_cast<double>(countVector.at(j)));

            rofScores << (rofScore / static_cast<double>(countVector.size()));

        }
/*
        for (int i = 0; i < rofScores.size(); ++i) {

            double pValue = StatFunctions::qromb([&](const double &value) {return StatFunctions::gaussianKernelDensityEstimator(value, rofScores, 0.01);}, 0, rofScores.at(i), 1.0e-3);

            boost::math::normal normalDistribution(0.0, 1.0);

            double zTransformedPValue;

            if (pValue == 0.5)
                zTransformedPValue = std::copysign(0.0, vector.at(i));
            else if (pValue == 0.0)
                zTransformedPValue = (std::signbit(vector.at(i)) ? boost::math::quantile(normalDistribution, pValue + std::numeric_limits<double>::min()) : -boost::math::quantile(normalDistribution, pValue + std::numeric_limits<double>::min()));
            else
                zTransformedPValue = (std::signbit(vector.at(i)) ? boost::math::quantile(normalDistribution, pValue) : -boost::math::quantile(normalDistribution, pValue));

            vector[i] = zTransformedPValue;

        }
*/
        vector = rofScores;

    };

    return lambda;

}

TransformSequenceUtils::ConvertToAbsoluteValues::ConvertToAbsoluteValues(QObject *parent) :
    TransformSequenceUtils::TransformFunction(parent, "Convert values to absolute")
{

}

TransformSequenceUtils::ConvertToAbsoluteValues::~ConvertToAbsoluteValues()
{

}

std::function<void (QVector<double> &)> TransformSequenceUtils::ConvertToAbsoluteValues::func()
{

    auto lambda = [](QVector<double> &vector) {


        for (double &value : vector)
            value = std::fabs(value);

    };

    return lambda;

}

TransformSequenceUtils::TransformFunctionList::TransformFunctionList() :
    QList()
{

    this->append(new VectorScalarAdd());

    this->append(new VectorScalarMin());

    this->append(new ScalarVectorMin());

    this->append(new VectorScalarMul());

    this->append(new VectorScalarDiv());

    this->append(new ScalarVectorDiv());

    this->append(new LogTransformVector());

    this->append(new CenterVectorToMean());

    this->append(new StandardizeVector());

    this->append(new NormalizeVector());

    this->append(new RescaleVectorWithTwoPercentiles());

    this->append(new RankVector());

    this->append(new RecodeVectorEqualOrLess());

    this->append(new RecodeVectorEqualOrGreater());

    this->append(new RecodeVectorUnequal);

    this->append(new DoubleMedianAbsoluteDeviation);

    this->append(new ResolutionBasedOutlierScore);

    this->append(new ConvertToAbsoluteValues);

}

TransformSequenceUtils::TransformFunctionList::~TransformFunctionList()
{

    qDeleteAll(this->begin(), this->end());

}

QStringList TransformSequenceUtils::TransformFunctionList::functionDescriptions() const
{
    QStringList _functionDescriptions;

    for (int i = 0; i < this->size(); ++i)
        _functionDescriptions.append(this->at(i)->functionDescriptionWithoutParametersReplacement());

    return _functionDescriptions;

}

TransformSequenceBuildingBlockWidget::TransformSequenceBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label) :
    QWidget(parent), ui(new Ui::TransformSequenceBuildingBlockWidget), d_rememberSettings(false)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    QCompleter *completer = new QCompleter(d_transformFunctionList.functionDescriptions(), this);

    completer->setCaseSensitivity(Qt::CaseInsensitive);

    completer->setFilterMode(Qt::MatchContains);

    ui->comboBox_transformFunctions->addItems(d_transformFunctionList.functionDescriptions());

    ui->comboBox_transformFunctions->setCompleter(completer);

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/transformSequenceBuildingBlockWidget");

    if (settings.contains("comboBox_transformFunctions/currentIndex"))
        ui->comboBox_transformFunctions->setCurrentIndex(settings.value("currentIndex").toInt());

    settings.endGroup();

    this->connect(ui->comboBox_transformFunctions, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBox_comboBox_transformFunctions_currentIndexChanged(int)));

    this->connect(ui->toolButton_add, SIGNAL(clicked()), this, SLOT(toolButton_add_clicked()));

    this->connect(ui->toolButton_remove, SIGNAL(clicked()), this, SLOT(toolButton_remove_clicked()));

    this->comboBox_comboBox_transformFunctions_currentIndexChanged(ui->comboBox_transformFunctions->currentIndex());

}

TransformSequenceBuildingBlockWidget::~TransformSequenceBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/transformSequenceBuildingBlockWidget");

        settings.setValue("comboBox_transformFunctions/currentIndex", ui->comboBox_transformFunctions->currentIndex());

        settings.endGroup();

    }

    delete ui;

}

QList<std::function<void (QVector<double> &)> > TransformSequenceBuildingBlockWidget::formatedTransformFunctionList() const
{

    return d_formatedTransformFunctionList;

}

QStringList TransformSequenceBuildingBlockWidget::functionDescriptionsWithParametersReplacement() const
{

    QStringList _functionDescriptionsWithParametersReplacement;

    for (int i = 0; i < ui->tableWidget_transformFunctions->rowCount(); ++i)
        _functionDescriptionsWithParametersReplacement.append(ui->tableWidget_transformFunctions->item(i, 1)->text());

    return _functionDescriptionsWithParametersReplacement;

}

void TransformSequenceBuildingBlockWidget::comboBox_comboBox_transformFunctions_currentIndexChanged(int index)
{

    for (int i = ui->tableWidget_parameters->rowCount() - 1; i >= 0; --i)
        ui->tableWidget_parameters->removeRow(i);

    TransformSequenceUtils::TransformFunction *transformFunction = d_transformFunctionList.at(index);

    ui->tableWidget_parameters->setRowCount(transformFunction->numberOfParameters());

    for (int i = 0; i < transformFunction->numberOfParameters(); ++i) {

        ui->tableWidget_parameters->setItem(i, 0, new QTableWidgetItem(transformFunction->parameterDescription(i)));

        QLineEdit *lineEdit = new QLineEdit;

        lineEdit->setValidator(transformFunction->parameterValidator(i));

        QTextStream out(stdout);

        this->connect(lineEdit, &QLineEdit::textChanged, [=](const QString &text) {transformFunction->parameterValueChanged(i, text);});

        ui->tableWidget_parameters->setCellWidget(i, 1, lineEdit);

    }

    ui->tableWidget_parameters->resizeColumnToContents(0);

    ui->tableWidget_parameters->resizeColumnToContents(1);

}

void TransformSequenceBuildingBlockWidget::toolButton_add_clicked()
{
    int newRowIndex = ui->tableWidget_transformFunctions->rowCount();

    ui->tableWidget_transformFunctions->setRowCount(ui->tableWidget_transformFunctions->rowCount() + 1);

    ui->tableWidget_transformFunctions->setItem(newRowIndex, 0, new QTableWidgetItem((newRowIndex == 0) ? "First apply" : "Then apply"));

    ui->tableWidget_transformFunctions->setItem(newRowIndex, 1, new QTableWidgetItem(d_transformFunctionList.at(ui->comboBox_transformFunctions->currentIndex())->functionDescriptionWithParametersReplacement()));

    ui->tableWidget_transformFunctions->resizeColumnToContents(0);

    ui->tableWidget_transformFunctions->resizeColumnToContents(1);

    d_formatedTransformFunctionList.append(d_transformFunctionList.at(ui->comboBox_transformFunctions->currentIndex())->func());

}

void TransformSequenceBuildingBlockWidget::toolButton_remove_clicked()
{
    QModelIndexList list = ui->tableWidget_transformFunctions->selectionModel()->selectedRows();

    for (int i = list.size() - 1; i >= 0; --i) {

        ui->tableWidget_transformFunctions->removeRow(list.at(i).row());

        d_formatedTransformFunctionList.removeAt(list.at(i).row());

    }

    if (ui->tableWidget_transformFunctions->rowCount() >= 1)
        ui->tableWidget_transformFunctions->item(0, 0)->setText("First apply");

}
