#include "selectfileandplotparametersdialog.h"
#include "ui_selectfileandplotparametersdialog.h"

SelectFileAndPlotParametersDialog::SelectFileAndPlotParametersDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::SelectFileAndPlotParametersDialog)
{

    ui->setupUi(this);

    QSettings settings;

    settings.beginGroup("SelectFileAndPlotParametersDialog");

    if (settings.contains("dpi"))
        ui->spinBox_DPI->setValue(settings.value("dpi").toInt());

    settings.endGroup();

    this->connect(ui->pushButton_browse, SIGNAL(clicked()), this, SLOT(pushButton_browseClicked()));

}

SelectFileAndPlotParametersDialog::~SelectFileAndPlotParametersDialog()
{

    QSettings settings;

    settings.beginGroup("SelectFileAndPlotParametersDialog");

    settings.setValue("dpi", ui->spinBox_DPI->value());

    settings.endGroup();

    delete ui;

}

int SelectFileAndPlotParametersDialog::dpiOfPlot() const
{

    return ui->spinBox_DPI->value();

}

int SelectFileAndPlotParametersDialog::heigthOfPlot() const
{

    return ui->spinBox_height->value();

}

QString SelectFileAndPlotParametersDialog::pathOfPlot() const
{

    return ui->lineEdit_path->text();

}

void SelectFileAndPlotParametersDialog::pushButton_browseClicked()
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - Select file");

    fileDialog.setAcceptMode(QFileDialog::AcceptSave);

    QSettings settings;

    settings.beginGroup("SelectFileAndPlotParametersDialog/fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    fileDialog.setNameFilters({"Project file (*.pdf)", "Portable Network Graphics (*.png)", "Windows Bitmap (*.bmp)", "Joint Photographic Experts Group (*.jpg)", "Portable Bitmap (*.pbm)", "Portable Graymap (*.pgm)", "Portable Pixmap (*.ppm)", "X11 Bitmap (*.xbm)", "X11 Pixmap (*.xpm)"});

    settings.endGroup();

    if (fileDialog.exec()) {

        QSettings settings;

        settings.beginGroup("SelectFileAndPlotParametersDialog/fileDialog");

        settings.setValue("state", fileDialog.saveState());

        settings.setValue("geometry", fileDialog.saveGeometry());

        settings.setValue("lastDirectory", fileDialog.directory().path());

        settings.endGroup();

        ui->lineEdit_path->setText(fileDialog.selectedFiles().first());
    }

}

int SelectFileAndPlotParametersDialog::widthOfPlot() const
{

    return ui->spinBox_width->value();

}

void SelectFileAndPlotParametersDialog::setDPIOfPlot(int dpi)
{

    ui->spinBox_DPI->setValue(dpi);

}

void SelectFileAndPlotParametersDialog::setHeightOfPlot(int height)
{

    ui->spinBox_height->setValue(height);

}

void SelectFileAndPlotParametersDialog::setWidthOfPlot(int width)
{

    ui->spinBox_width->setValue(width);

}
