#include "creategenenetworkworkerclass.h"

CreateGeneNetworkWorkerClass::CreateGeneNetworkWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CreateGeneNetworkWorkerClass::Parameters *>(parameters))
{

}

bool CreateGeneNetworkWorkerClass::checkInputParameters()
{

    if (d_parameters.exportDirectory.isEmpty() || !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no components selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no probes/genes selected");

        return false;

    }

    if (d_parameters.geneSetFiles.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no gene set files selected");

        return false;

    }

    for (const QString &geneSetFile : d_parameters.geneSetFiles) {

        if (!QFile(geneSetFile).exists()) {

            emit appendLogItem(LogListModel::ERROR, "no valid gene set file : \"" + geneSetFile + "\"");

            return false;

        }

    }

    if (d_parameters.minimumSizeOfGeneSet > d_parameters.maximumSizeOfGeneSet) {

        emit appendLogItem(LogListModel::ERROR, "minimum number of genes in a gene set can not be higher than the maximum number of genes");

        return false;

    }

    if (QSet<QString>(d_parameters.selectedItemIdentifiersAndIndexes.first.begin(), d_parameters.selectedItemIdentifiersAndIndexes.first.end()).size() != d_parameters.selectedItemIdentifiersAndIndexes.first.size()) {

        emit appendLogItem(LogListModel::ERROR, "duplicate probe/gene identifiers are not allowed");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.size() < d_parameters.minimumSizeOfGeneSet * 2) {

        emit appendLogItem(LogListModel::ERROR, "number of probe/gene identifiers should be at least a factor 2 higher than the minimum number of genes in a gene set");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.size() < 4) {

        emit appendLogItem(LogListModel::ERROR, "number of probe/gene identifiers should be at least 4");

        return false;

    }

    if (d_parameters.annotationLabelsDefiningSearchableKeys.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "at least one searchable key needs to be defined");

        return false;

    }

    QList<QString> annotationValues;

    d_parameters.baseDataset->annotationValues(d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.annotationLabelUsedToMatch, BaseMatrix::switchOrientation(d_parameters.orientation), annotationValues);

    if (annotationValues.size() != QSet<QString>(annotationValues.begin(), annotationValues.end()).size()) {

        emit appendLogItem(LogListModel::ERROR, "detected duplicate identifiers for annotation label used to match between geneset members and components");

        QHash<QString, int> identifierToCounter;

        for (const QString &annotationValue : annotationValues)
            identifierToCounter[annotationValue]++;

        for (const QString &annotationValue : annotationValues) {

            if (identifierToCounter.value(annotationValue) != 1)
                emit appendLogItem(LogListModel::ERROR, annotationValue + " -> " + QString::number(identifierToCounter.value(annotationValue)));

        }

        return false;

    }

    return true;

}

void CreateGeneNetworkWorkerClass::doWork()
{

    emit processStarted("create gene network");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Generate gene network"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of components selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of probes/genes selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    for (const QString &geneSetfile : d_parameters.geneSetFiles)
        emit appendLogItem(LogListModel::PARAMETER, "gene set file selected: \"" + geneSetfile + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "annotation label used to match between members of genesets and the component data: \"" + d_parameters.annotationLabelUsedToMatch + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "test to define enrichment vector for each gene set: \"" + d_parameters.testToDefineEnrichmentVectorForEachGeneSet + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "minimum number of genes in a gene set: " + QString::number(d_parameters.minimumSizeOfGeneSet));

    emit appendLogItem(LogListModel::PARAMETER, "maximum number of genes in a gene set: " + QString::number(d_parameters.maximumSizeOfGeneSet));

    emit appendLogItem(LogListModel::PARAMETER, "distance measurement to use in guilt-by-association analysis: \"" + d_parameters.distanceMeasurementForGuiltByAssociation + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "annotation labels defining searchable keys = " + ConvertFunctions::toString(d_parameters.annotationLabelsDefiningSearchableKeys));

    emit appendLogItem(LogListModel::PARAMETER, "permutation mode: " + (d_parameters.performPermutations ? QString("enabled") : QString("disabled")));

    if (d_parameters.performPermutations)
        emit appendLogItem(LogListModel::PARAMETER, "number of permutation rounds: " + QString::number(d_parameters.numberOfPermutationRounds));

    emit appendLogItem(LogListModel::PARAMETER, "output format: \"" + d_parameters.outputFormat + "\"");

    if (!d_parameters.exportDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    emit startProgress(0, "Constructing probe/gene identifier to index mapping", 0, d_parameters.selectedItemIdentifiersAndIndexes.second.size());

    for (int i = 0; i < d_parameters.selectedItemIdentifiersAndIndexes.second.size(); ++i) {

        QString geneIdentifier = d_parameters.baseDataset->annotationValue(d_parameters.selectedItemIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelUsedToMatch, BaseMatrix::switchOrientation(d_parameters.orientation)).toString();

        d_geneIdentifiers << geneIdentifier;

        d_geneIdentifierToIndex.insert(geneIdentifier, i);

        QList<QVariant> annotationValues = d_parameters.baseDataset->annotationValues(d_parameters.selectedItemIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelsDefiningSearchableKeys, BaseMatrix::switchOrientation(d_parameters.orientation));

        QString temp = annotationValues.first().toString();

        QStringList tokenizedAnnotations;

        tokenizedAnnotations << annotationValues.first().toString().remove('"');

        for (int j = 1; j < annotationValues.size(); ++j) {

            temp += " \\\\\\\\\\\\ " + annotationValues.at(j).toString();

            tokenizedAnnotations << annotationValues.at(j).toString().remove('"');
        }

        temp.remove('"');

        d_geneAnnotations.append(temp);

        d_tokenizedGeneAnnotations.append(tokenizedAnnotations);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    if (d_parameters.outputFormat == "Tab-delimited")
        this->writeGeneSearchableKeysToIndex_tabDelimited(d_parameters.exportDirectory);
    else if (d_parameters.outputFormat == "JSON")
        this->writeGeneSearchableKeysToIndex_json(d_parameters.exportDirectory);

    emit startProgress(0, "Importing gene set files", 0, d_parameters.geneSetFiles.size());

    QFile fileOut;

    if (d_parameters.outputFormat == "Tab-delimited")
        fileOut.setFileName(d_parameters.exportDirectory + "/dataBaseToIndex.txt");
    else if (d_parameters.outputFormat == "JSON")
        fileOut.setFileName(d_parameters.exportDirectory + "/dataBaseToIndex.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        if (d_parameters.outputFormat == "Tab-delimited")
            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/dataBaseToIndex.txt\"");
        else if (d_parameters.outputFormat == "JSON")
            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/dataBaseToIndex.json\"");

        QThread::currentThread()->quit();

        return;

    }

    QTextStream out(&fileOut);

    bool first = true;

    for (int i = 0; i < d_parameters.geneSetFiles.size(); ++i) {

        if (!this->readGeneSetFile(d_parameters.geneSetFiles.at(i))) {

            QThread::currentThread()->quit();

            return;

        }

        if (d_parameters.outputFormat == "Tab-delimited")
            out << d_parameters.geneSetFiles.at(i) << "\t" << i << Qt::endl;
        else if (d_parameters.outputFormat == "JSON") {

            if (first) {

                out << "[" << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << i + 1 << "," << Qt::endl << "\t\t\"label\":\"" << d_parameters.geneSetFiles.at(i) << "\"" << Qt::endl << "\t}";

                first = false;

            } else
                out << "," << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << i + 1 << "," << Qt::endl << "\t\t\"label\":\"" << d_parameters.geneSetFiles.at(i) << "\"" << Qt::endl << "\t}";


        }

        emit updateProgress(0, i + 1);

    }

    if (d_parameters.outputFormat == "JSON")
        out << Qt::endl << "]";

    fileOut.close();

    emit stopProgress(0);

    emit startProgress(0, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    QVector<QVector<double> > components;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        components << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    std::function<double (const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers)> enrichmentFunction;

    if (d_parameters.testToDefineEnrichmentVectorForEachGeneSet == "Student T")
        enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {StudentT<double> studentT(measurements, itemSampleCodedIdentifiers); return studentT.zTransformedPValue();};
    else if (d_parameters.testToDefineEnrichmentVectorForEachGeneSet == "Welch T")
        enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {WelchT<double> welchT(measurements, itemSampleCodedIdentifiers); return welchT.zTransformedPValue();};
    else if (d_parameters.testToDefineEnrichmentVectorForEachGeneSet == "Mann-Whitney U")
        enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {MannWhitneyU<double> mannWhitneyU(measurements, itemSampleCodedIdentifiers); return mannWhitneyU.zTransformedPValue();};
    else if (d_parameters.testToDefineEnrichmentVectorForEachGeneSet == "Kolmogorov-Smirnov Z")
        enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {KolmogorovsmirnovZ<double> kolmogorovsmirnovZ(measurements, itemSampleCodedIdentifiers); return kolmogorovsmirnovZ.zTransformedPValue();};
    else if (d_parameters.testToDefineEnrichmentVectorForEachGeneSet == "Kruskal-Wallis chi-squared")
        enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {KruskalWallisChiSquared<double> kruskalWallisChiSquared(measurements, itemSampleCodedIdentifiers); return kruskalWallisChiSquared.zTransformedPValue();};
    else if (d_parameters.testToDefineEnrichmentVectorForEachGeneSet == "Mean")
        enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {

            double sum = 0.0;

            int counter = 0;

            for (int i = 0; i < measurements.size(); ++i) {

                if (itemSampleCodedIdentifiers.at(i) == 0) {

                    sum += measurements.at(i);

                    ++counter;
                }

            }

            return sum / static_cast<double>(counter);

        };
    else if (d_parameters.testToDefineEnrichmentVectorForEachGeneSet == "Median") {

        enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {

            QVector<double> subsetOfMeasurements;

            for (int i = 0; i < measurements.size(); ++i) {

                if (itemSampleCodedIdentifiers.at(i) == 0)
                    subsetOfMeasurements << measurements.at(i);

            }

            return MathDescriptives::median(subsetOfMeasurements);

        };

    }

    QThread *mainThreadOfWorkerClass = QThread::currentThread();

    emit startProgress(0, "transpose components", 0, 0);

    QVector<QVector<double> > transposedComponents = MatrixOperations::transpose_multiThreaded(components);

    emit stopProgress(0);

    std::function<QPair<double, double> (const QVector<double> &independentVector, QVector<double> &dependentVector)> distanceFunction;

    if (d_parameters.performPermutations) {

        if (d_parameters.distanceMeasurementForGuiltByAssociation == "Pearson")
            distanceFunction = [&](const QVector<double> &independentVector, QVector<double> &dependentVector) {PearsonR<double> pearsonR(independentVector, dependentVector); return QPair<double, double>(pearsonR.statistic(), 0.0);};
        else if (d_parameters.distanceMeasurementForGuiltByAssociation == "Spearman")
            distanceFunction = [&](const QVector<double> &independentVector, QVector<double> &dependentVector) {SpearmanR<double> spearmanR(independentVector, dependentVector); return QPair<double, double>(spearmanR.statistic(), 0.0);};
        else if (d_parameters.distanceMeasurementForGuiltByAssociation == "Distance R")
            distanceFunction = [&](const QVector<double> &independentVector, QVector<double> &dependentVector) {DistanceCorrelation<double> distanceCorrelation(independentVector, dependentVector); return QPair<double, double>(distanceCorrelation.distanceCorrelationValue(), 0.0);};

    } else {

        if (d_parameters.distanceMeasurementForGuiltByAssociation == "Pearson")
            distanceFunction = [&](const QVector<double> &independentVector, QVector<double> &dependentVector) {PearsonR<double> pearsonR(independentVector, dependentVector); return QPair<double, double>(pearsonR.statistic(), pearsonR.zTransformedPValue());};
        else if (d_parameters.distanceMeasurementForGuiltByAssociation == "Spearman")
            distanceFunction = [&](const QVector<double> &independentVector, QVector<double> &dependentVector) {SpearmanR<double> spearmanR(independentVector, dependentVector); return QPair<double, double>(spearmanR.statistic(), spearmanR.zTransformedPValue());};
        else if (d_parameters.distanceMeasurementForGuiltByAssociation == "Distance R")
            distanceFunction = [&](const QVector<double> &independentVector, QVector<double> &dependentVector) {DistanceCorrelation<double> distanceCorrelation(independentVector, dependentVector); return QPair<double, double>(distanceCorrelation.distanceCorrelationValue(), distanceCorrelation.zTransformedPValue());};

    }

    QHash<int, QVector<QVector<double> > > geneSetSizeToGeneToEmpiricalNullDistribution;

    if (d_parameters.performPermutations) {

        std::random_device rd;

        std::mt19937 g(rd());

        double progressValue = 0.0;

        QMutex mutex;

        auto constructGeneToEmpiricalNullDistribution = [&](const int &sizeOfGeneset) {

            QVector<QVector<double> > geneToEmpiricalNullDistribution;

            geneToEmpiricalNullDistribution.reserve(d_geneIdentifiers.size());

            QVector<unsigned int> itemSampleCodedIdentifiers(d_geneIdentifierToIndex.size(), 1);

            for (int k = 0; k < sizeOfGeneset; ++k)
                itemSampleCodedIdentifiers[k] = 0;

            QVector<QVector<double> > permutationEnrichmentVectors;

            for (int permutationRound = 0; permutationRound < d_parameters.numberOfPermutationRounds; ++permutationRound) {

                mutex.lock();

                std::shuffle(itemSampleCodedIdentifiers.begin(), itemSampleCodedIdentifiers.end(), g);

                mutex.unlock();

                QVector<double> permutationEnrichmentVector;

                for (const QVector<double> &component : components) {

                    permutationEnrichmentVector << enrichmentFunction(component, itemSampleCodedIdentifiers);

                    if (mainThreadOfWorkerClass->isInterruptionRequested())
                        return permutationEnrichmentVectors;

                }

                permutationEnrichmentVectors.append(permutationEnrichmentVector);

                mutex.lock();

                progressValue += 1.0;

                mutex.unlock();
            }

            for (int indexOfGene = 0; indexOfGene < d_geneIdentifiers.size(); ++indexOfGene) {

                QVector<double> empiricalNullDistribution;

                empiricalNullDistribution.reserve(permutationEnrichmentVectors.size());

                const QVector<double> &temp(transposedComponents.at(indexOfGene));

                for (QVector<double> permutationEnrichmentVector : permutationEnrichmentVectors)
                    empiricalNullDistribution << distanceFunction(temp, permutationEnrichmentVector).first;

                if (mainThreadOfWorkerClass->isInterruptionRequested())
                    return geneToEmpiricalNullDistribution;

                geneToEmpiricalNullDistribution << empiricalNullDistribution;

                mutex.lock();

                progressValue += 1.0;

                mutex.unlock();

            }


            return geneToEmpiricalNullDistribution;

        };

        QFuture<QVector<QVector<double > > > future1 = QtConcurrent::mapped(FunctionsForMultiThreading::createSequenceOfIndexes(d_parameters.maximumSizeOfGeneSet - d_parameters.minimumSizeOfGeneSet + 1, d_parameters.minimumSizeOfGeneSet, 1), std::function<QVector<QVector<double> >(const int &)>(constructGeneToEmpiricalNullDistribution));

        this->monitorFutureAndWaitForFinish(future1, 0, "Constructing empirical null distributions", progressValue, 0.0, (d_parameters.maximumSizeOfGeneSet - d_parameters.minimumSizeOfGeneSet + 1) * (d_geneIdentifiers.size() + d_parameters.numberOfPermutationRounds));

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        for (int j = 0; j < future1.resultCount(); ++j)
            geneSetSizeToGeneToEmpiricalNullDistribution[j + d_parameters.minimumSizeOfGeneSet] = future1.resultAt(j);

    }

    emit startProgress(0, "Processing gene set databases", 0, d_parameters.geneSetFiles.size());

    for (int i = 0; i < d_parameters.geneSetFiles.size(); ++i) {

        QDir(d_parameters.exportDirectory).mkdir("dataBase_" + QString::number(i + 1));

        QString currentDirectory = d_parameters.exportDirectory + "/" + "dataBase_" + QString::number(i + 1);

        QStringList geneSetIdentifiers = d_databaseToGeneSetIdentifiers.at(i);

        const QStringList &geneSetDescriptions(d_databaseToGeneSetDescriptions.at(i));

        const QVector<QSet<QString> > &geneSetToGeneSetMembers(d_databaseToGeneSetToGeneSetMembers.at(i));

        QVector<QVector<double> > geneSetToDefaultEnrichmentVector;

        emit startProgress(1, "Constructing default enrichment vectors", 0, geneSetIdentifiers.size());

        for (int j = 0; j < geneSetIdentifiers.size(); ++j) {

            QVector<unsigned int> itemSampleCodedIdentifiers(d_geneIdentifierToIndex.size(), 1);

            for (const QString &geneSetMember : geneSetToGeneSetMembers.at(j))
                itemSampleCodedIdentifiers[d_geneIdentifierToIndex.value(geneSetMember)] = 0;

            QVector<double> defaultEnrichmentVector;

            for (const QVector<double> &component : components)
                defaultEnrichmentVector << enrichmentFunction(component, itemSampleCodedIdentifiers);

            geneSetToDefaultEnrichmentVector << defaultEnrichmentVector;

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(1, j + 1);

        }

        emit stopProgress(1);

        emit startProgress(1, "export default enrichment vectors", 0, 0);

        this->writeGeneSetToDefaultEnrichmentVector(i, geneSetToDefaultEnrichmentVector, d_parameters.exportDirectory, geneSetIdentifiers, geneSetDescriptions);

        emit stopProgress(1);


        auto guiltByAssociationFunction = [&](const int &indexOfGene) {

            QString geneIdentifier = d_geneIdentifiers.at(indexOfGene);

            QVector<EnrichmentDataElement> enrichmentDataElements;

            for (int j = 0; j < geneSetIdentifiers.size(); ++j) {

                QVector<double> defaultEnrichmentVector;

                EnrichmentDataElement enrichmentDataElement;

                enrichmentDataElement.geneSetIndex = j;

                enrichmentDataElement.geneIndex = indexOfGene;

                const QSet<QString> &geneSetMembers(geneSetToGeneSetMembers.at(j));

                if (geneSetMembers.contains(geneIdentifier)) {

                    enrichmentDataElement.numberOfGenesInGeneSet = geneSetMembers.size() - 1;

                    enrichmentDataElement.allreadyAssignedToGeneSet = true;

                    QVector<unsigned int> itemSampleCodedIdentifiers(d_geneIdentifierToIndex.size(), 1);

                    for (const QString &geneSetMember : geneSetMembers)
                        itemSampleCodedIdentifiers[d_geneIdentifierToIndex.value(geneSetMember)] = 0;

                    itemSampleCodedIdentifiers[d_geneIdentifierToIndex.value(geneIdentifier)] = 1;

                    for (const QVector<double> &component : components)
                        defaultEnrichmentVector << enrichmentFunction(component, itemSampleCodedIdentifiers);

                } else {

                    enrichmentDataElement.numberOfGenesInGeneSet = geneSetToGeneSetMembers.at(j).size();

                    enrichmentDataElement.allreadyAssignedToGeneSet = false;

                    defaultEnrichmentVector = geneSetToDefaultEnrichmentVector.at(j);

                }

                const QVector<double> &transposedComponentValues(transposedComponents.at(indexOfGene));

                QPair<double, double> statisticAndZTransformedPValue = distanceFunction(transposedComponentValues, defaultEnrichmentVector);

                enrichmentDataElement.associationStatistic = statisticAndZTransformedPValue.first;

                if (d_parameters.performPermutations) {

                    const QVector<double> &empiricalNullDistribution(geneSetSizeToGeneToEmpiricalNullDistribution.value(geneSetMembers.size()).at(indexOfGene));

                    double permutationPValue;

                    double kernelWidth = 0.05;

                    if (d_parameters.distanceMeasurementForGuiltByAssociation == "Distance R")
                        kernelWidth /= 2.0;

                    if (statisticAndZTransformedPValue.first < 0.0) {

                        permutationPValue = StatFunctions::qromb([&](const double &value) {return StatFunctions::gaussianKernelDensityEstimator(value, empiricalNullDistribution, kernelWidth);}, -1.0, statisticAndZTransformedPValue.first, 1.0e5);

                        if (permutationPValue > 0.5)
                            permutationPValue = StatFunctions::qromb([&](const double &value) {return StatFunctions::gaussianKernelDensityEstimator(value, empiricalNullDistribution, kernelWidth);}, statisticAndZTransformedPValue.first, 1.0, 1.0e-5);

                    } else if (statisticAndZTransformedPValue.first > 0.0) {

                        permutationPValue = StatFunctions::qromb([&](const double &value) {return StatFunctions::gaussianKernelDensityEstimator(value, empiricalNullDistribution, kernelWidth);}, statisticAndZTransformedPValue.first, 1.0, 1.0e-5);

                        if (permutationPValue > 0.5)
                            permutationPValue = StatFunctions::qromb([&](const double &value) {return StatFunctions::gaussianKernelDensityEstimator(value, empiricalNullDistribution, kernelWidth);}, -1.0, statisticAndZTransformedPValue.first, 1.0e-5);

                    } else
                        permutationPValue = 0.5;

                    if (d_parameters.distanceMeasurementForGuiltByAssociation == "Distance R")
                        permutationPValue /= 2.0;

                    boost::math::normal normalDistribution(0.0, 1.0);

                    if (permutationPValue >= 0.5)
                        enrichmentDataElement.zTransformedPValue = std::copysign(0.0, statisticAndZTransformedPValue.first);
                    else if (permutationPValue <= 0.0)
                        enrichmentDataElement.zTransformedPValue = (std::signbit(statisticAndZTransformedPValue.first) ? boost::math::quantile(normalDistribution, std::numeric_limits<double>::min()) : -boost::math::quantile(normalDistribution, std::numeric_limits<double>::min()));
                    else
                        enrichmentDataElement.zTransformedPValue = (std::signbit(statisticAndZTransformedPValue.first) ? boost::math::quantile(normalDistribution, permutationPValue) : -boost::math::quantile(normalDistribution, permutationPValue));

                } else
                    enrichmentDataElement.zTransformedPValue = statisticAndZTransformedPValue.second;

                enrichmentDataElements.append(enrichmentDataElement);

                if (mainThreadOfWorkerClass->isInterruptionRequested())
                    return enrichmentDataElements;

            }

            QVector<EnrichmentDataElement *> enrichmentDataElementsPointers;

            for (int j = 0; j < enrichmentDataElements.size(); ++j)
                enrichmentDataElementsPointers.append(&enrichmentDataElements[j]);

            std::sort(enrichmentDataElementsPointers.begin(), enrichmentDataElementsPointers.end(), [](EnrichmentDataElement *element1, EnrichmentDataElement *element2){ return std::fabs(element1->zTransformedPValue) > std::fabs(element2->zTransformedPValue);});


            if (d_parameters.outputFormat == "Tab-delimited")
                this->writePredictionsForIndividualGeneToFile_tabDelimited(indexOfGene, geneSetIdentifiers, geneSetDescriptions, enrichmentDataElementsPointers, currentDirectory);
            else if (d_parameters.outputFormat == "JSON")
                this->writePredictionsForIndividualGeneToFile_json(indexOfGene, geneSetIdentifiers, geneSetDescriptions, enrichmentDataElementsPointers, currentDirectory);

            return enrichmentDataElements;

        };

        QFuture<QVector<EnrichmentDataElement> > future2 = QtConcurrent::mapped(FunctionsForMultiThreading::createSequenceOfIndexes(d_geneIdentifiers.size(), 0, 1), std::function<QVector<EnrichmentDataElement>(const int &)>(guiltByAssociationFunction));

        this->monitorFutureAndWaitForFinish(future2, 1, "Determining guilt-by-association");

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        QVector<QVector<EnrichmentDataElement> > results;

        for (int j = 0; j < future2.resultCount(); ++j)
            results << future2.resultAt(j);

        this->writeMatrixWithZTransformedPValues(i, results, d_parameters.exportDirectory, geneSetIdentifiers, geneSetDescriptions);

        this->writeMatrixWithAssociationStatistic(i, results, d_parameters.exportDirectory, geneSetIdentifiers, geneSetDescriptions);

        MatrixOperations::transpose_inplace_multiThreaded(results);

        QFile fileOut2;

        if (d_parameters.outputFormat == "Tab-delimited")
            fileOut2.setFileName(currentDirectory + "/geneSetToIndex.txt");
        else if (d_parameters.outputFormat == "JSON")
            fileOut2.setFileName(currentDirectory + "/geneSetToIndex.json");

        if (!fileOut2.open(QIODevice::WriteOnly | QIODevice::Text)) {

            if (d_parameters.outputFormat == "Tab-delimited")
                emit appendLogItem(LogListModel::ERROR, "could not open file \"" + currentDirectory + "/geneSetToIndex.txt\"");
            else if (d_parameters.outputFormat == "JSON")
                emit appendLogItem(LogListModel::ERROR, "could not open file \"" + currentDirectory + "/geneSetToIndex.json\"");

            QThread::currentThread()->quit();

            return;

        }

        QTextStream out2(&fileOut2);

        bool first = true;

        for (int j = 0; j < results.size(); ++j) {

            if (d_parameters.outputFormat == "Tab-delimited")
                out2 << geneSetIdentifiers.at(j) << " \\\\\\ " << geneSetDescriptions.at(j) << "\t" << j << Qt::endl;
            else if (d_parameters.outputFormat == "JSON") {

                if (first) {

                    out2 << "[" << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << j + 1 << "," << Qt::endl << "\t\t\"label\":\"" << geneSetIdentifiers.at(j) << " \\\\\\\\\\\\ " << geneSetDescriptions.at(j) << "\"" << Qt::endl << "\t}";

                    first = false;

                } else
                    out2 << "," << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << j + 1 << "," << Qt::endl << "\t\t\"label\":\"" << geneSetIdentifiers.at(j) << " \\\\\\\\\\\\ " << geneSetDescriptions.at(j) << "\"" << Qt::endl << "\t}";

            }

            QVector<EnrichmentDataElement> &result(results[j]);

            std::sort(result.begin(), result.end(), [](EnrichmentDataElement element1, EnrichmentDataElement element2){ return std::fabs(element1.zTransformedPValue) > std::fabs(element2.zTransformedPValue);});

            if (d_parameters.outputFormat == "Tab-delimited")
                this->writePredictionsForIndividualGeneSetToFile_tabDelimited(j, result, currentDirectory);
            else if (d_parameters.outputFormat == "JSON")
                this->writePredictionsForIndividualGeneSetToFile_json(j, result, currentDirectory);

        }

        if (d_parameters.outputFormat == "JSON")
            out2 << Qt::endl << "]";

        fileOut2.close();

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);



    QThread::currentThread()->quit();

}

bool CreateGeneNetworkWorkerClass::readGeneSetFile(const QString &geneSetFile)
{

    d_file.setFileName(geneSetFile);

    if (!d_file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file: \"" + geneSetFile + "\"");

        return false;

    }

    emit startProgress(1, "Importing gene set file \"" + geneSetFile + "\"", 1, d_file.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    int numberOfLinesRead = 0;

    QSet<QString> geneSetIdentifiersSet;

    QStringList geneSetIdentifiers;

    QStringList geneSetDescriptions;

    QVector<QSet<QString> > geneSetToGeneSetMembers;

    while (!d_file.atEnd()) {

        QStringList tokens = QString(d_file.readLine()).remove(QRegExp("[\r\n]")).split("\t", Qt::SkipEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() < 3) {

            emit appendLogItem(LogListModel::ERROR, "error at line number " + QString::number(numberOfLinesRead) + " in gene set file \"" + geneSetFile + "\", not enough tokens read");

            d_file.close();

            return false;

        }

        QString geneSetIdentifier = tokens.at(0);

        QString geneSetDescription = tokens.at(1);

        QSet<QString> geneSetMembers;

        for (int i = 2; i < tokens.size(); ++i) {

            if (d_geneIdentifierToIndex.contains(tokens.at(i))) {

                if (!geneSetMembers.contains(tokens.at(i)))
                    geneSetMembers.insert(tokens.at(i));
                else
                    emit appendLogItem(LogListModel::WARNING, "check gene set definitions in file \"" + geneSetFile + "\" at line " + QString::number(numberOfLinesRead) + ", gene set with identifier \"" + geneSetIdentifier + "\" conatins duplicate members with identifier \"" + tokens.at(i) + "\"");

            }

        }

        if ((geneSetMembers.size() >= d_parameters.minimumSizeOfGeneSet) && (geneSetMembers.size() <= d_parameters.maximumSizeOfGeneSet)) {

            if (geneSetIdentifiersSet.contains(geneSetIdentifier)) {

                emit appendLogItem(LogListModel::WARNING, "duplicate gene set identifier \"" + geneSetIdentifier + "\" in gene set file \"" + geneSetFile + "\"");

                d_file.close();

                return false;

            }

            geneSetIdentifiers.append(geneSetIdentifier);

            geneSetDescriptions.append(geneSetDescription.replace('/', "\/"));

            geneSetToGeneSetMembers.append(geneSetMembers);

        } else
            emit appendLogItem(LogListModel::WARNING, "exluded gene set from gene set file \"" + geneSetFile + "\", identifier \"" + geneSetIdentifier + "\", description \"" + geneSetDescription + "\", number of members " + QString::number(geneSetMembers.size()) + ", members " + ConvertFunctions::toString(geneSetMembers.values()));

        if (QThread::currentThread()->isInterruptionRequested()) {

            d_file.close();

            return false;

        }

    }

    d_file.close();

    d_databaseToGeneSetIdentifiers.append(geneSetIdentifiers);

    d_databaseToGeneSetDescriptions.append(geneSetDescriptions);

    d_databaseToGeneSetToGeneSetMembers.append(geneSetToGeneSetMembers);

    emit stopProgress(1);

    emit appendLogItem(LogListModel::MESSAGE, "imported " + QString::number(geneSetIdentifiers.size()) + " gene sets from \"" + geneSetFile + "\"");

    return true;

}

void CreateGeneNetworkWorkerClass::updateFileProgress()
{

    emit updateProgress(1, d_file.pos());

}

bool CreateGeneNetworkWorkerClass::writePredictionsForIndividualGeneToFile_tabDelimited(int geneIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/predictionsGene_" + QString::number(geneIndex + 1) + ".txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/predictionsGene_" + QString::number(geneIndex + 1) + ".txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "Gene set identifier\t" << "Gene set description\t" << "Number of genes in gene set\t" << "Allready assigned to gene set\t" << "Correlation\t" << "Z-score" << Qt::endl;

    for (EnrichmentDataElement *enrichmentDataElement : enrichmentDataElements)
        out << geneSetIdentifiers.at(enrichmentDataElement->geneSetIndex) << "\t" << geneSetDescriptions.at(enrichmentDataElement->geneSetIndex) << "\t" << enrichmentDataElement->numberOfGenesInGeneSet << "\t" << enrichmentDataElement->allreadyAssignedToGeneSet << "\t" << enrichmentDataElement->associationStatistic << "\t" << enrichmentDataElement->zTransformedPValue << Qt::endl;

    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writePredictionsForIndividualGeneToFile_json(int geneIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/predictionsGene_" + QString::number(geneIndex + 1) + ".json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/predictionsGene_" + QString::number(geneIndex + 1) + ".json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (EnrichmentDataElement *enrichmentDataElement : enrichmentDataElements) {

        if (first) {

            out << "\t{\"id\":\"" << geneSetIdentifiers.at(enrichmentDataElement->geneSetIndex) << "\",\"description\":\"" << geneSetDescriptions.at(enrichmentDataElement->geneSetIndex) << "\",\"nGenes\":" << enrichmentDataElement->numberOfGenesInGeneSet << ",\"allreadyAssigned\":" << enrichmentDataElement->allreadyAssignedToGeneSet << ",\"statistic\":" << enrichmentDataElement->associationStatistic << ",\"zTransformedPValue\":" << std::fabs(enrichmentDataElement->zTransformedPValue) << ",\"geneSetIndex\":" << enrichmentDataElement->geneSetIndex + 1 << ",\"geneIndex\":" << enrichmentDataElement->geneIndex + 1 << "}";

            first = false;

        } else
            out << "," << Qt::endl << "\t{\"id\":\"" << geneSetIdentifiers.at(enrichmentDataElement->geneSetIndex) << "\",\"description\":\"" << geneSetDescriptions.at(enrichmentDataElement->geneSetIndex) << "\",\"nGenes\":" << enrichmentDataElement->numberOfGenesInGeneSet << ",\"allreadyAssigned\":" << enrichmentDataElement->allreadyAssignedToGeneSet << ",\"statistic\":" << enrichmentDataElement->associationStatistic << ",\"zTransformedPValue\":" << std::fabs(enrichmentDataElement->zTransformedPValue) << ",\"geneSetIndex\":" << enrichmentDataElement->geneSetIndex + 1 << ",\"geneIndex\":" << enrichmentDataElement->geneIndex + 1 << "}";

    }

    out << Qt::endl << "]}";

    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writePredictionsForIndividualGeneSetToFile_tabDelimited(int geneSetIndex, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/predictionsGeneSet_" + QString::number(geneSetIndex + 1) + ".txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/predictionsGeneSet_" + QString::number(geneSetIndex + 1) + ".txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "Gene identifier(s)" << "\tLink identifier" << "\tAllready assigned to gene set" << "\tCorrelation" << "\tZ-score" << Qt::endl;

    for (EnrichmentDataElement enrichmentDataElement : enrichmentDataElements)
        out << d_geneAnnotations.at(enrichmentDataElement.geneIndex) << "\t" << d_geneIdentifiers.at(enrichmentDataElement.geneIndex) << "\t" << enrichmentDataElement.allreadyAssignedToGeneSet << "\t" << enrichmentDataElement.associationStatistic << "\t" << enrichmentDataElement.zTransformedPValue << Qt::endl;

    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writePredictionsForIndividualGeneSetToFile_json(int geneSetIndex, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/predictionsGeneSet_" + QString::number(geneSetIndex + 1) + ".json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/predictionsGeneSet_" + QString::number(geneSetIndex + 1) + ".json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (EnrichmentDataElement enrichmentDataElement : enrichmentDataElements) {

        if (first) {

            out << "\t{\"id\":\"" << d_geneAnnotations.at(enrichmentDataElement.geneIndex) << "\",\"linkID\":" << d_geneIdentifiers.at(enrichmentDataElement.geneIndex) << ",\"allreadyAssigned\":" << enrichmentDataElement.allreadyAssignedToGeneSet << ",\"statistic\":" << enrichmentDataElement.associationStatistic << ",\"zTransformedPValue\":" << std::fabs(enrichmentDataElement.zTransformedPValue) << ",\"geneSetIndex\":" << enrichmentDataElement.geneSetIndex + 1 << ",\"geneIndex\":" << enrichmentDataElement.geneIndex + 1 << "}";

            first = false;

        } else
            out << "," << Qt::endl << "\t{\"id\":\"" << d_geneAnnotations.at(enrichmentDataElement.geneIndex) << "\",\"linkID\":" << d_geneIdentifiers.at(enrichmentDataElement.geneIndex) << ",\"allreadyAssigned\":" << enrichmentDataElement.allreadyAssignedToGeneSet << ",\"statistic\":" << enrichmentDataElement.associationStatistic << ",\"zTransformedPValue\":" << std::fabs(enrichmentDataElement.zTransformedPValue) << ",\"geneSetIndex\":" << enrichmentDataElement.geneSetIndex + 1 << ",\"geneIndex\":" << enrichmentDataElement.geneIndex + 1 << "}" << Qt::endl;

    }

    out << "]}";

    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writeGeneSearchableKeysToIndex_tabDelimited(const QString &path)
{

    QFile fileOut(path + "/geneToIndex.txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneToIndex.txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    for (int i = 0; i < d_geneAnnotations.size(); ++i)
        out << d_geneAnnotations.at(i) << "\t" << i + 1 << Qt::endl;


    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writeGeneSearchableKeysToIndex_json(const QString &path)
{

    QFile fileOut(path + "/geneToIndex.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneToIndex.json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "[";

    out << Qt::endl << "\t" << "{" << Qt::endl;

    out << "\t\t" << "\"value\":" << 1 << "," << Qt::endl;

    out << "\t\t" << "\"label\":\"";

    out << d_geneAnnotations.first();

    out << "\"" << Qt::endl;

    out << "\t}";

    for (int i = 1; i < d_geneAnnotations.size(); ++i) {

        out << "," << Qt::endl << "\t" << "{" << Qt::endl;

        out << "\t\t" << "\"value\":" << i + 1 << "," << Qt::endl;

        out << "\t\t" << "\"label\":\"";

        out << d_geneAnnotations.at(i);

        out << "\"" << Qt::endl;

        out << "\t}";

    }

    out << Qt::endl << "]";

    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writeMatrixWithZTransformedPValues(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > & enrichmentDataElements, const QString &path, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions)
{

    QFile fileOut(path + "/" + QString::number(databaseIndex + 1) + "_zTransformedPValues.txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/" + QString::number(databaseIndex + 1) + "_zTransformedPValues.txt");

        return false;

    }

    QTextStream out(&fileOut);

    out << d_parameters.geneSetFiles.at(databaseIndex) << Qt::endl;

    out << "nIdentifiers\t" << d_parameters.annotationLabelsDefiningSearchableKeys.size() << Qt::endl;

    out << "nGeneSets\t" << geneSetIdentifiers.size() << Qt::endl;

    for (int i = 0; i < geneSetIdentifiers.size(); ++i)
        out << geneSetIdentifiers.at(i) << "\t" << geneSetDescriptions.at(i) << Qt::endl;

    for (int i = 0; i < enrichmentDataElements.size(); ++i) {

        const QStringList &tokenizedAnnotations(d_tokenizedGeneAnnotations.at(i));

        out << tokenizedAnnotations.first();

        for (int j = 1; j < tokenizedAnnotations.size(); ++j)
            out << "\t" << tokenizedAnnotations.at(j);

        const QVector<EnrichmentDataElement> &temp(enrichmentDataElements.at(i));

        for (int j = 0; j < temp.size(); ++j)
            out << "\t" << temp.at(j).zTransformedPValue;

        out << Qt::endl;
    }

    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writeMatrixWithAssociationStatistic(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > &enrichmentDataElements, const QString &path, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions)
{

    QFile fileOut(path + "/" + QString::number(databaseIndex + 1) + "_associationStatistics.txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/" + QString::number(databaseIndex + 1) + "_associationStatistics.txt");

        return false;

    }

    QTextStream out(&fileOut);

    out << d_parameters.geneSetFiles.at(databaseIndex) << Qt::endl;

    out << "nIdentifiers\t" << d_parameters.annotationLabelsDefiningSearchableKeys.size() << Qt::endl;

    out << "nGeneSets\t" << geneSetIdentifiers.size() << Qt::endl;

    for (int i = 0; i < geneSetIdentifiers.size(); ++i)
        out << geneSetIdentifiers.at(i) << "\t" << geneSetDescriptions.at(i) << Qt::endl;

    for (int i = 0; i < enrichmentDataElements.size(); ++i) {

        const QStringList &tokenizedAnnotations(d_tokenizedGeneAnnotations.at(i));

        out << tokenizedAnnotations.first();

        for (int j = 1; j < tokenizedAnnotations.size(); ++j)
            out << "\t" << tokenizedAnnotations.at(j);

        const QVector<EnrichmentDataElement> &temp(enrichmentDataElements.at(i));

        for (int j = 0; j < temp.size(); ++j)
            out << "\t" << temp.at(j).associationStatistic;

        out << Qt::endl;
    }

    fileOut.close();

    return true;

}

bool CreateGeneNetworkWorkerClass::writeGeneSetToDefaultEnrichmentVector(int databaseIndex, const QVector<QVector<double> > &geneSetToDefaultEnrichmentVector, const QString &path, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions)
{

    QFile fileOut(path + "/" + QString::number(databaseIndex + 1) + "_geneSetToDefaultEnrichmentVector.txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/" + QString::number(databaseIndex + 1) + "_geneSetToDefaultEnrichmentVector.txt");

        return false;

    }

    QTextStream out(&fileOut);

    out << d_parameters.geneSetFiles.at(databaseIndex) << Qt::endl;

    for (int i = 0; i < geneSetToDefaultEnrichmentVector.size(); ++i) {

        out << geneSetIdentifiers.at(i) << "\t" << geneSetDescriptions.at(i);

        const QVector<double> &temp(geneSetToDefaultEnrichmentVector.at(i));

        for (int j = 0; j < temp.size(); ++j)
            out << "\t" << temp.at(j);

        out << Qt::endl;

    }

    fileOut.close();

    return true;

}
