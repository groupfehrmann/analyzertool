#ifndef FISHERSTRENDMETHOD_H
#define FISHERSTRENDMETHOD_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include <cmath>
#include <limits>

#include "boost/math/distributions/chi_squared.hpp"

#include "statistics/basemetaanalysistest.h"

class FishersTrendMethod : public BaseMetaAnalysisTest
{

public:

    FishersTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    ~FishersTrendMethod();

    QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const;

    const QStringList metaAnalysisTestDescriptiveLabels() const;

protected:

    void _reset();

    static QStringList d_baseStatisticalTestDescriptiveLabels;

    static QStringList d_metaAnalysisTestDescriptiveLabels;

    static QString d_nameOfMetaAnalysisTest;

    QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues();

    QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues();

    void calculateStatistic();

    void calculatedPValue();

};

#endif // FISHERSTRENDMETHOD_H
