#ifndef VECTOROPERATIONS_H
#define VECTOROPERATIONS_H

#include <QVector>
#include <QtConcurrent>
#include <QFutureSynchronizer>
#include <QPair>
#include <QThread>

#ifdef Q_OS_MAC
#include <Accelerate/Accelerate.h>
#endif

#include <functional>
#include <cmath>
#include <stdlib.h>
#include <random>
#include <chrono>

#include "math/functionsformultithreading.h"
#include "mathdescriptives.h"

namespace VectorOperations {

    template <typename Functor>
    QVector<double> applyFunctor_elementWise(const QVector<double> &vector, const Functor &functor)
    {

        QVector<double> result(vector.size());

        for (int i = 0; i < vector.size(); ++i)
            result[i] = functor(vector.at(i));

        return result;

    }

    template <typename Functor>
    QVector<double> applyFunctor_elementWise_multiThreaded(const QVector<double> &vector, const Functor &functor)
    {

        return QtConcurrent::blockingMapped<QVector<double> >(vector, std::function<double(const double &)>(functor));

    }

    template <typename Functor>
    QVector<double> &applyFunctor_elementWise_inplace(QVector<double> &vector, const Functor &functor)
    {

        std::for_each(vector.begin(), vector.end(), functor);

        return vector;

    }

    template <typename Functor>
    QVector<double> &applyFunctor_elementWise_inplace_multiThreaded(QVector<double> &vector, const Functor &functor)
    {

        QtConcurrent::blockingMap(vector, functor);

        return vector;

    }

    template <typename Functor>
    QVector<double> applyFunctor_elementWise(const QVector<double> &vector1, const QVector<double> &vector2, const Functor &functor)
    {

        QVector<double> result(vector1.size());

        for (int i = 0; i < vector1.size(); ++i)
            result[i] = functor(vector1.at(i), vector2.at(i));

        return result;

    }

    template <typename Functor>
    QVector<double> applyFunctor_elementWise_multiThreaded(const QVector<double> &vector1, const QVector<double> &vector2, const Functor &functor)
    {

        auto mappedFunction = [&](const int &index) {

            return functor(vector1.at(index), vector2.at(index));

        };

        return QtConcurrent::blockingMapped<QVector<double> >(FunctionsForMultiThreading::createSequenceOfIndexes(vector1.size(), 0, 1), std::function<double(const int &)>(mappedFunction));

    }

    template <typename Functor>
    QVector<double> &applyFunctor_elementWise_inplace(QVector<double> &vector1, const QVector<double> &vector2, const Functor &functor)
    {

        for (int i = 0; i < vector1.size(); ++i)
            functor(vector1[i], vector2.at(i));

        return vector1;

    }

    template <typename Functor>
    QVector<double> &applyFunctor_elementWise_inplace_multiThreaded(QVector<double> &vector1, const QVector<double> &vector2, const Functor &functor)
    {

        auto mappedFunction = [&](const int &index) {

            functor(vector1[index], vector2.at(index));

        };

        QtConcurrent::blockingMapped<void>(FunctionsForMultiThreading::createSequenceOfIndexes(vector1.size(), 0, 1), std::function<double(const int &)>(mappedFunction));

        return vector1;

    }

    QVector<double> vsAdd(const QVector<double> &vector, const double &scalar);

    QVector<double> &vsAdd_inplace(QVector<double> &vector, const double &scalar);

    QVector<double> vsSub(const QVector<double> &vector, const double &scalar);

    QVector<double> &vsSub_inplace(QVector<double> &vector, const double &scalar);

    QVector<double> svSub(const double &scalar, const QVector<double> &vector);

    QVector<double> &svSub_inplace(const double &scalar, QVector<double> &vector);

    QVector<double> vsMul(const QVector<double> &vector, const double &scalar);

    QVector<double> &vsMul_inplace(QVector<double> &vector, const double &scalar);

    QVector<double> vsDiv(const QVector<double> &vector, const double &scalar);

    QVector<double> &vsDiv_inplace(QVector<double> &vector, const double &scalar);

    QVector<double> svDiv(const double &scalar, const QVector<double> &vector);

    QVector<double> &svDiv_inplace(const double &scalar, QVector<double> &vector);


    QVector<double> vsMul_vsMul_vvAdd(const QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2);

    QVector<double> &vsMul_vsMul_vvAdd_inplace(QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2);

    QVector<double> vsMul_vsMul_vvMul(const QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2);

    QVector<double> &vsMul_vsMul_vvMul_inplace(QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2);


    long double dotProduct(const QVector<double> &vector1, const QVector<double> &vector2);

    QVector<double> standardizeVector(const QVector<double> &vector);

    QVector<double> standardizeVector_inplace(QVector<double> &vector);

    QVector<double> normalizeVector(const QVector<double> &vector);

    QVector<double> &normalizeVector_inplace(QVector<double> &vector);

    QVector<double> centerToMeanOfZero(const QVector<double> &vector);

    QVector<double> &centerToMeanOfZero_inplace(QVector<double> &vector);

    QVector<double> scaleVectorToStandardDeviation(const QVector<double> &vector);

    QVector<double> &scaleVectorToStandardDeviation_inplace(QVector<double> &vector);

    QVector<double> scaleVectorToStandardDeviation_x_SqrtOfVectorSizeMinusOne(const QVector<double> &vector);

    QVector<double> &scaleVectorToStandardDeviation_x_SqrtOfVectorSizeMinusOne_inplace(QVector<double> &vector);

    QVector<double> scaleVectorToUnitVariance(const QVector<double> &vector);

    QVector<double> &scaleVectorToUnitVariance_inplace(QVector<double> &vector);

    QVector<double> &fillWithRandomNumbersFromUniformRealDistribution(QVector<double> &vector, const double &minimumValue, const double &maximumValue);

    template <typename T>
    double crank(QVector<T> &vector)
    {
        QVector<T*> _vector(vector.size());

        for (int i = 0; i < vector.size(); ++i)
            _vector[i] = &vector[i];

        std::sort(_vector.begin(), _vector.end(), [](T *value1, T *value2) {return *value1 < *value2;});

        int j = 1;

        int ji;

        int jt;

        int n = _vector.size();

        double t;

        double rank;

        long double s = 0.0;

        while (j < n ) {

            if ((*_vector.at(j)) != (*_vector.at(j - 1))) {

                (*_vector.at(j - 1)) = j;

                ++j;

            } else {

                for (jt = j + 1; jt <= n && (*_vector.at(jt - 1)) == (*_vector.at(j - 1)); ++jt) ;

                rank = 0.5 * (j + jt - 1);

                for (ji = j; ji <= (jt - 1); ++ji)
                    (*_vector[ji - 1]) = rank;

                t = jt - j;

                s += (t * t * t - t);

                j = jt;
            }

        }

        if (j == n)
            (*_vector[n - 1]) = n;

        return s;

    }

    template <typename T>
    double crank_ptrs(QVector<T*> &vector)
    {

        std::sort(vector.begin(), vector.end(), [](T *value1, T *value2) {return *value1 < *value2;});

        int j = 1;

        int ji;

        int jt;

        int n = vector.size();

        double t;

        double rank;

        long double s = 0.0;

        while (j < n ) {

            if ((*vector.at(j)) != (*vector.at(j - 1))) {

                (*vector.at(j - 1)) = j;

                ++j;

            } else {

                for (jt = j + 1; jt <= n && (*vector.at(jt - 1)) == (*vector.at(j - 1)); ++jt) ;

                rank = 0.5 * (j + jt - 1);

                for (ji = j; ji <= (jt - 1); ++ji)
                    (*vector[ji - 1]) = rank;

                t = jt - j;

                s += (t * t * t - t);

                j = jt;
            }

        }

        if (j == n)
            (*vector[n - 1]) = n;

        return s;

    }
}

#endif // VECTOROPERATIONS_H
