#ifndef OPENPLOTFROMFILE_H
#define OPENPLOTFROMFILE_H

#include <QObject>
#include <QApplication>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QChart>
#include <QThread>

#include "charts/iodatastream.h"

using namespace QtCharts;

class OpenPlotFromFile : public QObject
{

    Q_OBJECT

public:

    explicit OpenPlotFromFile(QObject *parent = nullptr, const QString &path = QString(), bool *error = nullptr, QString *errorMessage = nullptr);

    const QSharedPointer<QChart> &chart();

private:

    QSharedPointer<QChart> d_chart;

    bool *d_error;

    QString *d_errorMessage;

    QEventLoop d_eventLoop;

signals:

    void startOpeningPlotFromFile(const QString &path);

    void finishedOpeningPlotFromFile();

private slots:

    void _startOpeningPlotFromFile(const QString &path);

};

#endif // OPENPLOTFROMFILE_H
