#ifndef STATFUNCTIONS_H
#define STATFUNCTIONS_H

#include <QVector>

#include <functional>
#include <cmath>

#include "boost/math/distributions/normal.hpp"

#include "math/interp_1d.h"
#include "math/trapzd.h"

namespace StatFunctions {

    double gaussianDensity(const double &x);

    double gaussianKernelDensityEstimator(const double &x, const QVector<double> &empricalDistribution, const double &bandwidth);

    double qromb(std::function<double (const double &x)> func, const double &a, const double &b, const double &eps);

    double fac(int n);

}

#endif // STATFUNCTIONS_H
