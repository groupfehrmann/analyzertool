#include "annotations.h"

AnnotationsData::AnnotationsData()
{

}

AnnotationsData::AnnotationsData(const AnnotationsData &other) :
    QSharedData(other), d_values(other.d_values)
{

}

AnnotationsData::~AnnotationsData()
{

}

Annotations::Annotations(QObject *parent) :
    QObject(parent), d_data(new AnnotationsData)
{

}

Annotations::Annotations(const Annotations &other) :
    QObject(other.parent()), d_data(other.d_data)
{

}

Annotations::~Annotations()
{

}

bool Annotations::appendLabel(const QString &label)
{

    if (d_data->d_values.contains(label))
        return false;

    d_data->d_values.insert(label, QHash<QString, QVariant>());

    d_data->d_labels.append(label);

    return true;

}

template <>
bool Annotations::appendValue(const QString &identifier, const QString &label, const QVariant &value)
{

    if (d_data->d_values.value(label).contains(identifier))
        return (d_data->d_values[label][identifier] == value);

    this->appendLabel(label);

    d_data->d_identifiers.insert(identifier);

    d_data->d_values[label][identifier] = value;

    return true;

}

bool Annotations::containsLabel(const QString &label) const
{

    return d_data->d_values.contains(label);

}

bool Annotations::containsIdentifier(const QString &identifier) const
{

    return d_data->d_identifiers.contains(identifier);

}

bool Annotations::containsIdentifier(const QString &identifier, const QString &label) const
{

    if (!d_data->d_values.contains(label))
        return false;

    return d_data->d_values.value(label).contains(identifier);

}

const QSet<QString> &Annotations::identifiers() const
{

    return d_data->d_identifiers;

}

template <>
QSet<QString> Annotations::identifiers(const QString &label, const QVariant &value) const {


    return QSet<QString>(d_data->d_values.value(label).keys(value).begin(), d_data->d_values.value(label).keys(value).end());

}

template <>
QSet<QString> Annotations::identifiers(const QString &label, const QList<QVariant> &values) const {

    QSet<QString> _identifiers;

    QHashIterator<QString, QVariant> it(d_data->d_values.value(label));

    while (it.hasNext()) {

        it.next();

        if (values.contains(it.value()))
            _identifiers.insert(it.key());

    }

    return _identifiers;

}

template <>
QSet<QString> Annotations::identifiers(const QSet<QString> &inputIdentifiers, const QString &label, const QVariant &value) const {

    QSet<QString> _identifiers;

    for (const QString &inputIdentifier: inputIdentifiers) {

        if (d_data->d_values.value(label).value(inputIdentifier) == value)
            _identifiers.insert(inputIdentifier);

    }

    return _identifiers;

}

bool Annotations::insertLabel(int index, const QString &label)
{

    if (d_data->d_values.contains(label))
        return false;

    d_data->d_labels.insert(index, label);

    return true;

}

bool Annotations::insertLabels(int index, const QStringList &labels)
{

    if (!std::all_of(labels.begin(), labels.end(), [this](const QString &label){return !this->d_data->d_values.contains(label);}))
        return false;

    for (int i = labels.size() - 1; i >= 0; --i)
        d_data->d_labels.insert(index, labels.at(i));

    return true;

}

const QString &Annotations::label(int index) const
{

    return d_data->d_labels.at(index);

}

const QStringList &Annotations::labels() const
{

    return d_data->d_labels;

}

bool Annotations::prependLabel(const QString &label)
{

    if (d_data->d_values.contains(label))
        return false;

    d_data->d_labels.prepend(label);

    return true;

}

void Annotations::swapLabels(int index1, int index2)
{

    d_data->d_labels.swapItemsAt(index1, index2);

}

const QVariant Annotations::value(const QString &identifier, const QString &label) const
{

    return d_data->d_values.value(label).value(identifier);

}

QList<QVariant> Annotations::values(const QString &identifier, const QStringList &labels) const
{

    QList<QVariant> _values;

    for (const QString &label: labels)
        _values << this->value(identifier, label);

    return _values;

}

QList<QVariant> Annotations::values(const QStringList &identifiers, const QString &label) const
{

    QList<QVariant> _values;

    for (const QString &identifier: identifiers)
        _values << this->value(identifier, label);

    return _values;

}

QList<QString> Annotations::values_string(const QStringList &identifiers, const QString &label) const
{

    QList<QString> _values;

    for (const QString &identifier: identifiers)
        _values << this->value(identifier, label).toString();

    return _values;

}

void Annotations::removeIdentifier(const QString &identifier)
{

    for (QHash<QString, QVariant> &values: d_data->d_values)
        values.remove(identifier);

    d_data->d_identifiers.remove(identifier);

}

void Annotations::removeLabel(const QString &label)
{

    d_data->d_values.remove(label);

    d_data->d_labels.removeAll(label);

}

void Annotations::removeLabels(const QStringList &labels)
{

    for (const QString &label: labels)
        this->removeLabel(label);

}

void Annotations::removeLabels(QList<int> indexes)
{

    std::sort(indexes.begin(), indexes.end(), std::greater<int>());

    for (const int &index: indexes)
        this->removeLabel(this->label(index));

}

void Annotations::removeValue(const QString &identifier, const QString &label)
{

    if (d_data->d_values.contains(label))
        d_data->d_values[label].remove(identifier);

    bool removeIdentifier = true;

    for (const QHash<QString, QVariant> &values: d_data->d_values) {

        if (values.contains(identifier)) {

            removeIdentifier = false;

            break;

        }

    }

    if (removeIdentifier)
        d_data->d_identifiers.remove(identifier);

}

bool Annotations::renameIdentifier(const QString &oldIdentifier, const QString &newIdentifier)
{

    if (!this->containsIdentifier(oldIdentifier) || (oldIdentifier == newIdentifier))
        return true;

    if (this->containsIdentifier(newIdentifier)) {

        for (const QString &label : d_data->d_labels) {

            if (this->value(oldIdentifier, label) != this->value(newIdentifier, label))
                return false;

        }

    } else {

        for (const QString &label : d_data->d_labels) {

            d_data->d_identifiers.insert(newIdentifier);

            d_data->d_values[label][newIdentifier] = this->value(oldIdentifier, label);

        }

    }

    this->removeIdentifier(oldIdentifier);

    return true;

}

bool Annotations::setLabel(int index, const QString &label)
{

    if (d_data->d_values.contains(label))
        return false;

    d_data->d_values.insert(label, d_data->d_values.take(d_data->d_labels.at(index)));

    d_data->d_labels[index] = label;

    return true;
}

Annotations &Annotations::operator=(const Annotations &other)
{

    if (this != &other)
        d_data.operator =(other.d_data);

    return *this;

}

QDataStream& operator<<(QDataStream& out, const Annotations &annotations)
{

    out << QString("_Annotations_begin#");

    out << QString("d_labels") << annotations.d_data->d_labels;

    out << QString("d_identifiers") << annotations.d_data->d_identifiers;

    out << QString("d_values") << annotations.d_data->d_values;

    out << QString("_Annotations_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, Annotations &annotations)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Annotations_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_labels"))
            in >> annotations.d_data->d_labels;
        else if (label == QString("d_identifiers"))
            in >> annotations.d_data->d_identifiers;
        else if (label == QString("d_values"))
            in >> annotations.d_data->d_values;
        else if (label == QString("_Annotations_end#"))
            return in;

    }

    return in;

}
