#ifndef LOGLISTMODEL_H
#define LOGLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include <QList>
#include <QDateTime>
#include <QColor>
#include <QApplication>

class LogSortFilterProxyModel : public QSortFilterProxyModel
{

public:

    LogSortFilterProxyModel(QObject *parent = 0);

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

};

class LogListModel : public QAbstractListModel
{

    Q_OBJECT

public:

    enum Type {

        MESSAGE,

        WARNING,

        ERROR,

        PARAMETER,

        NODATE,

        PROCESSSTARTORSTOP

    };

    struct LogItem {

        QString timeStamp;

        Type type;

        QString text;

    };

    LogListModel(QObject *parent = nullptr);

    QVariant data(QModelIndex const &index, int role = Qt::DisplayRole) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    bool show(int index) const;

private:

    QList<LogItem> d_logItems;

    bool d_showMessage;

    bool d_showWarning;

    bool d_showError;

    void setStandardMessage();

public slots:

    void appendLogItem(LogListModel::Type type, const QString &text);

    void clear();

    void setShowError(bool show);

    void setShowWarning(bool show);

    void setShowMessage(bool show);

};

#endif // LOGLISTMODEL_H
