#include "header.h"

HeaderData::HeaderData()
{

}

HeaderData::HeaderData(const HeaderData &other) :
    QSharedData(other), d_identifiers(other.d_identifiers), d_indexes(other.d_indexes), d_identifierToIndexes(other.d_identifierToIndexes), d_identifierToIndexes_selectedOnly(other.d_identifierToIndexes_selectedOnly), d_indexToIdentifier_selectedOnly(other.d_indexToIdentifier_selectedOnly)
{

}

HeaderData::~HeaderData()
{

}

QString Header::randomIdentifier(int lengthOfRandomIdentifier)
{

    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

    QString randomIdentifier;

    for(int i = 0; i < lengthOfRandomIdentifier; ++i) {

           int index =  QRandomGenerator::global()->generate() % possibleCharacters.length();

           QChar nextChar = possibleCharacters.at(index);

           randomIdentifier.append(nextChar);

       }

       return randomIdentifier;

}

QStringList Header::randomIdentifiers(int lengthOfRandomIdentifier, int numberOfRandomIdentifiers)
{

    QStringList _randomIdentifiers;

    for (int i = 0; i < numberOfRandomIdentifiers; ++i)
        _randomIdentifiers << Header::randomIdentifier(lengthOfRandomIdentifier);

    return _randomIdentifiers;

}

Header::Header(QObject *parent) :
    QObject(parent), d_data(new HeaderData)
{

}

Header::Header(const Header &other) :
    QObject(other.parent()), d_data(other.d_data)
{

}

Header::~Header()
{

}

void Header::append(const QString &identifier)
{

    int index = d_data->d_indexes.size();

    d_data->d_identifiers.append(identifier);

    d_data->d_indexes << index;

    d_data->d_selectionStatus.append(true);

    d_data->d_identifierToIndexes[identifier] << index;

    d_data->d_identifierToIndexes_selectedOnly[identifier] << index;

    d_data->d_indexToIdentifier_selectedOnly.insert(index, identifier);

}

void Header::append(const QStringList &identifiers)
{

    for(const QString &identifier: identifiers)
        this->append(identifier);

}

bool Header::areIndexesValid(const QList<int> &indexes) const
{

    int count = this->count();

    return std::all_of(indexes.begin(), indexes.end(), [count](int index){return index < count;});

}

bool Header::contains(const QString &identifier) const
{

    return d_data->d_identifierToIndexes.contains(identifier);

}

int Header::count(bool selectedOnly) const
{

    return selectedOnly ? d_data->d_indexToIdentifier_selectedOnly.count() : d_data->d_indexes.count();

}

void Header::deselectAll()
{

    std::fill(d_data->d_selectionStatus.begin(), d_data->d_selectionStatus.end(), false);

    this->updateInternalHashesAndMaps();

}

const QString &Header::identifier(int index) const
{

    return d_data->d_identifiers.at(index);

}

const QHash<QString, QList<int> > &Header::identifierToIndexes(bool selectedOnly) const
{

    return selectedOnly ? d_data->d_identifierToIndexes_selectedOnly : d_data->d_identifierToIndexes;

}

QStringList Header::identifiers(bool selectedOnly) const
{

    return selectedOnly ? static_cast<QStringList>(d_data->d_indexToIdentifier_selectedOnly.values()) : d_data->d_identifiers;

}

QStringList Header::identifiers(const QList<int> &indexes) const
{

    QStringList identifiersToReturn;

    identifiersToReturn.reserve(indexes.size());

    for(const int &index: indexes)
        identifiersToReturn << d_data->d_identifiers.at(index);

    return identifiersToReturn;

}

QPair<QStringList, QList<int> > Header::identifiersAndIndexes(bool selectedOnly) const
{

    return selectedOnly ? QPair<QStringList, QList<int> >(d_data->d_indexToIdentifier_selectedOnly.values(), d_data->d_indexToIdentifier_selectedOnly.keys()) : QPair<QStringList, QList<int> >(d_data->d_identifiers, d_data->d_indexes);

}

QList<int> Header::indexes(bool selectedOnly) const
{

    return selectedOnly ? d_data->d_indexToIdentifier_selectedOnly.keys() : d_data->d_indexes;

}

QList<int> Header::indexes(const QString &identifier, bool selectedOnly) const
{

    return selectedOnly ? d_data->d_identifierToIndexes_selectedOnly.value(identifier, QList<int>()) : d_data->d_identifierToIndexes.value(identifier, QList<int>());

}

QList<QList<int> > Header::indexes(const QStringList &identifiers, bool selectedOnly) const
{

    QList<QList<int> > indexesToReturn;

    indexesToReturn.reserve(identifiers.size());

    if (selectedOnly) {

        for(const QString &identifier: identifiers)
            indexesToReturn << d_data->d_identifierToIndexes_selectedOnly.value(identifier, QList<int>());

    } else {

        for(const QString &identifier: identifiers)
            indexesToReturn << d_data->d_identifierToIndexes.value(identifier, QList<int>());

    }

    return indexesToReturn;

}

QList<int> Header::indexesInContinuousList(const QStringList &identifiers, bool selectedOnly) const
{

    QList<int> indexesToReturn;

    if (selectedOnly) {

        for(const QString &identifier: identifiers)
            indexesToReturn << d_data->d_identifierToIndexes_selectedOnly.value(identifier, QList<int>());

    } else {

        for(const QString &identifier: identifiers)
            indexesToReturn << d_data->d_identifierToIndexes.value(identifier, QList<int>());

    }

    return indexesToReturn;

}

void Header::insert(int index, const QString &identifier)
{

    d_data->d_identifiers.insert(index, identifier);

    d_data->d_indexes << d_data->d_indexes.size();

    d_data->d_selectionStatus.insert(index, true);

    this->updateInternalHashesAndMaps();

}

void Header::insert(int index, const QStringList &identifiers)
{

    for (int i = identifiers.size() - 1; i >= 0; --i) {

        d_data->d_identifiers.insert(index, identifiers.at(i));

        d_data->d_indexes << d_data->d_indexes.size();

        d_data->d_selectionStatus.insert(index, true);

    }

    this->updateInternalHashesAndMaps();

}

bool Header::isSelected(int index) const
{

    return d_data->d_selectionStatus.at(index);

}

void Header::prepend(const QString &identifier)
{

    this->insert(0, identifier);

}

void Header::prepend(const QStringList &identifiers)
{

    this->insert(0, identifiers);

}

void Header::remove(int index)
{

    d_data->d_identifiers.removeAt(index);

    d_data->d_indexes.removeLast();

    d_data->d_selectionStatus.removeAt(index);

    this->updateInternalHashesAndMaps();

}

void Header::remove(QList<int> indexes)
{

    std::sort(indexes.begin(), indexes.end(), std::greater<int>());

    for(const int &index: indexes) {

        d_data->d_identifiers.removeAt(index);

        d_data->d_indexes.removeLast();

        d_data->d_selectionStatus.removeAt(index);

    }

    this->updateInternalHashesAndMaps();

}

void Header::remove(const QString &identifier, bool selectedOnly)
{

    this->remove(this->indexes(identifier, selectedOnly));

}

void Header::remove(const QStringList &identifiers, bool selectedOnly)
{

    QList<int> indexesToRemove;

    for(const QString &identifier: identifiers)
        indexesToRemove << this->indexes(identifier, selectedOnly);

    this->remove(indexesToRemove);

}

void Header::renameIdentifier(const QString &oldIdentifier, const QString &newIdentifier, bool selectedOnly)
{

    if (oldIdentifier == newIdentifier)
        return;

    QList<int> indexes = this->indexes(oldIdentifier, selectedOnly);

    for (const int &index: indexes)
        this->setIdentifier(index, newIdentifier);

}

void Header::select(int index)
{

    std::fill(d_data->d_selectionStatus.begin(), d_data->d_selectionStatus.end(), false);

    d_data->d_selectionStatus[index] = true;

    this->updateInternalHashesAndMaps();

}

void Header::select(const QList<int> &indexes)
{

    std::fill(d_data->d_selectionStatus.begin(), d_data->d_selectionStatus.end(), false);

    for (const int &index: indexes)
        d_data->d_selectionStatus[index] = true;

    this->updateInternalHashesAndMaps();

}

void Header::selectAll()
{

    std::fill(d_data->d_selectionStatus.begin(), d_data->d_selectionStatus.end(), true);

    this->updateInternalHashesAndMaps();

}


const QList<bool> &Header::selectionStatuses() const
{

    return d_data->d_selectionStatus;

}

void Header::setIdentifier(int index, const QString &identifier)
{

   QString currentIdentifier = d_data->d_identifiers.at(index);

    d_data->d_identifiers[index] = identifier;

    d_data->d_identifierToIndexes[currentIdentifier].removeOne(index);

    if (d_data->d_identifierToIndexes.value(currentIdentifier).isEmpty())
        d_data->d_identifierToIndexes.remove(currentIdentifier);

     d_data->d_identifierToIndexes[identifier].append(index);

    std::sort(d_data->d_identifierToIndexes[identifier].begin(), d_data->d_identifierToIndexes[identifier].end());

    if (d_data->d_selectionStatus.at(index)) {

        d_data->d_identifierToIndexes_selectedOnly[currentIdentifier].removeOne(index);

        if (d_data->d_identifierToIndexes_selectedOnly.value(currentIdentifier).isEmpty())
            d_data->d_identifierToIndexes_selectedOnly.remove(currentIdentifier);

        d_data->d_identifierToIndexes_selectedOnly[identifier].append(index);

        std::sort(d_data->d_identifierToIndexes_selectedOnly[identifier].begin(), d_data->d_identifierToIndexes_selectedOnly[identifier].end());

        d_data->d_indexToIdentifier_selectedOnly[index] = identifier;

    }

}

void Header::setSelectionStatus(int index, bool selectionStatus)
{
    if (d_data->d_selectionStatus.at(index) == selectionStatus)
        return;

    d_data->d_selectionStatus[index] = selectionStatus;

    const QString &identifier(d_data->d_identifiers.at(index));

    if (selectionStatus) {

        d_data->d_indexToIdentifier_selectedOnly[index] = identifier;

        d_data->d_identifierToIndexes_selectedOnly[identifier] << index;

        std::sort(d_data->d_identifierToIndexes_selectedOnly[identifier].begin(), d_data->d_identifierToIndexes_selectedOnly[identifier].end());

    } else {

        d_data->d_indexToIdentifier_selectedOnly.remove(index);

        d_data->d_identifierToIndexes_selectedOnly[identifier].removeOne(index);

        if (d_data->d_identifierToIndexes_selectedOnly.value(identifier).isEmpty())
            d_data->d_identifierToIndexes_selectedOnly.remove(identifier);

    }

}

void Header::setSelectionStatuses(const QString &identifier, bool selectionStatus)
{

    this->setSelectionStatuses(this->indexes(identifier), selectionStatus);

}

void Header::setSelectionStatuses(const QList<int> &indexes, bool selectionStatus)
{

    for (const int &index: indexes)
        this->setSelectionStatus(index, selectionStatus);

}

void Header::setSelectionStatuses(const QStringList &identifiers, bool selectionStatus)
{

    for (const QString &identifier: identifiers)
        this->setSelectionStatuses(this->indexes(identifier), selectionStatus);

}

void Header::splitIdentifiers(const QString &delimiter, int indexOfTokenToKeep, bool selectedOnly, Qt::SplitBehavior splitBehavior, Qt::CaseSensitivity caseSensitivity)
{

    for (int i = 0; i < d_data->d_identifiers.size(); ++i) {

        if (!selectedOnly || this->isSelected(i)) {

            QStringList tokens = d_data->d_identifiers.at(i).split(delimiter, splitBehavior, caseSensitivity);

            if (indexOfTokenToKeep < tokens.size())
                d_data->d_identifiers[i] = tokens.at(indexOfTokenToKeep);

        }

    }

    this->updateInternalHashesAndMaps();

}

void Header::swap(int index1, int index2, bool update)
{

    d_data->d_identifiers.swapItemsAt(index1, index2);

    d_data->d_selectionStatus.swapItemsAt(index1, index2);

    if (update)
        this->updateInternalHashesAndMaps();

}

void Header::toggleSelection(int index)
{

    this->setSelectionStatus(index, !this->isSelected(index));

}

QSet<QString> Header::uniqueIdentifiers(bool selectedOnly) const
{

    return selectedOnly ? QSet<QString>( d_data->d_identifierToIndexes_selectedOnly.keys().begin(),  d_data->d_identifierToIndexes_selectedOnly.keys().end()) : QSet<QString>( d_data->d_identifierToIndexes.keys().begin(),  d_data->d_identifierToIndexes.keys().end());

}

QSet<QString> Header::uniqueIdentifiers(const QList<int> &indexes) const
{

    QSet<QString> _uniqueIdentifiers;

    for(const int &index: indexes)
        _uniqueIdentifiers.insert(this->identifier(index));

    return _uniqueIdentifiers;

}

void Header::updateInternalHashesAndMaps()
{

    d_data->d_identifierToIndexes.clear();

    d_data->d_identifierToIndexes_selectedOnly.clear();

    d_data->d_indexToIdentifier_selectedOnly.clear();

    for(const int &index: d_data->d_indexes) {

        QString const &identifier(d_data->d_identifiers.at(index));

        d_data->d_identifierToIndexes[identifier] << index;

        if (d_data->d_selectionStatus.at(index)) {

            d_data->d_indexToIdentifier_selectedOnly[index] = identifier;

            d_data->d_identifierToIndexes_selectedOnly[identifier] << index;

        }

    }

}

Header &Header::operator=(const Header &other)
{

    if (this != &other)
        d_data.operator =(other.d_data);

    return *this;

}

QDataStream& operator<<(QDataStream& out, const Header &header)
{

    out << QString("_Header_begin#");

    out << QString("d_identifiers") << header.d_data->d_identifiers;

    out << QString("d_indexes") << header.d_data->d_indexes;

    out << QString("d_selectionStatus") << header.d_data->d_selectionStatus;

    out << QString("d_identifierToIndexes") << header.d_data->d_identifierToIndexes;

    out << QString("d_identifierToIndexes_selectedOnly") << header.d_data->d_identifierToIndexes_selectedOnly;

    out << QString("d_indexToIdentifier_selectedOnly") << header.d_data->d_indexToIdentifier_selectedOnly;

    out << QString("_Header_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, Header &header)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Header_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_identifiers"))
            in >> header.d_data->d_identifiers;
        else if (label == QString("d_indexes"))
            in >> header.d_data->d_indexes;
        else if (label == QString("d_selectionStatus"))
            in >> header.d_data->d_selectionStatus;
        else if (label == QString("d_identifierToIndexes"))
            in >> header.d_data->d_identifierToIndexes;
        else if (label == QString("d_identifierToIndexes_selectedOnly"))
            in >> header.d_data->d_identifierToIndexes_selectedOnly;
        else if (label == QString("d_indexToIdentifier_selectedOnly"))
            in >> header.d_data->d_indexToIdentifier_selectedOnly;
        else if (label == QString("_Header_end#"))
            return in;

    }

    return in;

}
