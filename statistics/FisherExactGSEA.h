#ifndef FISHEREXACTGSEA_H
#define FISHEREXACTGSEA_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "statistics/_fisherexact.h"
#include "math/mathdescriptives.h"
#include "math/matrixoperations.h"
#include "math/statfunctions.h"

#include "boost/math/distributions/hypergeometric.hpp"

template <typename T>
class FisherExactGSEA : public BaseStatisticalTest
{

public:

    FisherExactGSEA(double threshold);

    FisherExactGSEA(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers, double threshold);

    ~FisherExactGSEA();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;

    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    QVector<QVector<unsigned int> > d_structuredMeasurements;

    double d_numberOfCategories;

    double d_numberOfGroups;

    double d_threshold;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation FisherExactGSEA<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool FisherExactGSEA<T>::d_hasEffectSize = true;

template <typename T>
QStringList FisherExactGSEA<T>::d_sampleDescriptiveLabels = {"Count p", "Count n"};

template <typename T>
int FisherExactGSEA<T>::d_maximumNumberOfSamples = 2;

template <typename T>
int FisherExactGSEA<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int FisherExactGSEA<T>::d_minimumNumberOfItemsPerSample = 0;

template <typename T>
QString FisherExactGSEA<T>::d_nameOfEffectSize = QString();

template <typename T>
QString FisherExactGSEA<T>::d_nameOfTransformedEffectSize = QString();

template <typename T>
QString FisherExactGSEA<T>::d_nameOfStatisticalTest = "Fisher's exact";

template <typename T>
bool FisherExactGSEA<T>::d_rankedBased = false;

template <typename T>
QStringList FisherExactGSEA<T>::d_testDescriptiveLabels = {"Total count", "Fisher's Exact Threshold", "Significance (2-tailed)"};

template <typename T>
FisherExactGSEA<T>::FisherExactGSEA(double threshold) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

    d_threshold = threshold;

}

template <typename T>
FisherExactGSEA<T>::FisherExactGSEA(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers, double threshold) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    d_threshold = threshold;

    this->createStructuredMeasurements();

}

template <typename T>
FisherExactGSEA<T>::~FisherExactGSEA()
{

}

template <typename T>
QVector<QVariant> FisherExactGSEA<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    sampleDescriptiveValues << d_structuredMeasurements.at(0).at(0) << d_structuredMeasurements.at(0).at(1);

    sampleDescriptiveValues << d_structuredMeasurements.at(1).at(0) << d_structuredMeasurements.at(1).at(1);

    return sampleDescriptiveValues;

}

template <typename T>
void FisherExactGSEA<T>::calculateStatistic()
{

    d_statistic = 1.0;

}

template <typename T>
void FisherExactGSEA<T>::calculatePValue()
{

    QVector<unsigned int> row1 = d_structuredMeasurements.at(0);

    QVector<unsigned int> row2 = d_structuredMeasurements.at(1);

    boost::math::hypergeometric_distribution<double> distribution(row1.at(0) + row1.at(1), row1.at(0) + row2.at(0), static_cast<unsigned int>(d_measurements.size()));

    double pointProbability = boost::math::pdf<double>(distribution, row1.at(0));

    double sumPValue = pointProbability;

    while ((row1.at(0) > 0) && (row2.at(1) > 1)) {

        row1[0]--;

        row1[1]++;

        row2[0]++;

        row2[1]--;

        double temp = boost::math::pdf<double>(distribution, row1.at(0));

        if (temp <= pointProbability)
            sumPValue += temp;

    }

    row1 = d_structuredMeasurements.at(0);

    row2 = d_structuredMeasurements.at(1);

    while ((row2.at(0) > 0) && (row1.at(1) > 0)) {

        row1[0]++;

        row1[1]--;

        row2[0]--;

        row2[1]++;

        double temp = boost::math::pdf<double>(distribution, row1.at(0));

        if (temp <= pointProbability)
            sumPValue += temp;
    }

    d_pValue = sumPValue;

    if (d_pValue == 0.0) {

        d_pValue = std::numeric_limits<double>::min();

    }

}

template <typename T>
void FisherExactGSEA<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void FisherExactGSEA<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> FisherExactGSEA<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_sampleSize << d_statistic << d_pValue;

    return descriptiveValues;

}

template <typename T>
void FisherExactGSEA<T>::createStructuredMeasurements()
{

    d_numberOfCategories = 2;

    d_numberOfGroups = 2 ;

    d_sampleSize = d_itemSampleCodedIdentifiers.size();

    d_structuredMeasurements = QVector<QVector<unsigned int> >(2, QVector<unsigned int>(2, 0));

    if (d_threshold < 0.0) {

        for (int i = 0; i < d_measurements.size(); ++i) {

            if (d_measurements.at(i) <= d_threshold)
                d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)][0]++;
            else
                d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)][1]++;

        }

    } else {

        for (int i = 0; i < d_measurements.size(); ++i) {

            if (d_measurements.at(i) >= d_threshold)
                d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)][0]++;
            else
                d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)][1]++;

        }

    }

}

template <typename T>
void FisherExactGSEA<T>::_reset()
{

    this->createStructuredMeasurements();

}
#endif // FISHEREXACTGSEA_H
