#include "selectfontwidget.h"
#include "ui_selectfontwidget.h"

SelectFontWidget::SelectFontWidget(QWidget *parent, const QFont &defaultFont) :
    QWidget(parent), ui(new Ui::SelectFontWidget), d_font(defaultFont)
{

    ui->setupUi(this);

    this->connect(ui->toolButton_bold, SIGNAL(toggled(bool)), this, SLOT(toolButton_bold_toggeled(bool)));

    this->connect(ui->toolButton_italic, SIGNAL(toggled(bool)), this, SLOT(toolButton_italic_toggeled(bool)));

    this->connect(ui->fontComboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(setFontFamily(QString)));

    this->connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(setFontPointSize(int)));

    this->setCurrentFont(defaultFont);

}

SelectFontWidget::~SelectFontWidget()
{

    delete ui;

}

QFont SelectFontWidget::currentFont() const
{

    return d_font;

}

void SelectFontWidget::toolButton_bold_toggeled(bool value)
{

    d_font.setBold(value);

    emit currentFontChanged(d_font);

}

void SelectFontWidget::toolButton_italic_toggeled(bool value)
{

    d_font.setItalic(value);

    emit currentFontChanged(d_font);

}

void SelectFontWidget::setCurrentFont(const QFont &font)
{

    d_font = font;

    ui->fontComboBox->setCurrentText(font.family());

    ui->spinBox->setValue(font.pointSize());

    ui->toolButton_bold->setChecked(font.bold());

    ui->toolButton_italic->setChecked(font.italic());

    emit currentFontChanged(d_font);

}

void SelectFontWidget::setFontFamily(const QString &family)
{

    d_font.setFamily(family);

    emit currentFontChanged(d_font);

}

void SelectFontWidget::setFontPointSize(int value)
{

    d_font.setPointSize(value);

    emit currentFontChanged(d_font);


}
