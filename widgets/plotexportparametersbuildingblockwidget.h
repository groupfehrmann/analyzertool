#ifndef PLOTEXPORTPARAMETERSBUILDINGBLOCKWIDGET_H
#define PLOTEXPORTPARAMETERSBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QSettings>
#include <QFileDialog>

#include "dialogs/basedialog.h"

namespace Ui {

class PlotExportParametersBuildingBlockWidget;

}

class PlotExportParametersBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit PlotExportParametersBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString());

    ~PlotExportParametersBuildingBlockWidget();

    int dpiOfPlot() const;

    int heightOfPlot() const;

    QString exportDirectory() const;

    QString typeOfPlot() const;

    int widthOfPlot() const;

private:

    Ui::PlotExportParametersBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

private slots:

    void pushButton_browseClicked();

};

#endif // PLOTEXPORTPARAMETERSBUILDINGBLOCKWIDGET_H
