#ifndef SPLITIDENTIFIERSDIALOG_H
#define SPLITIDENTIFIERSDIALOG_H

#include "dialogs/basedialog.h"
#include "workerclasses/splitidentifiersworkerclass.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/selectdelimiterbuildingblockwidget.h"
#include "widgets/spinboxbuildingblockwidget.h"

class SplitIdentifiersDialog : public BaseDialog
{

    Q_OBJECT

public:

    SplitIdentifiersDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);


    ~SplitIdentifiersDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    bool d_selectedOnly;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_identifiers;

    SelectDelimiterBuildingBlockWidget *d_selectDelimiterBuildingBlockWidget;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_numberOfTokenToKeep;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // SPLITIDENTIFIERSDIALOG_H
