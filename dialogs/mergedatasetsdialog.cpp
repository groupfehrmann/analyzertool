#include "mergedatasetsdialog.h"

MergeDatasetsDialog::MergeDatasetsDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, const ProjectListModel *projectListModel) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_projectListModel(projectListModel)
{

    d_parameters = new MergeDatasetsWorkerClass::Parameters;

    for (QSharedPointer<Project> project : d_projectListModel->projectList()->projects()) {

        for (QSharedPointer<BaseDataset> baseDataset : project->baseDatasets()) {

            d_baseDatasets << baseDataset;

            d_baseDatasetDescriptions << baseDataset->parentProject()->name() + " - " + baseDataset->name();

        }

    }

    for (int i = 0; i < d_projectListModel->rowCount(); ++i) {

        for (int j = 0; j < d_projectListModel->rowCount(d_projectListModel->index(i, 0)); ++j)
            d_selectedOnlyStatusOfBaseDatasets << ((projectListModel->data(d_projectListModel->index(j, 0, d_projectListModel->index(i, 0)), Qt::CheckStateRole).toInt() == Qt::Checked) ? true : false);

    }

    this->setupWidgets();

}

MergeDatasetsDialog::~MergeDatasetsDialog()
{

    delete static_cast<MergeDatasetsWorkerClass::Parameters *>(d_parameters);

}

void MergeDatasetsDialog::initializeWidgets()
{
    d_itemsSelectorBuildingBlockWidget_basedatasets = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_basedatasets", "Select datasets to merge");

    d_itemsSelectorBuildingBlockWidget_basedatasets->setItemsIdentifier(d_baseDatasetDescriptions);

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_basedatasets, 0, 0);

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select merge orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 1, 0);

    d_comboBoxBuildingBlockWidget_mergedDatasetDataType = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_mergedDatasetDataType", "Merged dataset data type", {"short", "unsigned short", "int", "unsigned int", "long long", "unsigned long long", "float", "double", "unsigned char", "string", "bool"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_mergedDatasetDataType, 2, 0);

    d_comboBoxBuildingBlockWidget_mergeRowAnnotations = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_mergeRowAnnotations", "Merge row annotations", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_mergeRowAnnotations, 3, 0);

    d_comboBoxBuildingBlockWidget_mergeColumnAnnotations = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_mergeColumnAnnotations", "Merge column annotations", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_mergeColumnAnnotations, 4, 0);

    d_comboBoxBuildingBlockWidget_createUniqueIdentifiers = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_createUniqueIdentifiers", "Create unique identifiers when there is overlap between datasets", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_createUniqueIdentifiers, 5, 0);

    d_comboBoxBuildingBlockWidget_createUniqueAnnotationLabels = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_createUniqueAnnotationLabels", "Create unique annotation labels when there is overlap between datasets", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_createUniqueAnnotationLabels, 6, 0);

}

void MergeDatasetsDialog::initializeParameters()
{

    MergeDatasetsWorkerClass::Parameters *parameters = static_cast<MergeDatasetsWorkerClass::Parameters *>(d_parameters);

    for (int index : d_itemsSelectorBuildingBlockWidget_basedatasets->selectedItemsIdentifierAndNumber().second) {

        parameters->datasetsToMerge << d_baseDatasets.at(index);

        parameters->selectedOnlyStatusOfDatasetsToMerge << d_selectedOnlyStatusOfBaseDatasets.at(index);

    }

    parameters->mergedDatasetDataType = d_comboBoxBuildingBlockWidget_mergedDatasetDataType->pointerToInternalComboBox()->currentText();

    parameters->orientationToMerge = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->mergeRowAnnotations = (d_comboBoxBuildingBlockWidget_mergeRowAnnotations->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->mergeColumnAnnotations = (d_comboBoxBuildingBlockWidget_mergeColumnAnnotations->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->createUniqueIdentifiers = (d_comboBoxBuildingBlockWidget_createUniqueIdentifiers->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->createUniqueAnnotationLabels = (d_comboBoxBuildingBlockWidget_createUniqueAnnotationLabels->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

}
