#include "textresultitemwidget.h"
#include "ui_textresultitemwidget.h"

TextResultItemWidget::TextResultItemWidget(QWidget *parent, QSharedPointer<TextResultItem> textResultItem) :
    QWidget(parent), ui(new Ui::TextResultItemWidget), d_textResultItem(textResultItem)
{

    ui->setupUi(this);

    ui->textEdit->setText(d_textResultItem->text());

}

TextResultItemWidget::~TextResultItemWidget()
{

    delete ui;

}
