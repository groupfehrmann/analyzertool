#include "selectpenstylewidget.h"
#include "ui_selectpenstylewidget.h"

SelectPenStyleWidget::SelectPenStyleWidget(QWidget *parent, Qt::PenStyle defaultPenStyle) :
    QWidget(parent), ui(new Ui::SelectPenStyleWidget)
{

    ui->setupUi(this);

    ui->comboBox->addItem(this->createPixmap(Qt::NoPen), "No line at all", static_cast<int>(Qt::NoPen));

    ui->comboBox->addItem(this->createPixmap(Qt::SolidLine), "A plain line", static_cast<int>(Qt::SolidLine));

    ui->comboBox->addItem(this->createPixmap(Qt::DashLine), "Dashes separated by a few pixels", static_cast<int>(Qt::DashLine));

    ui->comboBox->addItem(this->createPixmap(Qt::DotLine), "Dots separated by a few pixels", static_cast<int>(Qt::DotLine));

    ui->comboBox->addItem(this->createPixmap(Qt::DashDotLine), "Alternate dots and dashes", static_cast<int>(Qt::DashDotLine));

    ui->comboBox->addItem(this->createPixmap(Qt::DashDotDotLine), "One dash, two dots, one dash, two dots", static_cast<int>(Qt::DashDotDotLine));

    this->connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexOfComboBoxChanged(int)));

    this->setCurrentPenStyle(defaultPenStyle);

}

SelectPenStyleWidget::~SelectPenStyleWidget()
{

    delete ui;

}

QPixmap SelectPenStyleWidget::createPixmap(Qt::PenStyle penStyle)
{

    QPixmap pixmap(150, 15);

    pixmap.fill(Qt::transparent);

    QPen pen;

    pen.setWidth(1);

    pen.setStyle(penStyle);

    QPainter painter(&pixmap);

    painter.setPen(pen);

    painter.drawLine(0, 7, 150, 7);

    return pixmap;

}

void SelectPenStyleWidget::currentIndexOfComboBoxChanged(int index)
{

    Q_UNUSED(index);

    emit currentPenStyleChanged(this->currentPenStyle());

}

Qt::PenStyle SelectPenStyleWidget::currentPenStyle()
{

    return static_cast<Qt::PenStyle>(ui->comboBox->currentData().toInt());

}

void SelectPenStyleWidget::setCurrentPenStyle(Qt::PenStyle penStyle)
{

    switch (penStyle) {

        case Qt::NoPen: ui->comboBox->setCurrentIndex(0); break;

        case Qt::SolidLine: ui->comboBox->setCurrentIndex(1); break;

        case Qt::DashLine: ui->comboBox->setCurrentIndex(2); break;

        case Qt::DotLine: ui->comboBox->setCurrentIndex(3); break;

        case Qt::DashDotLine: ui->comboBox->setCurrentIndex(4); break;

        case Qt::DashDotDotLine: ui->comboBox->setCurrentIndex(5); break;

        default : ui->comboBox->setCurrentIndex(-1);

    }

}
