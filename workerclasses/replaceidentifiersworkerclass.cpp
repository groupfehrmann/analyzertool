#include "replaceidentifiersworkerclass.h"

ReplaceIdentifiersWorkerClass::ReplaceIdentifiersWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<ReplaceIdentifiersWorkerClass::Parameters *>(parameters))
{

}

void ReplaceIdentifiersWorkerClass::doWork()
{
    emit processStarted("Replace identifiers");

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "annotation label defining new identifiers: \"" + d_parameters.annotationLabelDefiningNewIdentifiers + "\"");

    emit matrixDataBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    emit annotationsBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    Header &header(d_parameters.baseDataset->header(d_parameters.orientation));

    Annotations &annotations(d_parameters.baseDataset->annotations(d_parameters.orientation));

    emit startProgress(0, "Replacing identifiers", 0, d_parameters.selectedIdentifiersAndIndexes.second.size());

    for (int i = 0 ; i < d_parameters.selectedIdentifiersAndIndexes.second.size(); ++i) {

        QString newIdentifier = annotations.value(d_parameters.selectedIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningNewIdentifiers).toString();

        header.setIdentifier(d_parameters.selectedIdentifiersAndIndexes.second.at(i), newIdentifier);

        if (!annotations.labels().isEmpty()) {

            for (const QString &label : annotations.labels()) {

                QVariant annotationValue = annotations.value(d_parameters.selectedIdentifiersAndIndexes.first.at(i), label);

                if (!annotationValue.isNull()) {

                    if (!annotations.appendValue(newIdentifier, label, annotationValue))
                        emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + newIdentifier + "\", label \"" + label + "\", value to set \"" + annotationValue.toString() + "\", current value \"" + annotations.value(newIdentifier, label).toString() + "\"");

                }

            }

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

            emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Cleaning up annotations", 0, annotations.identifiers().size());

    int progressCounter = 0;

    for (const QString &identifier : annotations.identifiers().values()) {

        if (!header.contains(identifier))
            annotations.removeIdentifier(identifier);

        emit updateProgress(0, ++progressCounter);

    }

    emit stopProgress(0);

    emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    QThread::currentThread()->quit();

}
