#ifndef STACKEDWIDGET_H
#define STACKEDWIDGET_H

#include <QStackedWidget>
#include <QHash>
#include <QUuid>
#include <QChartView>

#include "base/basedataset.h"
#include "base/baseresultitem.h"
#include "widgets/datasetwidget.h"
#include "widgets/splashwidget.h"
#include "widgets/tableresultitemwidget.h"
#include "widgets/textresultitemwidget.h"
#include "widgets/plotresultitemwidget.h"

class StackedWidget : public QStackedWidget
{

    Q_OBJECT

public:

    StackedWidget(QWidget *parent = nullptr);

    QChartView *currentChartView();

    QTableView *currentTableView();

    QAbstractItemModel *currentTableModel();

    int indexOfSelectedTabInDatasetWidget();

private:

    QHash<QUuid, QWidget *> d_uuidToWidget;

public slots:

    void addDataset(const QSharedPointer<BaseDataset> &baseDataset);

    void addResultItem(const QSharedPointer<BaseResultItem> &resultItem);

    void annotationsBeginChange(const QUuid &uuid, BaseMatrix::Orientation orientation);

    void annotationsEndChange(const QUuid &uuid, BaseMatrix::Orientation orientation);

    void datasetBeginChange(const QUuid &uuid);

    void datasetEndChange(const QUuid &uuid);

    void matrixDataBeginChange(const QUuid &uuid);

    void matrixDataEndChange(const QUuid &uuid);

    void removeWidget(const QUuid &uuid);

    void setCurrentWidget(const QUuid &uuid);

    void showSelectedOnly(const QUuid &uuid, bool showSelectedOnly);

signals:

    void appendLogItem(LogListModel::Type type, const QString &text);

};

#endif // STACKEDWIDGET_H
