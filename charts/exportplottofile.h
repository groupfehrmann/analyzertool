#ifndef EXPORTPLOTTOFILE_H
#define EXPORTPLOTTOFILE_H

#include <QObject>
#include <QApplication>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QChart>
#include <QImage>
#include <QPainter>
#include <QSizeF>
#include <QSize>
#include <QRectF>
#include <QImageWriter>
#include <QPrinter>
#include <QGraphicsScene>
#include <QChartView>

using namespace QtCharts;

class ExportPlotToFile : public QObject
{

    Q_OBJECT

public:

    explicit ExportPlotToFile(QObject *parent = nullptr, QChart *chart = nullptr, const QString &path = QString(), int widthInMillimeters = 400, int heightInMillimeters = 400, int dpi = 200, bool *error = nullptr, QString *errorMessage = nullptr);

    explicit ExportPlotToFile(QObject *parent = nullptr, QChartView *chartView = nullptr, const QString &path = QString(), int widthInMillimeters = 400, int heightInMillimeters = 400, int dpi = 200, bool *error = nullptr, QString *errorMessage = nullptr);

private:

    bool *d_error;

    QString *d_errorMessage;

    QEventLoop d_eventLoop;

signals:

    void startExportingPlotToFile(QChart *chart, const QString &path, int widthInMillimeters, int heightInMilimeters, int dpi);

    void startExportingPlotToFile(QChartView *chartView, const QString &path, int widthInMillimeters, int heightInMilimeters, int dpi);

    void finishedExportingPlotToFile();

private slots:

    void _startExportingPlotToFile(QChart *chart, const QString &path, int widthInMillimeters, int heightInMillimeters, int dpi);

    void _startExportingPlotToFile(QChartView *chartView, const QString &path, int widthInMillimeters, int heightInMillimeters, int dpi);

};

#endif // EXPORTPLOTTOFILE_H
