#ifndef TRANSFORMDATAMATRIXWORKERCLASS_H
#define TRANSFORMDATAMATRIXWORKERCLASS_H

#include <QString>
#include <QStringList>
#include <QPair>
#include <QSharedPointer>

#include "base/basedataset.h"
#include "base/dataset.h"
#include "base/baseworkerclass.h"

class TransformDataMatrixWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        bool processOnlyValidNumbers;

        QList<std::function<void (QVector<double> &)> > formatedTransformFunctionList;

        QStringList functionDescriptionsWithParametersReplacement;

    };

    TransformDataMatrixWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    TransformDataMatrixWorkerClass::Parameters d_parameters;

    template <typename T> void _doWork();

    template <typename T> void processVector(int index);

};

template <typename T>
void TransformDataMatrixWorkerClass::_doWork()
{

    emit startProgress(0, "Transforming data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

    QFutureWatcher<void> futureWatcher;

    this->connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, [=](int progressValue){ emit updateProgress(0, progressValue);});

    futureWatcher.setFuture(QtConcurrent::map(d_parameters.selectedVariableIdentifiersAndIndexes.second, std::bind(&TransformDataMatrixWorkerClass::processVector<T>, this, std::placeholders::_1)));

    futureWatcher.waitForFinished();

    emit stopProgress(0);


}

template <typename T>
void TransformDataMatrixWorkerClass::processVector(int index)
{

    QVector<T*> vectorOfPointers = qSharedPointerCast<Dataset<T> >(d_parameters.baseDataset)->matrix()->vectorOfPointers(index, d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

    QVector<T*> vectorOfPointersToProcess;

    QVector<double> vectorOfValuesToProcess;


    if (d_parameters.processOnlyValidNumbers) {

        for (T *value : vectorOfPointers) {

            if (ConvertFunctions::isValidNumber(*value)) {

                vectorOfPointersToProcess << value;

                double temp;

                ConvertFunctions::convert(*value, temp);

                vectorOfValuesToProcess << temp;

            }

        }

    } else {

        vectorOfPointersToProcess = vectorOfPointers;

        for (T *value : vectorOfPointers)
            vectorOfValuesToProcess << QVariant(*value).toDouble();

    }

    for (int i = 0; i < d_parameters.formatedTransformFunctionList.size(); ++i)
        d_parameters.formatedTransformFunctionList.at(i)(vectorOfValuesToProcess);

    for (int i = 0; i < vectorOfValuesToProcess.size(); ++i)
        *vectorOfPointersToProcess[i] = QVariant(vectorOfValuesToProcess.at(i)).value<T>();

}

#endif // TRANSFORMDATAMATRIXWORKERCLASS_H
