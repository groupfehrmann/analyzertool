#include "resultlistmodel.h"

ResultListModelItem::ResultListModelItem(ResultListModelItem *parentItem, QObject *internalObject) :
    d_internalObject(internalObject), d_parentItem(parentItem)
{

    if (ResultList *resultList = dynamic_cast<ResultList *>(d_internalObject)) {

        for (int i = 0; i < resultList->numberOfResults(); ++i)
            this->appendChild(new ResultListModelItem(const_cast<ResultListModelItem*>(this), resultList->resultAt(i).data()));

    }

    if (Result *result = dynamic_cast<Result *>(d_internalObject)) {

        for (int i = 0; i < result->numberOfResultItems(); ++i)
            this->appendChild(new ResultListModelItem(const_cast<ResultListModelItem*>(this), result->baseResultItemAt(i).data()));

    }

}

ResultListModelItem::~ResultListModelItem()
{

    qDeleteAll(d_childItems);

}

void ResultListModelItem::appendChild(ResultListModelItem *child)
{

    d_childItems.append(child);

}

ResultListModelItem *ResultListModelItem::child(int row)
{

    return d_childItems.at(row);

}

int ResultListModelItem::childCount() const
{

    return d_childItems.count();

}

int ResultListModelItem::columnCount() const
{

    return 1;

}

QVariant ResultListModelItem::data(int column) const
{

    if (dynamic_cast<ResultList *>(d_internalObject)) {

        if (column == 0)
            return "Results";

    }

    if (Result *result = dynamic_cast<Result *>(d_internalObject)) {

        if (column == 0)
            return result->name();

    }

    if (BaseResultItem *baseResultItem = dynamic_cast<BaseResultItem *>(d_internalObject)) {

        if (column == 0)
            return baseResultItem->name();

    }

    return QVariant();

}

void ResultListModelItem::insertChild(int row, ResultListModelItem *child)
{

    d_childItems.insert(row, child);

}

QObject *ResultListModelItem::internalObject()
{

    return d_internalObject;

}

const QObject *ResultListModelItem::internalObject() const
{

    return d_internalObject;

}

ResultListModelItem *ResultListModelItem::parentItem()
{

    return d_parentItem;

}

void ResultListModelItem::removeChild(int row)
{

    delete d_childItems[row];

    d_childItems.removeAt(row);

}

void ResultListModelItem::removeChildren()
{

    qDeleteAll(d_childItems);

    d_childItems.clear();

}

int ResultListModelItem::row() const
{

    if (d_parentItem)
         return d_parentItem->d_childItems.indexOf(const_cast<ResultListModelItem*>(this));

     return 0;

}


ResultListModel::ResultListModel(QObject *parent, ResultList *resultList) :
    QAbstractItemModel(parent), d_resultList(resultList)
{

    d_resultList->setParent(this);

    this->connect(d_resultList, SIGNAL(resultItemAdded(QSharedPointer<BaseResultItem>)), this, SIGNAL(resultItemAdded(QSharedPointer<BaseResultItem>)));

    this->connect(d_resultList, SIGNAL(resultItemNameChanged(QUuid,QString)), this, SIGNAL(resultItemNameChanged(QUuid,QString)));

    this->connect(d_resultList, SIGNAL(resultItemRemoved(QUuid)), this, SIGNAL(resultItemRemoved(QUuid)));

    this->connect(d_resultList, SIGNAL(resultAdded(QSharedPointer<Result>)), this, SIGNAL(resultAdded(QSharedPointer<Result>)));

    this->connect(d_resultList, SIGNAL(resultNameChanged(QUuid,QString)), this, SIGNAL(resultNameChanged(QUuid,QString)));

    this->connect(d_resultList, SIGNAL(resultRemoved(QUuid)), this, SIGNAL(resultRemoved(QUuid)));

    d_rootItem = new ResultListModelItem(nullptr, d_resultList);

}

ResultListModel::~ResultListModel()
{

    delete d_rootItem;

}

int ResultListModel::columnCount(QModelIndex const &parent) const
{

    Q_UNUSED(parent);

    return 1;

}

QVariant ResultListModel::data(QModelIndex const &index, int role) const
{

    if (!index.isValid())
        return QVariant();

     ResultListModelItem *item = static_cast<ResultListModelItem*>(index.internalPointer());

     if (role == Qt::DisplayRole)
        return item->data(index.column());

    return QVariant();

}

Qt::ItemFlags ResultListModel::flags(QModelIndex const &index) const
{

    if (!index.isValid())
        return Qt::NoItemFlags;

    ResultListModelItem *item = static_cast<ResultListModelItem*>(index.internalPointer());

    if (item) {

        if (dynamic_cast<const Result *>(item->internalObject()))
            return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;

        if (dynamic_cast<const BaseResultItem *>(item->internalObject()))
            return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;

    }

    return QAbstractItemModel::flags(index);

}

QVariant ResultListModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    Q_UNUSED(section);

    Q_UNUSED(orientation);

    Q_UNUSED(role);

    return QVariant();

}

QModelIndex ResultListModel::index(int row, int column, const QModelIndex &parent) const
{

    if (!this->hasIndex(row, column, parent))
        return QModelIndex();

    ResultListModelItem *parentItem;

    if (!parent.isValid())
        parentItem = d_rootItem;
    else
        parentItem = static_cast<ResultListModelItem *>(parent.internalPointer());

    ResultListModelItem *childItem = parentItem->child(row);

    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();

}

bool ResultListModel::appendResult(Result *result, const QModelIndex &parent)
{

    if (!parent.isValid()) {

        this->beginInsertRows(parent, this->rowCount(), this->rowCount());

        d_resultList->appendResult(result);

        d_rootItem->insertChild(this->rowCount(), new ResultListModelItem(d_rootItem, result));

        this->endInsertRows();

        return true;

    }

    return false;

}

bool ResultListModel::appendResultItem(BaseResultItem *baseResultItem, const QModelIndex &parent)
{

    if (!parent.isValid())
        return false;

    ResultListModelItem *item = static_cast<ResultListModelItem *>(parent.internalPointer());

    if (Result *result = dynamic_cast<Result *>(item->internalObject())) {

        this->beginInsertRows(parent, this->rowCount(parent), this->rowCount(parent));

        result->appendResultItem(baseResultItem);

        item->insertChild(this->rowCount(parent), new ResultListModelItem(item, baseResultItem));

        this->endInsertRows();

        return true;

    }

    return false;

}

QModelIndex ResultListModel::parent(const QModelIndex &index) const
{

    if (!index.isValid())
        return QModelIndex();

    ResultListModelItem *childItem = static_cast<ResultListModelItem *>(index.internalPointer());

    ResultListModelItem *parentItem = childItem->parentItem();

    if (parentItem == d_rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);

}

const ResultList *ResultListModel::resultList() const
{

    return d_resultList;

}

void ResultListModel::removeAll()
{

    this->beginResetModel();

    d_rootItem->removeChildren();

    d_resultList->removeAllResults();

    this->endResetModel();

}

bool ResultListModel::removeRows(int row, int count, const QModelIndex &parent)
{

    this->beginRemoveRows(parent, row, row + count - 1);

    QList<int> rowIndexesToRemove;

    for (int i = 0; i < count; ++i)
        rowIndexesToRemove << row + i;

    ResultListModelItem *item = parent.isValid() ? static_cast<ResultListModelItem *>(parent.internalPointer()) : d_rootItem;

    std::sort(rowIndexesToRemove.begin(), rowIndexesToRemove.end(), std::greater<int>());

    for(const int &rowIndexToRemove: rowIndexesToRemove)
        item->removeChild(rowIndexToRemove);

    if (ResultList *resultList = dynamic_cast<ResultList *>(item->internalObject())) {

        resultList->removeResults(rowIndexesToRemove);

        this->endRemoveRows();

        return true;

    }

    if (Result *result = dynamic_cast<Result *>(item->internalObject())) {

        result->removeResultItems(rowIndexesToRemove);

        this->endRemoveRows();

        return true;

    }

    this->endRemoveRows();

    return false;

}

int ResultListModel::rowCount(QModelIndex const &parent) const
{

    ResultListModelItem *parentItem;

    if (!parent.isValid())
        parentItem = d_rootItem;
    else
        parentItem = static_cast<ResultListModelItem *>(parent.internalPointer());

    return parentItem->childCount();

}

bool ResultListModel::setData(QModelIndex const &index, QVariant const &value, int role)
{

    ResultListModelItem *item = static_cast<ResultListModelItem *>(index.internalPointer());

    if (role == Qt::EditRole) {

        if (Result *result = dynamic_cast<Result *>(item->internalObject())) {

            result->setName(value.toString());

            emit dataChanged(index, index);

            return true;

        }

        if (BaseResultItem *baseResultItem = dynamic_cast<BaseResultItem *>(item->internalObject())) {

            baseResultItem->setName(value.toString());

            emit dataChanged(index, index);

            return true;

        }

    }

    return false;

}
