#include "comboboxbuildingblockwidget.h"
#include "ui_comboboxbuildingblockwidget.h"

ComboBoxBuildingBlockWidget::ComboBoxBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, const QStringList &itemsText, const QVariantList &itemsUserData) :
    QWidget(parent), ui(new Ui::ComboBoxBuildingBlockWidget), d_rememberSettings(false)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    if (itemsText.isEmpty())
        return;
    else
        d_rememberSettings = true;

    if (itemsUserData.isEmpty())
        ui->comboBox->addItems(itemsText);
    else {

        for (int i = 0; i < itemsText.size(); ++i)
            ui->comboBox->addItem(itemsText.at(i), itemsUserData.at(i));

    }

    ui->comboBox->setCurrentIndex(0);

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/comboBoxBuildingBlockWidget");

    if (settings.contains("currentIndex"))
        ui->comboBox->setCurrentIndex(settings.value("currentIndex").toInt());

    settings.endGroup();

}

ComboBoxBuildingBlockWidget::~ComboBoxBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/comboBoxBuildingBlockWidget");

        settings.setValue("currentIndex", ui->comboBox->currentIndex());

        settings.endGroup();

    }

    delete ui;

}

QComboBox *ComboBoxBuildingBlockWidget::pointerToInternalComboBox()
{

    return ui->comboBox;

}

QLabel *ComboBoxBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}
