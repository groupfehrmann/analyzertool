#ifndef INDEPENDENTCOMPONENTANALYSISWORKERCLASS_H
#define INDEPENDENTCOMPONENTANALYSISWORKERCLASS_H

#include <QString>
#include <QList>
#include <QSharedPointer>
#include <QtConcurrent>
#include <QFutureWatcher>
#include <QDateTime>

#ifdef Q_OS_MAC
#include <Accelerate/Accelerate.h>
#endif

#include <functional>
#include <utility>
#include <cfloat>
#include <random>
#include <algorithm>

#include "base/baseworkerclass.h"
#include "math/vectoroperations.h"
#include "math/matrixoperations.h"
#include "math/mathdescriptives.h"
#include "math/fastdistancecorrelation.h"

#define FICA_APPROACH_DEFL 2
#define FICA_APPROACH_SYMM 1
#define FICA_NONLIN_POW3 10
#define FICA_NONLIN_TANH 20
#define FICA_NONLIN_GAUSS 30
#define FICA_NONLIN_SKEW 40
#define FICA_INIT_RAND  0
#define FICA_INIT_GUESS 1
#define FICA_TOL 1e-9

class IndependentComponentAnalysisWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    enum WhiteningMode{

        COVARIANCE = 0,

        DISTANCECOVARIANCE = 1

    };

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        double independentComponentClusteringCorrelationThreshold;

        QString contrastFunction;

        double credibilityIndexThreshold;

        double epsilon;

        bool finetuneMode;

        int maximumNumberOfIterations;

        QString multiThreadMode;

        double mu;

        int numberOfRunsToPerform;

        BaseMatrix::Orientation orientation;

        QString outputDirectory;

        double proportionOfSamplesToUseInAnIteration;

        int searchTimeConstant;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        bool stabilizationMode;

        long double thresholdCumulativeExplainedVariance;

        int thresholdMaximumNumberOfComponents;

        WhiteningMode whiteningMode;

        double distanceCorrelationThreshold;

    };

    IndependentComponentAnalysisWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    IndependentComponentAnalysisWorkerClass::Parameters d_parameters;

    QList<int> d_indexesForSubSampling;

    bool checkInputParameters();

    bool fastICA(QThread *workerThread, int progressIdentifier, const QVector<QVector<double> > &whitenedMatrix, const QVector<QVector<double> > &whiteningMatrix, const QVector<QVector<double> > &dewhiteningMatrix, QVector<QVector<double> > &mixingMatrix, QVector<QVector<double> > &seperatingMatrix, int contrastFunction = FICA_NONLIN_POW3, int maximumNumberOfIterations = 1000, bool finetuneMode = true, bool stabilizationMode = true, double mu = 1.0, const double &epsilon = 0.0001, double proportionOfSamplesToUseInAnIteration = 1.0);

    bool fastICA_multiThreaded(const QVector<QVector<double> > &whitenedMatrix, const QVector<QVector<double> > &whiteningMatrix, const QVector<QVector<double> > &dewhiteningMatrix, QVector<QVector<double> > &mixingMatrix, QVector<QVector<double> > &seperatingMatrix, int contrastFunction = FICA_NONLIN_POW3, int maximumNumberOfIterations = 1000, bool finetuneMode = true, bool stabilizationMode = true, double mu = 1.0, const double &epsilon = 0.0001, double proportionOfSamplesToUseInAnIteration = 1.0);

    QString secondsToTimeString(int s);

};

#endif // INDEPENDENTCOMPONENTANALYSISWORKERCLASS_H
