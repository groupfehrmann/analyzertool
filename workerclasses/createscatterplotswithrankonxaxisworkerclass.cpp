#include "createscatterplotswithrankonxaxisworkerclass.h"

CreateScatterPlotsWithRankOnXAxisWorkerClass::CreateScatterPlotsWithRankOnXAxisWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CreateScatterPlotsWithRankOnXAxisWorkerClass::Parameters *>(parameters))
{

}

void CreateScatterPlotsWithRankOnXAxisWorkerClass::doWork()
{

    emit processStarted("create scatter plots with rank on x-axis (Homo Sapiens)");

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Scatter plots with rank on x-axis (Homo Sapiens)"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "annotation label defining rank: \"" + d_parameters.annotationLabelDefiningRank + "\"");

    if (!d_parameters.annotationLabelDefiningSubsets.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining subsets: \"" + d_parameters.annotationLabelDefiningSubsets + "\"");

        emit appendLogItem(LogListModel::PARAMETER, "subset identifiers: " + ConvertFunctions::toString(d_parameters.subsetIdentifiers));

    }

    if (!d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining title: " + d_parameters.annotationLabelDefiningAlternativeVariableIdentifier);

    if (d_parameters.chartType == QChart::ChartTypeCartesian)
        emit appendLogItem(LogListModel::PARAMETER, "chart type: cartesian");
    else
        emit appendLogItem(LogListModel::PARAMETER, "chart type: polar");

    emit appendLogItem(LogListModel::PARAMETER, "plot mode: " + d_parameters.plotMode);

    if (!d_parameters.exportDirectory.isEmpty()) {

        if (d_parameters.widthOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale width of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "width of plot: " + QString::number(d_parameters.widthOfPlot));

        if (d_parameters.heightOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale height of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "height of plot: " + QString::number(d_parameters.heightOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "dpi of plot: " + QString::number(d_parameters.dpiOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "export type of plot: " + d_parameters.exportTypeOfPlot);

        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    }

    if (!d_parameters.pathToTemplatePlot.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "template plot selected: \"" + d_parameters.pathToTemplatePlot + "\"");

        emit startProgress(0, "Opening template plot", 0, 0);

        bool error = false;

        QString errorMessage;

        OpenPlotFromFile openPlotFromFile(nullptr, d_parameters.pathToTemplatePlot, &error, &errorMessage);

        if (error)
            emit appendLogItem(LogListModel::ERROR, errorMessage);
        else
            d_templatePlot = openPlotFromFile.chart();

        emit stopProgress(0);

    }

    emit startProgress(0, "Obtaining ranking", 0, 0);

    QList<QPair<int, std::tuple<QString, double, int> > > listOfPairedIndexWith_subsetIdentifier_originalRank_convertedRank = ConvertFunctions::toListOfPairedIndexWithTupleOfValues(d_parameters.baseDataset->indexToAnnotationValues<QString, double, int>(d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.annotationLabelDefiningSubsets, QSet<QString>(d_parameters.subsetIdentifiers.begin(), d_parameters.subsetIdentifiers.end()), d_parameters.annotationLabelDefiningRank, QSet<double>(), QString(), QSet<int>(), BaseMatrix::switchOrientation(d_parameters.orientation)));

    std::function<bool(const QPair<int, std::tuple<QString, double, int> > &element1, const QPair<int, std::tuple<QString, double, int> > &element2) > compareFunction = [](const QPair<int, std::tuple<QString, double, int> > &element1, const QPair<int, std::tuple<QString, double, int> > &element2) {

        if (std::get<1>(element1.second) < std::get<1>(element2.second))
            return true;
        else
            return false;

    };

    std::sort(listOfPairedIndexWith_subsetIdentifier_originalRank_convertedRank.begin(), listOfPairedIndexWith_subsetIdentifier_originalRank_convertedRank.end(), compareFunction);

    for (int i = 0; i < listOfPairedIndexWith_subsetIdentifier_originalRank_convertedRank.size(); ++i)
       std::get<2>(listOfPairedIndexWith_subsetIdentifier_originalRank_convertedRank[i].second) = i + 1;

    QHash<QString, std::tuple<QList<int>, QVector<double>, QVector<int> > > subsetIdentifierTo_indexes_originalRank_convertedRank = ConvertFunctions::toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple<QString, double, int>(listOfPairedIndexWith_subsetIdentifier_originalRank_convertedRank);

    if (d_parameters.annotationLabelDefiningSubsets.isEmpty())
        d_parameters.subsetIdentifiers << QString();
    else {

        for (int i = d_parameters.subsetIdentifiers.size() - 1; i >=0; --i) {

            if (!subsetIdentifierTo_indexes_originalRank_convertedRank.contains(d_parameters.subsetIdentifiers.at(i)))
                d_parameters.subsetIdentifiers.removeOne(d_parameters.subsetIdentifiers.at(i));

        }

    }

    emit stopProgress(0);

    QString fileExtension = d_parameters.exportTypeOfPlot.split(".").at(1);

    fileExtension.remove(")");

    if (d_parameters.plotMode == "All variables in single plot") {

        emit startProgress(0, "Creating plot", 0, 0);

        QVector<QVector<int> > xVectors;

        QVector<QVector<double> > yVectors;

        QStringList seriesLabels;

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i) {

            for (const QString &subsetIdentifier : d_parameters.subsetIdentifiers) {

                if (subsetIdentifier.isEmpty())
                    seriesLabels << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i);
                else
                    seriesLabels << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) + " - " + subsetIdentifier;


                xVectors << std::get<2>(subsetIdentifierTo_indexes_originalRank_convertedRank.value(subsetIdentifier));

                yVectors << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), std::get<0>(subsetIdentifierTo_indexes_originalRank_convertedRank.value(subsetIdentifier)), d_parameters.orientation);

            }

        }

        QSharedPointer<ScatterChart_RankOnXAxis> scatterChart_RankOnXAxis(new ScatterChart_RankOnXAxis(nullptr, "scatter plot - rank on x-axis", seriesLabels, xVectors, yVectors, d_parameters.chartType));

        if (d_templatePlot)
            CopyChartParameters(nullptr, scatterChart_RankOnXAxis->chart().data(), d_templatePlot.data());

        if (d_parameters.exportDirectory.isEmpty())
            emit resultItemAvailable(new PlotResultItem(nullptr, "scatter plot - rank on x-axis", scatterChart_RankOnXAxis->chart()));
        else {

            bool error;

            QString errorMessage;

            ExportPlotToFile(nullptr, scatterChart_RankOnXAxis->chart().data(), d_parameters.exportDirectory + "/scatterChart_RankOnXAxis." + fileExtension , d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

            if (error)
               emit appendLogItem(LogListModel::ERROR, errorMessage);

        }

        emit stopProgress(0);

    } else {

        emit startProgress(0, "Creating plots", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i) {

            QVector<QVector<int> > xVectors;

            QVector<QVector<double> > yVectors;

            for (const QString &subsetIdentifier : d_parameters.subsetIdentifiers) {

                xVectors << std::get<2>(subsetIdentifierTo_indexes_originalRank_convertedRank.value(subsetIdentifier));

                yVectors << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), std::get<0>(subsetIdentifierTo_indexes_originalRank_convertedRank.value(subsetIdentifier)), d_parameters.orientation);

            }

            QSharedPointer<ScatterChart_RankOnXAxis> scatterChart_RankOnXAxis(new ScatterChart_RankOnXAxis(nullptr, (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()), d_parameters.subsetIdentifiers, xVectors, yVectors, d_parameters.chartType));

            if (d_templatePlot)
                CopyChartParameters(nullptr, scatterChart_RankOnXAxis->chart().data(), d_templatePlot.data());

            if (d_parameters.exportDirectory.isEmpty())
                emit resultItemAvailable(new PlotResultItem(nullptr, "scatter plot - rank on x-axis - " + (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()), scatterChart_RankOnXAxis->chart()));
            else {

                bool error;

                QString errorMessage;

                ExportPlotToFile(nullptr, scatterChart_RankOnXAxis->chart().data(), d_parameters.exportDirectory + "/scatterChart_RankOnXAxis_" + (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()).simplified() + "." + fileExtension , d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

                if (error)
                    emit appendLogItem(LogListModel::ERROR, errorMessage);

            }

            if (QThread::currentThread()->isInterruptionRequested()) {

                scatterChart_RankOnXAxis.clear();

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(0, i + 1);

        }

        emit stopProgress(0);

    }

    QThread::currentThread()->quit();

}
