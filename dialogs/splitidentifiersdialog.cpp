#include "splitidentifiersdialog.h"

SplitIdentifiersDialog::SplitIdentifiersDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new SplitIdentifiersWorkerClass::Parameters;

    this->setupWidgets();

}

SplitIdentifiersDialog::~SplitIdentifiersDialog()
{

   delete static_cast<SplitIdentifiersWorkerClass::Parameters *>(d_parameters);

}

void SplitIdentifiersDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_identifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_identifiers", "Select identifiers");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_identifiers, 1, 0);

    d_selectDelimiterBuildingBlockWidget = new SelectDelimiterBuildingBlockWidget(this, "selectDelimiterBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectDelimiterBuildingBlockWidget, 2, 0);

    d_spinBoxBuildingBlockWidget_numberOfTokenToKeep = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_numberOfTokenToKeep", "select number of token to keep", true, 10, 1, 100, 1, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_numberOfTokenToKeep, 3, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void SplitIdentifiersDialog::initializeParameters()
{

    SplitIdentifiersWorkerClass::Parameters *parameters = static_cast<SplitIdentifiersWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->caseSensitivityForCustumDelimiter = d_selectDelimiterBuildingBlockWidget->pointerToInternalCheckBox()->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive;

    parameters->delimiterType = static_cast<SelectDelimiterBuildingBlockWidget::DelimiterType>(d_selectDelimiterBuildingBlockWidget->pointerToInternalComboBox()->currentIndex());

    parameters->custumDelimiter = d_selectDelimiterBuildingBlockWidget->delimiter();

    parameters->numberOfTokenToKeep = d_spinBoxBuildingBlockWidget_numberOfTokenToKeep->pointerToInternalSpinBox()->value();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->selectedIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_identifiers->selectedItemsIdentifierAndNumber();


}

void SplitIdentifiersDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > identifiersAndIndexes = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_identifiers->setItemsIdentifierAndNumber(identifiersAndIndexes.first, identifiersAndIndexes.second);

}
