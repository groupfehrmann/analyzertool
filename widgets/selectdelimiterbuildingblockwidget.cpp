#include "selectdelimiterbuildingblockwidget.h"
#include "ui_selectdelimiterbuildingblockwidget.h"

QString SelectDelimiterBuildingBlockWidget::delimiterTypeToString(SelectDelimiterBuildingBlockWidget::DelimiterType delimiterType)
{

    switch (delimiterType) {

        case CUSTUM : return "custum";

        case TAB : return "tab";

        case SPACE: return "space";

        case LINEFEED : return "line feed";

        case CARRIAGERETURN : return "carriage return";

        case WHITESPACE : return "white space";

    }

    return QString();

}

SelectDelimiterBuildingBlockWidget::SelectDelimiterBuildingBlockWidget(BaseDialog *parent, const QString &widgetId) :
    QWidget(parent), ui(new Ui::SelectDelimiterBuildingBlockWidget)
{

    ui->setupUi(this);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/selectDelimiterBuildingBlockWidget");

    if (settings.contains("comboBox/currentIndex"))
        ui->comboBox->setCurrentIndex(settings.value("comboBox/currentIndex").toInt());

    if (settings.contains("lineEdit/text"))
        ui->lineEdit->setText(settings.value("lineEdit/text").toString());

    if (settings.contains("lineEdit/text"))
        ui->checkBox->setChecked(settings.value("checkBox/checked").toBool());

    settings.endGroup();

    this->connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexOfComboboxChanged(int)));

    this->connect(ui->lineEdit, SIGNAL(textEdited(QString)), this, SIGNAL(delimiterChanged()));

    this->connect(ui->checkBox, SIGNAL(toggled(bool)), this, SIGNAL(delimiterChanged()));

    this->currentIndexOfComboboxChanged(ui->comboBox->currentIndex());

}

SelectDelimiterBuildingBlockWidget::~SelectDelimiterBuildingBlockWidget()
{

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/selectDelimiterBuildingBlockWidget");

    settings.setValue("comboBox/currentIndex", ui->comboBox->currentIndex());

    settings.setValue("lineEdit/text", ui->lineEdit->text());

    settings.setValue("checkBox/checked", ui->checkBox->isChecked());

    settings.endGroup();

    delete ui;

}

void SelectDelimiterBuildingBlockWidget::currentIndexOfComboboxChanged(int index)
{

    if (index == CUSTUM) {

        ui->lineEdit->setEnabled(true);

        ui->checkBox->setEnabled(true);

    } else {

        ui->lineEdit->setEnabled(false);

        ui->checkBox->setEnabled(false);

    }

    emit delimiterChanged();

}

QString SelectDelimiterBuildingBlockWidget::delimiter()
{

    switch (ui->comboBox->currentIndex()) {

        case CUSTUM: return ui->lineEdit->text();

        case TAB: return "\\t";

        case SPACE: return " ";

        case LINEFEED: return "\\n";

        case CARRIAGERETURN: return "\\r";

        case WHITESPACE: return "\\s+";

    }

    return QString();

}

QLineEdit *SelectDelimiterBuildingBlockWidget::pointerToInternalLineEdit()
{

    return ui->lineEdit;

}

QComboBox *SelectDelimiterBuildingBlockWidget::pointerToInternalComboBox()
{

    return ui->comboBox;

}

QCheckBox *SelectDelimiterBuildingBlockWidget::pointerToInternalCheckBox()
{

    return ui->checkBox;

}
