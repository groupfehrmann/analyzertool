#include "datasetwidget.h"
#include "ui_datasetwidget.h"

DatasetWidget::DatasetWidget(QWidget *parent, const QSharedPointer<BaseDataset> &baseDataset) :
    QWidget(parent), ui(new Ui::DatasetWidget), d_baseDataset(baseDataset)
{

    ui->setupUi(this);


    MatrixSortFilterProxyModel *matrixSortFilterProxyModel = new MatrixSortFilterProxyModel(this);

    MatrixModel *matrixModel = new MatrixModel(this, d_baseDataset);

    matrixSortFilterProxyModel->setSourceModel(matrixModel);

    ui->tableView_dataMatrix->setModel(matrixSortFilterProxyModel);


    AnnotationsSortFilterProxyModel *rowAnnotationsSortFilterProxyModel = new AnnotationsSortFilterProxyModel(this);

    AnnotationsModel *rowAnnotationsModel = new AnnotationsModel(ui->tableView_rowAnnotations, d_baseDataset, BaseMatrix::ROW);

    rowAnnotationsSortFilterProxyModel->setSourceModel(rowAnnotationsModel);

    ui->tableView_rowAnnotations->setModel(rowAnnotationsSortFilterProxyModel);


    AnnotationsSortFilterProxyModel *columnAnnotationsSortFilterProxyModel = new AnnotationsSortFilterProxyModel(this);

    AnnotationsModel *columnAnnotationsModel = new AnnotationsModel(ui->tableView_columnAnnotations, d_baseDataset, BaseMatrix::COLUMN);

    columnAnnotationsSortFilterProxyModel->setSourceModel(columnAnnotationsModel);

    ui->tableView_columnAnnotations->setModel(columnAnnotationsSortFilterProxyModel);


    ui->tableView_dataMatrix->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_dataMatrix->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_rowAnnotations->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_rowAnnotations->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_columnAnnotations->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_columnAnnotations->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);


    this->connect(matrixModel, SIGNAL(headerDataChanged(Qt::Orientation,int,int)), this, SLOT(headerDataForMatrixModelChanged(Qt::Orientation,int,int)));


    this->connect(ui->tableView_dataMatrix->verticalScrollBar(), SIGNAL(valueChanged(int)), ui->tableView_rowAnnotations->verticalScrollBar(), SLOT(setValue(int)));

    this->connect(ui->tableView_dataMatrix->horizontalScrollBar(), SIGNAL(valueChanged(int)), ui->tableView_columnAnnotations->verticalScrollBar(), SLOT(setValue(int)));

    this->connect(ui->tableView_rowAnnotations->verticalScrollBar(), SIGNAL(valueChanged(int)), ui->tableView_dataMatrix->verticalScrollBar(), SLOT(setValue(int)));

    this->connect(ui->tableView_columnAnnotations->verticalScrollBar(), SIGNAL(valueChanged(int)), ui->tableView_dataMatrix->horizontalScrollBar(), SLOT(setValue(int)));

    this->connect(ui->tableView_dataMatrix->verticalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenuVerticalHeaderDataMatrix(QPoint)));

    this->connect(ui->tableView_dataMatrix->horizontalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenuHorizontalHeaderDataMatrix(QPoint)));

    this->connect(ui->tableView_rowAnnotations->verticalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenuVerticalHeaderDataMatrix(QPoint)));

    this->connect(ui->tableView_rowAnnotations->horizontalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenuHorizontalHeaderAnnotations(QPoint)));

    this->connect(ui->tableView_columnAnnotations->verticalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenuHorizontalHeaderDataMatrix(QPoint)));

    this->connect(ui->tableView_columnAnnotations->horizontalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenuHorizontalHeaderAnnotations(QPoint)));

    this->createActions();

    this->createMenus();

}

DatasetWidget::~DatasetWidget()
{

    delete ui;

}

void DatasetWidget::annotationsBeginChange(BaseMatrix::Orientation orientation)
{

    if (orientation == BaseMatrix::ROW)
        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->sourceModel())->beginReset();
    else
        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->sourceModel())->beginReset();

}

void DatasetWidget::annotationsEndChange(BaseMatrix::Orientation orientation)
{

    if (orientation == BaseMatrix::ROW)
        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->sourceModel())->endReset();
    else
        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->sourceModel())->endReset();

}

void DatasetWidget::beginChange()
{

    static_cast<MatrixModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_dataMatrix->model())->sourceModel())->beginReset();

    static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->sourceModel())->beginReset();

    static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->sourceModel())->beginReset();

}

void DatasetWidget::copy_headerActivated()
{

    QHeaderView *headerView = static_cast<QHeaderView *>(d_copyAction_headerActivated->property("headerView").value<void *>());

    QClipboard *clipboard = QApplication::clipboard();

    QString str;

    QTextStream out(&str);

    if (headerView->orientation() == Qt::Horizontal) {

        QModelIndexList selectedColumnModelIndexes = headerView->selectionModel()->selectedColumns();

        for (int i = 0; i < headerView->model()->rowCount(); ++i) {

            out << headerView->model()->data(headerView->model()->index(i, selectedColumnModelIndexes.first().column())).toString();

            for (int j = 1; j < selectedColumnModelIndexes.size(); ++j)
                out << "\t" << headerView->model()->data(headerView->model()->index(i, selectedColumnModelIndexes.at(j).column())).toString();

            if (i < (headerView->model()->rowCount() - 1))
                out << Qt::endl;

        }


    } else {

        QModelIndexList selectedRowModelIndexes = headerView->selectionModel()->selectedRows();

        for (int i = 0; i < selectedRowModelIndexes.size(); ++i) {

            int currentRowIndex = selectedRowModelIndexes.at(i).row();

            out << headerView->model()->data(headerView->model()->index(currentRowIndex, 0)).toString();

            for (int j = 1; j < headerView->model()->columnCount(); ++j)
                out << "\t" << headerView->model()->data(headerView->model()->index(currentRowIndex, j)).toString();

            if (i < (selectedRowModelIndexes.size() - 1))
                out << Qt::endl;

        }

    }

    clipboard->setText(str);

}

void DatasetWidget::copyWithHeaderLabels_headerActivated()
{

    QHeaderView *headerView = static_cast<QHeaderView *>(d_copyWithHeaderLabelsAction_headerActivated->property("headerView").value<void *>());

    QClipboard *clipboard = QApplication::clipboard();

    QString str;

    QTextStream out(&str);

    if (headerView->orientation() == Qt::Horizontal) {

        QModelIndexList selectedColumnModelIndexes = headerView->selectionModel()->selectedColumns();

        for (int i = 0; i < selectedColumnModelIndexes.size(); ++i)
            out << "\t" << headerView->model()->headerData(selectedColumnModelIndexes.at(i).column(), Qt::Horizontal).toString();

        out << Qt::endl;

        for (int i = 0; i < headerView->model()->rowCount(); ++i) {

            out << headerView->model()->headerData(i, Qt::Vertical).toString();

            for (int j = 0; j < selectedColumnModelIndexes.size(); ++j)
                out << "\t" << headerView->model()->data(headerView->model()->index(i, selectedColumnModelIndexes.at(j).column())).toString();

            if (i < (headerView->model()->rowCount() - 1))
                out << Qt::endl;

        }


    } else {

        QModelIndexList selectedRowModelIndexes = headerView->selectionModel()->selectedRows();

        for (int i = 0; i < headerView->model()->columnCount(); ++i)
            out << "\t" << headerView->model()->headerData(i, Qt::Horizontal).toString();

        out << Qt::endl;

        for (int i = 0; i < selectedRowModelIndexes.size(); ++i) {

            int currentRowIndex = selectedRowModelIndexes.at(i).row();

            out << headerView->model()->headerData(currentRowIndex, Qt::Vertical).toString();

            for (int j = 0; j < headerView->model()->columnCount(); ++j)
                out << "\t" << headerView->model()->data(headerView->model()->index(currentRowIndex, j)).toString();

            if (i < (selectedRowModelIndexes.size() - 1))
                out << Qt::endl;

        }

    }

    clipboard->setText(str);

}

void DatasetWidget::copyToAnnotations_headerActivated()
{

    QHeaderView *headerView = static_cast<QHeaderView *>(d_copyToAnnotationsAction_headerActivated->property("headerView").value<void *>());

    if (headerView->orientation() == Qt::Vertical) {

        Annotations &annotations(d_baseDataset->annotations(BaseMatrix::COLUMN));

        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->sourceModel())->beginReset();

        QModelIndexList selectedRowModelIndexes = headerView->selectionModel()->selectedRows();

        for (int i = 0; i < selectedRowModelIndexes.size(); ++i) {

            int currentRowIndex = selectedRowModelIndexes.at(i).row();

            QString currentRowHeaderLabel = headerView->model()->headerData(currentRowIndex, Qt::Vertical).toString();

            for (int j = 0; j < headerView->model()->columnCount(); ++j) {

                if (!annotations.appendValue(headerView->model()->headerData(j, Qt::Horizontal).toString(), currentRowHeaderLabel, headerView->model()->data(headerView->model()->index(currentRowIndex, j))))
                    emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + headerView->model()->headerData(j, Qt::Horizontal).toString() + "\", label \"" + currentRowHeaderLabel + "\", value to set \"" + headerView->model()->data(headerView->model()->index(currentRowIndex, j)).toString() + "\", current value \"" + annotations.value(headerView->model()->headerData(j, Qt::Horizontal).toString(), currentRowHeaderLabel).toString() + "\"");

            }

        }

        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->sourceModel())->endReset();

    } else {

        Annotations &annotations(d_baseDataset->annotations(BaseMatrix::ROW));

        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->sourceModel())->beginReset();

        QModelIndexList selectedColumnModelIndexes = headerView->selectionModel()->selectedColumns();

        for (int i = 0; i < headerView->model()->rowCount(); ++i) {

            QString currentRowHeaderLabel = headerView->model()->headerData(i, Qt::Vertical).toString();

            for (int j = 0; j < selectedColumnModelIndexes.size(); ++j) {

                if (!annotations.appendValue(currentRowHeaderLabel, headerView->model()->headerData(selectedColumnModelIndexes.at(j).column(), Qt::Horizontal).toString(), headerView->model()->data(headerView->model()->index(i, selectedColumnModelIndexes.at(j).column()))))
                    emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + currentRowHeaderLabel + "\", label \"" + headerView->model()->headerData(selectedColumnModelIndexes.at(j).column(), Qt::Horizontal).toString() + "\", value to set \"" + headerView->model()->data(headerView->model()->index(i, selectedColumnModelIndexes.at(j).column())).toString() + "\", current value \"" + annotations.value(currentRowHeaderLabel, headerView->model()->headerData(selectedColumnModelIndexes.at(j).column(), Qt::Horizontal).toString()).toString() + "\"");

            }

        }

        static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->sourceModel())->endReset();

    }

}

void DatasetWidget::createActions()
{

    d_copyAction_headerActivated = new QAction("Copy", this);

    this->connect(d_copyAction_headerActivated, SIGNAL(triggered(bool)), this, SLOT(copy_headerActivated()));

    d_copyWithHeaderLabelsAction_headerActivated = new QAction("Copy with header labels", this);

    this->connect(d_copyWithHeaderLabelsAction_headerActivated, SIGNAL(triggered(bool)), this, SLOT(copyWithHeaderLabels_headerActivated()));

    d_copyToAnnotationsAction_headerActivated = new QAction("Copy to annotations", this);

    this->connect(d_copyToAnnotationsAction_headerActivated, SIGNAL(triggered(bool)), this, SLOT(copyToAnnotations_headerActivated()));

    d_deleteAction_headerActivated = new QAction("Delete", this);

    this->connect(d_deleteAction_headerActivated, SIGNAL(triggered(bool)), this, SLOT(delete_headerActivated()));

    d_insertAction_headerActivated = new QAction("Insert", this);

    this->connect(d_insertAction_headerActivated, SIGNAL(triggered(bool)), this, SLOT(insert_headerActivated()));

    d_selectDataAction_headerActivated = new QAction("Select", this);

    this->connect(d_selectDataAction_headerActivated, SIGNAL(triggered(bool)), this, SLOT(selectData_headerActivated()));

    d_deselectDataAction_headerActivated = new QAction("Deselect", this);

    this->connect(d_deselectDataAction_headerActivated, SIGNAL(triggered(bool)), this, SLOT(deselectData_headerActivated()));

    d_renameIdentifierWidgetAction_headerActivated = new QWidgetAction(this);

    d_renameIdentifierWidgetAction_headerActivated->setDefaultWidget(new QLineEdit(this));

    this->connect(static_cast<QLineEdit *>(d_renameIdentifierWidgetAction_headerActivated->defaultWidget()), SIGNAL(editingFinished()), this, SLOT(renameIdentifier_headerActivated()));

    d_renameAnnotationLabelWidgetAction_headerActivated = new QWidgetAction(this);

    d_renameAnnotationLabelWidgetAction_headerActivated->setDefaultWidget(new QLineEdit(this));

    this->connect(static_cast<QLineEdit *>(d_renameAnnotationLabelWidgetAction_headerActivated->defaultWidget()), SIGNAL(editingFinished()), this, SLOT(renameAnnotationLabel_headerActivated()));

}

void DatasetWidget::createMenus()
{

    d_dataMatrixHeaderMenu = new QMenu(this);

    d_dataMatrixHeaderMenu->addAction(d_copyAction_headerActivated);

    d_dataMatrixHeaderMenu->addAction(d_copyWithHeaderLabelsAction_headerActivated);

    d_dataMatrixHeaderMenu->addSeparator();

    d_dataMatrixHeaderMenu->addAction(d_copyToAnnotationsAction_headerActivated);

    d_dataMatrixHeaderMenu->addSeparator();

    d_dataMatrixHeaderMenu->addAction(d_deleteAction_headerActivated);

    d_dataMatrixHeaderMenu->addAction(d_insertAction_headerActivated);

    d_dataMatrixHeaderMenu->addSeparator();

    d_dataMatrixHeaderMenu->addAction(d_selectDataAction_headerActivated);

    d_dataMatrixHeaderMenu->addAction(d_deselectDataAction_headerActivated);

    d_dataMatrixHeaderMenu->addSeparator();

    d_dataMatrixHeaderMenu->addAction(d_renameIdentifierWidgetAction_headerActivated);

    this->connect(static_cast<QLineEdit *>(d_renameIdentifierWidgetAction_headerActivated->defaultWidget()), SIGNAL(editingFinished()), d_dataMatrixHeaderMenu, SLOT(close()));


    d_annotationsHeaderMenu = new QMenu(this);

    d_annotationsHeaderMenu->addAction(d_copyAction_headerActivated);

    d_annotationsHeaderMenu->addAction(d_copyWithHeaderLabelsAction_headerActivated);

    d_annotationsHeaderMenu->addSeparator();

    d_annotationsHeaderMenu->addAction(d_deleteAction_headerActivated);

    d_annotationsHeaderMenu->addAction(d_insertAction_headerActivated);

    d_annotationsHeaderMenu->addSeparator();

    d_annotationsHeaderMenu->addAction(d_renameAnnotationLabelWidgetAction_headerActivated);

    this->connect(static_cast<QLineEdit *>(d_renameAnnotationLabelWidgetAction_headerActivated->defaultWidget()), SIGNAL(editingFinished()), d_annotationsHeaderMenu, SLOT(close()));

}

QAbstractItemModel *DatasetWidget::currentTableModel()
{

    switch (ui->tabWidget->currentIndex()) {

        case 0 : return ui->tableView_dataMatrix->model();

        case 1 : return ui->tableView_rowAnnotations->model();

        case 2 : return ui->tableView_columnAnnotations->model();

        default : return nullptr;

    }

}

QTableView *DatasetWidget::currentTableView()
{

    switch (ui->tabWidget->currentIndex()) {

        case 0 : return ui->tableView_dataMatrix;

        case 1 : return ui->tableView_rowAnnotations;

        case 2 : return ui->tableView_columnAnnotations;

        default : return nullptr;

    }

}

int DatasetWidget::indexOfSelectedTab() const
{

    return ui->tabWidget->currentIndex();

}

void DatasetWidget::delete_headerActivated()
{

    QHeaderView *headerView = static_cast<QHeaderView *>(d_deleteAction_headerActivated->property("headerView").value<void *>());

    if (headerView->orientation() == Qt::Vertical) {

        QModelIndexList selectedRowModelIndexes = headerView->selectionModel()->selectedRows();

        QList<int> indexesToRemove;

        for (const QModelIndex &modelIndex: selectedRowModelIndexes)
            indexesToRemove << modelIndex.row();

        if (indexesToRemove.isEmpty())
            indexesToRemove << d_deleteAction_headerActivated->property("index").toInt();

        std::sort(indexesToRemove.begin(), indexesToRemove.end(), std::greater<int>());

        for (const int &indexToRemove: indexesToRemove)
            headerView->model()->removeRow(indexToRemove);

    } else {

        QModelIndexList selectedColumnModelIndexes = headerView->selectionModel()->selectedColumns();

        QList<int> indexesToRemove;

        for (const QModelIndex &modelIndex: selectedColumnModelIndexes)
            indexesToRemove << modelIndex.column();

        if (indexesToRemove.isEmpty())
            indexesToRemove << d_deleteAction_headerActivated->property("index").toInt();

       std::sort(indexesToRemove.begin(), indexesToRemove.end(), std::greater<int>());

        for (const int &indexToRemove: indexesToRemove)
            headerView->model()->removeColumn(indexToRemove);

    }

}

void DatasetWidget::deselectData_headerActivated()
{

    QHeaderView *headerView = static_cast<QHeaderView *>(d_deselectDataAction_headerActivated->property("headerView").value<void *>());

    QList<int> indexes;

    if (headerView->orientation() == Qt::Vertical) {

        QModelIndexList selectedRowModelIndexes = headerView->selectionModel()->selectedRows();

        for (const QModelIndex &modelIndex: selectedRowModelIndexes)
            indexes << modelIndex.row();

    } else {

        QModelIndexList selectedColumnModelIndexes = headerView->selectionModel()->selectedColumns();

        for (const QModelIndex &modelIndex: selectedColumnModelIndexes)
            indexes << modelIndex.column();

    }

    if (indexes.isEmpty())
        indexes << d_deselectDataAction_headerActivated->property("index").toInt();

    this->beginChange();

    d_baseDataset->header(static_cast<BaseMatrix::Orientation>(d_deselectDataAction_headerActivated->property("orientation").toInt())).setSelectionStatuses(indexes, false);

    this->endChange();

}

void DatasetWidget::endChange()
{

    static_cast<MatrixModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_dataMatrix->model())->sourceModel())->endReset();

    static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->sourceModel())->endReset();

    static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->sourceModel())->endReset();

}

void DatasetWidget::insert_headerActivated()
{

    QHeaderView *headerView = static_cast<QHeaderView *>(d_insertAction_headerActivated->property("headerView").value<void *>());

    QList<int> indexes;

    if (headerView->orientation() == Qt::Vertical) {

        QModelIndexList selectedRowModelIndexes = headerView->selectionModel()->selectedRows();

        for (const QModelIndex &modelIndex: selectedRowModelIndexes)
            indexes << modelIndex.row();

    } else {

        QModelIndexList selectedColumnModelIndexes = headerView->selectionModel()->selectedColumns();

        for (const QModelIndex &modelIndex: selectedColumnModelIndexes)
            indexes << modelIndex.column();

    }

    if (indexes.isEmpty())
        indexes << d_insertAction_headerActivated->property("index").toInt();

    std::sort(indexes.begin(), indexes.end(), std::greater<int>());

    int compareIndex = indexes.first();

    int lengthOfConsecutiveRange = 1;

    for (int i = 1; i < indexes.size(); ++i) {

        if ((compareIndex - 1) != indexes.at(i)) {

            if (headerView->orientation() == Qt::Vertical)
                headerView->model()->insertRows(compareIndex, lengthOfConsecutiveRange);
            else
                headerView->model()->insertColumns(compareIndex, lengthOfConsecutiveRange);

            compareIndex = indexes.at(i);

            lengthOfConsecutiveRange = 1;

        } else {

            --compareIndex;

            ++lengthOfConsecutiveRange;

        }

    }

    if (headerView->orientation() == Qt::Vertical)
        headerView->model()->insertRows(compareIndex, lengthOfConsecutiveRange);
    else
        headerView->model()->insertColumns(compareIndex, lengthOfConsecutiveRange);

}

void DatasetWidget::headerDataForMatrixModelChanged(Qt::Orientation orientation, int first, int last)
{

    if (orientation == Qt::Vertical)
        emit static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->sourceModel())->headerDataChanged(orientation, first, last);
    else
        emit static_cast<AnnotationsModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->sourceModel())->headerDataChanged(orientation, first, last);

}

void DatasetWidget::matrixDataBeginChange()
{

    static_cast<MatrixModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_dataMatrix->model())->sourceModel())->beginReset();

}

void DatasetWidget::matrixDataEndChange()
{

    static_cast<MatrixModel *>(static_cast<QSortFilterProxyModel *>(ui->tableView_dataMatrix->model())->sourceModel())->endReset();

}

void DatasetWidget::renameAnnotationLabel_headerActivated()
{

    QString text = static_cast<QLineEdit *>(d_renameAnnotationLabelWidgetAction_headerActivated->defaultWidget())->text();

    if (text.isEmpty())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(d_renameAnnotationLabelWidgetAction_headerActivated->property("headerView").value<void *>());

    if (headerView->orientation() == Qt::Vertical)
        headerView->model()->setHeaderData(headerView->selectionModel()->selectedRows().first().row(), Qt::Vertical, text);
    else
        headerView->model()->setHeaderData(headerView->selectionModel()->selectedColumns().first().column(), Qt::Horizontal, text);

}

void DatasetWidget::renameIdentifier_headerActivated()
{

    QString text = static_cast<QLineEdit *>(d_renameIdentifierWidgetAction_headerActivated->defaultWidget())->text();

    if (text.isEmpty())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(d_renameIdentifierWidgetAction_headerActivated->property("headerView").value<void *>());

    if (headerView->orientation() == Qt::Vertical)
        headerView->model()->setHeaderData(d_renameIdentifierWidgetAction_headerActivated->property("index").toInt(), Qt::Vertical, text);
    else
        headerView->model()->setHeaderData(d_renameIdentifierWidgetAction_headerActivated->property("index").toInt(), Qt::Horizontal, text);

}

void DatasetWidget::selectData_headerActivated()
{

    QHeaderView *headerView = static_cast<QHeaderView *>(d_selectDataAction_headerActivated->property("headerView").value<void *>());

    QList<int> indexes;

    if (headerView->orientation() == Qt::Vertical) {

        QModelIndexList selectedRowModelIndexes = headerView->selectionModel()->selectedRows();

        for (const QModelIndex &modelIndex: selectedRowModelIndexes)
            indexes << modelIndex.row();

    } else {

        QModelIndexList selectedColumnModelIndexes = headerView->selectionModel()->selectedColumns();

        for (const QModelIndex &modelIndex: selectedColumnModelIndexes)
            indexes << modelIndex.column();

    }

    if (indexes.isEmpty())
        indexes << d_selectDataAction_headerActivated->property("index").toInt();

    this->beginChange();

    d_baseDataset->header(static_cast<BaseMatrix::Orientation>(d_selectDataAction_headerActivated->property("orientation").toInt())).setSelectionStatuses(indexes, true);

    this->endChange();

}

void DatasetWidget::showContextMenuVerticalHeaderDataMatrix(const QPoint &pos)
{

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    QTableView *tableView = static_cast<QTableView *>(headerView->parent());

    int index = headerView->logicalIndexAt(headerView->orientation() == Qt::Vertical ? pos.y() : pos.x());

    if (index < 0)
        return;

    if (!headerView->selectionModel()->isRowSelected(index, QModelIndex())) {

        headerView->selectionModel()->clear();

        tableView->selectRow(index);

    }

    d_copyAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_copyWithHeaderLabelsAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_copyToAnnotationsAction_headerActivated->setText("Copy to column annotations");

    d_copyToAnnotationsAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_copyToAnnotationsAction_headerActivated->setVisible(tableView != ui->tableView_rowAnnotations);

    d_deleteAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_deleteAction_headerActivated->setProperty("index", index);

    d_insertAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_insertAction_headerActivated->setProperty("index", index);

    d_selectDataAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_selectDataAction_headerActivated->setProperty("orientation", BaseMatrix::ROW);

    d_selectDataAction_headerActivated->setProperty("index", index);

    d_deselectDataAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_deselectDataAction_headerActivated->setProperty("orientation", BaseMatrix::ROW);

    d_deselectDataAction_headerActivated->setProperty("index", index);

    QModelIndexList selectedRowModelIndexes = tableView->selectionModel()->selectedRows();

    if (selectedRowModelIndexes.size() > 1)
        d_renameIdentifierWidgetAction_headerActivated->setVisible(false);
    else {

        d_renameIdentifierWidgetAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

        d_renameIdentifierWidgetAction_headerActivated->setProperty("index", index);

        static_cast<QLineEdit *>(d_renameIdentifierWidgetAction_headerActivated->defaultWidget())->setText(headerView->model()->headerData(index, Qt::Vertical).toString());

        d_renameIdentifierWidgetAction_headerActivated->setVisible(true);

    }

    d_copyAction_headerActivated->setVisible(!selectedRowModelIndexes.isEmpty());

    d_copyWithHeaderLabelsAction_headerActivated->setVisible(!selectedRowModelIndexes.isEmpty());

    d_dataMatrixHeaderMenu->exec(QCursor::pos());

}

void DatasetWidget::showContextMenuHorizontalHeaderDataMatrix(const QPoint &pos)
{

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    QTableView *tableView = static_cast<QTableView *>(headerView->parent());

    int index = headerView->logicalIndexAt(headerView->orientation() == Qt::Horizontal ? pos.x() : pos.y());

    if (index < 0)
        return;

    if (tableView != ui->tableView_columnAnnotations) {

        if (!headerView->selectionModel()->isColumnSelected(index, QModelIndex())) {

            headerView->selectionModel()->clear();

            tableView->selectColumn(index);

        }

    } else {

        if (!headerView->selectionModel()->isRowSelected(index, QModelIndex())) {

            headerView->selectionModel()->clear();

            tableView->selectRow(index);

        }

    }

    d_copyAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_copyWithHeaderLabelsAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_copyToAnnotationsAction_headerActivated->setText("Copy to row annotations");

    d_copyToAnnotationsAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_copyToAnnotationsAction_headerActivated->setVisible(tableView != ui->tableView_columnAnnotations);

    d_deleteAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_deleteAction_headerActivated->setProperty("index", index);

    d_insertAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_insertAction_headerActivated->setProperty("index", index);

    d_selectDataAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_selectDataAction_headerActivated->setProperty("orientation", BaseMatrix::COLUMN);

    d_selectDataAction_headerActivated->setProperty("index", index);

    d_deselectDataAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_deselectDataAction_headerActivated->setProperty("orientation", BaseMatrix::COLUMN);

    d_deselectDataAction_headerActivated->setProperty("index", index);

    if (tableView != ui->tableView_columnAnnotations) {

        QModelIndexList selectedColumnModelIndexes = tableView->selectionModel()->selectedColumns();

        if (selectedColumnModelIndexes.size() > 1)
            d_renameIdentifierWidgetAction_headerActivated->setVisible(false);
        else {

            d_renameIdentifierWidgetAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

            d_renameIdentifierWidgetAction_headerActivated->setProperty("index", index);

            static_cast<QLineEdit *>(d_renameIdentifierWidgetAction_headerActivated->defaultWidget())->setText(headerView->model()->headerData(index, Qt::Horizontal).toString());

            d_renameIdentifierWidgetAction_headerActivated->setVisible(true);

        }

        d_copyAction_headerActivated->setVisible(!selectedColumnModelIndexes.isEmpty());

        d_copyWithHeaderLabelsAction_headerActivated->setVisible(!selectedColumnModelIndexes.isEmpty());

    } else {

        QModelIndexList selectedRowModelIndexes = tableView->selectionModel()->selectedRows();

        if (selectedRowModelIndexes.size() > 1)
            d_renameIdentifierWidgetAction_headerActivated->setVisible(false);
        else {

            d_renameIdentifierWidgetAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

            d_renameIdentifierWidgetAction_headerActivated->setProperty("index", index);

            static_cast<QLineEdit *>(d_renameIdentifierWidgetAction_headerActivated->defaultWidget())->setText(headerView->model()->headerData(index, Qt::Vertical).toString());

            d_renameIdentifierWidgetAction_headerActivated->setVisible(true);

        }

        d_copyAction_headerActivated->setVisible(!selectedRowModelIndexes.isEmpty());

        d_copyWithHeaderLabelsAction_headerActivated->setVisible(!selectedRowModelIndexes.isEmpty());

    }

    d_dataMatrixHeaderMenu->exec(QCursor::pos());

}

void DatasetWidget::showContextMenuHorizontalHeaderAnnotations(const QPoint &pos)
{

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    QTableView *tableView = static_cast<QTableView *>(headerView->parent());

    QModelIndex index = tableView->indexAt(pos);

    if (!index.isValid())
        return;

    if (!headerView->selectionModel()->isColumnSelected(index.column(), index.parent())) {

        headerView->selectionModel()->clear();

        static_cast<QTableView *>(headerView->parent())->selectColumn(index.column());

    }

    d_copyAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_copyWithHeaderLabelsAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_deleteAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    d_insertAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

    QModelIndexList selectedColumnModelIndexes = tableView->selectionModel()->selectedColumns();

    if (selectedColumnModelIndexes.size() == 1) {

        d_renameAnnotationLabelWidgetAction_headerActivated->setProperty("headerView", QVariant::fromValue(static_cast<void *>(headerView)));

        static_cast<QLineEdit *>(d_renameAnnotationLabelWidgetAction_headerActivated->defaultWidget())->setText(tableView->model()->headerData(selectedColumnModelIndexes.first().column(), Qt::Horizontal).toString());

        d_renameAnnotationLabelWidgetAction_headerActivated->setVisible(true);

    } else
        d_renameAnnotationLabelWidgetAction_headerActivated->setVisible(false);

    d_annotationsHeaderMenu->exec(QCursor::pos());

}

void DatasetWidget::showSelectedOnly(bool value)
{

    this->beginChange();

    static_cast<MatrixSortFilterProxyModel *>(ui->tableView_dataMatrix->model())->setShowSelectedOnly(value);

    static_cast<AnnotationsSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->setShowSelectedOnly(value);

    static_cast<AnnotationsSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->setShowSelectedOnly(value);

    this->endChange();

}
