#include "math/vectoroperations.h"

QVector<double> VectorOperations::vsAdd(const QVector<double> &vector, const double &scalar)
{

    QVector<double> outputVector(vector.size());

    #ifdef Q_OS_MAC

    vDSP_vsaddD(vector.data(), 1, &scalar, outputVector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (int i = 0; i < vector.size(); ++i)
        outputVector[i] = vector.at(i) + scalar;

    #endif

    return outputVector;

}

QVector<double> &VectorOperations::vsAdd_inplace(QVector<double> &vector, const double &scalar)
{

    #ifdef Q_OS_MAC

    vDSP_vsaddD(vector.data(), 1, &scalar, vector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (double &value : vector)
        value += scalar;

    #endif

    return vector;

}

QVector<double> VectorOperations::vsSub(const QVector<double> &vector, const double &scalar)
{

    QVector<double> outputVector(vector.size());

    #ifdef Q_OS_MAC

    double temp = -scalar;

    vDSP_vsaddD(vector.data(), 1, &temp, outputVector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (int i = 0; i < vector.size(); ++i)
        outputVector[i] = vector.at(i) - scalar;

    #endif

    return outputVector;

}

QVector<double> &VectorOperations::vsSub_inplace(QVector<double> &vector, const double &scalar)
{

    #ifdef Q_OS_MAC

    double temp = -scalar;

    vDSP_vsaddD(vector.data(), 1, &temp, vector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (double &value : vector)
        value -= scalar;

    #endif

    return vector;

}

QVector<double> VectorOperations::svSub(const double &scalar, const QVector<double> &vector)
{

    QVector<double> outputVector(vector.size());

    for (int i = 0; i < vector.size(); ++i)
        outputVector[i] = scalar - vector.at(i);

    return outputVector;

}

QVector<double> &VectorOperations::svSub_inplace(const double &scalar, QVector<double> &vector)
{

    for (double &value : vector)
        value = scalar - value;

    return vector;

}

QVector<double> VectorOperations::vsMul(const QVector<double> &vector, const double &scalar)
{

    QVector<double> outputVector(vector.size());

    #ifdef Q_OS_MAC

    vDSP_vsmulD(vector.data(), 1, &scalar, outputVector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (int i = 0; i < vector.size(); ++i)
        outputVector[i] = vector.at(i) * scalar;

    #endif

    return outputVector;

}

QVector<double> &VectorOperations::vsMul_inplace(QVector<double> &vector, const double &scalar)
{

    #ifdef Q_OS_MAC

    vDSP_vsmulD(vector.data(), 1, &scalar, vector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (double &value : vector)
        value *= scalar;

    #endif

    return vector;

}

QVector<double> VectorOperations::vsDiv(const QVector<double> &vector, const double &scalar)
{
    QVector<double> outputVector(vector.size());

    #ifdef Q_OS_MAC

    vDSP_vsdivD(vector.data(), 1, &scalar, outputVector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (int i = 0; i < vector.size(); ++i)
        outputVector[i] = vector.at(i) / scalar;

    #endif

    return outputVector;

}

QVector<double> &VectorOperations::vsDiv_inplace(QVector<double> &vector, const double &scalar)
{

    #ifdef Q_OS_MAC

    vDSP_vsdivD(vector.data(), 1, &scalar, vector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (double &value : vector)
        value /= scalar;

    #endif

    return vector;

}

QVector<double> VectorOperations::svDiv(const double &scalar, const QVector<double> &vector)
{

    QVector<double> outputVector(vector.size());

    #ifdef Q_OS_MAC

    vDSP_svdivD(&scalar, vector.data(), 1, outputVector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (int i = 0; i < vector.size(); ++i)
        outputVector[i] = scalar / vector.at(i);

    #endif

    return outputVector;

}

QVector<double> &VectorOperations::svDiv_inplace(const double &scalar, QVector<double> &vector)
{

    #ifdef Q_OS_MAC

    vDSP_svdivD(&scalar, vector.data(), 1, vector.data(), 1, static_cast<unsigned long>(vector.size()));

    #else

    for (double &value : vector)
        value = scalar / value;

    #endif

    return vector;

}

QVector<double> VectorOperations::vsMul_vsMul_vvAdd(const QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2)
{

    QVector<double> outputVector(vector1.size());

    #ifdef Q_OS_MAC

    vDSP_vsmsmaD(vector1.data(), 1, &scalar1, vector2.data(), 1, &scalar2, outputVector.data(), 1, static_cast<unsigned long>(vector1.size()));

    #else

    if (scalar1 == 1.0) {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = vector1.at(i) + vector2.at(i);

        } else {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = vector1.at(i) + vector2.at(i) * scalar2;

        }


    } else {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = scalar1 * vector1.at(i) + vector2.at(i);

        } else {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = scalar1 * vector1.at(i) + scalar2 * vector2.at(i);

        }

    }

    #endif

    return outputVector;

}

QVector<double> &VectorOperations::vsMul_vsMul_vvAdd_inplace(QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2)
{

    #ifdef Q_OS_MAC

    vDSP_vsmsmaD(vector1.data(), 1, &scalar1, vector2.data(), 1, &scalar2, vector1.data(), 1, static_cast<unsigned long>(vector1.size()));

    #else

    if (scalar1 == 1.0) {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i)
                vector1[i] += vector2.at(i);

        } else {

            for (int i = 0; i < vector1.size(); ++i)
                vector1[i] += vector2.at(i) * scalar2;

        }


    } else {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i) {

                double &value(vector1[i]);

                value = value * scalar1 + vector2.at(i);

            }

        } else {

            for (int i = 0; i < vector1.size(); ++i) {

                double &value(vector1[i]);

                value = value * scalar1 + vector2.at(i) * scalar2;

            }

        }

    }

    #endif

    return vector1;

}

QVector<double> VectorOperations::vsMul_vsMul_vvMul(const QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2)
{

    QVector<double> outputVector(vector1.size());

    if (scalar1 == 1.0) {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = vector1.at(i) * vector2.at(i);

        } else {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = vector1.at(i) * vector2.at(i) * scalar2;

        }


    } else {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = scalar1 * vector1.at(i) * vector2.at(i);

        } else {

            for (int i = 0; i < vector1.size(); ++i)
                outputVector[i] = scalar1 * vector1.at(i) * scalar2 * vector2.at(i);

        }

    }

    return outputVector;

}

QVector<double> &VectorOperations::vsMul_vsMul_vvMul_inplace(QVector<double> &vector1, const double &scalar1, const QVector<double> &vector2, const double &scalar2)
{

    if (scalar1 == 1.0) {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i)
                vector1[i] *= vector2.at(i);

        } else {

            for (int i = 0; i < vector1.size(); ++i)
                vector1[i] *= vector2.at(i) * scalar2;

        }


    } else {

        if (scalar2 == 1.0) {

            for (int i = 0; i < vector1.size(); ++i) {

                double &value(vector1[i]);

                value = value * scalar1 * vector2.at(i);

            }

        } else {

            for (int i = 0; i < vector1.size(); ++i) {

                double &value(vector1[i]);

                value = value * scalar1 * vector2.at(i) * scalar2;

            }

        }

    }

    return vector1;

}

long double VectorOperations::dotProduct(const QVector<double> &vector1, const QVector<double> &vector2)
{

    if (vector1.isEmpty())
        return std::numeric_limits<long double>::quiet_NaN();

    long double _dotProduct = static_cast<long double>(0.0);

    for (int i = 0; i < vector1.size(); ++i)
        _dotProduct += static_cast<long double>(vector1.at(i) * vector2.at(i));

    return _dotProduct;

}

QVector<double> VectorOperations::standardizeVector(const QVector<double> &vector)
{

    QVector<double> result = vector;

    #ifdef Q_OS_MAC

    double mean;

    double standardDeviation;

    vDSP_normalizeD(vector.data(), 1, result.data(), 1, &mean, &standardDeviation, static_cast<unsigned long>(vector.size()));

    #else

    QPair<double, double> meanAndStandardDeviation = MathDescriptives::meanAndStandardDeviation(vector);

    for (double &value : result)
        value = (value - meanAndStandardDeviation.first) / meanAndStandardDeviation.second;

    #endif

    return result;

}

QVector<double> VectorOperations::standardizeVector_inplace(QVector<double> &vector)
{

    #ifdef Q_OS_MAC

    double mean;

    double standardDeviation;

    vDSP_normalizeD(vector.data(), 1, vector.data(), 1, &mean, &standardDeviation, static_cast<unsigned long>(vector.size()));

    #else

    QPair<double, double> meanAndStandardDeviation = MathDescriptives::meanAndStandardDeviation(vector);

    for (double &value : vector)
        value = (value - meanAndStandardDeviation.first) / meanAndStandardDeviation.second;

    #endif

    return vector;

}

QVector<double> VectorOperations::normalizeVector(const QVector<double> &vector)
{

    return VectorOperations::vsDiv(vector, MathDescriptives::l2Norm(vector));

}

QVector<double> &VectorOperations::normalizeVector_inplace(QVector<double> &vector)
{

    return VectorOperations::vsDiv_inplace(vector, MathDescriptives::l2Norm(vector));

}

QVector<double> VectorOperations::centerToMeanOfZero(const QVector<double> &vector)
{

    return VectorOperations::vsAdd(vector, -MathDescriptives::mean(vector));

}

QVector<double> &VectorOperations::centerToMeanOfZero_inplace(QVector<double> &vector)
{

    return VectorOperations::vsAdd_inplace(vector, -MathDescriptives::mean(vector));

}

QVector<double> VectorOperations::scaleVectorToStandardDeviation(const QVector<double> &vector)
{

    return VectorOperations::vsDiv(vector, MathDescriptives::standardDeviation(vector));

}

QVector<double> &VectorOperations::scaleVectorToStandardDeviation_inplace(QVector<double> &vector)
{

    return VectorOperations::vsDiv_inplace(vector, MathDescriptives::standardDeviation(vector));

}

QVector<double> VectorOperations::scaleVectorToStandardDeviation_x_SqrtOfVectorSizeMinusOne(const QVector<double> &vector)
{

    return VectorOperations::vsDiv(vector, std::sqrt(static_cast<double>(vector.size() - 1.0)) * MathDescriptives::standardDeviation(vector));

}

QVector<double> &VectorOperations::scaleVectorToStandardDeviation_x_SqrtOfVectorSizeMinusOne_inplace(QVector<double> &vector)
{

    return VectorOperations::vsDiv_inplace(vector, std::sqrt(static_cast<double>(vector.size() - 1.0)) * MathDescriptives::standardDeviation(vector));

}

QVector<double> VectorOperations::scaleVectorToUnitVariance(const QVector<double> &vector)
{

    return VectorOperations::vsDiv(vector, MathDescriptives::variance(vector));

}

QVector<double> &VectorOperations::scaleVectorToUnitVariance_inplace(QVector<double> &vector)
{

    return VectorOperations::vsDiv_inplace(vector, MathDescriptives::variance(vector));

}

QVector<double> &VectorOperations::fillWithRandomNumbersFromUniformRealDistribution(QVector<double> &vector, const double &minimumValue, const double &maximumValue)
{

    std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());

    std::uniform_real_distribution<double> distribution(minimumValue, maximumValue);

    for (double &value : vector)
        value = distribution(generator);

    return vector;

}
