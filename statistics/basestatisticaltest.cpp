#include "basestatisticaltest.h"

BaseStatisticalTest::BaseStatisticalTest(const QString &nameOfStatisticalTest, const QStringList &testDescriptiveLabels, const QStringList &sampleDescriptiveLabels, const bool &rankedBased, const QString &nameOfEffectSize, const BaseStatisticalTest::EffectSizeTransformation &effectSizeTransformation, const bool &hasEffectSize, const QString &nameOfTransformedEffectSize, const int &maximumNumberOfSamples, const int &minimumNumberOfItemsPerSample, const int &minimumNumberOfSamples) :
    d_effectSize(std::numeric_limits<double>::quiet_NaN()),
    d_effectSizeTransformation(effectSizeTransformation),
    d_hasEffectSize(hasEffectSize),
    d_sampleDescriptiveLabels(sampleDescriptiveLabels),
    d_maximumNumberOfSamples(maximumNumberOfSamples),
    d_minimumNumberOfItemsPerSample(minimumNumberOfItemsPerSample),
    d_minimumNumberOfSamples(minimumNumberOfSamples),
    d_nameOfEffectSize(nameOfEffectSize),
    d_nameOfStatisticalTest(nameOfStatisticalTest),
    d_nameOfTransformedEffectSize(nameOfTransformedEffectSize),
    d_pValue(std::numeric_limits<double>::quiet_NaN()),
    d_rankedBased(rankedBased),
    d_sampleSize(std::numeric_limits<double>::quiet_NaN()),
    d_standardErrorOfEffectSize(std::numeric_limits<double>::quiet_NaN()),
    d_statistic(std::numeric_limits<double>::quiet_NaN()),
    d_testDescriptiveLabels(testDescriptiveLabels)
{

}

BaseStatisticalTest::~BaseStatisticalTest()
{

}

const double &BaseStatisticalTest::effectSize()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_effectSize))
        this->calculateEffectSize();

    return d_effectSize;

}

const BaseStatisticalTest::EffectSizeTransformation &BaseStatisticalTest::effectSizeTransformation() const
{

    return d_effectSizeTransformation;

}

const bool &BaseStatisticalTest::hasEffectSize() const
{

    return d_hasEffectSize;

}

QStringList BaseStatisticalTest::sampleDescriptiveLabels(const QStringList &sampleIdentifiers) const
{

    QStringList _sampleDescriptiveLabels;

    for (const QString &sampleIdentifier : sampleIdentifiers) {

        for (const QString &sampleDescriptiveLabel: d_sampleDescriptiveLabels)
            _sampleDescriptiveLabels << (sampleDescriptiveLabel + " \"" + sampleIdentifier + "\"");

    }

    return _sampleDescriptiveLabels;

}

QVector<QVariant> BaseStatisticalTest::samplesDescriptiveValues()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    return this->calculateSamplesDescriptivesValues();

}

const bool &BaseStatisticalTest::isRankedBased() const
{

    return d_rankedBased;

}

bool BaseStatisticalTest::isSignOfStatisticNegative() const
{

    return std::signbit(d_statistic);

}

int BaseStatisticalTest::maximumNumberOfSamples() const
{

    return d_maximumNumberOfSamples;

}

int BaseStatisticalTest::minimumNumberOfItemsPerSample() const
{

    return d_minimumNumberOfItemsPerSample;

}

int BaseStatisticalTest::minimumNumberOfSamples() const
{

    return d_minimumNumberOfSamples;

}

double BaseStatisticalTest::minusLog10TransformedPValue()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatePValue();

    if (std::isnan(d_pValue))
        return std::numeric_limits<double>::quiet_NaN();

    return -std::log10(d_pValue);

}

const QString &BaseStatisticalTest::nameOfStatisticalTest() const
{

    return d_nameOfStatisticalTest;

}

const QString &BaseStatisticalTest::nameOfEffectSize() const
{

    return d_nameOfEffectSize;

}

const QString &BaseStatisticalTest::nameOfTransformedEffectSize() const
{

    return d_nameOfTransformedEffectSize;

}

const double &BaseStatisticalTest::pValue()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatePValue();

    return d_pValue;

}

void BaseStatisticalTest::reset()
{

    d_sampleSize = 0;

    d_pValue = std::numeric_limits<double>::quiet_NaN();

    d_statistic = std::numeric_limits<double>::quiet_NaN();

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

    this->_reset();

}

double BaseStatisticalTest::sampleSize()
{

    return d_sampleSize;

}

const double &BaseStatisticalTest::standardErrorOfEffectSize()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_effectSize))
        this->calculateEffectSize();

    if (std::isnan(d_standardErrorOfEffectSize))
        this->calculateStandardErrorOfEffectSize();

    return d_standardErrorOfEffectSize;

}

const double &BaseStatisticalTest::statistic()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    return d_statistic;

}

const QStringList &BaseStatisticalTest::testDescriptiveLabels() const
{

    return d_testDescriptiveLabels;

}

QVector<QVariant> BaseStatisticalTest::testDescriptiveValues()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatePValue();

    if (std::isnan(d_effectSize))
        this->calculateEffectSize();

    if (std::isnan(d_standardErrorOfEffectSize))
        this->calculateStandardErrorOfEffectSize();

    return this->calculateTestDescriptivesValues();

}

double BaseStatisticalTest::transformedEffectSize()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_effectSize))
        this->calculateEffectSize();

    if (d_effectSizeTransformation == BaseStatisticalTest::NATURALLOGARITHM)
        return std::log(d_effectSize);
    else if (d_effectSizeTransformation == BaseStatisticalTest::FISHER_R_TO_Z) {

        if (d_effectSize == 1.0)
            return std::numeric_limits<double>::max();

        return 0.5 * std::log((1.0 + d_effectSize) / (1.0 - d_effectSize));

    }

    return std::numeric_limits<double>::quiet_NaN();

}

double BaseStatisticalTest::zTransformedPValue()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatePValue();

    if (std::isnan(d_pValue))
        return std::numeric_limits<double>::quiet_NaN();

    if (d_pValue == 1.0)
        return std::copysign(0.0, d_statistic);

    boost::math::normal dist(0.0, 1.0);

    return this->isSignOfStatisticNegative() ? boost::math::quantile(dist, d_pValue / 2.0) : -boost::math::quantile(dist, d_pValue / 2.0);

}
