#ifndef IODATASTREAM_H
#define IODATASTREAM_H

#include <QDataStream>
#include <QAbstractAxis>
#include <QBarCategoryAxis>
#include <QDateTimeAxis>
#include <QDateTime>
#include <QLogValueAxis>
#include <QValueAxis>
#include <QCategoryAxis>
#include <QAbstractSeries>
#include <QAbstractBarSeries>
#include <QBarSeries>
#include <QHorizontalBarSeries>
#include <QHorizontalPercentBarSeries>
#include <QHorizontalStackedBarSeries>
#include <QPercentBarSeries>
#include <QStackedBarSeries>
#include <QAreaSeries>
#include <QBoxPlotSeries>
#include <QPieSeries>
#include <QXYSeries>
#include <QLineSeries>
#include <QSplineSeries>
#include <QScatterSeries>
#include <QCandlestickSeries>
#include <QLegend>
#include <QPen>
#include <QBrush>
#include <QFont>
#include <QBarSet>
#include <QBoxSet>
#include <QPieSlice>
#include <QChart>
#include <QEasingCurve>

using namespace QtCharts;

QDataStream& operator<<(QDataStream& out, const QAbstractAxis &abstractAxis);

QDataStream& operator>>(QDataStream& in, QAbstractAxis &abstractAxis);

QDataStream& operator<<(QDataStream& out, const QBarCategoryAxis &barCategoryAxis);

QDataStream& operator>>(QDataStream& in, QBarCategoryAxis &barCategoryAxis);

QDataStream& operator<<(QDataStream& out, const QDateTimeAxis &dateTimeAxis);

QDataStream& operator>>(QDataStream& in, QDateTimeAxis &dateTimeAxis);

QDataStream& operator<<(QDataStream& out, const QLogValueAxis &logValueAxis);

QDataStream& operator>>(QDataStream& in, QLogValueAxis &logValueAxis);

QDataStream& operator<<(QDataStream& out, const QValueAxis &valueAxis);

QDataStream& operator>>(QDataStream& in, QValueAxis &valueAxis);

QDataStream& operator<<(QDataStream& out, const QCategoryAxis &categoryAxis);

QDataStream& operator>>(QDataStream& in, QCategoryAxis &categoryAxis);

QDataStream& operator<<(QDataStream& out, const QAbstractSeries &abstractSeries);

QDataStream& operator>>(QDataStream& in, QAbstractSeries &abstractSeries);

QDataStream& operator<<(QDataStream& out, const QAbstractBarSeries &abstractBarSeries);

QDataStream& operator>>(QDataStream& in, QAbstractBarSeries &abstractBarSeries);

QDataStream& operator<<(QDataStream& out, const QBarSeries &barSeries);

QDataStream& operator>>(QDataStream& in, QBarSeries &barSeries);

QDataStream& operator<<(QDataStream& out, const QHorizontalBarSeries &horizontalBarSeries);

QDataStream& operator>>(QDataStream& in, QHorizontalBarSeries &horizontalBarSeries);

QDataStream& operator<<(QDataStream& out, const QHorizontalPercentBarSeries &horizontalPercentBarSeries);

QDataStream& operator>>(QDataStream& in, QHorizontalPercentBarSeries &horizontalPercentBarSeries);

QDataStream& operator<<(QDataStream& out, const QHorizontalStackedBarSeries &horizontalStackedBarSeries);

QDataStream& operator>>(QDataStream& in, QHorizontalStackedBarSeries &horizontalStackedBarSeries);

QDataStream& operator<<(QDataStream& out, const QPercentBarSeries &percentBarSeries);

QDataStream& operator>>(QDataStream& in, QPercentBarSeries &percentBarSeries);

QDataStream& operator<<(QDataStream& out, const QStackedBarSeries &stackedBarSeries);

QDataStream& operator>>(QDataStream& in, QStackedBarSeries &stackedBarSeries);

QDataStream& operator<<(QDataStream& out, const QAreaSeries &areaSeries);

QDataStream& operator>>(QDataStream& in, QAreaSeries &areaSeries);

QDataStream& operator<<(QDataStream& out, const QBoxPlotSeries &boxPlotSeries);

QDataStream& operator>>(QDataStream& in, QBoxPlotSeries &boxPlotSeries);

QDataStream& operator<<(QDataStream& out, const QPieSeries &pieSeries);

QDataStream& operator>>(QDataStream& in, QPieSeries &pieSeries);

QDataStream& operator<<(QDataStream& out, const QXYSeries &xySeries);

QDataStream& operator>>(QDataStream& in, QXYSeries &xySeries);

QDataStream& operator<<(QDataStream& out, const QLineSeries &lineSeries);

QDataStream& operator>>(QDataStream& in, QLineSeries &lineSeries);

QDataStream& operator<<(QDataStream& out, const QSplineSeries &splineSeries);

QDataStream& operator>>(QDataStream& in, QSplineSeries &splineSeries);

QDataStream& operator<<(QDataStream& out, const QScatterSeries &scatterSeries);

QDataStream& operator>>(QDataStream& in, QScatterSeries &scatterSeries);

QDataStream& operator<<(QDataStream& out, const QBarSet &barSet);

QDataStream& operator>>(QDataStream& in, QBarSet &barSet);

QDataStream& operator<<(QDataStream& out, const QBoxSet &boxSet);

QDataStream& operator>>(QDataStream& in, QBoxSet &boxSet);

QDataStream& operator<<(QDataStream& out, const QPieSlice &pieSlice);

QDataStream& operator>>(QDataStream& in, QPieSlice &pieSlice);

QDataStream& operator<<(QDataStream& out, const QChart &chart);

QDataStream& operator>>(QDataStream& in, QChart &chart);

QDataStream& operator<<(QDataStream& out, const QLegend &legend);

QDataStream& operator>>(QDataStream& in, QLegend &legend);

#endif // IODATASTREAM_H
