#ifndef BASESTATISTICALTEST_H
#define BASESTATISTICALTEST_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>

#include <cmath>

#include "boost/math/distributions/normal.hpp"

class BaseStatisticalTest
{

public:

    enum EffectSizeTransformation {

        NOTRANSFORMATION = 0,

        NATURALLOGARITHM = 1,

        FISHER_R_TO_Z = 3

    };

    BaseStatisticalTest(const QString &nameOfStatisticalTest, const QStringList &testDescriptiveLabels, const QStringList &sampleDescriptiveLabels, const bool &rankedBased, const QString &nameOfEffectSize, const EffectSizeTransformation &effectSizeTransformation, const bool &hasEffectSize, const QString &nameOfEffectSizeTransformation, const int &maximumNumberOfSamples, const int &minimumNumberOfItemsPerSample, const int &minimumNumberOfSamples);

    virtual ~BaseStatisticalTest();

    const double &effectSize();

    const EffectSizeTransformation &effectSizeTransformation() const;

    const bool &hasEffectSize() const;

    QStringList sampleDescriptiveLabels(const QStringList &sampleIdentifiers) const;

    QVector<QVariant> samplesDescriptiveValues();

    const bool &isRankedBased() const;

    bool isSignOfStatisticNegative() const;

    double minusLog10TransformedPValue();

    int maximumNumberOfSamples() const;

    int minimumNumberOfItemsPerSample() const;

    int minimumNumberOfSamples() const;

    const QString &nameOfStatisticalTest() const;

    const QString &nameOfEffectSize() const;

    const QString &nameOfTransformedEffectSize() const;

    const double &pValue();

    void reset();

    double sampleSize();

    const double &standardErrorOfEffectSize();

    const double &statistic();

    const QStringList &testDescriptiveLabels() const;

    QVector<QVariant> testDescriptiveValues();

    double transformedEffectSize();

    double zTransformedPValue();

protected:

    virtual void _reset() = 0;

    double d_effectSize;

    const EffectSizeTransformation &d_effectSizeTransformation;

    const bool &d_hasEffectSize;

    const QStringList &d_sampleDescriptiveLabels;

    const int &d_maximumNumberOfSamples;

    const int &d_minimumNumberOfItemsPerSample;

    const int &d_minimumNumberOfSamples;

    const QString &d_nameOfEffectSize;

    const QString &d_nameOfStatisticalTest;

    const QString &d_nameOfTransformedEffectSize;

    double d_pValue;

    const bool &d_rankedBased;

    double d_sampleSize;

    double d_standardErrorOfEffectSize;

    double d_statistic;

    const QStringList &d_testDescriptiveLabels;

    virtual QVector<QVariant> calculateSamplesDescriptivesValues() = 0;

    virtual void calculateStatistic() = 0;

    virtual void calculatePValue() = 0;

    virtual void calculateEffectSize() = 0;

    virtual void calculateStandardErrorOfEffectSize() = 0;

    virtual QVector<QVariant> calculateTestDescriptivesValues() = 0;

};

#endif // BASESTATISTICALTEST_H
