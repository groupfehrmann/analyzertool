#ifndef CALCULATEDESCRIPTIVESDIALOG_H
#define CALCULATEDESCRIPTIVESDIALOG_H

#include <QString>
#include <QSharedPointer>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "widgets/permutationoptionsbuildingblockwidget.h"
#include "workerclasses/calculatedescriptivesworkerclass.h"

class CalculateDescriptivesDialog : public BaseDialog
{

    Q_OBJECT

public:

    CalculateDescriptivesDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~CalculateDescriptivesDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_descriptives;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_values;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    bool d_selectedOnly;

    bool d_valuesNeeded;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void annotationLabelDefiningItemSubsetsChanged(const QString &currentAnnotationLabel);

    void selectionOfDescriptivesChanged(const QStringList &selectedDescriptives, const QStringList &notSelectedDescriptives);

};

#endif // CALCULATEDESCRIPTIVESDIALOG_H
