#ifndef MODIFYIDENTIFIERSBASEDONREGEXPDIALOG_H
#define MODIFYIDENTIFIERSBASEDONREGEXPDIALOG_H

#include "dialogs/basedialog.h"
#include "workerclasses/modifyidentifiersbasedonregexpworkerclass.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/lineeditbuildingblockwidget.h"

class ModifyIdentifiersBasedOnRegExpDialog : public BaseDialog
{

    Q_OBJECT

public:

    ModifyIdentifiersBasedOnRegExpDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);


    ~ModifyIdentifiersBasedOnRegExpDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    bool d_selectedOnly;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_identifiers;

    LineEditBuildingBlockWidget *d_lineEditBuildingBlockWidget_pattern;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_patternSyntax;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_caseSensitivity;

    LineEditBuildingBlockWidget *d_lineEditBuildingBlockWidget_stringToReplaceWith;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void lineEditBuildingBlockWidget_pattern_textChanged(const QString &text);

};

#endif // MODIFYIDENTIFIERSBASEDONREGEXPDIALOG_H
