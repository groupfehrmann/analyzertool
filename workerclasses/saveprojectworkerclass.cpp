#include "saveprojectworkerclass.h"

SaveProjectWorkerClass::SaveProjectWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<SaveProjectWorkerClass::Parameters *>(parameters))
{

}

void SaveProjectWorkerClass::doWork()
{
    emit processStarted("Save project");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    emit appendLogItem(LogListModel::PARAMETER, "project to save: \"" + d_parameters.projectToSave->name() +"\"");

    QFile fileIn(d_parameters.pathToFile);

    if (!fileIn.open(QIODevice::WriteOnly)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Saving project", 0, 0);

    QDataStream out(&fileIn);

    out << *d_parameters.projectToSave.data();

    emit stopProgress(0);

    QThread::currentThread()->quit();

}
