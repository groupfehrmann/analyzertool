#include "openplotworkerclass.h"


OpenPlotWorkerClass::OpenPlotWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<OpenPlotWorkerClass::Parameters *>(parameters))
{

}

void OpenPlotWorkerClass::doWork()
{
    emit processStarted("Open plot");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    d_fileIn.setFileName(d_parameters.pathToFile);

    if (!d_fileIn.open(QIODevice::ReadOnly)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Opening plot", 0, 0);

    bool error = false;

    QString errorMessage;

    OpenPlotFromFile openPlotFromFile(nullptr, d_parameters.pathToFile, &error, &errorMessage);

    if (error)
        emit appendLogItem(LogListModel::ERROR, errorMessage);
    else
        emit resultItemAvailable(new PlotResultItem(nullptr, openPlotFromFile.chart()->title(), openPlotFromFile.chart()));

    emit stopProgress(0);

    emit addToRecentlyOpenedFiles(openPlotFromFile.chart()->title(), d_parameters.pathToFile);

    QThread::currentThread()->quit();

}
