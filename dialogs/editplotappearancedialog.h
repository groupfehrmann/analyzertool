#ifndef EDITPLOTAPPEARANCEDIALOG_H
#define EDITPLOTAPPEARANCEDIALOG_H

#include <QDialog>
#include <QChart>
#include <QSettings>

#include "charts/setchartparameterstreewidgetitem.h"

namespace Ui {

class EditPlotAppearanceDialog;

}

class EditPlotAppearanceDialog : public QDialog
{

    Q_OBJECT

public:

    explicit EditPlotAppearanceDialog(QWidget *parent = nullptr, QChart *chart = nullptr);

    ~EditPlotAppearanceDialog();

private:

    Ui::EditPlotAppearanceDialog *ui;

};

#endif // EDITPLOTAPPEARANCEDIALOG_H
