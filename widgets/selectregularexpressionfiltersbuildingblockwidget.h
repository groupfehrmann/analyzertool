#ifndef SELECTREGULAREXPRESSIONFILTERSBUILDINGBLOCKWIDGET_H
#define SELECTREGULAREXPRESSIONFILTERSBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QStringList>
#include <QSettings>
#include <QTableWidgetItem>
#include <QSharedPointer>

#include "dialogs/basedialog.h"
#include "base/basedataset.h"
#include "base/basematrix.h"
#include "workerclasses/selectwithregularexpressionworkerclass.h"

namespace Ui {

class SelectRegularExpressionFiltersBuildingBlockWidget;

}

class SelectRegularExpressionFiltersBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectRegularExpressionFiltersBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>());

    ~SelectRegularExpressionFiltersBuildingBlockWidget();

    QList<SelectWithRegularExpressionWorkerClass::Filter> getFilters();

private:

    Ui::SelectRegularExpressionFiltersBuildingBlockWidget *ui;

    QSharedPointer<BaseDataset> d_baseDataset;

    BaseMatrix::Orientation d_orientation;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

    int d_defaultIndexForComboBox_mode;

    int d_defaultIndexForComboBox_syntax;

    bool d_defaultStatusForCheckBox_caseSensitivity;

public slots:

    void setOrientation(BaseMatrix::Orientation orientation);

private slots:

    void toolButton_add_clicked();

    void toolButton_remove_clicked();

};

#endif // SELECTREGULAREXPRESSIONFILTERSBUILDINGBLOCKWIDGET_H
