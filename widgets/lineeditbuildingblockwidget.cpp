#include "lineeditbuildingblockwidget.h"
#include "ui_lineeditbuildingblockwidget.h"

LineEditBuildingBlockWidget::LineEditBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, const QString &defaultText) :
    QWidget(parent), ui(new Ui::LineEditBuildingBlockWidget), d_rememberSettings(false)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    if (defaultText.isEmpty())
        return;
    else
        d_rememberSettings = true;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/lineEditBuildingBlockWidget");

    if (settings.contains("text"))
        ui->lineEdit->setText(settings.value("text").toString());

    settings.endGroup();

}

LineEditBuildingBlockWidget::~LineEditBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/lineEditBuildingBlockWidget");

        settings.setValue("text", ui->lineEdit->text());

        settings.endGroup();

    }

    delete ui;

}

QLineEdit *LineEditBuildingBlockWidget::pointerToInternalLineEdit()
{

    return ui->lineEdit;

}

QLabel *LineEditBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}
