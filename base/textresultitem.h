#ifndef TEXTRESULTITEM_H
#define TEXTRESULTITEM_H

#include <QObject>
#include <QString>

#include "base/baseresultitem.h"

class TextResultItem : public BaseResultItem
{

    Q_OBJECT

public:

    TextResultItem(QObject *parent = nullptr, const QString &name = "Text result item");

    void setText(const QString &text);

    const QString &text() const;

private:

    QString d_text;

};

#endif // TEXTRESULTITEM_H
