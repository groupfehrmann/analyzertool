#include "createpieplotsdialog.h"

CreatePiePlotsDialog::CreatePiePlotsDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new CreatePiePlotsWorkerClass::Parameters;

    this->setupWidgets();

}

CreatePiePlotsDialog::~CreatePiePlotsDialog()
{

   delete static_cast<CreatePiePlotsWorkerClass::Parameters *>(d_parameters);

}

void CreatePiePlotsDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 2, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets", "Select annotation label defining subsets (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets, 3, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier", "Select annotation label defining alternative pie title (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier, 4, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeItemIdentifier = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeItemIdentifier", "Select annotation label defining alternative pie slice title (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeItemIdentifier, 5, 0);

    d_comboBoxBuildingBlockWidget_addValueToLabel = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_addValueToLabel", "Add value to label", {"Enabled", "Disabled"}, {true, false});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_addValueToLabel, 6, 0);

    d_plotExportParametersBuildingBlockWidget = new PlotExportParametersBuildingBlockWidget(this, "plotExportParametersBuildingBlockWidget");

    d_gridLayout->addWidget(d_plotExportParametersBuildingBlockWidget, 7, 0);

    d_selectFileBuildingBlockWidget_templatePlot = new SelectFileBuildingBlockWidget(this, "selectFileBuildingBlockWidget_templatePlot", "Select template plot (optional)", QFileDialog::AcceptOpen, true);

    d_gridLayout->addWidget(d_selectFileBuildingBlockWidget_templatePlot, 8, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void CreatePiePlotsDialog::initializeParameters()
{

    CreatePiePlotsWorkerClass::Parameters *parameters = static_cast<CreatePiePlotsWorkerClass::Parameters *>(d_parameters);

    parameters->addValueToLabel = d_comboBoxBuildingBlockWidget_addValueToLabel->pointerToInternalComboBox()->currentData().toBool();

    parameters->annotationLabelDefiningAlternativeVariableIdentifier = d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningAlternativeItemIdentifier = d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeItemIdentifier->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningSubsets = d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->currentText();

    parameters->baseDataset = d_baseDataset;

    parameters->dpiOfPlot = d_plotExportParametersBuildingBlockWidget->dpiOfPlot();

    parameters->exportDirectory = d_plotExportParametersBuildingBlockWidget->exportDirectory();

    parameters->exportTypeOfPlot = d_plotExportParametersBuildingBlockWidget->typeOfPlot();

    parameters->heightOfPlot = d_plotExportParametersBuildingBlockWidget->heightOfPlot();

    parameters->pathToTemplatePlot = d_selectFileBuildingBlockWidget_templatePlot->pointerToInternalLineEdit()->text();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->widthOfPlot = d_plotExportParametersBuildingBlockWidget->widthOfPlot();

}

void CreatePiePlotsDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeItemIdentifier->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeItemIdentifier->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(orientation).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

}
