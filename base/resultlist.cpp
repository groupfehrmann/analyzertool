#include "resultlist.h"

ResultListData::ResultListData()
{

}

ResultListData::ResultListData(const ResultListData &other) :
    QSharedData(other), d_uuidToResult(other.d_uuidToResult), d_uuidOfResults(other.d_uuidOfResults)
{

}

ResultListData::~ResultListData()
{

}


ResultList::ResultList(QObject *parent) :
    QObject(parent), d_data(new ResultListData)
{

}

ResultList::ResultList(const ResultList &other) :
    QObject(other.parent()), d_data(other.d_data)
{

}

ResultList::~ResultList()
{

}

void ResultList::appendResult(Result *result)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    result->setParent(0);

    this->connect(result, SIGNAL(resultItemNameChanged(QUuid,QString)), this, SIGNAL(resultItemNameChanged(QUuid,QString)));

    this->connect(result, SIGNAL(nameChanged(QUuid,QString)), this, SIGNAL(resultNameChanged(QUuid,QString)));

    this->connect(result, SIGNAL(resultItemAdded(QSharedPointer<BaseResultItem>)), this, SIGNAL(resultItemAdded(QSharedPointer<BaseResultItem>)));

    this->connect(result, SIGNAL(resultItemRemoved(QUuid)), this, SIGNAL(resultItemRemoved(QUuid)));

    QSharedPointer<Result> sharedPointerResult(result);

    d_data->d_uuidToResult.insert(result->universallyUniqueIdentifier(), sharedPointerResult);

    d_data->d_uuidOfResults.append(result->universallyUniqueIdentifier());

    emit this->resultAdded(sharedPointerResult);

    for (int i = 0; i < result->numberOfResultItems(); ++i)
        emit this->resultItemAdded(result->baseResultItemAt(i));

}

int ResultList::numberOfResults() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToResult.count();

}

const QSharedPointer<Result> ResultList::resultAt(int index) const
{
    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToResult.value(d_data->d_uuidOfResults.at(index), QSharedPointer<Result>());

}

const QSharedPointer<Result> ResultList::resultAt(const QUuid &uuid) const
{
    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToResult.value(uuid, QSharedPointer<Result>());

}

void ResultList::removeAllResults()
{

    this->removeResults(d_data->d_uuidOfResults);

}

void ResultList::removeResult(const QUuid &uuid)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidToResult.value(uuid)->removeAllResultItems();

    d_data->d_uuidToResult.remove(uuid);

    d_data->d_uuidOfResults.removeOne(uuid);

    emit this->resultRemoved(uuid);

}

void ResultList::removeResult(int index)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    const QUuid &uuid(d_data->d_uuidOfResults.at(index));

    d_data->d_uuidToResult.value(uuid)->removeAllResultItems();

    d_data->d_uuidToResult.remove(uuid);

    d_data->d_uuidOfResults.removeAt(index);

    emit this->resultRemoved(uuid);

}

void ResultList::removeResults(const QList<QUuid> &uuids)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    for (const QUuid &uuid: uuids) {

        d_data->d_uuidToResult.value(uuid)->removeAllResultItems();

        d_data->d_uuidToResult.remove(uuid);

        d_data->d_uuidOfResults.removeOne(uuid);

        emit this->resultRemoved(uuid);

    }

}

void ResultList::removeResults(const QList<int> &indexes)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QList<int> sortedIndexes = indexes;

    std::sort(sortedIndexes.begin(), sortedIndexes.end(), std::greater<int>());

    for(const int &sortedIndex: sortedIndexes) {

        const QUuid &uuid(d_data->d_uuidOfResults.at(sortedIndex));

        d_data->d_uuidToResult.value(uuid)->removeAllResultItems();

        d_data->d_uuidToResult.remove(uuid);

        d_data->d_uuidOfResults.removeAt(sortedIndex);

        emit this->resultRemoved(uuid);

    }

}

ResultList &ResultList::operator=(const ResultList &other)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    if (this != &other)
        d_data.operator=(other.d_data);

    return *this;

}
