#include "createbarplotsdialog.h"

CreateBarPlotsDialog::CreateBarPlotsDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly, QAbstractSeries::SeriesType seriesType) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new CreateBarPlotsWorkerClass::Parameters;

    static_cast<CreateBarPlotsWorkerClass::Parameters *>(d_parameters)->seriesType = seriesType;

    this->setupWidgets();

}

CreateBarPlotsDialog::~CreateBarPlotsDialog()
{

   delete static_cast<CreateBarPlotsWorkerClass::Parameters *>(d_parameters);

}

void CreateBarPlotsDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_barSetLabels = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_barSetLabels", "Select bar set labels");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_barSetLabels, 1, 0);

    d_itemsSelectorBuildingBlockWidget_barCategoryLabels = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_barCategoryLabels", "Select bar category labels");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_barCategoryLabels, 2, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarSetLabels = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarSetLabels", "Select annotation label defining alternative bar set labels (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarSetLabels, 3, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarCategoryLabels = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarCategoryLabels", "Select annotation label defining alternative bar category labels (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarCategoryLabels, 4, 0);

    d_comboBoxBuildingBlockWidget_plotMode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_plotMode", "Select plot mode", {"All bar sets in single plot", "Each bar set in seperate plot"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_plotMode, 5, 0);

    d_plotExportParametersBuildingBlockWidget = new PlotExportParametersBuildingBlockWidget(this, "plotExportParametersBuildingBlockWidget");

    d_gridLayout->addWidget(d_plotExportParametersBuildingBlockWidget, 6, 0);

    d_selectFileBuildingBlockWidget_templatePlot = new SelectFileBuildingBlockWidget(this, "selectFileBuildingBlockWidget_templatePlot", "Select template plot (optional)", QFileDialog::AcceptOpen, true);

    d_gridLayout->addWidget(d_selectFileBuildingBlockWidget_templatePlot, 7, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void CreateBarPlotsDialog::initializeParameters()
{

    CreateBarPlotsWorkerClass::Parameters *parameters = static_cast<CreateBarPlotsWorkerClass::Parameters *>(d_parameters);

    parameters->annotationLabelDefiningAlternativeBarSetLabels = d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarSetLabels->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningAlternativeBarCategoryLabels = d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarCategoryLabels->pointerToInternalComboBox()->currentText();

    parameters->barCategoryLabels = d_itemsSelectorBuildingBlockWidget_barCategoryLabels->selectedItemsIdentifierAndNumber();

    parameters->baseDataset = d_baseDataset;

    parameters->barSetLabels = d_itemsSelectorBuildingBlockWidget_barSetLabels->selectedItemsIdentifierAndNumber();

    parameters->dpiOfPlot = d_plotExportParametersBuildingBlockWidget->dpiOfPlot();

    parameters->exportDirectory = d_plotExportParametersBuildingBlockWidget->exportDirectory();

    parameters->exportTypeOfPlot = d_plotExportParametersBuildingBlockWidget->typeOfPlot();

    parameters->heightOfPlot = d_plotExportParametersBuildingBlockWidget->heightOfPlot();

    parameters->pathToTemplatePlot = d_selectFileBuildingBlockWidget_templatePlot->pointerToInternalLineEdit()->text();

    parameters->plotMode = d_comboBoxBuildingBlockWidget_plotMode->pointerToInternalComboBox()->currentText();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->widthOfPlot = d_plotExportParametersBuildingBlockWidget->widthOfPlot();

}

void CreateBarPlotsDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > barSetsLabelAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_barSetLabels->setItemsIdentifierAndNumber(barSetsLabelAndIndex.first, barSetsLabelAndIndex.second);

    QPair<QStringList, QList<int> > barCategoriesLabelAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_barCategoryLabels->setItemsIdentifierAndNumber(barCategoriesLabelAndIndex.first, barCategoriesLabelAndIndex.second);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarSetLabels->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(orientation).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarCategoryLabels->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

}
