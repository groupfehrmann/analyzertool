#include "plotexportparametersbuildingblockwidget.h"
#include "ui_plotexportparametersbuildingblockwidget.h"

PlotExportParametersBuildingBlockWidget::PlotExportParametersBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label) :
    QWidget(parent), ui(new Ui::PlotExportParametersBuildingBlockWidget)
{

    ui->setupUi(this);

    ui->comboBox_type->addItems({"Project file (*.pdf)", "Portable Network Graphics (*.png)", "Windows Bitmap (*.bmp)", "Joint Photographic Experts Group (*.jpg)", "Portable Bitmap (*.pbm)", "Portable Graymap (*.pgm)", "Portable Pixmap (*.ppm)", "X11 Bitmap (*.xbm)", "X11 Pixmap (*.xpm)"});

    if (!label.isEmpty())
        ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/plotExportParametersBuildingBlockWidget");

    if (settings.contains("comboBox_type/currentIndex"))
        ui->comboBox_type->setCurrentIndex(settings.value("comboBox_type/currentIndex").toInt());

    if (settings.contains("spinBox_width/value"))
        ui->spinBox_width->setValue(settings.value("spinBox_width/value").toInt());

    if (settings.contains("spinBox_height/value"))
        ui->spinBox_height->setValue(settings.value("spinBox_height/value").toInt());

    if (settings.contains("spinBox_DPI/value"))
        ui->spinBox_DPI->setValue(settings.value("spinBox_DPI/value").toInt());

    if (settings.contains("lineEdit_path/text"))
        ui->lineEdit_path->setText(settings.value("lineEdit_path/text").toString());

    settings.endGroup();

    this->connect(ui->pushButton_browse, SIGNAL(clicked()), this, SLOT(pushButton_browseClicked()));


}

PlotExportParametersBuildingBlockWidget::~PlotExportParametersBuildingBlockWidget()
{

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/plotExportParametersBuildingBlockWidget");

    settings.setValue("comboBox_type/currentIndex", ui->comboBox_type->currentIndex());

    settings.setValue("spinBox_width/value", ui->spinBox_width->value());

    settings.setValue("spinBox_height/value", ui->spinBox_height->value());

    settings.setValue("spinBox_DPI/value", ui->spinBox_DPI->value());

    settings.setValue("lineEdit_path/text", ui->lineEdit_path->text());

    settings.endGroup();

    delete ui;

}

int PlotExportParametersBuildingBlockWidget::dpiOfPlot() const
{

    return ui->spinBox_DPI->value();

}

int PlotExportParametersBuildingBlockWidget::heightOfPlot() const
{

    return ui->spinBox_height->value();

}

QString PlotExportParametersBuildingBlockWidget::exportDirectory() const
{

    return ui->lineEdit_path->text();

}

void PlotExportParametersBuildingBlockWidget::pushButton_browseClicked()
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - Select output directory");

    fileDialog.setFileMode(QFileDialog::Directory);

    fileDialog.setOption(QFileDialog::ShowDirsOnly, true);

    QSettings settings;

    settings.beginGroup("plotExportParametersBuildingBlockWidget/fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    settings.endGroup();

    if (fileDialog.exec()) {

        QSettings settings;

        settings.beginGroup("SelectFileAndPlotParametersDialog/fileDialog");

        settings.setValue("state", fileDialog.saveState());

        settings.setValue("geometry", fileDialog.saveGeometry());

        settings.setValue("lastDirectory", fileDialog.directory().path());

        settings.endGroup();

        ui->lineEdit_path->setText(fileDialog.directory().absolutePath());

    }

}

QString PlotExportParametersBuildingBlockWidget::typeOfPlot() const
{

    return ui->comboBox_type->currentText();

}

int PlotExportParametersBuildingBlockWidget::widthOfPlot() const
{

    return ui->spinBox_width->value();

}
