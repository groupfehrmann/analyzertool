#include "mergedatasetsworkerclass.h"

MergeDatasetsWorkerClass::MergeDatasetsWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<MergeDatasetsWorkerClass::Parameters *>(parameters))
{

    if (d_parameters.mergedDatasetDataType == "short")
        d_baseDataset = new Dataset<short>();
    else if (d_parameters.mergedDatasetDataType == "unsigned short")
        d_baseDataset = new Dataset<unsigned short>();
    else if (d_parameters.mergedDatasetDataType == "int")
        d_baseDataset = new Dataset<int>();
    else if (d_parameters.mergedDatasetDataType == "unsigned int")
        d_baseDataset = new Dataset<unsigned int>();
    else if (d_parameters.mergedDatasetDataType == "long long")
        d_baseDataset = new Dataset<long long>();
    else if (d_parameters.mergedDatasetDataType == "unsigned long long")
        d_baseDataset = new Dataset<unsigned long long>();
    else if (d_parameters.mergedDatasetDataType == "float")
        d_baseDataset = new Dataset<float>();
    else if (d_parameters.mergedDatasetDataType == "double")
        d_baseDataset = new Dataset<double>();
    else if (d_parameters.mergedDatasetDataType == "unsigned char")
        d_baseDataset = new Dataset<unsigned char>();
    else if (d_parameters.mergedDatasetDataType == "string")
        d_baseDataset = new Dataset<QString>();
    else if (d_parameters.mergedDatasetDataType == "bool")
        d_baseDataset = new Dataset<bool>();

}

void MergeDatasetsWorkerClass::doWork()
{

    emit processStarted("Merge datasets");

    if (d_parameters.datasetsToMerge.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no datasets selected");

        QThread::currentThread()->quit();

        return;

    }

    QStringList baseDatasetDescriptions;

    for (QSharedPointer<BaseDataset> baseDataset : d_parameters.datasetsToMerge)
        baseDatasetDescriptions << baseDataset->parentProject()->name() + " - " + baseDataset->name();

    emit appendLogItem(LogListModel::PARAMETER, "datasets to merge: " + ConvertFunctions::toString(baseDatasetDescriptions));

    emit appendLogItem(LogListModel::PARAMETER, "selected only status of datasets to merge: " + ConvertFunctions::toString(d_parameters.selectedOnlyStatusOfDatasetsToMerge));

    emit appendLogItem(LogListModel::PARAMETER, "merge orientation: " + BaseMatrix::orientationToString(d_parameters.orientationToMerge));

    emit appendLogItem(LogListModel::PARAMETER, "merged data type: " + d_parameters.mergedDatasetDataType);

    emit appendLogItem(LogListModel::PARAMETER, "merged row annotations: " + (d_parameters.mergeRowAnnotations ? QString("true") : QString("false")));

    emit appendLogItem(LogListModel::PARAMETER, "merged column annotations: " + (d_parameters.mergeColumnAnnotations ? QString("true") : QString("false")));

    emit appendLogItem(LogListModel::PARAMETER, "create unique identifiers when there is overlap between datasets: " + (d_parameters.createUniqueIdentifiers ? QString("true") : QString("false")));

    emit appendLogItem(LogListModel::PARAMETER, "create unique annotation labels when there is overlap between datasets: " + (d_parameters.createUniqueAnnotationLabels ? QString("true") : QString("false")));

    emit startProgress(0, "Matching identifiers between datasets", 0, 0);

    bool ok;

    QString errorMessage;

    ProjectListAuxillaryFunctions::MatchedIdentifiersAndIndexes matchedIdentifiersAndIndexesForMergeOrientation = ProjectListAuxillaryFunctions::matchIdentifiersAndIndexesBetweenDatasets(d_parameters.datasetsToMerge, QStringList(), d_parameters.orientationToMerge, d_parameters.selectedOnlyStatusOfDatasetsToMerge, QStringList(), ok, errorMessage);

    if (!ok) {

        emit appendLogItem(LogListModel::ERROR, errorMessage);

        QThread::currentThread()->quit();

        return;

    }

    d_baseDataset->header(d_parameters.orientationToMerge).append(matchedIdentifiersAndIndexesForMergeOrientation.matchedIdentifiers);

    emit stopProgress(0);

    emit appendLogItem(LogListModel::MESSAGE, "number of matched identifiers between datasets: " + QString::number(matchedIdentifiersAndIndexesForMergeOrientation.matchedIdentifiers.size()));

    emit startProgress(0, "Processing datasets", 0, d_parameters.datasetsToMerge.size());

    QSet<QString> uniqueIdentifiers;

    for (int i = 0; i < d_parameters.datasetsToMerge.size(); ++i) {

        QHash<QString, QString> originalIdentifierToNewIdentifier;

        QSharedPointer<BaseDataset> datasetToMerge = d_parameters.datasetsToMerge.at(i);

        const Header &headerOfDatasetToMerge = datasetToMerge->header(BaseMatrix::switchOrientation(d_parameters.orientationToMerge));

        QList<int> indexesOfDatasetToMerge = headerOfDatasetToMerge.indexes(d_parameters.selectedOnlyStatusOfDatasetsToMerge.at(i));

        const QList<int> &matchedIndexesOfDatasetToMergeForMergeOrientation(matchedIdentifiersAndIndexesForMergeOrientation.matchedIndexes.at(i));

        emit startProgress(1, "Merging data of dataset \"" + datasetToMerge->parentProject()->name() + " - " + datasetToMerge->name() + "\"", 0, indexesOfDatasetToMerge.size());

        for (int j = 0; j < indexesOfDatasetToMerge.size(); ++j) {

            QString originalIdentifier = headerOfDatasetToMerge.identifier(indexesOfDatasetToMerge.at(j));

            int count = 0;

            QString newIdentifier = originalIdentifier;

            if (d_parameters.createUniqueIdentifiers) {

                while (uniqueIdentifiers.contains(newIdentifier))
                    newIdentifier = originalIdentifier + "_" + QString::number(++count);

            }

            originalIdentifierToNewIdentifier.insert(originalIdentifier, newIdentifier);

            if (d_parameters.mergedDatasetDataType == "short")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfShort(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "unsigned short")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfUnsignedShort(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "int")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfInt(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "unsigned int")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfUnsignedInt(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "long long")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfLongLong(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "unsigned long long")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfUnsignedLongLong(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "float")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfFloat(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "double")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfDouble(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "unsigned char")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfUnsignedChar(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "string")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfQString(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);
            else if (d_parameters.mergedDatasetDataType == "bool")
                ok = d_baseDataset->appendVector(newIdentifier, datasetToMerge->baseMatrix()->vectorOfBool(indexesOfDatasetToMerge.at(j), matchedIndexesOfDatasetToMergeForMergeOrientation, BaseMatrix::switchOrientation(d_parameters.orientationToMerge)), BaseMatrix::switchOrientation(d_parameters.orientationToMerge), &errorMessage);

            if (!ok) {

                emit appendLogItem(LogListModel::ERROR, "conversion error for identifier \"" + originalIdentifier + "\" with index " + QString::number(indexesOfDatasetToMerge.at(j)) + " in dataset \"" + datasetToMerge->parentProject()->name() + " - " + datasetToMerge->name() + "\"");

                emit appendLogItem(LogListModel::ERROR, errorMessage);

                QThread::currentThread()->quit();

                return;

            }

            if (!d_baseDataset->annotations(BaseMatrix::switchOrientation(d_parameters.orientationToMerge)).appendValue(newIdentifier, "Original dataset", datasetToMerge->name()))
                emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + newIdentifier + "\", label \"Original dataset\", value to set \"" + datasetToMerge->name() + "\", current value \"" + d_baseDataset->annotations(BaseMatrix::switchOrientation(d_parameters.orientationToMerge)).value(newIdentifier, "Original dataset").toString() + "\"");

            emit updateProgress(1, j + 1);

       }

        uniqueIdentifiers.unite(headerOfDatasetToMerge.uniqueIdentifiers(d_parameters.selectedOnlyStatusOfDatasetsToMerge.at(i)));

        emit stopProgress(1);

        if ((d_parameters.mergeRowAnnotations && (d_parameters.orientationToMerge == BaseMatrix::ROW)) || (d_parameters.mergeColumnAnnotations && (d_parameters.orientationToMerge == BaseMatrix::COLUMN))) {

            emit startProgress(1, "Merging " + BaseMatrix::orientationToString(d_parameters.orientationToMerge) + " annotations of dataset \"" + datasetToMerge->parentProject()->name() + " - " + datasetToMerge->name() + "\"", 0, 0);

            const Annotations &annotationsOfDatasetToMerge(datasetToMerge->annotations(d_parameters.orientationToMerge));

            Annotations &annotations(d_baseDataset->annotations(d_parameters.orientationToMerge));

            for (const QString &annotationLabel : annotationsOfDatasetToMerge.labels()) {

                if (annotations.containsLabel(annotationLabel))
                    emit appendLogItem(LogListModel::WARNING, tr("annotation label \"") + annotationLabel + "\" from dataset \"" + datasetToMerge->parentProject()->name() + " - " + datasetToMerge->name() + "\" is allready present in merged " + BaseMatrix::orientationToString(d_parameters.orientationToMerge) + " annotations");

                int count = 0;

                QString newAnnotationLabel = annotationLabel;

                if (d_parameters.createUniqueAnnotationLabels) {

                    while (annotations.containsLabel(newAnnotationLabel))
                        newAnnotationLabel = annotationLabel + "_" + QString::number(++count);

                }

                for (const QString &identifier : matchedIdentifiersAndIndexesForMergeOrientation.matchedIdentifiers) {

                    if (annotationsOfDatasetToMerge.containsIdentifier(identifier, annotationLabel)) {

                        if (!annotations.appendValue(identifier, newAnnotationLabel, annotationsOfDatasetToMerge.value(identifier, annotationLabel)))
                            emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + identifier + "\", label \"" + newAnnotationLabel + "\", value to set \"" + annotationsOfDatasetToMerge.value(identifier, annotationLabel).toString() + "\", current value \"" + annotations.value(identifier, newAnnotationLabel).toString() + "\"");

                    }

                }

            }

            emit stopProgress(1);

        }

        if ((d_parameters.mergeRowAnnotations && (BaseMatrix::switchOrientation(d_parameters.orientationToMerge) == BaseMatrix::ROW)) || (d_parameters.mergeColumnAnnotations && (BaseMatrix::switchOrientation(d_parameters.orientationToMerge) == BaseMatrix::COLUMN))) {

            emit startProgress(1, "Merging " + BaseMatrix::orientationToString(BaseMatrix::switchOrientation(d_parameters.orientationToMerge)) + " annotations of dataset \"" + datasetToMerge->parentProject()->name() + " - " + datasetToMerge->name() + "\"", 0, 0);

            const Annotations &annotationsOfDatasetToMerge(datasetToMerge->annotations(BaseMatrix::switchOrientation(d_parameters.orientationToMerge)));

            Annotations &annotations(d_baseDataset->annotations(BaseMatrix::switchOrientation(d_parameters.orientationToMerge)));

            for (const QString &annotationLabel : annotationsOfDatasetToMerge.labels()) {

                if (annotations.containsLabel(annotationLabel))
                    emit appendLogItem(LogListModel::WARNING, tr("annotation label \"") + annotationLabel + "\" from dataset \"" + datasetToMerge->parentProject()->name() + " - " + datasetToMerge->name() + "\" is allready present in merged " + BaseMatrix::orientationToString(BaseMatrix::switchOrientation(d_parameters.orientationToMerge)) + " annotations");

                int count = 0;

                QString newAnnotationLabel = annotationLabel;

                if (d_parameters.createUniqueAnnotationLabels) {

                    while (annotations.containsLabel(newAnnotationLabel))
                        newAnnotationLabel = annotationLabel + "_" + QString::number(++count);

                }

                for (const QString &originalIdentifier : originalIdentifierToNewIdentifier.keys()) {

                    if (annotationsOfDatasetToMerge.containsIdentifier(originalIdentifier, annotationLabel)) {

                        if (!annotations.appendValue(originalIdentifierToNewIdentifier.value(originalIdentifier), newAnnotationLabel, annotationsOfDatasetToMerge.value(originalIdentifier, annotationLabel)))
                            emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + originalIdentifierToNewIdentifier.value(originalIdentifier) + "\", label \"" + newAnnotationLabel + "\", value to set \"" + annotationsOfDatasetToMerge.value(originalIdentifier, annotationLabel).toString() + "\", current value \"" + annotations.value(originalIdentifierToNewIdentifier.value(originalIdentifier), newAnnotationLabel).toString() + "\"");

                    }

                    if (d_parameters.createUniqueIdentifiers)
                        annotations.appendValue(originalIdentifierToNewIdentifier.value(originalIdentifier), "_#_orignatingFromDataset", datasetToMerge->parentProject()->name() + " - " + datasetToMerge->name());

                }

            }

            emit stopProgress(1);

        }

        emit updateProgress(0, i + 1);

    }

    emit appendLogItem(LogListModel::MESSAGE, "number of rows: " + QString::number(d_baseDataset->header(BaseMatrix::ROW).count()));

    emit appendLogItem(LogListModel::MESSAGE, "number of columns: " + QString::number(d_baseDataset->header(BaseMatrix::COLUMN).count()));

    emit datasetAvailable(d_baseDataset);

    QThread::currentThread()->quit();

}
