#ifndef SELECTDELIMITERBUILDINGBLOCKWIDGET_H
#define SELECTDELIMITERBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>

#include "dialogs/basedialog.h"

namespace Ui {

class SelectDelimiterBuildingBlockWidget;

}

class SelectDelimiterBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    enum DelimiterType {

        CUSTUM = 0,

        TAB = 1,

        SPACE = 2,

        LINEFEED = 3,

        CARRIAGERETURN = 4,

        WHITESPACE = 5

    };

    static QString delimiterTypeToString(SelectDelimiterBuildingBlockWidget::DelimiterType delimiterType);

    explicit SelectDelimiterBuildingBlockWidget(BaseDialog *parent = 0, const QString &widgetId = QString());

    ~SelectDelimiterBuildingBlockWidget();

    QString delimiter();

    QLineEdit *pointerToInternalLineEdit();

    QComboBox *pointerToInternalComboBox();

    QCheckBox *pointerToInternalCheckBox();

private:

    Ui::SelectDelimiterBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

signals:

    void delimiterChanged();

private slots:

    void currentIndexOfComboboxChanged(int index);

};

#endif // SELECTDELIMITERBUILDINGBLOCKWIDGET_H
