#include "calculatedistancematrixdialog.h"

CalculateDistanceMatrixDialog::CalculateDistanceMatrixDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, const ProjectListModel *projectListModel) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_projectListModel(projectListModel)
{

    d_parameters = new CalculateDistanceMatrixWorkerClass::Parameters;

    for (QSharedPointer<Project> project : d_projectListModel->projectList()->projects()) {

        for (QSharedPointer<BaseDataset> baseDataset : project->baseDatasets()) {

            d_baseDatasets << baseDataset;

            d_baseDatasetDescriptions << baseDataset->parentProject()->name() + " - " + baseDataset->name();

        }

    }

    for (int i = 0; i < d_projectListModel->rowCount(); ++i) {

        for (int j = 0; j < d_projectListModel->rowCount(d_projectListModel->index(i, 0)); ++j)
            d_selectedOnlyStatusOfBaseDatasets << ((projectListModel->data(d_projectListModel->index(j, 0, d_projectListModel->index(i, 0)), Qt::CheckStateRole).toInt() == Qt::Checked) ? true : false);

    }

    this->setupWidgets();

}

CalculateDistanceMatrixDialog::~CalculateDistanceMatrixDialog()
{

    delete static_cast<CalculateDistanceMatrixWorkerClass::Parameters *>(d_parameters);

}

void CalculateDistanceMatrixDialog::initializeWidgets()
{

    d_comboBoxBuildingBlockWidget_dataSet1 = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_dataSet1", "Select dataset 1", d_baseDatasetDescriptions);

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_dataSet1, 0, 0);

    d_orientationBuildingBlockWidget_dataSet1 = new OrientationBuildingBlockWidget(this, "orientationBuildingBlockWidget_dataSet1", "Select orientation for dataset 1 containing variables");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget_dataSet1, 0, 1);

    d_itemsSelectorBuildingBlockWidget_variables1 = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables1", "Select variables for dataset 1");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables1, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items1 = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items1", "Select items for dataset 1");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items1, 1, 1);

    d_comboBoxBuildingBlockWidget_dataSet2 = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_dataSet2", "Select dataset 2", d_baseDatasetDescriptions);

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_dataSet2, 2, 0);

    d_orientationBuildingBlockWidget_dataSet2 = new OrientationBuildingBlockWidget(this, "orientationBuildingBlockWidget_dataSet2", "Select orientation for dataset 2 containing variables");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget_dataSet2, 2, 1);

    d_itemsSelectorBuildingBlockWidget_variables2 = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables2", "Select variables for dataset 2");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables2, 3, 0);

    d_itemsSelectorBuildingBlockWidget_items2 = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items2", "Select items for dataset 2");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items2, 3, 1);

    d_comboBoxBuildingBlockWidget_distanceFunction = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_distanceFunction", "Select distance function", {"Pearson", "Spearman", "Distance covariance", "Distance correlation", "Pearson (pairwise complete method)", "Spearman (pairwise complete method)", "Distance covariance (pairwise complete method)", "Distance correlation (pairwise complete method)"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_distanceFunction, 4, 0);

    d_comboBoxBuildingBlockWidget_calculatePValues = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_calculatePValues", "Calculate p-values", {"Enabled", "Disabled"});

    d_spinBoxBuildingBlockWidget_numberOfPermutations = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_numberOfPermutations", "Select number of permutations (requirement for some distance functions)", true, 10, 100, 100000000, 1000, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_numberOfPermutations, 5, 0);

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_calculatePValues, 6, 0);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory (optional)", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 7, 0);

    this->connect(d_orientationBuildingBlockWidget_dataSet1, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationDataset1Changed(BaseMatrix::Orientation)));

    this->connect(d_orientationBuildingBlockWidget_dataSet2, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationDataset2Changed(BaseMatrix::Orientation)));

    this->connect(d_comboBoxBuildingBlockWidget_calculatePValues->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(setEnableStatusOfSpinBoxNumberOfPermutations()));

    this->connect(d_comboBoxBuildingBlockWidget_distanceFunction->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(setEnableStatusOfSpinBoxNumberOfPermutations()));

    this->connect(d_comboBoxBuildingBlockWidget_dataSet1->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxBuildingBlockWidget_dataSet1_currentIndexChanged(int)));

    this->connect(d_comboBoxBuildingBlockWidget_dataSet2->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxBuildingBlockWidget_dataSet2_currentIndexChanged(int)));

    this->orientationDataset1Changed(d_orientationBuildingBlockWidget_dataSet1->selectedOrientation());

    this->orientationDataset2Changed(d_orientationBuildingBlockWidget_dataSet2->selectedOrientation());

    this->setEnableStatusOfSpinBoxNumberOfPermutations();

}

void CalculateDistanceMatrixDialog::initializeParameters()
{

    CalculateDistanceMatrixWorkerClass::Parameters *parameters = static_cast<CalculateDistanceMatrixWorkerClass::Parameters *>(d_parameters);

    parameters->dataset1 = d_baseDatasets.at(d_comboBoxBuildingBlockWidget_dataSet1->pointerToInternalComboBox()->currentIndex());

    parameters->orientationDataset1 = d_orientationBuildingBlockWidget_dataSet1->selectedOrientation();

    parameters->selectedItemIdentifiersAndIndexesDataset1 = d_itemsSelectorBuildingBlockWidget_items1->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexesDataset1 = d_itemsSelectorBuildingBlockWidget_variables1->selectedItemsIdentifierAndNumber();

    parameters->dataset2 = d_baseDatasets.at(d_comboBoxBuildingBlockWidget_dataSet2->pointerToInternalComboBox()->currentIndex());

    parameters->orientationDataset2 = d_orientationBuildingBlockWidget_dataSet2->selectedOrientation();

    parameters->selectedItemIdentifiersAndIndexesDataset2 = d_itemsSelectorBuildingBlockWidget_items2->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexesDataset2 = d_itemsSelectorBuildingBlockWidget_variables2->selectedItemsIdentifierAndNumber();

    parameters->distanceFunction = d_comboBoxBuildingBlockWidget_distanceFunction->pointerToInternalComboBox()->currentText();

    parameters->calculatePValues = (d_comboBoxBuildingBlockWidget_calculatePValues->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->numberOfPermutations = d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalSpinBox()->value();

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

}

void CalculateDistanceMatrixDialog::orientationDataset1Changed(BaseMatrix::Orientation orientation)
{

    int currentDataset1Index = d_comboBoxBuildingBlockWidget_dataSet1->pointerToInternalComboBox()->currentIndex();

    if (currentDataset1Index == -1)
        return;

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDatasets.at(currentDataset1Index)->header(orientation).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(currentDataset1Index));

    d_itemsSelectorBuildingBlockWidget_variables1->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDatasets.at(currentDataset1Index)->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(currentDataset1Index));

    d_itemsSelectorBuildingBlockWidget_items1->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

}

void CalculateDistanceMatrixDialog::orientationDataset2Changed(BaseMatrix::Orientation orientation)
{

    int currentDataset2Index = d_comboBoxBuildingBlockWidget_dataSet2->pointerToInternalComboBox()->currentIndex();

    if (currentDataset2Index == -1)
        return;

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDatasets.at(currentDataset2Index)->header(orientation).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(currentDataset2Index));

    d_itemsSelectorBuildingBlockWidget_variables2->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDatasets.at(currentDataset2Index)->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(currentDataset2Index));

    d_itemsSelectorBuildingBlockWidget_items2->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

}

void CalculateDistanceMatrixDialog::setEnableStatusOfSpinBoxNumberOfPermutations()
{

    bool pValuesRequested = (d_comboBoxBuildingBlockWidget_calculatePValues->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    if (!pValuesRequested) {

        d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalLabel()->setEnabled(false);

        d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalSpinBox()->setEnabled(false);

        return;

    }

    QString currentDistanceFunction = d_comboBoxBuildingBlockWidget_distanceFunction->pointerToInternalComboBox()->currentText();

    if ((currentDistanceFunction == "Distance covariance") || (currentDistanceFunction == "Distance correlation") || (currentDistanceFunction == "Distance covariance (pairwise complete method)") || (currentDistanceFunction == "Distance correlation (pairwise complete method)")) {

        d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalLabel()->setEnabled(true);

        d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalSpinBox()->setEnabled(true);

    } else {

        d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalLabel()->setEnabled(false);

        d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalSpinBox()->setEnabled(false);

    }

}

void CalculateDistanceMatrixDialog::comboBoxBuildingBlockWidget_dataSet1_currentIndexChanged(int index)
{

    if (index == -1)
        return;

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDatasets.at(index)->header(d_orientationBuildingBlockWidget_dataSet1->selectedOrientation()).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(index));

    d_itemsSelectorBuildingBlockWidget_variables1->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDatasets.at(index)->header(BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget_dataSet1->selectedOrientation())).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(index));

    d_itemsSelectorBuildingBlockWidget_items1->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

}

void CalculateDistanceMatrixDialog::comboBoxBuildingBlockWidget_dataSet2_currentIndexChanged(int index)
{

    if (index == -1)
        return;

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDatasets.at(index)->header(d_orientationBuildingBlockWidget_dataSet2->selectedOrientation()).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(index));

    d_itemsSelectorBuildingBlockWidget_variables2->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDatasets.at(index)->header(BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget_dataSet2->selectedOrientation())).identifiersAndIndexes(d_selectedOnlyStatusOfBaseDatasets.at(index));

    d_itemsSelectorBuildingBlockWidget_items2->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

}
