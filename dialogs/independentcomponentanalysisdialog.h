#ifndef INDEPENDENTCOMPONENTANALYSISDIALOG_H
#define INDEPENDENTCOMPONENTANALYSISDIALOG_H

#include <QString>
#include <QSharedPointer>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "widgets/doublespinboxbuildingblockwidget.h"
#include "widgets/spinboxbuildingblockwidget.h"
#include "workerclasses/independentcomponentanalysisworkerclass.h"

class IndependentComponentAnalysisDialog : public BaseDialog
{

    Q_OBJECT

public:

    IndependentComponentAnalysisDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~IndependentComponentAnalysisDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_whiteningMode;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_contrastFunction;

    DoubleSpinBoxBuildingBlockWidget *d_doubleSpinBoxBuildingBlockWidget_thresholdCumulativeExplainedVariance;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_thresholdMaximumNumberOfComponents;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_maximumIterations;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_finetuneMode;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_stabilizationMode;

    DoubleSpinBoxBuildingBlockWidget *d_doubleSpinBoxBuildingBlockWidget_mu;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_searchTimeConstant;

    DoubleSpinBoxBuildingBlockWidget *d_doubleSpinBoxBuildingBlockWidget_epsilon;

    DoubleSpinBoxBuildingBlockWidget *d_doubleSpinBoxBuildingBlockWidget_proportionOfSamplesToUseInAnIteration;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_numberOfRunsToPerform;

    DoubleSpinBoxBuildingBlockWidget *d_doubleSpinBoxBuildingBlockWidget_independentComponentClusteringCorrelationThreshold;

    DoubleSpinBoxBuildingBlockWidget *d_doubleSpinBoxBuildingBlockWidget_credibilityIndexThreshold;

    DoubleSpinBoxBuildingBlockWidget *d_doubleSpinBoxBuildingBlockWidget_distanceCorrelationThreshold;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_multiThreadMode;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void numberOfRunsChanged(const int &numberOfRuns);

    void selectionOfVariableIdentifiersAndIndexesChanged(const QStringList &selectedItemsIdentifier, const QStringList &notSelectedItemsIdentifier);

};

#endif // INDEPENDENTCOMPONENTANALYSISDIALOG_H
