#include "plotresultitemwidget.h"
#include "ui_plotresultitemwidget.h"

PlotResultItemWidget::PlotResultItemWidget(QWidget *parent, QSharedPointer<PlotResultItem> plotResultItem) :
    QWidget(parent), ui(new Ui::PlotResultItemWidget), d_plotResultItem(plotResultItem)
{

    ui->setupUi(this);

    QVBoxLayout *mainLayout = new QVBoxLayout(this);

    mainLayout->setContentsMargins(0, 0, 0, 0);

    d_chart = d_plotResultItem->chart();

    d_chartView = new QChartView(d_plotResultItem->chart().data(), this);

    mainLayout->addWidget(d_chartView);

    this->setLayout(mainLayout);

}

PlotResultItemWidget::~PlotResultItemWidget()
{

    d_chartView->setChart(new QChart()); // Hack, current chart needs to be released because QSharedPointer is keeping track, setParent(0) on current chart does not work. THIS IS PROBABLY A QT BUG

    delete ui;

}

QChartView *PlotResultItemWidget::chartView()
{

    return d_chartView;

}
