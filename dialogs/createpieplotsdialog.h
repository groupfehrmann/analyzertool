#ifndef CREATEPIEPLOTSDIALOG_H
#define CREATEPIEPLOTSDIALOG_H

#include <QString>
#include <QSharedPointer>
#include <QAbstractSeries>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/plotexportparametersbuildingblockwidget.h"
#include "workerclasses/createpieplotsworkerclass.h"
#include "widgets/selectfilebuildingblockwidget.h"

using namespace QtCharts;

class CreatePiePlotsDialog : public BaseDialog
{

    Q_OBJECT

public:

    CreatePiePlotsDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~CreatePiePlotsDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeItemIdentifier;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_addValueToLabel;

    PlotExportParametersBuildingBlockWidget *d_plotExportParametersBuildingBlockWidget;

    SelectFileBuildingBlockWidget *d_selectFileBuildingBlockWidget_templatePlot;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // CREATEPIEPLOTSDIALOG_H
