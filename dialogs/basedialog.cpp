#include "basedialog.h"
#include "ui_basedialog.h"

BaseDialog::BaseDialog(QWidget *parent, const QString &title, const QString &identifierForSettings) :
    QDialog(parent), ui(new Ui::BaseDialog), d_title(title), d_identifierForSettings(identifierForSettings)
{

    ui->setupUi(this);

    this->setWindowTitle(QCoreApplication::applicationName() + " - " + d_title);

    QSettings settings;

    settings.beginGroup(d_identifierForSettings);

    if (settings.contains("geometry"))
        this->restoreGeometry(settings.value("geometry").toByteArray());

    settings.endGroup();

}

BaseDialog::~BaseDialog()
{

    QSettings settings;

    settings.beginGroup(d_identifierForSettings);

    settings.setValue("geometry", this->saveGeometry());

    settings.endGroup();

    delete ui;

}

const QString &BaseDialog::identifierForSettings() const
{

    return d_identifierForSettings;

}

void *BaseDialog::parameters()
{

    this->initializeParameters();

    return d_parameters;

}

void BaseDialog::setupWidgets()
{

    d_gridLayout = new QGridLayout;

    this->initializeWidgets();

    d_gridLayout->addWidget(ui->buttonBox, d_gridLayout->rowCount(), 0, -1, -1);

    this->setLayout(d_gridLayout);

}
