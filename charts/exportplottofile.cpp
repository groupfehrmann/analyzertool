#include "exportplottofile.h"

ExportPlotToFile::ExportPlotToFile(QObject *parent, QChart *chart, const QString &path, int widthInMillimeters, int heightInMillimeters, int dpi, bool *error, QString *errorMessage) :
    QObject(parent), d_error(error), d_errorMessage(errorMessage)
{

    this->moveToThread(QApplication::instance()->thread());

    this->connect(this, SIGNAL(startExportingPlotToFile(QChart*,QString,int,int,int)), this, SLOT(_startExportingPlotToFile(QChart*,QString,int,int,int)));

    this->connect(this, SIGNAL(finishedExportingPlotToFile()), &d_eventLoop, SLOT(quit()));

    emit startExportingPlotToFile(chart, path, widthInMillimeters, heightInMillimeters, dpi);

    d_eventLoop.exec();

}

ExportPlotToFile::ExportPlotToFile(QObject *parent, QChartView *chartView, const QString &path, int widthInMillimeters, int heightInMillimeters, int dpi, bool *error, QString *errorMessage) :
    QObject(parent), d_error(error), d_errorMessage(errorMessage)
{

    this->moveToThread(QApplication::instance()->thread());

    this->connect(this, SIGNAL(startExportingPlotToFile(QChartView*,QString,int,int,int)), this, SLOT(_startExportingPlotToFile(QChartView*,QString,int,int,int)));

    this->connect(this, SIGNAL(finishedExportingPlotToFile()), &d_eventLoop, SLOT(quit()));

    emit startExportingPlotToFile(chartView, path, widthInMillimeters, heightInMillimeters, dpi);

    d_eventLoop.exec();

}

void ExportPlotToFile::_startExportingPlotToFile(QChart *chart, const QString &path, int widthInMillimeters, int heightInMillimeters, int dpi)
{

    QGraphicsScene graphicsScene;

    graphicsScene.addItem(chart);

    if (widthInMillimeters == -1)
        widthInMillimeters = (graphicsScene.sceneRect().size().width() / double(dpi)) * double(25.4);

    if (heightInMillimeters == -1)
        heightInMillimeters = (graphicsScene.sceneRect().size().height() / double(dpi)) * double(25.4);

    const double mmToInch = 1.0 / 25.4;

    const QSizeF sizeF = QSizeF(widthInMillimeters, heightInMillimeters)  * mmToInch * dpi;

    chart->setGeometry(QRectF(QPointF(0.0, 0.0), sizeF));

    graphicsScene.setSceneRect(QRectF(QPointF(0.0, 0.0), sizeF));

    QString format = QFileInfo(path).suffix().toLower();

    if (format == "pdf") {

        QPrinter printer;

        printer.setOutputFormat(QPrinter::PdfFormat);

        printer.setColorMode(QPrinter::Color);

        printer.setFullPage(false);

        printer.setPaperSize(QSizeF(widthInMillimeters, heightInMillimeters), QPrinter::Millimeter);

        printer.setDocName(chart->title());

        printer.setOutputFileName(path);

        printer.setResolution(dpi);

        QPainter painter(&printer);

        graphicsScene.render(&painter, QRectF(), QRectF(), Qt::IgnoreAspectRatio);

        painter.end();

        *d_errorMessage = QString();

        *d_error = false;

    } else if (QImageWriter::supportedImageFormats().indexOf(format.toLatin1()) >= 0) {

        const int dotsPerMeter = qRound(dpi * mmToInch * 1000.0);

        QImage image(sizeF.toSize(), QImage::Format_ARGB32);

        image.setDotsPerMeterX(dotsPerMeter);

        image.setDotsPerMeterY(dotsPerMeter);

        image.fill(QColor(Qt::white).rgb());

        QPainter painter(&image);

        graphicsScene.render(&painter, QRectF(), QRectF(), Qt::IgnoreAspectRatio);

        painter.end();

        if (!image.save(path, format.toLatin1(), 100)) {

            *d_errorMessage = "could not export plot to file";

            *d_error = false;
        }

    }

    graphicsScene.removeItem(chart);

    emit finishedExportingPlotToFile();

}

void ExportPlotToFile::_startExportingPlotToFile(QChartView *chartView, const QString &path, int widthInMillimeters, int heightInMillimeters, int dpi)
{

    if (widthInMillimeters == -1)
        widthInMillimeters = (chartView->sceneRect().size().width() / double(dpi)) * double(25.4);

    if (heightInMillimeters == -1)
        heightInMillimeters = (chartView->sceneRect().size().height() / double(dpi)) * double(25.4);

    const double mmToInch = 1.0 / 25.4;

    const QSizeF sizeF = QSizeF(widthInMillimeters, heightInMillimeters)  * mmToInch * dpi;

    chartView->chart()->setGeometry(QRectF(QPointF(0.0, 0.0), sizeF));

    chartView->setSceneRect(QRectF(QPointF(0.0, 0.0), sizeF));

    QString format = QFileInfo(path).suffix().toLower();

    if (format == "pdf") {

        QPrinter printer;

        printer.setOutputFormat(QPrinter::PdfFormat);

        printer.setColorMode(QPrinter::Color);

        printer.setFullPage(false);

        printer.setPaperSize(QSizeF(widthInMillimeters, heightInMillimeters), QPrinter::Millimeter);

        printer.setDocName(chartView->chart()->title());

        printer.setOutputFileName(path);

        printer.setResolution(dpi);

        QPainter painter(&printer);

        chartView->render(&painter, QRectF(), QRect(), Qt::IgnoreAspectRatio);

        painter.end();

        *d_errorMessage = QString();

        *d_error = false;

    } else if (QImageWriter::supportedImageFormats().indexOf(format.toLatin1()) >= 0) {

        const int dotsPerMeter = qRound(dpi * mmToInch * 1000.0);

        QImage image(sizeF.toSize(), QImage::Format_ARGB32);

        image.setDotsPerMeterX(dotsPerMeter);

        image.setDotsPerMeterY(dotsPerMeter);

        image.fill(QColor(Qt::white).rgb());

        QPainter painter(&image);

        chartView->render(&painter, QRectF(), QRect(), Qt::IgnoreAspectRatio);

        painter.end();

        if (!image.save(path, format.toLatin1(), 100)) {

            *d_errorMessage = "could not export plot to file";

            *d_error = false;

        }

    }

    emit finishedExportingPlotToFile();

}
