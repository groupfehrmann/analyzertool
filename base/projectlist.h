#ifndef PROJECTLIST_H
#define PROJECTLIST_H

#include <QObject>
#include <QApplication>
#include <QSharedData>
#include <QHash>
#include <QSharedPointer>
#include <QList>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>

#include "base/project.h"
#include "base/basedataset.h"

namespace ProjectListAuxillaryFunctions {

    struct MatchedIdentifiersAndIndexes {

        QList<QSharedPointer<BaseDataset > > baseDatasets;

        QStringList matchedIdentifiers;

        QVector<QList<int> > matchedIndexes;

    };

    MatchedIdentifiersAndIndexes matchIdentifiersAndIndexesBetweenDatasets(const QList<QSharedPointer<BaseDataset> > &baseDatasets, const QStringList &optionalValidIdentifiers, BaseMatrix::Orientation orientation, const QList<bool> &selectedOnlyStatusOfDatasets, const QStringList &optionalAnnotationLabelDefiningAlternativeIdentifiers, bool &ok, QString &errorMessage);

    MatchedIdentifiersAndIndexes matchIdentifiersAndIndexesBetweenDatasets(const QList<QSharedPointer<BaseDataset> > &baseDatasets, const QStringList &optionalValidIdentifiers, const QList<BaseMatrix::Orientation> &orientations, const QList<bool> &selectedOnlyStatusOfDatasets, const QStringList &optionalAnnotationLabelDefiningAlternativeIdentifiers, bool &ok, QString &errorMessage);

}

class ProjectListData : public QSharedData
{

public:

    ProjectListData();

    ProjectListData(const ProjectListData &other);

    ~ProjectListData();

    QHash<QUuid, QSharedPointer<Project> > d_uuidToProject;

    QList<QUuid> d_uuidOfProjects;

};

class ProjectList : public QObject
{

    Q_OBJECT

public:

    ProjectList(QObject *parent = nullptr);

    ProjectList(const ProjectList &other);

    ~ProjectList();

    int numberOfProjects() const;

    const QSharedPointer<Project> projectAt(int index) const;

    const QSharedPointer<Project> projectAt(const QUuid &uuid) const;

    QList<QSharedPointer<Project> > projects() const;

    ProjectList &operator=(const ProjectList &other);

private:

    QSharedDataPointer<ProjectListData> d_data;

    mutable QReadWriteLock d_readWriteLock;

public slots:

    void appendProject(Project *project);

    void insertProject(int index, Project *project);

    void removeAllProjects();

    void removeProject(const QUuid &uuid);

    void removeProject(int index);

    void removeProjects(const QList<QUuid> &uuids);

    void removeProjects(const QList<int> &indexes);

signals:

    void datasetAdded(const QSharedPointer<BaseDataset> &baseDataset);

    void datasetNameChanged(const QUuid &uuid, const QString &name);

    void datasetRemoved(const QUuid &uuid);

    void projectAdded(const QSharedPointer<Project> &project);

    void projectNameChanged(const QUuid &uuid, const QString &name);

    void projectRemoved(const QUuid &uuid);

};

Q_DECLARE_TYPEINFO(ProjectList, Q_MOVABLE_TYPE);

#endif // PROJECTLIST_H
