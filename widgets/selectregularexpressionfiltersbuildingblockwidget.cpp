#include "selectregularexpressionfiltersbuildingblockwidget.h"
#include "ui_selectregularexpressionfiltersbuildingblockwidget.h"

SelectRegularExpressionFiltersBuildingBlockWidget::SelectRegularExpressionFiltersBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, QSharedPointer<BaseDataset> baseDataset) :
    QWidget(parent), ui(new Ui::SelectRegularExpressionFiltersBuildingBlockWidget), d_baseDataset(baseDataset), d_rememberSettings(true)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/selectRegularExpressionFiltersBuildingBlockWidget");

    if (settings.contains("defaultIndexForComboBox_mode"))
        d_defaultIndexForComboBox_mode = settings.value("defaultIndexForComboBox_mode").toInt();
    else
        d_defaultIndexForComboBox_mode = 0;

    if (settings.contains("defaultIndexForComboBox_syntax"))
        d_defaultIndexForComboBox_syntax = settings.value("defaultIndexForComboBox_syntax").toInt();
    else
        d_defaultIndexForComboBox_syntax = 0;

    if (settings.contains("defaultStatusForCheckBox_caseSensitivity"))
        d_defaultStatusForCheckBox_caseSensitivity = settings.value("defaultStatusForCheckBox_caseSensitivity").toBool();
    else
        d_defaultStatusForCheckBox_caseSensitivity = false;

    settings.endGroup();

    this->connect(ui->toolButton_add, SIGNAL(clicked()), this, SLOT(toolButton_add_clicked()));

    this->connect(ui->toolButton_remove, SIGNAL(clicked()), this, SLOT(toolButton_remove_clicked()));

}

SelectRegularExpressionFiltersBuildingBlockWidget::~SelectRegularExpressionFiltersBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/selectRegularExpressionFiltersBuildingBlockWidget");

        settings.setValue("defaultIndexForComboBox_mode", d_defaultIndexForComboBox_mode);

        settings.setValue("defaultIndexForComboBox_syntax", d_defaultIndexForComboBox_syntax);

        settings.setValue("defaultStatusForCheckBox_caseSensitivity", d_defaultStatusForCheckBox_caseSensitivity);

        settings.endGroup();

    }

    delete ui;

}

void SelectRegularExpressionFiltersBuildingBlockWidget::toolButton_add_clicked()
{

    int newRowIndex = ui->tableWidget->rowCount();

    ui->tableWidget->setRowCount(ui->tableWidget->rowCount() + 1);

    QComboBox *comboBox_mode = new QComboBox(this);

    comboBox_mode->addItem("Optional", SelectWithRegularExpressionWorkerClass::OPTIONAL);

    comboBox_mode->addItem("Required", SelectWithRegularExpressionWorkerClass::REQUIRED);

    comboBox_mode->setCurrentIndex(d_defaultIndexForComboBox_mode);

    ui->tableWidget->setCellWidget(newRowIndex, 0, comboBox_mode);

    QLineEdit *lineEdit_pattern = new QLineEdit(this);

    ui->tableWidget->setCellWidget(newRowIndex, 1, lineEdit_pattern);

    QComboBox *comboBox_syntax = new QComboBox(this);

    comboBox_syntax->addItem("Regular expression", QRegExp::RegExp2);

    comboBox_syntax->addItem("Wildcard", QRegExp::Wildcard);

    comboBox_syntax->addItem("Fixed string", QRegExp::FixedString);

    comboBox_syntax->setCurrentIndex(d_defaultIndexForComboBox_syntax);

    ui->tableWidget->setCellWidget(newRowIndex, 2, comboBox_syntax);

    QCheckBox *checkBox_caseSensitivity = new QCheckBox("Case sensitive", this);

    ui->tableWidget->setCellWidget(newRowIndex, 3, checkBox_caseSensitivity);

    QComboBox *comboBox_applyOn = new QComboBox(this);

    ui->tableWidget->setCellWidget(newRowIndex, 4, comboBox_applyOn);

    ui->tableWidget->resizeColumnToContents(0);

    ui->tableWidget->resizeColumnToContents(1);

    ui->tableWidget->resizeColumnToContents(2);

    ui->tableWidget->resizeColumnToContents(3);

    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

    this->setOrientation(d_orientation);

}

void SelectRegularExpressionFiltersBuildingBlockWidget::toolButton_remove_clicked()
{

    QModelIndexList list = ui->tableWidget->selectionModel()->selectedRows();

    for (int i = list.size() - 1; i >= 0; --i)
        ui->tableWidget->removeRow(list.at(i).row());

}

void SelectRegularExpressionFiltersBuildingBlockWidget::setOrientation(BaseMatrix::Orientation orientation)
{

    d_orientation = orientation;

    for (int rowIndex = 0; rowIndex < ui->tableWidget->rowCount(); ++rowIndex) {

        QComboBox *comboBox = static_cast<QComboBox *>(ui->tableWidget->cellWidget(rowIndex, 4));

        comboBox->clear();

        comboBox->addItem((orientation == BaseMatrix::ROW ? "Row identifiers in dataset" : "Column identifiers in dataset"));

        comboBox->setItemData(0, QBrush(QColor(244, 116, 66)), Qt::ForegroundRole);

        for (const QString &annotationLabel : d_baseDataset->annotations(orientation).labels())
            comboBox->addItem((orientation == BaseMatrix::ROW ? "Values for row annotation label: " : "Values for column annotation label: ") + annotationLabel, annotationLabel);

    }

}

QList<SelectWithRegularExpressionWorkerClass::Filter> SelectRegularExpressionFiltersBuildingBlockWidget::getFilters()
{

    QList<SelectWithRegularExpressionWorkerClass::Filter> filters;

    for (int rowIndex = 0; rowIndex < ui->tableWidget->rowCount(); ++rowIndex) {

        SelectWithRegularExpressionWorkerClass::Filter filter;

        filter.mode = static_cast<SelectWithRegularExpressionWorkerClass::Mode>(static_cast<QComboBox *>(ui->tableWidget->cellWidget(rowIndex, 0))->currentData().toInt());

        filter.regularExpression = QRegExp(static_cast<QLineEdit *>(ui->tableWidget->cellWidget(rowIndex, 1))->text(), static_cast<QCheckBox *>(ui->tableWidget->cellWidget(rowIndex, 3))->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive, static_cast<QRegExp::PatternSyntax>(static_cast<QComboBox *>(ui->tableWidget->cellWidget(rowIndex, 2))->currentData().toInt()));

        filter.annotationLabel = static_cast<QComboBox *>(ui->tableWidget->cellWidget(rowIndex, 4))->currentIndex() == 0 ? QString() : static_cast<QComboBox *>(ui->tableWidget->cellWidget(rowIndex, 4))->currentData().toString();

        filters << filter;

        d_defaultIndexForComboBox_mode = static_cast<QComboBox *>(ui->tableWidget->cellWidget(rowIndex, 0))->currentIndex();

        d_defaultIndexForComboBox_syntax = static_cast<QComboBox *>(ui->tableWidget->cellWidget(rowIndex, 2))->currentIndex();

    }

    return filters;

}
