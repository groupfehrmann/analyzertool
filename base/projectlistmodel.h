#ifndef PROJECTLISTMODEL_H
#define PROJECTLISTMODEL_H

#include <QAbstractItemModel>
#include <QSharedPointer>
#include <QList>

#include "base/projectlist.h"

class ProjectListModelItem
{

public:

    ProjectListModelItem(ProjectListModelItem *parentItem = nullptr, QObject *internalObject = nullptr);

    ~ProjectListModelItem();

    void appendChild(ProjectListModelItem *child);

    ProjectListModelItem *child(int row);

    int childCount() const;

    int columnCount() const;

    QVariant data(int column) const;

    void insertChild(int row, ProjectListModelItem *child);

    QObject *internalObject();

    const QObject *internalObject() const;

    bool isChecked() const;

    ProjectListModelItem *parentItem();

    void removeChild(int row);

    void removeChildren();

    int row() const;

    void setChecked(bool checked);

private:

    bool d_checked;

    QList<ProjectListModelItem *> d_childItems;

    QObject *d_internalObject;

    ProjectListModelItem *d_parentItem;

};

class ProjectListModel : public QAbstractItemModel
{

    Q_OBJECT

public:

    ProjectListModel(QObject *parent = nullptr, ProjectList *projectList = nullptr);

    ~ProjectListModel();

    int columnCount(QModelIndex const &parent = QModelIndex()) const;

    QVariant data(QModelIndex const &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(QModelIndex const &index) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;

    QModelIndex parent(const QModelIndex &index) const;

    const ProjectList * projectList() const;

    void removeAll();

    bool removeRows(int row, int count, const QModelIndex &parent);

    int rowCount(QModelIndex const &parent = QModelIndex()) const;

    bool setData(QModelIndex const &index, QVariant const &value, int role = Qt::EditRole);

private:

    ProjectList *d_projectList;

    ProjectListModelItem *d_rootItem;

public slots:

    bool insertProject(int row, Project *project, const QModelIndex &parent);

    bool insertBaseDataset(int row, BaseDataset *baseDataset, const QModelIndex &parent);

signals:

    void datasetAdded(const QSharedPointer<BaseDataset> &baseDataset);

    void datasetNameChanged(const QUuid &uuid, const QString &name);

    void datasetRemoved(const QUuid &uuid);

    void projectRemoved(const QUuid &uuid);

    void projectNameChanged(const QUuid &uuid, const QString &name);

    void projectAdded(const QSharedPointer<Project> &project);

    void showSelectedOnly(const QUuid &uuid, bool showSelectedOnly);

};

#endif // PROJECTLISTMODEL_H
