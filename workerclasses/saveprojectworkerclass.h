#ifndef SAVEPROJECTWORKERCLASS_H
#define SAVEPROJECTWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QFile>

#include "base/baseworkerclass.h"
#include "base/project.h"

class SaveProjectWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

        QSharedPointer<Project> projectToSave;

    };

    SaveProjectWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    SaveProjectWorkerClass::Parameters d_parameters;

};

#endif // SAVEPROJECTWORKERCLASS_H
