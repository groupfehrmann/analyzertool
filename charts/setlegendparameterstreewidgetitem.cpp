#include "setlegendparameterstreewidgetitem.h"

SetLegendParametersTreeWidgetItem::SetLegendParametersTreeWidgetItem(QTreeWidget *parent, const QString &label, QLegend *legend) :
    QObject(), QTreeWidgetItem(parent), d_legend(legend), d_initialized(false)
{

    if (legend) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetLegendParametersTreeWidgetItem::SetLegendParametersTreeWidgetItem(QTreeWidgetItem *parent, const QString &label, QLegend *legend) :
   QObject(), QTreeWidgetItem(parent), d_legend(legend), d_initialized(false)
{

    if (legend) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetLegendParametersTreeWidgetItem::~SetLegendParametersTreeWidgetItem()
{

}

void SetLegendParametersTreeWidgetItem::initialize(const QString &label)
{

    this->setText(0, label);

    d_selectAlignmentWidget = new QComboBox();

    d_selectAlignmentWidget->addItem("Top", Qt::AlignTop);

    d_selectAlignmentWidget->addItem("Bottom", Qt::AlignBottom);

    d_selectAlignmentWidget->addItem("Left", Qt::AlignLeft);

    d_selectAlignmentWidget->addItem("Right", Qt::AlignRight);

    switch (d_legend->alignment()) {

        case Qt::AlignTop: d_selectAlignmentWidget->setCurrentIndex(0); break;

        case Qt::AlignBottom: d_selectAlignmentWidget->setCurrentIndex(1); break;

        case Qt::AlignLeft: d_selectAlignmentWidget->setCurrentIndex(2); break;

        case Qt::AlignRight: d_selectAlignmentWidget->setCurrentIndex(3); break;

        default: break;

    }

    this->connect(d_selectAlignmentWidget, SIGNAL(currentIndexChanged(int)), this, SLOT(setAlignment(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Alignment"}), 1, d_selectAlignmentWidget);


    d_selectBackgroundVisibleWidget = new QCheckBox;

    d_selectBackgroundVisibleWidget->setChecked(d_legend->isBackgroundVisible());

    this->connect(d_selectBackgroundVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setBackgroundVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Background visible"}), 1, d_selectBackgroundVisibleWidget);


    d_selectBorderColorWidget = new SelectColorWidget(nullptr, d_legend->borderColor());

    this->connect(d_selectBorderColorWidget, SIGNAL(colorSelected(QColor)), this, SLOT(setBorderColor(QColor)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Border color"}), 1, d_selectBorderColorWidget);


    d_setBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(this, "Brush", d_legend->brush());

    this->connect(d_setBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setBrush(QBrush)));


    d_selectFontWidget = new SelectFontWidget(nullptr, d_legend->font());

    this->connect(d_selectFontWidget, SIGNAL(currentFontChanged(QFont)), this, SLOT(setFont(QFont)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Font"}), 1, d_selectFontWidget);


    d_setLabelBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(this, "Label brush", d_legend->labelBrush());

    this->connect(d_setLabelBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setLabelBrush(QBrush)));


    d_setPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(this, "Pen", d_legend->pen());

    this->connect(d_setPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setPen(QPen)));


    d_selectReverseMarkersWidget = new QCheckBox;

    d_selectReverseMarkersWidget->setChecked(d_legend->reverseMarkers());

    this->connect(d_selectReverseMarkersWidget, SIGNAL(toggled(bool)), this, SLOT(setReverseMarkers(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Reverse markers"}), 1, d_selectReverseMarkersWidget);


    d_selectShowToolTipsWidget = new QCheckBox;

    d_selectShowToolTipsWidget->setChecked(d_legend->showToolTips());

    this->connect(d_selectShowToolTipsWidget, SIGNAL(toggled(bool)), this, SLOT(setShowToolTips(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Show tool tips"}), 1, d_selectShowToolTipsWidget);


    d_selectVisibleWidget = new QCheckBox;

    d_selectVisibleWidget->setChecked(d_legend->isVisible());

    this->connect(d_selectVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Visible"}), 1, d_selectVisibleWidget);

}

void SetLegendParametersTreeWidgetItem::setAlignment(int index)
{

    if (index == 0)
        d_legend->setAlignment(Qt::AlignTop);
    else if (index == 1)
        d_legend->setAlignment(Qt::AlignBottom);
    else if (index == 2)
        d_legend->setAlignment(Qt::AlignLeft);
    else if (index == 3)
        d_legend->setAlignment(Qt::AlignRight);

}

void SetLegendParametersTreeWidgetItem::setBackgroundVisible(bool visible)
{

    d_legend->setBackgroundVisible(visible);

}

void SetLegendParametersTreeWidgetItem::setBorderColor(QColor color)
{

    d_legend->setBorderColor(color);

}

void SetLegendParametersTreeWidgetItem::setBrush(const QBrush &brush)
{

    d_legend->setBrush(brush);

}

void SetLegendParametersTreeWidgetItem::setFont(const QFont &font)
{

    d_legend->setFont(font);

}

void SetLegendParametersTreeWidgetItem::setLabelBrush(const QBrush &brush)
{

    d_legend->setLabelBrush(brush);

}

void SetLegendParametersTreeWidgetItem::setLegend(const QString &label, QLegend *legend)
{

    d_legend = legend;

    qDeleteAll(this->takeChildren());

    this->initialize(label);

    switch (d_legend->alignment()) {

        case Qt::AlignTop: d_selectAlignmentWidget->setCurrentIndex(0); break;

        case Qt::AlignBottom: d_selectAlignmentWidget->setCurrentIndex(1); break;

        case Qt::AlignLeft: d_selectAlignmentWidget->setCurrentIndex(2); break;

        case Qt::AlignRight: d_selectAlignmentWidget->setCurrentIndex(3); break;

        default: break;

    }

    d_selectBackgroundVisibleWidget->setChecked(d_legend->isBackgroundVisible());

    d_selectBorderColorWidget->setCurrentColor(d_legend->borderColor());

    d_setBrushParametersTreeWidgetItem->setCurrentBrush(d_legend->brush());

    d_selectFontWidget->setCurrentFont(d_legend->font());

    d_setLabelBrushParametersTreeWidgetItem->setCurrentBrush(d_legend->labelBrush());

    d_setPenParametersTreeWidgetItem->setCurrentPen(d_legend->pen());

    d_selectReverseMarkersWidget->setChecked(d_legend->reverseMarkers());

    d_selectShowToolTipsWidget->setChecked(d_legend->showToolTips());

    d_selectVisibleWidget->setChecked(d_legend->isVisible());

}

void SetLegendParametersTreeWidgetItem::setPen(const QPen &pen)
{

    d_legend->setPen(pen);

}

void SetLegendParametersTreeWidgetItem::setReverseMarkers(bool reverse)
{

    d_legend->setReverseMarkers(reverse);

}

void SetLegendParametersTreeWidgetItem::setShowToolTips(bool show)
{

    d_legend->setShowToolTips(show);

}

void SetLegendParametersTreeWidgetItem::setVisible(bool visible)
{

    d_legend->setVisible(visible);

}
