#include "spinboxbuildingblockwidget.h"
#include "ui_spinboxbuildingblockwidget.h"

SpinBoxBuildingBlockWidget::SpinBoxBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, bool rememberSettings, int displayIntegerBase, int minumum, int maximum, int value, int singleStep, const QString &prefix, const QString &suffix) :
    QWidget(parent), ui(new Ui::SpinBoxBuildingBlockWidget), d_rememberSettings(rememberSettings)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    ui->spinBox->setRange(minumum, maximum);

    ui->spinBox->setValue(value);

    ui->spinBox->setDisplayIntegerBase(displayIntegerBase);

    ui->spinBox->setPrefix(prefix);

    ui->spinBox->setSuffix(suffix);

    ui->spinBox->setSingleStep(singleStep);

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/spinBoxBuildingBlockWidget");

    if (settings.contains("value"))
        ui->spinBox->setValue(settings.value("value").toInt());

    settings.endGroup();

}

SpinBoxBuildingBlockWidget::~SpinBoxBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/spinBoxBuildingBlockWidget");

        settings.setValue("value", ui->spinBox->value());

        settings.endGroup();

    }

    delete ui;

}

QSpinBox *SpinBoxBuildingBlockWidget::pointerToInternalSpinBox()
{

    return ui->spinBox;

}

QLabel *SpinBoxBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}
