#include "loglistmodel.h"

LogSortFilterProxyModel::LogSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{

}

bool LogSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    LogListModel *logListModel = static_cast<LogListModel *>(this->sourceModel());

    return logListModel->show(sourceRow);

}

LogListModel::LogListModel(QObject *parent) :
    QAbstractListModel(parent)
{

    this->setStandardMessage();

}

void LogListModel::appendLogItem(LogListModel::Type type, const QString &text)
{

    this->beginInsertRows(QModelIndex(), d_logItems.size(), d_logItems.size());

    d_logItems << LogListModel::LogItem { QDateTime::currentDateTime().toString("d MMM yyyy - hh:mm:ss"), type, text };

    this->endInsertRows();

}

void LogListModel::clear()
{

    this->beginResetModel();

    d_logItems.clear();

    this->endResetModel();

    this->setStandardMessage();

}

QVariant LogListModel::data(QModelIndex const &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    LogListModel::LogItem const &logItem(d_logItems.at(index.row()));

    if (role == Qt::DisplayRole) {

        if (logItem.type == LogListModel::NODATE)
            return logItem.text;
        else
            return logItem.timeStamp + "\t" + logItem.text;

    } else if (role == Qt::BackgroundRole) {

        switch (logItem.type) {

        case LogListModel::MESSAGE :
            return QColor(Qt::white);

        case LogListModel::WARNING :
            return QColor(255, 153, 0);

        case LogListModel::ERROR :
            return QColor(204, 0, 0);

        case LogListModel::PROCESSSTARTORSTOP :
            return QColor(102, 179, 255);

        default :
            QVariant();

        }

    } else if (role == Qt::ForegroundRole) {

        if (logItem.type == LogListModel::ERROR)
            return QColor(Qt::white);
        else if (logItem.type == LogListModel::PROCESSSTARTORSTOP)
            return QColor(Qt::white);
        else if (logItem.type == LogListModel::PARAMETER)
            return QColor(Qt::lightGray);
        else
            return QVariant();

    }

    return QVariant();

}

int LogListModel::rowCount(const QModelIndex &parent) const
{

    if (parent.isValid())
        return 0;

    return d_logItems.count();

}

void LogListModel::setShowError(bool show)
{

    this->beginResetModel();

    d_showError = show;

    this->endResetModel();

}

void LogListModel::setShowWarning(bool show)
{

    this->beginResetModel();

    d_showWarning = show;

    this->endResetModel();

}

void LogListModel::setShowMessage(bool show)
{
    this->beginResetModel();

    d_showMessage = show;

    this->endResetModel();

}

void LogListModel::setStandardMessage()
{

    this->appendLogItem(LogListModel::NODATE, QCoreApplication::applicationName());

    this->appendLogItem(LogListModel::NODATE, "Last modified on " + QCoreApplication::applicationVersion());

    this->appendLogItem(LogListModel::NODATE, "Developed by Rudolf S.N. Fehrmann");

    this->appendLogItem(LogListModel::NODATE, "");

}

bool LogListModel::show(int index) const
{

    switch (d_logItems.at(index).type) {

        case MESSAGE : return d_showMessage;

        case WARNING : return d_showWarning;

        case ERROR : return d_showError;

        default : return true;

    }

}
