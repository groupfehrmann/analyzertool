#include "exporttojsonfiledialog.h"

ExportToJSONFileDialog::ExportToJSONFileDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new ExportToJSONFileWorkerClass::Parameters;

    this->setupWidgets();

}

ExportToJSONFileDialog::~ExportToJSONFileDialog()
{

   delete static_cast<ExportToJSONFileWorkerClass::Parameters *>(d_parameters);

}

void ExportToJSONFileDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 1, 1);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsForVariable = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_annotationLabelsForVariable", "Select annotation labels for variables (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_annotationLabelsForVariable, 2, 0);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsForItem = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_annotationLabelsForItem", "Select annotation labels for items (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_annotationLabelsForItem, 2, 1);

    d_itemsSelectorBuildingBlockWidget_annotationIdentifiersForItem = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_annotationIdentifiersForItem", "Select valid annotation identifiers for items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_annotationIdentifiersForItem, 3, 1);

    d_comboBoxBuildingBlockWidget_exportFormat = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_exportFormat", "Select export format", {"json for distroChart", "json for JEasyUI DataGrid"});

    QCompleter *completer1 = new QCompleter({"json for distroChart", "json for JEasyUI DataGrid"}, this);

    completer1->setCaseSensitivity(Qt::CaseInsensitive);

    completer1->setFilterMode(Qt::MatchContains);

    d_comboBoxBuildingBlockWidget_exportFormat->pointerToInternalComboBox()->setEditable(true);

    d_comboBoxBuildingBlockWidget_exportFormat->pointerToInternalComboBox()->setCompleter(completer1);

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_exportFormat, 3, 0);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory (optional)", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 4, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->connect(d_itemsSelectorBuildingBlockWidget_annotationLabelsForItem, SIGNAL(selectionChanged(const QStringList &, const QStringList &)), this, SLOT(annotationLabelsForItem_selectionChanged(const QStringList &, const QStringList &)));

    this->connect(d_itemsSelectorBuildingBlockWidget_items, SIGNAL(selectionChanged(const QStringList &, const QStringList &)), this, SLOT(items_selectionChanged(const QStringList &, const QStringList &)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void ExportToJSONFileDialog::initializeParameters()
{

    ExportToJSONFileWorkerClass::Parameters *parameters = static_cast<ExportToJSONFileWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->annotationLabelsForVariables = d_itemsSelectorBuildingBlockWidget_annotationLabelsForVariable->selectedItemsIdentifierAndNumber().first;

    parameters->annotationLabelsForItems = d_itemsSelectorBuildingBlockWidget_annotationLabelsForItem->selectedItemsIdentifierAndNumber().first;

    parameters->annotationIdentifiersForItems = d_itemsSelectorBuildingBlockWidget_annotationIdentifiersForItem->selectedItemsIdentifierAndNumber().first;

    parameters->exportFormat = d_comboBoxBuildingBlockWidget_exportFormat->pointerToInternalComboBox()->currentText();

}

void ExportToJSONFileDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsForVariable->setItemsIdentifier(d_baseDataset->annotations(orientation).labels());

    d_itemsSelectorBuildingBlockWidget_annotationLabelsForItem->setItemsIdentifier(d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

}

void ExportToJSONFileDialog::items_selectionChanged(const QStringList &selectedItems, const QStringList &notSelectedItems)
{

    Q_UNUSED(selectedItems);

    Q_UNUSED(notSelectedItems);

    this->annotationLabelsForItem_selectionChanged(d_itemsSelectorBuildingBlockWidget_annotationLabelsForItem->selectedItemsIdentifierAndNumber().first, QStringList());

}

void ExportToJSONFileDialog::annotationLabelsForItem_selectionChanged(const QStringList &selectedItemAnnotationLabels, const QStringList &notSelectedItemAnnotationLabels)
{

    Q_UNUSED(notSelectedItemAnnotationLabels);

    QStringList curatedLabels;

    if (selectedItemAnnotationLabels.isEmpty()) {

        curatedLabels = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber().first;

    } else {

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

        for (int i = 0; i < selectedItemIdentifiersAndIndexes.second.size(); ++i) {

            QList<QVariant> annotationValues;

                annotationValues = d_baseDataset->annotationValues(selectedItemIdentifiersAndIndexes.second.at(i), selectedItemAnnotationLabels, BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget->selectedOrientation()));

            QString temp = annotationValues.first().toString();

            for (int j = 1; j < annotationValues.size(); ++j)
                temp += " \\\\\\\\\\\\ " + annotationValues.at(j).toString();

            temp.remove('"');

            curatedLabels.append(temp);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

        }

    }

    curatedLabels.removeOne(QString());

    QSet<QString> uniqueCuratedLabels;

    QStringList uniqueCuratedLabelsOrderPreserved;

    for (const QString &curatedLabel : curatedLabels) {

        if (!uniqueCuratedLabels.contains(curatedLabel)) {

            uniqueCuratedLabels.insert(curatedLabel);

            uniqueCuratedLabelsOrderPreserved.append(curatedLabel);

        }
    }

    d_itemsSelectorBuildingBlockWidget_annotationIdentifiersForItem->setItemsIdentifier(uniqueCuratedLabelsOrderPreserved);

}
