#ifndef PERMUTATIONOPTIONSBUILDINGBLOCKWIDGET_H
#define PERMUTATIONOPTIONSBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QCheckBox>
#include <QSpinBox>

#include "dialogs/basedialog.h"

namespace Ui {

class PermutationOptionsBuildingBlockWidget;

}

class PermutationOptionsBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit PermutationOptionsBuildingBlockWidget(BaseDialog *parent = 0, const QString &widgetId = QString());

    ~PermutationOptionsBuildingBlockWidget();

    QCheckBox *pointerToInternalCheckBox_enable();

    QSpinBox *pointerToInternalSpinBox_FDR();

    QSpinBox *pointerToInternalSpinBox_CL();

    QSpinBox *pointerToInternalSpinBox_Permutations();

private:

    Ui::PermutationOptionsBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

signals:

    void delimiterChanged();

private slots:

    void checkBox_Enable_stateChanged(int state);

};

#endif // PERMUTATIONOPTIONSBUILDINGBLOCKWIDGET_H
