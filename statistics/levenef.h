#ifndef LEVENEF_H
#define LEVENEF_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/fisher_f.hpp"

template <typename T>
class LeveneF : public BaseStatisticalTest
{

public:

    LeveneF();

    LeveneF(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~LeveneF();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;



    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    QVector<QVector<T> > d_structuredMeasurements;

    QVector<QPair<double, double> > d_sampleMeanAndVariance;

    double d_df1;

    double d_df2;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation LeveneF<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool LeveneF<T>::d_hasEffectSize = false;

template <typename T>
QStringList LeveneF<T>::d_sampleDescriptiveLabels = {"Count", "Mean", "Variance"};

template <typename T>
int LeveneF<T>::d_maximumNumberOfSamples = std::numeric_limits<int>::max();

template <typename T>
int LeveneF<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int LeveneF<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString LeveneF<T>::d_nameOfEffectSize = "NA";

template <typename T>
QString LeveneF<T>::d_nameOfTransformedEffectSize = "NA";

template <typename T>
QString LeveneF<T>::d_nameOfStatisticalTest = "Levene F";

template <typename T>
bool LeveneF<T>::d_rankedBased = false;

template <typename T>
QStringList LeveneF<T>::d_testDescriptiveLabels = {"Levene's F", "Degree of freedom 1", "Degree of freedom 2", "Significance (2-tailed)"};

template <typename T>
LeveneF<T>::LeveneF() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
LeveneF<T>::LeveneF(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    this->createStructuredMeasurements();

}

template <typename T>
LeveneF<T>::~LeveneF()
{

}

template <typename T>
QVector<QVariant> LeveneF<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i)
        sampleDescriptiveValues << d_structuredMeasurements.at(i).size() << d_sampleMeanAndVariance.at(i).first << d_sampleMeanAndVariance.at(i).second;

    return sampleDescriptiveValues;

}

template <typename T>
void LeveneF<T>::calculateStatistic()
{

    double w1 = 0.0;

    double z_ = 0.0;

    QVector<double> mean_i;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i) {

        d_sampleMeanAndVariance[i] = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(i));

        for (T &measurement: d_structuredMeasurements[i])
            measurement = std::fabs(measurement - d_sampleMeanAndVariance.at(i).first);

        double z_i = MathDescriptives::sum(d_structuredMeasurements.at(i));

        z_ += z_i;

        z_i = z_i / d_structuredMeasurements.at(i).size();

        mean_i << z_i;

        for (const T &measurement: d_structuredMeasurements.at(i))
            w1 += (measurement - z_i) * (measurement - z_i);

    }

    z_ = z_ / double(d_measurements.size());

    double w2 = 0.0;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i)
        w2 += d_structuredMeasurements.at(i).size() * ((mean_i.at(i) - z_) * (mean_i.at(i) - z_));

    d_statistic = ((double(d_measurements.size()) - double(d_structuredMeasurements.size())) / double(d_structuredMeasurements.size() - 1.0)) * (w2 / w1);

}

template <typename T>
void LeveneF<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    d_df1 = d_structuredMeasurements.size() - 1.0;

    d_df2 = d_measurements.size() - d_structuredMeasurements.size();

    boost::math::fisher_f dist(d_df1, d_df2);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void LeveneF<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void LeveneF<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> LeveneF<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_statistic << d_df1 << d_df2 << d_pValue;

    return descriptiveValues;

}

template <typename T>
void LeveneF<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize((*std::max_element(d_itemSampleCodedIdentifiers.begin(), d_itemSampleCodedIdentifiers.end())) + 1);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

    d_sampleMeanAndVariance.resize(d_structuredMeasurements.size());
}

template <typename T>
void LeveneF<T>::_reset()
{

    this->createStructuredMeasurements();

}

#endif // LEVENEF_H
