#ifndef PROCESSCONTROLER_H
#define PROCESSCONTROLER_H

#include <QObject>
#include <QThread>
#include <QSharedPointer>
#include <QString>

#include "base/baseworkerclass.h"

class ProcessControler : public QObject
{

    Q_OBJECT

public:

    explicit ProcessControler(QObject *parent = nullptr);

    ~ProcessControler();

    bool processRunning() const;

private:

    BaseWorkerClass *d_currentBaseWorkerClass;

    QThread *d_workerThread;

signals:

    void addToRecentlyOpenedFiles(const QString &name, const QString &path);

    void annotationsBeginChange(const QUuid &uuid, BaseMatrix::Orientation orientation);

    void annotationsEndChange(const QUuid &uuid, BaseMatrix::Orientation orientation);

    void appendLogItem(LogListModel::Type type, const QString &text);

    void datasetAvailable(BaseDataset *dataset);

    void datasetBeginChange(const QUuid &uuid);

    void datasetEndChange(const QUuid &uuid);

    void matrixDataBeginChange(const QUuid &uuid);

    void matrixDataEndChange(const QUuid &uuid);

    void processStarted(const QString &processTitle);

    void processStopped();

    void projectAvailable(Project *project);

    void resultAvailable(Result *result);

    void resultItemAvailable(BaseResultItem *resultItem);

    void startProgress(int index, const QString &label, const double &minimumValue, const double &maximumValue);

    void stopProgress(int index);

    void updateProgress(int index, const double &value);

public slots:

    void startProcess(BaseWorkerClass *baseWorkerClass = nullptr);

    void cancelWorkerThread();

private slots:

    void deleteCurrentBaseWorkerClass();

};

#endif // PROCESSCONTROLER_H
