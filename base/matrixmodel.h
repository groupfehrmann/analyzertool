#ifndef MATRIXMODEL_H
#define MATRIXMODEL_H

#include <QAbstractTableModel>
#include <QSharedPointer>
#include <QMetaType>
#include <QColor>
#include <QSortFilterProxyModel>

#include <numeric>

#include "base/basedataset.h"
#include "base/dataset.h"
#include "widgets/datasetwidget.h"

class MatrixSortFilterProxyModel : public QSortFilterProxyModel
{

    Q_OBJECT

public:

    explicit MatrixSortFilterProxyModel(QObject *parent = nullptr);

    bool showSelectedOnly() const;

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

    bool filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const;

private:

    bool d_showSelectedOnly;

public slots:

    void setShowSelectedOnly(bool showSelectedOnly);

};

class MatrixModel : public QAbstractTableModel
{

    Q_OBJECT

    friend class MatrixSortFilterProxyModel;

public:

    MatrixModel(QObject *parent = nullptr, const QSharedPointer<BaseDataset> &baseDataset = QSharedPointer<BaseDataset>());

    int columnCount(QModelIndex const &parent = QModelIndex()) const;

    QVariant data(QModelIndex const &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(QModelIndex const &index) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    bool insertColumns(int column, int count, const QModelIndex &parent= QModelIndex());

    bool insertRows(int row, int count, const QModelIndex &parent= QModelIndex());

    bool removeColumns(int column, int count, const QModelIndex &parent= QModelIndex());

    bool removeRows(int row, int count, const QModelIndex &parent= QModelIndex());

    int rowCount(QModelIndex const &parent = QModelIndex()) const;

    bool setData(QModelIndex const &index, QVariant const &value, int role = Qt::EditRole);

    bool setHeaderData(int section, Qt::Orientation orientation, QVariant const &value, int role = Qt::EditRole);

    void setSelectionStatus(int section, bool selectionStatus, BaseMatrix::Orientation orientation);

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    bool d_blockModel;

public slots:

    void beginReset();

    void endReset();

};

#endif // MATRIXMODEL_H
