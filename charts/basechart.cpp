#include "basechart.h"

BaseChart::BaseChart(QObject *parent) :
    QObject(parent)
{

    this->moveToThread(QApplication::instance()->thread());

    this->connect(this, SIGNAL(finishedInitializationOfChart()), &d_eventLoop, SLOT(quit()));

    this->connect(this, SIGNAL(deleteChart()), this, SLOT(_deleteChart()));

    this->connect(this, SIGNAL(finishedDeletingChart()), &d_eventLoop, SLOT(quit()));

}

BaseChart::~BaseChart()
{

    this->moveToThread(QApplication::instance()->thread());

    emit deleteChart();

    d_eventLoop.exec();

}

const QSharedPointer<QChart> &BaseChart::chart()
{

    return d_chart;

}

void BaseChart::_deleteChart()
{

    d_chart.clear();

    emit finishedDeletingChart();

}
