#ifndef _FISHEREXACT_H
#define _FISHEREXACT_H

#include <QVector>

//#include "sortutils.h"

namespace FisherExact {

    int desc(const void * v1, const void * v2);

    double getPartialSumm(QVector<unsigned int> &row, QVector<unsigned int> &sumcolumn, int n);

    void getNumerator(double &summ, QVector<unsigned int> &row, int rowsum, QVector<unsigned int> &sumcolumn, double threshold, int index, int n, double global_denominator);

    double getDenominator(int suma, int sumb );

    double FisherExact(QVector<unsigned int> const &A, QVector<unsigned int> const &B);

}

#endif // _FISHEREXACT_H
