#include "calculatemodulescoresdialog.h"

CalculateModuleScoresDialog::CalculateModuleScoresDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new CalculateModuleScoresWorkerClass::Parameters;

    this->setupWidgets();

}

CalculateModuleScoresDialog::~CalculateModuleScoresDialog()
{

   delete static_cast<CalculateModuleScoresWorkerClass::Parameters *>(d_parameters);

}

void CalculateModuleScoresDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 2, 0);

    d_selectFileBuildingBlockWidget = new SelectFileBuildingBlockWidget(this, "selectFileBuildingBlockWidget", "Select file defining modules", QFileDialog::AcceptOpen, true);

    d_gridLayout->addWidget(d_selectFileBuildingBlockWidget, 3, 0);

    d_comboBoxBuildingBlockWidget_formatFileDefiningModules = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_formatFileDefiningModules", "Select format of file defining modules", {"module defined by gene membership only", "module defined by gene membership and weight", "module defined by weight and gene membership"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_formatFileDefiningModules, 4, 0);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables", "Select annotation labels (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables, 5, 0);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory (optional)", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 6, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void CalculateModuleScoresDialog::initializeParameters()
{

    CalculateModuleScoresWorkerClass::Parameters *parameters = static_cast<CalculateModuleScoresWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->annotationLabelsDefiningAnnotationValuesForVariables = d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables->selectedItemsIdentifierAndNumber().first;

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->fileDefiningModules = d_selectFileBuildingBlockWidget->pointerToInternalLineEdit()->text();

    parameters->formatOfFileDefiningModules = d_comboBoxBuildingBlockWidget_formatFileDefiningModules->pointerToInternalComboBox()->currentText();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

}

void CalculateModuleScoresDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables->setItemsIdentifier(d_baseDataset->annotations(orientation).labels());

}

