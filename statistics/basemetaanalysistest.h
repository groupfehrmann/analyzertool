#ifndef BASEMETAANALYSISTEST_H
#define BASEMETAANALYSISTEST_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include "statistics/basestatisticaltest.h"

class BaseMetaAnalysisTest
{

public:

    BaseMetaAnalysisTest(const QString &nameOfMetaAnalysisTest, const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    virtual ~BaseMetaAnalysisTest();

    virtual QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const = 0;

    QVector<QVariant> baseStatisticalTestDescriptiveValues();

    virtual const QStringList metaAnalysisTestDescriptiveLabels() const = 0;

    QVector<QVariant> metaAnalysisTestDescriptiveValues();

    bool isSignOfStatisticNegative() const;

    double minusLog10TransformedPValue();

    const QString &nameOfMetaAnalysisTest() const;

    const double &pValue();

    void reset();

    const double &statistic();

    double zTransformedPValue();

protected:

    virtual void _reset() = 0;

    const QVector<QSharedPointer<BaseStatisticalTest> > &d_baseStatisticalTests;

    const QString &d_nameOfMetaAnalysisTest;

    double d_pValue;

    double d_statistic;

    virtual QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues() = 0;

    virtual QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues() = 0;

    virtual void calculateStatistic() = 0;

    virtual void calculatedPValue() = 0;

};

#endif // BASEMETAANALYSISTEST_H
