#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QScrollBar>

#include "base/loglistmodel.h"

namespace Ui {

class LogWidget;

}

class LogWidget : public QWidget
{

    Q_OBJECT

public:

    explicit LogWidget(QWidget *parent = nullptr);

    ~LogWidget();

private:

    Ui::LogWidget *ui;

    LogListModel d_logListModel;

public slots:

    void appendLogItem(LogListModel::Type type, const QString &text);

private slots:

    void exportLog();

};

#endif // LOGWIDGET_H
