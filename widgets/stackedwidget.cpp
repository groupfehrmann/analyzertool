#include "stackedwidget.h"

StackedWidget::StackedWidget(QWidget *parent) :
    QStackedWidget(parent)
{

    this->QStackedWidget::addWidget(new SplashWidget(this));

}

void StackedWidget::addDataset(const QSharedPointer<BaseDataset> &baseDataset)
{

    DatasetWidget *datasetWidget = new DatasetWidget(nullptr, baseDataset);

    this->connect(datasetWidget, SIGNAL(appendLogItem(LogListModel::Type,QString)), this, SIGNAL(appendLogItem(LogListModel::Type,QString)));

    d_uuidToWidget.insert(baseDataset->universallyUniqueIdentifier(), datasetWidget);

    this->QStackedWidget::addWidget(datasetWidget);

}

void StackedWidget::addResultItem(const QSharedPointer<BaseResultItem> &resultItem)
{

    if (resultItem->type() == BaseResultItem::TABLE) {

        TableResultItemWidget *tableResultItemWidget = new TableResultItemWidget(nullptr, qSharedPointerCast<TableResultItem>(resultItem));

        d_uuidToWidget.insert(resultItem->universallyUniqueIdentifier(), tableResultItemWidget);

        this->QStackedWidget::addWidget(tableResultItemWidget);

    } else if (resultItem->type() == BaseResultItem::PLOT) {

        PlotResultItemWidget *plotResultItemWidget = new PlotResultItemWidget(nullptr, qSharedPointerCast<PlotResultItem>(resultItem));

        d_uuidToWidget.insert(resultItem->universallyUniqueIdentifier(), plotResultItemWidget);

        this->QStackedWidget::addWidget(plotResultItemWidget);

    } else if (resultItem->type() == BaseResultItem::TEXT) {

        TextResultItemWidget *textResultItemWidget = new TextResultItemWidget(nullptr, qSharedPointerCast<TextResultItem>(resultItem));

        d_uuidToWidget.insert(resultItem->universallyUniqueIdentifier(), textResultItemWidget);

        this->QStackedWidget::addWidget(textResultItemWidget);

    }

}

void StackedWidget::annotationsBeginChange(const QUuid &uuid, BaseMatrix::Orientation orientation)
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(d_uuidToWidget.value(uuid)))
        datasetWidget->annotationsBeginChange(orientation);

}

void StackedWidget::annotationsEndChange(const QUuid &uuid, BaseMatrix::Orientation orientation)
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(d_uuidToWidget.value(uuid)))
        datasetWidget->annotationsEndChange(orientation);

}

QChartView *StackedWidget::currentChartView()
{

    if (PlotResultItemWidget *plotResultItemWidget = dynamic_cast<PlotResultItemWidget *>(this->currentWidget()))
        return plotResultItemWidget->chartView();
    else
        return nullptr;

}

QAbstractItemModel *StackedWidget::currentTableModel()
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(this->currentWidget()))
        return datasetWidget->currentTableModel();
    else if (TableResultItemWidget *tableResultItemWidget = dynamic_cast<TableResultItemWidget *>(this->currentWidget()))
        return tableResultItemWidget->currentTableModel();
    else
        return nullptr;

}

QTableView *StackedWidget::currentTableView()
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(this->currentWidget()))
        return datasetWidget->currentTableView();
    else if (TableResultItemWidget *tableResultItemWidget = dynamic_cast<TableResultItemWidget *>(this->currentWidget()))
        return tableResultItemWidget->currentTableView();
    else
        return nullptr;

}

void StackedWidget::datasetBeginChange(const QUuid &uuid)
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(d_uuidToWidget.value(uuid)))
        datasetWidget->beginChange();

}

void StackedWidget::datasetEndChange(const QUuid &uuid)
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(d_uuidToWidget.value(uuid)))
        datasetWidget->endChange();

}

int StackedWidget::indexOfSelectedTabInDatasetWidget()
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(this->currentWidget()))
        return datasetWidget->indexOfSelectedTab();
    else
        return -1;

}

void StackedWidget::matrixDataBeginChange(const QUuid &uuid)
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(d_uuidToWidget.value(uuid)))
        datasetWidget->matrixDataBeginChange();

}

void StackedWidget::matrixDataEndChange(const QUuid &uuid)
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(d_uuidToWidget.value(uuid)))
        datasetWidget->matrixDataEndChange();

}

void StackedWidget::removeWidget(const QUuid &uuid)
{

    if (d_uuidToWidget.contains(uuid)) {

        this->QStackedWidget::removeWidget(d_uuidToWidget.value(uuid));

        delete d_uuidToWidget[uuid];

        d_uuidToWidget.remove(uuid);

    }

}

void StackedWidget::setCurrentWidget(const QUuid &uuid)
{

    if (d_uuidToWidget.contains(uuid))
        this->QStackedWidget::setCurrentWidget(d_uuidToWidget.value(uuid));
    else
        this->QStackedWidget::setCurrentIndex(0);

}

void StackedWidget::showSelectedOnly(const QUuid &uuid, bool showSelectedOnly)
{

    if (DatasetWidget *datasetWidget = dynamic_cast<DatasetWidget *>(d_uuidToWidget.value(uuid)))
        datasetWidget->showSelectedOnly(showSelectedOnly);

}
