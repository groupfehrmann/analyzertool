#ifndef SPEARMANR_H
#define SPEARMANR_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/vectoroperations.h"

#include "boost/math/distributions/students_t.hpp"
#include "boost/math/distributions/normal.hpp"

template <typename T>
class SpearmanR : public BaseStatisticalTest
{

public:

    SpearmanR();

    SpearmanR(const QVector<T> &independentVector, QVector<T> &dependentVector, bool dependentVectorIsRanked = false, const double &tiesCorrectionFactorForDependentVector = 0, bool pairwiseCompleteMethod = false);

    ~SpearmanR();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;


    QVector<T> d_independentVector;

    QVector<T> *d_dependentVector;

    QVector<T> _dependentVector;

    double d_tiesCorrectionFactorIndependentVector;

    double d_tiesCorrectionFactorDependentVector;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation SpearmanR<T>::d_effectSizeTransformation = BaseStatisticalTest::FISHER_R_TO_Z;

template <typename T>
bool SpearmanR<T>::d_hasEffectSize = true;

template <typename T>
QStringList SpearmanR<T>::d_sampleDescriptiveLabels = {"Count"};

template <typename T>
int SpearmanR<T>::d_maximumNumberOfSamples = 1;

template <typename T>
int SpearmanR<T>::d_minimumNumberOfSamples = 1;

template <typename T>
int SpearmanR<T>::d_minimumNumberOfItemsPerSample = 4;

template <typename T>
QString SpearmanR<T>::d_nameOfEffectSize = "Fisher's z' transformation";

template <typename T>
QString SpearmanR<T>::d_nameOfTransformedEffectSize = "Fisher's z' transformation";

template <typename T>
QString SpearmanR<T>::d_nameOfStatisticalTest = "Spearman R";

template <typename T>
bool SpearmanR<T>::d_rankedBased = true;

template <typename T>
QStringList SpearmanR<T>::d_testDescriptiveLabels = {"Fisher's z' transformation", "Standard error Fisher's z' transformation", "Spearman R", "Lower bound 95% confidence interval of Spearman R", "Upper bound 95% confidence interval of Spearman R", "Significance (2-tailed)"};

template <typename T>
SpearmanR<T>::SpearmanR() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_dependentVector(&_dependentVector)
{

}

template <typename T>
SpearmanR<T>::SpearmanR(const QVector<T> &independentVector, QVector<T> &dependentVector, bool dependentVectorIsRanked, const double &tiesCorrectionFactorForDependentVector, bool pairwiseCompleteMethod) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_independentVector(independentVector), d_dependentVector(&dependentVector)
{

    if (pairwiseCompleteMethod) {

        d_independentVector.clear();

        for (int i = 0; i < dependentVector.size(); ++i) {

            if (std::isfinite(dependentVector.at(i)) && std::isfinite(independentVector.at(i))) {

                _dependentVector << dependentVector.at(i);

                d_independentVector << independentVector.at(i);

            }

        }

        d_dependentVector = &_dependentVector;

    }

    if (dependentVectorIsRanked)
        d_tiesCorrectionFactorDependentVector = tiesCorrectionFactorForDependentVector;
    else
        d_tiesCorrectionFactorDependentVector = VectorOperations::crank(*d_dependentVector);


    d_tiesCorrectionFactorIndependentVector = VectorOperations::crank(d_independentVector);

}

template <typename T>
SpearmanR<T>::~SpearmanR()
{

}

template <typename T>
QVector<QVariant> SpearmanR<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    sampleDescriptiveValues << d_independentVector.size();

    return sampleDescriptiveValues;

}

template <typename T>
void SpearmanR<T>::calculateStatistic()
{

    double n = d_independentVector.size();

    double d = 0.0;

    for (int i = 0; i < n; ++i)
        d += (d_independentVector.at(i) - d_dependentVector->at(i)) * (d_independentVector.at(i) - d_dependentVector->at(i));

    double en3n = n * n * n - n;

    double fac = (1.0 - d_tiesCorrectionFactorIndependentVector / en3n) * (1.0 - d_tiesCorrectionFactorDependentVector / en3n);

    d_statistic = (1.0 - (6.0 / en3n) * (d + (d_tiesCorrectionFactorIndependentVector + d_tiesCorrectionFactorDependentVector) / 12.0)) / std::sqrt(fac);

}

template <typename T>
void SpearmanR<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::students_t dist(d_independentVector.size() - 2.0);

    d_pValue = std::numeric_limits<double>::min();

    double fac = (d_statistic + 1.0) * (1.0 - d_statistic);

    if (fac > 0.0)
        d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, fabs(d_statistic * std::sqrt((d_independentVector.size() - 2.0) / fac))));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void SpearmanR<T>::calculateEffectSize()
{

    double value = double(1.0 + d_statistic) / double(1.0 - d_statistic);

    if (std::isinf(value))
        value = std::numeric_limits<double>::max();

    d_effectSize = 0.5 * std::log(value);

}

template <typename T>
void SpearmanR<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = (1.0 / std::sqrt(d_independentVector.size() - 3.0));

}

template <typename T>
QVector<QVariant> SpearmanR<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    boost::math::normal normalDistribution(0.0, 1.0);

    double w = boost::math::quantile(boost::math::complement(normalDistribution, 0.025)) * d_standardErrorOfEffectSize;

    descriptiveValues << d_independentVector.size();

    double rLower = (std::exp(2.0 * (d_effectSize - w)) - 1.0) / (std::exp(2.0 * (d_effectSize - w)) + 1.0);

    double rUpper = (std::exp(2.0 * (d_effectSize + w)) - 1.0) / (std::exp(2.0 * (d_effectSize + w)) + 1.0);

    descriptiveValues << d_effectSize << d_standardErrorOfEffectSize << d_statistic << rLower << rUpper << d_pValue;

    return descriptiveValues;

}

template <typename T>
void SpearmanR<T>::createStructuredMeasurements()
{

}

template <typename T>
void SpearmanR<T>::_reset()
{

}

#endif // SPEARMANR_H
