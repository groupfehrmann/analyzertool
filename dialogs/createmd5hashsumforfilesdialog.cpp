#include "createmd5hashsumforfilesdialog.h"


CreateMD5HashSumForFilesDialog::CreateMD5HashSumForFilesDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings)
{

    d_parameters = new CreateMD5HashSumForFilesWorkerClass::Parameters;

    this->setupWidgets();

}

CreateMD5HashSumForFilesDialog::~CreateMD5HashSumForFilesDialog()
{

    delete static_cast<CreateMD5HashSumForFilesWorkerClass::Parameters *>(d_parameters);

}

void CreateMD5HashSumForFilesDialog::initializeWidgets()
{

    d_selectImportDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectImportDirectory", "Select import directory", true);

    d_gridLayout->addWidget(d_selectImportDirectory, 0, 0);

    d_lineEditBuildingBlockWidget_patternToFilterValidFiles = new LineEditBuildingBlockWidget(this, "id", "Pattern to filter valid files", "*.*");

    d_gridLayout->addWidget(d_lineEditBuildingBlockWidget_patternToFilterValidFiles, 1, 0);

    d_comboBoxBuildingBlockWidget_copyUniqueFilesToExportDirectory = new ComboBoxBuildingBlockWidget(this, "d_comboBoxBuildingBlockWidget_copyUniqueFilesToExportDirectory", "Copy unique files to export directory", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_copyUniqueFilesToExportDirectory, 2, 0);

    d_selectExportDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectExportDirectory", "Select export directory (optional)", true);

    d_gridLayout->addWidget(d_selectExportDirectory, 3, 0);

}


void CreateMD5HashSumForFilesDialog::initializeParameters()
{

    CreateMD5HashSumForFilesWorkerClass::Parameters *parameters = static_cast<CreateMD5HashSumForFilesWorkerClass::Parameters *>(d_parameters);

    parameters->importDirectory = d_selectImportDirectory->pointerToInternalLineEdit()->text();

    parameters->exportDirectory = d_selectExportDirectory->pointerToInternalLineEdit()->text();

    parameters->copyUniqueFilesToExportDirectory = (d_comboBoxBuildingBlockWidget_copyUniqueFilesToExportDirectory->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->patternToFilterValidFiles = d_lineEditBuildingBlockWidget_patternToFilterValidFiles->pointerToInternalLineEdit()->text();

}
