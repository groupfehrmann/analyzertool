#ifndef CONVERTFUNCTIONS_H
#define CONVERTFUNCTIONS_H

#include <QVariant>
#include <QVector>
#include <QPair>
#include <QList>
#include <QMap>

#include <limits>
#include <tuple>
#include <cmath>

namespace ConvertFunctions {

    bool isValidNumber(const short &value);

    bool isValidNumber(const unsigned short &value);

    bool isValidNumber(const int &value);

    bool isValidNumber(const unsigned int &value);

    bool isValidNumber(const long long &value);

    bool isValidNumber(const unsigned long long &value);

    bool isValidNumber(const float &value);

    bool isValidNumber(const double &value);

    bool isValidNumber(const QChar &value);

    bool isValidNumber(const QString &value);

    bool isValidNumber(const bool &value);

    bool isValidNumber(const unsigned char &value);

    bool isValidNumber(const QVariant &value);


    bool canConvert(const QVariant &variant, short &value);

    bool canConvert(const QVariant &variant, unsigned short &value);

    bool canConvert(const QVariant &variant, int &value);

    bool canConvert(const QVariant &variant, unsigned int &value);

    bool canConvert(const QVariant &variant, long long &value);

    bool canConvert(const QVariant &variant, unsigned long long &value);

    bool canConvert(const QVariant &variant, float &value);

    bool canConvert(const QVariant &variant, double &value);

    bool canConvert(const QVariant &variant, QChar &value);

    bool canConvert(const QVariant &variant, QString &value);

    bool canConvert(const QVariant &variant, bool &value);

    bool canConvert(const QVariant &variant, unsigned char &value);

    bool canConvert(const QVariant &variant, QVariant &value);

    template <typename T>
    bool canConvert(const QVariant &variant) {

        T value;

        return ConvertFunctions::canConvert(variant, value);

    }

    template <typename T, typename U>
    void convert(const T &value, U &convertedValue) {

        ConvertFunctions::canConvert(QVariant(value), convertedValue);

    }

    template <typename T>
    void convert(const QVariant &variant, T &value) {

        ConvertFunctions::canConvert(variant, value);

    }

    template <>
    void convert(const short &value, short &convertedValue);

    template <>
    void convert(const unsigned short &value, unsigned short &convertedValue);

    template <>
    void convert(const int &value, int &convertedValue);

    template <>
    void convert(const unsigned int &value, unsigned int &convertedValue);

    template <>
    void convert(const long long &value, long long &convertedValue);

    template <>
    void convert(const unsigned long long &value, unsigned long long &convertedValue);

    template <>
    void convert(const float &value, float &convertedValue);

    template <>
    void convert(const double &value, double &convertedValue);

    template <>
    void convert(const QChar &value, QChar &convertedValue);

    template <>
    void convert(const QString &value, QString &convertedValue);

    template <>
    void convert(const bool &value, bool &convertedValue);

    template <>
    void convert(const unsigned char &value, unsigned char &convertedValue);

    template <>
    void convert(const QVariant &value, QVariant &convertedValue);


    template <typename T, typename U>
    QVector<T> convertVector(const QVector<U> &vector, bool *ok = nullptr, QString *conversionError = nullptr) {

        if (conversionError)
            *conversionError = "conversion error(s) to type " + QString(QMetaType::typeName(qMetaTypeId<T>())) + " for the following tokens:" ;

        if (ok)
            *ok = true;

        QVector<T> convertedVector(vector.size());

        for (int i = 0; i < vector.size(); ++i) {

            U value = vector.at(i);

            T convertedValue;

            if (!ConvertFunctions::canConvert(QVariant(value), convertedValue)) {

                if (conversionError)
                    (*conversionError) += " \"" + QVariant(value).toString() + "\"";

                if (ok)
                    *ok = false;

            }

            convertedVector[i] = convertedValue;

        }

        if (ok) {

            if (conversionError && *ok)
                *conversionError = QString();

        }

        return convertedVector;

    }

    template <>
    QVector<short> convertVector(const QVector<short> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<unsigned short> convertVector(const QVector<unsigned short> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<int> convertVector(const QVector<int> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<unsigned int> convertVector(const QVector<unsigned int> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<long> convertVector(const QVector<long> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<long long> convertVector(const QVector<long long> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<unsigned long long> convertVector(const QVector<unsigned long long> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<float> convertVector(const QVector<float> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<double> convertVector(const QVector<double> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<QChar> convertVector(const QVector<QChar> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<QString> convertVector(const QVector<QString> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<bool> convertVector(const QVector<bool> &vector, bool *ok, QString *conversionError);

    template <>
    QVector<unsigned char> convertVector(const QVector<unsigned char> &vector, bool *ok, QString *conversionError);

    template <typename T, typename U>
    QList<T> convertList(const QList<U> &list, bool *ok = nullptr, QString *conversionError = nullptr) {

        if (conversionError)
            *conversionError = "conversion error(s) to type " + QString(QMetaType::typeName(qMetaTypeId<T>())) + " for the following tokens:" ;

        QList<T> convertedList;

        convertedList.reserve(list.size());

        for (int i = 0; i < list.size(); ++i) {

            U value = list.at(i);

            T convertedValue;

            if (!ConvertFunctions::canConvert(QVariant(value), convertedValue)) {

                if (conversionError)
                    (*conversionError) += " \"" + QVariant(value).toString() + "\"";

                if (ok)
                    *ok = false;

            }

            convertedList << convertedValue;

        }

        if (ok) {

            if (conversionError)
                *conversionError = QString();

            *ok = true;

        }

        return convertedList;

    }

    template <typename T, typename U>
    QList<T> convertList_noConversionErrorCheck(const QList<U> &list) {

        QList<T> convertedList;

        T convertedValue;

        for (const U &value : list)
            convertedList << ConvertFunctions::canConvert(QVariant(value), convertedValue);

        return convertedList;

    }

    template <typename T>
    QString toString(const T &sequence) {

        if (sequence.isEmpty())
            return QString();

        QString string;

        string += "\"" + QVariant(sequence.first()).toString() + "\"";

        for (int i = 1; i < sequence.size(); ++i)
            string += ", \"" + QVariant(sequence.at(i)).toString() + "\"";

        return string;

    }


    template <typename T, typename U>
    std::tuple<QList<int>, QVector<T>, QVector<U> > toTupleOfSequences(const QMap<int, std::tuple<T, U> > &integerToTuple) {

        std::tuple<QList<int>, QVector<T>, QVector<U> > tupleOfSequences;

        QMapIterator<int, std::tuple<T, U> > it(integerToTuple);

        while (it.hasNext()) {

            it.next();

            const std::tuple<T, U> &currentTuple(it.value());

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<0>(currentTuple);

            std::get<2>(tupleOfSequences) << std::get<1>(currentTuple);

        }

        return tupleOfSequences;

    }

    template <typename T, typename U, typename V>
    std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V> > toTupleOfSequences(const QMap<int, std::tuple<T, U, V> > &integerToTuple) {

        std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V> > tupleOfSequences;

        QMapIterator<int, std::tuple<T, U, V> > it(integerToTuple);

        while (it.hasNext()) {

            it.next();

            const std::tuple<T, U, V> &currentTuple(it.value());

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<0>(currentTuple);

            std::get<2>(tupleOfSequences) << std::get<1>(currentTuple);

            std::get<3>(tupleOfSequences) << std::get<2>(currentTuple);

        }

        return tupleOfSequences;

    }

    template <typename T, typename U, typename V, typename W>
    std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W> > toTupleOfSequences(const QMap<int, std::tuple<T, U, V, W> > &integerToTuple) {

        std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W> > tupleOfSequences;

        QMapIterator<int, std::tuple<T, U, V, W> > it(integerToTuple);

        while (it.hasNext()) {

            it.next();

            const std::tuple<T, U, V, W> &currentTuple(it.value());

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<0>(currentTuple);

            std::get<2>(tupleOfSequences) << std::get<1>(currentTuple);

            std::get<3>(tupleOfSequences) << std::get<2>(currentTuple);

            std::get<4>(tupleOfSequences) << std::get<3>(currentTuple);

        }

        return tupleOfSequences;

    }

    template <typename T, typename U, typename V, typename W, typename X>
    std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W> > toTupleOfSequences(const QMap<int, std::tuple<T, U, V, W, X> > &integerToTuple) {

        std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > tupleOfSequences;

        QMapIterator<int, std::tuple<T, U, V, W, X> > it(integerToTuple);

        while (it.hasNext()) {

            it.next();

            const std::tuple<T, U, V, W, X> &currentTuple(it.value());

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<0>(currentTuple);

            std::get<2>(tupleOfSequences) << std::get<1>(currentTuple);

            std::get<3>(tupleOfSequences) << std::get<2>(currentTuple);

            std::get<4>(tupleOfSequences) << std::get<3>(currentTuple);

            std::get<5>(tupleOfSequences) << std::get<4>(currentTuple);

        }

        return tupleOfSequences;

    }

    template <typename T, typename U>
    QList<QPair<int, std::tuple<T, U> > > toListOfPairedIndexWithTupleOfValues(const QMap<int, std::tuple<T, U> > &indexToTupleOfValues) {

        QList<QPair<int, std::tuple<T, U> > > listOfPairedIndexWithTupleOfValue;

        listOfPairedIndexWithTupleOfValue.reserve(indexToTupleOfValues.size());

        QMapIterator<int, std::tuple<T, U> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            listOfPairedIndexWithTupleOfValue << QPair<int, std::tuple<T, U> >(it.key(), it.value());

        }

        return listOfPairedIndexWithTupleOfValue;

    }

    template <typename T, typename U, typename V>
    QList<QPair<int, std::tuple<T, U, V> > > toListOfPairedIndexWithTupleOfValues(const QMap<int, std::tuple<T, U, V> > &indexToTupleOfValues) {

        QList<QPair<int, std::tuple<T, U, V> > > listOfPairedIndexWithTupleOfValue;

        listOfPairedIndexWithTupleOfValue.reserve(indexToTupleOfValues.size());

        QMapIterator<int, std::tuple<T, U, V> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            listOfPairedIndexWithTupleOfValue << QPair<int, std::tuple<T, U, V> >(it.key(), it.value());

        }

        return listOfPairedIndexWithTupleOfValue;

    }

    template <typename T, typename U, typename V, typename W>
    QList<QPair<int, std::tuple<T, U, V, W> > > toListOfPairedIndexWithTupleOfValues(const QMap<int, std::tuple<T, U, V, W> > &indexToTupleOfValues) {

        QList<QPair<int, std::tuple<T, U, V, W> > > listOfPairedIndexWithTupleOfValue;

        listOfPairedIndexWithTupleOfValue.reserve(indexToTupleOfValues.size());

        QMapIterator<int, std::tuple<T, U, V, W> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            listOfPairedIndexWithTupleOfValue << QPair<int, std::tuple<T, U, V, W> >(it.key(), it.value());

        }

        return listOfPairedIndexWithTupleOfValue;

    }

    template <typename T, typename U, typename V, typename W, typename X>
    QList<QPair<int, std::tuple<T, U, V, W, X> > > toListOfPairedIndexWithTupleOfValues(const QMap<int, std::tuple<T, U, V, W, X> > &indexToTupleOfValues) {

        QList<QPair<int, std::tuple<T, U, V, W, X> > > listOfPairedIndexWithTupleOfValue;

        listOfPairedIndexWithTupleOfValue.reserve(indexToTupleOfValues.size());

        QMapIterator<int, std::tuple<T, U, V, W, X> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            listOfPairedIndexWithTupleOfValue << QPair<int, std::tuple<T, U, V, W, X> >(it.key(), it.value());

        }

        return listOfPairedIndexWithTupleOfValue;

    }

    template <typename T, typename U>
    std::tuple<QList<int>, QVector<T>, QVector<U> > toTupleOfSequences(const QList<QPair<int, std::tuple<T, U> > > &listOfPairedIndexWithTupleOfValues) {

        std::tuple<QList<int>, QVector<T>, QVector<U> > tupleOfSequences;

        for (const QPair<int, std::tuple<T, U> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::get<0>(tupleOfSequences.second) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<0>(pairedIndexWithTupleOfValues.second);

            std::get<2>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

        }

        return tupleOfSequences;

    }

    template <typename T, typename U, typename V>
    std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V> > toTupleOfSequences(const QList<QPair<int, std::tuple<T, U, V> > > &listOfPairedIndexWithTupleOfValues) {

        std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V> > tupleOfSequences;

        for (const QPair<int, std::tuple<T, U, V> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::get<0>(tupleOfSequences.second) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<0>(pairedIndexWithTupleOfValues.second);

            std::get<2>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

            std::get<3>(tupleOfSequences) << std::get<2>(pairedIndexWithTupleOfValues.second);

        }

        return tupleOfSequences;

    }

    template <typename T, typename U, typename V, typename W>
    std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W> > toTupleOfSequences(const QList<QPair<int, std::tuple<T, U, V, W> > > &listOfPairedIndexWithTupleOfValues) {

        std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W> > tupleOfSequences;

        for (const QPair<int, std::tuple<T, U, V, W> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::get<0>(tupleOfSequences.second) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<0>(pairedIndexWithTupleOfValues.second);

            std::get<2>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

            std::get<3>(tupleOfSequences) << std::get<2>(pairedIndexWithTupleOfValues.second);

            std::get<4>(tupleOfSequences) << std::get<3>(pairedIndexWithTupleOfValues.second);

        }

        return tupleOfSequences;

    }

    template <typename T, typename U, typename V, typename W, typename X>
    std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > toTupleOfSequences(const QList<QPair<int, std::tuple<T, U, V, W, X> > > &listOfPairedIndexWithTupleOfValues) {

        std::tuple<QList<int>, QVector<T>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > tupleOfSequences;

        for (const QPair<int, std::tuple<T, U, V, W, X> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::get<0>(tupleOfSequences.second) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<0>(pairedIndexWithTupleOfValues.second);

            std::get<2>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

            std::get<3>(tupleOfSequences) << std::get<2>(pairedIndexWithTupleOfValues.second);

            std::get<4>(tupleOfSequences) << std::get<3>(pairedIndexWithTupleOfValues.second);

            std::get<5>(tupleOfSequences) << std::get<4>(pairedIndexWithTupleOfValues.second);

        }

        return tupleOfSequences;

    }

    template<typename T, typename U>
    QHash<T, std::tuple<QList<int>, QVector<U> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QList<QPair<int, std::tuple<T, U> > > &listOfPairedIndexWithTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U> > > keyToTupleOfSequences;

        for (const QPair<int, std::tuple<T, U> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::tuple<QList<int>, QVector<U> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(pairedIndexWithTupleOfValues.second)]);

            std::get<0>(tupleOfSequences) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

        }

        return keyToTupleOfSequences;

    }

    template<typename T, typename U, typename V>
    QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QList<QPair<int, std::tuple<T, U, V> > > &listOfPairedIndexWithTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V> > > keyToTupleOfSequences;

        for (const QPair<int, std::tuple<T, U, V> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::tuple<QList<int>, QVector<U>, QVector<V> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(pairedIndexWithTupleOfValues.second)]);

            std::get<0>(tupleOfSequences) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

            std::get<2>(tupleOfSequences) << std::get<2>(pairedIndexWithTupleOfValues.second);

        }

        return keyToTupleOfSequences;

    }

    template<typename T, typename U, typename V, typename W>
    QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QList<QPair<int, std::tuple<T, U, V, W> > > &listOfPairedIndexWithTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W> > > keyToTupleOfSequences;

        for (const QPair<int, std::tuple<T, U, V, W> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(pairedIndexWithTupleOfValues.second)]);

            std::get<0>(tupleOfSequences) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

            std::get<2>(tupleOfSequences) << std::get<2>(pairedIndexWithTupleOfValues.second);

            std::get<3>(tupleOfSequences) << std::get<3>(pairedIndexWithTupleOfValues.second);

        }

        return keyToTupleOfSequences;

    }

    template<typename T, typename U, typename V, typename W, typename X>
    QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QList<QPair<int, std::tuple<T, U, V, W, X> > > &listOfPairedIndexWithTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > > keyToTupleOfSequences;

        for (const QPair<int, std::tuple<T, U, V, W> > &pairedIndexWithTupleOfValues : listOfPairedIndexWithTupleOfValues) {

            std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W>, QVector<W> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(pairedIndexWithTupleOfValues.second)]);

            std::get<0>(tupleOfSequences) << pairedIndexWithTupleOfValues.first;

            std::get<1>(tupleOfSequences) << std::get<1>(pairedIndexWithTupleOfValues.second);

            std::get<2>(tupleOfSequences) << std::get<2>(pairedIndexWithTupleOfValues.second);

            std::get<3>(tupleOfSequences) << std::get<3>(pairedIndexWithTupleOfValues.second);

            std::get<4>(tupleOfSequences) << std::get<4>(pairedIndexWithTupleOfValues.second);

        }

        return keyToTupleOfSequences;

    }

    template<typename T, typename U>
    QHash<T, std::tuple<QList<int>, QVector<U> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QMap<int, std::tuple<T, U> > &indexToTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U> > > keyToTupleOfSequences;

        QMapIterator<int, std::tuple<T, U> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            std::tuple<QList<int>, QVector<U> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(it.value())]);

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<1>(it.value());

        }

        return keyToTupleOfSequences;

    }

    template<typename T, typename U, typename V>
    QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QMap<int, std::tuple<T, U, V> > &indexToTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V> > > keyToTupleOfSequences;

        QMapIterator<int, std::tuple<T, U, V> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            std::tuple<QList<int>, QVector<U>, QVector<V> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(it.value())]);

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<1>(it.value());

            std::get<2>(tupleOfSequences) << std::get<2>(it.value());

        }

        return keyToTupleOfSequences;

    }

    template<typename T, typename U, typename V, typename W>
    QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QMap<int, std::tuple<T, U, V, W> > &indexToTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W> > > keyToTupleOfSequences;

        QMapIterator<int, std::tuple<T, U, V, W> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(it.value())]);

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<1>(it.value());

            std::get<2>(tupleOfSequences) << std::get<2>(it.value());

            std::get<3>(tupleOfSequences) << std::get<3>(it.value());

        }

        return keyToTupleOfSequences;

    }

    template<typename T, typename U, typename V, typename W, typename X>
    QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > > toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(const QMap<int, std::tuple<T, U, V, W, X> > &indexToTupleOfValues) {

        QHash<T, std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > > keyToTupleOfSequences;

        QMapIterator<int, std::tuple<T, U, V, W, X> > it(indexToTupleOfValues);

        while (it.hasNext()) {

            it.next();

            std::tuple<QList<int>, QVector<U>, QVector<V>, QVector<W>, QVector<X> > &tupleOfSequences(keyToTupleOfSequences[std::get<0>(it.value())]);

            std::get<0>(tupleOfSequences) << it.key();

            std::get<1>(tupleOfSequences) << std::get<1>(it.value());

            std::get<2>(tupleOfSequences) << std::get<2>(it.value());

            std::get<3>(tupleOfSequences) << std::get<3>(it.value());

            std::get<4>(tupleOfSequences) << std::get<4>(it.value());

        }

        return keyToTupleOfSequences;

    }

    template <typename T>
    QVector<T*> removeInvalidNumbers(const QVector<T*> &vector) {

        QVector<T*> temp;

        for (T *value : vector) {

            if (ConvertFunctions::isValidNumber(*value))
                temp << value;

        }

        return temp;

    }

    template <typename T>
    QVector<T> removeInvalidNumbers(const QVector<T> &vector) {

        QVector<T> temp;

        for (const T &value : vector) {

            if (ConvertFunctions::isValidNumber(value))
                temp << value;

        }

        return temp;

    }

    template <typename T>
    QVector<T> convertToValueVector(const QVector<T*> &vector) {

        QVector<T> temp;

        temp.reserve(vector.size());

        for (T *value : vector)
          temp << *value;

        return temp;

    }

    template <typename T>
    QVector<unsigned int> recodeToVectorOfUnsignedInt(const QVector<T> &vector, QVector<T> &rankedValues) {

        QHash<T, unsigned int> valueToUnsignedInt;

        QVector<unsigned int> recodedVector;

        recodedVector.reserve(vector.size());

        if (!rankedValues.isEmpty()) {

            for (const T &rankedValue : rankedValues)
                valueToUnsignedInt.insert(rankedValue, valueToUnsignedInt.count());

            for (const T &value : vector)
                recodedVector << valueToUnsignedInt.value(value);

        } else {

            for (const T &value : vector) {

                if (valueToUnsignedInt.contains(value))
                    recodedVector << valueToUnsignedInt.value(value);
                else
                    valueToUnsignedInt.insert(value, valueToUnsignedInt.count());

            }

            rankedValues.resize(valueToUnsignedInt.count());

            QHashIterator<T, unsigned int> it(valueToUnsignedInt);

            while (it.hasNext()) {

                it.next();

                rankedValues[it.value()] = it.key();

            }

        }

        return recodedVector;

    }

    template <typename T>
    QVector<unsigned int> recodeToVectorOfUnsignedInt(const QVector<T> &vector, QList<T> &rankedValues) {

        QHash<T, unsigned int> valueToUnsignedInt;

        QVector<unsigned int> recodedVector;

        recodedVector.reserve(vector.size());

        if (!rankedValues.isEmpty()) {

            for (const T &rankedValue : rankedValues)
                valueToUnsignedInt.insert(rankedValue, valueToUnsignedInt.count());

            for (const T &value : vector)
                recodedVector << valueToUnsignedInt.value(value);

        } else {

            for (const T &value : vector) {

                if (valueToUnsignedInt.contains(value))
                    recodedVector << valueToUnsignedInt.value(value);
                else
                    valueToUnsignedInt.insert(value, valueToUnsignedInt.count());

            }

            rankedValues.clear();

            for (int i = 0; i < valueToUnsignedInt.size(); ++i)
                rankedValues << T();

            QHashIterator<T, unsigned int> it(valueToUnsignedInt);

            while (it.hasNext()) {

                it.next();

                rankedValues[it.value()] = it.key();

            }

        }

        return recodedVector;

    }

}

#endif // CONVERTFUNCTIONS_H
