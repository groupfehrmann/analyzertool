#ifndef BASEDIALOG_H
#define BASEDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QGridLayout>

namespace Ui {

class BaseDialog;

}

class BaseDialog : public QDialog
{

    Q_OBJECT

public:

    explicit BaseDialog(QWidget *parent = 0, const QString &title = QString(), const QString &identifierForSettings = QString());

    ~BaseDialog();

    void *parameters();

    void setupWidgets();

    const QString &identifierForSettings() const;

protected:

    Ui::BaseDialog *ui;

    QString d_title;

    QString d_identifierForSettings;

    QGridLayout *d_gridLayout;

    void *d_parameters;

    virtual void initializeWidgets() = 0;

    virtual void initializeParameters() = 0;

};

#endif // BASEDIALOG_H
