#include "doublespinboxbuildingblockwidget.h"
#include "ui_doublespinboxbuildingblockwidget.h"

DoubleSpinBoxBuildingBlockWidget::DoubleSpinBoxBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, bool rememberSettings, int decimals, double minumum, double maximum, double value, double singleStep, const QString &prefix, const QString &suffix) :
    QWidget(parent), ui(new Ui::DoubleSpinBoxBuildingBlockWidget), d_rememberSettings(rememberSettings)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    ui->doubleSpinBox->setRange(minumum, maximum);

    ui->doubleSpinBox->setValue(value);

    ui->doubleSpinBox->setDecimals(decimals);

    ui->doubleSpinBox->setPrefix(prefix);

    ui->doubleSpinBox->setSuffix(suffix);

    ui->doubleSpinBox->setSingleStep(singleStep);

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/doubleSpinBoxBuildingBlockWidget");

    if (settings.contains("value"))
        ui->doubleSpinBox->setValue(settings.value("value").toDouble());

    settings.endGroup();

}

DoubleSpinBoxBuildingBlockWidget::~DoubleSpinBoxBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/doubleSpinBoxBuildingBlockWidget");

        settings.setValue("value", ui->doubleSpinBox->value());

        settings.endGroup();

    }

    delete ui;

}

QDoubleSpinBox *DoubleSpinBoxBuildingBlockWidget::pointerToInternalDoubleSpinBox()
{

    return ui->doubleSpinBox;

}

QLabel *DoubleSpinBoxBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}
