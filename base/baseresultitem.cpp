#include "baseresultitem.h"

BaseResultItemData::BaseResultItemData()
{

}

BaseResultItemData::BaseResultItemData(const BaseResultItemData &other) :
    QSharedData(other)
{

}

BaseResultItemData::~BaseResultItemData()
{

}

BaseResultItem::BaseResultItem(QObject *parent, const QString &name) :
    QObject(parent), d_data(new BaseResultItemData), d_uuid(QUuid::createUuid())
{

    d_data->d_name = name;

}

BaseResultItem::BaseResultItem(const BaseResultItem &other) :
    QObject(other.parent()), d_data(other.d_data), d_uuid(QUuid::createUuid())
{

}

BaseResultItem::~BaseResultItem()
{

}

const QString &BaseResultItem::name() const
{

    return d_data->d_name;

}

void BaseResultItem::setName(const QString &name)
{

    d_data->d_name = name;

    emit this->nameChanged(d_uuid, name);

}

BaseResultItem::Type BaseResultItem::type() const
{

    return static_cast<BaseResultItem::Type>(d_data->d_type);

}

const QUuid &BaseResultItem::universallyUniqueIdentifier() const
{

    return d_uuid;

}

BaseResultItem &BaseResultItem::operator=(const BaseResultItem &other)
{

    if (this != &other)
        d_data.operator =(other.d_data);

    return *this;

}

QDataStream& operator<<(QDataStream& out, BaseResultItem &baseResultItem)
{

    out << QString("_BaseResultItem_begin#");

    out << QString("d_type") << baseResultItem.d_data->d_type;

    out << QString("_BaseResultItem_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, BaseResultItem &baseResultItem)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_BaseResultItem_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_baseMatrix")) {

        } else if (label == QString("d_type")) {

            int temp;

            in >> temp;

            baseResultItem.d_data->d_type = static_cast<BaseResultItem::Type>(temp);

        } else if (label == QString("_BaseResultItem_end#"))
            return in;

    }

    return in;

}
