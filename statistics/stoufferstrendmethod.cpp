#include "stoufferstrendmethod.h"

QStringList StouffersTrendMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "Z-transformed ( Significance (2-tailed) )", "Sign of statistic", "Sample size"};

QStringList StouffersTrendMethod::d_metaAnalysisTestDescriptiveLabels = {"Z", "Significance (2-tailed)"};

QString StouffersTrendMethod::d_nameOfMetaAnalysisTest = "Stouffer's trend method";


StouffersTrendMethod::StouffersTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

StouffersTrendMethod::~StouffersTrendMethod()
{

}

QStringList StouffersTrendMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList StouffersTrendMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> StouffersTrendMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    boost::math::normal dist(0.0, 1.0);

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << -1.0 * boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0) << (baseStatisticalTest->isSignOfStatisticNegative() ? "-" : "+") << baseStatisticalTest->sampleSize();

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> StouffersTrendMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void StouffersTrendMethod::calculateStatistic()
{

    boost::math::normal dist(0.0, 1.0);

    d_statistic = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        if (baseStatisticalTest->isSignOfStatisticNegative())
            d_statistic -= -1.0 *boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0);
        else
            d_statistic += -1.0 *boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0);

    }

    d_statistic /= std::sqrt(d_baseStatisticalTests.size());

}

void StouffersTrendMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, std::fabs(d_statistic)));

}

void StouffersTrendMethod::_reset()
{

}
