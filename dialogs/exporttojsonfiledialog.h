#ifndef EXPORTTOJSONFILEDIALOG_H
#define EXPORTTOJSONFILEDIALOG_H

#include <QString>
#include <QSharedPointer>
#include <QCompleter>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "workerclasses/exporttojsonfileworkerclass.h"

class ExportToJSONFileDialog : public BaseDialog
{

    Q_OBJECT

public:

    ExportToJSONFileDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~ExportToJSONFileDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_annotationLabelsForVariable;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_annotationLabelsForItem;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_annotationIdentifiersForItem;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_exportFormat;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void items_selectionChanged(const QStringList &selectedItems, const QStringList &notSelectedItems);

    void annotationLabelsForItem_selectionChanged(const QStringList &selectedItemAnnotationLabels, const QStringList &notSelectedItemAnnotationLabels);

};

#endif // EXPORTTOJSONFILEDIALOG_H
