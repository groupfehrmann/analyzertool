#ifndef RESULTLIST_H
#define RESULTLIST_H

#include <QObject>
#include <QSharedData>
#include <QHash>
#include <QSharedPointer>
#include <QList>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>

#include "base/result.h"
#include "base/baseresultitem.h"

class ResultListData : public QSharedData
{

public:

    ResultListData();

    ResultListData(const ResultListData &other);

    ~ResultListData();

    QHash<QUuid, QSharedPointer<Result> > d_uuidToResult;

    QList<QUuid> d_uuidOfResults;

};

class ResultList : public QObject
{

    Q_OBJECT

public:

    ResultList(QObject *parent = nullptr);

    ResultList(const ResultList &other);

    ~ResultList();

    int numberOfResults() const;

    const QSharedPointer<Result> resultAt(int index) const;

    const QSharedPointer<Result> resultAt(const QUuid &uuid) const;

    ResultList &operator=(const ResultList &other);

private:

    QSharedDataPointer<ResultListData> d_data;

    mutable QReadWriteLock d_readWriteLock;

public slots:

    void appendResult(Result *result);

    void removeAllResults();

    void removeResult(const QUuid &uuid);

    void removeResult(int index);

    void removeResults(const QList<QUuid> &uuids);

    void removeResults(const QList<int> &indexes);

signals:

    void resultItemAdded(const QSharedPointer<BaseResultItem> &baseResultItem);

    void resultItemNameChanged(const QUuid &uuid, const QString &name);

    void resultItemRemoved(const QUuid &uuid);

    void resultAdded(const QSharedPointer<Result> &result);

    void resultNameChanged(const QUuid &uuid, const QString &name);

    void resultRemoved(const QUuid &uuid);

};

Q_DECLARE_TYPEINFO(ResultList, Q_MOVABLE_TYPE);

#endif // RESULTLIST_H
