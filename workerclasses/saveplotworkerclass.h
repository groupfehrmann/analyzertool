#ifndef SAVEPLOTWORKERCLASS_H
#define SAVEPLOTWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QFile>
#include <QFileInfo>
#include <QChartView>
#include <QImage>
#include <QPainter>
#include <QSizeF>
#include <QRectF>
#include <QImageWriter>

#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/iodatastream.h"

class SavePlotWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

        QChartView *chartViewToSave;

    };

    SavePlotWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    SavePlotWorkerClass::Parameters d_parameters;

};

#endif // SAVEPLOTWORKERCLASS_H
