#ifndef ANNOTATIONS_H
#define ANNOTATIONS_H

#include <QObject>
#include <QSharedData>
#include <QHash>
#include <QVariant>
#include <QPair>
#include <QString>
#include <QSet>
#include <QMetaType>
#include <QDataStream>

#include <tuple>

#include "base/convertfunctions.h"

#include <QTextStream>

class AnnotationsData : public QSharedData
{

public:

    AnnotationsData();

    AnnotationsData(const AnnotationsData &other);

    ~AnnotationsData();

    QHash<QString, QHash<QString, QVariant> > d_values;

    QStringList d_labels;

    QSet<QString> d_identifiers;

};

class Annotations : public QObject
{

    Q_OBJECT

public:

    Annotations(QObject *parent = nullptr);

    Annotations(const Annotations &other);

    ~Annotations();

    bool appendLabel(const QString &label);

    template <typename T> bool appendValue(const QString &identifier, const QString &label, const T &value) {

        if (d_data->d_values.value(label).contains(identifier))
            return (d_data->d_values[label][identifier] == QVariant(value));

        this->appendLabel(label);

        d_data->d_identifiers.insert(identifier);

        d_data->d_values[label][identifier] = value;

        return true;

    }

    bool containsLabel(const QString &label) const;

    bool containsIdentifier(const QString &identifier) const;

    bool containsIdentifier(const QString &identifier, const QString &label) const;

    const QSet<QString> &identifiers() const;

    template <typename T> QSet<QString> identifiers(const QString &label) const {

        return this->identifiers<T>(d_data->d_identifiers, label);

    }

    template <typename T> QSet<QString> identifiers(const QString &label, const T &validValue) const {

        return this->identifiers<T>(d_data->d_identifiers, label, validValue);

    }

    template <typename T> QSet<QString> identifiers(const QString &label, const QSet<T> &validValues = QSet<T>()) const {

        return this->identifiers<T>(d_data->d_identifiers, label, validValues);

    }

    template <typename T> QSet<QString> identifiers(const QStringList &labels, const QList<QSet<T> > &validValues) const {

        return this->identifiers<T>(d_data->d_identifiers, labels, validValues);

    }

    template <typename T> QSet<QString> identifiers(const QSet<QString> &inputIdentifiers, const QString &label) const {

        if (label.isEmpty())
            return inputIdentifiers;

        QSet<QString> _identifiers;

        QHash<QString, QVariant> identifierToValueForLabel = d_data->d_values.value(label);

        for (const QString &inputIdentifier: inputIdentifiers) {

            if (ConvertFunctions::canConvert<T>(identifierToValueForLabel.value(inputIdentifier)))
                _identifiers.insert(inputIdentifier);

        }

        return _identifiers;

    }
        
    template <typename T> QSet<QString> identifiers(const QSet<QString> &inputIdentifiers, const QString &label, const T &validValue) const {

        if (label.isEmpty())
            return inputIdentifiers;

        QSet<QString> _identifiers;

        QHash<QString, QVariant> identifierToValueForLabel = d_data->d_values.value(label);

        QVariant _validValue(validValue);

        for (const QString &inputIdentifier: inputIdentifiers) {

            if (identifierToValueForLabel.value(inputIdentifier) == _validValue)
                _identifiers.insert(inputIdentifier);

        }

        return _identifiers;

    }

    template <typename T> QSet<QString> identifiers(const QSet<QString> &inputIdentifiers, const QString &label, const QSet<T> &validValues = QSet<T>()) const {

        if (label.isEmpty())
            return inputIdentifiers;

        if (validValues.isEmpty())
            return this->identifiers<T>(inputIdentifiers, label);

        QSet<QString> _identifiers;

        QHash<QString, QVariant> identifierToValueForLabel = d_data->d_values.value(label);

        for (const QString &inputIdentifier : inputIdentifiers) {

            if (validValues.contains(identifierToValueForLabel.value(inputIdentifier).value<T>()))
                _identifiers << inputIdentifier;

        }

        return _identifiers;

    }

    template <typename T> QSet<QString> identifiers(const QSet<QString> &inputIdentifiers, const QStringList &labels, const QList<QSet<T> > &validValues) const {

        QSet<QString> _identifiers = inputIdentifiers;

        for (int i = 0; i < labels.size(); ++i) {

            const QString &currentLabel(labels.at(i));

            const QSet<T> &currentValidValues(validValues.at(i));

            _identifiers = this->identifiers(_identifiers, currentLabel, currentValidValues);

        }

        return _identifiers;

    }

    template <typename T> QHash<QString, T> identifierToValue(const QString &label) const {

        return this->identifierToValue<T>(d_data->d_identifiers, label);

    }

    template <typename T> QHash<QString, T> identifierToValue(const QString &label, const T &validValue) const {

        return this->identifierToValue<T>(d_data->d_identifiers, label, validValue);

    }

    template <typename T> QHash<QString, T> identifierToValue(const QString &label, const QSet<T> &validValues = QSet<T>()) const {

        return this->identifierToValue<T>(d_data->d_identifiers, label, validValues);

    }

    template <typename T> QHash<QString, T> identifierToValue(const QSet<QString> &inputIdentifiers, const QString &label) const {

        QHash<QString, T> _identifierToValue;

        QHash<QString, QVariant> identifierToValueForLabel = d_data->d_values.value(label);

        T convertedValue;

        for (const QString &inputIdentifier: inputIdentifiers) {

            if (label.isEmpty())
                _identifierToValue.insert(inputIdentifier, QVariant());
            else if (ConvertFunctions::canConvert<T>(identifierToValueForLabel.value(inputIdentifier), convertedValue))
                _identifierToValue.insert(inputIdentifier, convertedValue);

        }

        return _identifierToValue;

    }

    template <typename T> QHash<QString, T> identifierToValue(const QSet<QString> &inputIdentifiers, const QString &label, const T &validValue) const {

        QHash<QString, T> _identifierToValue;

        if (label.isEmpty())
            _identifierToValue = this->identifierToValue<T>(inputIdentifiers, label);
        else {

            for (const QString &identifier: this->identifiers(inputIdentifiers, label, validValue))
                _identifierToValue.insert(identifier, validValue);

        }

        return _identifierToValue;

    }

    template <typename T> QHash<QString, T> identifierToValue(const QSet<QString> &inputIdentifiers, const QString &label, const QSet<T> &validValues = QSet<T>()) const {

        if (validValues.isEmpty())
            this->identifierToValue<T>(inputIdentifiers, label);

        if (label.isEmpty())
            return this->identifierToValue<T>(inputIdentifiers, label);

        QHash<QString, T> _identifierToValue;

        for (const T &validValue: validValues)
            _identifierToValue.unite(this->identifierToValue(inputIdentifiers, label, validValue));

        return _identifierToValue;

    }

    template <typename T> QHash<QString, QList<T> > identifierToValues(const QStringList &labels) const {

        return this->identifierToValues<T>(d_data->d_identifiers, labels);

    }

    template <typename T> QHash<QString, QList<T> > identifierToValues(const QStringList &labels, const QList<QSet<T> > &validValues) const {

        return this->identifierToValues<T>(d_data->d_identifiers, labels, validValues);

    }

    template <typename T> QHash<QString, QList<T> > identifierToValues(const QSet<QString> &inputIdentifiers, const QStringList &labels) const {

        QHash<QString, QList<T> > _identifierToValues;

        QSet<QString> _identifiers = inputIdentifiers;

        for (const QString &label : labels)
            _identifiers = this->identifiers<T>(inputIdentifiers, label);

        for (const QString &identifier : _identifiers)
            _identifierToValues.insert(identifier, ConvertFunctions::convertList_noConversionErrorCheck<T, QVariant>(this->values(identifier, labels)));

        return _identifierToValues;

    }

    template <typename T> QHash<QString, QList<T> > identifierToValues(const QSet<QString> &inputIdentifiers, const QStringList &labels, const QList<QSet<T> > &validValues) const {

        QHash<QString, QList<T> > _identifierToValues;

        QSet<QString> _identifiers = inputIdentifiers;

        for (int i = 0; i < labels.size(); ++i)
            _identifiers = this->identifiers<T>(inputIdentifiers, labels.at(i), validValues.at(i));

        for (const QString &identifier : _identifiers)
            _identifierToValues.insert(identifier, ConvertFunctions::convertList_noConversionErrorCheck<T, QVariant>(this->values(identifier, labels)));

        return _identifierToValues;

    }

    template <typename T, typename U> QHash<QString, std::tuple<T, U> > identifierToTupleOfValues(const QString &label1, const QString &label2) const {

        return this->identifierToTupleOfValues<T, U>(d_data->d_identifiers, label1, QSet<T>(), label2, QSet<U>());

    }

    template <typename T, typename U, typename V> QHash<QString, std::tuple<T, U, V> > identifierToTupleOfValues(const QString &label1, const QString &label2, const QString &label3) const {

        return this->identifierToTupleOfValues<T, U, V>(d_data->d_identifiers, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>());

    }

    template <typename T, typename U, typename V, typename W> QHash<QString, std::tuple<T, U, V, W> > identifierToTupleOfValues(const QString &label1, const QString &label2, const QString &label3, const QString &label4) const {

        return this->identifierToTupleOfValues<T, U, V, W>(d_data->d_identifiers, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>(), label4, QSet<W>());

    }

    template <typename T, typename U, typename V, typename W, typename X> QHash<QString, std::tuple<T, U, V, W, X> > identifierToTupleOfValues(const QString &label1, const QString &label2, const QString &label3, const QString &label4, const QString &label5) const {

        return this->identifierToTupleOfValues<T, U, V, W, X>(d_data->d_identifiers, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>(), label4, QSet<W>(), label5, QSet<X>());

    }

    template <typename T, typename U> QHash<QString, std::tuple<T, U> > identifierToTupleOfValues(const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2) const {

        return this->identifierToTupleOfValues<T, U>(d_data->d_identifiers, label1, validValues1, label2, validValues2);

    }

    template <typename T, typename U, typename V> QHash<QString, std::tuple<T, U, V> > identifierToTupleOfValues(const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3) const {

        return this->identifierToTupleOfValues<T, U, V>(d_data->d_identifiers, label1, validValues1, label2, validValues2, label3, validValues3);

    }

    template <typename T, typename U, typename V, typename W> QHash<QString, std::tuple<T, U, V, W> > identifierToTupleOfValues(const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3, const QString &label4, const QSet<W> &validValues4) const {

        return this->identifierToTupleOfValues<T, U, V, W>(d_data->d_identifiers, label1, validValues1, label2, validValues2, label3, validValues3, label4, validValues4);

    }

    template <typename T, typename U, typename V, typename W, typename X> QHash<QString, std::tuple<T, U, V, W, X> > identifierToTupleOfValues(const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3, const QString &label4, const QSet<W> &validValues4, const QString &label5, const QSet<X> &validValues5) const {

        return this->identifierToTupleOfValues<T, U, V, W, X>(d_data->d_identifiers, label1, validValues1, label2, validValues2, label3, validValues3, label4, validValues4, label5, validValues5);

    }

    template <typename T, typename U> QHash<QString, std::tuple<T, U> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QString &label2) const {

        return this->identifierToTupleOfValues<T, U>(inputIdentifiers, label1, QSet<T>(), label2, QSet<U>());

    }

    template <typename T, typename U, typename V> QHash<QString, std::tuple<T, U, V> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QString &label2, const QString &label3) const {

        return this->identifierToTupleOfValues<T, U, V>(inputIdentifiers, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>());

    }

    template <typename T, typename U, typename V, typename W> QHash<QString, std::tuple<T, U, V, W> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QString &label2, const QString &label3, const QString &label4) const {

        return this->identifierToTupleOfValues<T, U, V, W>(inputIdentifiers, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>(), label4, QSet<W>());

    }

    template <typename T, typename U, typename V, typename W, typename X> QHash<QString, std::tuple<T, U, V, W, X> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QString &label2, const QString &label3, const QString &label4, const QString &label5) const {

        return this->identifierToTupleOfValues<T, U, V, W, X>(inputIdentifiers, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>(), label4, QSet<W>(), label5, QSet<X>());

    }

    template <typename T, typename U> QHash<QString, std::tuple<T, U> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2) const {

        QHash<QString, std::tuple<T, U> > _identifierToTupleOfValues;

        for (const QString &identifier : inputIdentifiers) {

            std::tuple<T, U> convertedValues;

            if (this->values(identifier, label1, validValues1, label2, validValues2, convertedValues))
                _identifierToTupleOfValues.insert(identifier, convertedValues);

        }

        return _identifierToTupleOfValues;

    }

    template <typename T, typename U, typename V> QHash<QString, std::tuple<T, U, V> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3) const {

        QHash<QString, std::tuple<T, U, V> > _identifierToTupleOfValues;

        for (const QString &identifier : inputIdentifiers) {

            std::tuple<T, U, V> convertedValues;

            if (this->values(identifier, label1, validValues1, label2, validValues2, label3, validValues3, convertedValues))
                _identifierToTupleOfValues.insert(identifier, convertedValues);

        }

        return _identifierToTupleOfValues;

    }

    template <typename T, typename U, typename V, typename W> QHash<QString, std::tuple<T, U, V, W> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3, const QString &label4, const QSet<W> &validValues4) const {

        QHash<QString, std::tuple<T, U, V, W> > _identifierToTupleOfValues;

        for (const QString &identifier : inputIdentifiers) {

            std::tuple<T, U, V, W> convertedValues;

            if (this->values(identifier, label1, validValues1, label2, validValues2, label3, validValues3, label4, validValues4, convertedValues))
                _identifierToTupleOfValues.insert(identifier, convertedValues);

        }

        return _identifierToTupleOfValues;

    }

    template <typename T, typename U, typename V, typename W, typename X> QHash<QString, std::tuple<T, U, V, W, X> > identifierToTupleOfValues(const QSet<QString> &inputIdentifiers, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3, const QString &label4, const QSet<W> &validValues4, const QString &label5, const QSet<X> &validValues5) const {

        QHash<QString, std::tuple<T, U, V, W, X> > _identifierToTupleOfValues;

        for (const QString &identifier : inputIdentifiers) {

            std::tuple<T, U, V, W, X> convertedValues;

            if (this->values(identifier, label1, validValues1, label2, validValues2, label3, validValues3, label4, validValues4, label5, validValues5, convertedValues))
                _identifierToTupleOfValues.insert(identifier, convertedValues);

        }

        return _identifierToTupleOfValues;

    }

    bool insertLabel(int index, const QString &label);

    bool insertLabels(int index, const QStringList &labels);

    const QString &label(int index) const;

    const QStringList &labels() const;

    bool prependLabel(const QString &label);

    void removeIdentifier(const QString &identifier);

    void removeLabel(const QString &label);

    void removeLabels(const QStringList &labels);

    void removeLabels(QList<int> indexes);

    void removeValue(const QString &identifier, const QString &label);

    bool renameIdentifier(const QString &oldIdentifier, const QString &newIdentifier);

    bool setLabel(int index, const QString &label);

    template <typename T> void setValue(const QString &identifier, const QString &label, const T &value) {

        this->appendLabel(label);

        d_data->d_values[label][identifier] = value;

    }

    void swapLabels(int index1, int index2);

    template <typename T> QSet<T> uniqueValues(const QString &label) const {

        return this->uniqueValues<T>(d_data->d_identifiers, label);

    }

    template <typename T> QSet<T> uniqueValues(const QSet<QString> &inputIdentifiers, const QString &label) const {

        QSet<T> _uniqueValues;

        QHash<QString, QVariant> identifierToValue = d_data->d_values.value(label);

        T convertedValue;

        for (const QString &inputIdentifier: inputIdentifiers) {

            if (ConvertFunctions::canConvert<T>(identifierToValue.value(inputIdentifier), convertedValue))
                _uniqueValues.insert(convertedValue);

        }

        return _uniqueValues;

    }

    const QVariant value(const QString &identifier, const QString &label) const;

    template <typename T> bool value(const QString &identifier, const QString &label, T &_value) const {

        if (label.isEmpty()) {

            _value = T();

            return true;

        }

        return ConvertFunctions::canConvert(d_data->d_values.value(label).value(identifier), _value);

    }

    QList<QVariant> values(const QString &identifier, const QStringList &labels) const;

    template <typename T> bool values(const QString &identifier, const QStringList &labels, QList<T> &_values) const {

        _values.reserve(labels.size());

        bool ok = true;

        T convertedValue;

        for (const QString &label : labels) {

            if (label.isEmpty()) {
                _values << T();
            } else if (!this->value(identifier, label, convertedValue)) {

                ok = false;

                _values << convertedValue;

            }

        }

        return ok;

    }

    template <typename T, typename U> bool values(const QString &identifier, const QString &label1, const QString &label2, std::tuple<T, U> &_values) const {

        return this->values(identifier, label1, QSet<T>(), label2, QSet<U>(), _values);

    }

    template <typename T, typename U, typename V> bool values(const QString &identifier, const QString &label1, const QString &label2, const QString &label3, std::tuple<T, U, V> &_values) const {

        return this->values(identifier, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>(), _values);

    }

    template <typename T, typename U, typename V, typename W> bool values(const QString &identifier, const QString &label1, const QString &label2, const QString &label3, const QString &label4, std::tuple<T, U, V, W> &_values) const {

        return this->values(identifier, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>(), label4, QSet<W>(), _values);

    }

    template <typename T, typename U, typename V, typename W, typename X> bool values(const QString &identifier, const QString &label1, const QString &label2, const QString &label3, const QString &label4, const QString &label5, std::tuple<T, U, V, W, X> &_values) const {

        return this->values(identifier, label1, QSet<T>(), label2, QSet<U>(), label3, QSet<V>(), label4, QSet<W>(), label5, QSet<X>(), _values);

    }

    template <typename T, typename U> bool values(const QString &identifier, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, std::tuple<T, U> &_values) const {

        bool ok = true;

        T convertedValue1;

        U convertedValue2;

        if (!this->value(identifier, label1, convertedValue1))
            ok = false;

        if (!this->value(identifier, label2, convertedValue2))
            ok = false;

        if ((!label1.isEmpty()) && (!validValues1.isEmpty()) && (!validValues1.contains(convertedValue1)))
            ok = false;

        if ((!label2.isEmpty()) && (!validValues2.isEmpty()) && (!validValues2.contains(convertedValue2)))
            ok = false;

        std::get<0>(_values) = convertedValue1;

        std::get<1>(_values) = convertedValue2;

        return ok;

    }

    template <typename T, typename U, typename V> bool values(const QString &identifier, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3, std::tuple<T, U, V> &_values) const {

        bool ok = true;

        T convertedValue1;

        U convertedValue2;

        V convertedValue3;

        if (!this->value(identifier, label1, convertedValue1))
            ok = false;

        if (!this->value(identifier, label2, convertedValue2))
            ok = false;

        if (!this->value(identifier, label3, convertedValue3))
            ok = false;

        if ((!label1.isEmpty()) && (!validValues1.isEmpty()) && (!validValues1.contains(convertedValue1)))
            ok = false;

        if ((!label2.isEmpty()) && (!validValues2.isEmpty()) && (!validValues2.contains(convertedValue2)))
            ok = false;

        if ((!label3.isEmpty()) && (!validValues3.isEmpty()) && (!validValues3.contains(convertedValue3)))
            ok = false;

        std::get<0>(_values) = convertedValue1;

        std::get<1>(_values) = convertedValue2;

        std::get<2>(_values) = convertedValue3;

        return ok;

    }

    template <typename T, typename U, typename V, typename W> bool values(const QString &identifier, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3, const QString &label4, const QSet<W> &validValues4, std::tuple<T, U, V, W> &_values) const {

        bool ok = true;

        T convertedValue1;

        U convertedValue2;

        V convertedValue3;

        W convertedValue4;

        if (!this->value(identifier, label1, convertedValue1))
            ok = false;

        if (!this->value(identifier, label2, convertedValue2))
            ok = false;

        if (!this->value(identifier, label3, convertedValue3))
            ok = false;

        if (!this->value(identifier, label4, convertedValue4))
            ok = false;

        if ((!label1.isEmpty()) && (!validValues1.isEmpty()) && (!validValues1.contains(convertedValue1)))
            ok = false;

        if ((!label2.isEmpty()) && (!validValues2.isEmpty()) && (!validValues2.contains(convertedValue2)))
            ok = false;

        if ((!label3.isEmpty()) && (!validValues3.isEmpty()) && (!validValues3.contains(convertedValue3)))
            ok = false;

        if ((!label4.isEmpty()) && (!validValues4.isEmpty()) && (!validValues4.contains(convertedValue4)))
            ok = false;

        std::get<0>(_values) = convertedValue1;

        std::get<1>(_values) = convertedValue2;

        std::get<2>(_values) = convertedValue3;

        std::get<3>(_values) = convertedValue4;

        return ok;

    }

    template <typename T, typename U, typename V, typename W, typename X> bool values(const QString &identifier, const QString &label1, const QSet<T> &validValues1, const QString &label2, const QSet<U> &validValues2, const QString &label3, const QSet<V> &validValues3, const QString &label4, const QSet<W> &validValues4, const QString &label5, const QSet<X> &validValues5, std::tuple<T, U, V, W, X> &_values) const {

        bool ok = true;

        T convertedValue1;

        U convertedValue2;

        V convertedValue3;

        W convertedValue4;

        X convertedValue5;

        if (!this->value(identifier, label1, convertedValue1))
            ok = false;

        if (!this->value(identifier, label2, convertedValue2))
            ok = false;

        if (!this->value(identifier, label3, convertedValue3))
            ok = false;

        if (!this->value(identifier, label4, convertedValue4))
            ok = false;

        if (!this->value(identifier, label5, convertedValue5))
            ok = false;

        if ((!label1.isEmpty()) && (!validValues1.isEmpty()) && (!validValues1.contains(convertedValue1)))
            ok = false;

        if ((!label2.isEmpty()) && (!validValues2.isEmpty()) && (!validValues2.contains(convertedValue2)))
            ok = false;

        if ((!label3.isEmpty()) && (!validValues3.isEmpty()) && (!validValues3.contains(convertedValue3)))
            ok = false;

        if ((!label4.isEmpty()) && (!validValues4.isEmpty()) && (!validValues4.contains(convertedValue4)))
            ok = false;

        if ((!label5.isEmpty()) && (!validValues5.isEmpty()) && (!validValues5.contains(convertedValue5)))
            ok = false;

        std::get<0>(_values) = convertedValue1;

        std::get<1>(_values) = convertedValue2;

        std::get<2>(_values) = convertedValue3;

        std::get<3>(_values) = convertedValue4;

        std::get<4>(_values) = convertedValue5;

        return ok;

    }

    QList<QVariant> values(const QStringList &identifiers, const QString &label) const;

    template <typename T> bool values(const QStringList &identifiers, const QString &label, QList<T> &_values) const {

        _values.reserve(identifiers.size());

        if (label.isEmpty()) {

            for (int i = 0; i < identifiers.size(); ++i)
                _values << T();

            return true;

        }

        bool ok = true;

        QHash<QString, QVariant> identifierToValueForLabel = d_data->d_values.value(label);

        T convertedValue;

        for (const QString &identifier : identifiers) {

            if (!ConvertFunctions::canConvert(identifierToValueForLabel.value(identifier), convertedValue))
                ok = false;

            _values << convertedValue;

        }

        return ok;

    }

    QList<QString> values_string(const QStringList &identifiers, const QString &label) const;

    template <typename T> QHash<T, QSet<QString> > valueToIdentifiers(const QString &label) const {

        return this->valueToIdentifiers<T>(d_data->d_identifiers, label);

    }

    template <typename T> QHash<T, QSet<QString> > valueToIdentifiers(const QString &label, const T &validValue) const {

        return this->valueToIdentifiers<T>(d_data->d_identifiers, label, validValue);

    }

    template <typename T> QHash<T, QSet<QString> > valueToIdentifiers(const QString &label, const QSet<T> &validValues = QSet<T>()) const {

        return this->valueToIdentifiers<T>(d_data->d_identifiers, label, validValues);

    }

    template <typename T> QHash<T, QSet<QString> > valueToIdentifiers(const QSet<QString> &inputIdentifiers, const QString &label) const {

        QHash<T, QSet<QString> > _valueToIdentifiers;

        if (label.isEmpty()) {

            _valueToIdentifiers.insert(T(), inputIdentifiers);

            return _valueToIdentifiers;

        }

        QSet<T> uniqueValues = this->uniqueValues<T>(label);

        for (const T &uniqueValue: uniqueValues)
            _valueToIdentifiers.insert(uniqueValue, this->identifiers(inputIdentifiers, label, uniqueValue));

        return _valueToIdentifiers;

    }

    template <typename T> QHash<T, QSet<QString> > valueToIdentifiers(const QSet<QString> &inputIdentifiers, const QString &label, const T &validValue) const {

        if (label.isEmpty())
            return this->valueToIdentifiers<T>(inputIdentifiers, label);

        QHash<T, QSet<QString> > _valueToIdentifiers;

        QSet<QString> identifiers = this->identifiers(inputIdentifiers, label, validValue);

        if (!identifiers.isEmpty())
            _valueToIdentifiers.insert(validValue, identifiers);

        return _valueToIdentifiers;

    }

    template <typename T> QHash<T, QSet<QString> > valueToIdentifiers(const QSet<QString> &inputIdentifiers, const QString &label, const QSet<T> &validValues = QSet<T>()) const {

        if (label.isEmpty())
            return this->valueToIdentifiers<T>(inputIdentifiers, label);

        if (validValues.isEmpty())
            return this->valueToIdentifiers<T>(inputIdentifiers, label);

        QHash<T, QSet<QString> > _valueToIdentifiers;

        for (const T &validValue: validValues)
            _valueToIdentifiers.unite(this->valueToIdentifiers(inputIdentifiers, label, validValue));

        return _valueToIdentifiers;

    }

    Annotations &operator=(const Annotations &other);

    friend QDataStream& operator<<(QDataStream& out, const Annotations &annotations);

    friend QDataStream& operator>>(QDataStream& in, Annotations &annotations);

private:

    QSharedDataPointer<AnnotationsData> d_data;

};

#endif // ANNOTATIONS_H
