#ifndef SETCHARTPARAMETERSTREEWIDGETITEM_H
#define SETCHARTPARAMETERSTREEWIDGETITEM_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QChart>
#include <QPolarChart>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QPen>
#include <QBrush>
#include <QComboBox>

#include "charts/setbrushparameterstreewidgetitem.h"
#include "charts/setpenparameterstreewidgetitem.h"
#include "charts/setmarginsparameterstreewidgetitem.h"
#include "charts/selectfontwidget.h"
#include "charts/setaxisparameterstreewidgetitem.h"
#include "charts/setseriesparameterstreewidgetitem.h"
#include "charts/setlegendparameterstreewidgetitem.h"

using namespace QtCharts;

class SetChartParametersTreeWidgetItem : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetChartParametersTreeWidgetItem(QTreeWidget *parent = nullptr, const QString &label = "Chart", QChart *chart = nullptr);

    explicit SetChartParametersTreeWidgetItem(QTreeWidgetItem *parent = nullptr, const QString &label = "Chart", QChart *chart = nullptr);

    ~SetChartParametersTreeWidgetItem();

public slots:

    void setChart(const QString &label, QChart *chart);

private:

    void initialize(const QString &label);

    QChart *d_chart;

    bool d_initialized;

    SetBrushParametersTreeWidgetItem *d_setBackgroundBrushParametersTreeWidgetItem;

    SetPenParametersTreeWidgetItem *d_setBackgroundPenParametersTreeWidgetItem;

    QDoubleSpinBox *d_selectBackgroundRoundnessWidget;

    QCheckBox *d_selectBackgroundVisibleWidget;

    QCheckBox *d_selectDropShadowEnabledWidget;

    SetLegendParametersTreeWidgetItem *d_setLegendParametersTreeWidgetItem;

    SetMarginsParametersTreeWidgetItem *d_setMarginsParametersTreeWidgetItem;

    SetBrushParametersTreeWidgetItem *d_setPlotAreaBackgroundBrushParametersTreeWidgetItem;

    SetPenParametersTreeWidgetItem *d_setPlotAreaBackgroundPenParametersTreeWidgetItem;

    QCheckBox *d_selectPlotAreaBackgroundVisibleWidget;

    QLineEdit *d_selectTitleWidget;

    SetBrushParametersTreeWidgetItem *d_setTitleBackgroundBrushParametersTreeWidgetItem;

    SelectFontWidget *d_selectTitleFontWidget;    

private slots:

    void setBackgroundBrush(const QBrush &brush);

    void setBackgroundPen(const QPen &pen);

    void setBackgroundRoundness(qreal diameter);

    void setBackgroundVisible(bool visible);

    void setDropShadowEnabled(bool enabled);

    void setMargins(const QMargins &margins);

    void setPlotAreaBackgroundBrush(const QBrush &brush);

    void setPlotAreaBackgroundPen(const QPen &pen);

    void setPlotAreaBackgroundVisible(bool visible);

    void setTitle(const QString &title);

    void setTitleBrush(const QBrush &brush);

    void setTitleFont(const QFont &font);

};

#endif // SETCHARTPARAMETERSTREEWIDGETITEM_H
