#include "genesetenrichmentanalysisdialog.h"

GeneSetEnrichmentAnalysisDialog::GeneSetEnrichmentAnalysisDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new GeneSetEnrichmentAnalysisWorkerClass::Parameters;

    this->setupWidgets();

}

GeneSetEnrichmentAnalysisDialog::~GeneSetEnrichmentAnalysisDialog()
{

   delete static_cast<GeneSetEnrichmentAnalysisWorkerClass::Parameters *>(d_parameters);

}

void GeneSetEnrichmentAnalysisDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select ranked gene lists");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select probes/genes");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 1, 1);

    d_selectMultipleFilesBuildingBlockWidget_geneSetDatabases = new SelectMultipleFilesBuildingBlockWidget(this, "selectMultipleFilesBuildingBlockWidget_geneSetDatabases", "Select files containing gene set definitions", QFileDialog::AcceptOpen, true);

    d_gridLayout->addWidget(d_selectMultipleFilesBuildingBlockWidget_geneSetDatabases, 3, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelUsedToMatch = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelUsedToMatch", "Select annotation label to match members of gene sets with genes in the ranked lists");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelUsedToMatch, 4, 0);

    d_comboBoxBuildingBlockWidget_testToDefineEnrichment = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_testToDefineEnrichment", "Select test to define enrichment", {"Student T", "Welch T", "Mann-Whitney U", "Kolmogorov-Smirnov Z", "Fisher exact"});

    QCompleter *completer1 = new QCompleter({"Student T", "Welch T", "Mann-Whitney U", "Kolmogorov-Smirnov Z", "Fisher exact"}, this);

    completer1->setCaseSensitivity(Qt::CaseInsensitive);

    completer1->setFilterMode(Qt::MatchContains);

    d_comboBoxBuildingBlockWidget_testToDefineEnrichment->pointerToInternalComboBox()->setEditable(true);

    d_comboBoxBuildingBlockWidget_testToDefineEnrichment->pointerToInternalComboBox()->setCompleter(completer1);

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_testToDefineEnrichment, 5, 0);

    d_spinBoxBuildingBlockWidget_minimumSizeOfGeneSet = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_minimumSizeOfGeneSet", "Select minimum number of genes in a gene set", true, 10, 2, 100000, 10, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_minimumSizeOfGeneSet, 6, 0);

    d_spinBoxBuildingBlockWidget_maximumSizeOfGeneSet = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_maximumSizeOfGeneSet", "Select maximum number of genes in a gene set", true, 10, 2, 100000, 500, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_maximumSizeOfGeneSet, 7, 0);

    d_comboBoxBuildingBlockWidget_performPermutations = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_performPermutations", "Perform permutations", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_performPermutations, 8, 0);

    d_spinBoxBuildingBlockWidget_numberOfPermutations = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_numberOfPermutations", "Number of permutation rounds", true, 10, 1, 100000000, 1000, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_numberOfPermutations, 8, 1);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningSearchableKeys = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_annotationLabelsDefiningSearchableKeys", "Select annotation labels to define searchable keywords");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningSearchableKeys, 9, 0);

    d_comboBoxBuildingBlockWidget_outputFormat = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_outputFormat", "Select output format", {"Tab-delimited", "JSON"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_outputFormat, 10, 0);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 11, 0);

    d_comboBoxBuildingBlockWidget_exportGeneSetMemberFiles = new ComboBoxBuildingBlockWidget(this, "d_comboBoxBuildingBlockWidget_exportGeneSetMemberFiles", "Export geneset member files", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_exportGeneSetMemberFiles, 12, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void GeneSetEnrichmentAnalysisDialog::initializeParameters()
{

    GeneSetEnrichmentAnalysisWorkerClass::Parameters *parameters = static_cast<GeneSetEnrichmentAnalysisWorkerClass::Parameters *>(d_parameters);

    parameters->annotationLabelUsedToMatch = d_comboBoxBuildingBlockWidget_annotationLabelUsedToMatch->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelsDefiningSearchableKeys = d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningSearchableKeys->selectedItemsIdentifierAndNumber().first;

    parameters->baseDataset = d_baseDataset;

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->geneSetFiles = d_selectMultipleFilesBuildingBlockWidget_geneSetDatabases->pointerToInternalPlainTextEdit()->toPlainText().split(QRegExp("\\n"), Qt::SkipEmptyParts);

    parameters->maximumSizeOfGeneSet = d_spinBoxBuildingBlockWidget_maximumSizeOfGeneSet->pointerToInternalSpinBox()->value();

    parameters->minimumSizeOfGeneSet = d_spinBoxBuildingBlockWidget_minimumSizeOfGeneSet->pointerToInternalSpinBox()->value();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->outputFormat = d_comboBoxBuildingBlockWidget_outputFormat->pointerToInternalComboBox()->currentText();

    parameters->performPermutations = (d_comboBoxBuildingBlockWidget_performPermutations->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->numberOfPermutationRounds = d_spinBoxBuildingBlockWidget_numberOfPermutations->pointerToInternalSpinBox()->value();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->testToDefineEnrichment = d_comboBoxBuildingBlockWidget_testToDefineEnrichment->pointerToInternalComboBox()->currentText();

    parameters->exportGeneSetMemberFiles = (d_comboBoxBuildingBlockWidget_exportGeneSetMemberFiles->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

}

void GeneSetEnrichmentAnalysisDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningSearchableKeys->setItemsIdentifier(d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelUsedToMatch->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelUsedToMatch->pointerToInternalComboBox()->addItems(QStringList() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

}
