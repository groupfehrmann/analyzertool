#include "deduplicateidentifiersdialog.h"

DeduplicateIdentifiersDialog::DeduplicateIdentifiersDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new DeduplicateIdentifiersWorkerClass::Parameters;

    this->setupWidgets();

}

DeduplicateIdentifiersDialog::~DeduplicateIdentifiersDialog()
{

   delete static_cast<DeduplicateIdentifiersWorkerClass::Parameters *>(d_parameters);

}

void DeduplicateIdentifiersDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_identifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_identifiers", "Select identifiers");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_identifiers, 1, 0);

    d_comboBoxBuildingBlockWidget_mode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_mode", "Select mode", {"Keep random duplicate identifier", "Keep first of duplicate identifiers", "Keep last of duplicate identifiers"}, {DeduplicateIdentifiersWorkerClass::KEEPRANDOM, DeduplicateIdentifiersWorkerClass::KEEPFIRST, DeduplicateIdentifiersWorkerClass::KEEPLAST});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_mode, 2, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void DeduplicateIdentifiersDialog::initializeParameters()
{

    DeduplicateIdentifiersWorkerClass::Parameters *parameters = static_cast<DeduplicateIdentifiersWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->mode = static_cast<DeduplicateIdentifiersWorkerClass::Mode>(d_comboBoxBuildingBlockWidget_mode->pointerToInternalComboBox()->currentData().toInt());

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->selectedIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_identifiers->selectedItemsIdentifierAndNumber();


}

void DeduplicateIdentifiersDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > identifiersAndIndexes = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_identifiers->setItemsIdentifierAndNumber(identifiersAndIndexes.first, identifiersAndIndexes.second);

}
