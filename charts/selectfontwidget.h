#ifndef SELECTFONTWIDGET_H
#define SELECTFONTWIDGET_H

#include <QWidget>
#include <QString>
#include <QFont>
#include <QFontComboBox>

namespace Ui {

class SelectFontWidget;

}

class SelectFontWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectFontWidget(QWidget *parent = 0, const QFont &defaultFont = QFont());

    ~SelectFontWidget();

    QFont currentFont() const;

public slots:

    void setCurrentFont(const QFont &font);

private:

    Ui::SelectFontWidget *ui;

    QFont d_font;

private slots:

    void toolButton_bold_toggeled(bool value);

    void toolButton_italic_toggeled(bool value);

    void setFontPointSize(int value);

    void setFontFamily(const QString &family);

signals:

    void currentFontChanged(const QFont &font);

};

#endif // SELECTFONTWIDGET_H
