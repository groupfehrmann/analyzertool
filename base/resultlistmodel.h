#ifndef RESULTLISTMODEL_H
#define RESULTLISTMODEL_H

#include <QAbstractItemModel>
#include <QSharedPointer>

#include "base/resultlist.h"
#include "base/baseresultitem.h"

class ResultListModelItem
{

public:

    ResultListModelItem(ResultListModelItem *parentItem = nullptr, QObject *internalObject = nullptr);

    ~ResultListModelItem();

    void appendChild(ResultListModelItem *child);

    ResultListModelItem *child(int row);

    int childCount() const;

    int columnCount() const;

    QVariant data(int column) const;

    void insertChild(int row, ResultListModelItem *child);

    QObject *internalObject();

    const QObject *internalObject() const;

    ResultListModelItem *parentItem();

    void removeChild(int row);

    void removeChildren();

    int row() const;

private:

    QList<ResultListModelItem *> d_childItems;

    QObject *d_internalObject;

    ResultListModelItem *d_parentItem;

};

class ResultListModel : public QAbstractItemModel
{

    Q_OBJECT

public:

    ResultListModel(QObject *parent = nullptr, ResultList *resultList = nullptr);

    ~ResultListModel();

    int columnCount(QModelIndex const &parent = QModelIndex()) const;

    QVariant data(QModelIndex const &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(QModelIndex const &index) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    QModelIndex index(int row, int column, const QModelIndex &parent) const;

    QModelIndex parent(const QModelIndex &index) const;

    const ResultList *resultList() const;

    void removeAll();

    bool removeRows(int row, int count, const QModelIndex &parent);

    int rowCount(QModelIndex const &parent = QModelIndex()) const;

    bool setData(QModelIndex const &index, QVariant const &value, int role = Qt::EditRole);

private:

    ResultList *d_resultList;

    ResultListModelItem *d_rootItem;

public slots:

    bool appendResult(Result *result, const QModelIndex &parent);

    bool appendResultItem(BaseResultItem *baseResultItem, const QModelIndex &parent);

signals:

    void resultItemAdded(const QSharedPointer<BaseResultItem> &baseResultItem);

    void resultItemNameChanged(const QUuid &uuid, const QString &name);

    void resultItemRemoved(const QUuid &uuid);

    void resultRemoved(const QUuid &uuid);

    void resultNameChanged(const QUuid &uuid, const QString &name);

    void resultAdded(const QSharedPointer<Result> &result);

};

#endif // RESULTLISTMODEL_H
