#ifndef SETBOXSETPARAMETERSWIDGET_H
#define SETBOXSETPARAMETERSWIDGET_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QBoxSet>
#include <QLineEdit>

#include "charts/setbrushparameterstreewidgetitem.h"
#include "charts/setpenparameterstreewidgetitem.h"
#include "charts/selectfontwidget.h"
#include "charts/selectcolorwidget.h"

using namespace QtCharts;

class SetBoxSetParametersWidget : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetBoxSetParametersWidget(QTreeWidget *parent = nullptr, const QString &label = "Box set", QBoxSet *boxSet = nullptr);

    explicit SetBoxSetParametersWidget(QTreeWidgetItem *parent = nullptr, const QString &label = "Box set", QBoxSet *boxSet = nullptr);

    ~SetBoxSetParametersWidget();

public slots:

    void setBoxSet(const QString &label, QBoxSet *boxSet);

private:

    void initialize(const QString &label);

    QBoxSet *d_boxSet;

    bool d_initialized;

    SetBrushParametersTreeWidgetItem *d_setBrushParametersTreeWidgetItem;

    QLineEdit *d_setLabelWidget;

    SetPenParametersTreeWidgetItem *d_setPenParametersTreeWidgetItem;

private slots:

    void setBrush(const QBrush &brush);

    void setLabel(const QString label);

    void setPen(const QPen &pen);

};

#endif // SETBOXSETPARAMETERSWIDGET_H
