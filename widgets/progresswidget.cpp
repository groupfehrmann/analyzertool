#include "progresswidget.h"
#include "ui_progresswidget.h"

QString ProgressWidget::secondsToTimeString(qint64 s)
{
    qint64 days = s / 86400;

    qint64 seconds = s - days * 86400;

    qint64 hours = seconds / 3600;

    seconds -= hours * 3600;

    qint64 minutes = seconds / 60;

    seconds -= minutes * 60;

    QString str;

    if (days)
        str += QString("%1d ").arg(days, 1);

    if (hours >= 1)
        str += QString("%1h ").arg(hours, 2);

    if (minutes >= 1)
        str += QString("%1m ").arg(minutes, 2);

    if (seconds >= 1)
        str += QString("%1s ").arg(seconds, 2);

    if (str.isEmpty())
        str = QString();

    return str;

}

ProgressWidget::ProgressWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProgressWidget)
{

    ui->setupUi(this);

    ui->spinBox_maxThreadCount->setMaximum(QThread::idealThreadCount());

    ui->spinBox_maxThreadCount->setValue(QThread::idealThreadCount());

    QSettings settings;

    settings.beginGroup("progressWidget");

    if (settings.contains("splitter_left/state"))
        ui->splitter_left->restoreState(settings.value("splitter_left/state").toByteArray());

    if (settings.contains("splitter_right/state"))
        ui->splitter_right->restoreState(settings.value("splitter_right/state").toByteArray());

    if (settings.contains("spinBox_maxThreadCount/value")) {

        ui->spinBox_maxThreadCount->setValue(settings.value("spinBox_maxThreadCount/value").toInt());

        QThreadPool::globalInstance()->setMaxThreadCount(settings.value("spinBox_maxThreadCount/value").toInt());

    }

    settings.endGroup();

    for (int i = 0; i < QThread::idealThreadCount() + 4; ++i) {

        QLabel *labelProgressTitle = new QLabel;

        d_indexToProgressTitle << labelProgressTitle;

        ui->verticalLayout_1->addWidget(labelProgressTitle);

        QProgressBar *progressBar = new QProgressBar;

        d_indexToProgressBar << progressBar;

        ui->verticalLayout_2->addWidget(progressBar);

        QLabel *labelEta = new QLabel;

        d_indexToEtaLabel << labelEta;

        ui->verticalLayout_3->addWidget(labelEta);

        d_indexToStartTime << QDateTime();

        d_indexToTimeOfUpdateETA << QDateTime();

        d_indexToETAInSeconds << std::numeric_limits<int>::quiet_NaN();

        d_indexToMinimumAndMaximumValue << QPair<double, double>(0, 100);

        d_indexToCurrentValue << 0;

        this->setVisibility(i, false);

    }

    this->connect(ui->pushButton, SIGNAL(clicked(bool)), this, SIGNAL(cancelWorkerThread()));

    this->connect(ui->pushButton, &QPushButton::clicked, [=](){ ui->lineEdit_title->setText("process will be stopped");});

    this->connect(ui->spinBox_maxThreadCount, SIGNAL(valueChanged(int)), this, SLOT(maximumThreadCountChanged(int)));

}

ProgressWidget::~ProgressWidget()
{

    QSettings settings;

    settings.beginGroup("progressWidget");

    settings.setValue("splitter_left/state", ui->splitter_left->saveState());

    settings.setValue("splitter_right/state", ui->splitter_right->saveState());

    settings.setValue("spinBox_maxThreadCount/value", ui->spinBox_maxThreadCount->value());

    settings.endGroup();

    delete ui;

}

void ProgressWidget::setVisibility(int index, bool visible)
{

    d_indexToProgressTitle.at(index)->setVisible(visible);

    d_indexToProgressBar.at(index)->setVisible(visible);

    d_indexToEtaLabel.at(index)->setVisible(visible);

}

void ProgressWidget::startProcess(const QString &processTitle)
{

    emit appendLogItem(LogListModel::NODATE, "");

    emit appendLogItem(LogListModel::PROCESSSTARTORSTOP, "starting process \"" + processTitle + "\"");

    ui->lineEdit_title->setText(processTitle);

    ui->pushButton->setEnabled(true);

    d_startTimeOfProcess = QDateTime::currentDateTime();

    d_timerIdentifier = this->startTimer(1000);

    ui->spinBox_maxThreadCount->setEnabled(false);

}

void ProgressWidget::startProgress(int index, const QString &progressTitle, const double &minimumValue, const double &maximumValue)
{

    d_indexToCurrentValue[index] = minimumValue;

    d_indexToProgressTitle.at(index)->setText(progressTitle);

    d_indexToStartTime[index] = QDateTime::currentDateTime();

    d_indexToEtaLabel.at(index)->setText(QString());

    d_indexToTimeOfUpdateETA[index] = QDateTime();

    d_indexToETAInSeconds[index] = std::numeric_limits<int>::quiet_NaN();

    QPair<double, double> &minimumAndMaximumValue(d_indexToMinimumAndMaximumValue[index]);

    minimumAndMaximumValue.first = minimumValue;

    minimumAndMaximumValue.second = maximumValue;

    if (std::fabs(minimumValue - maximumValue) <= std::numeric_limits<double>::epsilon())
        d_indexToProgressBar.at(index)->setRange(0, 0);
    else
        d_indexToProgressBar.at(index)->setRange(0, 100000);

    d_indexToProgressBar.at(index)->setValue(0);

    this->setVisibility(index, true);

}

void ProgressWidget::stopProcess()
{

    emit appendLogItem(LogListModel::PROCESSSTARTORSTOP, "stopping process \"" + ui->lineEdit_title->text() + "\", total processing time " + (ui->lineEdit_time->text().isEmpty() ? "< 1s" : ui->lineEdit_time->text()));

    ui->lineEdit_time->clear();

    ui->lineEdit_title->setText("No process running");

    ui->pushButton->setEnabled(false);

    for (int i = 0; i < QThreadPool::globalInstance()->maxThreadCount() + 4; ++i)
        this->setVisibility(i, false);


    this->killTimer(d_timerIdentifier);

    ui->spinBox_maxThreadCount->setEnabled(true);

}

void ProgressWidget::stopProgress(int index)
{

    this->setVisibility(index, false);

}

void ProgressWidget::updateProgress(int index, const double &value)
{

    d_indexToCurrentValue[index] = value;

}

void ProgressWidget::maximumThreadCountChanged(int count)
{

    QThreadPool::globalInstance()->setMaxThreadCount(count);

}

#include <QTextStream>

void ProgressWidget::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

    ui->lineEdit_time->setText(this->secondsToTimeString(d_startTimeOfProcess.secsTo(QDateTime::currentDateTime())));

    for (int index = 0; index < QThreadPool::globalInstance()->maxThreadCount() + 4; ++index) {

        if (d_indexToProgressTitle.at(index)->isVisible()) {

            QPair<double, double> &minimumAndMaximumValue(d_indexToMinimumAndMaximumValue[index]);

            if (!(std::fabs(minimumAndMaximumValue.first - minimumAndMaximumValue.second) <= std::numeric_limits<double>::epsilon())) {

                int oldValue = d_indexToProgressBar.at(index)->value();

                int newValue = static_cast<int>(((d_indexToCurrentValue.at(index) - minimumAndMaximumValue.first) / (minimumAndMaximumValue.second - minimumAndMaximumValue.first)) * static_cast<double>(100000));

                d_indexToProgressBar.at(index)->setValue(static_cast<int>(newValue));

                qint64 secondsPassed = d_indexToStartTime.at(index).secsTo(QDateTime::currentDateTime());

                if (secondsPassed > 10) {

                    if (oldValue == newValue) {

                        if (newValue > 0)
                            d_indexToEtaLabel.at(index)->setText("ETA : " + this->secondsToTimeString(d_indexToETAInSeconds.at(index) - d_indexToTimeOfUpdateETA.at(index).secsTo(QDateTime::currentDateTime())));
                        else
                            d_indexToEtaLabel.at(index)->setText(QString());

                    } else {

                        d_indexToETAInSeconds[index] = static_cast<qint64>(static_cast<double>(secondsPassed) * ((static_cast<double>(100000) - static_cast<double>(newValue)) / static_cast<double>(newValue)));

                        d_indexToTimeOfUpdateETA[index] = QDateTime::currentDateTime();

                        d_indexToEtaLabel.at(index)->setText("ETA : " + this->secondsToTimeString(d_indexToETAInSeconds.at(index)));

                    }

                } else
                    d_indexToEtaLabel.at(index)->setText(QString());

            }

        }

    }

}
