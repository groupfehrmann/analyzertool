#ifndef COMPARESAMPLESCONTINUOUSDIALOG_H
#define COMPARESAMPLESCONTINUOUSDIALOG_H

#include <QString>
#include <QSharedPointer>
#include <QCompleter>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "widgets/permutationoptionsbuildingblockwidget.h"
#include "workerclasses/comparesamplesworkerclass.h"
#include "statistics/studentt.h"
#include "statistics/welcht.h"
#include "statistics/levenef.h"
#include "statistics/mannwhitneyu.h"
#include "statistics/kolmogorovsmirnovz.h"
#include "statistics/kruskalwallischisquared.h"
#include "statistics/brownforsythef.h"
#include "statistics/bartlettchisquared.h"
#include "statistics/ftestf.h"
#include "statistics/vanderwaerdent.h"

class CompareSamplesContinuousDialog : public BaseDialog
{

    Q_OBJECT

public:

    CompareSamplesContinuousDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~CompareSamplesContinuousDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSamples;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_itemSampleIdentifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_nameOfStatisticalTest;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_nameOfMetaAnalysisApproach;

    PermutationOptionsBuildingBlockWidget *d_permutationOptionsBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void annotationLabelDefiningItemSamplesChanged(const QString &currentAnnotationLabel);

    void annotationLabelDefiningItemSubsetsChanged(const QString &currentAnnotationLabel);

};

#endif // COMPARESAMPLESCONTINUOUSDIALOG_H
