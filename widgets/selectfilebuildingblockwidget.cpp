#include "selectfilebuildingblockwidget.h"
#include "ui_selectfilebuildingblockwidget.h"

SelectFileBuildingBlockWidget::SelectFileBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, QFileDialog::AcceptMode acceptMode, bool rememberSettings) :
    QWidget(parent), ui(new Ui::SelectFileBuildingBlockWidget), d_acceptMode(acceptMode), d_rememberSettings(rememberSettings)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/selectFileBuildingBlockWidget");

        if (settings.contains("currentPath"))
            ui->lineEdit->setText(settings.value("currentPath").toString());

        settings.endGroup();

        this->evaluatePath();

    }

    this->connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(pushButton_clicked()));

    this->connect(ui->lineEdit, SIGNAL(textEdited(QString)), this, SLOT(lineEdit_edited(QString)));

}

SelectFileBuildingBlockWidget::~SelectFileBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/selectFileBuildingBlockWidget");

        settings.setValue("currentPath", ui->lineEdit->text());

        settings.endGroup();

    }

    delete ui;

}

void SelectFileBuildingBlockWidget::evaluatePath()
{

    if (QFile::exists(ui->lineEdit->text())) {

        ui->lineEdit->setStyleSheet(QString());

        emit pathChanged(ui->lineEdit->text());

    } else {

        ui->lineEdit->setStyleSheet(QString("color: rgb(255, 0, 0);"));

        emit invalidPath();

    }

}

void SelectFileBuildingBlockWidget::lineEdit_edited(const QString &text)
{

    Q_UNUSED(text);

    this->evaluatePath();

}

QLabel *SelectFileBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}

QLineEdit *SelectFileBuildingBlockWidget::pointerToInternalLineEdit()
{

    return ui->lineEdit;

}

QPushButton *SelectFileBuildingBlockWidget::pointerToInternalPushButton()
{

    return ui->pushButton;

}

void SelectFileBuildingBlockWidget::pushButton_clicked()
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - Select file");

    fileDialog.setAcceptMode(d_acceptMode);

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    settings.endGroup();

    if (fileDialog.exec()) {

        ui->lineEdit->setText(fileDialog.selectedFiles().first());

        this->evaluatePath();

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/fileDialog");

        settings.setValue("state", fileDialog.saveState());

        settings.setValue("geometry", fileDialog.saveGeometry());

        settings.setValue("lastDirectory", fileDialog.directory().path());

        settings.endGroup();

    } else
        ui->lineEdit->setText(QString());

}

