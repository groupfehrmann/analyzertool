#ifndef LIPTAKSTRENDMETHOD_H
#define LIPTAKSTRENDMETHOD_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include <cmath>
#include <limits>

#include "boost/math/distributions/normal.hpp"

#include "statistics/basemetaanalysistest.h"

class LiptaksTrendMethod : public BaseMetaAnalysisTest
{

public:

    LiptaksTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    ~LiptaksTrendMethod();

    QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const;

    const QStringList metaAnalysisTestDescriptiveLabels() const;

protected:

    void _reset();

    static QStringList d_baseStatisticalTestDescriptiveLabels;

    static QStringList d_metaAnalysisTestDescriptiveLabels;

    static QString d_nameOfMetaAnalysisTest;

    QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues();

    QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues();

    void calculateStatistic();

    void calculatedPValue();

};

#endif // LIPTAKSTRENDMETHOD_H
