#ifndef SCATTERCHART_GENOMICMAPPINGRANKONXAXIS_HOMOSAPIENS_H
#define SCATTERCHART_GENOMICMAPPINGRANKONXAXIS_HOMOSAPIENS_H

#include <QObject>
#include <QChart>
#include <QPolarChart>
#include <QStringList>
#include <QEventLoop>
#include <QScatterSeries>
#include <QCategoryAxis>

#include "charts/exportplottofile.h"
#include "charts/basechart.h"

using namespace QtCharts;

class ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens : public BaseChart
{

    Q_OBJECT

public:

    explicit ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens(QObject *parent = nullptr, const QString &title = QString(), const QVector<int> &chromosomeMappingsForAllSubsets = QVector<int>(), const QStringList &subsetIdentifiers = QStringList(), const QVector<QVector<int> > &ranks_xAxis = QVector<QVector<int> >(), const QVector<QVector<double> > &values_yAxis = QVector<QVector<double> >(), QChart::ChartType chartType = QChart::ChartTypeCartesian);

    explicit ScatterChart_GenomicMappingRankOnXAxis_HomoSapiens(QObject *parent = nullptr, const QString &title = QString(), const QVector<int> &chromosomeMappingsForAllSubsets = QVector<int>(), const QVector<int> &ranks_xAxis = QVector<int>(), const QVector<double> &values_yAxis = QVector<double> (), QChart::ChartType chartType = QChart::ChartTypeCartesian);

private:

    QChart::ChartType d_chartType;

signals:

    void initializeChart(const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis);

private slots:

    void _initializeChart(const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis);

};

#endif // SCATTERCHART_GENOMICMAPPINGRANKONXAXIS_HOMOSAPIENS_H
