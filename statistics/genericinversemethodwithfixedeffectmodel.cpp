#include "genericinversemethodwithfixedeffectmodel.h"

QString GenericInverseMethodWithFixedEffectModel::d_nameOfMetaAnalysisTest = "Generic inverse method with fixed effect model";

GenericInverseMethodWithFixedEffectModel::GenericInverseMethodWithFixedEffectModel(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

GenericInverseMethodWithFixedEffectModel::~GenericInverseMethodWithFixedEffectModel()
{

}

QStringList GenericInverseMethodWithFixedEffectModel::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (int i = 0; i < subsetIdentifiers.size(); ++i) {

        const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest(d_baseStatisticalTests.at(i));

        const QString &subsetIdentifier(subsetIdentifiers.at(i));
        _baseStatisticalTestDescriptiveLabels << baseStatisticalTest->nameOfEffectSize() + " \"" + subsetIdentifier + "\"";

        _baseStatisticalTestDescriptiveLabels << "Standard error of " + baseStatisticalTest->nameOfEffectSize() + " \"" + subsetIdentifier + "\"";

        if (baseStatisticalTest->effectSizeTransformation() != BaseStatisticalTest::NOTRANSFORMATION)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTest->nameOfTransformedEffectSize() + " \"" + subsetIdentifier + "\"";

        _baseStatisticalTestDescriptiveLabels << "Weight \"" + subsetIdentifier + "\"";

        _baseStatisticalTestDescriptiveLabels << "Sample size \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList GenericInverseMethodWithFixedEffectModel::metaAnalysisTestDescriptiveLabels() const
{

    QStringList _metaAnalysisTestDescriptiveLabels;

    const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest(d_baseStatisticalTests.first());


    _metaAnalysisTestDescriptiveLabels << "Heterogeneity statistic (Q)" << "Q Significance (2-tailed)" << "I-squared";

    if (baseStatisticalTest->effectSizeTransformation() != BaseStatisticalTest::NOTRANSFORMATION) {

        _metaAnalysisTestDescriptiveLabels << "Mean " + baseStatisticalTest->nameOfTransformedEffectSize();

        _metaAnalysisTestDescriptiveLabels << "Lower bound 95% confidence interval " + baseStatisticalTest->nameOfTransformedEffectSize();

        _metaAnalysisTestDescriptiveLabels << "Upper bound 95% confidence interval " + baseStatisticalTest->nameOfTransformedEffectSize();

    }

    _metaAnalysisTestDescriptiveLabels << "Mean " + baseStatisticalTest->nameOfEffectSize();

    _metaAnalysisTestDescriptiveLabels << "Lower bound 95% confidence interval " + baseStatisticalTest->nameOfEffectSize();

    _metaAnalysisTestDescriptiveLabels << "Upper bound 95% confidence interval " + baseStatisticalTest->nameOfEffectSize();

    _metaAnalysisTestDescriptiveLabels << "Z" << "Significance (2-tailed)";

    return _metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> GenericInverseMethodWithFixedEffectModel::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        double standardError = baseStatisticalTest->standardErrorOfEffectSize();

        double inverseVarianceWeight = 1.0 / (standardError * standardError);

        baseStatisticalTestDescriptiveValues << baseStatisticalTest->effectSize();

        baseStatisticalTestDescriptiveValues << baseStatisticalTest->standardErrorOfEffectSize();

        if (baseStatisticalTest->effectSizeTransformation() != BaseStatisticalTest::NOTRANSFORMATION)
            baseStatisticalTestDescriptiveValues << baseStatisticalTest->transformedEffectSize();

        baseStatisticalTestDescriptiveValues << inverseVarianceWeight << baseStatisticalTest->sampleSize();

    }

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> GenericInverseMethodWithFixedEffectModel::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    double q = d_sumOfInverseVarianceWeightedSquaredEffectSizes - ((d_sumOfInverseVarianceWeightedEffectSizes * d_sumOfInverseVarianceWeightedEffectSizes) / d_sumOfInverseVarianceWeights);

    double iSquared = 100.0 * ((q - (d_baseStatisticalTests.size() - 1)) / q);

    if (iSquared < 0.0)
        iSquared = 0.0;

    metaAnalysisTestDescriptiveValues << q << boost::math::cdf(boost::math::complement(boost::math::chi_squared(d_baseStatisticalTests.size() - 1), q)) << iSquared;

    double w = boost::math::quantile(boost::math::complement(boost::math::normal(0.0, 1.0), 0.025)) * d_standardErrorEffectSize;

    BaseStatisticalTest::EffectSizeTransformation effectSizeTransformation = d_baseStatisticalTests.first()->effectSizeTransformation();

    if (effectSizeTransformation != BaseStatisticalTest::NOTRANSFORMATION) {

        metaAnalysisTestDescriptiveValues << d_meanEffectSize << d_meanEffectSize - w << d_meanEffectSize + w;

        if (effectSizeTransformation == BaseStatisticalTest::NATURALLOGARITHM)
            metaAnalysisTestDescriptiveValues << std::exp(d_meanEffectSize) << std::exp(d_meanEffectSize - w) << std::exp(d_meanEffectSize + w);
        else if (effectSizeTransformation == BaseStatisticalTest::FISHER_R_TO_Z)
            metaAnalysisTestDescriptiveValues << (std::exp(2.0 * d_meanEffectSize) - 1.0) / (std::exp(2.0 * d_meanEffectSize) + 1.0) << (std::exp(2.0 * (d_meanEffectSize - w)) - 1.0) / (std::exp(2.0 * (d_meanEffectSize - w)) + 1.0) << (std::exp(2.0 * (d_meanEffectSize + w)) - 1.0) / (std::exp(2.0 * (d_meanEffectSize + w)) + 1.0);

    } else
        metaAnalysisTestDescriptiveValues << d_meanEffectSize << d_meanEffectSize - w << d_meanEffectSize + w;

    metaAnalysisTestDescriptiveValues << d_statistic << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void GenericInverseMethodWithFixedEffectModel::calculateStatistic()
{

    d_sumOfInverseVarianceWeights = 0.0;

    d_sumOfInverseVarianceWeightedEffectSizes = 0.0;

    d_sumOfInverseVarianceWeightedSquaredEffectSizes = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        double effectSize = baseStatisticalTest->effectSize();

        double standardError = baseStatisticalTest->standardErrorOfEffectSize();

        double inverseVarianceWeight = 1.0 / (standardError * standardError);

        d_sumOfInverseVarianceWeights += inverseVarianceWeight;

        d_sumOfInverseVarianceWeightedEffectSizes += effectSize * inverseVarianceWeight;

        d_sumOfInverseVarianceWeightedSquaredEffectSizes += inverseVarianceWeight * effectSize * effectSize;

    }

    d_meanEffectSize = d_sumOfInverseVarianceWeightedEffectSizes / d_sumOfInverseVarianceWeights;

    d_standardErrorEffectSize = 1.0 / std::sqrt(d_sumOfInverseVarianceWeights);

    d_statistic = d_meanEffectSize / d_standardErrorEffectSize;

}

void GenericInverseMethodWithFixedEffectModel::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, fabs(d_statistic)));

}

void GenericInverseMethodWithFixedEffectModel::_reset()
{

}
