#include "histogramchart.h"

HistogramChart::HistogramChart(QObject *parent, const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &valueVectors, int numberOfBins, const double &lowerBound, const double &upperBound, bool scaleToFrequencies) :
    BaseChart(parent)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QVector<QVector<double> >,int,double,double,bool)), this, SLOT(_initializeChart(QString,QStringList,QVector<QVector<double> >,int,double,double,bool)));

    emit initializeChart(title, subsetIdentifiers, valueVectors, numberOfBins, lowerBound, upperBound, scaleToFrequencies);

    d_eventLoop.exec();

}



HistogramChart::HistogramChart(QObject *parent, const QString &title, const QVector<double> &valueVector, int numberOfBins, const double &lowerBound, const double &upperBound, bool scaleToFrequencies) :
    BaseChart(parent)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QVector<QVector<double> >,int,double,double,bool)), this, SLOT(_initializeChart(QString,QStringList,QVector<QVector<double> >,int,double,double,bool)));

    emit initializeChart(title, {QString()}, QVector<QVector<double> >() << valueVector, numberOfBins, lowerBound, upperBound, scaleToFrequencies);

    d_eventLoop.exec();

}

void HistogramChart::_initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &valueVectors, int numberOfBins, const double &lowerBound, const double &upperBound, bool scaleToFrequencies)
{

    d_chart = QSharedPointer<QChart>(new QChart);

    QValueAxis *yAxis = new QValueAxis();

    QBarCategoryAxis *xAxis1 = new QBarCategoryAxis();

    QValueAxis *xAxis2 = new QValueAxis();

    xAxis2->setRange(lowerBound, upperBound);

    double maximum = std::numeric_limits<double>::lowest();

    d_chart->addAxis(xAxis2, Qt::AlignBottom);

    xAxis2->setGridLineVisible(false);

    if (valueVectors.size() == 1)
        d_chart->legend()->setVisible(false);

    for (int i = 0; i < subsetIdentifiers.size(); ++i) {

        QBarSet *set = new QBarSet(subsetIdentifiers.at(i));

        QVector<std::tuple<double, double, double> > histogram = scaleToFrequencies ? MathDescriptives::histogram_frequency_lowerBound_upperBound(valueVectors.at(i), lowerBound, upperBound, numberOfBins) : MathDescriptives::histogram_count_lowerBound_upperBound(valueVectors.at(i), lowerBound, upperBound, numberOfBins);

        for (const std::tuple<double, double, double> &bin : histogram) {

            double temp = std::get<0>(bin);

            *set << temp;

            if (temp > maximum)
                maximum = temp;

        }

        QBarSeries *barSeries = new QBarSeries;

        if (!subsetIdentifiers.at(i).isEmpty())
            barSeries->setName(subsetIdentifiers.at(i));

        barSeries->append(set);

        d_chart->addSeries(barSeries);

        d_chart->addAxis(xAxis1, Qt::AlignBottom);

        d_chart->addAxis(yAxis, Qt::AlignLeft);

        if (valueVectors.size() > 1)
            barSeries->setOpacity(0.5);

        barSeries->setBarWidth(1.0);

    }

    if (scaleToFrequencies)
        yAxis->setTitleText("Frequency");
    else
        yAxis->setTitleText("Count");

    yAxis->setRange(0, maximum);

    yAxis->applyNiceNumbers();

    xAxis1->setGridLineVisible(false);

    xAxis1->setVisible(false);

    d_chart->setTitle(title);

    emit finishedInitializationOfChart();

}
