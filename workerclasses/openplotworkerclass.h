#ifndef OPENPLOTWORKERCLASS_H
#define OPENPLOTWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QFile>
#include <QChart>

#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/openplotfromfile.h"

class OpenPlotWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

    };

    OpenPlotWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    OpenPlotWorkerClass::Parameters d_parameters;

    QFile d_fileIn;

};

#endif // OPENPLOTWORKERCLASS_H
