#ifndef CREATESCATTERPLOTSWITHRANKEDGENOMICMAPPINGONXAXISHOMOSAPIENSWORKERCLASS_H
#define CREATESCATTERPLOTSWITHRANKEDGENOMICMAPPINGONXAXISHOMOSAPIENSWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QAbstractSeries>
#include <QDir>

#include <functional>
#include <utility>
#include <algorithm>

#include "base/basedataset.h"
#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/exportplottofile.h"
#include "charts/scatterchart_genomicmappingrankonxaxis_homosapiens.h"
#include "base/convertfunctions.h"
#include "charts/openplotfromfile.h"
#include "charts/copychartparameters.h"

using namespace QtCharts;


class CreateScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString annotationLabelDefiningAlternativeVariableIdentifier;

        QString annotationLabelDefiningBasepairPosition;

        QString annotationLabelDefiningChromosomePosition;

        QString annotationLabelDefiningSubsets;

        QChart::ChartType chartType;

        QSharedPointer<BaseDataset> baseDataset;

        int dpiOfPlot;

        QString exportDirectory;

        QString exportTypeOfPlot;

        int heightOfPlot;

        BaseMatrix::Orientation orientation;

        QString pathToTemplatePlot;

        QString plotMode;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        QStringList subsetIdentifiers;

        int widthOfPlot;

    };

    CreateScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CreateScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensWorkerClass::Parameters d_parameters;

    QSharedPointer<QChart> d_templatePlot;

};

#endif // CREATESCATTERPLOTSWITHRANKEDGENOMICMAPPINGONXAXISHOMOSAPIENSWORKERCLASS_H
