#include "replaceidentifiersdialog.h"

ReplaceIdentifiersDialog::ReplaceIdentifiersDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new ReplaceIdentifiersWorkerClass::Parameters;

    this->setupWidgets();

}

ReplaceIdentifiersDialog::~ReplaceIdentifiersDialog()
{

   delete static_cast<ReplaceIdentifiersWorkerClass::Parameters *>(d_parameters);

}

void ReplaceIdentifiersDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_identifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_identifiers", "Select identifiers");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_identifiers, 1, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningNewIdentifiers = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningNewIdentifiers", "Select annotation label defining new identifiers");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningNewIdentifiers, 2, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void ReplaceIdentifiersDialog::initializeParameters()
{

    ReplaceIdentifiersWorkerClass::Parameters *parameters = static_cast<ReplaceIdentifiersWorkerClass::Parameters *>(d_parameters);

    parameters->annotationLabelDefiningNewIdentifiers = d_comboBoxBuildingBlockWidget_annotationLabelDefiningNewIdentifiers->pointerToInternalComboBox()->currentText();

    parameters->baseDataset = d_baseDataset;

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->selectedIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_identifiers->selectedItemsIdentifierAndNumber();


}

void ReplaceIdentifiersDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > identifiersAndIndexes = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_identifiers->setItemsIdentifierAndNumber(identifiersAndIndexes.first, identifiersAndIndexes.second);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningNewIdentifiers->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningNewIdentifiers->pointerToInternalComboBox()->addItems(d_baseDataset->annotations(orientation).labels());

}
