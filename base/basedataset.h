#ifndef BASEDATASET_H
#define BASEDATASET_H

#include <QObject>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QMap>
#include <QHash>
#include <QHashIterator>
#include <QUuid>

#include <tuple>

#include "base/header.h"
#include "base/basematrix.h"
#include "base/matrix.h"
#include "base/annotations.h"

class Project;

class BaseDatasetData : public QSharedData
{

public:

    BaseDatasetData();

    BaseDatasetData(const BaseDatasetData &other);

    ~BaseDatasetData();

    Header d_verticalHeader;

    Header d_horizontalHeader;

    BaseMatrix *d_baseMatrix;

    Annotations d_columnAnnotations;

    Annotations d_rowAnnotations;

    QString d_name;

    Project *d_parentProject;

};

class BaseDataset : public QObject
{
    Q_OBJECT

public:

    BaseDataset(QObject *parent = nullptr);

    BaseDataset(const BaseDataset &other);

    virtual ~BaseDataset();

    Annotations &annotations(BaseMatrix::Orientation orientation);

    const Annotations &annotations(BaseMatrix::Orientation orientation) const;


    const QVariant annotationValue(int index, const QString &annotationLabel, BaseMatrix::Orientation orientation) const;

    template <typename T> bool annotationValue(int index, const QString &annotationLabel, BaseMatrix::Orientation orientation, T &_annotationValue) const {

        return this->annotations(orientation).value(this->header(orientation).identifier(index), annotationLabel, _annotationValue);

    }

    const QVariant annotationValue(int indexOfIdentifier, int indexOfAnnotationLabel, BaseMatrix::Orientation orientation) const;

    template <typename T> bool annotationValue(int indexOfIdentifier, int indexOfAnnotationLabel, BaseMatrix::Orientation orientation, T &_annotationValue) const {

        return this->annotations(orientation).value(this->header(orientation).identifier(indexOfIdentifier), this->annotations(orientation).label(indexOfAnnotationLabel), _annotationValue);

    }

    QList<QVariant> annotationValues(int index, const QStringList &annotationLabels, BaseMatrix::Orientation orientation) const;

    template <typename T> bool annotationValues(int index, const QStringList &annotationLabels, BaseMatrix::Orientation orientation, QList<T> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), annotationLabels, _annotationValues);

    }

    template <typename T, typename U> bool annotationValues(int index, const QString &label1, const QString &label2, BaseMatrix::Orientation orientation, std::tuple<T, U> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, label2, _annotationValues);

    }

    template <typename T, typename U, typename V> bool annotationValues(int index, const QString &label1, const QString &label2, const QString &label3, BaseMatrix::Orientation orientation, std::tuple<T, U, V> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, label2, label3, _annotationValues);

    }

    template <typename T, typename U, typename V, typename W> bool annotationValues(int index, const QString &label1, const QString &label2, const QString &label3, const QString &label4, BaseMatrix::Orientation orientation, std::tuple<T, U, V, W> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, label2, label3, label4, _annotationValues);

    }

    template <typename T, typename U, typename V, typename W, typename X> bool annotationValues(int index, const QString &label1, const QString &label2, const QString &label3, const QString &label4, const QString &label5, BaseMatrix::Orientation orientation, std::tuple<T, U, V, W, X> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, label2, label3, label4, label5, _annotationValues);

    }

    template <typename T, typename U> bool annotationValues(int index, const QString &label1, const QSet<T> validAnnotationValues1, const QString &label2, const QSet<U> validAnnotationValues2, BaseMatrix::Orientation orientation, std::tuple<T, U> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, validAnnotationValues1, label2, validAnnotationValues2, _annotationValues);

    }

    template <typename T, typename U, typename V> bool annotationValues(int index, const QString &label1, const QSet<T> validAnnotationValues1, const QString &label2, const QSet<U> validAnnotationValues2, const QString &label3, const QSet<V> validAnnotationValues3, BaseMatrix::Orientation orientation, std::tuple<T, U, V> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, validAnnotationValues1, label2, validAnnotationValues2, label3, validAnnotationValues3, _annotationValues);

    }

    template <typename T, typename U, typename V, typename W> bool annotationValues(int index, const QString &label1, const QSet<T> validAnnotationValues1, const QString &label2, const QSet<U> validAnnotationValues2, const QString &label3, const QSet<V> validAnnotationValues3, const QString &label4, const QSet<W> validAnnotationValues4, BaseMatrix::Orientation orientation, std::tuple<T, U, V, W> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, validAnnotationValues1, label2, validAnnotationValues2, label3, validAnnotationValues3, label4, validAnnotationValues4, _annotationValues);

    }

    template <typename T, typename U, typename V, typename W, typename X> bool annotationValues(int index, const QString &label1, const QSet<T> validAnnotationValues1, const QString &label2, const QSet<U> validAnnotationValues2, const QString &label3, const QSet<V> validAnnotationValues3, const QString &label4, const QSet<W> validAnnotationValues4, const QString &label5, const QSet<X> validAnnotationValues5, BaseMatrix::Orientation orientation, std::tuple<T, U, V, W, X> &_annotationValues) const
    {

        return this->annotations(orientation).values(this->header(orientation).identifier(index), label1, validAnnotationValues1, label2, validAnnotationValues2, label3, validAnnotationValues3, label4, validAnnotationValues4, label5, validAnnotationValues5, _annotationValues);

    }

    QList<QVariant> annotationValues(const QList<int> &indexes, const QString &annotationLabel, BaseMatrix::Orientation orientation) const;

    template <typename T> bool annotationValues(const QList<int> &indexes, const QString &annotationLabel, BaseMatrix::Orientation orientation, QList<T> &_annotationValues) const {

        return this->annotations(orientation).values(this->header(orientation).identifiers(indexes), annotationLabel, _annotationValues);

    }

    template <typename T> QHash<T, QList<int> > annotationValueToIndexes(const QString &annotationLabel, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->annotationValueToIndexes<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, orientation);

    }

    template <typename T> QHash<T, QList<int> > annotationValueToIndexes(const QString &annotationLabel, const T &validAnnotationValue, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->annotationValueToIndexes<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, validAnnotationValue, orientation);

    }

    template <typename T> QHash<T, QList<int> > annotationValueToIndexes(const QString &annotationLabel, const QSet<T> &validAnnotationValues, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->annotationValueToIndexes(this->header(orientation).indexes(selectedOnly), annotationLabel, validAnnotationValues, orientation);

    }

    template <typename T> QHash<T, QList<int> > annotationValueToIndexes(const QList<int> &inputIndexes, const QString &annotationLabel, BaseMatrix::Orientation orientation) const {

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QHash<T, QList<int> > _annotationValueToIndexes;

        T convertedAnnotationValue;

        for (const int &inputIndex: inputIndexes) {

            if (annotations.value(header.identifier(inputIndex), annotationLabel, convertedAnnotationValue))
                _annotationValueToIndexes[convertedAnnotationValue] << inputIndex;

        }

        return _annotationValueToIndexes;

    }

    template <typename T> QHash<T, QList<int> > annotationValueToIndexes(const QList<int> &inputIndexes, const QString &annotationLabel, const T &validAnnotationValue, BaseMatrix::Orientation orientation) const {

        if (annotationLabel.isEmpty())
            return this->annotationValueToIndexes<T>(inputIndexes, annotationLabel, orientation);

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QHash<T, QList<int> > _annotationValueToIndexes;

        QVariant _validAnnotationValue = QVariant(validAnnotationValue);

        for (const int &inputIndex: inputIndexes) {

            if (annotations.value(header.identifier(inputIndex), annotationLabel) == _validAnnotationValue)
                _annotationValueToIndexes[validAnnotationValue] << inputIndex;

        }

        return _annotationValueToIndexes;

    }

    template <typename T> QHash<T, QList<int> > annotationValueToIndexes(const QList<int> &inputIndexes, const QString &annotationLabel, const QSet<T> &validAnnotationValues, BaseMatrix::Orientation orientation) const {

        if (annotationLabel.isEmpty())
            return this->annotationValueToIndexes<T>(inputIndexes, annotationLabel, orientation);

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QHash<T, QList<int> > _annotationValueToIndexes;

        T convertedAnnotationValue;

        for (const int &inputIndex: inputIndexes) {

            if (annotations.value(header.identifier(inputIndex), annotationLabel, convertedAnnotationValue)) {

                if (validAnnotationValues.contains(convertedAnnotationValue))
                    _annotationValueToIndexes[convertedAnnotationValue] << inputIndex;

            }

        }

        return _annotationValueToIndexes;

    }

    void appendVector(const QString &identifier, BaseMatrix::Orientation orientation);

    template <typename T>
    bool appendVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage = nullptr);

    void appendVectors(const QStringList &identifiers, BaseMatrix::Orientation orientation);

    BaseMatrix *baseMatrix();

    const BaseMatrix *baseMatrix() const;

    Header &header(BaseMatrix::Orientation orientation);

    const Header &header(BaseMatrix::Orientation orientation) const;

    template <typename T> QList<int> indexes(const QString &annotationLabel, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexes<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, orientation);

    }

    template <typename T> QList<int> indexes(const QString &annotationLabel, const T &validAnnotationValue, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexes<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, validAnnotationValue, orientation);

    }

    template <typename T> QList<int> indexes(const QString &annotationLabel, const QSet<T> &validAnnotationValues, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexes<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, validAnnotationValues, orientation);

    }

    template <typename T> QList<int> indexes(const QList<int> &inputIndexes, const QString &annotationLabel, BaseMatrix::Orientation orientation) const {

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QList<int> _indexes;

        T convertedAnnotationValue;

        for (const int &inputIndex: inputIndexes) {

            if (annotations.value(header.identifier(inputIndex), annotationLabel, convertedAnnotationValue))
                _indexes << inputIndex;

        }

        std::sort(_indexes.begin(), _indexes.end(), std::greater<int>());

        return _indexes;

    }

    template <typename T> QList<int> indexes(const QList<int> &inputIndexes, const QString &annotationLabel, const T &validAnnotationValue, BaseMatrix::Orientation orientation) const {

        if (annotationLabel.isEmpty())
            return this->indexes<T>(inputIndexes, annotationLabel, orientation);

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QList<int> _indexes;

        QVariant _validAnnotationValue = QVariant(validAnnotationValue);

        for (const int &inputIndex: inputIndexes) {

            if (annotations.value(header.identifier(inputIndex), annotationLabel) == _validAnnotationValue)
                _indexes << inputIndex;

        }

        std::sort(_indexes.begin(), _indexes.end(), std::greater<int>());

        return _indexes;

    }

    template <typename T> QList<int> indexes(const QList<int> &inputIndexes, const QString &annotationLabel, const QSet<T> &validAnnotationValues, BaseMatrix::Orientation orientation) const {

        if (annotationLabel.isEmpty())
            return this->indexes<T>(inputIndexes, annotationLabel, orientation);

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QList<int> _indexes;

        T convertedAnnotationValue;

        for (const int &inputIndex: inputIndexes) {

            if (annotations.value(header.identifier(inputIndex), annotationLabel, convertedAnnotationValue)) {

                if (validAnnotationValues.contains(convertedAnnotationValue))
                    _indexes << inputIndex;

            }

        }

        std::sort(_indexes.begin(), _indexes.end(), std::greater<int>());

        return _indexes;

    }

    template <typename T> QMap<int, T> indexToAnnotationValue(const QString &annotationLabel, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValue<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, orientation);

    }

    template <typename T> QMap<int, T> indexToAnnotationValue(const QString &annotationLabel, const T &validAnotationValue, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValue<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, validAnotationValue, orientation);

    }

    template <typename T> QMap<int, T> indexToAnnotationValue(const QString &annotationLabel, const QSet<T> &validAnnotationValues, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValue<T>(this->header(orientation).indexes(selectedOnly), annotationLabel, validAnnotationValues, orientation);

    }

    template <typename T> QMap<int, T> indexToAnnotationValue(const QList<int> &inputIndexes, const QString &annotationLabel, BaseMatrix::Orientation orientation) const {

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QMap<int, T> _indexToAnnotationValue;

        T convertedAnnotationValue;

        for (const int &inputIndex: inputIndexes) {

            const QString &currentIdentifier(header.identifier(inputIndex));

            if (annotations.value(currentIdentifier, annotationLabel, convertedAnnotationValue))
                _indexToAnnotationValue.insert(inputIndex, convertedAnnotationValue);

        }

        return _indexToAnnotationValue;

    }

    template <typename T> QMap<int, T> indexToAnnotationValue(const QList<int> &inputIndexes, const QString &annotationLabel, const T &validAnnotationValue, BaseMatrix::Orientation orientation) const {

        if (annotationLabel.isEmpty())
            return this->indexToAnnotationValue<T>(inputIndexes, annotationLabel, orientation);

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QMap<int, T> _indexToAnnotationValue;

        QVariant _validAnnotationValue = QVariant(validAnnotationValue);

        for (const int &inputIndex: inputIndexes) {

            if (annotations.value(header.identifier(inputIndex), annotationLabel) == _validAnnotationValue)
                _indexToAnnotationValue.insert(inputIndex, validAnnotationValue);


        }

        return _indexToAnnotationValue;

    }

    template <typename T> QMap<int, T> indexToAnnotationValue(const QList<int> &inputIndexes, const QString &annotationLabel, const QSet<T> &validAnnotationValues, BaseMatrix::Orientation orientation) const {

        if (annotationLabel.isEmpty())
            return this->indexToAnnotationValue<T>(inputIndexes, annotationLabel, orientation);

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QMap<int, T> _indexToAnnotationValue;

        T convertedAnnotationValue;

        for (const int &inputIndex: inputIndexes) {

            const QString &currentIdentifier(header.identifier(inputIndex));

            if (annotations.value(currentIdentifier, annotationLabel, convertedAnnotationValue)) {

                if (validAnnotationValues.contains(convertedAnnotationValue))
                    _indexToAnnotationValue.insert(inputIndex, convertedAnnotationValue);

            }

        }

        return _indexToAnnotationValue;

    }

    template <typename T, typename U> QMap<int, std::tuple<T, U> > indexToAnnotationValues(const QString &annotationLabel1, const QString &annotationLabel2, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U>(this->header(orientation).indexes(selectedOnly), annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), orientation);

    }

    template <typename T, typename U, typename V> QMap<int, std::tuple<T, U, V> > indexToAnnotationValues(const QString &annotationLabel1, const QString &annotationLabel2, const QString &annotationLabel3, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U, V>(this->header(orientation).indexes(selectedOnly), annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), annotationLabel3, QSet<V>(), orientation);

    }

    template <typename T, typename U, typename V, typename W> QMap<int, std::tuple<T, U, V, W> > indexToAnnotationValues(const QString &annotationLabel1, const QString &annotationLabel2, const QString &annotationLabel3, const QString &annotationLabel4, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U, V, W>(this->header(orientation).indexes(selectedOnly), annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), annotationLabel3, QSet<V>(), annotationLabel4, QSet<W>(), orientation);

    }

    template <typename T, typename U, typename V, typename W, typename X> QMap<int, std::tuple<T, U, V, W, X> > indexToAnnotationValues(const QString &annotationLabel1, const QString &annotationLabel2, const QString &annotationLabel3, const QString &annotationLabel4, const QString &annotationLabel5, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U, V, W, X>(this->header(orientation).indexes(selectedOnly), annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), annotationLabel3, QSet<V>(), annotationLabel4, QSet<W>(), annotationLabel5, QSet<X>(), orientation);

    }

    template <typename T, typename U> QMap<int, std::tuple<T, U> > indexToAnnotationValues(const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U>(this->header(orientation).indexes(selectedOnly), annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, orientation);

    }

    template <typename T, typename U, typename V> QMap<int, std::tuple<T, U, V> > indexToAnnotationValues(const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, const QString &annotationLabel3, const QSet<V> &validAnnotationValues3, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U, V>(this->header(orientation).indexes(selectedOnly), annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, annotationLabel3, validAnnotationValues3, orientation);

    }

    template <typename T, typename U, typename V, typename W> QMap<int, std::tuple<T, U, V, W> > indexToAnnotationValues(const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, const QString &annotationLabel3, const QSet<V> &validAnnotationValues3, const QString &annotationLabel4, const QSet<W> &validAnnotationValues4, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U, V, W>(this->header(orientation).indexes(selectedOnly), annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, annotationLabel3, validAnnotationValues3, annotationLabel4, validAnnotationValues4, orientation);

    }

    template <typename T, typename U, typename V, typename W, typename X> QMap<int, std::tuple<T, U, V, W, X> > indexToAnnotationValues(const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, const QString &annotationLabel3, const QSet<V> &validAnnotationValues3, const QString &annotationLabel4, const QSet<W> &validAnnotationValues4, const QString &annotationLabel5, const QSet<X> &validAnnotationValues5, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->indexToAnnotationValues<T, U, V, W, X>(this->header(orientation).indexes(selectedOnly), annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, annotationLabel3, validAnnotationValues3, annotationLabel4, validAnnotationValues4, annotationLabel5, validAnnotationValues5, orientation);

    }

    template <typename T, typename U> QMap<int, std::tuple<T, U> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QString &annotationLabel2, BaseMatrix::Orientation orientation) const {

        return this->indexToAnnotationValues<T, U>(inputIndexes, annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), orientation);

    }

    template <typename T, typename U, typename V> QMap<int, std::tuple<T, U, V> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QString &annotationLabel2, const QString &annotationLabel3, BaseMatrix::Orientation orientation) const {

        return this->indexToAnnotationValues<T, U, V>(inputIndexes, annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), annotationLabel3, QSet<V>(), orientation);

    }

    template <typename T, typename U, typename V, typename W> QMap<int, std::tuple<T, U, V, W> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QString &annotationLabel2, const QString &annotationLabel3, const QString &annotationLabel4, BaseMatrix::Orientation orientation) const {

        return this->indexToAnnotationValues<T, U, V, W>(inputIndexes, annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), annotationLabel3, QSet<V>(), annotationLabel4, QSet<W>(), orientation);

    }

    template <typename T, typename U, typename V, typename W, typename X> QMap<int, std::tuple<T, U, V, W, X> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QString &annotationLabel2, const QString &annotationLabel3, const QString &annotationLabel4, const QString &annotationLabel5, BaseMatrix::Orientation orientation) const {

        return this->indexToAnnotationValues<T, U, V, W, X>(inputIndexes, annotationLabel1, QSet<T>(), annotationLabel2, QSet<U>(), annotationLabel3, QSet<V>(), annotationLabel4, QSet<W>(), annotationLabel5, QSet<X>(), orientation);

    }

    template <typename T, typename U> QMap<int, std::tuple<T, U> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, BaseMatrix::Orientation orientation) const {

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QMap<int, std::tuple<T, U> > _indexToAnnotationValues;

        for (const int &inputIndex: inputIndexes) {

            const QString &currentIdentifier(header.identifier(inputIndex));

            std::tuple<T, U> convertedAnnotationValues;

            if (annotations.values(currentIdentifier, annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, convertedAnnotationValues))
                    _indexToAnnotationValues.insert(inputIndex, convertedAnnotationValues);

        }

        return _indexToAnnotationValues;

    }

    template <typename T, typename U, typename V> QMap<int, std::tuple<T, U, V> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, const QString &annotationLabel3, const QSet<V> &validAnnotationValues3, BaseMatrix::Orientation orientation) const {

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QMap<int, std::tuple<T, U, V> > _indexToAnnotationValues;

        for (const int &inputIndex: inputIndexes) {

            const QString &currentIdentifier(header.identifier(inputIndex));

            std::tuple<T, U, V> convertedAnnotationValues;

            if (annotations.values(currentIdentifier, annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, annotationLabel3, validAnnotationValues3, convertedAnnotationValues))
                _indexToAnnotationValues.insert(inputIndex, convertedAnnotationValues);

        }

        return _indexToAnnotationValues;

    }

    template <typename T, typename U, typename V, typename W> QMap<int, std::tuple<T, U, V, W> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, const QString &annotationLabel3, const QSet<V> &validAnnotationValues3, const QString &annotationLabel4, const QSet<W> &validAnnotationValues4, BaseMatrix::Orientation orientation) const {

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QMap<int, std::tuple<T, U, V, W> > _indexToAnnotationValues;

        for (const int &inputIndex: inputIndexes) {

            const QString &currentIdentifier(header.identifier(inputIndex));

            std::tuple<T, U, V, W> convertedAnnotationValues;

            if (annotations.values(currentIdentifier, annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, annotationLabel3, validAnnotationValues3, annotationLabel4, validAnnotationValues4, convertedAnnotationValues))
                    _indexToAnnotationValues.insert(inputIndex, convertedAnnotationValues);

        }

        return _indexToAnnotationValues;

    }

    template <typename T, typename U, typename V, typename W, typename X> QMap<int, std::tuple<T, U, V, W, X> > indexToAnnotationValues(const QList<int> &inputIndexes, const QString &annotationLabel1, const QSet<T> &validAnnotationValues1, const QString &annotationLabel2, const QSet<U> &validAnnotationValues2, const QString &annotationLabel3, const QSet<V> &validAnnotationValues3, const QString &annotationLabel4, const QSet<W> &validAnnotationValues4, const QString &annotationLabel5, const QSet<X> &validAnnotationValues5, BaseMatrix::Orientation orientation) const {

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        QMap<int, std::tuple<T, U, V, W, X> > _indexToAnnotationValues;

        for (const int &inputIndex: inputIndexes) {

            const QString &currentIdentifier(header.identifier(inputIndex));

            std::tuple<T, U, V, W, X> convertedAnnotationValues;

            if (annotations.values(currentIdentifier, annotationLabel1, validAnnotationValues1, annotationLabel2, validAnnotationValues2, annotationLabel3, validAnnotationValues3, annotationLabel4, validAnnotationValues4, annotationLabel5, validAnnotationValues5, convertedAnnotationValues))
                    _indexToAnnotationValues.insert(inputIndex, convertedAnnotationValues);

        }

        return _indexToAnnotationValues;

    }

    void insertVector(int index, const QString &identifier, BaseMatrix::Orientation orientation);

    template <typename T>
    bool insertVector(int index, const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage = nullptr);

    void insertVectors(int index, const QStringList &identifiers, BaseMatrix::Orientation orientation);

    const QString &name() const;

    const Project *parentProject() const;

    void prependVector(const QString &identifier, BaseMatrix::Orientation orientation);

    template <typename T>
    bool prependVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage = nullptr);

    void prependVectors(const QStringList &identifiers, BaseMatrix::Orientation orientation);

    void removeVector(int index, BaseMatrix::Orientation orientation);

    void removeVectors(const QList<int> &indexes, BaseMatrix::Orientation orientation);

    void removeVectors(const QString &identifier, BaseMatrix::Orientation orientation, bool selectedOnly = false);

    void removeVectors(const QStringList &identifiers, BaseMatrix::Orientation orientation, bool selectedOnly = false);

    template <typename T> void setAnnotationValue(int index, const QString &annotationLabel, T &annotationValue, BaseMatrix::Orientation orientation) {

        this->annotations(orientation).setValue(this->header(orientation).identifier(index), annotationLabel, annotationValue);

    }

    template <typename T> void setAnnotationValue(int indexOfIdentifier, int indexOfAnnotationLabel, T &annotationValue, BaseMatrix::Orientation orientation) {

        this->annotations(orientation).setValue(this->header(orientation).identifier(indexOfIdentifier), this->annotations(orientation).label(indexOfAnnotationLabel), annotationValue);

    }

    void setParentProject(Project *parentProject);

    void swap(int index1, int index2, BaseMatrix::Orientation orientation, bool update = true);

    void transpose();

    template <typename T> QSet<T> uniqueAnnotationValues(const QString &label, BaseMatrix::Orientation orientation, bool selectedOnly = false) const {

        return this->uniqueAnnotationValues<T>(this->header(orientation).indexes(selectedOnly), label, orientation);

    }

    template <typename T> QSet<T> uniqueAnnotationValues(const QList<int> &inputIndexes, const QString &label, BaseMatrix::Orientation orientation) const {

        QSet<T> _uniqueAnnotationValues;

        const Header &header(this->header(orientation));

        const Annotations &annotations(this->annotations(orientation));

        for (const int &inputIndex: inputIndexes) {

            T value;

            if (annotations.value(header.identifier(inputIndex), label, value))
                _uniqueAnnotationValues.insert(value);

        }

        return _uniqueAnnotationValues;

    }

    const QUuid &universallyUniqueIdentifier() const;

    BaseDataset &operator=(const BaseDataset &other);

    friend QDataStream& operator<<(QDataStream& out, BaseDataset &baseDataset);

    friend QDataStream& operator>>(QDataStream& in, BaseDataset &baseDataset);

protected:

    QSharedDataPointer<BaseDatasetData> d_data;

    QUuid d_uuid;

public slots:

    void setName(const QString &name);

signals:

    void nameChanged(const QUuid &uuid, const QString &name);

};

Q_DECLARE_TYPEINFO(BaseDataset, Q_MOVABLE_TYPE);

template <typename T>
bool BaseDataset::appendVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage)
{

    this->BaseDataset::header(orientation).append(identifier);

    return d_data->d_baseMatrix->appendVector(vector, orientation, errorMessage);

}

template <typename T>
bool BaseDataset::insertVector(int index, const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage)
{

    this->BaseDataset::header(orientation).insert(index, identifier);

    return d_data->d_baseMatrix->insertVector(index, vector, orientation, errorMessage);

}

template <typename T>
bool BaseDataset::prependVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage)
{

    this->BaseDataset::header(orientation).prepend(identifier);

    return d_data->d_baseMatrix->prependVector(vector, orientation, errorMessage);

}

#endif // BASEDATASET_H
