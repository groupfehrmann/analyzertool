#ifndef SPINBOXBUILDINGBLOCKWIDGET_H
#define SPINBOXBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QSpinBox>
#include <QLabel>
#include <QStringList>
#include <QSettings>
#include <QString>

#include "dialogs/basedialog.h"

namespace Ui {

class SpinBoxBuildingBlockWidget;

}

class SpinBoxBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SpinBoxBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), bool rememberSettings = false, int displayIntegerBase = 10, int mimumum = 0, int maximum = 100, int value = 1, int singleStep = 1, const QString &prefix = QString(), const QString &suffix = QString());

    ~SpinBoxBuildingBlockWidget();

    QSpinBox *pointerToInternalSpinBox();

    QLabel *pointerToInternalLabel();

private:

    Ui::SpinBoxBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

};

#endif // SPINBOXBUILDINGBLOCKWIDGET_H
