#include "projectsandresultsoverviewwidget.h"
#include "ui_projectsandresultsoverviewwidget.h"

ProjectsAndResultsOverviewWidget::ProjectsAndResultsOverviewWidget(QWidget *parent) :
    QWidget(parent), ui(new Ui::ProjectsAndResultsOverviewWidget)
{

    ui->setupUi(this);

    d_projectListModel = new ProjectListModel(this, new ProjectList);

    ui->treeView_projects->setModel(d_projectListModel);

    d_resultListModel = new ResultListModel(this, new ResultList);

    ui->treeView_results->setModel(d_resultListModel);

    this->connect(ui->comboBox_selectOverview, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBox_selectOverview_currentIndexChanged(int)));

    this->connect(ui->treeView_projects->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this, SLOT(modelIndexActivated(QModelIndex,QModelIndex)));

    this->connect(ui->treeView_results->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this, SLOT(modelIndexActivated(QModelIndex,QModelIndex)));

    this->connect(d_projectListModel, SIGNAL(datasetAdded(QSharedPointer<BaseDataset>)), this, SIGNAL(datasetAdded(QSharedPointer<BaseDataset>)));

    this->connect(d_projectListModel, SIGNAL(datasetNameChanged(QUuid,QString)), this, SIGNAL(datasetNameChanged(QUuid,QString)));

    this->connect(d_projectListModel, SIGNAL(datasetRemoved(QUuid)), this, SIGNAL(datasetRemoved(QUuid)));

    this->connect(d_projectListModel, SIGNAL(projectAdded(QSharedPointer<Project>)), this, SIGNAL(projectAdded(QSharedPointer<Project>)));

    this->connect(d_projectListModel, SIGNAL(projectNameChanged(QUuid,QString)), this, SIGNAL(projectNameChanged(QUuid,QString)));

    this->connect(d_projectListModel, SIGNAL(projectRemoved(QUuid)), this, SIGNAL(projectRemoved(QUuid)));

    this->connect(d_projectListModel, SIGNAL(showSelectedOnly(QUuid,bool)), this, SIGNAL(showSelectedOnly(QUuid,bool)));

    this->connect(d_resultListModel, SIGNAL(resultItemAdded(QSharedPointer<BaseResultItem>)), this, SIGNAL(resultItemAdded(QSharedPointer<BaseResultItem>)));

    this->connect(d_resultListModel, SIGNAL(resultItemNameChanged(QUuid,QString)), this, SIGNAL(resultItemNameChanged(QUuid,QString)));

    this->connect(d_resultListModel, SIGNAL(resultItemRemoved(QUuid)), this, SIGNAL(resultItemRemoved(QUuid)));

    this->connect(d_resultListModel, SIGNAL(resultAdded(QSharedPointer<Result>)), this, SIGNAL(resultAdded(QSharedPointer<Result>)));

    this->connect(d_resultListModel, SIGNAL(resultNameChanged(QUuid,QString)), this, SIGNAL(resultNameChanged(QUuid,QString)));

    this->connect(d_resultListModel, SIGNAL(resultRemoved(QUuid)), this, SIGNAL(resultRemoved(QUuid)));

    this->addProject(new Project());

}

ProjectsAndResultsOverviewWidget::~ProjectsAndResultsOverviewWidget()
{

    delete ui;

}

void ProjectsAndResultsOverviewWidget::addDataset(BaseDataset *baseDataset)
{

    QModelIndex currentIndex = ui->treeView_projects->currentIndex();

    if (!currentIndex.isValid()) {

        this->addProject(new Project);

        currentIndex = ui->treeView_projects->currentIndex();

    }

    ProjectListModelItem *item = static_cast<ProjectListModelItem *>(currentIndex.internalPointer());

    if (dynamic_cast<Project *>(item->internalObject()))
        d_projectListModel->insertBaseDataset(d_projectListModel->rowCount(currentIndex), baseDataset, currentIndex);

    if (dynamic_cast<BaseDataset *>(item->internalObject())) {

        d_projectListModel->insertBaseDataset(d_projectListModel->rowCount(currentIndex.parent()), baseDataset, currentIndex.parent());

        currentIndex = currentIndex.parent();

    }

    ui->treeView_projects->setCurrentIndex(d_projectListModel->index(d_projectListModel->rowCount(currentIndex) - 1, 0, currentIndex));

    this->blockSignals(true);

    ui->comboBox_selectOverview->setCurrentIndex(0);

    this->blockSignals(false);

}

void ProjectsAndResultsOverviewWidget::addProject(Project *project)
{

    d_projectListModel->insertProject(d_projectListModel->rowCount(), project, QModelIndex());

    ui->treeView_projects->setCurrentIndex(d_projectListModel->index(d_projectListModel->rowCount() - 1, 0, QModelIndex()));

    this->blockSignals(true);

    ui->comboBox_selectOverview->setCurrentIndex(0);

    this->blockSignals(false);

}

void ProjectsAndResultsOverviewWidget::addResult(Result *result)
{

    d_resultListModel->appendResult(result, QModelIndex());

    ui->treeView_results->setCurrentIndex(d_resultListModel->index(d_resultListModel->rowCount() - 1, 0, QModelIndex()));

    this->blockSignals(true);

    ui->comboBox_selectOverview->setCurrentIndex(1);

    this->blockSignals(false);

}

void ProjectsAndResultsOverviewWidget::addResultItem(BaseResultItem *baseResultItem)
{

    QModelIndex currentIndex = ui->treeView_results->currentIndex();

    if (!currentIndex.isValid()) {

        this->addResult(new Result);

        currentIndex = ui->treeView_results->currentIndex();

    }

    ResultListModelItem *item = static_cast<ResultListModelItem *>(currentIndex.internalPointer());

    if (dynamic_cast<Result *>(item->internalObject()))
        d_resultListModel->appendResultItem(baseResultItem, currentIndex);

    if (dynamic_cast<BaseResultItem *>(item->internalObject())) {

        d_resultListModel->appendResultItem(baseResultItem, currentIndex.parent());

        currentIndex = currentIndex.parent();

    }

    ui->treeView_results->setCurrentIndex(d_resultListModel->index(d_resultListModel->rowCount(currentIndex) - 1, 0, currentIndex));

    this->blockSignals(true);

    ui->comboBox_selectOverview->setCurrentIndex(1);

    this->blockSignals(false);

}

void ProjectsAndResultsOverviewWidget::comboBox_selectOverview_currentIndexChanged(int index)
{

    if (index == 0) {

        if (ui->treeView_projects->currentIndex().isValid()) {

            ProjectListModelItem *item = static_cast<ProjectListModelItem*>(ui->treeView_projects->currentIndex().internalPointer());

            if (const Project *project = dynamic_cast<const Project *>(item->internalObject())) {

                emit itemActivated(project->universallyUniqueIdentifier());

                emit projectSelected();

                return;

            }

            if (const BaseDataset *baseDataset = dynamic_cast<const BaseDataset *>(item->internalObject())) {

                emit itemActivated(baseDataset->universallyUniqueIdentifier());

                emit datasetSelected();

                return;

            }

        } else
            emit itemActivated(QUuid());

    } else {

        if (ui->treeView_results->currentIndex().isValid()) {

            ResultListModelItem *item = static_cast<ResultListModelItem*>(ui->treeView_results->currentIndex().internalPointer());

            if (const Result *result = dynamic_cast<const Result *>(item->internalObject())) {

                emit itemActivated(result->universallyUniqueIdentifier());

                emit resultSelected();

                return;

            }

            if (const BaseResultItem *baseResultItem = dynamic_cast<const BaseResultItem *>(item->internalObject())) {

                emit itemActivated(baseResultItem->universallyUniqueIdentifier());

                emit resultItemSelected();

                return;

            }

        } else
            emit itemActivated(QUuid());

    }

}

QSharedPointer<PlotResultItem> ProjectsAndResultsOverviewWidget::currentPlotResultItem() const
{

    QModelIndex currentIndex = ui->treeView_results->currentIndex();

    if (!currentIndex.isValid() || !currentIndex.parent().isValid())
        return QSharedPointer<PlotResultItem>();

    QSharedPointer<PlotResultItem> item = qSharedPointerDynamicCast<PlotResultItem>(d_resultListModel->resultList()->resultAt(currentIndex.parent().row())->baseResultItemAt(currentIndex.row()));

    if (item)
        return item;
    else
        return QSharedPointer<PlotResultItem>();

}

QSharedPointer<Project> ProjectsAndResultsOverviewWidget::currentProject() const
{

    QModelIndex currentIndex = ui->treeView_projects->currentIndex();

    if (!currentIndex.isValid())
        return QSharedPointer<Project>();

    if (!currentIndex.parent().isValid())
        return d_projectListModel->projectList()->projectAt(currentIndex.row());
    else
        return d_projectListModel->projectList()->projectAt(currentIndex.parent().row());

}

QSharedPointer<BaseDataset> ProjectsAndResultsOverviewWidget::currentDataset() const
{

    QModelIndex currentIndex = ui->treeView_projects->currentIndex();

    if (!currentIndex.isValid() || !currentIndex.parent().isValid())
        return QSharedPointer<BaseDataset>();

    return d_projectListModel->projectList()->projectAt(currentIndex.parent().row())->baseDatasetAt(currentIndex.row());

}

QSharedPointer<Result> ProjectsAndResultsOverviewWidget::currentResult() const
{

    QModelIndex currentIndex = ui->treeView_results->currentIndex();

    if (!currentIndex.isValid())
        return QSharedPointer<Result>();

    if (!currentIndex.parent().isValid())
        return d_resultListModel->resultList()->resultAt(currentIndex.row());
    else
        return d_resultListModel->resultList()->resultAt(currentIndex.parent().row());

}

QSharedPointer<BaseResultItem> ProjectsAndResultsOverviewWidget::currentResultItem() const
{

    QModelIndex currentIndex = ui->treeView_results->currentIndex();

    if (!currentIndex.isValid() || !currentIndex.parent().isValid())
        return QSharedPointer<BaseResultItem>();

    return d_resultListModel->resultList()->resultAt(currentIndex.parent().row())->baseResultItemAt(currentIndex.row());

}

bool ProjectsAndResultsOverviewWidget::currentSelectedOnly() const
{

    QModelIndex currentIndex = ui->treeView_projects->currentIndex();

    if (!currentIndex.isValid() || !currentIndex.parent().isValid())
        return false;

    return (d_projectListModel->data(currentIndex, Qt::CheckStateRole).toInt() == Qt::Checked) ? true : false;

}

ProjectsAndResultsOverviewWidget::ItemType ProjectsAndResultsOverviewWidget::currentSelectedItemType() const
{

    if (ui->comboBox_selectOverview->currentIndex() == 0) {

        QModelIndex currentIndex = ui->treeView_projects->currentIndex();

        if (!currentIndex.isValid())
            return ProjectsAndResultsOverviewWidget::INVALID;

        ProjectListModelItem *item = static_cast<ProjectListModelItem *>(currentIndex.internalPointer());

        if (dynamic_cast<const Project *>(item->internalObject()))
            return ProjectsAndResultsOverviewWidget::PROJECT;

        if (dynamic_cast<const BaseDataset *>(item->internalObject()))
            return ProjectsAndResultsOverviewWidget::DATASET;

    } else {

        QModelIndex currentIndex = ui->treeView_results->currentIndex();

        if (!currentIndex.isValid())
            return ProjectsAndResultsOverviewWidget::INVALID;

        ResultListModelItem *item = static_cast<ResultListModelItem *>(currentIndex.internalPointer());

        if (dynamic_cast<const Result *>(item->internalObject()))
            return ProjectsAndResultsOverviewWidget::RESULT;

        if (dynamic_cast<const TextResultItem *>(item->internalObject()))
            return ProjectsAndResultsOverviewWidget::TEXTRESULTITEM;

        if (dynamic_cast<const TableResultItem *>(item->internalObject()))
            return ProjectsAndResultsOverviewWidget::TABLERESULTITEM;

        if (dynamic_cast<const PlotResultItem *>(item->internalObject()))
            return ProjectsAndResultsOverviewWidget::PLOTRESULTITEM;

    }

    return ProjectsAndResultsOverviewWidget::INVALID;

}

void ProjectsAndResultsOverviewWidget::modelIndexActivated(const QModelIndex &current, const QModelIndex &previous) const
{

    Q_UNUSED(previous);

    if (!current.isValid()) {

        emit noValidItemSelected();

        emit itemActivated(QUuid());

        return;

    }

    if (ui->treeView_projects == static_cast<QItemSelectionModel *>(this->sender())->parent()) {

        ProjectListModelItem *item = static_cast<ProjectListModelItem*>(current.internalPointer());

        if (const Project *project = dynamic_cast<const Project *>(item->internalObject())) {

            emit itemActivated(project->universallyUniqueIdentifier());

            emit projectSelected();

            return;

        }

        if (const BaseDataset *baseDataset = dynamic_cast<const BaseDataset *>(item->internalObject())) {

            emit itemActivated(baseDataset->universallyUniqueIdentifier());

            emit datasetSelected();

            return;

        }

    }

    if (ui->treeView_results == static_cast<QItemSelectionModel *>(this->sender())->parent()) {

        ResultListModelItem *item = static_cast<ResultListModelItem*>(current.internalPointer());

        if (const Result *result = dynamic_cast<const Result *>(item->internalObject())) {

            emit itemActivated(result->universallyUniqueIdentifier());

            emit resultSelected();

            return;

        }

        if (const BaseResultItem *baseResultItem = dynamic_cast<const BaseResultItem *>(item->internalObject())) {

            emit itemActivated(baseResultItem->universallyUniqueIdentifier());

            emit resultItemSelected();

            return;

        }

    }

    emit itemActivated(QUuid());

}

const ProjectListModel *ProjectsAndResultsOverviewWidget::projectListModel() const
{

    return d_projectListModel;

}

void ProjectsAndResultsOverviewWidget::removeAll()
{

    this->modelIndexActivated(QModelIndex(), QModelIndex());

    d_projectListModel->removeAll();

    d_resultListModel->removeAll();

    ui->comboBox_selectOverview->setCurrentIndex(0);

}

void ProjectsAndResultsOverviewWidget::removeCurrentSelected()
{

    if (ui->comboBox_selectOverview->currentIndex() == 0) {

        if (ui->treeView_projects->currentIndex().isValid())
            d_projectListModel->removeRow(ui->treeView_projects->currentIndex().row(), ui->treeView_projects->currentIndex().parent());

        if (d_projectListModel->rowCount() == 0)
            ui->comboBox_selectOverview->setCurrentIndex(0);

    } else {

        if (ui->treeView_results->currentIndex().isValid())
            d_resultListModel->removeRow(ui->treeView_results->currentIndex().row(), ui->treeView_results->currentIndex().parent());

        if (d_resultListModel->rowCount() == 0)
            ui->comboBox_selectOverview->setCurrentIndex(0);

    }

}
