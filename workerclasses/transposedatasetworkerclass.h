#ifndef TRANSPOSEDATASETWORKERCLASS_H
#define TRANSPOSEDATASETWORKERCLASS_H

#include <QString>
#include <QSharedPointer>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"

class TransposeDatasetWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QSharedPointer<BaseDataset> datasetToTranspose;

    };

    TransposeDatasetWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    TransposeDatasetWorkerClass::Parameters d_parameters;

};

#endif // TRANSPOSEDATASETWORKERCLASS_H
