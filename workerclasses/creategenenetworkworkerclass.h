#ifndef CREATEGENENETWORKWORKERCLASS_H
#define CREATEGENENETWORKWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QVariantList>

#include <tuple>
#include <algorithm>
#include <cmath>

#include "boost/math/distributions/normal.hpp"

#include "base/baseworkerclass.h"
#include "base/basedataset.h"
#include "base/convertfunctions.h"
#include "statistics/studentt.h"
#include "statistics/welcht.h"
#include "statistics/levenef.h"
#include "statistics/mannwhitneyu.h"
#include "statistics/kolmogorovsmirnovz.h"
#include "statistics/kruskalwallischisquared.h"
#include "statistics/pearsonr.h"
#include "statistics/spearmanr.h"
#include "statistics/distancecorrelation.h"
#include "math/statfunctions.h"

class CreateGeneNetworkWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QStringList annotationLabelsDefiningSearchableKeys;

        QString annotationLabelUsedToMatch;

        QSharedPointer<BaseDataset> baseDataset;

        QString distanceMeasurementForGuiltByAssociation;

        QString exportDirectory;

        QStringList geneSetFiles;

        int maximumSizeOfGeneSet;

        int minimumSizeOfGeneSet;

        int numberOfPermutationRounds;

        BaseMatrix::Orientation orientation;

        QString outputFormat;

        bool performPermutations;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        QString testToDefineEnrichmentVectorForEachGeneSet;

    };

    struct EnrichmentDataElement {

        int geneSetIndex;

        int geneIndex;

        int numberOfGenesInGeneSet;

        double zTransformedPValue;

        double associationStatistic;

        bool allreadyAssignedToGeneSet;

    };

    CreateGeneNetworkWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

    void updateFileProgress();

private:

    QFile d_file;

    QVector<QStringList> d_databaseToGeneSetIdentifiers;

    QVector<QStringList> d_databaseToGeneSetDescriptions;

    QVector<QVector<QSet<QString> > > d_databaseToGeneSetToGeneSetMembers;

    QStringList d_geneIdentifiers;

    QHash<QString, int> d_geneIdentifierToIndex;

    QStringList d_geneAnnotations;

    QVector<QStringList> d_tokenizedGeneAnnotations;

    CreateGeneNetworkWorkerClass::Parameters d_parameters;

    bool checkInputParameters();

    bool readGeneSetFile(const QString &geneSetFile);

    bool writePredictionsForIndividualGeneToFile_tabDelimited(int geneIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path);

    bool writePredictionsForIndividualGeneToFile_json(int geneIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path);

    bool writePredictionsForIndividualGeneSetToFile_tabDelimited(int geneSetIndex, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path);

    bool writePredictionsForIndividualGeneSetToFile_json(int geneSetIndex, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path);

    bool writeGeneSearchableKeysToIndex_tabDelimited(const QString &path);

    bool writeGeneSearchableKeysToIndex_json(const QString &path);

    bool writeMatrixWithZTransformedPValues(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > & enrichmentDataElements, const QString &path, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions);

    bool writeMatrixWithAssociationStatistic(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > & enrichmentDataElements, const QString &path, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions);

    bool writeGeneSetToDefaultEnrichmentVector(int databaseIndex, const QVector<QVector<double> > &geneSetToDefaultEnrichmentVector, const QString &path, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions);

};

#endif // CREATEGENENETWORKWORKERCLASS_H
