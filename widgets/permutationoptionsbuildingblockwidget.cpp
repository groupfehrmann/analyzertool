#include "permutationoptionsbuildingblockwidget.h"
#include "ui_permutationoptionsbuildingblockwidget.h"

PermutationOptionsBuildingBlockWidget::PermutationOptionsBuildingBlockWidget(BaseDialog *parent, const QString &widgetId) :
    QWidget(parent), ui(new Ui::PermutationOptionsBuildingBlockWidget)
{

    ui->setupUi(this);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/permutationOptionsBuildingBlockWidget");

    if (settings.contains("checkBox_Enable/state"))
        ui->checkBox_Enable->setCheckState(static_cast<Qt::CheckState>(settings.value("checkBox_Enable/state").toInt()));

    if (settings.contains("spinBox_FDR/value"))
        ui->spinBox_FDR->setValue(settings.value("spinBox_FDR/value").toInt());

    if (settings.contains("spinBox_CL/value"))
        ui->spinBox_CL->setValue(settings.value("spinBox_CL/value").toInt());

    if (settings.contains("spinBox_Permutations/value"))
        ui->spinBox_Permutations->setValue(settings.value("spinBox_Permutations/value").toInt());

    settings.endGroup();

    this->connect(ui->checkBox_Enable, SIGNAL(stateChanged(int)), this, SLOT(checkBox_Enable_stateChanged(int)));

    this->checkBox_Enable_stateChanged(ui->checkBox_Enable->checkState());

}

PermutationOptionsBuildingBlockWidget::~PermutationOptionsBuildingBlockWidget()
{

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/permutationOptionsBuildingBlockWidget");

    settings.setValue("checkBox_Enable/state", ui->checkBox_Enable->checkState());

    settings.setValue("spinBox_FDR/value", ui->spinBox_FDR->value());

    settings.setValue("spinBox_CL/value", ui->spinBox_CL->value());

    settings.setValue("spinBox_Permutations/value", ui->spinBox_Permutations->value());

    settings.endGroup();

    delete ui;

}

void PermutationOptionsBuildingBlockWidget::checkBox_Enable_stateChanged(int state)
{

    bool checked = (static_cast<Qt::CheckState>(state) == Qt::Checked);

    ui->spinBox_FDR->setEnabled(checked);

    ui->spinBox_CL->setEnabled(checked);

    ui->spinBox_Permutations->setEnabled(checked);

}

QCheckBox *PermutationOptionsBuildingBlockWidget::pointerToInternalCheckBox_enable()
{

    return ui->checkBox_Enable;

}

QSpinBox *PermutationOptionsBuildingBlockWidget::pointerToInternalSpinBox_FDR()
{

    return ui->spinBox_FDR;

}

QSpinBox *PermutationOptionsBuildingBlockWidget::pointerToInternalSpinBox_CL()
{

    return ui->spinBox_CL;

}

QSpinBox *PermutationOptionsBuildingBlockWidget::pointerToInternalSpinBox_Permutations()
{

    return ui->spinBox_Permutations;

}
