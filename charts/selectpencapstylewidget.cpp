#include "selectpencapstylewidget.h"
#include "ui_selectpencapstylewidget.h"

SelectPenCapStyleWidget::SelectPenCapStyleWidget(QWidget *parent, Qt::PenCapStyle defaultPenCapStyle) :
    QWidget(parent), ui(new Ui::SelectPenCapStyleWidget)
{

    ui->setupUi(this);

    ui->comboBox->addItem(this->createPixmap(Qt::FlatCap), "A square line end that does not cover the end point of the line", static_cast<int>(Qt::FlatCap));

    ui->comboBox->addItem(this->createPixmap(Qt::SquareCap), "A square line end that covers the end point and extends beyond it by half the line width", static_cast<int>(Qt::SquareCap));

    ui->comboBox->addItem(this->createPixmap(Qt::RoundCap), "a rounded line end", static_cast<int>(Qt::RoundCap));

    this->connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexOfComboBoxChanged(int)));

    this->setCurrentPenCapStyle(defaultPenCapStyle);

}

SelectPenCapStyleWidget::~SelectPenCapStyleWidget()
{

    delete ui;

}

QPixmap SelectPenCapStyleWidget::createPixmap(Qt::PenCapStyle penCapStyle)
{

    QPixmap pixmap(100, 100);

    pixmap.fill(Qt::transparent);

    QPen pen;

    pen.setWidth(10);

    pen.setStyle(Qt::SolidLine);

    pen.setCapStyle(penCapStyle);

    QPainter painter(&pixmap);

    painter.setPen(pen);

    QPointF points[5] = { QPointF(10.0, 90.0), QPointF(10.0, 10.0), QPointF(90.0, 10.0), QPointF(90.0, 50.0), QPointF(30.0, 50.0) };

    painter.drawPolyline(points, 5);

    return pixmap;

}

void SelectPenCapStyleWidget::currentIndexOfComboBoxChanged(int index)
{

    Q_UNUSED(index);

    emit currentPenCapStyleChanged(this->currentPenCapStyle());

}

Qt::PenCapStyle SelectPenCapStyleWidget::currentPenCapStyle()
{

    return static_cast<Qt::PenCapStyle>(ui->comboBox->currentData().toInt());

}

void SelectPenCapStyleWidget::setCurrentPenCapStyle(Qt::PenCapStyle penCapStyle)
{

    switch (penCapStyle) {

        case Qt::FlatCap: ui->comboBox->setCurrentIndex(0); break;

        case Qt::SquareCap: ui->comboBox->setCurrentIndex(1); break;

        case Qt::RoundCap: ui->comboBox->setCurrentIndex(2); break;

        default : ui->comboBox->setCurrentIndex(-1);

    }

}
