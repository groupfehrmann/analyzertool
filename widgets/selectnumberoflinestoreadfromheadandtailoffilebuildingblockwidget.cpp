#include "selectnumberoflinestoreadfromheadandtailoffilebuildingblockwidget.h"
#include "ui_selectnumberoflinestoreadfromheadandtailoffilebuildingblockwidget.h"

SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget::SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget(BaseDialog *parent, const QString &widgetId) :
    QWidget(parent), ui(new Ui::SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget)
{

    ui->setupUi(this);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget");

    if (settings.contains("numberOfLinesToReadFromHeadOfFile"))
        ui->spinBox_numberOfLinesFromHeadOfFile->setValue(settings.value("numberOfLinesToReadFromHeadOfFile").toInt());

    if (settings.contains("numberOfLinesToReadFromTailOfFile"))
        ui->spinBox_numberOfLinesFromtailOfFile->setValue(settings.value("numberOfLinesToReadFromTailOfFile").toInt());

    settings.endGroup();

}

SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget::~SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget()
{

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget");

    settings.setValue("numberOfLinesToReadFromHeadOfFile", ui->spinBox_numberOfLinesFromHeadOfFile->value());

    settings.setValue("numberOfLinesToReadFromTailOfFile", ui->spinBox_numberOfLinesFromtailOfFile->value());

    delete ui;

}

QSpinBox *SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget::pointerToInternalSpinBoxNumberOfLinesFromHeadOfFile()
{

    return ui->spinBox_numberOfLinesFromHeadOfFile;

}

QSpinBox *SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget::pointerToInternalSpinBoxNumberOfLinesFromTailOfFile()
{

    return ui->spinBox_numberOfLinesFromtailOfFile;

}
