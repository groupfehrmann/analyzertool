#include "scatterchart_genomicmappingonxaxis_homosapiens.h"

ScatterChart_GenomicMappingOnXAxis_HomoSapiens::ScatterChart_GenomicMappingOnXAxis_HomoSapiens(QObject *parent, const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QVector<double> &rankedCumulativeBasepairMappingForAllItems, double minimumBasepair, double maximumBasepair, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &basepairMappings_xAxes, const QVector<QVector<double> > &values_yAxes, QChart::ChartType chartType, bool seperateYAxisForEachSubset) :
    BaseChart(parent), d_chartType(chartType), d_seperateYAxisForEachSubset(seperateYAxisForEachSubset)
{

    this->connect(this, SIGNAL(initializeChart(QString,QVector<int>,QVector<double>,double,double,QStringList,QVector<QVector<double> >,QVector<QVector<double> >)), SLOT(_initializeChart(QString,QVector<int>,QVector<double>,double,double,QStringList,QVector<QVector<double> >,QVector<QVector<double> >)));

    emit initializeChart(title, chromosomeMappingsForAllSubsets, rankedCumulativeBasepairMappingForAllItems, minimumBasepair, maximumBasepair, subsetIdentifiers, basepairMappings_xAxes, values_yAxes);

    d_eventLoop.exec();

}

ScatterChart_GenomicMappingOnXAxis_HomoSapiens::ScatterChart_GenomicMappingOnXAxis_HomoSapiens(QObject *parent, const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QVector<double> &rankedCumulativeBasepairMappingForAllItems, double minimumBasepair, double maximumBasepair, const QVector<double> &basepairMappings_xAxis, const QVector<double> &values_yAxis, QChart::ChartType chartType, bool seperateYAxisForEachSubset) :
    BaseChart(parent), d_chartType(chartType), d_seperateYAxisForEachSubset(seperateYAxisForEachSubset)
{

    this->connect(this, SIGNAL(initializeChart(QString,QVector<int>,QVector<double>,double,double,QStringList,QVector<QVector<double> >,QVector<QVector<double> >)), SLOT(_initializeChart(QString,QVector<int>,QVector<double>,double,double,QStringList,QVector<QVector<double> >,QVector<QVector<double> >)));

    emit initializeChart(title, chromosomeMappingsForAllSubsets, rankedCumulativeBasepairMappingForAllItems, minimumBasepair, maximumBasepair, {QString()}, QVector<QVector<double> >() << basepairMappings_xAxis, QVector<QVector<double> >() << values_yAxis);

    d_eventLoop.exec();

}

void ScatterChart_GenomicMappingOnXAxis_HomoSapiens::_initializeChart(const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QVector<double> &rankedCumulativeBasepairMappingForAllItems, double minimumBasepair, double maximumBasepair, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &basepairMappings_xAxes, const QVector<QVector<double> > &values_yAxes)
{

    QChart *chart;

    if (d_chartType == QChart::ChartTypePolar)
        chart = new QPolarChart;
    else
        chart = new QChart;

    chart->setTitle(title);


    QCategoryAxis *xAxis = new QCategoryAxis;

    xAxis->setMin(minimumBasepair);

    xAxis->setMax(maximumBasepair);

    xAxis->setShadesColor(QColor(245, 245, 245));

    xAxis->setShadesVisible(true);

    xAxis->setShadesBorderColor(Qt::transparent);

    xAxis->setGridLineVisible(false);

    xAxis->setTitleText("Genomic mapping");

    xAxis->setGridLineVisible(false);

    for (int i = 1; i < chromosomeMappingsForAllSubsets.size() - 1; ++i) {

        if (chromosomeMappingsForAllSubsets.at(i - 1) != chromosomeMappingsForAllSubsets.at(i)) {

            int currentChromosomeNumber = chromosomeMappingsForAllSubsets.at(i - 1);

            if (currentChromosomeNumber == 23)
                xAxis->append("X", rankedCumulativeBasepairMappingForAllItems.at(i));
            else if (currentChromosomeNumber == 24)
                xAxis->append("X", rankedCumulativeBasepairMappingForAllItems.at(i));
            else
                xAxis->append(QString::number(chromosomeMappingsForAllSubsets.at(i - 1)), rankedCumulativeBasepairMappingForAllItems.at(i));

        }

    }

    xAxis->append(QString::number(chromosomeMappingsForAllSubsets.last()), rankedCumulativeBasepairMappingForAllItems.last());

    if (d_chartType == QChart::ChartTypePolar)
        static_cast<QPolarChart *>(chart)->addAxis(xAxis, QPolarChart::PolarOrientationAngular);
    else
        chart->addAxis(xAxis, Qt::AlignBottom);

    QList<QValueAxis *> yAxes;

    for (int i = 0; i < (d_seperateYAxisForEachSubset ? subsetIdentifiers.size() : 1); ++i) {

        yAxes.append(new QValueAxis());

        if (d_chartType == QChart::ChartTypePolar)
            static_cast<QPolarChart *>(chart)->addAxis(yAxes.at(i), QPolarChart::PolarOrientationRadial);
        else
            chart->addAxis(yAxes.at(i), Qt::AlignLeft);

    }

    if (values_yAxes.size() == 1)
        chart->legend()->setVisible(false);


    double yMinimumValue = std::numeric_limits<double>::max();

    double yMaximumValue = std::numeric_limits<double>::lowest();


    for (int i = 0; i < subsetIdentifiers.size(); ++i) {

        QScatterSeries *scatterSeries = new QScatterSeries;

        if (!subsetIdentifiers.at(i).isEmpty())
            scatterSeries->setName(subsetIdentifiers.at(i));

        const QVector<double> &current_basepairMappings_xAxis(basepairMappings_xAxes.at(i));

        const QVector<double> &current_values_yAxis(values_yAxes.at(i));


        if (d_seperateYAxisForEachSubset) {

            yMinimumValue = std::numeric_limits<double>::max();

            yMaximumValue = std::numeric_limits<double>::lowest();

        }

        for (int j = 0; j < current_basepairMappings_xAxis.size(); ++j) {

            double current_value_yAxis = current_values_yAxis.at(j);

            scatterSeries->append(current_basepairMappings_xAxis.at(j), current_value_yAxis);

            yMinimumValue = std::min(yMinimumValue, current_value_yAxis);

            yMaximumValue = std::max(yMaximumValue, current_value_yAxis);

        }

        chart->addSeries(scatterSeries);

        scatterSeries->attachAxis(xAxis);

        if (d_seperateYAxisForEachSubset) {

            scatterSeries->attachAxis(yAxes.at(i));

            yAxes.at(i)->setRange(yMinimumValue, yMaximumValue);

        } else
            scatterSeries->attachAxis(yAxes.first());

        scatterSeries->setMarkerSize(5);

        scatterSeries->setBorderColor(Qt::transparent);

        scatterSeries->setOpacity(0.5);

    }

    if (!d_seperateYAxisForEachSubset)
    yAxes.first()->setRange(yMinimumValue, yMaximumValue);

    for (int i = 0; i < yAxes.size(); ++i)
        yAxes.at(i)->applyNiceNumbers();

    d_chart = QSharedPointer<QChart>(chart);

    emit finishedInitializationOfChart();

}
