#ifndef COMBOBOXBUILDINGBLOCKWIDGET_H
#define COMBOBOXBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QStringList>
#include <QSettings>

#include "dialogs/basedialog.h"

namespace Ui {

class ComboBoxBuildingBlockWidget;

}

class ComboBoxBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit ComboBoxBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), const QStringList &itemsText = QStringList(), const QVariantList &itemsUserData = QVariantList());

    ~ComboBoxBuildingBlockWidget();

    QComboBox *pointerToInternalComboBox();

    QLabel *pointerToInternalLabel();

private:

    Ui::ComboBoxBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

};

#endif // COMBOBOXBUILDINGBLOCKWIDGET_H
