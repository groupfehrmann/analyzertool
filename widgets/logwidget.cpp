#include "logwidget.h"
#include "ui_logwidget.h"

LogWidget::LogWidget(QWidget *parent) :
    QWidget(parent), ui(new Ui::LogWidget)
{

    ui->setupUi(this);

    LogSortFilterProxyModel *logSortFilterProxyModel = new LogSortFilterProxyModel(ui->listView_log);

    logSortFilterProxyModel->setSourceModel(&d_logListModel);

    ui->listView_log->setModel(logSortFilterProxyModel);

    this->connect(ui->checkBox_messages, SIGNAL(toggled(bool)), &d_logListModel, SLOT(setShowMessage(bool)));

    this->connect(ui->checkBox_warnings, SIGNAL(toggled(bool)), &d_logListModel, SLOT(setShowWarning(bool)));

    this->connect(ui->checkBox_errors, SIGNAL(toggled(bool)), &d_logListModel, SLOT(setShowError(bool)));

    this->connect(ui->pushButton_clear, SIGNAL(clicked(bool)), &d_logListModel, SLOT(clear()));

    this->connect(ui->pushButton_export, SIGNAL(clicked(bool)), this, SLOT(exportLog()));

    QSettings settings;

    settings.beginGroup("logWidget");

    if (settings.contains("checkBox_messages/checked"))
        ui->checkBox_messages->setChecked(settings.value("checkBox_messages/checked").toBool());

    if (settings.contains("checkBox_warnings/checked"))
        ui->checkBox_warnings->setChecked(settings.value("checkBox_warnings/checked").toBool());

    if (settings.contains("checkBox_errors/checked"))
        ui->checkBox_errors->setChecked(settings.value("checkBox_errors/checked").toBool());

    d_logListModel.setShowMessage(ui->checkBox_messages->isChecked());

    d_logListModel.setShowWarning(ui->checkBox_warnings->isChecked());

    d_logListModel.setShowError(ui->checkBox_errors->isChecked());

    settings.endGroup();

}

LogWidget::~LogWidget()
{

    QSettings settings;

    settings.beginGroup("logWidget");

    settings.setValue("checkBox_messages/checked", ui->checkBox_messages->isChecked());

    settings.setValue("checkBox_warnings/checked", ui->checkBox_warnings->isChecked());

    settings.setValue("checkBox_errors/checked", ui->checkBox_errors->isChecked());

    settings.endGroup();

    delete ui;

}

void LogWidget::appendLogItem(LogListModel::Type type, const QString &text)
{

    if (ui->listView_log->verticalScrollBar()->value() == ui->listView_log->verticalScrollBar()->maximum()) {

        d_logListModel.appendLogItem(type, text);

        ui->listView_log->scrollToBottom();

    } else
        d_logListModel.appendLogItem(type, text);

}

void LogWidget::exportLog()
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - Select file");

    fileDialog.setAcceptMode(QFileDialog::AcceptSave);

    fileDialog.setDefaultSuffix("txt");

    QSettings settings;

    settings.beginGroup("logWidget");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    settings.endGroup();

    if (!fileDialog.exec())
        return;

    settings.beginGroup("fileDialog");

    settings.setValue("state", fileDialog.saveState());

    settings.setValue("geometry", fileDialog.saveGeometry());

    settings.setValue("lastDirectory", fileDialog.directory().path());

    settings.endGroup();

    QFile file(fileDialog.selectedFiles().first());

    if (fileDialog.selectedFiles().first().isEmpty() || !file.open(QIODevice::WriteOnly | QIODevice::Text))
        d_logListModel.appendLogItem(LogListModel::ERROR, "could not open file \"" + fileDialog.selectedFiles().first() + "\"");

    QTextStream out(&file);

    for (int i = 0; i < d_logListModel.rowCount(); ++i)
        out << d_logListModel.data(d_logListModel.index(i)).toString() << Qt::endl;

    file.close();

    d_logListModel.appendLogItem(LogListModel::MESSAGE, "exported log to \"" + fileDialog.selectedFiles().first() + "\"");

}
