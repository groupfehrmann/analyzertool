#ifndef CREATESCATTERPLOTSWITHGENOMICMAPPINGONXAXISHOMOSAPIENSWORKERCLASS_H
#define CREATESCATTERPLOTSWITHGENOMICMAPPINGONXAXISHOMOSAPIENSWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QAbstractSeries>
#include <QDir>

#include <functional>
#include <utility>
#include <algorithm>

#include "base/basedataset.h"
#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/exportplottofile.h"
#include "charts/scatterchart_genomicmappingonxaxis_homosapiens.h"
#include "base/convertfunctions.h"
#include "charts/openplotfromfile.h"
#include "charts/copychartparameters.h"

using namespace QtCharts;


class CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    static unsigned int d_numberOfBasepairsPerAutosomalChromosome[24];

    static unsigned int d_numberOfBasepairsPerAutosomalChromosome_cumulative[25];

    struct Parameters {

        QString annotationLabelDefiningAlternativeVariableIdentifier;

        QString annotationLabelDefiningBasepairPosition;

        QString annotationLabelDefiningChromosomePosition;

        QString annotationLabelDefiningSubsets;

        QChart::ChartType chartType;

        QSharedPointer<BaseDataset> baseDataset;

        int dpiOfPlot;

        QString exportDirectory;

        QString prefixStringToUseInPlotName;

        QString exportTypeOfPlot;

        int heightOfPlot;

        BaseMatrix::Orientation orientation;

        QString pathToTemplatePlot;

        QString plotMode;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        bool seperateYAxisForEachSubset;

        QStringList subsetIdentifiers;

        int widthOfPlot;

    };

    CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::Parameters d_parameters;

    QSharedPointer<QChart> d_templatePlot;

};

#endif // CREATESCATTERPLOTSWITHGENOMICMAPPINGONXAXISHOMOSAPIENSWORKERCLASS_H
