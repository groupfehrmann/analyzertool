#include "calculatedescriptivesdialog.h"

CalculateDescriptivesDialog::CalculateDescriptivesDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new CalculateDescriptivesWorkerClass::Parameters;

    this->setupWidgets();

}

CalculateDescriptivesDialog::~CalculateDescriptivesDialog()
{

   delete static_cast<CalculateDescriptivesWorkerClass::Parameters *>(d_parameters);

}

void CalculateDescriptivesDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 1, 1);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets", "Select annotation label defining item subsets (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets, 3, 0);

    d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers", "Select item subset identifiers (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers, 4, 0);

    d_itemsSelectorBuildingBlockWidget_descriptives = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_descriptives", "Select descriptives", {"mean", "mean of absolute values", "mean of squared values", "standard deviation", "variance", "median", "sum", "sum of absolute values", "sum of squared values", "minimum", "maximum", "minimum of absolute values", "maximum of absolute values", "one-sample wilcoxon signed rank - deviation from zero - -log10(p-value)", "count", "percentage", "coefficient of variation"});

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_descriptives, 4, 1);

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables", "Select annotation labels (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables, 5, 0);

    d_itemsSelectorBuildingBlockWidget_values = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_values", "Select values to be used for descriptive function");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_values, 5, 1);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory (optional)", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 6, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->connect(d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox(), SIGNAL(currentTextChanged(QString)), this, SLOT(annotationLabelDefiningItemSubsetsChanged(QString)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

    this->connect(d_itemsSelectorBuildingBlockWidget_descriptives, SIGNAL(selectionChanged(QStringList,QStringList)), this, SLOT(selectionOfDescriptivesChanged(QStringList,QStringList)));

    this->selectionOfDescriptivesChanged(d_itemsSelectorBuildingBlockWidget_descriptives->selectedItemsIdentifierAndNumber().first, QStringList());

}

void CalculateDescriptivesDialog::initializeParameters()
{

    CalculateDescriptivesWorkerClass::Parameters *parameters = static_cast<CalculateDescriptivesWorkerClass::Parameters *>(d_parameters);

    parameters->annotationLabelDefiningItemSubsets = d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelsDefiningAnnotationValuesForVariables = d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables->selectedItemsIdentifierAndNumber().first;

    parameters->baseDataset = d_baseDataset;

    if (d_valuesNeeded)
        parameters->selectedValues = d_itemsSelectorBuildingBlockWidget_values->selectedItemsIdentifierAndNumber().first;

    parameters->descriptives = d_itemsSelectorBuildingBlockWidget_descriptives->selectedItemsIdentifierAndNumber().first;

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->exportDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->itemSubsetIdentifiers = d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers->selectedItemsIdentifierAndNumber().first;

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

}

void CalculateDescriptivesDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningItemSubsets->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables->setItemsIdentifier(d_baseDataset->annotations(orientation).labels());

}

void CalculateDescriptivesDialog::annotationLabelDefiningItemSubsetsChanged(const QString &currentAnnotationLabel)
{

    if (currentAnnotationLabel.isEmpty()) {

        d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers->clear();

        return;

    }

    QStringList itemSubsetIdentifiers = d_baseDataset->uniqueAnnotationValues<QString>(currentAnnotationLabel, BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget->selectedOrientation()), d_selectedOnly).values();

    itemSubsetIdentifiers.removeOne(QString());

    std::sort(itemSubsetIdentifiers.begin(), itemSubsetIdentifiers.end());

    d_itemsSelectorBuildingBlockWidget_itemSubsetIdentifiers->setItemsIdentifier(itemSubsetIdentifiers);

}

void CalculateDescriptivesDialog::selectionOfDescriptivesChanged(const QStringList &selectedDescriptives, const QStringList &notSelectedDescriptives)
{

    Q_UNUSED(notSelectedDescriptives);

    d_itemsSelectorBuildingBlockWidget_values->setEnabled(false);

    d_valuesNeeded = false;

    for (const QString &descriptive : selectedDescriptives) {

        if ((descriptive == "count") || (descriptive == "percentage")) {

            d_itemsSelectorBuildingBlockWidget_values->setItemsIdentifier(d_baseDataset->baseMatrix()->uniqueValuesOfQString().values());

            d_itemsSelectorBuildingBlockWidget_values->setEnabled(true);

            d_valuesNeeded = true;

            break;

        }

    }

}
