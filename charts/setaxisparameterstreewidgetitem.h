#ifndef SETAXISPARAMETERSTREEWIDGETITEM_H
#define SETAXISPARAMETERSTREEWIDGETITEM_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QAbstractAxis>
#include <QValueAxis>
#include <QCategoryAxis>
#include <QDateTimeAxis>
#include <QLogValueAxis>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QPen>
#include <QBrush>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QPushButton>

#include "charts/setbrushparameterstreewidgetitem.h"
#include "charts/setpenparameterstreewidgetitem.h"
#include "charts/selectfontwidget.h"
#include "charts/selectcolorwidget.h"

using namespace QtCharts;

class SetAxisParametersTreeWidgetItem : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetAxisParametersTreeWidgetItem(QTreeWidget *parent = nullptr, const QString &label = "Axis", QAbstractAxis *axis = nullptr);

    explicit SetAxisParametersTreeWidgetItem(QTreeWidgetItem *parent = nullptr, const QString &label = "Axis", QAbstractAxis *axis = nullptr);

    ~SetAxisParametersTreeWidgetItem();

public slots:

    void setAxis(const QString &label, QAbstractAxis *axis);

private:

    void initialize(const QString &label);

    QAbstractAxis *d_axis;

    bool d_initialized;

    SetPenParametersTreeWidgetItem *d_setGridLinePenParametersTreeWidgetItem;

    QCheckBox *d_selectGridLineVisibleWidget;

    SetPenParametersTreeWidgetItem *d_setMinorGridLinePenParametersTreeWidgetItem;

    QCheckBox *d_selectMinorGridLineVisibleWidget;

    QSpinBox *d_selectLabelsAngleWidget;

    SetBrushParametersTreeWidgetItem *d_setLabelsBrushParametersTreeWidgetItem;

    SelectFontWidget *d_selectLabelsFontWidget;

    QCheckBox *d_selectLabelsVisibleWidget;

    SetPenParametersTreeWidgetItem *d_setLinePenParametersTreeWidgetItem;

    QCheckBox *d_selectLineVisibleWidget;

    SetBrushParametersTreeWidgetItem *d_setShadesBrushParametersTreeWidgetItem;

    SetPenParametersTreeWidgetItem *d_setShadesPenParametersTreeWidgetItem;

    QCheckBox *d_selectShadesVisibleWidget;

    QLineEdit *d_selectTitleWidget;

    SetBrushParametersTreeWidgetItem *d_setTitleBrushParametersTreeWidgetItem;

    SelectFontWidget *d_selectTitleFontWidget;

    QLineEdit *d_selectLabelFormatWidget;

    QDoubleSpinBox *d_selectMaxWidget;

    QDoubleSpinBox *d_selectMinWidget;

    QSpinBox *d_selectMinorTickCountWidget;

    QSpinBox *d_selectTickCountWidget;

    QComboBox *d_selectAxisLabelsPositionWidget;

    QDateTimeEdit *d_selectMaxDateWidget;

    QDateTimeEdit *d_selectMinDateWidget;

    QSpinBox *d_selectBaseWidget;

private slots:

    void SetGridLinePen(const QPen &pen);

    void setGridLineVisible(bool visible);

    void setMinorGridLinePen(const QPen &pen);

    void setMinorGridLineVisible(bool visible);

    void setLabelsAngle(int angle);

    void setLabelsBrush(const QBrush &brush);

    void setLabelsFont(const QFont &font);

    void setLabelsVisible(bool visible);

    void setLinePen(const QPen &pen);

    void setLineVisible(bool visible);

    void setShadesBrush(const QBrush &brush);

    void setShadesPen(const QPen &pen);

    void setShadesVisible(bool visible);

    void setTitleBrush(const QBrush &brush);

    void setTitleFont(const QFont &font);

    void setTitleText(const QString &title);

    void setTitleVisible(bool visible);

    void setLabelFormat(const QString &format);

    void setMax(qreal max);

    void setMin(qreal min);

    void setMinorTickCount(int count);

    void setTickCount(int count);

    void setLabelsPosition(int index);

    void setMax(const QDateTime &max);

    void setMin(const QDateTime &min);

    void setBase(int base);

};

#endif // SETAXISPARAMETERSTREEWIDGETITEM_H
