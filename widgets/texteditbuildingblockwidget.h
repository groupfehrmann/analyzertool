#ifndef TEXTEDITBUILDINGBLOCKWIDGET_H
#define TEXTEDITBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QLabel>
#include <QStringList>
#include <QSettings>

#include "dialogs/basedialog.h"

namespace Ui {

class TextEditBuildingBlockWidget;

}

class TextEditBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit TextEditBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), const QString &defaultText = QString());

    ~TextEditBuildingBlockWidget();

    QTextEdit *pointerToInternalTextEdit();

    QLabel *pointerToInternalLabel();

private:

    Ui::TextEditBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

};

#endif // TEXTEDITBUILDINGBLOCKWIDGET_H
