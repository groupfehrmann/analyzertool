#include "importdatamatrixfromsinglefiledialog.h"

ImportDatasetFromSingleFileDialog::ImportDatasetFromSingleFileDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings)
{

    d_parameters = new ImportDatasetFromSingleFileWorkerClass::Parameters;

    this->setupWidgets();

}

ImportDatasetFromSingleFileDialog::~ImportDatasetFromSingleFileDialog()
{

    delete static_cast<ImportDatasetFromSingleFileWorkerClass::Parameters *>(d_parameters);

}

void ImportDatasetFromSingleFileDialog::initializeWidgets()
{
    d_comboBoxBuildingBlockWidget_datasetDataType = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_datasetDataType", "Dataset data type", {"short", "unsigned short", "int", "unsigned int", "long long", "unsigned long long", "float", "double", "unsigned char", "string", "bool"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_datasetDataType, 0, 0);

    d_selectFileBuildingBlockWidget = new SelectFileBuildingBlockWidget(this, "selectFileBuildingBlockWidget", "Select file", QFileDialog::AcceptOpen, false);

    d_gridLayout->addWidget(d_selectFileBuildingBlockWidget, 1, 0);

    d_comboBoxBuildingBlockWidget_natureOfFirstLabelInHeader = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_natureOfFirstLabelInHeader", "Select if first label in header defines column containing row identifiers or first column containing data", {"Label defines column containing row identifiers", "Label defines first column containing data"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_natureOfFirstLabelInHeader, 2, 0);

    d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget = new SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget(this, "selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget, 3, 0);

    d_comboBoxBuildingBlockWidget_headerLine = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_headerLine", "Select header line");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_headerLine, 4, 0);

    d_comboBoxBuildingBlockWidget_firstDataLine = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_firstDataLine", "Select first data line");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_firstDataLine, 5, 0);

    d_comboBoxBuildingBlockWidget_lastDataLine = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_lastDataLine", "Select last data line");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_lastDataLine, 6, 0);

    d_selectDelimiterBuildingBlockWidget = new SelectDelimiterBuildingBlockWidget(this, "selectDelimiterBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectDelimiterBuildingBlockWidget, 7, 0);

    d_selectHeaderLabelsBuildingBlockWidget = new SelectHeaderLabelsBuildingBlockWidget(this, "selectHeaderLabelsBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectHeaderLabelsBuildingBlockWidget, 8, 0);

    this->connect(d_selectFileBuildingBlockWidget, SIGNAL(pathChanged(QString)), this, SLOT(pathChanged(QString)));

    this->connect(d_selectFileBuildingBlockWidget, SIGNAL(invalidPath()), d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox(), SLOT(clear()));

    this->connect(d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(headerLineChanged(int)));

    this->connect(d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(firstDataLineChanged(int)));

    this->connect(d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(lastDataLineChanged(int)));

    this->connect(d_selectDelimiterBuildingBlockWidget, SIGNAL(delimiterChanged()), this, SLOT(updateHeaderLabelsWidget()));

    this->connect(d_comboBoxBuildingBlockWidget_natureOfFirstLabelInHeader->pointerToInternalComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(natureOfFirstLabelInHeaderChanged(int)));

}

void ImportDatasetFromSingleFileDialog::firstDataLineChanged(int index)
{

    if (index == -1) {

        d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->clear();

        d_lineNumberOfFirstDataLine = -1;

        return;

    }

    d_lineNumberOfFirstDataLine = d_lineNumberOfHeaderLine + index + 1;

    d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->clear();

    int lineNumberStart = d_lineNumberOfFirstDataLine - d_offSetLineNumber;

    if (lineNumberStart + 1 == d_listOfLineInTail.size())
        d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInTail.mid(d_lineNumberOfFirstDataLine - 1, 1));
    else
        d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInTail.mid(((lineNumberStart <= 0) ? 0 : lineNumberStart + 1)));

    d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->setCurrentIndex(d_comboBoxBuildingBlockWidget_lastDataLine->pointerToInternalComboBox()->count() - 1);

}

void ImportDatasetFromSingleFileDialog::headerLineChanged(int index)
{

    if (index == -1) {

        d_lineNumberOfHeaderLine = -1;

        d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox()->clear();

        d_selectHeaderLabelsBuildingBlockWidget->headerLineChanged(QStringList());

        return;

    }

    d_lineNumberOfHeaderLine = index + 1;

    d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_firstDataLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInHead.mid(index + 1));

    this->updateHeaderLabelsWidget();

}

void ImportDatasetFromSingleFileDialog::initializeParameters()
{

    ImportDatasetFromSingleFileWorkerClass::Parameters *parameters = static_cast<ImportDatasetFromSingleFileWorkerClass::Parameters *>(d_parameters);

    parameters->datasetDataType = d_comboBoxBuildingBlockWidget_datasetDataType->pointerToInternalComboBox()->currentText();

    parameters->pathToFile = d_selectFileBuildingBlockWidget->pointerToInternalLineEdit()->text();

    if (d_comboBoxBuildingBlockWidget_natureOfFirstLabelInHeader->pointerToInternalComboBox()->currentIndex() == 0)
        parameters->firstLabelInHeaderMode = ImportDatasetFromSingleFileWorkerClass::REPRESENTCOLUMNWITHROWIDENTIFIERS;
    else
        parameters->firstLabelInHeaderMode = ImportDatasetFromSingleFileWorkerClass::REPRESENTFIRSTCOLUMNWITHDATA;

    parameters->lineNumberHeaderLine = d_lineNumberOfHeaderLine;

    parameters->lineNumberFirstDataLine = d_lineNumberOfFirstDataLine;

    parameters->lineNumberLastDataLine = d_lineNumberOfLastDataLine;

    parameters->delimiterType = static_cast<SelectDelimiterBuildingBlockWidget::DelimiterType>(d_selectDelimiterBuildingBlockWidget->pointerToInternalComboBox()->currentIndex());

    parameters->custumDelimiter = d_selectDelimiterBuildingBlockWidget->delimiter();

    parameters->caseSensitivityForCustumDelimiter = d_selectDelimiterBuildingBlockWidget->pointerToInternalCheckBox()->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive;

    parameters->indexAndLabelForColumnDefiningRowIdentifiers = d_selectHeaderLabelsBuildingBlockWidget->indexAndLabelForColumnDefiningRowIdentifiers();

    parameters->indexAndLabelForColumnDefiningFirstDataColumn = d_selectHeaderLabelsBuildingBlockWidget->indexAndLabelForColumnDefiningFirstDataColumn();

    parameters->indexAndLabelForColumnDefiningSecondDataColumn = d_selectHeaderLabelsBuildingBlockWidget->indexAndLabelForColumnDefiningSecondDataColumn();

}

void ImportDatasetFromSingleFileDialog::lastDataLineChanged(int index)
{

    if (index == -1)
        return;

    int lineNumberStart = d_lineNumberOfFirstDataLine - d_offSetLineNumber;

    if (lineNumberStart + 1 == d_listOfLineInTail.size())
        d_lineNumberOfLastDataLine = d_lineNumberOfFirstDataLine;
    else
        d_lineNumberOfLastDataLine = (lineNumberStart <= 0) ? d_offSetLineNumber + index : d_lineNumberOfFirstDataLine + index + 1;

}

void ImportDatasetFromSingleFileDialog::pathChanged(const QString &path)
{

    QFile file(path);

    if (path.isEmpty() || !file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->clear();

        return;

    }

    int numberOfLinesToReadFromHeadOfFile = d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget->pointerToInternalSpinBoxNumberOfLinesFromHeadOfFile()->value();

    int numberOfLinesToReadFromTailOfFile = d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget->pointerToInternalSpinBoxNumberOfLinesFromTailOfFile()->value();

    int lineCounter = 0;

    QList<qint64> filePositions;

    while (!file.atEnd()) {

        if (++lineCounter > numberOfLinesToReadFromHeadOfFile)
            break;

        filePositions << file.pos();

        d_listOfLineInHead << QString(file.readLine()).remove(QRegExp("[\r\n]"));

        if (d_listOfLineInHead.last().size() > 150)
            d_listOfTrimmedLineInHead << d_listOfLineInHead.last().left(75).simplified() + " ...]----[... " + d_listOfLineInHead.last().right(75).simplified();
        else
            d_listOfTrimmedLineInHead << d_listOfLineInHead.last().simplified();

    }

    while (!file.atEnd()) {

        filePositions << file.pos();

        file.readLine();

    }

    d_offSetLineNumber = filePositions.size() - numberOfLinesToReadFromTailOfFile + 1;

    if (d_offSetLineNumber < 1)
        d_offSetLineNumber = 1;

    file.seek(filePositions[d_offSetLineNumber - 1]);

    while (!file.atEnd()) {

        d_listOfLineInTail << QString(file.readLine()).remove(QRegExp("[\r\n]"));

        if (d_listOfLineInTail.last().size() > 150)
            d_listOfTrimmedLineInTail << d_listOfLineInTail.last().left(75).simplified() + " ...]----[... " + d_listOfLineInTail.last().right(75).simplified();
        else
            d_listOfTrimmedLineInTail << d_listOfLineInTail.last().simplified();

    }

    file.close();

    d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->addItems(d_listOfTrimmedLineInHead);

}

void ImportDatasetFromSingleFileDialog::updateHeaderLabelsWidget()
{

    if (d_listOfLineInHead.isEmpty())
        return;

    QStringList dummy;

    if (d_comboBoxBuildingBlockWidget_natureOfFirstLabelInHeader->pointerToInternalComboBox()->currentIndex() == 1)
        dummy << "[DUMMY]";

    if (d_selectDelimiterBuildingBlockWidget->pointerToInternalComboBox()->currentIndex() == SelectDelimiterBuildingBlockWidget::CUSTUM)
        d_selectHeaderLabelsBuildingBlockWidget->headerLineChanged(dummy << d_listOfLineInHead.at(d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->currentIndex()).split(d_selectDelimiterBuildingBlockWidget->delimiter(), Qt::KeepEmptyParts, (d_selectDelimiterBuildingBlockWidget->pointerToInternalCheckBox()->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive)));
    else
        d_selectHeaderLabelsBuildingBlockWidget->headerLineChanged(dummy << d_listOfLineInHead.at(d_comboBoxBuildingBlockWidget_headerLine->pointerToInternalComboBox()->currentIndex()).split(QRegExp(d_selectDelimiterBuildingBlockWidget->delimiter()), Qt::KeepEmptyParts));

}

void ImportDatasetFromSingleFileDialog::natureOfFirstLabelInHeaderChanged(int index)
{

    Q_UNUSED(index);

    this->updateHeaderLabelsWidget();

}
