#include "selectbrushstylewidget.h"
#include "ui_selectbrushstylewidget.h"

SelectBrushStyleWidget::SelectBrushStyleWidget(QWidget *parent, Qt::BrushStyle defaultBrushStyle) :
    QWidget(parent), ui(new Ui::SelectBrushStyleWidget)
{

    ui->setupUi(this);

    ui->comboBox->addItem(this->createPixmap(Qt::NoBrush), "No brush pattern", static_cast<int>(Qt::NoBrush));

    ui->comboBox->addItem(this->createPixmap(Qt::SolidPattern), "Uniform color", static_cast<int>(Qt::SolidPattern));

    ui->comboBox->addItem(this->createPixmap(Qt::Dense1Pattern), "Extremely dense brush pattern", static_cast<int>(Qt::Dense1Pattern));

    ui->comboBox->addItem(this->createPixmap(Qt::Dense2Pattern), "Very dense brush pattern", static_cast<int>(Qt::Dense2Pattern));

    ui->comboBox->addItem(this->createPixmap(Qt::Dense3Pattern), "Somewhat dense brush pattern", static_cast<int>(Qt::Dense3Pattern));

    ui->comboBox->addItem(this->createPixmap(Qt::Dense4Pattern), "Half dense brush pattern", static_cast<int>(Qt::Dense4Pattern));

    ui->comboBox->addItem(this->createPixmap(Qt::Dense5Pattern), "Somewhat sparse brush pattern", static_cast<int>(Qt::Dense5Pattern));

    ui->comboBox->addItem(this->createPixmap(Qt::Dense6Pattern), "Very sparse brush pattern", static_cast<int>(Qt::Dense6Pattern));

    ui->comboBox->addItem(this->createPixmap(Qt::Dense7Pattern), "Extremely sparse brush pattern", static_cast<int>(Qt::Dense7Pattern));

    ui->comboBox->addItem(this->createPixmap(Qt::HorPattern), "Horizontal lines", static_cast<int>(Qt::HorPattern));

    ui->comboBox->addItem(this->createPixmap(Qt::VerPattern), "Vertical lines", static_cast<int>(Qt::VerPattern));

    ui->comboBox->addItem(this->createPixmap(Qt::CrossPattern), "Crossing horizontal and vertical lines", static_cast<int>(Qt::CrossPattern));

    ui->comboBox->addItem(this->createPixmap(Qt::BDiagPattern), "Backward diagonal lines", static_cast<int>(Qt::BDiagPattern));

    ui->comboBox->addItem(this->createPixmap(Qt::FDiagPattern), "Forward diagonal lines", static_cast<int>(Qt::FDiagPattern));

    ui->comboBox->addItem(this->createPixmap(Qt::DiagCrossPattern), "Crossing diagonal lines", static_cast<int>(Qt::DiagCrossPattern));

    this->connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexOfComboBoxChanged(int)));

    this->setCurrentBrushStyle(defaultBrushStyle);

}

SelectBrushStyleWidget::~SelectBrushStyleWidget()
{

    delete ui;

}

QPixmap SelectBrushStyleWidget::createPixmap(Qt::BrushStyle brushStyle)
{

    QPixmap pixmap(150, 16);

    pixmap.fill(Qt::transparent);

    QPainter painter(&pixmap);

    painter.setBrush(QBrush(Qt::black, brushStyle));

    painter.drawRect(QRect(QPoint(0, 0), QPoint(150, 16)));

    return pixmap;

}

void SelectBrushStyleWidget::currentIndexOfComboBoxChanged(int index)
{

    Q_UNUSED(index);

    emit currentBrushStyleChanged(this->currentBrushStyle());

}

Qt::BrushStyle SelectBrushStyleWidget::currentBrushStyle()
{

    return static_cast<Qt::BrushStyle>(ui->comboBox->currentData().toInt());

}

void SelectBrushStyleWidget::setCurrentBrushStyle(Qt::BrushStyle brushStyle)
{

    switch (brushStyle) {

        case Qt::NoBrush: ui->comboBox->setCurrentIndex(0); break;

        case Qt::SolidPattern: ui->comboBox->setCurrentIndex(1); break;

        case Qt::Dense1Pattern: ui->comboBox->setCurrentIndex(2); break;

        case Qt::Dense2Pattern: ui->comboBox->setCurrentIndex(3); break;

        case Qt::Dense3Pattern: ui->comboBox->setCurrentIndex(4); break;

        case Qt::Dense4Pattern: ui->comboBox->setCurrentIndex(5); break;

        case Qt::Dense5Pattern: ui->comboBox->setCurrentIndex(6); break;

        case Qt::Dense6Pattern: ui->comboBox->setCurrentIndex(7); break;

        case Qt::Dense7Pattern: ui->comboBox->setCurrentIndex(8); break;

        case Qt::HorPattern: ui->comboBox->setCurrentIndex(9); break;

        case Qt::VerPattern: ui->comboBox->setCurrentIndex(10); break;

        case Qt::CrossPattern: ui->comboBox->setCurrentIndex(11); break;

        case Qt::BDiagPattern: ui->comboBox->setCurrentIndex(12); break;

        case Qt::FDiagPattern: ui->comboBox->setCurrentIndex(13); break;

        case Qt::DiagCrossPattern: ui->comboBox->setCurrentIndex(14); break;

        default : ui->comboBox->setCurrentIndex(-1);

    }

}
