#include "fastdistancecorrelation.h"

double FastDistanceCorrelation::fastDistanceCorrelation(const QVector<double> &x, const QVector<double> &y, bool unbiased)
{

    QVector<double> parameters = FastDistanceCorrelation::fastDistanceCovariance(x, y, unbiased, true);

    double xDistanceVariance = parameters.at(1);

    double yDistanceVariance = parameters.at(2);

    return std::sqrt(parameters.at(0) / std::sqrt(xDistanceVariance * yDistanceVariance));

}

QVector<double> FastDistanceCorrelation::fastDistanceCovariance(const QVector<double> &x, const QVector<double> &y, bool unbiased, bool allParameters)
{

    if (x.size() != y.size()) {

        if (allParameters)
            return QVector<double>(3, std::numeric_limits<double>::quiet_NaN());
        else
            return QVector<double>(1, std::numeric_limits<double>::quiet_NaN());

    }

    QVector<double> sums = FastDistanceCorrelation::fastDistanceCovarianceSums(x, y, allParameters);

    double n = static_cast<double>(x.size());

    double d1;

    double d2;

    double d3;

    if (unbiased) {

        d1 = n * (n - 3.0);

        d2 = d1 * (n - 2.0);

        d3 = d2 * (n - 1.0);

    } else {

        d1 = n * n;

        d2 = n * n * n;

        d3 = n * n * n *n;

    }

    if (allParameters) {

        QVector<double> results(3);

        results[0] = (sums.at(0) / d1) - (2.0 * sums.at(1) / d2) + (sums.at(2) / d3);

        results[1] = (sums.at(3) / d1) - (2.0 * sums.at(5) / d2) + (sums.at(7) / d3);

        results[2] = (sums.at(4) / d1) - (2.0 * sums.at(6) / d2) + (sums.at(8) / d3);

        return results;

    } else {

        QVector<double> results(1);

        results[0] = (sums.at(0) / d1) - (2.0 * sums.at(1) / d2) + (sums.at(2) / d3);

        return results;

    }
}

QVector<double> FastDistanceCorrelation::fastDistanceCovarianceSums(const QVector<double> &x, const QVector<double> &y, bool allSums)
{

    if (x.size() != y.size()) {

        if (allSums)
            return QVector<double>(9, std::numeric_limits<double>::quiet_NaN());
        else
            return QVector<double>(3, std::numeric_limits<double>::quiet_NaN());

    }

    QVector<std::tuple<double, int, int, double> > xSortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(x);

    QVector<std::tuple<double, int, int, double> > ySortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(y);

    QVector<double> x1(x.size());

    QVector<double> y1(y.size());

    x1[0] = std::get<0>(xSortedOrderedRanked.at(0));

    y1[0] = y.at(std::get<1>(xSortedOrderedRanked.at(0)));

    for (int i = 1; i < x.size(); ++i) {

        const std::tuple<double, int, int, double> &tuple(xSortedOrderedRanked.at(i));

        x1[i] = std::get<0>(tuple);

        y1[i] = y.at(std::get<1>(tuple));

    }

    QVector<double> adot(x.size());

    QVector<double> bdot(x.size());

    double adotdot = 0.0;

    double bdotdot = 0.0;

    double sum2 = 0.0;

    for (int i = 0; i < x.size(); ++i) {

        int xalphai = std::get<2>(xSortedOrderedRanked.at(i)) - 1;

        int yalphai = std::get<2>(ySortedOrderedRanked.at(i)) - 1;


        const std::tuple<double, int, int, double> &xTuple(xSortedOrderedRanked.at(xalphai));

        const std::tuple<double, int, int, double> &yTuple(ySortedOrderedRanked.at(yalphai));


        double adoti = std::get<3>(xSortedOrderedRanked.back()) + (2.0 * static_cast<double>(xalphai) - static_cast<double>(x.size())) * x.at(i) - 2.0 * (std::get<3>(xTuple) - std::get<0>(xTuple));

        double bdoti = std::get<3>(ySortedOrderedRanked.back()) + (2.0 * static_cast<double>(yalphai) - static_cast<double>(y.size())) * y.at(i) - 2.0 * (std::get<3>(yTuple) - std::get<0>(yTuple));

        adot[i] = adoti;

        bdot[i] = bdoti;

        sum2 += adoti * bdoti;

        adotdot += adoti;

        bdotdot += bdoti;

    }

    double sum3 = adotdot * bdotdot;

    QVector<std::tuple<double, int, int, double> > y1SortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(y1);

    QVector<double> gamma_1 = FastDistanceCorrelation::partialSum2D(x1, y1, QVector<double>(x.size(), 1.0), xSortedOrderedRanked, y1SortedOrderedRanked);

    QVector<double> gamma_x = FastDistanceCorrelation::partialSum2D(x1, y1, x1, xSortedOrderedRanked, y1SortedOrderedRanked);

    QVector<double> gamma_y = FastDistanceCorrelation::partialSum2D(x1, y1, y1, xSortedOrderedRanked, y1SortedOrderedRanked);

    QVector<double> x1y1(x.size());

    for (int i = 0; i < x1y1.size(); ++i)
        x1y1[i] = x1.at(i) * y1.at(i);

    QVector<double> gamma_xy = FastDistanceCorrelation::partialSum2D(x1, y1, x1y1, xSortedOrderedRanked, y1SortedOrderedRanked);

    double sum1 = 0.0;

    for (int i = 0; i < x.size(); ++i)
        sum1 += (x.at(i) * y.at(i) * gamma_1.at(i)) + gamma_xy.at(i) - (x.at(i) * gamma_y.at(i)) - (y.at(i) * gamma_x.at(i));

    QVector<double> sums(9, std::numeric_limits<double>::quiet_NaN());

    sums[0] = sum1;

    sums[1] = sum2;

    sums[2] = sum3;

    if (allSums) {

        double n = static_cast<double>(x.size());

        sums[3] = 2 * n * (n - 1) * MathDescriptives::variance(x);

        sums[4] = 2 * n * (n - 1) * MathDescriptives::variance(y);

        double sum1 = 0.0;

        double sum2 = 0.0;

        for (int i = 0; i < x.size(); ++i) {

            sum1 += std::pow(adot.at(i), 2);

            sum2 += std::pow(bdot.at(i), 2);

        }

        sums[5] = sum1;

        sums[6] = sum2;

        sums[7] = std::pow(adotdot, 2.0);

        sums[8] = std::pow(bdotdot, 2.0);

    }

    return sums;

}

QVector<double> FastDistanceCorrelation::partialSum2D(const QVector<double> &x, const QVector<double> &y, const QVector<double> &c, const QVector<std::tuple<double, int, int, double> > &x_orderStatistics_orderIndices_rank_cummulativeSum, const QVector<std::tuple<double, int, int, double> > &y_orderStatistics_orderIndices_rank_cummulativeSum)
{

    QVector<double> xPartialSums(x.size(), 0.0);

    QVector<double> yPartialSums(y.size(), 0.0);

    QVector<double> yRanked(y.size());

    yRanked[0] = std::get<2>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(0));

    double cdot = c.at(0);

    for (int i = 1; i < c.size(); ++i) {

        cdot += c.at(i);

        xPartialSums[i] = xPartialSums.at(i - 1) + c.at(i - 1);

        yPartialSums[i] = yPartialSums.at(i - 1) + c.at( std::get<1>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(i - 1)));

        yRanked[i] = std::get<2>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(i));

    }

    QVector<double> binaryTreeSums = FastDistanceCorrelation::binaryTreeSums(yRanked, c);

    QVector<double> gamma(x.size());

    for (int i = 0; i < x.size(); ++i) {

        int xI = std::get<2>(x_orderStatistics_orderIndices_rank_cummulativeSum.at(i)) - 1;

        gamma[i] = cdot - c.at(xI) - 2.0 * yPartialSums.at(std::get<2>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(xI)) - 1) - 2.0 * xPartialSums.at(xI) + 4.0 * binaryTreeSums.at(xI);

    }

    return gamma;

}

QVector<double> FastDistanceCorrelation::binaryTreeSums(const QVector<double> &y, const QVector<double> &c)
{

    int n = y.size();

    int L = static_cast<int>(std::ceil(std::log2(static_cast<double>(n))));

    QVector<int> powersOfTwo(L + 2);

    for (int i = 0; i <= L + 1; ++i)
        powersOfTwo[i] = static_cast<int>(std::pow(2.0, static_cast<double>(i)));

    QVector<double> s(powersOfTwo.at(L + 1), 0.0);

    QVector<double> binaryTreeSums(n, 0.0);

    for (int i = 1; i < n; ++i) {

        double y_iminusone = y.at(i - 1);

        double c_iminusone = c.at(i - 1);

        int pos = static_cast<int>(std::ceil(y_iminusone));

        s[pos - 1] += c_iminusone;

        for (int l = 1; l < L; ++l) {

            pos = static_cast<int>(std::ceil(y_iminusone / powersOfTwo.at(l)));

            int scale = l;

            while (scale != 0) {

                --scale;

                pos += powersOfTwo.at(L - scale);

            }

            s[pos - 1] += c_iminusone;

        }

        double &binaryTreeSums_i(binaryTreeSums[i]);

        double y_i_minusone = y.at(i) - 1.0;

        pos = static_cast<int>(y_i_minusone);

        if ((static_cast<double>(pos) / 2.0) > static_cast<int>(pos / 2))
            binaryTreeSums_i += s.at(pos - 1);

        for (int l = 1; l < L; ++l) {

            pos = static_cast<int>(y_i_minusone / powersOfTwo.at(l));

            if ((static_cast<double>(pos) / 2.0) > static_cast<int>(pos / 2)) {

                int scale = l;

                while (scale != 0) {

                    --scale;

                    pos += powersOfTwo.at(L - scale);

                }

                binaryTreeSums_i += s.at(pos - 1);

           }

        }

    }

    return binaryTreeSums;

}

bool FastDistanceCorrelation::sortAscendingOnFirstElementOfTupleAndThenOnSecondElementOfTuple(const std::tuple<double, int, int, double> &element1, const std::tuple<double, int, int, double> &element2)
{

    if (std::get<0>(element1) < std::get<0>(element2))
        return true;

    if (std::get<0>(element1) == std::get<0>(element2))
        return std::get<1>(element1) < std::get<1>(element2);
    else
        return false;

}

QVector<std::tuple<double, int, int, double> > FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(const QVector<double> vector)
{

    QVector<std::tuple<double, int, int, double> > vector_orderStatistics_orderIndices_rank_cummulativeSum;

    vector_orderStatistics_orderIndices_rank_cummulativeSum.reserve(vector.size());

    for (int i = 0; i < vector.size(); ++i) {

        const double &value(vector.at(i));

        vector_orderStatistics_orderIndices_rank_cummulativeSum.push_back({value, i, 1, value});

    }

    std::sort(vector_orderStatistics_orderIndices_rank_cummulativeSum.begin(), vector_orderStatistics_orderIndices_rank_cummulativeSum.end(), FastDistanceCorrelation::sortAscendingOnFirstElementOfTupleAndThenOnSecondElementOfTuple);

    std::get<2>(vector_orderStatistics_orderIndices_rank_cummulativeSum[std::get<1>(vector_orderStatistics_orderIndices_rank_cummulativeSum.first())]) = 1;

    for (int i = 1; i < vector_orderStatistics_orderIndices_rank_cummulativeSum.size(); ++i) {

        std::tuple<double, int, int, double> &tuple(vector_orderStatistics_orderIndices_rank_cummulativeSum[i]);

        std::get<2>(vector_orderStatistics_orderIndices_rank_cummulativeSum[std::get<1>(tuple)]) = i + 1;

        std::get<3>(tuple) = std::get<3>(vector_orderStatistics_orderIndices_rank_cummulativeSum.at(i - 1)) + std::get<0>(tuple);

    }

    return vector_orderStatistics_orderIndices_rank_cummulativeSum;

}
