#include "principalcomponentanalysisworkerclass.h"

PrincipalComponentAnalysisWorkerClass::PrincipalComponentAnalysisWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<PrincipalComponentAnalysisWorkerClass::Parameters *>(parameters))
{

}

bool PrincipalComponentAnalysisWorkerClass::checkInputParameters()
{

    if (!d_parameters.outputDirectory.isEmpty() && !QDir(d_parameters.outputDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.outputDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected");

        return false;

    }

    return true;

}

void PrincipalComponentAnalysisWorkerClass::doWork()
{

    emit processStarted("principal component analysis");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.outputDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Principal component analysis"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    if (d_parameters.mode == COVARIANCE)
        emit appendLogItem(LogListModel::PARAMETER, "mode: covariance");
    else if (d_parameters.mode == CORRELATION)
        emit appendLogItem(LogListModel::PARAMETER, "mode: correlation");
    else if (d_parameters.mode == DISTANCECOVARIANCE)
        emit appendLogItem(LogListModel::PARAMETER, "mode: distance covariance");
    else if (d_parameters.mode == DISTANCECORRELATION)
        emit appendLogItem(LogListModel::PARAMETER, "mode: distance correlation");


    emit appendLogItem(LogListModel::PARAMETER, "number of components to extract: " + QString::number(d_parameters.numberOfComponentsToExtract));

    if (!d_parameters.outputDirectory.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.outputDirectory + "\"");

    }

    // Fetching data
    ////////////////

    emit startProgress(0, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    QVector<QVector<double> > matrix;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        matrix << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    if ((d_parameters.mode == COVARIANCE) || (d_parameters.mode == CORRELATION)) {

        // Centering data
        ////////////////

        QFuture<void> future1 = QtConcurrent::map(matrix, VectorOperations::centerToMeanOfZero_inplace);

        this->monitorFutureAndWaitForFinish(future1, 0, "Centering data");

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        // Scaling data (only for correlation mode)
        ////////////////

        if (d_parameters.mode == CORRELATION) {

            QFuture<void> future2 = QtConcurrent::map(matrix, VectorOperations::normalizeVector_inplace);

            this->monitorFutureAndWaitForFinish(future2, 0, "Scaling data with l2norm");

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

        }

    }

    // Calculating covariance, correlation, distance covariance or distance correlation matrix
    ////////////////

    if (d_parameters.mode == COVARIANCE)
        emit startProgress(0, "Calculating covariance matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);
    else if (d_parameters.mode == CORRELATION)
        emit startProgress(0, "Calculating correlation matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);
    else if (d_parameters.mode == DISTANCECOVARIANCE)
        emit startProgress(0, "Calculating distance covariance matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);
    else if (d_parameters.mode == DISTANCECORRELATION)
        emit startProgress(0, "Calculating distance correlation matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);

    double progressValue = 0.0;

    this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(0, progressValue);}, Qt::DirectConnection);

    d_timerThread.start();

    if (d_parameters.mode == COVARIANCE) {

        auto dotProduct_scaledBySizeOfVectorsMinusOne = [] (const QVector<double> &vector1, const QVector<double> &vector2) {

            return VectorOperations::dotProduct(vector1, vector2) / static_cast<long double>(vector1.size() - 1.0);

        };

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, dotProduct_scaledBySizeOfVectorsMinusOne, progressValue);

    } else if (d_parameters.mode == CORRELATION)
        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, VectorOperations::dotProduct, progressValue);
    else if (d_parameters.mode == DISTANCECOVARIANCE) {

        auto dCovFunc = [](const QVector<double> &vector1, const QVector<double> &vector2) {return std::sqrt(FastDistanceCorrelation::fastDistanceCovariance(vector1, vector2, false, false).first());};

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, dCovFunc, progressValue);

    } else if (d_parameters.mode == DISTANCECORRELATION) {

        auto dCorFunc = [](const QVector<double> &vector1, const QVector<double> &vector2) {return FastDistanceCorrelation::fastDistanceCorrelation(vector1, vector2, false);};

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, dCorFunc, progressValue);

    }

    d_timerThread.quit();

    this->disconnect(d_timer, nullptr, nullptr, nullptr);

    emit stopProgress(0);

    double totalExplainedVariance = 0.0;

    if ((d_parameters.mode == COVARIANCE) || (d_parameters.mode == DISTANCECOVARIANCE)){

        for (int i = 0; i < matrix.size(); ++i)
            totalExplainedVariance += matrix.at(i).at(i);

    } else
        totalExplainedVariance = matrix.size();

    if (d_parameters.mode == COVARIANCE) {

        emit startProgress(0, "Exporting covariance matrix", 0, 0);

        if (!this->processTableResult("Covariance matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
            return;

    } else if (d_parameters.mode == CORRELATION) {

        emit startProgress(0, "Exporting correlation matrix", 0, 0);

        if (!this->processTableResult("Correlation matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
            return;

    } else if (d_parameters.mode == DISTANCECOVARIANCE) {

        emit startProgress(0, "Exporting distance covariance matrix", 0, 0);

        if (!this->processTableResult("Distance covariance matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
            return;

    }  else if (d_parameters.mode == DISTANCECORRELATION) {

        emit startProgress(0, "Exporting distance correlation matrix", 0, 0);

        if (!this->processTableResult("Distance correlation matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
            return;

    }

    emit stopProgress(0);

    // Performing householder transformation of covariance, correlation, distance covariance or distance correlation matrix
    ////////////////

    emit startProgress(0, "Performing Householder transformation", 0, (1.0 / 6.0) * (7.0 * double(matrix.size()) * double(matrix.size()) * double(matrix.size()) - 918.0 * double(matrix.size()) * double(matrix.size()) + 93605.0 * double(matrix.size()) - 182388.0));

    QVector<double> d;

    QVector<double> e;

    this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(0, progressValue);}, Qt::DirectConnection);

    d_timerThread.start();

    MatrixOperations::tred2_multiThreaded(matrix, d, e, progressValue);

    d_timerThread.quit();

    this->disconnect(d_timer, nullptr, nullptr, nullptr);

    emit stopProgress(0);

    // Calculating eigenvalues and eigenvector of real tridiagonal matrix
    ////////////////

    emit startProgress(0, "Calculating eigenvalues and eigenvectors of real tridiagonal matrix", 0, 0);

    QVector<double> eigenvalues;

    int info = MatrixOperations::eigenvaluesAndEigenVectorsOfRealTridiagonalMatrix(d, e, d_parameters.numberOfComponentsToExtract, eigenvalues, matrix);

    if (info < 0)
        emit appendLogItem(LogListModel::ERROR, "DSTEGR: " + QString::number(info) + "th parameter had illegal value");
    else if (info == 0)
        emit appendLogItem(LogListModel::MESSAGE, "DSTEGR: Algorithm succeeded");
    else if (info == 1)
        emit appendLogItem(LogListModel::ERROR, "DSTEGR: Algorithm failed to converge");
    else if (info == 2)
        emit appendLogItem(LogListModel::ERROR, "DSTEGR: Inverse iteration failed to converge");

    emit stopProgress(0);

    QVector<QVector<QVariant> > eigenvalueSummary;

    double sumExplainedVariance = 0.0;

    QStringList componentLabels;

    int count = 0;

    for (const double &eigenvalue : eigenvalues) {

        eigenvalueSummary << QVector<QVariant> {eigenvalue, (std::fabs(eigenvalue) / totalExplainedVariance) * 100.0, ((sumExplainedVariance += std::fabs(eigenvalue)) / totalExplainedVariance) * 100.0};

        componentLabels << "Component " + QString::number(++count);

    }

    emit startProgress(0, "Exporting eigenvalue summary", 0, 0);

    if (!this->processTableResult("Eigenvalue summary", {"Eigenvalue", "Explained variance", "Cumulative explained variance"}, componentLabels, eigenvalueSummary, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);

    emit startProgress(0, "Exporting eigenvectors", 0, 0);

    if (!this->processTableResult("Eigenvectors", componentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);

    // Fetching data
    ////////////////

    emit startProgress(0, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    QVector<QVector<double> > principalComponentScores;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        principalComponentScores << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    if ((d_parameters.mode == COVARIANCE) || (d_parameters.mode == CORRELATION) || (d_parameters.mode == DISTANCECOVARIANCE) || (d_parameters.mode == DISTANCECORRELATION)) {

        // Centering data
        ////////////////
/*
        emit startProgress(0, "Centering data", 0, matrix.size());

        QFutureWatcher<void> futureWatcher;

        this->connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, [=](int progressValue){ emit updateProgress(0, progressValue);});

        futureWatcher.setFuture(QtConcurrent::map(principalComponentScores, VectorOperations::centerToMeanOfZero_inplace));

        futureWatcher.waitForFinished();

        emit stopProgress(0);
*/

        QFuture<void> future2 = QtConcurrent::map(principalComponentScores, VectorOperations::centerToMeanOfZero_inplace);

        this->monitorFutureAndWaitForFinish(future2, 0, "Centering data");

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        // Scaling data (only for correlation mode)
        ////////////////

        if ((d_parameters.mode == CORRELATION) || (d_parameters.mode == DISTANCECORRELATION)) {
/*
            emit startProgress(0, "Scaling data", 0, matrix.size());

            futureWatcher.setFuture(QtConcurrent::map(principalComponentScores, VectorOperations::scaleVectorToStandardDeviation_inplace));

            futureWatcher.waitForFinished();

            emit stopProgress(0);
*/
            QFuture<void> future3 = QtConcurrent::map(principalComponentScores, VectorOperations::scaleVectorToStandardDeviation_inplace);

            this->monitorFutureAndWaitForFinish(future2, 0, "Scaling data");

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

        }

    }

    // Calculating principal component scores
    ////////////////

    emit startProgress(0, "Calculating principal component scores", 0, 0);

    MatrixOperations::transpose_inplace_multiThreaded(principalComponentScores);

    MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(principalComponentScores, matrix);

    emit stopProgress(0);

    emit startProgress(0, "Exporting principal component scores", 0, 0);

    if (!this->processTableResult("principal component scores", componentLabels, d_parameters.selectedItemIdentifiersAndIndexes.first, principalComponentScores, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);


    // Calculating factorloadings (only for correlation mode)
    ////////////////

    if ((d_parameters.mode == CORRELATION) || (d_parameters.mode == DISTANCECORRELATION)) {

        emit startProgress(0, "Calculating factorloadings", 0, 0);

        MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded_horizontal(matrix, VectorOperations::applyFunctor_elementWise(eigenvalues, [](const double &value){ return std::sqrt(value);}), [](double &x, const double &y) {x *= y;});

        emit stopProgress(0);

        emit startProgress(0, "Exporting factorloadings", 0, 0);

        if (!this->processTableResult("Factorloadings", componentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
            return;

        emit stopProgress(0);

    }

    QThread::currentThread()->quit();

}
