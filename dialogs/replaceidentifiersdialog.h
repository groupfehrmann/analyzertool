#ifndef REPLACEIDENTIFIERSDIALOG_H
#define REPLACEIDENTIFIERSDIALOG_H

#include "dialogs/basedialog.h"
#include "workerclasses/replaceidentifiersworkerclass.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"

class ReplaceIdentifiersDialog : public BaseDialog
{

    Q_OBJECT

public:

    ReplaceIdentifiersDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);


    ~ReplaceIdentifiersDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    bool d_selectedOnly;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_identifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningNewIdentifiers;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // REPLACEIDENTIFIERSDIALOG_H
