#include "result.h"

ResultData::ResultData() :
    d_name("New result")
{

}

ResultData::ResultData(const ResultData &other) :
    QSharedData(other), d_uuidToResultItem(other.d_uuidToResultItem), d_uuidOfResultItems(other.d_uuidOfResultItems), d_name(other.d_name)
{

}

ResultData::~ResultData()
{

}

Result::Result(QObject *parent, const QString &name) :
    QObject(parent), d_data(new ResultData), d_uuid(QUuid::createUuid())
{

    d_data->d_name = name;

}

Result::Result(const Result &other) :
    QObject(other.parent()), d_data(other.d_data), d_uuid(QUuid::createUuid())
{

}

Result::~Result()
{

}

void Result::appendResultItem(BaseResultItem *baseResultItem)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    baseResultItem->setParent(0);

    this->connect(baseResultItem, SIGNAL(nameChanged(QUuid,QString)), this, SIGNAL(resultItemNameChanged(QUuid,QString)));

    QSharedPointer<BaseResultItem> sharedPointerBaseResultItem(baseResultItem);

    d_data->d_uuidToResultItem.insert(baseResultItem->universallyUniqueIdentifier(), sharedPointerBaseResultItem);

    d_data->d_uuidOfResultItems.append(baseResultItem->universallyUniqueIdentifier());

    emit this->resultItemAdded(sharedPointerBaseResultItem);

}

const QSharedPointer<BaseResultItem> Result::baseResultItemAt(int index) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToResultItem.value(d_data->d_uuidOfResultItems.at(index), QSharedPointer<BaseResultItem>());

}

const QSharedPointer<BaseResultItem> Result::baseResultItemAt(const QUuid &uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToResultItem.value(uuid, QSharedPointer<BaseResultItem>());

}

const QString &Result::name() const
{

    return d_data->d_name;

}

int Result::numberOfResultItems() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToResultItem.count();

}

void Result::removeAllResultItems()
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidToResultItem.clear();

    for (const QUuid &uuid: d_data->d_uuidOfResultItems)
        emit this->resultItemRemoved(uuid);

    d_data->d_uuidOfResultItems.clear();

}

void Result::removeResultItem(const QUuid &uuid)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidToResultItem.remove(uuid);

    d_data->d_uuidOfResultItems.removeOne(uuid);

    emit this->resultItemRemoved(uuid);

}

void Result::removeResultItems(const QList<QUuid> &uuids)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    for (const QUuid &uuid: uuids) {

        d_data->d_uuidToResultItem.remove(uuid);

        d_data->d_uuidOfResultItems.removeOne(uuid);

        emit this->resultItemRemoved(uuid);

    }

}

void Result::removeResultItem(int index)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    const QUuid &uuid(d_data->d_uuidOfResultItems.at(index));

    d_data->d_uuidToResultItem.remove(uuid);

    d_data->d_uuidOfResultItems.removeAt(index);

    emit this->resultItemRemoved(uuid);

}

void Result::removeResultItems(const QList<int> &indexes)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QList<int> sortedIndexes = indexes;

    std::sort(sortedIndexes.begin(), sortedIndexes.end(), std::greater<int>());

    for(const int &sortedIndex: sortedIndexes) {

        const QUuid &uuid(d_data->d_uuidOfResultItems.at(sortedIndex));

        d_data->d_uuidToResultItem.remove(uuid);

        d_data->d_uuidOfResultItems.removeAt(sortedIndex);

        emit this->resultItemRemoved(uuid);

    }

}

void Result::setName(const QString &name)
{

    d_data->d_name = name;

    emit this->nameChanged(d_uuid, name);

}

const QUuid &Result::universallyUniqueIdentifier() const
{

    return d_uuid;

}

Result &Result::operator=(const Result &other)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    if (this != &other)
        d_data.operator=(other.d_data);

    return *this;

}

QDataStream& operator<<(QDataStream& out, const Result &result)
{

    out << QString("_Result_begin#");

    out << QString("d_uuidOfResultItems") << result.d_data->d_uuidToResultItem.size();

    QHashIterator<QUuid, QSharedPointer<BaseResultItem> > it(result.d_data->d_uuidToResultItem);

    while (it.hasNext())
        out << *it.next().value().data();

    out << QString("_Result_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, Result &result)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Result_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_uuidOfResultItems")) {

            int numberOfResultItems;

            in >> numberOfResultItems;

            for (int i = 0; i < numberOfResultItems; ++i) {

                BaseResultItem *baseResultItem = new BaseResultItem();

                in >> *baseResultItem;

                result.appendResultItem(baseResultItem);

            }

        } else if (label == QString("_Result_end#"))
            return in;

    }

    return in;

}
