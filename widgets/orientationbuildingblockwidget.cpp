#include "orientationbuildingblockwidget.h"
#include "ui_orientationbuildingblockwidget.h"

OrientationBuildingBlockWidget::OrientationBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, bool rememberSettings) :
    QWidget(parent), ui(new Ui::OrientationBuildingBlockWidget), d_rememberSettings(rememberSettings)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    d_buttonGroup.addButton(ui->checkBox_row, 0);

    d_buttonGroup.addButton(ui->checkBox_column, 1);

    d_buttonGroup.setExclusive(true);

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/orientationBuildingBlockWidget");

        if (settings.contains("checkId"))
            d_buttonGroup.button(settings.value("checkId").toInt())->setChecked(true);

        settings.endGroup();

    }

    this->connect(ui->checkBox_row, SIGNAL(toggled(bool)), this, SLOT(checkBox_row_toggled(bool)));

}

OrientationBuildingBlockWidget::~OrientationBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/orientationBuildingBlockWidget");

        settings.setValue("checkId", d_buttonGroup.checkedId());

        settings.endGroup();

    }

    delete ui;

}

void OrientationBuildingBlockWidget::checkBox_row_toggled(bool toggled)
{

    if (toggled)
        emit orientationChanged(BaseMatrix::ROW);
    else
        emit orientationChanged(BaseMatrix::COLUMN);

}

BaseMatrix::Orientation OrientationBuildingBlockWidget::selectedOrientation() const
{

    return (ui->checkBox_row->isChecked() ? BaseMatrix::ROW : BaseMatrix::COLUMN);

}
