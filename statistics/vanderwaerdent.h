#ifndef VANDERWAERDENT_H
#define VANDERWAERDENT_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/chi_squared.hpp"
#include "boost/math/distributions/normal.hpp"
#include "math/vectoroperations.h"

template <typename T>
class VanDerWaerdenT : public BaseStatisticalTest
{

public:

    VanDerWaerdenT();

    VanDerWaerdenT(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~VanDerWaerdenT();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;

    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    double d_df;

    double d_tiesCorrectionFactor;

    QVector<QVector<T> > d_structuredMeasurements;

    int d_numberOfSamples;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation VanDerWaerdenT<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool VanDerWaerdenT<T>::d_hasEffectSize = false;

template <typename T>
QStringList VanDerWaerdenT<T>::d_sampleDescriptiveLabels = {"Count", "Mean rank", "Sum of ranks"};

template <typename T>
int VanDerWaerdenT<T>::d_maximumNumberOfSamples = std::numeric_limits<int>::max();

template <typename T>
int VanDerWaerdenT<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int VanDerWaerdenT<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString VanDerWaerdenT<T>::d_nameOfEffectSize = "Not applicable";

template <typename T>
QString VanDerWaerdenT<T>::d_nameOfTransformedEffectSize = "Not applicable";

template <typename T>
QString VanDerWaerdenT<T>::d_nameOfStatisticalTest = "van der Waerden T";

template <typename T>
bool VanDerWaerdenT<T>::d_rankedBased = true;

template <typename T>
QStringList VanDerWaerdenT<T>::d_testDescriptiveLabels = {"van der Waerden T", "degree of freedoms", "Significance (2-tailed)"};

template <typename T>
VanDerWaerdenT<T>::VanDerWaerdenT() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
VanDerWaerdenT<T>::VanDerWaerdenT(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    d_tiesCorrectionFactor = VectorOperations::crank(d_measurements);

    d_numberOfSamples = (*std::max_element(d_itemSampleCodedIdentifiers.begin(), d_itemSampleCodedIdentifiers.end())) + 1;

    this->createStructuredMeasurements();

}

template <typename T>
VanDerWaerdenT<T>::~VanDerWaerdenT()
{

}

template <typename T>
QVector<QVariant> VanDerWaerdenT<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i) {

        double n = d_structuredMeasurements.at(i).size();

        double sum = MathDescriptives::sum(d_structuredMeasurements.at(i));

        sampleDescriptiveValues << n << sum / n << sum;

    }

    return sampleDescriptiveValues;

}

template <typename T>
void VanDerWaerdenT<T>::calculateStatistic()
{

    double variance = 0.0;

    double t = 0.0;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i) {

        double sumSquared = MathDescriptives::sumOfSquaredValues(d_structuredMeasurements.at(i));

        variance += sumSquared;

        double meanGroup = MathDescriptives::mean(d_structuredMeasurements.at(i));

        t += d_structuredMeasurements.at(i).size() * meanGroup * meanGroup;
    }

    variance /= double(d_measurements.size() - 1.0);

    t /= variance;

    d_statistic =  t;

}

template <typename T>
void VanDerWaerdenT<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    d_df = d_structuredMeasurements.size() - 1;

    boost::math::chi_squared dist(d_df);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void VanDerWaerdenT<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void VanDerWaerdenT<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> VanDerWaerdenT<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_statistic << d_df << d_pValue;

    return descriptiveValues;

}

template <typename T>
void VanDerWaerdenT<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize(d_numberOfSamples);

    boost::math::normal dist1(0.0, 1.0);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << boost::math::quantile(dist1, d_measurements.at(i) / double(d_measurements.size() + 1.0));

}

template <typename T>
void VanDerWaerdenT<T>::_reset()
{

    this->createStructuredMeasurements();

}

#endif // VANDERWAERDENT_H
