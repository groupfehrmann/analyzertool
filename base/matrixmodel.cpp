#include "matrixmodel.h"

MatrixSortFilterProxyModel::MatrixSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent), d_showSelectedOnly(false)
{

}

bool MatrixSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    MatrixModel *matrixModel = static_cast<MatrixModel *>(this->sourceModel());

    if (!d_showSelectedOnly)
        return true;

    return matrixModel->d_baseDataset->header(BaseMatrix::ROW).isSelected(sourceRow);

}

bool MatrixSortFilterProxyModel::filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    MatrixModel *matrixModel = static_cast<MatrixModel *>(this->sourceModel());

    if (!d_showSelectedOnly)
        return true;

    return matrixModel->d_baseDataset->header(BaseMatrix::COLUMN).isSelected(sourceColumn);

}

bool MatrixSortFilterProxyModel::showSelectedOnly() const
{

    return d_showSelectedOnly;

}

void MatrixSortFilterProxyModel::setShowSelectedOnly(bool showSelectedOnly)
{

    d_showSelectedOnly = showSelectedOnly;

}

MatrixModel::MatrixModel(QObject *parent, const QSharedPointer<BaseDataset> &baseDataset) :
    QAbstractTableModel(parent), d_baseDataset(baseDataset)
{

    d_blockModel = false;

}

int MatrixModel::columnCount(QModelIndex const &parent) const
{

    if (d_blockModel)
        return 0;

    Q_UNUSED(parent);

    return d_baseDataset->header(BaseMatrix::COLUMN).count();

}

QVariant MatrixModel::data(QModelIndex const &index, int role) const
{

    if (d_blockModel || !index.isValid())
        return QVariant();

    switch (role) {

        case Qt::DisplayRole:
            return QVariant(d_baseDataset->baseMatrix()->valueAsVariant(index.row(), index.column()));

        case Qt::EditRole:
            return QVariant(d_baseDataset->baseMatrix()->valueAsVariant(index.row(), index.column())).toString();

        case Qt::ForegroundRole:
            return (!d_baseDataset->header(BaseMatrix::ROW).isSelected(index.row()) || !d_baseDataset->header(BaseMatrix::COLUMN).isSelected(index.column())) ? QColor(Qt::gray) : QColor(Qt::black);

        case Qt::StatusTipRole:
            return "row index " + QString::number(index.row()) + ", column index " + QString::number(index.column())  + " - \"" + d_baseDataset->header(BaseMatrix::ROW).identifier(index.row()) + "\", \"" + d_baseDataset->header(BaseMatrix::COLUMN).identifier(index.column()) + "\"";

        default:
            return QVariant();

    }

}

Qt::ItemFlags MatrixModel::flags(QModelIndex const &index) const
{

    if (d_blockModel || !index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;

}

QVariant MatrixModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if (d_blockModel)
        return QVariant();

    switch (role) {

        case Qt::DisplayRole:
            return (orientation == Qt::Vertical) ? d_baseDataset->header(BaseMatrix::ROW).identifier(section) : d_baseDataset->header(BaseMatrix::COLUMN).identifier(section);

        case Qt::EditRole:
            return (orientation == Qt::Vertical) ? d_baseDataset->header(BaseMatrix::ROW).identifier(section) : d_baseDataset->header(BaseMatrix::COLUMN).identifier(section);

        case Qt::ForegroundRole:
            return d_baseDataset->header((orientation == Qt::Vertical) ? BaseMatrix::ROW : BaseMatrix::COLUMN).isSelected(section) ? QColor(Qt::black) : QColor(Qt::red);

        case Qt::StatusTipRole:
            return "index " + QString::number(section);

        default:
            return QVariant();

    }

}

bool MatrixModel::insertColumns(int column, int count, const QModelIndex &parent)
{

    if (d_blockModel)
        return false;

    this->beginInsertColumns(parent, column, column + count - 1);

    QStringList identifiers;

    for (int i = 0; i < count; ++i)
        identifiers << Header::randomIdentifier(10);

    d_baseDataset->insertVectors(column, identifiers, BaseMatrix::COLUMN);

    this->endInsertColumns();

    return true;

}

bool MatrixModel::insertRows(int row, int count, const QModelIndex &parent)
{

    if (d_blockModel)
        return false;

    this->beginInsertRows(parent, row, row + count - 1);

    QStringList identifiers;

    for (int i = 0; i < count; ++i)
        identifiers << Header::randomIdentifier(10);

    d_baseDataset->insertVectors(row, identifiers, BaseMatrix::ROW);

    this->endInsertRows();

    return true;

}

bool MatrixModel::removeColumns(int column, int count, const QModelIndex &parent)
{

    if (d_blockModel)
        return false;

    Q_UNUSED(parent);

    this->beginRemoveColumns(parent, column, column + count - 1);

    QList<int> columnIndexesToRemove;

    for (int i = 0; i < count; ++i)
        columnIndexesToRemove << column + i;

    if (!d_baseDataset->header(BaseMatrix::COLUMN).areIndexesValid(columnIndexesToRemove))
        return false;

    d_baseDataset->removeVectors(columnIndexesToRemove, BaseMatrix::COLUMN);

    this->endRemoveColumns();

    return true;

}

bool MatrixModel::removeRows(int row, int count, const QModelIndex &parent)
{

    if (d_blockModel)
        return false;

    Q_UNUSED(parent);

    this->beginRemoveRows(parent, row, row + count - 1);

    QList<int> rowIndexesToRemove;

    for (int i = 0; i < count; ++i)
        rowIndexesToRemove << row + i;

    if (!d_baseDataset->header(BaseMatrix::ROW).areIndexesValid(rowIndexesToRemove))
        return false;

    d_baseDataset->removeVectors(rowIndexesToRemove, BaseMatrix::ROW);

    this->endRemoveRows();

    return true;

}

int MatrixModel::rowCount(QModelIndex const &parent) const
{

    if (d_blockModel)
        return 0;

    Q_UNUSED(parent);

    return d_baseDataset->header(BaseMatrix::ROW).count();

}

bool MatrixModel::setData(QModelIndex const &index, QVariant const &value, int role)
{

    if (d_blockModel || !index.isValid())
        return false;

    switch (role) {

        case Qt::EditRole:
            d_baseDataset->baseMatrix()->setValueAsVariant(index.row(), index.column(), value);
        break;

        default:
            return false;

    }

    emit dataChanged(index, index);

    return true;

}

bool MatrixModel::setHeaderData(int section, Qt::Orientation orientation, QVariant const &value, int role)
{

    if (d_blockModel)
        return false;

    switch (role) {

        case Qt::EditRole:
            d_baseDataset->header((orientation == Qt::Vertical) ? BaseMatrix::ROW : BaseMatrix::COLUMN).setIdentifier(section, value.toString());
        break;

        default:
            return false;

    }

    emit headerDataChanged(orientation, section, section);

    return true;

}

void MatrixModel::setSelectionStatus(int section, bool selectionStatus, BaseMatrix::Orientation orientation)
{

    if (d_blockModel)
        return;

    d_baseDataset->header(orientation).setSelectionStatus(section, selectionStatus);

    emit headerDataChanged((orientation == BaseMatrix::ROW) ? Qt::Vertical : Qt::Horizontal, section, section);

}

void MatrixModel::beginReset()
{

    this->beginResetModel();

    d_blockModel = true;

}

void MatrixModel::endReset()
{

    d_blockModel = false;

    this->endResetModel();

}
