#ifndef SELECTBRUSHSTYLEWIDGET_H
#define SELECTBRUSHSTYLEWIDGET_H

#include <QWidget>
#include <QBrush>
#include <QPixmap>
#include <QPainter>

namespace Ui {

class SelectBrushStyleWidget;

}

class SelectBrushStyleWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectBrushStyleWidget(QWidget *parent = 0, Qt::BrushStyle defaultBrushStyle = Qt::SolidPattern);

    ~SelectBrushStyleWidget();

    Qt::BrushStyle currentBrushStyle();

public slots:

    void setCurrentBrushStyle(Qt::BrushStyle brushStyle);

private:

    Ui::SelectBrushStyleWidget *ui;

    QPixmap createPixmap(Qt::BrushStyle brushStyle);

private slots:

    void currentIndexOfComboBoxChanged(int index);

signals:

    void currentBrushStyleChanged(Qt::BrushStyle brushStyle);

};

#endif // SELECTBRUSHSTYLEWIDGET_H
