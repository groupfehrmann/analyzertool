#ifndef CREATEBARPLOTSDIALOG_H
#define CREATEBARPLOTSDIALOG_H

#include <QString>
#include <QSharedPointer>
#include <QAbstractSeries>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/plotexportparametersbuildingblockwidget.h"
#include "workerclasses/createbarplotsworkerclass.h"
#include "widgets/selectfilebuildingblockwidget.h"

using namespace QtCharts;

class CreateBarPlotsDialog : public BaseDialog
{

    Q_OBJECT

public:

    CreateBarPlotsDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false, QAbstractSeries::SeriesType seriesType = QAbstractSeries::SeriesTypeBar);

    ~CreateBarPlotsDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_barSetLabels;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_barCategoryLabels;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarSetLabels;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeBarCategoryLabels;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_plotMode;

    PlotExportParametersBuildingBlockWidget *d_plotExportParametersBuildingBlockWidget;

    SelectFileBuildingBlockWidget *d_selectFileBuildingBlockWidget_templatePlot;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // CREATEBARPLOTSDIALOG_H
