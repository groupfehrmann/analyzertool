#ifndef ANNOTATIONSMODEL_H
#define ANNOTATIONSMODEL_H

#include <QAbstractTableModel>
#include <QSharedPointer>
#include <QMetaType>
#include <QColor>
#include <QSortFilterProxyModel>

#include <numeric>

#include "base/basedataset.h"
#include "base/dataset.h"
#include "widgets/datasetwidget.h"

class AnnotationsSortFilterProxyModel : public QSortFilterProxyModel
{

    Q_OBJECT

public:

    explicit AnnotationsSortFilterProxyModel(QObject *parent = 0);

    bool showSelectedOnly() const;

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

    bool filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const;

private:

    bool d_showSelectedOnly;

public slots:

    void setShowSelectedOnly(bool showSelectedOnly);

};

class AnnotationsModel : public QAbstractTableModel
{

    Q_OBJECT

    friend class AnnotationsSortFilterProxyModel;

public:

    AnnotationsModel(QObject *parent = nullptr, const QSharedPointer<BaseDataset> &baseDataset = QSharedPointer<BaseDataset>(), BaseMatrix::Orientation orientation = BaseMatrix::ROW);

    int columnCount(QModelIndex const &parent = QModelIndex()) const;

    QVariant data(QModelIndex const &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(QModelIndex const &index) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex());

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());

    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex());

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    int rowCount(QModelIndex const &parent = QModelIndex()) const;

    bool setData(QModelIndex const &index, QVariant const &value, int role = Qt::EditRole);

    bool setHeaderData(int section, Qt::Orientation orientation, QVariant const &value, int role = Qt::EditRole);

private:

    Annotations *d_annotations;

    QSharedPointer<BaseDataset> d_baseDataset;

    BaseMatrix::Orientation d_orientation;

    Header *d_verticalHeader;

    bool d_blockModel;

public slots:

    void beginReset();

    void endReset();

};

#endif // ANNOTATIONSMODEL_H
