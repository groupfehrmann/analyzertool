#ifndef KOLMOGOROVSMIRNOVZ_H
#define KOLMOGOROVSMIRNOVZ_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"

template <typename T>
class KolmogorovsmirnovZ : public BaseStatisticalTest
{

public:

    KolmogorovsmirnovZ();

    KolmogorovsmirnovZ(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~KolmogorovsmirnovZ();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;

    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    QVector<QVector<T> > d_structuredMeasurements;

    double d_d;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation KolmogorovsmirnovZ<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool KolmogorovsmirnovZ<T>::d_hasEffectSize = false;

template <typename T>
QStringList KolmogorovsmirnovZ<T>::d_sampleDescriptiveLabels = {"Count", "Mean", "Variance"};

template <typename T>
int KolmogorovsmirnovZ<T>::d_maximumNumberOfSamples = std::numeric_limits<int>::max();

template <typename T>
int KolmogorovsmirnovZ<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int KolmogorovsmirnovZ<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString KolmogorovsmirnovZ<T>::d_nameOfEffectSize = "NA";

template <typename T>
QString KolmogorovsmirnovZ<T>::d_nameOfTransformedEffectSize = "NA";

template <typename T>
QString KolmogorovsmirnovZ<T>::d_nameOfStatisticalTest = "Kolmogorov-Smirnov Z";

template <typename T>
bool KolmogorovsmirnovZ<T>::d_rankedBased = false;

template <typename T>
QStringList KolmogorovsmirnovZ<T>::d_testDescriptiveLabels = {"D", "Kolmogorov-Smirnov Z" "Significance (2-tailed)"};

template <typename T>
KolmogorovsmirnovZ<T>::KolmogorovsmirnovZ() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
KolmogorovsmirnovZ<T>::KolmogorovsmirnovZ(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    this->createStructuredMeasurements();

}

template <typename T>
KolmogorovsmirnovZ<T>::~KolmogorovsmirnovZ()
{

}

template <typename T>
QVector<QVariant> KolmogorovsmirnovZ<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    QPair<double, double> meandAndVarianceSample1 = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(0));

    QPair<double, double> meandAndVarianceSample2 = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(1));

    sampleDescriptiveValues << d_structuredMeasurements.at(0).size() << meandAndVarianceSample1.first << meandAndVarianceSample1.second;

    sampleDescriptiveValues << d_structuredMeasurements.at(1).size() << meandAndVarianceSample2.first << meandAndVarianceSample2.second;

    return sampleDescriptiveValues;

}

template <typename T>
void KolmogorovsmirnovZ<T>::calculateStatistic()
{

    std::sort(d_structuredMeasurements[0].begin(), d_structuredMeasurements[0].end());

    std::sort(d_structuredMeasurements[1].begin(), d_structuredMeasurements[1].end());

    int j1 = 0;

    int j2 = 0;

    d_d = 0.0;

    double d1;

    double d2;

    double dt;

    double fn1 = 0.0;

    double fn2 = 0.0;

    double en1 = d_structuredMeasurements.at(0).size();

    double en2 = d_structuredMeasurements.at(1).size();

    while (j1 < en1 && j2 < en2) {

        if ((d1 = d_structuredMeasurements.at(0).at(j1)) <= (d2 = d_structuredMeasurements.at(1).at(j2)))
            fn1 = ++j1 / en1;

        if (d2 <= d1)
            fn2 = ++j2 / en2;

        if ((dt = std::fabs(fn2 - fn1)) > d_d)
            d_d = dt;
    }

    d_statistic = std::sqrt(en1 * en2 / (en1 + en2)) * d_d;

}

template <typename T>
void KolmogorovsmirnovZ<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    const double EPS1 = 1.0e-6;

    const double EPS2 = 1.0e-16;

    double fac = 2.0;

    double sum = 0.0;

    double termbf = 0.0;

    double a2 = -2.0 * d_d * d_d;

    for (int j = 1; j <= 100; ++j) {

        double term = fac * std::exp(a2 * j * j);

        sum += term;

        if (std::fabs(term) <= EPS1 * termbf || std::fabs(term) <= EPS2 * sum) {

            d_pValue = sum;

            return;

        }

        fac = -fac;

        termbf = std::fabs(term);

    }

    d_pValue = 1.0;

}

template <typename T>
void KolmogorovsmirnovZ<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void KolmogorovsmirnovZ<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> KolmogorovsmirnovZ<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_d << d_statistic << d_pValue;

    return descriptiveValues;

}

template <typename T>
void KolmogorovsmirnovZ<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize((*std::max_element(d_itemSampleCodedIdentifiers.begin(), d_itemSampleCodedIdentifiers.end())) + 1);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

}

template <typename T>
void KolmogorovsmirnovZ<T>::_reset()
{

    this->createStructuredMeasurements();

}

#endif // KOLMOGOROVSMIRNOVZ_H
