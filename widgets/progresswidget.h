#ifndef PROGRESSWIDGET_H
#define PROGRESSWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QList>
#include <QLabel>
#include <QProgressBar>
#include <QDateTime>
#include <QThread>
#include <QThreadPool>

#include <cmath>

#include "base/loglistmodel.h"

namespace Ui {

class ProgressWidget;

}

class ProgressWidget : public QWidget
{

    Q_OBJECT

public:

    static QString secondsToTimeString(qint64 s);

    explicit ProgressWidget(QWidget *parent = nullptr);

    ~ProgressWidget();

private:

    Ui::ProgressWidget *ui;

    QList<QLabel *> d_indexToProgressTitle;

    QList<QProgressBar *> d_indexToProgressBar;

    QList<QLabel *> d_indexToEtaLabel;

    QList<QDateTime> d_indexToStartTime;

    QList<QDateTime> d_indexToTimeOfUpdateETA;

    QList<qint64> d_indexToETAInSeconds;

    QList<QPair<double, double> > d_indexToMinimumAndMaximumValue;

    QList<double> d_indexToCurrentValue;

    QDateTime d_startTimeOfProcess;

    int d_timerIdentifier;

    void setVisibility(int index, bool visible);

protected:

    void timerEvent(QTimerEvent *event);

signals:

    void appendLogItem(LogListModel::Type type, const QString &text);

    void cancelWorkerThread();

public slots:

    void startProcess(const QString &processTitle);

    void startProgress(int index, const QString &progressTitle, const double &minimumValue, const double &maximumValue);

    void stopProcess();

    void stopProgress(int index);

    void updateProgress(int index, const double &value);

private slots:

    void maximumThreadCountChanged(int count);

};

#endif // PROGRESSWIDGET_H
