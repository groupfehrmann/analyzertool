#include "importdatamatrixfromsinglefileworkerclass.h"

ImportDatasetFromSingleFileWorkerClass::ImportDatasetFromSingleFileWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<ImportDatasetFromSingleFileWorkerClass::Parameters *>(parameters))
{

    if (d_parameters.datasetDataType == "short")
        d_baseDataset = new Dataset<short>();
    else if (d_parameters.datasetDataType == "unsigned short")
        d_baseDataset = new Dataset<unsigned short>();
    else if (d_parameters.datasetDataType == "int")
        d_baseDataset = new Dataset<int>();
    else if (d_parameters.datasetDataType == "unsigned int")
        d_baseDataset = new Dataset<unsigned int>();
    else if (d_parameters.datasetDataType == "long long")
        d_baseDataset = new Dataset<long long>();
    else if (d_parameters.datasetDataType == "unsigned long long")
        d_baseDataset = new Dataset<unsigned long long>();
    else if (d_parameters.datasetDataType == "float")
        d_baseDataset = new Dataset<float>();
    else if (d_parameters.datasetDataType == "double")
        d_baseDataset = new Dataset<double>();
    else if (d_parameters.datasetDataType == "unsigned char")
        d_baseDataset = new Dataset<unsigned char>();
    else if (d_parameters.datasetDataType == "string")
        d_baseDataset = new Dataset<QString>();
    else if (d_parameters.datasetDataType == "bool")
        d_baseDataset = new Dataset<bool>();

}

void ImportDatasetFromSingleFileWorkerClass::cancel()
{

    delete d_baseDataset;

    QThread::currentThread()->quit();

}

void ImportDatasetFromSingleFileWorkerClass::doWork()
{

    emit processStarted("Import dataset from single file");

    emit appendLogItem(LogListModel::PARAMETER, "import data type: " + d_parameters.datasetDataType);

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    if (d_parameters.firstLabelInHeaderMode == ImportDatasetFromSingleFileWorkerClass::REPRESENTCOLUMNWITHROWIDENTIFIERS)
        emit appendLogItem(LogListModel::PARAMETER, "first label in header mode: represents column with row identifiers");
    else
        emit appendLogItem(LogListModel::PARAMETER, "first label in header mode: represents first column with data");

    emit appendLogItem(LogListModel::PARAMETER, "line number of header: " + QString::number(d_parameters.lineNumberHeaderLine));

    emit appendLogItem(LogListModel::PARAMETER, "line number of first data line: " + QString::number(d_parameters.lineNumberFirstDataLine));

    emit appendLogItem(LogListModel::PARAMETER, "line number of last data line: " + QString::number(d_parameters.lineNumberLastDataLine));

    emit appendLogItem(LogListModel::PARAMETER, "column defining row identifiers: index " + QString::number(d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first) + ", label \"" + d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.second + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "column defining first data column: index " + QString::number(d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first) + ", label \"" + d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.second + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "column defining second data column: index " + QString::number(d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.first) + ", label \"" + d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.second + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "delimiter type: \"" + SelectDelimiterBuildingBlockWidget::delimiterTypeToString(d_parameters.delimiterType) + "\"");

    if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
        emit appendLogItem(LogListModel::PARAMETER, "custum delimiter \"" + d_parameters.custumDelimiter + "\", case sensitive: " + (d_parameters.caseSensitivityForCustumDelimiter == Qt::CaseSensitive ? "true" : "false"));

    d_fileIn.setFileName(d_parameters.pathToFile);

    if (!d_fileIn.exists() || !d_fileIn.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\"");

        this->cancel();

        return;

    }

    emit startProgress(0, "Importing file", 0, d_fileIn.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    int numberOfLinesRead = 0;

    for (int i = 0; i < d_parameters.lineNumberHeaderLine - 1; ++i) {

        d_fileIn.readLine();

        ++numberOfLinesRead;

    }

    QStringList tokens;

    if (d_parameters.firstLabelInHeaderMode == ImportDatasetFromSingleFileWorkerClass::REPRESENTFIRSTCOLUMNWITHDATA)
        tokens << "[DUMMY]";

    if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
        tokens << QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(d_parameters.custumDelimiter, Qt::KeepEmptyParts, d_parameters.caseSensitivityForCustumDelimiter);
    else
        tokens << QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(QRegExp(d_parameters.custumDelimiter), Qt::KeepEmptyParts);

    ++numberOfLinesRead;

    int numberOfTokensInHeaderLine = tokens.size();

    int deltaIndex = (d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.first != -1) ? d_parameters.indexAndLabelForColumnDefiningSecondDataColumn.first - d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first : tokens.size();

    for (int i = d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first; i < tokens.size(); i += deltaIndex)
        d_baseDataset->header(BaseMatrix::COLUMN).append(tokens.at(i));

    for (int i = 0; i < d_parameters.lineNumberFirstDataLine - d_parameters.lineNumberHeaderLine - 1; ++i) {

        d_fileIn.readLine();

        ++numberOfLinesRead;

    }

    while (!d_fileIn.atEnd() && (numberOfLinesRead != d_parameters.lineNumberLastDataLine)) {

        if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
            tokens = QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(d_parameters.custumDelimiter, Qt::KeepEmptyParts, d_parameters.caseSensitivityForCustumDelimiter);
        else
            tokens = QString(d_fileIn.readLine()).remove(QRegExp("[\r\n]")).split(QRegExp(d_parameters.custumDelimiter), Qt::KeepEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() != numberOfTokensInHeaderLine)
            emit appendLogItem(LogListModel::WARNING, "the number of tokens read at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInHeaderLine) + " tokens detected in the header line");

        QVector<QString> vectorOfStrings;

        for (int i = d_parameters.indexAndLabelForColumnDefiningFirstDataColumn.first; i < tokens.size(); i += deltaIndex)
            vectorOfStrings << tokens.at(i);

        if (vectorOfStrings.size() != d_baseDataset->header(BaseMatrix::COLUMN).count()) {

            emit appendLogItem(LogListModel::ERROR, "at line number " + QString::number(numberOfLinesRead) + " the number of data tokens " + QString::number(vectorOfStrings.size()) + " is unequal to the " + QString::number(d_baseDataset->header(BaseMatrix::COLUMN).count()) + " column labels detected in the header line");

            this->cancel();

            return;

        }

        QString errorMessage;

        if (!d_baseDataset->appendVector(tokens.at(d_parameters.indexAndLabelForColumnDefiningRowIdentifiers.first), vectorOfStrings, BaseMatrix::ROW, &errorMessage))
            emit appendLogItem(LogListModel::WARNING, "at line " + QString::number(numberOfLinesRead) + " " + errorMessage);

        if (QThread::currentThread()->isInterruptionRequested()) {

            this->cancel();

            return;

        }

    }

    if (numberOfLinesRead != d_parameters.lineNumberLastDataLine)
        emit appendLogItem(LogListModel::WARNING, "could not read until last data line requested");

    emit stopProgress(0);

    emit appendLogItem(LogListModel::MESSAGE, "number of rows: " + QString::number(d_baseDataset->header(BaseMatrix::ROW).count()));

    emit appendLogItem(LogListModel::MESSAGE, "number of columns: " + QString::number(d_baseDataset->header(BaseMatrix::COLUMN).count()));

    emit datasetAvailable(d_baseDataset);

    QThread::currentThread()->quit();

}

void ImportDatasetFromSingleFileWorkerClass::updateFileProgress()
{

    emit updateProgress(0, d_fileIn.pos());

}
