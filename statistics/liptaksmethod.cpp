#include "liptaksmethod.h"

QStringList LiptaksMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "Z-transformed ( Significance (2-tailed) )", "Weight", "Sample size"};

QStringList LiptaksMethod::d_metaAnalysisTestDescriptiveLabels = {"Z", "Significance (2-tailed)"};

QString LiptaksMethod::d_nameOfMetaAnalysisTest = "Liptak's method";


LiptaksMethod::LiptaksMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

LiptaksMethod::~LiptaksMethod()
{

}

QStringList LiptaksMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList LiptaksMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> LiptaksMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    boost::math::normal dist(0.0, 1.0);

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << -1.0 * boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0) << std::sqrt(baseStatisticalTest->sampleSize()) << baseStatisticalTest->sampleSize();

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> LiptaksMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void LiptaksMethod::calculateStatistic()
{

    boost::math::normal dist(0.0, 1.0);

    d_statistic = 0.0;

    double w = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        d_statistic += -1.0 *boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0) * std::sqrt(baseStatisticalTest->sampleSize());

        w += baseStatisticalTest->sampleSize();

    }

    d_statistic /= std::sqrt(w);

}

void LiptaksMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, std::fabs(d_statistic)));

}

void LiptaksMethod::_reset()
{

}
