#ifndef GENESETENRICHMENTANALYSISDIALOG_H
#define GENESETENRICHMENTANALYSISDIALOG_H

#include <QString>
#include <QSharedPointer>
#include <QCompleter>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectmultiplefilesbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "widgets/spinboxbuildingblockwidget.h"

#include "workerclasses/genesetenrichmentanalysisworkerclass.h"

class GeneSetEnrichmentAnalysisDialog : public BaseDialog
{

    Q_OBJECT

public:

    GeneSetEnrichmentAnalysisDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~GeneSetEnrichmentAnalysisDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    SelectMultipleFilesBuildingBlockWidget *d_selectMultipleFilesBuildingBlockWidget_geneSetDatabases;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelUsedToMatch;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_minimumSizeOfGeneSet;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_maximumSizeOfGeneSet;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_testToDefineEnrichment;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_performPermutations;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_numberOfPermutations;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_outputFormat;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningSearchableKeys;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_exportGeneSetMemberFiles;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // GENESETENRICHMENTANALYSISDIALOG_H
