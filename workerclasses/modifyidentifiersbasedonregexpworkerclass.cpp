#include "modifyidentifiersbasedonregexpworkerclass.h"

ModifyIdentifiersBasedOnRegExpWorkerClass::ModifyIdentifiersBasedOnRegExpWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<ModifyIdentifiersBasedOnRegExpWorkerClass::Parameters *>(parameters))
{

}

void ModifyIdentifiersBasedOnRegExpWorkerClass::doWork()
{

    emit processStarted("Modify identifiers based on regular expression");

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of identifiers selected: " + QString::number(d_parameters.selectedIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "regular expression \"" + d_parameters.regularExpression.pattern() + "\"");

    if (Qt::CaseInsensitive == d_parameters.regularExpression.caseSensitivity())
        emit appendLogItem(LogListModel::PARAMETER, "case sensitivity : insensitive");
    else
        emit appendLogItem(LogListModel::PARAMETER, "case sensitivity : sensitive");

    if (QRegExp::RegExp2 == d_parameters.regularExpression.patternSyntax())
        emit appendLogItem(LogListModel::PARAMETER, "pattern syntax : Regular expression");
    else if (QRegExp::Wildcard == d_parameters.regularExpression.patternSyntax())
        emit appendLogItem(LogListModel::PARAMETER, "pattern syntax : Wildcard");
    else if (QRegExp::FixedString == d_parameters.regularExpression.patternSyntax())
        emit appendLogItem(LogListModel::PARAMETER, "pattern syntax : Fixed string");

    emit appendLogItem(LogListModel::PARAMETER, "string to replace with \"" + d_parameters.stringToReplaceWith + "\"");

    if (!d_parameters.regularExpression.isValid()) {

        emit appendLogItem(LogListModel::ERROR, "regular expression is invalid: \"" + d_parameters.regularExpression.errorString() + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit matrixDataBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    emit annotationsBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    Header &header(d_parameters.baseDataset->header(d_parameters.orientation));

    Annotations &annotations(d_parameters.baseDataset->annotations(d_parameters.orientation));

    emit startProgress(0, "Modifying identifiers", 0, d_parameters.selectedIdentifiersAndIndexes.second.size());

    for (int i = 0 ; i < d_parameters.selectedIdentifiersAndIndexes.second.size(); ++i) {

        QString currentIdentifier = d_parameters.selectedIdentifiersAndIndexes.first.at(i);

        QString newIdentifier = currentIdentifier;

        newIdentifier.replace(d_parameters.regularExpression, d_parameters.stringToReplaceWith);

        header.setIdentifier(d_parameters.selectedIdentifiersAndIndexes.second.at(i), newIdentifier);

        if (QThread::currentThread()->isInterruptionRequested()) {

            emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

            emit matrixDataBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier());

            QThread::currentThread()->quit();

            return;

        }

        if (!annotations.labels().isEmpty()) {

            for (const QString &label : annotations.labels()) {

                QVariant annotationValue = annotations.value(currentIdentifier, label);

                if (!annotationValue.isNull()) {

                    if (!annotations.appendValue(newIdentifier, label, annotationValue))
                        emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + newIdentifier + "\", label \"" + label + "\", value to set \"" + annotationValue.toString() + "\", current value \"" + annotations.value(newIdentifier, label).toString() + "\"");

                }

            }

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Cleaning up annotations", 0, annotations.identifiers().size());

    int progressCounter = 0;

    for (const QString &identifier : annotations.identifiers().values()) {

        if (!header.contains(identifier))
            annotations.removeIdentifier(identifier);

        emit updateProgress(0, ++progressCounter);

    }

    emit stopProgress(0);

    emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    QThread::currentThread()->quit();

}
