#include "annotationsmodel.h"

AnnotationsSortFilterProxyModel::AnnotationsSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent), d_showSelectedOnly(false)
{

}

bool AnnotationsSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    AnnotationsModel *annotationsModel = static_cast<AnnotationsModel *>(this->sourceModel());

    if (!d_showSelectedOnly)
        return true;

    return annotationsModel->d_baseDataset->header(annotationsModel->d_orientation).isSelected(sourceRow);

}

bool AnnotationsSortFilterProxyModel::filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceColumn);

    Q_UNUSED(sourceParent);

    return true;

}

bool AnnotationsSortFilterProxyModel::showSelectedOnly() const
{

    return d_showSelectedOnly;

}

void AnnotationsSortFilterProxyModel::setShowSelectedOnly(bool showSelectedOnly)
{

    d_showSelectedOnly = showSelectedOnly;

}

AnnotationsModel::AnnotationsModel(QObject *parent, const QSharedPointer<BaseDataset> &baseDataset, BaseMatrix::Orientation orientation) :
    QAbstractTableModel(parent), d_baseDataset(baseDataset), d_orientation(orientation)
{

    d_annotations = &d_baseDataset->annotations(orientation);

    d_verticalHeader = &d_baseDataset->header(orientation);

    d_blockModel = false;

}

int AnnotationsModel::columnCount(QModelIndex const &parent) const
{

    if (d_blockModel)
        return 0;

    Q_UNUSED(parent);

    return d_annotations->labels().size();

}

QVariant AnnotationsModel::data(QModelIndex const &index, int role) const
{

    if (d_blockModel || !index.isValid())
        return QVariant();

    switch (role) {

    case Qt::DisplayRole:
        return d_annotations->value(d_verticalHeader->identifier(index.row()), d_annotations->label(index.column()));

    case Qt::EditRole:
        return d_annotations->value(d_verticalHeader->identifier(index.row()), d_annotations->label(index.column())).toString();

    case Qt::ForegroundRole:
        return (!d_verticalHeader->isSelected(index.row())) ? QColor(Qt::gray) : QColor(Qt::black);

    case Qt::StatusTipRole:
        return "identifier index " + QString::number(index.row()) + ", label index " + QString::number(index.column())  + " - \"" + d_verticalHeader->identifier(index.row()) + "\", \"" + d_annotations->label(index.column()) + "\"";
    default:
        return QVariant();

    }

}

Qt::ItemFlags AnnotationsModel::flags(QModelIndex const &index) const
{

    if (d_blockModel || !index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;

}

QVariant AnnotationsModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if (d_blockModel)
        return QVariant();

    switch (role) {

    case Qt::DisplayRole:
        return (orientation == Qt::Vertical) ? d_verticalHeader->identifier(section) : d_annotations->label(section);

    case Qt::EditRole:
        return (orientation == Qt::Vertical) ? d_verticalHeader->identifier(section) : d_annotations->label(section);

    case Qt::ForegroundRole:
        return (orientation == Qt::Vertical) ? ((!d_verticalHeader->isSelected(section)) ? QColor(Qt::red) : QColor(Qt::black)) : QColor(Qt::black);

    case Qt::StatusTipRole:
        return (orientation == Qt::Vertical) ? "identifier index " + QString::number(section) : "label index " + QString::number(section);

    default:
        return QVariant();

    }

}

bool AnnotationsModel::insertColumns(int column, int count, const QModelIndex &parent)
{

    Q_UNUSED(parent);

    if (d_blockModel)
        return false;

    this->beginInsertColumns(parent, column, column + count - 1);

    bool ok = d_annotations->insertLabels(column, Header::randomIdentifiers(10, count));

    this->endInsertColumns();

    return ok;

}

bool AnnotationsModel::insertRows(int row, int count, const QModelIndex &parent)
{

    Q_UNUSED(parent);

    if (d_blockModel)
        return false;

    this->beginInsertRows(parent, row, row + count - 1);

    QStringList identifiers;

    for (int i = 0; i < count; ++i)
        identifiers << Header::randomIdentifier(10);

    d_baseDataset->insertVectors(row, identifiers, d_orientation);

    this->endInsertRows();

    return true;

}

bool AnnotationsModel::removeColumns(int column, int count, const QModelIndex &parent)
{

    Q_UNUSED(parent);

    if (d_blockModel)
        return false;

    this->beginRemoveColumns(parent, column, column + count - 1);

    QList<int> columnIndexesToRemove;

    for (int i = 0; i < count; ++i)
        columnIndexesToRemove << column + i;

    d_annotations->removeLabels(columnIndexesToRemove);

    this->endRemoveColumns();

    return true;

}

bool AnnotationsModel::removeRows(int row, int count, const QModelIndex &parent)
{

    Q_UNUSED(parent);

    if (d_blockModel)
        return false;

    this->beginRemoveRows(parent, row, row + count - 1);

    QList<int> rowIndexesToRemove;

    for (int i = 0; i < count; ++i)
        rowIndexesToRemove << row + i;

    if (!d_verticalHeader->areIndexesValid(rowIndexesToRemove))
        return false;

    d_baseDataset->removeVectors(rowIndexesToRemove, d_orientation);

    this->endRemoveRows();

    return true;

}

int AnnotationsModel::rowCount(QModelIndex const &parent) const
{

    if (d_blockModel)
        return 0;

    Q_UNUSED(parent);

    return d_verticalHeader->count();

}

bool AnnotationsModel::setData(QModelIndex const &index, QVariant const &value, int role)
{

    if (d_blockModel || !index.isValid())
        return false;

    switch (role) {

        case Qt::EditRole: d_annotations->setValue(d_verticalHeader->identifier(index.row()), d_annotations->label(index.column()), value); break;

        default:
            return false;

    }

    emit dataChanged(index, index);

    return true;

}

bool AnnotationsModel::setHeaderData(int section, Qt::Orientation orientation, QVariant const &value, int role)
{

    if (d_blockModel)
        return false;

    bool ok = true;

    switch (role) {

        case Qt::EditRole: {

            if (orientation == Qt::Vertical)
                d_verticalHeader->setIdentifier(section, value.toString());
            else
                ok = d_annotations->setLabel(section, value.toString());
        } break;

        default:
            return false;

    }

    emit headerDataChanged(orientation, section, section);

    return ok;

}

void AnnotationsModel::beginReset()
{

    this->beginResetModel();

    d_blockModel = true;

}

void AnnotationsModel::endReset()
{

    d_blockModel = false;

    this->endResetModel();

}
