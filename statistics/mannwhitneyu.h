#ifndef MANNWHITNEYU_H
#define MANNWHITNEYU_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/normal.hpp"
#include "math/vectoroperations.h"

template <typename T>
class MannWhitneyU : public BaseStatisticalTest
{

public:

    MannWhitneyU();

    MannWhitneyU(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~MannWhitneyU();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;

    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    double d_n1;

    double d_n2;

    double d_u1;

    double d_z;

    double d_pointBiserialR;

    double d_tiesCorrectionFactor;

    QVector<QVector<T> > d_structuredMeasurements;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation MannWhitneyU<T>::d_effectSizeTransformation = BaseStatisticalTest::FISHER_R_TO_Z;

template <typename T>
bool MannWhitneyU<T>::d_hasEffectSize = true;

template <typename T>
QStringList MannWhitneyU<T>::d_sampleDescriptiveLabels = {"Count", "Mean rank", "Sum of ranks"};

template <typename T>
int MannWhitneyU<T>::d_maximumNumberOfSamples = 2;

template <typename T>
int MannWhitneyU<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int MannWhitneyU<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString MannWhitneyU<T>::d_nameOfEffectSize = "Fisher's z' transformation";

template <typename T>
QString MannWhitneyU<T>::d_nameOfTransformedEffectSize = "Fisher's z' transformation";

template <typename T>
QString MannWhitneyU<T>::d_nameOfStatisticalTest = "Mann-Whitney U";

template <typename T>
bool MannWhitneyU<T>::d_rankedBased = true;

template <typename T>
QStringList MannWhitneyU<T>::d_testDescriptiveLabels = {"Point-biserial R", "Fisher's point-biserial R to Z", "Standard error Fisher's point-biserial R to Z", "Area under the curve (AUC)", "Z", "Mann-Whitney U", "Significance (2-tailed)"};

template <typename T>
MannWhitneyU<T>::MannWhitneyU() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
MannWhitneyU<T>::MannWhitneyU(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    d_tiesCorrectionFactor = VectorOperations::crank(d_measurements);

    this->createStructuredMeasurements();

}

template <typename T>
MannWhitneyU<T>::~MannWhitneyU()
{

}

template <typename T>
QVector<QVariant> MannWhitneyU<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    double n1 = d_structuredMeasurements.at(0).size();

    double sumOfRanksSample1 = MathDescriptives::sum(d_structuredMeasurements.at(0));

    sampleDescriptiveValues << n1 << sumOfRanksSample1 / n1  << sumOfRanksSample1;

    double n2 = d_structuredMeasurements.at(1).size();

    double sumOfRanksSample2 = MathDescriptives::sum(d_structuredMeasurements.at(1));

    sampleDescriptiveValues << n2 << sumOfRanksSample2 / n2 << sumOfRanksSample2;

    return sampleDescriptiveValues;

}

template <typename T>
void MannWhitneyU<T>::calculateStatistic()
{

    double sumOfRankSample1;

    if (d_structuredMeasurements.at(0).size() < d_structuredMeasurements.at(1).size()) {

        sumOfRankSample1 = MathDescriptives::sum(d_structuredMeasurements.at(0));

        d_n1 = d_structuredMeasurements.at(0).size();

        d_n2 = d_structuredMeasurements.at(1).size();

    } else {

        sumOfRankSample1 = MathDescriptives::sum(d_structuredMeasurements.at(1));

        d_n1 = d_structuredMeasurements.at(1).size();

        d_n2 = d_structuredMeasurements.at(0).size();

    }

    double n = d_measurements.size();

    double sumOfRankSample2 = ((n * (n + 1.0)) / 2.0) - sumOfRankSample1;

    d_u1 = d_n1 * d_n2 + ((d_n1 * (d_n1 + 1.0)) / 2.0) - sumOfRankSample1;

    double u2 = d_n1 * d_n2 + ((d_n2 * (d_n2 + 1.0)) / 2.0) - sumOfRankSample2;

    d_statistic =  d_u1 < u2 ? d_u1 : u2;

}

template <typename T>
void MannWhitneyU<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    d_z = (d_statistic - (d_n1 * d_n2 / 2.0)) / std::sqrt(((d_n1 * d_n2 * (d_n1 + d_n2 + 1.0)) / 12.0) - ((d_n1 * d_n2 * d_tiesCorrectionFactor) / (12.0 * (d_n1 + d_n2) * (d_n1 + d_n2 - 1.0))));

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, fabs(d_z)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void MannWhitneyU<T>::calculateEffectSize()
{

    d_pointBiserialR = std::fabs(d_z) / std::sqrt(d_measurements.size());

    if (d_pointBiserialR == -1.0) {

        d_effectSize = std::numeric_limits<double>::lowest();

        return;

    }

    if (d_pointBiserialR == 1.0) {

        d_effectSize = std::numeric_limits<double>::max();

        return;

    }

    d_effectSize = std::atanh(d_pointBiserialR);

}

template <typename T>
void MannWhitneyU<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = (1.0 / std::sqrt(d_n1 + d_n2 - 3.0));

}

template <typename T>
QVector<QVariant> MannWhitneyU<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_pointBiserialR << d_effectSize << d_standardErrorOfEffectSize << ((d_structuredMeasurements.at(0).size() * d_structuredMeasurements.at(1).size() + ((d_structuredMeasurements.at(0).size() * (d_structuredMeasurements.at(0).size() + 1.0)) / 2.0) - MathDescriptives::sum(d_structuredMeasurements.at(0))) / (d_n1 * d_n2)) - 0.5 << d_z << d_statistic << d_pValue;

    return descriptiveValues;

}

template <typename T>
void MannWhitneyU<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize(2);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

}

template <typename T>
void MannWhitneyU<T>::_reset()
{

    this->createStructuredMeasurements();

}

#endif // MANNWHITNEYU_H
