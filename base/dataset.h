#ifndef DATASET_H
#define DATASET_H

#include <QObject>

#include "base/basedataset.h"
#include "base/matrix.h"

template <typename T>
class Dataset : public BaseDataset
{

public:

    Dataset(QObject *parent = nullptr);

    Dataset(const Dataset<T> &other);

    ~Dataset();

    void appendVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation);

    void appendVectors(const QStringList &identifiers, const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation);

    void insertVector(int index, const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation);

    void insertVectors(int index, const QStringList &identifiers, const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation);

    Matrix<T> *matrix();

    const Matrix<T> *matrix() const;

    void prependVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation);

    void prependVectors(const QStringList &identifiers, const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation);

    Dataset &operator=(const Dataset<T> &other);

    template <typename U> friend QDataStream& operator<<(QDataStream& out, const Dataset<U> &dataset);

    template <typename U> friend QDataStream& operator>>(QDataStream& in, Dataset<U> &dataset);

};

Q_DECLARE_TYPEINFO(Dataset<short>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<unsigned short>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<int>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<unsigned int>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<long long>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<unsigned long long>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<float>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<double>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<unsigned char>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<QChar>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<QString>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Dataset<bool>, Q_MOVABLE_TYPE);

template <typename T>
Dataset<T>::Dataset(QObject *parent) :
    BaseDataset(parent)
{

    d_data->d_baseMatrix = new Matrix<T>(parent);

}

template <typename T>
Dataset<T>::Dataset(const Dataset<T> &other) :
    BaseDataset(other.parent())
{

    d_data = other.d_data;

}

template <typename T>
Dataset<T>::~Dataset()
{

}

template <typename T>
void Dataset<T>::appendVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).append(identifier);

    static_cast<Matrix<T> *>(d_data->d_baseMatrix)->appendVector(vector, orientation);

}

template <typename T>
void Dataset<T>::appendVectors(const QStringList &identifiers, const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).append(identifiers);

    static_cast<Matrix<T> *>(d_data->d_baseMatrix)->appendVectors(vectors, orientation);

}

template <typename T>
void Dataset<T>::insertVector(int index, const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).insert(index, identifier);

    static_cast<Matrix<T> *>(d_data->d_baseMatrix)->insertVector(index, vector, orientation);

}

template <typename T>
void Dataset<T>::insertVectors(int index, const QStringList &identifiers, const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).insert(index, identifiers);

    static_cast<Matrix<T> *>(d_data->d_baseMatrix)->insertVectors(index, vectors, orientation);

}

template <typename T>
Matrix<T> *Dataset<T>::matrix()
{

    return static_cast<Matrix<T> *>(d_data->d_baseMatrix);

}

template <typename T>
const Matrix<T> *Dataset<T>::matrix() const
{

    return static_cast<const Matrix<T> *>(d_data->d_baseMatrix);

}

template <typename T>
void Dataset<T>::prependVector(const QString &identifier, const QVector<T> &vector, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).prepend(identifier);

    static_cast<Matrix<T> *>(d_data->d_baseMatrix)->prependVector(vector, orientation);

}

template <typename T>
void Dataset<T>::prependVectors(const QStringList &identifiers, const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).prepend(identifiers);

    static_cast<Matrix<T> *>(d_data->d_baseMatrix)->prependVectors(vectors, orientation);

}

template <typename T>
Dataset<T> &Dataset<T>::operator=(const Dataset<T> &other)
{

    if (this != &other)
        d_data.operator =(other.d_data);

    return *this;

}

template <typename U>
QDataStream& operator<<(QDataStream& out, const Dataset<U> &dataset)
{

    out << QString("_Dataset_begin#");

    out << QString("d_baseMatrix") << *static_cast<Matrix<U> *>(dataset.d_data->d_baseMatrix);

    out << QString("d_verticalHeader") << dataset.d_data->d_verticalHeader;

    out << QString("d_horizontalHeader") << dataset.d_data->d_horizontalHeader;

    out << QString("d_columnAnnotations") << dataset.d_data->d_columnAnnotations;

    out << QString("d_rowAnnotations") << dataset.d_data->d_rowAnnotations;

    out << QString("_Dataset_end#");

    return out;

}

template <typename U>
QDataStream& operator>>(QDataStream& in, Dataset<U> &dataset)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Dataset_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_elements"))
            in >> *static_cast<Matrix<U> *>(dataset.d_data->d_baseMatrix);
        else if (label == QString("d_verticalHeader#"))
            in >> dataset.d_data->d_verticalHeader;
        else if (label == QString("d_horizontalHeader#"))
            in >> dataset.d_data->d_horizontalHeader;
        else if (label == QString("d_columnAnnotations#"))
            in >> dataset.d_data->d_columnAnnotations;
        else if (label == QString("d_rowAnnotations#"))
            in >> dataset.d_data->d_rowAnnotations;
        else if (label == QString("_Dataset_end#"))
            return in;

    }

    return in;

}

#endif // DATASET_H

