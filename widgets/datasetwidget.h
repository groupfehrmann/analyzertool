#ifndef DATASETWIDGET_H
#define DATASETWIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include <QScrollBar>
#include <QModelIndex>
#include <QModelIndexList>
#include <QAction>
#include <QMenu>
#include <QVariant>
#include <QLineEdit>
#include <QWidgetAction>
#include <QClipboard>
#include <QTextStream>
#include <QTableView>
#include <QAbstractItemModel>

#include "base/basedataset.h"
#include "base/annotationsmodel.h"
#include "base/matrixmodel.h"
#include "base/convertfunctions.h"
#include "base/loglistmodel.h"

namespace Ui {

class DatasetWidget;

}

class DatasetWidget : public QWidget
{

    Q_OBJECT

public:

    explicit DatasetWidget(QWidget *parent = nullptr, const QSharedPointer<BaseDataset> &baseDataset = QSharedPointer<BaseDataset>());

    ~DatasetWidget();

    QAbstractItemModel *currentTableModel();

    QTableView *currentTableView();

    int indexOfSelectedTab() const;

private:

    Ui::DatasetWidget *ui;

    QSharedPointer<BaseDataset> d_baseDataset;

    QAction *d_copyAction_headerActivated;

    QAction *d_copyToAnnotationsAction_headerActivated;

    QAction *d_copyWithHeaderLabelsAction_headerActivated;

    QAction *d_deleteAction_headerActivated;

    QAction *d_deselectDataAction_headerActivated;

    QAction *d_insertAction_headerActivated;

    QWidgetAction *d_renameIdentifierWidgetAction_headerActivated;

    QWidgetAction *d_renameAnnotationLabelWidgetAction_headerActivated;

    QAction *d_selectDataAction_headerActivated;

    QMenu *d_annotationsHeaderMenu;

    QMenu *d_dataMatrixHeaderMenu;

    void createActions();

    void createMenus();

public slots:

    void annotationsBeginChange(BaseMatrix::Orientation orientation);

    void annotationsEndChange(BaseMatrix::Orientation orientation);

    void beginChange();

    void endChange();

    void matrixDataBeginChange();

    void matrixDataEndChange();

    void showSelectedOnly(bool value);

private slots:

    void copy_headerActivated();

    void copyWithHeaderLabels_headerActivated();

    void copyToAnnotations_headerActivated();

    void delete_headerActivated();

    void insert_headerActivated();

    void deselectData_headerActivated();

    void headerDataForMatrixModelChanged(Qt::Orientation orientation, int first, int last);

    void renameAnnotationLabel_headerActivated();

    void renameIdentifier_headerActivated();

    void selectData_headerActivated();

    void showContextMenuVerticalHeaderDataMatrix(const QPoint &pos);

    void showContextMenuHorizontalHeaderDataMatrix(const QPoint &pos);

    void showContextMenuHorizontalHeaderAnnotations(const QPoint &pos);

signals:

    void appendLogItem(LogListModel::Type type, const QString &text);

};

#endif // DATASETWIDGET_H
