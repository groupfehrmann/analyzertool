#ifndef DEDUPLICATEIDENTIFIERSDIALOG_H
#define DEDUPLICATEIDENTIFIERSDIALOG_H

#include "dialogs/basedialog.h"
#include "workerclasses/deduplicateidentifiersworkerclass.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"

class DeduplicateIdentifiersDialog : public BaseDialog
{

    Q_OBJECT

public:

    DeduplicateIdentifiersDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);


    ~DeduplicateIdentifiersDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    bool d_selectedOnly;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_identifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_mode;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // DEDUPLICATEIDENTIFIERSDIALOG_H
