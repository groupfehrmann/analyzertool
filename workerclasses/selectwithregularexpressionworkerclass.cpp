#include "selectwithregularexpressionworkerclass.h"

SelectWithRegularExpressionWorkerClass::SelectWithRegularExpressionWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<SelectWithRegularExpressionWorkerClass::Parameters *>(parameters))
{

}

void SelectWithRegularExpressionWorkerClass::doWork()
{

    emit processStarted("Select with regular expression");

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of identifiers selected: " + QString::number(d_parameters.selectedIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "sequence of filters to apply:");

    for (int i = 0; i < d_parameters.filters.size(); ++i) {

        const Filter &filter(d_parameters.filters.at(i));

        QString str = "filter " + QString::number(i + 1);

        str += "\tapply to : " + (filter.annotationLabel.isEmpty() ? "identifiers" : "annotation values with label \"" + filter.annotationLabel + "\"");

        switch (filter.mode) {

            case REQUIRED: str += "\tmode : Required"; break;

            case OPTIONAL: str += "\tmode : Optional"; break;

            default: break;

        }

        str += "\tpattern : " + filter.regularExpression.pattern();

        if (QRegExp::RegExp2 == filter.regularExpression.patternSyntax())
            str += "\tpattern syntax : regular expression";
        else if (QRegExp::Wildcard == filter.regularExpression.patternSyntax())
            str += "\tpattern syntax : wildcard";
        else if (QRegExp::FixedString == filter.regularExpression.patternSyntax())
            str += "\tpattern syntax : fixed string";

        if (filter.regularExpression.caseSensitivity() == Qt::CaseSensitive)
            str += "\tcase sensitivity : case sensitive";
        else
            str += "\tcase sensitivity : case insensitive";

        emit appendLogItem(LogListModel::PARAMETER, str);

    }

    emit matrixDataBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    emit annotationsBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    Header &header(d_parameters.baseDataset->header(d_parameters.orientation));

    Annotations &annotations(d_parameters.baseDataset->annotations(d_parameters.orientation));

    emit startProgress(0, "Selecting identifiers", 0, d_parameters.selectedIdentifiersAndIndexes.second.size());

    QList<int> indexesToSelect;

    for (int i = 0 ; i < d_parameters.selectedIdentifiersAndIndexes.second.size(); ++i) {

        bool filterSuccessful = false;

        for (const Filter &filter : d_parameters.filters) {

            QString stringToEvaluate = filter.annotationLabel.isEmpty() ? d_parameters.selectedIdentifiersAndIndexes.first.at(i) : annotations.value(d_parameters.selectedIdentifiersAndIndexes.first.at(i), filter.annotationLabel).toString();

            if (filter.mode == REQUIRED) {

                if (!filter.regularExpression.exactMatch(stringToEvaluate)) {

                    filterSuccessful = false;

                    break;

                } else
                    filterSuccessful = true;

            } else if (filter.mode == OPTIONAL) {

                if (filter.regularExpression.exactMatch(stringToEvaluate))
                    filterSuccessful = true;

            }

        }

        if (filterSuccessful)
            indexesToSelect << d_parameters.selectedIdentifiersAndIndexes.second.at(i);

        if (QThread::currentThread()->isInterruptionRequested()) {

            emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

            emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    header.deselectAll();

    header.setSelectionStatuses(indexesToSelect, true);

    emit stopProgress(0);

    emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    QThread::currentThread()->quit();

}
