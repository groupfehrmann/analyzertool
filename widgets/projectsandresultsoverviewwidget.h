#ifndef PROJECTSANDRESULTSOVERVIEWWIDGET_H
#define PROJECTSANDRESULTSOVERVIEWWIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include <QUuid>

#include "base/projectlistmodel.h"
#include "base/resultlistmodel.h"
#include "base/basedataset.h"
#include "base/project.h"
#include "base/result.h"
#include "base/baseresultitem.h"
#include "base/textresultitem.h"
#include "base/tableresultitem.h"
#include "base/plotresultitem.h"

namespace Ui {

class ProjectsAndResultsOverviewWidget;

}

class ProjectsAndResultsOverviewWidget : public QWidget
{

    Q_OBJECT

public:

    enum ItemType {

        PROJECT = 0,

        DATASET = 1,

        RESULT = 2,

        TEXTRESULTITEM = 3,

        TABLERESULTITEM = 4,

        PLOTRESULTITEM = 5,

        INVALID = 6

    };

    explicit ProjectsAndResultsOverviewWidget(QWidget *parent = nullptr);

    ~ProjectsAndResultsOverviewWidget();

    QSharedPointer<BaseDataset> currentDataset() const;

    QSharedPointer<PlotResultItem> currentPlotResultItem() const;

    QSharedPointer<Project> currentProject() const;

    QSharedPointer<BaseResultItem> currentResultItem() const;

    QSharedPointer<Result> currentResult() const;

    bool currentSelectedOnly() const;

    ItemType currentSelectedItemType() const;

    const ProjectListModel *projectListModel() const;

private:

    Ui::ProjectsAndResultsOverviewWidget *ui;

    ProjectListModel *d_projectListModel;

    ResultListModel *d_resultListModel;

public slots:

    void addDataset(BaseDataset *baseDataset);

    void addProject(Project *project);

    void addResult(Result *result);

    void addResultItem(BaseResultItem *baseResultItem);

    void removeAll();

    void removeCurrentSelected();

private slots:

    void comboBox_selectOverview_currentIndexChanged(int index);

    void modelIndexActivated(const QModelIndex &current, const QModelIndex &previous) const;

signals:

    void datasetAdded(const QSharedPointer<BaseDataset> &baseDataset);

    void datasetNameChanged(const QUuid &uuid, const QString &name);

    void datasetRemoved(const QUuid &uuid);

    void datasetSelected() const;

    void itemActivated(const QUuid &uuid) const;

    void noValidItemSelected() const;

    void projectAdded(const QSharedPointer<Project> &project);

    void projectNameChanged(const QUuid &uuid, const QString &name);

    void projectRemoved(const QUuid &uuid);

    void projectSelected() const;

    void resultAdded(const QSharedPointer<Result> &result);

    void resultItemAdded(const QSharedPointer<BaseResultItem> &baseResultItem);

    void resultItemNameChanged(const QUuid &uuid, const QString &name);

    void resultItemRemoved(const QUuid &uuid);

    void resultItemSelected() const;

    void resultNameChanged(const QUuid &uuid, const QString &name);

    void resultRemoved(const QUuid &uuid);

    void resultSelected() const;

    void showSelectedOnly(const QUuid &uuid, bool showSelectedOnly);

};

#endif // PROJECTSANDRESULTSOVERVIEWWIDGET_H
