#ifndef SCATTERCHART_GENOMICMAPPINGONXAXIS_HOMOSAPIENS_H
#define SCATTERCHART_GENOMICMAPPINGONXAXIS_HOMOSAPIENS_H

#include <QObject>
#include <QChart>
#include <QPolarChart>
#include <QStringList>
#include <QEventLoop>
#include <QScatterSeries>
#include <QCategoryAxis>

#include "charts/exportplottofile.h"
#include "charts/basechart.h"

using namespace QtCharts;

class ScatterChart_GenomicMappingOnXAxis_HomoSapiens : public BaseChart
{

    Q_OBJECT

public:

    explicit ScatterChart_GenomicMappingOnXAxis_HomoSapiens(QObject *parent = nullptr, const QString &title = QString(), const QVector<int> &chromosomeMappingsForAllSubsets = QVector<int>(), const QVector<double> &rankedCumulativeBasepairMappingForAllItems = QVector<double>(), double minimumBasepair = 1.0, double maximumBasepair = 3095677412.0, const QStringList &subsetIdentifiers = QStringList(), const QVector<QVector<double> > &basepairMappings_xAxes = QVector<QVector<double> >(), const QVector<QVector<double> > &values_yAxes = QVector<QVector<double> >(), QChart::ChartType chartType = QChart::ChartTypeCartesian, bool seperateYAxisForEachSubset = false);

    explicit ScatterChart_GenomicMappingOnXAxis_HomoSapiens(QObject *parent = nullptr, const QString &title = QString(), const QVector<int> &chromosomeMappingsForAllSubsets = QVector<int>(), const QVector<double> &rankedCumulativeBasepairMappingForAllItems = QVector<double>(), double minimumBasepair = 1.0, double maximumBasepair = 3095677412.0, const QVector<double> &basepairMappings_xAxis = QVector<double>(), const QVector<double> &values_yAxis = QVector<double> (), QChart::ChartType chartType = QChart::ChartTypeCartesian, bool seperateYAxisForEachSubset = false);

private:

    QChart::ChartType d_chartType;

    bool d_seperateYAxisForEachSubset;

signals:

    void initializeChart(const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QVector<double> &rankedCumulativeBasepairMappingForAllItems, double minimumBasepair, double maximumBasepair, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &basepairMappings_xAxes, const QVector<QVector<double> > &values_yAxes);

private slots:

    void _initializeChart(const QString &title, const QVector<int> &chromosomeMappingsForAllSubsets, const QVector<double> &rankedCumulativeBasepairMappingForAllItems, double minimumBasepair, double maximumBasepair, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &basepairMappings_xAxes, const QVector<QVector<double> > &values_yAxes);

};

#endif // SCATTERCHART_GENOMICMAPPINGONXAXIS_HOMOSAPIENS_H
