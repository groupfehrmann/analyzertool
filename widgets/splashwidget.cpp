#include "splashwidget.h"
#include "ui_splashwidget.h"

SplashWidget::SplashWidget(QWidget *parent) :
    QWidget(parent), ui(new Ui::SplashWidget)
{

    ui->setupUi(this);

    ui->label3->setText("Last modified on " + QCoreApplication::applicationVersion());

}

SplashWidget::~SplashWidget()
{

    delete ui;

}
