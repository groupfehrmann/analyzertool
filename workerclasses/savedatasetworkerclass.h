#ifndef SAVEDATASETWORKERCLASS_H
#define SAVEDATASETWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QFile>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"

class SaveDatasetWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

        QSharedPointer<BaseDataset> datasetToSave;

    };

    SaveDatasetWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    SaveDatasetWorkerClass::Parameters d_parameters;

};

#endif // SAVEDATASETWORKERCLASS_H
