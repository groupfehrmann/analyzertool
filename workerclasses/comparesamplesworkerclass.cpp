#include "comparesamplesworkerclass.h"

CompareSamplesWorkerClass::CompareSamplesWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CompareSamplesWorkerClass::Parameters *>(parameters))
{

}

bool CompareSamplesWorkerClass::checkInputParameters()
{

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected");

        return false;

    }

    if ((d_parameters.baseStatisticalTest->minimumNumberOfSamples() > d_parameters.itemSampleIdentifiers.size()) || (d_parameters.baseStatisticalTest->maximumNumberOfSamples() < d_parameters.itemSampleIdentifiers.size())) {

        emit appendLogItem(LogListModel::ERROR, "invalid number of sample identifiers selected: " + QString::number(d_parameters.itemSampleIdentifiers.size()) + "; statistical test \"" + d_parameters.baseStatisticalTest->nameOfStatisticalTest() + "\" requires a minimum of " + QString::number(d_parameters.baseStatisticalTest->minimumNumberOfSamples()) + " and a maximum of " + QString::number(d_parameters.baseStatisticalTest->maximumNumberOfSamples()));

        return false;

    }

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty() && (d_parameters.itemSubsetIdentifiers.size() < 2)) {

        appendLogItem(LogListModel::ERROR, "minimum of two item subset identifiers need to be selected");

        return false;

    }

    if (!d_parameters.annotationLabelDefiningStrata.isEmpty() && (d_parameters.strataIdentifiers.size() < 2)) {

        appendLogItem(LogListModel::ERROR, "minimum of two strata identifiers need to be selected");

        return false;

    }

    if ((d_parameters.comparisonDescription == "Compare samples (categorical)") && d_parameters.categoryIdentifiers.isEmpty()){

        appendLogItem(LogListModel::ERROR, "no category identifiers selected");

        return false;

    }

    return true;

}

void CompareSamplesWorkerClass::doWork()
{

    emit processStarted(d_parameters.comparisonDescription);

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, d_parameters.comparisonDescription));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "annotation label defining item samples: \"" + d_parameters.annotationLabelDefiningItemSamples + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "item sample identifiers = " + ConvertFunctions::toString(d_parameters.itemSampleIdentifiers));

    if (!d_parameters.annotationLabelDefiningPairedItems.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining paired items: \"" + d_parameters.annotationLabelDefiningPairedItems + "\"");

    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining item subsets: \"" + d_parameters.annotationLabelDefiningItemSubsets + "\"");

        emit appendLogItem(LogListModel::PARAMETER, "item subset identifiers: " + ConvertFunctions::toString(d_parameters.itemSubsetIdentifiers));

    } else
        d_parameters.itemSubsetIdentifiers << QString();

    if (!d_parameters.annotationLabelDefiningStrata.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining strata identifiers: \"" + d_parameters.annotationLabelDefiningStrata + "\"");

        emit appendLogItem(LogListModel::PARAMETER, "strata identifiers: " + ConvertFunctions::toString(d_parameters.strataIdentifiers));

    }

    if (!d_parameters.categoryIdentifiers.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "selected category identifiers: " + ConvertFunctions::toString(d_parameters.categoryIdentifiers));

    emit appendLogItem(LogListModel::PARAMETER, "name of statistical test selected: \"" + d_parameters.nameOfStatisticalTest + "\"");

    if (d_parameters.performMultivariatePermutationTest) {

        emit appendLogItem(LogListModel::PARAMETER, "multivariate permutation test: false discovery rate = " + QString::number(d_parameters.falseDiscoveryRate) + "%");

        emit appendLogItem(LogListModel::PARAMETER, "multivariate permutation test: confidence interval = " + QString::number(d_parameters.confidenceLevel) + "%");

        emit appendLogItem(LogListModel::PARAMETER, "multivariate permutation test: number of permutations = " + QString::number(d_parameters.numberOfPermutations));

    }

    if (!d_parameters.annotationLabelsDefiningAnnotationValuesForVariables.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation labels defining annotation values for selected variables = " + ConvertFunctions::toString(d_parameters.annotationLabelsDefiningAnnotationValuesForVariables));

    if (!d_parameters.exportDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    emit startProgress(0, "Obtaining sample annotations", 0, 0);

    QHash<QString, std::tuple<QList<int>, QVector<QString>, QVector<QString>, QVector<QString> > > itemSubsetIdentifierTo_indexes_itemSampleIdentifier_itemPairIdentifier_itemStrataIdentifier = ConvertFunctions::toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple(d_parameters.baseDataset->indexToAnnotationValues<QString, QString, QString, QString>(d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.annotationLabelDefiningItemSubsets, QSet<QString>(d_parameters.itemSubsetIdentifiers.begin(), d_parameters.itemSubsetIdentifiers.end()), d_parameters.annotationLabelDefiningItemSamples, QSet<QString>(d_parameters.itemSampleIdentifiers.begin(), d_parameters.itemSampleIdentifiers.end()), d_parameters.annotationLabelDefiningPairedItems, QSet<QString>(), d_parameters.annotationLabelDefiningStrata, QSet<QString>(d_parameters.strataIdentifiers.begin(), d_parameters.strataIdentifiers.begin()), BaseMatrix::switchOrientation(d_parameters.orientation)));

    emit stopProgress(0);

    emit startProgress(0, "Validating item annotations", 0, 0);

    for (int indexOfItemSubsetIdentifier = d_parameters.itemSubsetIdentifiers.size() - 1; indexOfItemSubsetIdentifier >= 0 ; --indexOfItemSubsetIdentifier) {

        QString itemSubsetIdentifier = d_parameters.itemSubsetIdentifiers.at(indexOfItemSubsetIdentifier);


        QVector<QString> unsignedIntToItemPairIdentifier;

        QVector<QString> unsignedIntToItemStrataIdentifier;

        QVector<unsigned int> itemSampleCodedIdentifiers = ConvertFunctions::recodeToVectorOfUnsignedInt(std::get<1>(itemSubsetIdentifierTo_indexes_itemSampleIdentifier_itemPairIdentifier_itemStrataIdentifier.value(itemSubsetIdentifier)), d_parameters.itemSampleIdentifiers);

        QVector<unsigned int> itemPairCodedIdentifiers;

        if (!d_parameters.annotationLabelDefiningPairedItems.isEmpty())
            itemPairCodedIdentifiers = ConvertFunctions::recodeToVectorOfUnsignedInt(std::get<2>(itemSubsetIdentifierTo_indexes_itemSampleIdentifier_itemPairIdentifier_itemStrataIdentifier.value(itemSubsetIdentifier)), unsignedIntToItemPairIdentifier);

        QVector<unsigned int> itemStrataCodedIdentifiers;

        if (!d_parameters.annotationLabelDefiningStrata.isEmpty())
            itemStrataCodedIdentifiers = ConvertFunctions::recodeToVectorOfUnsignedInt(std::get<3>(itemSubsetIdentifierTo_indexes_itemSampleIdentifier_itemPairIdentifier_itemStrataIdentifier.value(itemSubsetIdentifier)), unsignedIntToItemStrataIdentifier);

        d_itemSubsetIdentifierTo_indexes_itemSampleCodedIdentifiers_itemPairCodedIdentifiers_itemStrataCodedIdentifiers.insert(itemSubsetIdentifier, std::make_tuple(std::get<0>(itemSubsetIdentifierTo_indexes_itemSampleIdentifier_itemPairIdentifier_itemStrataIdentifier.value(itemSubsetIdentifier)), itemSampleCodedIdentifiers, itemPairCodedIdentifiers, itemStrataCodedIdentifiers));

        bool valid = true;

        if (!d_parameters.annotationLabelDefiningPairedItems.isEmpty()) {

            QVector<QVector<bool> > checkMatrix(unsignedIntToItemPairIdentifier.size(), QVector<bool>(d_parameters.itemSampleIdentifiers.size(), false));

            for (int i = 0; i < itemPairCodedIdentifiers.size(); ++i) {

                if (!checkMatrix[static_cast<int>(itemPairCodedIdentifiers.at(i))][static_cast<int>(itemSampleCodedIdentifiers.at(i))])
                    checkMatrix[static_cast<int>(itemPairCodedIdentifiers.at(i))][static_cast<int>(itemSampleCodedIdentifiers.at(i))] = true;
                else {

                    emit appendLogItem(LogListModel::WARNING, (d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? QString() : "for item subset \"" + itemSubsetIdentifier + "\": ") + "item pair identifier \"" + unsignedIntToItemPairIdentifier.at(static_cast<int>(itemPairCodedIdentifiers.at(i))) + "\" was detected more than once for item sample identifier \"" + d_parameters.itemSampleIdentifiers.at(static_cast<int>(itemSampleCodedIdentifiers.at(i))) + "\"");

                    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
                        emit appendLogItem(LogListModel::WARNING, "removing item subset \"" + itemSubsetIdentifier + "\" for further analysis");

                    valid = false;

                }

            }

            if (valid) {

                for (int i = 0; i < checkMatrix.size(); ++i) {

                    if (int index = checkMatrix.at(i).indexOf(false)) {

                        emit appendLogItem(LogListModel::WARNING, (d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? QString() : "for item subset \"" + itemSubsetIdentifier + "\": ") + "item pair identifier \"" + unsignedIntToItemPairIdentifier.at(static_cast<int>(itemPairCodedIdentifiers.at(i))) + "\" was not detected for item sample identifier \"" + d_parameters.itemSampleIdentifiers.at(static_cast<int>(itemSampleCodedIdentifiers.at(index))) + "\"");

                        if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
                            emit appendLogItem(LogListModel::WARNING, "removing item subset \"" + itemSubsetIdentifier + "\" for further analysis");

                        valid = false;

                    }

                }

            }

        }

        if (valid) {

            for (int i = 0; i < d_parameters.itemSampleIdentifiers.size(); ++i) {

                if (itemSampleCodedIdentifiers.count(static_cast<unsigned int>(i)) < d_parameters.baseStatisticalTest->minimumNumberOfItemsPerSample()) {

                    emit appendLogItem(LogListModel::WARNING, (d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? QString() : "for item subset \"" + itemSubsetIdentifier + "\": ") + "there are not enough items in sample \"" + d_parameters.itemSampleIdentifiers.at(i) + "\", a minimum of " + QString::number(d_parameters.baseStatisticalTest->minimumNumberOfItemsPerSample()) + " is required for statistical test \"" + d_parameters.baseStatisticalTest->nameOfStatisticalTest() + "\"");

                    if (!d_parameters.annotationLabelDefiningItemSubsets.isEmpty())
                        emit appendLogItem(LogListModel::WARNING, "removing item subset \"" + itemSubsetIdentifier + "\" for further analysis");

                    valid = false;

                }

            }

        }

        if (!valid)
            d_parameters.itemSubsetIdentifiers.removeAt(indexOfItemSubsetIdentifier);
        else {

            QString str = "item sample assignment summary ->";

            for(int codedItemSampleIdentifier = 0; codedItemSampleIdentifier < d_parameters.itemSampleIdentifiers.size(); ++codedItemSampleIdentifier)
                str += " [\"" + d_parameters.itemSampleIdentifiers.at(codedItemSampleIdentifier) + "\", n = " + QString::number(itemSampleCodedIdentifiers.count(static_cast<unsigned int>(codedItemSampleIdentifier))) + "]";

            emit appendLogItem(LogListModel::MESSAGE, (d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? QString() : "for item subset \"" + itemSubsetIdentifier + "\": ") + str);

            if (!itemPairCodedIdentifiers.isEmpty())
                emit appendLogItem(LogListModel::MESSAGE, (d_parameters.annotationLabelDefiningItemSubsets.isEmpty() ? QString() : "for item subset \"" + itemSubsetIdentifier + "\": ") + "number of paired samples identified is " + QString::number(*std::max_element(itemPairCodedIdentifiers.begin(), itemPairCodedIdentifiers.end()) + 1));

        }

    }

    if (d_parameters.itemSubsetIdentifiers.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no more valid data left to perform samples comparision");

        QThread::currentThread()->quit();

        return;

    }

    emit stopProgress(0);

    switch (d_parameters.baseDataset->baseMatrix()->metaTypeIdentifier()) {

        case QMetaType::Short: this->_doWork<short>(); break;

        case QMetaType::UShort: this->_doWork<unsigned short>(); break;

        case QMetaType::Int: this->_doWork<int>(); break;

        case QMetaType::UInt: this->_doWork<unsigned int>(); break;

        case QMetaType::LongLong: this->_doWork<long long>(); break;

        case QMetaType::ULongLong: this->_doWork<unsigned long long>(); break;

        case QMetaType::Float: this->_doWork<float>(); break;

        case QMetaType::Double: this->_doWork<double>(); break;

        case QMetaType::UChar: this->_doWork<unsigned char>(); break;

        case QMetaType::QChar: this->_doWork<QChar>(); break;

        case QMetaType::QString: this->_doWork<QString>(); break;

        case QMetaType::Bool: this->_doWork<bool>(); break;

        default: break;

    }

    QThread::currentThread()->quit();

}

double CompareSamplesWorkerClass::numberOfPossiblePermutations(const QVector<unsigned int> &itemSampleCodedIdentifiers, const QVector<unsigned int> &itemPairCodedIdentifiers)
{

    auto fac = [](int n) {

        double f = 1.0;

        for (int i = n; i > 1; --i)
            f *= double(i);

        return f;

    };

    unsigned int numberOfitemSampleCodedIdentifiers = *std::max_element(itemSampleCodedIdentifiers.begin(), itemSampleCodedIdentifiers.end()) + 1;

    if (!itemPairCodedIdentifiers.isEmpty())
        return std::pow(fac(static_cast<int>(numberOfitemSampleCodedIdentifiers)), *std::max_element(itemPairCodedIdentifiers.begin(), itemPairCodedIdentifiers.end()) + 1);

    QVector<unsigned int> itemSampleCodedIdentifierToCount(static_cast<int>(numberOfitemSampleCodedIdentifiers), 0.0);

    for (const unsigned int &itemSampleCodedIdentifier : itemSampleCodedIdentifiers)
        itemSampleCodedIdentifierToCount[static_cast<int>(itemSampleCodedIdentifier)]++;

    double _numberOfPossiblePermutations = 1.0;

    for (const unsigned int &count : itemSampleCodedIdentifierToCount)
        _numberOfPossiblePermutations *= fac(static_cast<int>(count));

    _numberOfPossiblePermutations = fac(itemSampleCodedIdentifiers.size()) / _numberOfPossiblePermutations;

    if (std::isinf(_numberOfPossiblePermutations))
        return std::numeric_limits<double>::max();
    else
        return _numberOfPossiblePermutations;

}
