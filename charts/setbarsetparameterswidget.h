#ifndef SETBARSETPARAMETERSWIDGET_H
#define SETBARSETPARAMETERSWIDGET_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QBarSet>
#include <QLineEdit>

#include "charts/setbrushparameterstreewidgetitem.h"
#include "charts/setpenparameterstreewidgetitem.h"
#include "charts/selectfontwidget.h"
#include "charts/selectcolorwidget.h"

using namespace QtCharts;

class SetBarSetParametersWidget : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetBarSetParametersWidget(QTreeWidget *parent = nullptr, const QString &label = "Bar set", QBarSet *barSet = nullptr);

    explicit SetBarSetParametersWidget(QTreeWidgetItem *parent = nullptr, const QString &label = "Bar set", QBarSet *barSet = nullptr);

    ~SetBarSetParametersWidget();

public slots:

    void setBarSet(const QString &label, QBarSet *barSet);

private:

    void initialize(const QString &label);

    QBarSet *d_barSet;

    bool d_initialized;

    SetBrushParametersTreeWidgetItem *d_setBrushParametersTreeWidgetItem;

    QLineEdit *d_setLabelWidget;

    SelectColorWidget *d_selectLabelColorWidget;

    SelectFontWidget *d_selectLabelFontWidget;

    SetPenParametersTreeWidgetItem *d_setPenParametersTreeWidgetItem;

private slots:

    void setBrush(const QBrush &brush);

    void setLabel(const QString label);

    void setLabelColor(QColor color);

    void setLabelFont(const QFont &font);

    void setPen(const QPen &pen);

};

#endif // SETBARSETPARAMETERSWIDGET_H
