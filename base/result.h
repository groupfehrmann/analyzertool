#ifndef RESULT_H
#define RESULT_H

#include <QObject>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QHash>
#include <QSharedPointer>
#include <QUuid>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>
#include <QDataStream>

#include "base/baseresultitem.h"

class ResultData : public QSharedData
{

public:

    ResultData();

    ResultData(const ResultData &other);

    ~ResultData();

    QHash<QUuid, QSharedPointer<BaseResultItem> > d_uuidToResultItem;

    QList<QUuid> d_uuidOfResultItems;

    QString d_name;

};

class Result : public QObject
{
    Q_OBJECT

public:

    Result(QObject *parent = nullptr, const QString &name = "Result item");

    Result(const Result &other);

    ~Result();

    const QSharedPointer<BaseResultItem> baseResultItemAt(const QUuid &uuid) const;

    const QSharedPointer<BaseResultItem> baseResultItemAt(int index) const;

    const QString &name() const;

    int numberOfResultItems() const;

    const QUuid &universallyUniqueIdentifier() const;

    Result &operator=(const Result &other);

    friend QDataStream& operator<<(QDataStream& out, const Result &result);

    friend QDataStream& operator>>(QDataStream& in, Result &result);

private:

    QSharedDataPointer<ResultData> d_data;

    mutable QReadWriteLock d_readWriteLock;

    QUuid d_uuid;

public slots:

    void appendResultItem(BaseResultItem *baseResultItem);

    void removeAllResultItems();

    void removeResultItem(const QUuid &uuid);

    void removeResultItems(const QList<QUuid> &uuids);

    void removeResultItem(int index);

    void removeResultItems(const QList<int> &indexes);

    void setName(const QString &name);

signals:

    void resultItemRemoved(const QUuid &uuid);

    void resultItemAdded(const QSharedPointer<BaseResultItem> &baseResultItem);

    void resultItemNameChanged(const QUuid &uuid, const QString &name);

    void nameChanged(const QUuid &uuid, const QString &name);

};

#endif // RESULT_H
