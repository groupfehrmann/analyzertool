#ifndef FUNCTIONSFORMULTITHREADING_H
#define FUNCTIONSFORMULTITHREADING_H

#include <QList>
#include <QPair>
#include <QVector>
#include <QFuture>
#include <QFutureSynchronizer>
#include <QThreadPool>
#include <QtConcurrent>

#include <cstdlib>
#include <random>

namespace FunctionsForMultiThreading {

    QList<QPair<int, int> > createIndexBlocksForMultiThreading(int sizeOfSequence, int numberOfThreads);

    QVector<QVector<int> > createIndexSchedulerForMultiThreading(int sizeOfSequence, int numberOfThreads);

    template <typename T>
    QVector<QVector<T> > &detachMatrix(QVector<QVector<T> > &matrix) {

        for (QVector<T> &vector : matrix)
            vector.detach();

        matrix.detach();

        return matrix;

    }

    QVector<int> createSequenceOfIndexes(int numberOfIndexes, int startIndex, int stride, bool shuffle = false);

}

#endif // FUNCTIONSFORMULTITHREADING_H
