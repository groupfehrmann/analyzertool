#ifndef IMPORTANNOTATIONSWORKERCLASS_H
#define IMPORTANNOTATIONSWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QFile>
#include <QSharedPointer>
#include <QStringList>
#include <QSet>

#include "base/basedataset.h"
#include "base/dataset.h"
#include "base/basematrix.h"
#include "base/baseworkerclass.h"
#include "widgets/selectdelimiterbuildingblockwidget.h"

class ImportAnnotationsWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        BaseMatrix::Orientation orientation;

        QString pathToFile;

        int lineNumberHeaderLine;

        int lineNumberFirstDataLine;

        int lineNumberLastDataLine;

        SelectDelimiterBuildingBlockWidget::DelimiterType delimiterType;

        QString custumDelimiter;

        Qt::CaseSensitivity caseSensitivityForCustumDelimiter;

        QPair<int, QString> indexAndLabelForColumnDefiningRowIdentifiers;

        QPair<int, QString> indexAndLabelForColumnDefiningFirstDataColumn;

        QPair<int, QString> indexAndLabelForColumnDefiningSecondDataColumn;

    };

    ImportAnnotationsWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    ImportAnnotationsWorkerClass::Parameters d_parameters;

    QFile d_fileIn;

private slots:

    void updateFileProgress();

};

#endif // IMPORTANNOTATIONSWORKERCLASS_H
