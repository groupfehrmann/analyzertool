#ifndef DistanceCorrelation_H
#define DistanceCorrelation_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "math/statfunctions.h"
#include "math/fastdistancecorrelation.h"

template <typename T>
class DistanceCorrelation : public BaseStatisticalTest
{

public:

    DistanceCorrelation();

    explicit DistanceCorrelation(const QVector<T> &independentVector, QVector<T> &dependentVector, bool pairwiseCompleteMethod = false);

    explicit DistanceCorrelation(const QVector<T> &independentVector, QVector<T> &dependentVector, int numberOfPermutations, bool pairwiseCompleteMethod = false);

    ~DistanceCorrelation();

    double distanceCorrelationValue();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;


    QVector<T> d_independentVector;

    QVector<T> *d_dependentVector;

    QVector<T> _dependentVector;

    int d_numberOfPermutations;

    QVector<double> d_intermediateResults;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation DistanceCorrelation<T>::d_effectSizeTransformation = BaseStatisticalTest::FISHER_R_TO_Z;

template <typename T>
bool DistanceCorrelation<T>::d_hasEffectSize = true;

template <typename T>
QStringList DistanceCorrelation<T>::d_sampleDescriptiveLabels = {"Count"};

template <typename T>
int DistanceCorrelation<T>::d_maximumNumberOfSamples = 1;

template <typename T>
int DistanceCorrelation<T>::d_minimumNumberOfSamples = 1;

template <typename T>
int DistanceCorrelation<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString DistanceCorrelation<T>::d_nameOfEffectSize = "Fisher's z' transformation";

template <typename T>
QString DistanceCorrelation<T>::d_nameOfTransformedEffectSize = "Fisher's z' transformation";

template <typename T>
QString DistanceCorrelation<T>::d_nameOfStatisticalTest = "Distance R";

template <typename T>
bool DistanceCorrelation<T>::d_rankedBased = false;

template <typename T>
QStringList DistanceCorrelation<T>::d_testDescriptiveLabels = {"Fisher's z' transformation", "Standard error Fisher's z' transformation", "Distance R", "Lower bound 95% confidence interval of Distance R", "Upper bound 95% confidence interval of Distance R", "Statistic", "Significance"};

template <typename T>
DistanceCorrelation<T>::DistanceCorrelation() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_dependentVector(&_dependentVector)
{

}

template <typename T>
DistanceCorrelation<T>::DistanceCorrelation(const QVector<T> &independentVector, QVector<T> &dependentVector, bool pairwiseCompleteMethod) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_independentVector(independentVector), d_dependentVector(&dependentVector), d_numberOfPermutations(1000)
{

    if (pairwiseCompleteMethod) {

        d_independentVector.clear();

        for (int i = 0; i < dependentVector.size(); ++i) {

            if (std::isfinite(dependentVector.at(i)) && std::isfinite(independentVector.at(i))) {

                _dependentVector << dependentVector.at(i);

                d_independentVector << independentVector.at(i);

            }

        }

        d_dependentVector = &_dependentVector;

    }

}

template <typename T>
DistanceCorrelation<T>::DistanceCorrelation(const QVector<T> &independentVector, QVector<T> &dependentVector, int numberOfPermutations, bool pairwiseCompleteMethod) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_independentVector(independentVector), d_dependentVector(&dependentVector), d_numberOfPermutations(numberOfPermutations)
{

    if (pairwiseCompleteMethod) {

        d_independentVector.clear();

        for (int i = 0; i < dependentVector.size(); ++i) {

            if (!std::isnan(dependentVector.at(i)) && !std::isnan(independentVector.at(i))) {

                _dependentVector << dependentVector.at(i);

                d_independentVector << independentVector.at(i);

            }

        }

        d_dependentVector = &_dependentVector;

    }

}

template <typename T>
DistanceCorrelation<T>::~DistanceCorrelation()
{

}

template <typename T>
double DistanceCorrelation<T>::distanceCorrelationValue()
{

    this->statistic();

    double xDistanceVariance = d_intermediateResults.at(1);

    double yDistanceVariance = d_intermediateResults.at(2);

    double temp = std::sqrt(d_intermediateResults.at(0) / std::sqrt(xDistanceVariance * yDistanceVariance));

    if (std::isnan(temp) || std::isinf(temp))
        temp = 0.0;

    if (temp < 0.0)
        temp = 0.0;
    else if (temp > 1.0)
        temp = 1.0;

    return temp;
}

template <typename T>
QVector<QVariant> DistanceCorrelation<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    sampleDescriptiveValues << d_independentVector.size();

    return sampleDescriptiveValues;

}

template <typename T>
void DistanceCorrelation<T>::calculateStatistic()
{

    d_intermediateResults = FastDistanceCorrelation::fastDistanceCovariance(d_independentVector, *d_dependentVector, false, true);

    d_statistic = static_cast<double>(d_independentVector.size() * d_intermediateResults.at(0));

}

template <typename T>
void DistanceCorrelation<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    QVector<double> empiricalNullDistribution(d_numberOfPermutations);

    QVector<double> copyOfDependentVector = *d_dependentVector;

    std::random_device rd;

    std::mt19937 g(rd());

    for (int i = 0; i < d_numberOfPermutations; ++i) {

        std::shuffle(copyOfDependentVector.begin(), copyOfDependentVector.end(), g);

        while (copyOfDependentVector == *d_dependentVector)
            std::shuffle(copyOfDependentVector.begin(), copyOfDependentVector.end(), g);

        empiricalNullDistribution[i] = FastDistanceCorrelation::fastDistanceCorrelation(d_independentVector, copyOfDependentVector);

    }

    d_pValue = StatFunctions::qromb([&](const double &value) {return StatFunctions::gaussianKernelDensityEstimator(value, empiricalNullDistribution, 0.01);}, this->distanceCorrelationValue(), 1.0, 1.0e-5);

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void DistanceCorrelation<T>::calculateEffectSize()
{

    double value = double(1.0 + d_intermediateResults.at(1)) / double(1.0 - d_intermediateResults.at(1));

    if (std::isinf(value))
        value = std::numeric_limits<double>::max();

    d_effectSize = 0.5 * std::log(value);

}

template <typename T>
void DistanceCorrelation<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = (1.0 / std::sqrt(d_independentVector.size() - 3.0));

}

template <typename T>
QVector<QVariant> DistanceCorrelation<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    boost::math::normal normalDistribution(0.0, 1.0);

    double w = boost::math::quantile(boost::math::complement(normalDistribution, 0.025)) * d_standardErrorOfEffectSize;

    double rLower = (std::exp(2.0 * (d_effectSize - w)) - 1.0) / (std::exp(2.0 * (d_effectSize - w)) + 1.0);

    double temp = std::exp(2.0 * (d_effectSize + w));

    if (std::isinf(temp) || (std::fabs(temp - std::numeric_limits<double>::max()) <= std::numeric_limits<double>::epsilon()))
        temp = std::numeric_limits<double>::max() - 1.0;

    double rUpper = (temp - 1.0) / (temp + 1.0);

    descriptiveValues << d_effectSize << d_standardErrorOfEffectSize << this->distanceCorrelationValue() << rLower << rUpper << d_statistic << d_pValue;

    return descriptiveValues;

}

template <typename T>
void DistanceCorrelation<T>::createStructuredMeasurements()
{

}

template <typename T>
void DistanceCorrelation<T>::_reset()
{

}

#endif // DistanceCorrelation_H
