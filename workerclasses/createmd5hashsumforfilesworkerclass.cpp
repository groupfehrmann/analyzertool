#include "createmd5hashsumforfilesworkerclass.h"

CreateMD5HashSumForFilesWorkerClass::CreateMD5HashSumForFilesWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CreateMD5HashSumForFilesWorkerClass::Parameters *>(parameters))
{

}

void CreateMD5HashSumForFilesWorkerClass::doWork()
{

    emit processStarted("Create MD5 hash sum for files");

    emit appendLogItem(LogListModel::PARAMETER, "import directory: " + d_parameters.importDirectory);

    emit appendLogItem(LogListModel::PARAMETER, "pattern to filter valid files: \"" + d_parameters.patternToFilterValidFiles +"\"");


    if (!QDir(d_parameters.importDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid import directory : \"" + d_parameters.importDirectory + "\"");

        QThread::currentThread()->quit();

        return;

    }

    if ((!d_parameters.exportDirectory.isEmpty() || d_parameters.copyUniqueFilesToExportDirectory) && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Scanning for files with pattern \"" + d_parameters.patternToFilterValidFiles + "\"", 0, 0);

    QStringList pathsToValidFiles = this->getPathsToValidFilesInDirectory(d_parameters.importDirectory, d_parameters.patternToFilterValidFiles);

    emit stopProgress(0);

    QHash<QByteArray, QStringList> MD5HashSumToFiles;

    emit startProgress(0, "Processing valid files", 0, pathsToValidFiles.size());

    for (int i = 0; i < pathsToValidFiles.size(); ++i) {

        QFile file(pathsToValidFiles.at(i));

        if (file.open(QIODevice::ReadOnly)) {

            QCryptographicHash cryptographicHash(QCryptographicHash::Md5);

            cryptographicHash.addData(&file);

            if (d_parameters.copyUniqueFilesToExportDirectory && !MD5HashSumToFiles.contains(cryptographicHash.result())) {

                if (!file.copy(d_parameters.exportDirectory + "/" + QFileInfo(file).fileName()))
                    emit appendLogItem(LogListModel::ERROR, "failed to copy \"" + pathsToValidFiles.at(i) + "\" to \"" + d_parameters.exportDirectory + "/" + QFileInfo(file).fileName() + "\" with error \"" + file.errorString() + "\"");

            }

            MD5HashSumToFiles[cryptographicHash.result()] << pathsToValidFiles.at(i);

        } else
            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + pathsToValidFiles.at(i) + "\"");

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit appendLogItem(LogListModel::MESSAGE, "total number of files identified: " + QString::number(pathsToValidFiles.size()));

    emit appendLogItem(LogListModel::MESSAGE, "total number of unique files identified: " + QString::number(MD5HashSumToFiles.size()));

    emit startProgress(0, "Exporting file to MD5 hash sum table", 0, 0);


    QHashIterator<QByteArray, QStringList> it(MD5HashSumToFiles);

    QVector<QVector<QVariant> > fileToMD5HashSum;

    QStringList verticalLabels;

    while (it.hasNext()) {

        it.next();

        for (const QString &path: it.value()) {

            verticalLabels << QString::number(verticalLabels.size() + 1);

            fileToMD5HashSum << QVector<QVariant>({path, it.key().toHex().constData()});

        }

    }

    if (!this->processTableResult("File to MD5 hash sum table", {"Path", "MD5 hash sum"}, verticalLabels, fileToMD5HashSum, d_parameters.exportDirectory))
        return;

    emit stopProgress(0);

    QThread::currentThread()->quit();

}

QStringList CreateMD5HashSumForFilesWorkerClass::getPathsToValidFilesInDirectory(const QString &importDirectory, const QString &patternToFilterValidFiles)
{

    QRegExp regExp(patternToFilterValidFiles, Qt::CaseInsensitive, QRegExp::Wildcard);

    QDir directory(importDirectory);

    QFileInfoList fileInfoList = directory.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDir::Unsorted);

    QStringList pathsToValidFiles;

    unsigned int numberOfValidFilesDetectedInDirectory = 0;

    for (const QFileInfo &fileInfo: fileInfoList) {

        if (fileInfo.isDir())
            pathsToValidFiles << this->getPathsToValidFilesInDirectory(fileInfo.absoluteFilePath(), patternToFilterValidFiles);
        else {

            if (regExp.exactMatch(fileInfo.absoluteFilePath())) {

                pathsToValidFiles << fileInfo.absoluteFilePath();

                ++numberOfValidFilesDetectedInDirectory;

            }

        }
    }

    emit appendLogItem(LogListModel::MESSAGE, "detected " + QString::number(numberOfValidFilesDetectedInDirectory) + " files with suffix \"" + patternToFilterValidFiles + "\" in directory \"" + importDirectory + "\"");

    return pathsToValidFiles;

}
