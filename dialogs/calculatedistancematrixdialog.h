#ifndef CALCULATEDISTANCEMATRIXDIALOG_H
#define CALCULATEDISTANCEMATRIXDIALOG_H

#include "base/projectlist.h"
#include "dialogs/basedialog.h"
#include "workerclasses/calculatedistancematrixworkerclass.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "widgets/spinboxbuildingblockwidget.h"
#include "base/projectlistmodel.h"

class CalculateDistanceMatrixDialog : public BaseDialog
{

    Q_OBJECT

public:

    CalculateDistanceMatrixDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), const ProjectListModel *projectListModel = nullptr);


    ~CalculateDistanceMatrixDialog();

private:

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_dataSet1;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget_dataSet1;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables1;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items1;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_dataSet2;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget_dataSet2;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables2;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items2;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_distanceFunction;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_calculatePValues;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_numberOfPermutations;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    const ProjectListModel * d_projectListModel;

    QList<QSharedPointer<BaseDataset> > d_baseDatasets;

    QStringList d_baseDatasetDescriptions;

    QList<bool> d_selectedOnlyStatusOfBaseDatasets;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationDataset1Changed(BaseMatrix::Orientation orientation);

    void orientationDataset2Changed(BaseMatrix::Orientation orientation);

    void setEnableStatusOfSpinBoxNumberOfPermutations();

    void comboBoxBuildingBlockWidget_dataSet1_currentIndexChanged(int index);

    void comboBoxBuildingBlockWidget_dataSet2_currentIndexChanged(int index);

};

#endif // CALCULATEDISTANCEMATRIXDIALOG_H
