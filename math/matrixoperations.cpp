#include "math/matrixoperations.h"

QVector<double> MatrixOperations::columnVector(const QVector<QVector<double> > &matrix, int columnIndex)
{

    QVector<double> _columnVector;

    _columnVector.reserve(matrix.size());

    for (const QVector<double> &vector : matrix)
        _columnVector << vector.at(columnIndex);

    return _columnVector;

}

QVector<double> MatrixOperations::rowVector_x_matrix(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    QVector<double> result(matrix.first().size(), 0.0);

    for (int i = 0; i < rowVector.size(); ++i) {

        const double &temp(rowVector.at(i));

        const QVector<double> &currentRowVectorOfMatrix(matrix.at(i));

        for (int j = 0; j < currentRowVectorOfMatrix.size(); ++j)
            result[j] += currentRowVectorOfMatrix.at(j) * temp;

    }

    return result;

}

QVector<double> MatrixOperations::rowVector_x_matrix_multiThreaded(const QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    auto mapFunctor = [&](const int &index) {

        return VectorOperations::vsMul(matrix.at(index), rowVector.at(index));

    };

    auto reduceFunctor = [](QVector<double> &result, const QVector<double> &intermediateResult) {

        VectorOperations::vsMul_vsMul_vvAdd_inplace(result, 1.0, intermediateResult, 1.0);

    };

   return QtConcurrent::blockingMappedReduced<QVector<double> >(FunctionsForMultiThreading::createSequenceOfIndexes(rowVector.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor), std::function<void(QVector<double> &, const QVector<double> &)>(reduceFunctor));

}

QVector<double> &MatrixOperations::rowVector_x_matrix_inplace(QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    rowVector = MatrixOperations::rowVector_x_matrix(rowVector, matrix);

    return rowVector;

}

QVector<double> &MatrixOperations::rowVector_x_matrix_inplace_multiThreaded(QVector<double> &rowVector, const QVector<QVector<double> > &matrix)
{

    rowVector = MatrixOperations::rowVector_x_matrix_multiThreaded(rowVector, matrix);

    return rowVector;

}

QVector<double> MatrixOperations::matrix_x_columnVector(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector)
{

    QVector<double> result;

    result.reserve(matrix.size());

    for (const QVector<double> &currentRowVector : matrix)
        result << static_cast<double>(VectorOperations::dotProduct(currentRowVector, columnVector));

    return result;

}


QVector<double> MatrixOperations::matrix_x_columnVector_multiThreaded(const QVector<QVector<double> > &matrix, const QVector<double> &columnVector)
{

    return QtConcurrent::blockingMapped<QVector<double> >(matrix, std::bind(VectorOperations::dotProduct, std::placeholders::_1, std::cref(columnVector)));

}

QVector<double> &MatrixOperations::matrix_x_columnVector_inplace(const QVector<QVector<double> > &matrix, QVector<double> &columnVector)
{

    columnVector = MatrixOperations::matrix_x_columnVector(matrix, columnVector);

    return columnVector;

}

QVector<double> &MatrixOperations::matrix_x_columnVector_inplace_multiThreaded(const QVector<QVector<double> > &matrix, QVector<double> &columnVector)
{

    columnVector = MatrixOperations::matrix_x_columnVector_multiThreaded(matrix, columnVector);

    return columnVector;

}

QVector<QVector<double> > MatrixOperations::matrix1_x_matrix2(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    QVector<QVector<double> > result;

    result.reserve(matrix1.size());

    for (const QVector<double> &vector : matrix1)
        result << MatrixOperations::rowVector_x_matrix(vector, matrix2);

    return result;

}

QVector<QVector<double> > MatrixOperations::matrix1_x_matrix2_multiThreaded(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    auto mapFunctor = [&](const QVector<double> &vector) {

        return MatrixOperations::rowVector_x_matrix(vector, matrix2);

    };

    return QtConcurrent::blockingMapped<QVector<QVector<double> > >(matrix1, std::function<QVector<double>(const QVector<double> &)>(mapFunctor));

}

QVector<QVector<double> > &MatrixOperations::matrix1_x_matrix2_inplace(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    for (QVector<double> &currentRowVectorOfMatrix1 : matrix1)
        MatrixOperations::rowVector_x_matrix_inplace(currentRowVectorOfMatrix1, matrix2);

    return matrix1;

}


QVector<QVector<double> > &MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2)
{

    QtConcurrent::blockingMap(matrix1, std::bind(MatrixOperations::rowVector_x_matrix_inplace, std::placeholders::_1, std::cref(matrix2)));

    return matrix1;

}

QVector<QVector<double> > MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose(const QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    QVector<QVector<double> > result(matrix.size(), QVector<double>(matrix.size()));

    for (int i = 0; i < matrix.size(); ++i) {

        QVector<double> temp = VectorOperations::vsMul_vsMul_vvMul(matrix.at(i), 1.0, scalars, 1.0);

        QVector<double> &currentRowVectorInResult(result[i]);

        currentRowVectorInResult[i] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(i)));

        for (int j = i + 1; j < matrix.size(); ++j)
            result[j][i] = currentRowVectorInResult[j] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(j)));

    }

    return result;

}

QVector<QVector<double> > MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_multiThreaded(const QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    auto mapFunctor = [&](const int &index) {

        QVector<double> temp = VectorOperations::vsMul_vsMul_vvMul(matrix.at(index), 1.0, scalars, 1.0);

        QVector<double> result(matrix.size());

        result[index] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(index)));

        for (int j = index + 1; j < matrix.size(); ++j)
            result[j] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(j)));

        return result;

    };

    QVector<QVector<double> > result = QtConcurrent::blockingMapped<QVector<QVector<double> > >(FunctionsForMultiThreading::createSequenceOfIndexes(matrix.size(), 0, 1), std::function<QVector<double>(const int &)>(mapFunctor));

    auto mirrorFunctor = [&](int &index) {

        QVector<double> &currentRowInResult(result[index]);

        for (int i = 0; i < index; ++i)
            currentRowInResult[i] = result.at(i).at(index);

    };

    QVector<int> sequenceOfIndexes = FunctionsForMultiThreading::createSequenceOfIndexes(result.size(), 0, 1);

    QtConcurrent::blockingMap(sequenceOfIndexes, mirrorFunctor);

    return result;

}

QVector<QVector<double> > &MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_inplace(QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    for (int i = 0; i < matrix.size(); ++i) {

        QVector<double> intermediateVector(matrix.size());

        QVector<double> temp = VectorOperations::vsMul_vsMul_vvMul(matrix.at(i), 1.0, scalars, 1.0);

        for (int j = 0; j < i; ++j)
            intermediateVector[j] = matrix.at(j).at(i);

        intermediateVector[i] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(i)));

        for (int j = i + 1; j < matrix.size(); ++j)
            intermediateVector[j] = static_cast<double>(VectorOperations::dotProduct(temp, matrix.at(j)));

        matrix[i] = intermediateVector;

    }

    return matrix;

}

QVector<QVector<double> > &MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_inplace_multiThreaded(QVector<QVector<double> > &matrix, const QVector<double> &scalars)
{

    FunctionsForMultiThreading::detachMatrix(matrix);

    QSemaphore semaphore(1);

    QReadWriteLock readWriteLock;

    auto functor = [&](const double &value1, const double &value2){ return value1 * value2;};

    auto processor = [&](const QVector<int> &indexesToProcess) {

        for (const int &indexToProcess : indexesToProcess) {

            semaphore.acquire(indexToProcess + 1);

            readWriteLock.lockForRead();

            QVector<double> intermediateVector(matrix.size());

            QVector<double> currentRowVectorInMatrix = VectorOperations::applyFunctor_elementWise(matrix.at(indexToProcess), scalars, functor);

            intermediateVector[indexToProcess] = static_cast<double>(VectorOperations::dotProduct(currentRowVectorInMatrix, matrix.at(indexToProcess)));

            if (indexToProcess + 1 < matrix.size()) {

                intermediateVector[indexToProcess + 1] = static_cast<double>(VectorOperations::dotProduct(currentRowVectorInMatrix, matrix.at(indexToProcess + 1)));

                semaphore.release(indexToProcess + 2);

            }

            for (int i = indexToProcess + 2; i < matrix.size(); ++i)
                intermediateVector[i] = static_cast<double>(VectorOperations::dotProduct(currentRowVectorInMatrix, matrix.at(i)));

            readWriteLock.unlock();

            readWriteLock.lockForWrite();

            matrix[indexToProcess] = intermediateVector;

            readWriteLock.unlock();

        }

    };


    QFutureSynchronizer<void> futureSynchronizer;

    QVector<QVector<int> > schedule = FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(matrix.size(), QThreadPool::globalInstance()->maxThreadCount());

    for (const QVector<int> &indexesToProcess : schedule)
        futureSynchronizer.addFuture(QtConcurrent::run(processor, indexesToProcess));

    futureSynchronizer.waitForFinished();

    futureSynchronizer.clearFutures();

    auto mirrorFunction = [&](const QVector<int> &indexesToProcess) {

        for (const int &indexToProcess : indexesToProcess) {

            QVector<double> &currentRowVector(matrix[indexToProcess]);

            for (int i = 0; i < indexToProcess; ++i)
                currentRowVector[i] = matrix.at(i).at(indexToProcess);

        }

    };

    for (const QVector<int> &indexesToProcess : schedule)
        futureSynchronizer.addFuture(QtConcurrent::run(mirrorFunction, indexesToProcess));

    futureSynchronizer.waitForFinished();

    return matrix;

}

int MatrixOperations::eigenSym(QVector<QVector<double> > &symmetricMatrixOnInput_eigenvectorsOnOutput, QVector<double> &eigenvalues)
{

    QVector<double> d;

    QVector<double> e;

    MatrixOperations::tred2(symmetricMatrixOnInput_eigenvectorsOnOutput, d, e);

    return MatrixOperations::eigenvaluesAndEigenVectorsOfRealTridiagonalMatrix(d, e, symmetricMatrixOnInput_eigenvectorsOnOutput.size(), eigenvalues, symmetricMatrixOnInput_eigenvectorsOnOutput);

}

int MatrixOperations::eigenSym_multiThreaded(QVector<QVector<double> > &symmetricMatrixOnInput_eigenvectorsOnOutput, QVector<double> &eigenvalues)
{

    QVector<double> d;

    QVector<double> e;

    MatrixOperations::tred2_multiThreaded(symmetricMatrixOnInput_eigenvectorsOnOutput, d, e);

    return MatrixOperations::eigenvaluesAndEigenVectorsOfRealTridiagonalMatrix(d, e, symmetricMatrixOnInput_eigenvectorsOnOutput.size(), eigenvalues, symmetricMatrixOnInput_eigenvectorsOnOutput);

}

int MatrixOperations::eigenvaluesAndEigenVectorsOfRealTridiagonalMatrix(QVector<double> &d, QVector<double> &e, int numberOfComponents, QVector<double> &eigenvalues, QVector<QVector<double> > &realSymmetricTridiagonalMatrixOnInput_eigenvectorsOnOutput)
{

    char jobz = 'v';

    char range = 'i';

    int n = d.size();

    int il = n - numberOfComponents + 1;

    int iu = n;

    int m;

    int ldz = n;

    int lwork = -1;

    int liwork = -1;

    std::vector<int> isuppz(static_cast<unsigned long>(2 * n));

    double vl = 0.0;

    double vu = 0.0;

    double abstol = 0.0;

    std::vector<double> w(static_cast<unsigned long>(n));

    std::vector<double> z(static_cast<unsigned long>(numberOfComponents * n));

    e.remove(0);

    e.append(0.0);

    double optimalLWorkSize;

    int optimalIWorkSize;

    int info;

    #ifdef Q_OS_MAC

    dstegr_(&jobz, &range, &n, d.data(), e.data(), &vl, &vu, &il, &iu, &abstol, &m, w.data(), z.data(), &ldz, isuppz.data(), &optimalLWorkSize, &lwork, &optimalIWorkSize, &liwork, &info);

    #endif

    #ifdef Q_OS_LINUX

    info = LAPACKE_dstegr_work(LAPACK_COL_MAJOR, jobz, range, n, d.data(), e.data(), vl, vu, il, iu, abstol, &m, w.data(), z.data(), ldz, isuppz.data(), &optimalLWorkSize, lwork, &optimalIWorkSize, liwork);

    #endif

    lwork = static_cast<int>(optimalLWorkSize);

    liwork = optimalIWorkSize;

    std::vector<int> iwork(static_cast<unsigned long>(liwork));

    std::vector<double> work(static_cast<unsigned long>(lwork));

    #ifdef Q_OS_MAC

    dstegr_(&jobz, &range, &n, d.data(), e.data(), &vl, &vu, &il, &iu, &abstol, &m, w.data(), z.data(), &ldz, isuppz.data(), work.data(), &lwork, iwork.data(), &liwork, &info);

    #endif

    #ifdef Q_OS_LINUX

    info = LAPACKE_dstegr_work(LAPACK_COL_MAJOR, jobz, range, n, d.data(), e.data(), vl, vu, il, iu, abstol, &m, w.data(), z.data(), ldz, isuppz.data(), work.data(), lwork, iwork.data(), liwork);

    #endif

    d.clear();

    e.clear();

    eigenvalues.resize(numberOfComponents);

    for (int i = numberOfComponents - 1; i >= 0; --i)
        eigenvalues[numberOfComponents - i - 1] = w.at(static_cast<unsigned long>(i));

    QVector<QVector<double> > _z(n);

    for (int l = 0; l < n; ++l) {

        QVector<double> &_z_currentRow(_z[l]);

        for (int i = numberOfComponents - 1; i >= 0; --i)
            _z_currentRow << std::move(z.at(static_cast<unsigned long>(i) * static_cast<unsigned long>(n) + static_cast<unsigned long>(l)));

    }

    MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(realSymmetricTridiagonalMatrixOnInput_eigenvectorsOnOutput, _z);

    return info;

}

QVector<QVector<double> > &MatrixOperations::fillMatrixWithRandomNumbersFromUniformRealDistribution(QVector<QVector<double> > &matrix, const double &minimumValue, const double &maximumValue)
{

    std::default_random_engine generator(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));

    std::uniform_real_distribution<double> distribution(minimumValue, maximumValue);

    for (QVector<double> &vector : matrix) {

        for (double &value : vector)
            value = distribution(generator);

    }

    return matrix;

}

QVector<QVector<double> > &MatrixOperations::mPower_inplace(QVector<QVector<double> > &symmetricRealMatrix, const double &power)
{

    QVector<double> eigenvalues;

    MatrixOperations::eigenSym(symmetricRealMatrix, eigenvalues);

    VectorOperations::applyFunctor_elementWise_inplace(eigenvalues, [&](double &value){value = std::pow(value, power);});

    return MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_inplace(symmetricRealMatrix, eigenvalues);

}

QVector<QVector<double> > &MatrixOperations::mPower_inplace_multiThreaded(QVector<QVector<double> > &symmetricRealMatrix, const double &power)
{

    QVector<double> eigenvalues;

    MatrixOperations::eigenSym_multiThreaded(symmetricRealMatrix, eigenvalues);

    VectorOperations::applyFunctor_elementWise_inplace_multiThreaded(eigenvalues, [&](double &value){value = std::pow(value, power);});

    return MatrixOperations::matrix_x_diagonalizedScalars_x_matrixTranspose_inplace_multiThreaded(symmetricRealMatrix, eigenvalues);

}

int MatrixOperations::singularValueDecomposition(const QVector<QVector<double> > &matrix, QVector<QVector<double> > &u, QVector<double> &s, QVector<QVector<double> > &vTransposed)
{

    char jobz = 'S';

    int m = matrix.size();

    int n = matrix.isEmpty() ? 0 : matrix.first().size();

    double *a = MatrixOperations::matrixToColumnMajorOrderArray(matrix);

    int lda = m;

    s = QVector<double>(std::min(m, n));

    int ldu = m;

    double *_u = new double[ldu * std::min(m, n)];

    int ldvt = std::min(m, n);

    double *_vTransposed = new double[ldvt * n];

    int lwork = -1;

    double optimalSizeOfWork;

    int *iwork = new int[8 * std::min(n, m)];

    int info;

    #ifdef Q_OS_MAC

    dgesdd_(&jobz, &m, &n, a, &lda, s.data(), _u, &ldu, _vTransposed, &ldvt, &optimalSizeOfWork, &lwork, iwork, &info);

    #endif

    #ifdef Q_OS_LINUX

    info = LAPACKE_dgesdd_work(LAPACK_COL_MAJOR, jobz, m, n, a, lda, s.data(), _u, ldu, _vTransposed, ldvt, &optimalSizeOfWork, lwork, iwork);

    #endif

    lwork = static_cast<int>(optimalSizeOfWork);

    double *work = new double[lwork];

    #ifdef Q_OS_MAC

    dgesdd_(&jobz, &m, &n, a, &lda, s.data(), _u, &ldu, _vTransposed, &ldvt, work, &lwork, iwork, &info);

    #endif

    #ifdef Q_OS_LINUX

    info = LAPACKE_dgesdd_work(LAPACK_COL_MAJOR, jobz, m, n, a, lda, s.data(), _u, ldu, _vTransposed, ldvt, work, lwork, iwork);

    #endif

    delete [] work;

    delete [] iwork;

    if (info != 0)
        return info;

    vTransposed = MatrixOperations::columnMajorOrderArrayToMatrix(_vTransposed, ldvt, n);

    delete [] _vTransposed;

    u = MatrixOperations::columnMajorOrderArrayToMatrix(_u, ldu, std::min(m, n));

    delete [] _u;

    return info;

}

QVector<QVector<double> > MatrixOperations::subMatrixBasedOnColumnIndexes(const QVector<QVector<double> > &matrix, QList<int> columnIndexes)
{

    QVector<QVector<double> > subMatrix;

    subMatrix.reserve(matrix.size());

    for (const QVector<double> &vector : matrix) {

        QVector<double> temp;

        temp.reserve(columnIndexes.size());

        for (const int &columnIndex : columnIndexes)
            temp << vector.at(columnIndex);

        subMatrix << temp;
    }

    return subMatrix;

}

void MatrixOperations::tred2(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e)
{

    double dummyProgressValue;

    MatrixOperations::tred2(symmetricMatrix, d, e, dummyProgressValue);

}

void MatrixOperations::tred2(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e, double &progressValue)
{

    progressValue = 0.0;

    int n = symmetricMatrix.size();

    d.resize(n);

    e.resize(n);

    for (int i = n - 1; i > 0; --i) {

        QVector<double> &vector_i(symmetricMatrix[i]);

        int l = i - 1;

        long double h = static_cast<long double>(0.0);

        long double scale = static_cast<long double>(0.0);

        if (l > 0) {

            for (int k = 0; k < i; ++k)
                scale += std::fabs(static_cast<long double>(vector_i.at(k)));


            if ((std::fabs(scale - static_cast<long double>(0.0)) <= std::numeric_limits<long double>::epsilon())) {

                e[i] = vector_i.at(l);

            } else {

                for (int k = 0; k < i; ++k) {

                    double &value(vector_i[k]);

                    value /= scale;

                    h += static_cast<long double>(value) * static_cast<long double>(value);

                }

                long double f = static_cast<long double>(vector_i.at(l));

                long double g = (f >= static_cast<long double>(0.0) ? -std::sqrt(h) : std::sqrt(h));

                e[i] = static_cast<double>(scale * g);

                h -= f * g;

                vector_i[l] = static_cast<double>(f - g);

                f = static_cast<long double>(0.0);

                for (int j = 0; j < i; ++j) {

                    QVector<double> const &vector_j(symmetricMatrix.at(j));

                    long double e_j = static_cast<long double>(0.0);

                    for (int k = 0; k < j + 1; ++k)
                        e_j += static_cast<long double>(vector_j.at(k)) * static_cast<long double>(vector_i.at(k));

                    for (int k = j + 1; k < i; ++k)
                        e_j += static_cast<long double>(symmetricMatrix.at(k).at(j)) * static_cast<long double>(vector_i.at(k));

                    e_j /= h;

                    symmetricMatrix[j][i] = static_cast<double>(static_cast<long double>(vector_i.at(j)) / h);

                    e[j] = static_cast<double>(e_j);

                    f += (e_j * static_cast<long double>(vector_i.at(j)));

                }

                long double hh = f / (h + h);

                for (int j = 0; j < i; ++j)
                    e[j] -= hh * static_cast<long double>(vector_i.at(j));

                for (int j = 0; j < i; ++j) {

                    long double f = static_cast<long double>(vector_i.at(j));

                    long double g = static_cast<long double>(e.at(j));

                    QVector<double> &vector_j(symmetricMatrix[j]);

                    for (int k = 0; k < j + 1; ++k)
                        vector_j[k] -= (f * static_cast<long double>(e.at(k)) + g * static_cast<long double>(vector_i.at(k)));

                }

            }

            progressValue += ((i - 1.0) * ((3.0 * i) + 4.0)) / 2.0;

        } else
            e[i] = vector_i.at(l);

        d[i] = static_cast<double>(h);

    }

    d[0] = 0.0;

    e[0] = 0.0;

    MatrixOperations::transpose_inplace(symmetricMatrix);

    for (int i = 0; i < n; ++i) {

        QVector<double> vector_j;

        for (int j = 0; j < n; ++j)
            vector_j << symmetricMatrix.at(j).at(i);

        if (d.at(i) != 0.0) {

            QVector<double> const &vector_i(symmetricMatrix.at(i));

            for (int l = 0; l < i; ++l) {

                QVector<double> &vector_l(symmetricMatrix[l]);

                long double g = static_cast<long double>(0.0);

                for (int k = 0; k < i; ++k)
                    g += static_cast<long double>(vector_j.at(k)) * static_cast<long double>(vector_l.at(k));

                for (int k = 0; k < i; ++k)
                    vector_l[k] -= g * static_cast<long double>(vector_i.at(k));

            }

        }

        d[i] = vector_j.at(i);

        symmetricMatrix[i][i] = 1.0;

        QVector<double> &vector_i(symmetricMatrix[i]);

        for (int j = 0; j < i; ++j)
            vector_i[j] = symmetricMatrix[j][i] = 0.0;

        progressValue += 2.0 * i * i + i + n;

    }

    MatrixOperations::transpose_inplace(symmetricMatrix);
}

void MatrixOperations::tred2_multiThreaded(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e)
{

    double dummyProgressValue;

    MatrixOperations::tred2_multiThreaded(symmetricMatrix, d, e, dummyProgressValue);

}

void MatrixOperations::tred2_multiThreaded(QVector<QVector<double> > &symmetricMatrix, QVector<double> &d, QVector<double> &e, double &progressValue)
{

    progressValue = 0.0;

    QReadWriteLock readWriteLock;

    int n = symmetricMatrix.size();

    d.resize(n);

    e.resize(n);

    QVector<QVector<int> > schedule = FunctionsForMultiThreading::createIndexSchedulerForMultiThreading(symmetricMatrix.size(), QThreadPool::globalInstance()->maxThreadCount());

    for (int i = n - 1; i > 0; --i) {

        schedule[i % QThreadPool::globalInstance()->maxThreadCount()].pop_back();

        QVector<double> &vector_i(symmetricMatrix[i]);

        int l = i - 1;

        double h = 0.0;

        double scale = 0.0;

        if (l > 0) {

            for (int k = 0; k < i; ++k)
                scale += std::fabs(vector_i.at(k));

            if (scale == 0.0) {

                e[i] = vector_i.at(l);

            } else {

                auto loop0 = [&](const QVector<int> &indexesToProcess) {

                    double _h = 0.0;

                    for (const int &indexToProcess : indexesToProcess) {

                        double &value(vector_i[indexToProcess]);

                        value /= scale;

                        _h += value * value;

                    }

                    readWriteLock.lockForWrite();

                    h += _h;

                    readWriteLock.unlock();

                };

                QFutureSynchronizer<void> futureSynchronizer;

                for (const QVector<int> &indexesToProcess : schedule)
                    futureSynchronizer.addFuture(QtConcurrent::run(loop0, indexesToProcess));

                futureSynchronizer.waitForFinished();

                double f = vector_i.at(l);

                double g = (f >= 0.0 ? -std::sqrt(h) : std::sqrt(h));

                e[i] = scale * g;

                h -= f * g;

                vector_i[l] = f - g;

                f = 0.0;

                auto loop1 = [&](const QVector<int> &indexesToProcess) {

                    double _f = 0.0;

                    for (const int &indexToProcess : indexesToProcess) {

                        QVector<double> const &vector_indexToProcess(symmetricMatrix.at(indexToProcess));

                        double e_indexToProcess = 0.0;

                        for (int k = 0; k < indexToProcess + 1; ++k)
                            e_indexToProcess += vector_indexToProcess.at(k) * vector_i.at(k);

                        for (int k = indexToProcess + 1; k < i; ++k)
                            e_indexToProcess += symmetricMatrix.at(k).at(indexToProcess) * vector_i.at(k);

                        e_indexToProcess /= h;

                        symmetricMatrix[indexToProcess][i] = vector_i.at(indexToProcess) / h;

                        e[indexToProcess] = e_indexToProcess;

                        _f += (e_indexToProcess * vector_i.at(indexToProcess));

                    }

                    readWriteLock.lockForWrite();

                    f += _f;

                    readWriteLock.unlock();

                };

                futureSynchronizer.clearFutures();

                for (const QVector<int> &indexesToProcess : schedule)
                    futureSynchronizer.addFuture(QtConcurrent::run(loop1, indexesToProcess));

                futureSynchronizer.waitForFinished();

                double hh = f / (h + h);

                for (int j = 0; j < i; ++j)
                    e[j] -= hh * vector_i.at(j);

                auto loop2 = [&](const QVector<int> &indexesToProcess) {

                    for (const int &indexToProcess : indexesToProcess) {

                        double f = vector_i.at(indexToProcess);

                        double g = e.at(indexToProcess);

                        QVector<double> &vector_indexToProcess(symmetricMatrix[indexToProcess]);

                        for (int k = 0; k < indexToProcess + 1; ++k)
                            vector_indexToProcess[k] -= (f * e.at(k) + g * vector_i.at(k));

                    }

                };

                futureSynchronizer.clearFutures();

                for (const QVector<int> &indexesToProcess : schedule)
                    futureSynchronizer.addFuture(QtConcurrent::run(loop2, indexesToProcess));

                futureSynchronizer.waitForFinished();

            }

            progressValue += ((i - 1.0) * ((3.0 * i) + 4.0)) / 2.0;

        } else
            e[i] = vector_i.at(l);

        d[i] = h;

    }

    d[0] = 0.0;

    e[0] = 0.0;

    MatrixOperations::transpose_inplace_multiThreaded(symmetricMatrix);

    schedule.clear();

    schedule.resize(QThreadPool::globalInstance()->maxThreadCount());

    for (int i = 0; i < n; ++i) {

        QVector<double> vector_j;

        for (int j = 0; j < n; ++j)
            vector_j << symmetricMatrix.at(j).at(i);

        if (i != 0)
            schedule[(i % QThreadPool::globalInstance()->maxThreadCount())] << (i - 1);

        if (d.at(i) != 0.0) {

            QVector<double> const &vector_i(symmetricMatrix.at(i));

            auto loop3 = [&](const QVector<int> &indexesToProcess) {

                for (const int &indexToProcess : indexesToProcess) {

                    QVector<double> &vector_indexToProcess(symmetricMatrix[indexToProcess]);

                    double g = 0.0;

                    for (int k = 0; k < i; ++k)
                        g += vector_j.at(k) * vector_indexToProcess.at(k);

                    for (int k = 0; k < i; ++k)
                        vector_indexToProcess[k] -= g * vector_i.at(k);

                }

            };

            QFutureSynchronizer<void> futureSynchronizer;

            for (const QVector<int> &indexesToProcess : schedule)
                futureSynchronizer.addFuture(QtConcurrent::run(loop3, indexesToProcess));

            futureSynchronizer.waitForFinished();

        }

        d[i] = vector_j.at(i);

        symmetricMatrix[i][i] = 1.0;

        QVector<double> &vector_i(symmetricMatrix[i]);

        for (int j = 0; j < i; ++j)
            vector_i[j] = symmetricMatrix[j][i] = 0.0;

        progressValue += 2.0 * i * i + i + n;

    }

    MatrixOperations::transpose_inplace_multiThreaded(symmetricMatrix);

}

QVector<QVector<double> > &MatrixOperations::orthogonalizeMatrix_inplace(QVector<QVector<double> > &matrix)
{

    QVector<QVector<double> > u;

    QVector<QVector<double> > v;

    QVector<double> s;

    MatrixOperations::singularValueDecomposition(matrix, u, s, v);

    double tol = s.size() * 2.2e-16 * MathDescriptives::maximum(s);

    int r = 0;

    for (int i = 0; i < s.size(); ++i)
        if (s.at(i) > tol) r++;

    matrix = u;

    for (int i = 0; i < matrix.size(); ++i)
        matrix[i].resize(r);

    return matrix;

}
