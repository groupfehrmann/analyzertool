#include "selectcolorwidget.h"
#include "ui_selectcolorwidget.h"

SelectColorWidget::SelectColorWidget(QWidget *parent, const QColor &currentColor) :
    QWidget(parent), ui(new Ui::SelectColorWidget), d_colorDialog(new QColorDialog(this))
{

    ui->setupUi(this);

    d_currentColor = currentColor;

    this->setColorInfo(currentColor);

    d_colorDialog->setOption(QColorDialog::ShowAlphaChannel, true);

    this->connect(d_colorDialog, SIGNAL(colorSelected(QColor)), this, SIGNAL(colorSelected(QColor)));

    this->connect(d_colorDialog, SIGNAL(currentColorChanged(QColor)), this, SIGNAL(currentColorChanged(QColor)));

    this->connect(ui->toolButton, SIGNAL(clicked()), this, SLOT(toolButton_triggered()));

}

SelectColorWidget::~SelectColorWidget()
{

    delete ui;

}

QColor SelectColorWidget::currentColor() const
{

    return d_currentColor;

}

void SelectColorWidget::toolButton_triggered()
{

    d_colorDialog->setCurrentColor(d_currentColor);

    if (d_colorDialog->exec()) {

        this->setColorInfo(d_colorDialog->currentColor());

        d_currentColor = d_colorDialog->currentColor();

    } else
        this->setCurrentColor(d_currentColor);

}

void SelectColorWidget::setColorInfo(const QColor &color)
{

    QPalette palette;

    palette.setColor(QPalette::Base, color);

    ui->lineEdit1->setPalette(palette);

    ui->lineEdit2->setText("#AARRGGBB: " + color.name(QColor::HexArgb));

}

void SelectColorWidget::setCurrentColor(const QColor &color)
{

    this->setColorInfo(color);

    d_currentColor = color;

    d_colorDialog->setCurrentColor(color);

}
