#ifndef CALCULATEMODULESCORESDIALOG_H
#define CALCULATEMODULESCORESDIALOG_H

#include <QString>
#include <QSharedPointer>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/selectfilebuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "workerclasses/calculatemodulescoresworkerclass.h"

class CalculateModuleScoresDialog : public BaseDialog
{

    Q_OBJECT

public:

    CalculateModuleScoresDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~CalculateModuleScoresDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    SelectFileBuildingBlockWidget *d_selectFileBuildingBlockWidget;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_formatFileDefiningModules;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_annotationLabelsDefiningAnnotationValuesForVariables;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // CALCULATEMODULESCORESDIALOG_H
