#include "transformdatamatrixworkerclass.h"

TransformDataMatrixWorkerClass::TransformDataMatrixWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<TransformDataMatrixWorkerClass::Parameters *>(parameters))
{

}

void TransformDataMatrixWorkerClass::doWork()
{

    emit processStarted("Transform data matrix");

    if (d_parameters.formatedTransformFunctionList.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no transform functions defined");

        QThread::currentThread()->quit();

        return;

    }

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "transform sequence: " + ConvertFunctions::toString(d_parameters.functionDescriptionsWithParametersReplacement));

    emit appendLogItem(LogListModel::PARAMETER, "transform number: " + QString::number(d_parameters.formatedTransformFunctionList.size()));


    switch (d_parameters.baseDataset->baseMatrix()->metaTypeIdentifier()) {

        case QMetaType::Short: this->_doWork<short>(); break;

        case QMetaType::UShort: this->_doWork<unsigned short>(); break;

        case QMetaType::Int: this->_doWork<int>(); break;

        case QMetaType::UInt: this->_doWork<unsigned int>(); break;

        case QMetaType::LongLong: this->_doWork<long long>(); break;

        case QMetaType::ULongLong: this->_doWork<unsigned long long>(); break;

        case QMetaType::Float: this->_doWork<float>(); break;

        case QMetaType::Double: this->_doWork<double>(); break;

        case QMetaType::UChar: this->_doWork<unsigned char>(); break;

        case QMetaType::QChar: this->_doWork<QChar>(); break;

        case QMetaType::QString: this->_doWork<QString>(); break;

        case QMetaType::Bool: this->_doWork<bool>(); break;

        default: break;

    }

    QThread::currentThread()->quit();

}
