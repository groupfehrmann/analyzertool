#ifndef LINEEDITBUILDINGBLOCKWIDGET_H
#define LINEEDITBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QStringList>
#include <QSettings>

#include "dialogs/basedialog.h"

namespace Ui {

class LineEditBuildingBlockWidget;

}

class LineEditBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit LineEditBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), const QString &defaultText = QString());

    ~LineEditBuildingBlockWidget();

    QLineEdit *pointerToInternalLineEdit();

    QLabel *pointerToInternalLabel();

private:

    Ui::LineEditBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

};

#endif // LINEEDITBUILDINGBLOCKWIDGET_H
