#ifndef EXPORTTOJSONFILEWORKERCLASS_H
#define EXPORTTOJSONFILEWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QVariantList>

#include <tuple>
#include <algorithm>
#include <cmath>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"

class ExportToJSONFileWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QStringList annotationLabelsForItems;

        QStringList annotationLabelsForVariables;

        QStringList annotationIdentifiersForItems;

        QSharedPointer<BaseDataset> baseDataset;

        QString exportDirectory;

        QString exportFormat;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

    };

    ExportToJSONFileWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    ExportToJSONFileWorkerClass::Parameters d_parameters;

    bool checkInputParameters();

    void processExportFormat_forDistroChart();

    void processExportFormat_forJEasyUIDataGrid();

    bool exportSearchableKeysForVariables();

};

#endif // EXPORTTOJSONFILEWORKERCLASS_H
