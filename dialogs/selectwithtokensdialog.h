#ifndef SELECTWITHTOKENSDIALOG_H
#define SELECTWITHTOKENSDIALOG_H

#include "dialogs/basedialog.h"
#include "workerclasses/selectwithtokensworkerclass.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/texteditbuildingblockwidget.h"
#include "widgets/selectdelimiterbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"

class SelectWithTokensDialog : public BaseDialog
{

    Q_OBJECT

public:

    SelectWithTokensDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);


    ~SelectWithTokensDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    bool d_selectedOnly;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_identifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_mode;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_caseSensitivity;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_applyToIdentifiers;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_ApplyToAnnotationLabels;

    TextEditBuildingBlockWidget *d_textEditBuildingBlockWidget_tokensToMatch;

    SelectDelimiterBuildingBlockWidget *d_selectDelimiterBuildingBlockWidget;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // SELECTWITHTOKENSDIALOG_H
