#ifndef TABLERESULTITEMWIDGET_H
#define TABLERESULTITEMWIDGET_H

#include <QWidget>
#include <QAbstractItemModel>
#include <QTableView>

#include "base/tableresultitem.h"

namespace Ui {

class TableResultItemWidget;

}

class TableResultItemWidget : public QWidget
{

    Q_OBJECT

public:

    explicit TableResultItemWidget(QWidget *parent = nullptr, QSharedPointer<TableResultItem> tableResultItem = QSharedPointer<TableResultItem>());

    ~TableResultItemWidget();

    QAbstractItemModel *currentTableModel();

    QTableView *currentTableView();

private:

    Ui::TableResultItemWidget *ui;

    QSharedPointer<TableResultItem> d_tableResultItem;

};

#endif // TABLERESULTITEMWIDGET_H
