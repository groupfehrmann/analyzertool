#include "saveplotworkerclass.h"

SavePlotWorkerClass::SavePlotWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<SavePlotWorkerClass::Parameters *>(parameters))
{

}

void SavePlotWorkerClass::doWork()
{
    emit processStarted("Save plot");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    emit appendLogItem(LogListModel::PARAMETER, "plot to save: \"" + d_parameters.chartViewToSave->chart()->title() +"\"");

    emit startProgress(0, "Saving plot", 0, 0);

    QFile fileIn(d_parameters.pathToFile);

    if (!fileIn.open(QIODevice::WriteOnly)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\"");

        return;

    }

    QDataStream out(&fileIn);

    out << QString("_Plot_begin#");

    out << *d_parameters.chartViewToSave->chart();

    out << QString("_Plot_end#");

    emit stopProgress(0);

    QThread::currentThread()->quit();

}
