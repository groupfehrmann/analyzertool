#include "convertfunctions.h"

bool ConvertFunctions::isValidNumber(const short &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const unsigned short &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const int &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const unsigned int &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const long long &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const unsigned long long &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const float &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const double &value)
{

    return std::isfinite(value);

}

bool ConvertFunctions::isValidNumber(const QChar &value)
{

    return value.isDigit();

}

bool ConvertFunctions::isValidNumber(const QString &value)
{

    bool ok;

    double temp = value.toDouble(&ok);

    if (!ok)
        return false;

    return std::isfinite(temp);

}

bool ConvertFunctions::isValidNumber(const bool &value)
{

    Q_UNUSED(value);

    return true;

}

bool ConvertFunctions::isValidNumber(const unsigned char &value)
{

    return QChar(value).isDigit();

}

bool ConvertFunctions::isValidNumber(const QVariant &value)
{

    bool ok;

    double temp = value.toDouble(&ok);

    if (!ok)
        return false;

    return std::isfinite(temp);

}

bool ConvertFunctions::canConvert(const QVariant &variant, short &value)
{

    bool ok;

    int _value = variant.toInt(&ok);

    if (ok && (_value >= std::numeric_limits<short>::min()) && (_value <= std::numeric_limits<short>::max()))
        value = static_cast<short>(_value);
    else
        value = std::numeric_limits<short>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, unsigned short &value)
{

    bool ok;

    int _value = variant.toInt(&ok);

    if (ok && (_value >= std::numeric_limits<unsigned short>::min()) && (_value <= std::numeric_limits<unsigned short>::max()))
        value = static_cast<unsigned short>(_value);
    else
        value = std::numeric_limits<unsigned short>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, int &value)
{

    bool ok;

    value = variant.toInt(&ok);

    if (!ok)
        value = std::numeric_limits<int>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, unsigned int &value)
{

    bool ok;

    value = variant.toUInt(&ok);

    if (!ok)
        value = std::numeric_limits<unsigned int>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, long long &value)
{

    bool ok;

    value = variant.toLongLong(&ok);

    if (!ok)
        value = std::numeric_limits<long long>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, unsigned long long &value)
{

    bool ok;

    value = variant.toULongLong(&ok);

    if (!ok)
        value = std::numeric_limits<unsigned long long>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, float &value)
{

    bool ok;

    value = variant.toFloat(&ok);

    if (!ok)
        value = std::numeric_limits<float>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, double &value)
{

    bool ok;

    value = variant.toDouble(&ok);

    if (!ok)
        value = std::numeric_limits<double>::quiet_NaN();

    return ok;

}

bool ConvertFunctions::canConvert(const QVariant &variant, QChar &value)
{

    value = variant.toChar();

    return true;

}

bool ConvertFunctions::canConvert(const QVariant &variant, QString &value)
{

    value = variant.toString();

    return true;

}

bool ConvertFunctions::canConvert(const QVariant &variant, bool &value)
{

    value = variant.toBool();

    return true;

}

bool ConvertFunctions::canConvert(const QVariant &variant, unsigned char &value)
{

    value = variant.toChar().toLatin1();

    return true;

}

bool ConvertFunctions::canConvert(const QVariant &variant, QVariant &value)
{

    value = variant;

    return true;

}

template <>
void ConvertFunctions::convert(const short &value, short &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const unsigned short &value, unsigned short &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const int &value, int &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const unsigned int &value, unsigned int &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const long long &value, long long &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const unsigned long long &value, unsigned long long &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const float &value, float &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const double &value, double &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const QChar &value, QChar &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const QString &value, QString &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const bool &value, bool &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const unsigned char &value, unsigned char &convertedValue)
{

    convertedValue = value;

}

template <>
void ConvertFunctions::convert(const QVariant &value, QVariant &convertedValue)
{

    convertedValue = value;

}

template <>
QVector<short> ConvertFunctions::convertVector(const QVector<short> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<unsigned short> ConvertFunctions::convertVector(const QVector<unsigned short> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<int> ConvertFunctions::convertVector(const QVector<int> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<unsigned int> ConvertFunctions::convertVector(const QVector<unsigned int> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<long> ConvertFunctions::convertVector(const QVector<long> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<long long> ConvertFunctions::convertVector(const QVector<long long> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<unsigned long long> ConvertFunctions::convertVector(const QVector<unsigned long long> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<float> ConvertFunctions::convertVector(const QVector<float> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<double> ConvertFunctions::convertVector(const QVector<double> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<QChar> ConvertFunctions::convertVector(const QVector<QChar> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<QString> ConvertFunctions::convertVector(const QVector<QString> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<bool> ConvertFunctions::convertVector(const QVector<bool> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}

template <>
QVector<unsigned char> ConvertFunctions::convertVector(const QVector<unsigned char> &vector, bool *ok, QString *conversionError)
{

    if (ok)
        *ok = true;

    if (conversionError)
        *conversionError = QString();

    return vector;

}
