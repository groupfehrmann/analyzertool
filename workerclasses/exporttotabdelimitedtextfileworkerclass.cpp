#include "exporttotabdelimitedtextfileworkerclass.h"

ExportToTabDelimitedTextFileWorkerClass::ExportToTabDelimitedTextFileWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<ExportToTabDelimitedTextFileWorkerClass::Parameters *>(parameters))
{

}

void ExportToTabDelimitedTextFileWorkerClass::doWork()
{
    emit processStarted("Export to tab-delimited text file");

    emit appendLogItem(LogListModel::PARAMETER, "exporting: \"" + d_parameters.descriptionOfObjectToExport +"\"");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    QFile fileIn(d_parameters.pathToFile);

    if (!fileIn.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Exporting", 0, d_parameters.abstractItemModel->rowCount());

    QTextStream out(&fileIn);

    for (int i = 0; i < d_parameters.abstractItemModel->columnCount(); ++i)
        out << "\t" << d_parameters.abstractItemModel->headerData(i, Qt::Horizontal).toString();

    out << Qt::endl;

    for (int i = 0; i < d_parameters.abstractItemModel->rowCount(); ++i) {

        out << d_parameters.abstractItemModel->headerData(i, Qt::Vertical).toString();

        for (int j = 0; j < d_parameters.abstractItemModel->columnCount(); ++j)
            out << "\t" << d_parameters.abstractItemModel->data(d_parameters.abstractItemModel->index(i, j)).toString();

        out << Qt::endl;

        emit updateProgress(0, i + 1);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

    }

    emit stopProgress(0);

    QThread::currentThread()->quit();

}
