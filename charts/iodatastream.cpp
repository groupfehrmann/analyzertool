#include "charts/iodatastream.h"

QDataStream& operator<<(QDataStream& out, const QAbstractAxis &abstractAxis)
{

    out << QString("_QAbstractAxis_begin#");

    out << QString("gridLinePen") << abstractAxis.gridLinePen();

    out << QString("isGridLineVisible") << abstractAxis.isGridLineVisible();

    out << QString("labelsAngle") << abstractAxis.labelsAngle();

    out << QString("labelsBrush") << abstractAxis.labelsBrush();

    out << QString("labelsFont") << abstractAxis.labelsFont();

    out << QString("labelsVisible") << abstractAxis.labelsVisible();

    out << QString("linePen") << abstractAxis.linePen();

    out << QString("isLineVisible") << abstractAxis.isLineVisible();

    out << QString("minorGridLinePen") << abstractAxis.minorGridLinePen();

    out << QString("isMinorGridLineVisible") << abstractAxis.isMinorGridLineVisible();

    out << QString("isReverse") << abstractAxis.isReverse();

    out << QString("shadesBrush") << abstractAxis.shadesBrush();

    out << QString("shadesPen") << abstractAxis.shadesPen();

    out << QString("shadesVisible") << abstractAxis.shadesVisible();

    out << QString("titleBrush") << abstractAxis.titleBrush();

    out << QString("titleFont") << abstractAxis.titleFont();

    out << QString("titleText") << abstractAxis.titleText();

    out << QString("isTitleVisible") << abstractAxis.isTitleVisible();

    out << QString("isVisible") << abstractAxis.isVisible();

    out << QString("_QAbstractAxis_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QAbstractAxis &abstractAxis)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QAbstractAxis_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("gridLinePen")) {

            QPen pen;

            in >> pen;

            abstractAxis.setGridLinePen(pen);

        } else if (label == QString("isGridLineVisible")) {

            bool visible;

            in >> visible;

            abstractAxis.setGridLineVisible(visible);

        } else if (label == QString("labelsAngle")) {

            int angle;

            in >> angle;

            abstractAxis.setLabelsAngle(angle);

        } else if (label == QString("labelsBrush")) {

            QBrush brush;

            in >> brush;

            abstractAxis.setLabelsBrush(brush);

        } else if (label == QString("labelsFont")) {

            QFont font;

            in >> font;

            abstractAxis.setLabelsFont(font);

        } else if (label == QString("labelsVisible")) {

            bool visible;

            in >> visible;

            abstractAxis.setLabelsVisible(visible);

        } else if (label == QString("linePen")) {

            QPen pen;

            in >> pen;

            abstractAxis.setLinePen(pen);

        } else if (label == QString("isLineVisible")) {

            bool visible;

            in >> visible;

            abstractAxis.setLineVisible(visible);

        } else if (label == QString("minorGridLinePen")) {

            QPen pen;

            in >> pen;

            abstractAxis.setMinorGridLinePen(pen);

        } else if (label == QString("isMinorGridLineVisible")) {

            bool visible;

            in >> visible;

            abstractAxis.setMinorGridLineVisible(visible);

        } else if (label == QString("isReverse")) {

            bool reverse;

            in >> reverse;

            abstractAxis.setReverse(reverse);

        } else if (label == QString("shadesBrush")) {

            QBrush brush;

            in >> brush;

            abstractAxis.setShadesBrush(brush);

        } else if (label == QString("shadesPen")) {

            QPen pen;

            in >> pen;

            abstractAxis.setShadesPen(pen);

        } else if (label == QString("shadesVisible")) {

            bool visible;

            in >> visible;

            abstractAxis.setShadesVisible(visible);

        } else if (label == QString("titleBrush")) {

            QBrush brush;

            in >> brush;

            abstractAxis.setTitleBrush(brush);

        } else if (label == QString("titleFont")) {

            QFont font;

            in >> font;

            abstractAxis.setTitleFont(font);

        } else if (label == QString("titleText")) {

            QString text;

            in >> text;

            abstractAxis.setTitleText(text);

        } else if (label == QString("isTitleVisible")) {

            bool visible;

            in >> visible;

            abstractAxis.setTitleVisible(visible);

        } else if (label == QString("isVisible")) {

            bool visible;

            in >> visible;

            abstractAxis.setVisible(visible);

        } else if (label == QString("_QAbstractAxis_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QBarCategoryAxis &barCategoryAxis)
{

    out << QString("_QBarCategoryAxis_begin#");

    out << QString("abstractAxis") << static_cast<const QAbstractAxis &>(barCategoryAxis);

    out << QString("categories") << const_cast<QBarCategoryAxis &>(barCategoryAxis).categories();

    out << QString("max") << barCategoryAxis.max();

    out << QString("min") << barCategoryAxis.min();

    out << QString("_QBarCategoryAxis_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QBarCategoryAxis &barCategoryAxis)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QBarCategoryAxis_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractAxis"))
            in >> static_cast<QAbstractAxis &>(barCategoryAxis);
        else if (label == QString("categories")) {

            QStringList categories;

            in >> categories;

            barCategoryAxis.setCategories(categories);

        } else if (label == QString("max")) {

            QString max;

            in >> max;

            barCategoryAxis.setMax(max);

        } else if (label == QString("min")) {

            QString min;

            in >> min;

            barCategoryAxis.setMin(min);

        } else if (label == QString("_QBarCategoryAxis_end#"))
            return in;

    }

    return in;
}

QDataStream& operator<<(QDataStream& out, const QDateTimeAxis &dateTimeAxis)
{

    out << QString("_QDateTimeAxis_begin#");

    out << QString("abstractAxis") << static_cast<const QAbstractAxis &>(dateTimeAxis);

    out << QString("format") << dateTimeAxis.format();

    out << QString("max") << dateTimeAxis.max();

    out << QString("min") << dateTimeAxis.min();

    out << QString("tickCount") << dateTimeAxis.tickCount();

    out << QString("_QDateTimeAxis_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QDateTimeAxis &dateTimeAxis)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QDateTimeAxis_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractAxis"))
            in >> static_cast<QAbstractAxis &>(dateTimeAxis);
        else if (label == QString("format")) {

            QString format;

            in >> format;

            dateTimeAxis.setFormat(format);

        } else if (label == QString("max")) {

            QDateTime max;

            in >> max;

            dateTimeAxis.setMax(max);

        } else if (label == QString("min")) {

            QDateTime min;

            in >> min;

            dateTimeAxis.setMin(min);

        } else if (label == QString("tickCount")) {

            int count;

            in >> count;

            dateTimeAxis.setTickCount(count);

        } else if (label == QString("_QDateTimeAxis_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QLogValueAxis &logValueAxis)
{

    out << QString("_QLogValueAxis_begin#");

    out << QString("abstractAxis") << static_cast<const QAbstractAxis &>(logValueAxis);

    out << QString("base") << logValueAxis.base();

    out << QString("labelFormat") << logValueAxis.labelFormat();

    out << QString("max") << logValueAxis.max();

    out << QString("min") << logValueAxis.min();

    out << QString("_QLogValueAxis_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QLogValueAxis &logValueAxis)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QLogValueAxis_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractAxis"))
            in >> static_cast<QAbstractAxis &>(logValueAxis);
        else if (label == QString("base")) {

            qreal base;

            in >> base;

            logValueAxis.setBase(base);

       } else if (label == QString("labelFormat")) {

            QString format;

            in >> format;

            logValueAxis.setLabelFormat(format);

        } else if (label == QString("max")) {

            qreal max;

            in >> max;

            logValueAxis.setMax(max);

        } else if (label == QString("min")) {

            qreal min;

            in >> min;

            logValueAxis.setMin(min);

        } else if (label == QString("_QLogValueAxis_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QValueAxis &valueAxis)
{

    out << QString("_QValueAxis_begin#");

    out << QString("abstractAxis") << static_cast<const QAbstractAxis &>(valueAxis);

    out << QString("labelFormat") << valueAxis.labelFormat();

    out << QString("max") << valueAxis.max();

    out << QString("min") << valueAxis.min();

    out << QString("minorTickCount") << valueAxis.minorTickCount();

    out << QString("tickCount") << valueAxis.tickCount();

    out << QString("_QValueAxis_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QValueAxis &valueAxis)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QValueAxis_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractAxis"))
            in >> static_cast<QAbstractAxis &>(valueAxis);
        else if (label == QString("labelFormat")) {

            QString format;

            in >> format;

            valueAxis.setLabelFormat(format);

        } else if (label == QString("max")) {

            qreal max;

            in >> max;

            valueAxis.setMax(max);

        } else if (label == QString("min")) {

            qreal min;

            in >> min;

            valueAxis.setMin(min);

        } else if (label == QString("minorTickCount")) {

            int count;

            in >> count;

            valueAxis.setMinorTickCount(count);

        } else if (label == QString("tickCount")) {

            int count;

            in >> count;

            valueAxis.setTickCount(count);

        } else if (label == QString("_QValueAxis_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QCategoryAxis &categoryAxis)
{

    out << QString("_QCategoryAxis_begin#");

    out << QString("valueAxis") << static_cast<const QValueAxis &>(categoryAxis);

    out << QString("startValue") << categoryAxis.startValue();

    out << QString("categoriesLabelsWithEndValues") << const_cast<QCategoryAxis &>(categoryAxis).categoriesLabels().size();

    for (const QString &categoryLabel : const_cast<QCategoryAxis &>(categoryAxis).categoriesLabels())
        out << categoryLabel << categoryAxis.endValue(categoryLabel);

    out << QString("labelsPosition") << static_cast<int>(categoryAxis.labelsPosition());

    out << QString("_QCategoryAxis_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QCategoryAxis &categoryAxis)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QCategoryAxis_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("valueAxis"))
            in >> static_cast<QValueAxis &>(categoryAxis);
        else if (label == QString("categoriesLabelsWithEndValues")) {

            int numberOfCategoryLabels;

            in >> numberOfCategoryLabels;

            for (int i = 0; i < numberOfCategoryLabels; ++i) {

                QString categoryLabel;

                qreal endValue;

                in >> categoryLabel;

                in >> endValue;

                categoryAxis.append(categoryLabel, endValue);

            }

        } else if (label == QString("labelsPosition")) {

            int position;

            in >> position;

            categoryAxis.setLabelsPosition(static_cast<QCategoryAxis::AxisLabelsPosition>(position));

        } else if (label == QString("startValue")) {

            qreal startValue;

            in >> startValue;

            categoryAxis.setStartValue(startValue);

        } else if (label == QString("_QCategoryAxis_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QAbstractSeries &abstractSeries)
{

    out << QString("_QAbstractSeries_begin#");

    out << QString("name") << abstractSeries.name();

    out << QString("opacity") << abstractSeries.opacity();

    out << QString("useOpenGL") << abstractSeries.useOpenGL();

    out << QString("visible") << abstractSeries.isVisible();

    out << QString("_QAbstractSeries_end#");

    return out;
}

QDataStream& operator>>(QDataStream& in, QAbstractSeries &abstractSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QAbstractSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("name")) {

            QString name;

            in >> name;

            abstractSeries.setName(name);

        } else if (label == QString("opacity")) {

            qreal opacity;

            in >> opacity;

            abstractSeries.setOpacity(opacity);

        } else if (label == QString("useOpenGL")) {

            bool useOpenGL;

            in >> useOpenGL;

            abstractSeries.setUseOpenGL(useOpenGL);

        } else if (label == QString("visible")) {

            bool visible;

            in >> visible;

            abstractSeries.setVisible(visible);

        } else if (label == QString("_QAbstractSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QAbstractBarSeries &abstractBarSeries)
{

    out << QString("_QAbstractBarSeries_begin#");

    out << QString("abstractSeries") << static_cast<const QAbstractSeries &>(abstractBarSeries);

    out << QString("barSets") << abstractBarSeries.count();

    for (int i = 0; i < abstractBarSeries.count(); ++i)
        out << *abstractBarSeries.barSets().at(i);

    out << QString("barWidth") << abstractBarSeries.barWidth();

    out << QString("labelsAngle") << abstractBarSeries.labelsAngle();

    out << QString("labelsFormat") << abstractBarSeries.labelsFormat();

    out << QString("labelsPosition") << static_cast<int>(abstractBarSeries.labelsPosition());

    out << QString("labelsVisible") << abstractBarSeries.isLabelsVisible();

    out << QString("_QAbstractBarSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QAbstractBarSeries &abstractBarSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QAbstractBarSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractSeries"))
            in >> static_cast<QAbstractSeries &>(abstractBarSeries);
        else if (label == QString("barSets")) {

            int count;

            in >> count;

            for (int i = 0; i < count; ++i) {

                QBarSet *barSet = new QBarSet("barSet");

                in >> *barSet;

                abstractBarSeries.append(barSet);

            }

        } else if (label == QString("barWidth")) {

            qreal width;

            in >> width;

            abstractBarSeries.setBarWidth(width);

        } else if (label == QString("labelsAngle")) {

            qreal angle;

            in >> angle;

            abstractBarSeries.setLabelsAngle(angle);

        } else if (label == QString("labelsFormat")) {

            QString format;

            in >> format;

            abstractBarSeries.setLabelsFormat(format);

        } else if (label == QString("labelsPosition")) {

            int position;

            in >> position;

            abstractBarSeries.setLabelsPosition(static_cast<QAbstractBarSeries::LabelsPosition>(position));

        } else if (label == QString("labelsVisible")) {

            bool visible;

            in >> visible;

            abstractBarSeries.setLabelsVisible(visible);

        } else if (label == QString("_QAbstractBarSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QBarSeries &barSeries)
{

    out << QString("_QBarSeries_begin#");

    out << QString("abstractBarSeries") << static_cast<const QAbstractBarSeries &>(barSeries);

    out << QString("_QBarSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QBarSeries &barSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QBarSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractBarSeries"))
            in >> static_cast<QAbstractBarSeries &>(barSeries);
        else if (label == QString("_QBarSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QHorizontalBarSeries &horizontalBarSeries)
{

    out << QString("_QHorizontalBarSeries_begin#");

    out << QString("abstractBarSeries") << static_cast<const QAbstractBarSeries &>(horizontalBarSeries);

    out << QString("_QHorizontalBarSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QHorizontalBarSeries &horizontalBarSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QHorizontalBarSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractBarSeries"))
            in >> static_cast<QAbstractBarSeries &>(horizontalBarSeries);
        else if (label == QString("_QHorizontalBarSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QHorizontalPercentBarSeries &horizontalPercentBarSeries)
{

    out << QString("_QHorizontalPercentBarSeries_begin#");

    out << QString("abstractBarSeries") << static_cast<const QAbstractBarSeries &>(horizontalPercentBarSeries);

    out << QString("_QHorizontalPercentBarSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QHorizontalPercentBarSeries &horizontalPercentBarSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QHorizontalPercentBarSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractBarSeries"))
            in >> static_cast<QAbstractBarSeries &>(horizontalPercentBarSeries);
        else if (label == QString("_QHorizontalPercentBarSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QHorizontalStackedBarSeries &horizontalStackedBarSeries)
{

    out << QString("_QHorizontalStackedBarSeries_begin#");

    out << QString("abstractBarSeries") << static_cast<const QAbstractBarSeries &>(horizontalStackedBarSeries);

    out << QString("_QHorizontalStackedBarSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QHorizontalStackedBarSeries &horizontalStackedBarSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QHorizontalStackedBarSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractBarSeries"))
            in >> static_cast<QAbstractBarSeries &>(horizontalStackedBarSeries);
        else if (label == QString("_QHorizontalStackedBarSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QPercentBarSeries &percentBarSeries)
{

    out << QString("_QPercentBarSeries_begin#");

    out << QString("abstractBarSeries") << static_cast<const QAbstractBarSeries &>(percentBarSeries);

    out << QString("_QPercentBarSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QPercentBarSeries &percentBarSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QPercentBarSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractBarSeries"))
            in >> static_cast<QAbstractBarSeries &>(percentBarSeries);
        else if (label == QString("_QPercentBarSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QStackedBarSeries &stackedBarSeries)
{

    out << QString("_QStackedBarSeries_begin#");

    out << QString("abstractBarSeries") << static_cast<const QAbstractBarSeries &>(stackedBarSeries);

    out << QString("_QStackedBarSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QStackedBarSeries &stackedBarSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QStackedBarSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractBarSeries"))
            in >> static_cast<QAbstractBarSeries &>(stackedBarSeries);
        else if (label == QString("_QStackedBarSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QAreaSeries &areaSeries)
{

    out << QString("_QAreaSeries_begin#");

    out << QString("abstractSeries") << static_cast<const QAbstractSeries &>(areaSeries);

    out << QString("brush") << areaSeries.brush();

    out << QString("lowerSeries") << *areaSeries.lowerSeries();

    out << QString("pen") << areaSeries.pen();

    out << QString("pointLabelsClipping") << areaSeries.pointLabelsClipping();

    out << QString("pointLabelsFont") << areaSeries.pointLabelsFont();

    out << QString("pointLabelsFormat") << areaSeries.pointLabelsFormat();

    out << QString("pointLabelsVisible") << areaSeries.pointLabelsVisible();

    out << QString("pointsVisible") << areaSeries.pointsVisible();

    out << QString("upperSeries") << *areaSeries.upperSeries();

    out << QString("_QAreaSeries_end#");

    return out;

}



QDataStream& operator>>(QDataStream& in, QAreaSeries &areaSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QAreaSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractSeries"))
            in >> static_cast<QAbstractSeries &>(areaSeries);
        else if (label == QString("brush")) {

            QBrush brush;

            in >> brush;

            areaSeries.setBrush(brush);

        } else if (label == QString("lowerSeries")) {

            QLineSeries *lineSeries = new QLineSeries;

            in >> *lineSeries;

            areaSeries.setLowerSeries(lineSeries);

        } else if (label == QString("pen")) {

            QPen pen;

            in >> pen;

            areaSeries.setPen(pen);

        } else if (label == QString("pointLabelsClipping")) {

            bool clipping;

            in >> clipping;

            areaSeries.setPointLabelsClipping(clipping);

        } else if (label == QString("pointLabelsFont")) {

            QFont font;

            in >> font;

            areaSeries.setPointLabelsFont(font);

        } else if (label == QString("pointLabelsFormat")) {

            QString format;

            in >> format;

            areaSeries.setPointLabelsFormat(format);

        } else if (label == QString("pointLabelsVisible")) {

            bool visible;

            in >> visible;

            areaSeries.setPointLabelsVisible(visible);

        } else if (label == QString("pointsVisible")) {

            bool visible;

            in >> visible;

            areaSeries.setPointsVisible(visible);

        } else if (label == QString("upperSeries")) {

            QLineSeries *lineSeries = new QLineSeries;

            in >> *lineSeries;

            areaSeries.setUpperSeries(lineSeries);


        } else if (label == QString("_QAreaSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QBoxPlotSeries &boxPlotSeries)
{

    out << QString("_QBoxPlotSeries_begin#");

    out << QString("abstractSeries") << static_cast<const QAbstractSeries &>(boxPlotSeries);

    out << QString("boxSets") << boxPlotSeries.count();

    for (int i = 0; i < boxPlotSeries.count(); ++i)
        out << *boxPlotSeries.boxSets().at(i);

    out << QString("boxOutlineVisible") << const_cast<QBoxPlotSeries &>(boxPlotSeries).boxOutlineVisible();

    out << QString("boxWidth") << const_cast<QBoxPlotSeries &>(boxPlotSeries).boxWidth();

    out << QString("brush") << boxPlotSeries.brush();

    out << QString("pen") << boxPlotSeries.pen();

    out << QString("_QBoxPlotSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QBoxPlotSeries &boxPlotSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QBoxPlotSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractSeries"))
            in >> static_cast<QAbstractSeries &>(boxPlotSeries);
        else if (label == QString("boxOutlineVisible")) {

            bool visible;

            in >> visible;

            boxPlotSeries.setBoxOutlineVisible(visible);

        } else if (label == QString("boxWidth")) {

            qreal width;

            in >> width;

            boxPlotSeries.setBoxWidth(width);

        } else if (label == QString("boxSets")) {

            int count;

            in >> count;

            for (int i = 0; i < count; ++i) {

                QBoxSet *boxSet = new QBoxSet("boxSet");

                in >> *boxSet;

                boxPlotSeries.append(boxSet);

            }

        } else if (label == QString("brush")) {

            QBrush brush;

            in >> brush;

            boxPlotSeries.setBrush(brush);

        } else if (label == QString("pen")) {

            QPen pen;

            in >> pen;

            boxPlotSeries.setPen(pen);

        } else if (label == QString("_QBoxPlotSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QPieSeries &pieSeries)
{

    out << QString("_QPieSeries_begin#");

    out << QString("abstractSeries") << static_cast<const QAbstractSeries &>(pieSeries);

    out << QString("holeSize") << pieSeries.holeSize();

    out << QString("horizontalPosition") << pieSeries.horizontalPosition();

    out << QString("pieEndAngle") << pieSeries.pieEndAngle();

    out << QString("pieSize") << pieSeries.pieSize();

    out << QString("pieStartAngle") << pieSeries.pieStartAngle();

    out << QString("slices") << pieSeries.count();

    for (int i = 0; i < pieSeries.count(); ++i)
        out << *pieSeries.slices().at(i);

    out << QString("verticalPosition") << pieSeries.verticalPosition();

    out << QString("_QPieSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QPieSeries &pieSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QPieSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractSeries"))
            in >> static_cast<QAbstractSeries &>(pieSeries);
        else if (label == QString("holeSize")) {

            qreal size;

            in >> size;

            pieSeries.setHoleSize(size);

        } else if (label == QString("horizontalPosition")) {

            qreal position;

            in >> position;

            pieSeries.setHorizontalPosition(position);

        } else if (label == QString("pieEndAngle")) {

            qreal angle;

            in >> angle;

            pieSeries.setPieEndAngle(angle);

        } else if (label == QString("pieSize")) {

            qreal size;

            in >> size;

            pieSeries.setPieSize(size);

        }else if (label == QString("pieStartAngle")) {

            qreal angle;

            in >> angle;

            pieSeries.setPieStartAngle(angle);

        } else if (label == QString("slices")) {

            int count;

            in >> count;

            for (int i = 0; i < count; ++i) {

                QPieSlice *slice = new QPieSlice();

                in >> *slice;

                pieSeries.append(slice);

            }

        } else if (label == QString("verticalPosition")) {

            qreal position;

            in >> position;

            pieSeries.setVerticalPosition(position);

        } else if (label == QString("_QPieSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QXYSeries &xySeries)
{

    out << QString("_QXYSeries_begin#");

    out << QString("abstractSeries") << static_cast<const QAbstractSeries &>(xySeries);

    out << QString("brush") << xySeries.brush();

    out << QString("pen") << xySeries.pen();

    out << QString("points") <<  xySeries.points();

    out << QString("pointLabelsClipping") << xySeries.pointLabelsClipping();

    out << QString("pointLabelsFont") << xySeries.pointLabelsFont();

    out << QString("pointLabelsFormat") << xySeries.pointLabelsFormat();

    out << QString("pointLabelsVisible") << xySeries.pointLabelsVisible();

    out << QString("pointsVisible") << xySeries.pointsVisible();

    out << QString("_QXYSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QXYSeries &xySeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QXYSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("abstractSeries"))
            in >> static_cast<QAbstractSeries &>(xySeries);
        else if (label == QString("brush")) {

            QBrush brush;

            in >> brush;

            xySeries.setBrush(brush);

        } else if (label == QString("pen")) {

            QPen pen;

            in >> pen;

            xySeries.setPen(pen);

        } else if (label == QString("points")) {

            QList<QPointF> points;

            in >> points;

            xySeries << points;

        } else if (label == QString("pointLabelsClipping")) {

            bool clipping;

            in >> clipping;

            xySeries.setPointLabelsClipping(clipping);

        } else if (label == QString("pointLabelsFont")) {

            QFont font;

            in >> font;

            xySeries.setPointLabelsFont(font);

        } else if (label == QString("pointLabelsFormat")) {

            QString format;

            in >> format;

            xySeries.setPointLabelsFormat(format);

        } else if (label == QString("pointLabelsVisible")) {

            bool visible;

            in >> visible;

            xySeries.setPointLabelsVisible(visible);

        } else if (label == QString("pointsVisible")) {

            bool visible;

            in >> visible;

            xySeries.setPointsVisible(visible);

        } else if (label == QString("_QXYSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QLineSeries &lineSeries)
{

    out << QString("_QLineSeries_begin#");

    out << QString("xySeries") << static_cast<const QXYSeries &>(lineSeries);

    out << QString("_QLineSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QLineSeries &lineSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QLineSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("xySeries"))
            in >> static_cast<QXYSeries &>(lineSeries);
        else if (label == QString("_QLineSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QSplineSeries &splineSeries)
{

    out << QString("_QSplineSeries_begin#");

    out << QString("lineSeries") << static_cast<const QLineSeries &>(splineSeries);

    out << QString("_QSplineSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QSplineSeries &splineSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QSplineSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("lineSeries"))
            in >> static_cast<QLineSeries &>(splineSeries);
        else if (label == QString("_QSplineSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QScatterSeries &scatterSeries)
{

    out << QString("_QScatterSeries_begin#");

    out << QString("xySeries") << static_cast<const QXYSeries &>(scatterSeries);

    out << QString("markerShape") << static_cast<int>(scatterSeries.markerShape());

    out << QString("markerSize") << scatterSeries.markerSize();

    out << QString("_QScatterSeries_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QScatterSeries &scatterSeries)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QScatterSeries_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("xySeries"))
            in >> static_cast<QXYSeries &>(scatterSeries);
        else if (label == QString("markerShape")) {

            int shape;

            in >> shape;

            scatterSeries.setMarkerShape(static_cast<QScatterSeries::MarkerShape>(shape));

        } else if (label == QString("markerSize")) {

            qreal size;

            in >> size;

            scatterSeries.setMarkerSize(size);

        } else if (label == QString("_QScatterSeries_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QBarSet &barSet)
{

    out << QString("_QBarSet_begin#");

    out << QString("brush") << barSet.brush();

    out << QString("label") << barSet.label();

    out << QString("labelColor") << const_cast<QBarSet &>(barSet).labelColor();

    out << QString("labelFont") << barSet.labelFont();

    out << QString("pen") << barSet.pen();

    out << QString("values") << barSet.count();

    for (int i = 0; i < barSet.count(); ++i)
        out << barSet.at(i);

    out << QString("_QBarSet_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QBarSet &barSet)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QBarSet_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("brush")) {

            QBrush brush;

            in >> brush;

            barSet.setBrush(brush);

        } else if (label == QString("label")) {

            QString label;

            in >> label;

            barSet.setLabel(label);

        } else if (label == QString("labelColor")) {

            QColor color;

            in >> color;

            barSet.setLabelColor(color);

        } else if (label == QString("labelFont")) {

            QFont font;

            in >> font;

            barSet.setLabelFont(font);

        } else if (label == QString("pen")) {

            QPen pen;

            in >> pen;

            barSet.setPen(pen);

        } else if (label == QString("values")) {

            int count;

            in >> count;

            for (int i = 0; i < count; ++i) {

                qreal value;

                in >> value;

                barSet.append(value);

            }

        } else if (label == QString("_QBarSet_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QBoxSet &boxSet)
{

    out << QString("_QBoxSet_begin#");

    out << QString("brush") << boxSet.brush();

    out << QString("label") << boxSet.label();

    out << QString("pen") << boxSet.pen();

    out << QString("values") << boxSet.count();

    for (int i = 0; i < boxSet.count(); ++i)
        out << boxSet.at(i);

    out << QString("_QBoxSet_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QBoxSet &boxSet)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QBoxSet_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("brush")) {

            QBrush brush;

            in >> brush;

            boxSet.setBrush(brush);

        } else if (label == QString("label")) {

            QString label;

            in >> label;

            boxSet.setLabel(label);

        } else if (label == QString("pen")) {

            QPen pen;

            in >> pen;

            boxSet.setPen(pen);

        } else if (label == QString("values")) {

            int count;

            in >> count;

            for (int i = 0; i < count; ++i) {

                qreal value;

                in >> value;

                boxSet.append(value);

            }

        } else if (label == QString("_QBoxSet_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QPieSlice &pieSlice)
{

    out << QString("_QPieSlice_begin#");

    out << QString("borderWidth") << const_cast<QPieSlice &>(pieSlice).borderWidth();

    out << QString("brush") << pieSlice.brush();

    out << QString("explodeDistanceFactor") << pieSlice.explodeDistanceFactor();

    out << QString("exploded") << pieSlice.isExploded();

    out << QString("label") << pieSlice.label();

    out << QString("labelArmLengthFactor") << pieSlice.labelArmLengthFactor();

    out << QString("labelBrush") << pieSlice.labelBrush();

    out << QString("labelFont") << pieSlice.labelFont();

    out << QString("labelPosition") << static_cast<int>(const_cast<QPieSlice &>(pieSlice).labelPosition());

    out << QString("labelVisible") << pieSlice.isLabelVisible();

    out << QString("pen") << pieSlice.pen();

    out << QString("value") << pieSlice.value();

    out << QString("_QPieSlice_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QPieSlice &pieSlice)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QPieSlice_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("borderWidth")) {

            int width;

            in >> width;

            pieSlice.setBorderWidth(width);

        } else if (label == QString("brush")) {

            QBrush brush;

            in >> brush;

            pieSlice.setBrush(brush);

        } else if (label == QString("explodeDistanceFactor")) {

            qreal explodeDistanceFactor;

            in >> explodeDistanceFactor;

            pieSlice.setExplodeDistanceFactor(explodeDistanceFactor);

        } else if (label == QString("exploded")) {

            bool exploded;

            in >> exploded;

            pieSlice.setExploded(exploded);

        } else if (label == QString("label")) {

            QString label;

            in >> label;

            pieSlice.setLabel(label);

        } else if (label == QString("labelArmLengthFactor")) {

            qreal labelArmLengthFactor;

            in >> labelArmLengthFactor;

            pieSlice.setLabelArmLengthFactor(labelArmLengthFactor);

        } else if (label == QString("labelBrush")) {

            QBrush brush;

            in >> brush;

            pieSlice.setLabelBrush(brush);

        } else if (label == QString("labelFont")) {

            QFont font;

            in >> font;

            pieSlice.setLabelFont(font);

        } else if (label == QString("labelPosition")) {

            int position;

            in >> position;

            pieSlice.setLabelPosition(static_cast<QPieSlice::LabelPosition>(position));

        } else if (label == QString("labelVisible")) {

            bool visible;

            in >> visible;

            pieSlice.setLabelVisible(visible);

        } else if (label == QString("pen")) {

            QPen pen;

            in >> pen;

            pieSlice.setPen(pen);

        } else if (label == QString("value")) {

            qreal value;

            in >> value;

            pieSlice.setValue(value);

        } else if (label == QString("_QPieSlice_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QChart &chart)
{

    out << QString("_QChart_begin#");

    out << QString("animationDuration") << chart.animationDuration();

    out << QString("animationEasingCurve") << chart.animationEasingCurve();

    out << QString("animationOptions") << static_cast<int>(chart.animationOptions());

    out << QString("axes") << chart.axes().count();

    for (int i = 0; i < chart.axes().count(); ++i) {

        switch(chart.axes().at(i)->type()) {

            case QAbstractAxis::AxisTypeValue : out << static_cast<int>(QAbstractAxis::AxisTypeValue) << chart.axes().at(i)->alignment() << *static_cast<const QValueAxis *>(chart.axes().at(i)); break;

            case QAbstractAxis::AxisTypeBarCategory : out << static_cast<int>(QAbstractAxis::AxisTypeBarCategory) << chart.axes().at(i)->alignment() << *static_cast<const QBarCategoryAxis *>(chart.axes().at(i)); break;

            case QAbstractAxis::AxisTypeCategory : out << static_cast<int>(QAbstractAxis::AxisTypeCategory) << chart.axes().at(i)->alignment() << *static_cast<const QCategoryAxis *>(chart.axes().at(i)); break;

            case QAbstractAxis::AxisTypeDateTime : out << static_cast<int>(QAbstractAxis::AxisTypeDateTime) << chart.axes().at(i)->alignment() << *static_cast<const QDateTimeAxis *>(chart.axes().at(i)); break;

            case QAbstractAxis::AxisTypeLogValue : out << static_cast<int>(QAbstractAxis::AxisTypeLogValue) << chart.axes().at(i)->alignment() << *static_cast<const QLogValueAxis *>(chart.axes().at(i)); break;

            case QAbstractAxis::AxisTypeNoAxis : return out;

        }

    }

    out << QString("backgroundBrush") << chart.backgroundBrush();

    out << QString("backgroundPen") << chart.backgroundPen();

    out << QString("backgroundRoundness") << chart.backgroundRoundness();

    out << QString("backgroundVisible") << chart.isBackgroundVisible();

    out << QString("dropShadowEnabled") << chart.isDropShadowEnabled();

    out << QString("legend") << *chart.legend();

    out << QString("margins") << chart.margins();

    out << QString("plotAreaBackgroundBrush") << chart.plotAreaBackgroundBrush();

    out << QString("plotAreaBackgroundPen") << chart.plotAreaBackgroundPen();

    out << QString("plotAreaBackgroundVisible") << chart.isPlotAreaBackgroundVisible();

    out << QString("series") << chart.series().count();

    for (int i = 0; i < chart.series().count(); ++i) {

        QAbstractSeries * const &serie(chart.series().at(i));

        switch(serie->type()) {

            case QAbstractSeries::SeriesTypeLine : out << static_cast<int>(QAbstractSeries::SeriesTypeLine) << *static_cast<const QLineSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeArea : out << static_cast<int>(QAbstractSeries::SeriesTypeArea) << *static_cast<const QAreaSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeBar : out << static_cast<int>(QAbstractSeries::SeriesTypeBar) << *static_cast<const QBarSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeStackedBar : out << static_cast<int>(QAbstractSeries::SeriesTypeStackedBar) << *static_cast<const QStackedBarSeries *>(serie); break;

            case QAbstractSeries::SeriesTypePercentBar : out << static_cast<int>(QAbstractSeries::SeriesTypePercentBar) << *static_cast<const QPercentBarSeries *>(serie); break;

            case QAbstractSeries::SeriesTypePie : out << static_cast<int>(QAbstractSeries::SeriesTypePie) << *static_cast<const QPieSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeScatter : out << static_cast<int>(QAbstractSeries::SeriesTypeScatter) << *static_cast<const QScatterSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeSpline : out << static_cast<int>(QAbstractSeries::SeriesTypeSpline) << *static_cast<const QSplineSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeHorizontalBar : out << static_cast<int>(QAbstractSeries::SeriesTypeHorizontalBar) << *static_cast<const QHorizontalBarSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeHorizontalStackedBar : out << static_cast<int>(QAbstractSeries::SeriesTypeHorizontalStackedBar) << *static_cast<const QHorizontalStackedBarSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeHorizontalPercentBar : out << static_cast<int>(QAbstractSeries::SeriesTypeHorizontalPercentBar) << *static_cast<const QHorizontalPercentBarSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeBoxPlot : out << static_cast<int>(QAbstractSeries::SeriesTypeBoxPlot) << *static_cast<const QBoxPlotSeries *>(serie); break;

            case QAbstractSeries::SeriesTypeCandlestick : out << static_cast<int>(QAbstractSeries::SeriesTypeCandlestick) << *static_cast<const QCandlestickSeries *>(serie); break;

        }

        QList<int> indexOfAttachedAxes;

        for (QAbstractAxis * const &abstractAxis : serie->attachedAxes()) {

            int indexOfAttachedAxis = chart.axes().indexOf(abstractAxis);

            if (indexOfAttachedAxis != -1)
                indexOfAttachedAxes << indexOfAttachedAxis;

        }

        out << indexOfAttachedAxes.count();

        for (const int &index : indexOfAttachedAxes)
            out << index;


    }

    out << QString("title") << chart.title();

    out << QString("titleBrush") << chart.titleBrush();

    out << QString("titleFont") << chart.titleFont();

    out << QString("_QChart_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, QChart &chart)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QChart_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("animationDuration")) {

            int duration;

            in >> duration;

            chart.setAnimationDuration(duration);

        } else if (label == QString("animationEasingCurve")) {

            QEasingCurve easingCurve;

            in >> easingCurve;

            chart.setAnimationEasingCurve(easingCurve);

        } else if (label == QString("animationOptions")) {

            int animationOptions;

            in >> animationOptions;

            chart.setAnimationOptions(static_cast<QChart::AnimationOptions>(animationOptions));

        } else if (label == QString("axes")) {

            int numberOfAxes;

            in >> numberOfAxes;

            for (int i = 0; i < numberOfAxes; ++i) {

                int axisType;

                in >> axisType;

                int alignmentValue;

                in >> alignmentValue;

                Qt::Alignment alignment = static_cast<Qt::Alignment>(alignmentValue);

                switch (static_cast<QAbstractAxis::AxisType>(axisType)) {

                    case QAbstractAxis::AxisTypeValue : { QValueAxis *axis = new QValueAxis; in >> *axis;chart.addAxis(axis, alignment);} break;

                    case QAbstractAxis::AxisTypeBarCategory : { QBarCategoryAxis *axis = new QBarCategoryAxis; in >> *axis; chart.addAxis(axis, alignment);} break;

                    case QAbstractAxis::AxisTypeCategory : { QCategoryAxis *axis = new QCategoryAxis; in >> *axis; chart.addAxis(axis, alignment);} break;

                    case QAbstractAxis::AxisTypeDateTime : { QDateTimeAxis *axis = new QDateTimeAxis; in >> *axis; chart.addAxis(axis, alignment);} break;

                    case QAbstractAxis::AxisTypeLogValue : { QLogValueAxis *axis = new QLogValueAxis; in >> *axis; chart.addAxis(axis, alignment);} break;

                    case QAbstractAxis::AxisTypeNoAxis : ;

                }

            }

        } else if (label == QString("backgroundBrush")) {

            QBrush brush;

            in >> brush;

            chart.setBackgroundBrush(brush);

        } else if (label == QString("backgroundPen")) {

            QPen pen;

            in >> pen;

            chart.setBackgroundPen(pen);

        } else if (label == QString("backgroundRoundness")) {

            qreal diameter;

            in >> diameter;

            chart.setBackgroundRoundness(diameter);

        } else if (label == QString("backgroundVisible")) {

            bool visible;

            in >> visible;

            chart.setBackgroundVisible(visible);

        } else if (label == QString("dropShadowEnabled")) {

            bool enable;

            in >> enable;

            chart.setDropShadowEnabled(enable);

        } else if (label == QString("legend")) {

            in >> *chart.legend();

        } else if (label == QString("margins")) {

            QMargins margins;

            in >> margins;

            chart.setMargins(margins);

        } else if (label == QString("plotAreaBackgroundBrush")) {

            QBrush brush;

            in >> brush;

            chart.setPlotAreaBackgroundBrush(brush);

        } else if (label == QString("plotAreaBackgroundPen")) {

            QPen pen;

            in >> pen;

            chart.setPlotAreaBackgroundPen(pen);

        } else if (label == QString("plotAreaBackgroundVisible")) {

            bool visible;

            in >> visible;

            chart.setPlotAreaBackgroundVisible(visible);

        } else if (label == QString("series")) {

            int numberOfSeries;

            in >> numberOfSeries;

            for (int i = 0; i < numberOfSeries; ++i) {

                int seriesType;

                in >> seriesType;

                QAbstractSeries *abstractSeries;

                switch (static_cast<QAbstractSeries::SeriesType>(seriesType)) {

                    case QAbstractSeries::SeriesTypeLine : { QLineSeries *series = new QLineSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeArea : { QAreaSeries *series = new QAreaSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeBar : { QBarSeries *series = new QBarSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeStackedBar : { QStackedBarSeries *series = new QStackedBarSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypePercentBar : { QPercentBarSeries *series = new QPercentBarSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypePie : { QPieSeries *series = new QPieSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeScatter : { QScatterSeries *series = new QScatterSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeSpline : { QSplineSeries *series = new QSplineSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeHorizontalBar : { QHorizontalBarSeries *series = new QHorizontalBarSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeHorizontalStackedBar : { QHorizontalStackedBarSeries *series = new QHorizontalStackedBarSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeHorizontalPercentBar : { QHorizontalPercentBarSeries *series = new QHorizontalPercentBarSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeBoxPlot : { QBoxPlotSeries *series = new QBoxPlotSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    case QAbstractSeries::SeriesTypeCandlestick : { QCandlestickSeries *series = new QCandlestickSeries; in >> *series; chart.addSeries(series); abstractSeries = series;} break;

                    default : abstractSeries = new QLineSeries; break;

                }

                int numberOfAttachedAxes;

                in >> numberOfAttachedAxes;

                for (int i = 0; i < numberOfAttachedAxes; ++i) {

                    int indexOfAttachedAxis;

                    in >> indexOfAttachedAxis;

                    abstractSeries->attachAxis(chart.axes().at(indexOfAttachedAxis));

                }

            }

        } else if (label == QString("title")) {

            QString title;

            in >> title;

            chart.setTitle(title);

        } else if (label == QString("titleBrush")) {

            QBrush brush;

            in >> brush;

            chart.setTitleBrush(brush);

        } else if (label == QString("titleFont")) {

            QFont font;

            in >> font;

            chart.setTitleFont(font);

        } else if (label == QString("_QChart_end#"))
            return in;

    }

    return in;

}

QDataStream& operator<<(QDataStream& out, const QLegend &legend)
{

    out << QString("_QLegend_begin#");

    out << QString("alignment") << legend.alignment();

    out << QString("backgroundVisible") << legend.isBackgroundVisible();

    out << QString("brush") << legend.brush();

    out << QString("font") << legend.font();

    out << QString("borderColor") << const_cast<QLegend &>(legend).borderColor();

    out << QString("pen") << legend.pen();

    out << QString("reverseMarkers") << const_cast<QLegend &>(legend).reverseMarkers();

    out << QString("showToolTips") << legend.showToolTips();

    out << QString("visible") << legend.isVisible();

    out << QString("_QLegend_end#");

    return out;
}

QDataStream& operator>>(QDataStream& in, QLegend &legend)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_QLegend_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("alignment")) {

            int alignment;

            in >> alignment;

            legend.setAlignment(static_cast<Qt::Alignment>(alignment));

        } else if (label == QString("backgroundVisible")) {

            bool visible;

            in >> visible;

            legend.setBackgroundVisible(visible);

        } else if (label == QString("brush")) {

            QBrush brush;

            in >> brush;

            legend.setBrush(brush);

        } else if (label == QString("font")) {

            QFont font;

            in >> font;

            legend.setFont(font);

        } else if (label == QString("borderColor")) {

            QColor color;

            in >> color;

            legend.setBorderColor(color);

        } else if (label == QString("pen")) {

            QPen pen;

            in >> pen;

            legend.setPen(pen);

        } else if (label == QString("reverseMarkers")) {

            bool enable;

            in >> enable;

            legend.setReverseMarkers(enable);

        } else if (label == QString("showToolTips")) {

            bool show;

            in >> show;

            legend.setShowToolTips(show);

        } else if (label == QString("visible")) {

            bool visible;

            in >> visible;

            legend.setVisible(visible);

        } else if (label == QString("_QLegend_end#"))
            return in;

    }

    return in;

}
