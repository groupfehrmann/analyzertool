#ifndef LUDCMP_H
#define LUDCMP_H

#include <QVector>

#include <cmath>





struct LUdcmp {

    int n;

    QVector<QVector<double> > lu;

    QVector<int> indx;

    double d;

    QVector<QVector<double> > &aref;

    LUdcmp( QVector<QVector<double> > &a );

    void solve( QVector<double> &b, QVector<double> &x );

    void solve( QVector<QVector<double> > &b, QVector<QVector<double> > &x );

    void inverse( QVector<QVector<double> > &ainv );

    double det();

    void mprove( QVector<double> &b, QVector<double> &x );

    QString errorMessage;

    bool succes;
};

#endif
