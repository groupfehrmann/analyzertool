#include "mathdescriptives.h"

double MathDescriptives::sum(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    long double _sum = static_cast<long double>(0.0);

    for (const double &value : vector)
        _sum += static_cast<long double>(value);

    return static_cast<double>(_sum);

}

double MathDescriptives::sumOfAbsoluteValues(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    long double _sumOfAbsoluteValues = static_cast<long double>(0.0);

    for (const double &value : vector)
        _sumOfAbsoluteValues += std::fabs(static_cast<long double>(value));

    return static_cast<double>(_sumOfAbsoluteValues);

}

double MathDescriptives::sumOfSquaredValues(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    long double _sumOfSquaredValues = static_cast<long double>(0.0);

    for (const double &value : vector)
        _sumOfSquaredValues += static_cast<long double>(value) * static_cast<long double>(value);

    return static_cast<double>(_sumOfSquaredValues);

}

double MathDescriptives::mean(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    return MathDescriptives::sum(vector) / static_cast<double>(vector.size());

}

double MathDescriptives::meanOfAbsoluteValues(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    return MathDescriptives::sumOfAbsoluteValues(vector) / static_cast<double>(vector.size());

}

double MathDescriptives::meanOfSquaredValues(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    return MathDescriptives::sumOfSquaredValues(vector) / static_cast<double>(vector.size());

}

double MathDescriptives::median(QVector<double> vector)
{

    std::sort(vector.begin(), vector.end());

    return MathDescriptives::percentileOfPresortedVector(vector, 0.5);

}

double MathDescriptives::variance(const QVector<double> &vector)
{

    if (vector.isEmpty() || (vector.size() == 1))
        return std::numeric_limits<double>::quiet_NaN();

    double m = MathDescriptives::mean(vector);

    double ep = 0.0;

    double variance = 0.0;

    double n = vector.size();

    for (int i = 0; i < vector.size(); ++i) {

        double sum = vector.at(i) - m;

        ep += sum;

        variance += sum * sum;

    }

    return (variance - ep * ep / n) / (n - 1.0);

}

double MathDescriptives::standardDeviation(const QVector<double> &vector)
{

    if (vector.isEmpty() || (vector.size() == 1))
        return std::numeric_limits<double>::quiet_NaN();

    return std::sqrt(MathDescriptives::variance(vector));

}

QPair<double, double> MathDescriptives::meanAndVariance(const QVector<double> &vector)
{

    if (vector.isEmpty() || (vector.size() == 1))
        return QPair<double, double>(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());

    double m = MathDescriptives::mean(vector);

    double ep = 0.0;

    double variance = 0.0;

    double n = vector.size();

    for (int i = 0; i < vector.size(); ++i) {

        double sum = vector.at(i) - m;

        ep += sum;

        variance += sum * sum;

    }

    return QPair<double, double>(m, ((variance - ep * ep / n) / (n - 1.0)));

}

QPair<double, double> MathDescriptives::meanAndStandardDeviation(const QVector<double> &vector)
{

    if (vector.isEmpty() || (vector.size() == 1))
        return QPair<double, double>(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());

    QPair<double, double> meandAndVariance = MathDescriptives::meanAndVariance(vector);

    return QPair<double, double>(meandAndVariance.first, std::sqrt(meandAndVariance.second));

}

double MathDescriptives::l2Norm(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    return std::sqrt(MathDescriptives::sumOfSquaredValues(vector));

}

QVector<double> MathDescriptives::sum_vertical(const QVector<QVector<double> > &matrix)
{

    if (matrix.isEmpty())
        return QVector<double>();

    QVector<long double> _sum_vertical(matrix.first().size(), static_cast<long double>(0.0));

    for (const QVector<double> &vector : matrix) {

        for (int i = 0; i < vector.size(); ++i)
            _sum_vertical[i] += static_cast<long double>(vector.at(i));

    }

    QVector<double> result(_sum_vertical.size());

    for (int i = 0; i < result.size(); ++i)
        result[i] = static_cast<double>(_sum_vertical.at(i));

    return result;

}

QVector<double> MathDescriptives::sum_vertical_multiThreaded(const QVector<QVector<double> > &matrix)
{

    if (matrix.isEmpty())
        return QVector<double>();

    QMutex mutex;

    QVector<double> _sum_vertical(matrix.first().size(), 0.0);

    auto mapFunction = [&](const QPair<int, int> &block) {

        QVector<long double> _sum_vertical_intermediateResult(matrix.first().size(), static_cast<long double>(0.0));

        for (int i = block.first; i < block.second; ++i) {

            const QVector<double> &currentRowVectorOfMatrix(matrix.at(i));

            for (int j = 0; j < currentRowVectorOfMatrix.size(); ++j)
                _sum_vertical_intermediateResult[j] += static_cast<long double>(currentRowVectorOfMatrix.at(j));

        }

        mutex.lock();

        for (int i = 0; i < _sum_vertical.size(); ++i)
            _sum_vertical[i] += _sum_vertical_intermediateResult.at(i);

        mutex.unlock();

    };

    QFutureSynchronizer<void> futureSynchronizer;

    for (const QPair<int, int> &block : FunctionsForMultiThreading::createIndexBlocksForMultiThreading(matrix.size(), QThreadPool::globalInstance()->maxThreadCount()))
        futureSynchronizer.addFuture(QtConcurrent::run(mapFunction, block));

    futureSynchronizer.waitForFinished();

    return _sum_vertical;

}

double MathDescriptives::minimum(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    #ifdef Q_OS_MAC

    double _minimum;

    vDSP_minvD(vector.data(), 1, &_minimum, static_cast<const unsigned long>(vector.size()));

    return _minimum;

    #else

    return *std::min_element(vector.begin(), vector.end());

    #endif


}

double MathDescriptives::minimumOfAbsoluteValues(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    #ifdef Q_OS_MAC

    double _minimumOfAbsoluteValues;

    vDSP_minmgvD(vector.data(), 1, &_minimumOfAbsoluteValues, static_cast<const unsigned long>(vector.size()));

    return _minimumOfAbsoluteValues;

    #else

    return *std::min_element(vector.begin(), vector.end(), [](const double &value1, const double &value2){return (std::abs(value1) < std::abs(value2));});

    #endif

}

double MathDescriptives::maximum(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    #ifdef Q_OS_MAC

    double _minimum;

    vDSP_maxvD(vector.data(), 1, &_minimum, static_cast<const unsigned long>(vector.size()));

    return _minimum;

    #else

    return *std::max_element(vector.begin(), vector.end());

    #endif


}

double MathDescriptives::maximumOfAbsoluteValues(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    #ifdef Q_OS_MAC

    double _maximumOfAbsoluteValues;

    vDSP_maxmgvD(vector.data(), 1, &_maximumOfAbsoluteValues, static_cast<const unsigned long>(vector.size()));

    return _maximumOfAbsoluteValues;

    #else

    return *std::min_element(vector.begin(), vector.end(), [](const double &value1, const double &value2){return (std::abs(value1) < std::abs(value2));});

    #endif

}

double MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquareMatrix(const QVector<QVector<double> > &squareMatrix)
{

    if (squareMatrix.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    QVector<double> diagonal;

    diagonal.reserve(squareMatrix.size());

    for (int i = 0; i < squareMatrix.size(); ++i)
        diagonal << std::fabs(squareMatrix.at(i).at(i));

    return MathDescriptives::minimum(diagonal);

}

QPair<double, double> MathDescriptives::minimumAndMaximum(const QVector<double> &vector)
{

    if (vector.isEmpty())
        return QPair<double, double>(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());

    #ifdef Q_OS_MAC

    double minimum;

    double maximum;

    vDSP_minvD(vector.data(), 1, &minimum, static_cast<const unsigned long>(vector.size()));

    vDSP_maxvD(vector.data(), 1, &maximum, static_cast<const unsigned long>(vector.size()));

    return QPair<double, double>(minimum, maximum);

    #else

    auto result = std::minmax_element(vector.begin(), vector.end());

    return QPair<double, double>(*result.first, *result.second);

    #endif

}

QPair<double, double> MathDescriptives::minimumAndMaximum(const QVector<QVector<double> > &matrix) {

    QVector<double> minimumsAndMaximumsPerRow;

    for (const QVector<double> &vector : matrix) {

        QPair<double, double> temp = MathDescriptives::minimumAndMaximum(vector);

        minimumsAndMaximumsPerRow << temp.first;

        minimumsAndMaximumsPerRow << temp.second;

    }

    return MathDescriptives::minimumAndMaximum(minimumsAndMaximumsPerRow);

}

QVector<std::tuple<double, double, double> > MathDescriptives::histogram_count_lowerBound_upperBound(QVector<double> vector, const double &lowerBound, const double &upperBound, int numberOfBins)
{

    if (vector.isEmpty() || (std::fabs(upperBound - lowerBound) <= std::numeric_limits<double>::epsilon()))
        return QVector<std::tuple<double, double, double> >();

    if (numberOfBins < 1)
        numberOfBins = static_cast<int>(std::ceil(std::sqrt(vector.size())));

    double binWidth = (upperBound - lowerBound) / static_cast<double>(numberOfBins);

    std::sort(vector.begin(), vector.end());

    double lowerBoundOfBin = lowerBound;

    QVector<double>::iterator startOfBin = std::lower_bound(vector.begin(), vector.end(), lowerBound);

    QVector<std::tuple<double, double, double> > _histogram(numberOfBins);

    for (std::tuple<double, double, double> &tuple : _histogram) {

        std::get<1>(tuple) = lowerBoundOfBin;

        std::get<2>(tuple) = (lowerBoundOfBin += binWidth);

        QVector<double>::iterator startOfNextBin = std::lower_bound(startOfBin, vector.end(), lowerBoundOfBin);

        std::get<0>(tuple) = std::distance(startOfBin, startOfNextBin);

        startOfBin = startOfNextBin;

    }

    return _histogram;

}

QVector<std::tuple<double, double, double> > MathDescriptives::histogram_frequency_lowerBound_upperBound(const QVector<double> &vector, const double &lowerBound, const double &upperBound, int numberOfBins)
{

    QVector<std::tuple<double, double, double> > _histogram_frequency_lowerBound_upperBound = MathDescriptives::histogram_count_lowerBound_upperBound(vector, lowerBound, upperBound, numberOfBins);

    double sumOfCounts = 0.0;

    for (const std::tuple<double, double, double> &bin : _histogram_frequency_lowerBound_upperBound)
        sumOfCounts += std::get<0>(bin);

    for (std::tuple<double, double, double> &bin : _histogram_frequency_lowerBound_upperBound)
        std::get<0>(bin) /= sumOfCounts;

    return _histogram_frequency_lowerBound_upperBound;

}

double MathDescriptives::OneSampleWilcoxonSignedRankMinusLog10PValue(const QVector<double> &vector, const double &offset)
{

    QVector<double> differences(vector.size());

    QVector<double *> pointersToDifferences;

    QVector<double> signs;

    for (int i = 0; i < vector.size(); ++i) {

        differences[i] = vector.at(i) - offset;

        signs << ((differences[i] < 0.0) ? -1 : 1);

        if (differences[i] != 0.0)
            pointersToDifferences << &differences[i];

    }


    double tiesCorrectionFactor = VectorOperations::crank_ptrs(pointersToDifferences);

    for (int i = 0; i < differences.size(); ++i)
        differences[i] *= signs.at(i);

    double countNegatives = 0.0;

    double countPositives = 0.0;

    double sumNegatives = 0.0;

    double sumPositives = 0.0;

    for (int i = 0; i < differences.size(); ++i) {

        if (differences.at(i) > 0) {

            ++countPositives;

            sumPositives += differences.at(i);

        } else if (differences.at(i) < 0) {

            ++countNegatives;

            sumNegatives += differences.at(i);

        }

    }

    double n = countNegatives + countPositives;

    double z;

    if (sumPositives < std::fabs(sumNegatives))
        z = (sumPositives - (n * (n + 1.0) / 4.0)) / sqrt(((n * (n + 1.0) * (2.0 * n + 1.0)) / 24.0) - (tiesCorrectionFactor / 48.0));
    else
        z = -1.0 * ((std::fabs(sumNegatives) - (n * (n + 1.0) / 4.0)) / sqrt(((n * (n + 1.0) * (2.0 * n + 1.0)) / 24.0) - (tiesCorrectionFactor / 48.0)));

    if (n == 0.0)
        z = 0.0;

    boost::math::normal dist(0.0, 1.0);

    return -std::log10(2.0 * boost::math::cdf(boost::math::complement(dist, fabs(z))));

}
