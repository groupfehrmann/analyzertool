#ifndef MATRIX_H
#define MATRIX_H

#include <QObject>
#include <QVector>
#include <QList>
#include <QSharedDataPointer>
#include <QtGlobal>
#include <QMetaType>
#include <QChar>
#include <QString>
#include <QSharedData>
#include <QPair>
#include <QDataStream>
#include <QSet>

#include <functional>
#include <algorithm>
#include <vector>

#include "base/basematrix.h"
#include "math/matrixoperations.h"
#include "base/convertfunctions.h"

template <typename T>
class MatrixData : public QSharedData
{

public:

    MatrixData();

    MatrixData(const MatrixData<T> &other);

    ~MatrixData();

    QVector<QVector<T> > d_elements;

};

template <typename T>
MatrixData<T>::MatrixData()
{

}

template <typename T>
MatrixData<T>::MatrixData(const MatrixData<T> &other) :
    QSharedData(other), d_elements(other.d_elements)
{

}

template <typename T>
MatrixData<T>::~MatrixData()
{

}

template <typename T>
class Matrix : public BaseMatrix
{

    template<typename U> friend class Dataset;

public:

    Matrix(QObject *parent = nullptr);

    Matrix(const Matrix<T> &other);

    ~Matrix();

    const QVector<QVector<T> > &elements() const;

    QPair<int, int> firstIndexOf(const T &value, const QPair<int, int> &from = QPair<int, int>(0,0)) const;

    QList<QPair<int, int> > indexesOf(const T &value, const QPair<int, int> &from = QPair<int, int>(0,0)) const;

    void setValueAsVariant(int rowIndex, int columnIndex, const QVariant &variant);

    void recodeVector(int index, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction);

    void recodeVector(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction);

    void recodeVectors(const QList<int> &indexes, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction);

    void recodeVectors(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction);

    std::vector<std::reference_wrapper<T> > stlVectorOfReferences(int index, BaseMatrix::Orientation orientation);

    std::vector<std::reference_wrapper<T> > stlVectorOfReferences(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation);

    std::vector<std::vector<std::reference_wrapper<T> > > stlVectorsOfReferences(const QList<int> &indexes, BaseMatrix::Orientation orientation);

    std::vector<std::vector<std::reference_wrapper<T> > > stlVectorsOfReferences(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation);

    QSet<T> uniqueValues() const;

    QSet<QString> uniqueValuesOfQString() const;

    QSet<T> uniqueValuesInVector(int index, BaseMatrix::Orientation orientation) const;

    QSet<T> uniqueValuesInVector(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QSet<T> uniqueValuesInVectors(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QSet<T> uniqueValuesInVectors(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    T &value(int rowIndex, int columnIndex);

    const T &value(int rowIndex, int columnIndex) const;

    QVariant valueAsVariant(int rowIndex, int columnIndex) const;

    QVector<T> vector(int index, BaseMatrix::Orientation orientation) const;

    QVector<T> vector(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<T*> vectorOfPointers(int index, BaseMatrix::Orientation orientation);

    QVector<T*> vectorOfPointers(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation);

    QVector<QVector<T> > vectors(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<T> > vectors(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<T*> > vectorsOfPointers(const QList<int> &indexes, BaseMatrix::Orientation orientation);

    QVector<QVector<T*> > vectorsOfPointers(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation);

    QVector<short> vectorOfShort(int index, BaseMatrix::Orientation orientation) const;

    QVector<short> vectorOfShort(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<short> > vectorsOfShort(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<short> > vectorsOfShort(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<unsigned short> vectorOfUnsignedShort(int index, BaseMatrix::Orientation orientation) const;

    QVector<unsigned short> vectorOfUnsignedShort(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned short> > vectorsOfUnsignedShort(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned short> > vectorsOfUnsignedShort(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<int> vectorOfInt(int index, BaseMatrix::Orientation orientation) const;

    QVector<int> vectorOfInt(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<int> > vectorsOfInt(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<int> > vectorsOfInt(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<unsigned int> vectorOfUnsignedInt(int index, BaseMatrix::Orientation orientation) const;

    QVector<unsigned int> vectorOfUnsignedInt(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned int> > vectorsOfUnsignedInt(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned int> > vectorsOfUnsignedInt(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<long long> vectorOfLongLong(int index, BaseMatrix::Orientation orientation) const;

    QVector<long long> vectorOfLongLong(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<long long> > vectorsOfLongLong(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<long long> > vectorsOfLongLong(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<unsigned long long> vectorOfUnsignedLongLong(int index, BaseMatrix::Orientation orientation) const;

    QVector<unsigned long long> vectorOfUnsignedLongLong(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned long long> > vectorsOfUnsignedLongLong(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned long long> > vectorsOfUnsignedLongLong(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<float> vectorOfFloat(int index, BaseMatrix::Orientation orientation) const;

    QVector<float> vectorOfFloat(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<float> > vectorsOfFloat(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<float> > vectorsOfFloat(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<double> vectorOfDouble(int index, BaseMatrix::Orientation orientation) const;

    QVector<double> vectorOfDouble(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<double> > vectorsOfDouble(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<double> > vectorsOfDouble(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QChar> vectorOfQChar(int index, BaseMatrix::Orientation orientation) const;

    QVector<QChar> vectorOfQChar(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<QChar> > vectorsOfQChar(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<QChar> > vectorsOfQChar(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<unsigned char> vectorOfUnsignedChar(int index, BaseMatrix::Orientation orientation) const;

    QVector<unsigned char> vectorOfUnsignedChar(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned char> > vectorsOfUnsignedChar(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<unsigned char> > vectorsOfUnsignedChar(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QString> vectorOfQString(int index, BaseMatrix::Orientation orientation) const;

    QVector<QString> vectorOfQString(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<QString> > vectorsOfQString(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<QString> > vectorsOfQString(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<bool> vectorOfBool(int index, BaseMatrix::Orientation orientation) const;

    QVector<bool> vectorOfBool(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<bool> > vectorsOfBool(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<bool> > vectorsOfBool(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVariant> vectorOfQVariant(int index, BaseMatrix::Orientation orientation) const;

    QVector<QVariant> vectorOfQVariant(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QVector<QVector<QVariant> > vectorsOfQVariant(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    QVector<QVector<QVariant> > vectorsOfQVariant(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    QDataStream &writeToDataStream(QDataStream &out);

    QDataStream &readFromDataStream(QDataStream &in);

    Matrix &operator=(const Matrix<T> &other);

    QVector<T> &operator[](int rowIndex);

    const QVector<T> &operator[](int rowIndex) const;

    T &operator()(int rowIndex, int columnIndex);

    const T &operator()(int rowIndex, int columnIndex) const;

    QVector<T> operator()(int rowIndex, const QList<int> &columnIndexes) const;

    template <typename U> friend QDataStream& operator<<(QDataStream& out, const Matrix<U> &matrix);

    template <typename U> friend QDataStream& operator>>(QDataStream& in, Matrix<U> &matrix);

private:

    QSharedDataPointer<MatrixData<T> > d_data;

    void insertVector(int index, Orientation orientation);

    bool insertVector(int index, const QVector<short> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<unsigned short> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<int> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<unsigned int> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<long long> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<unsigned long long> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<float> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<double> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<unsigned char> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<QChar> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<QString> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<bool> &vector, Orientation orientation, QString *conversionError = nullptr);

    bool insertVector(int index, const QVector<QVariant> &vector, Orientation orientation, QString *conversionError = nullptr);

    void removeVector(int index, BaseMatrix::Orientation orientation);

    void removeVectors(QList<int> indexes, BaseMatrix::Orientation orientation);

    void swap(int index1, int index2, BaseMatrix::Orientation orientation);

    void transpose();

    void insertVectorThatMatchesTypeOfMatrix(int index, const QVector<T> &vector, BaseMatrix::Orientation orientation);

    template <typename U> QSet<U> uniqueValuesOf_U() const;

    template <typename U> QVector<U> vectorOf_U(int index, BaseMatrix::Orientation orientation) const;

    template <typename U> QVector<U> vectorOf_U(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

    template <typename U> QVector<QVector<U> > vectorsOf_U(const QList<int> &indexes, BaseMatrix::Orientation orientation) const;

    template <typename U> QVector<QVector<U> > vectorsOf_U(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const;

};

Q_DECLARE_TYPEINFO(Matrix<short>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<unsigned short>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<int>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<unsigned int>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<long long>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<unsigned long long>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<float>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<double>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<unsigned char>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<QChar>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<QString>, Q_MOVABLE_TYPE);

Q_DECLARE_TYPEINFO(Matrix<bool>, Q_MOVABLE_TYPE);

template <typename T>
Matrix<T>::Matrix(QObject *parent) :
    BaseMatrix(parent), d_data(new MatrixData<T>)
{

    BaseMatrix::d_data->d_metaTypeIdentifier = qMetaTypeId<T>();

}

template <typename T>
Matrix<T>::Matrix(const Matrix<T> &other) :
    BaseMatrix(other.parent()), d_data(other.d_data)
{

}

template <typename T>
Matrix<T>::~Matrix()
{

}

template <typename T>
const QVector<QVector<T> > &Matrix<T>::elements() const
{

    return d_data->d_elements;

}

template <typename T>
QPair<int, int> Matrix<T>::firstIndexOf(const T &value, const QPair<int, int> &from) const
{

    for (int rowIndex = from.first; rowIndex < this->BaseMatrix::d_data->d_rowCount; ++rowIndex) {

        for (int columnIndex = from.second; columnIndex < this->BaseMatrix::d_data->d_columnCount; ++columnIndex) {

            if (d_data->d_elements.at(rowIndex).at(columnIndex) == value)
                return QPair<int, int>(rowIndex, columnIndex);

        }

    }

}

template <typename T>
QList<QPair<int, int> > Matrix<T>::indexesOf(const T &value, const QPair<int, int> &from) const
{

    QList<QPair<int, int> > _indexesOf;

    for (int rowIndex = from.first; rowIndex < this->BaseMatrix::d_data->d_rowCount; ++rowIndex) {

        for (int columnIndex = from.second; columnIndex < this->BaseMatrix::d_data->d_columnCount; ++columnIndex) {

            if (d_data->d_elements.at(rowIndex).at(columnIndex) == value)
                _indexesOf.append(QPair<int, int>(rowIndex, columnIndex));

        }

    }

    return _indexesOf;

}

template <typename T>
void Matrix<T>::insertVector(int index, Orientation orientation)
{

    this->insertVectorThatMatchesTypeOfMatrix(index, QVector<T>(this->count(BaseMatrix::switchOrientation(orientation))), orientation);

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<short> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, short>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<unsigned short> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, unsigned short>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<int> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, int>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<unsigned int> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, unsigned int>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<long long> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, long long>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<unsigned long long> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, unsigned long long>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<float> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, float>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<double> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, double>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<unsigned char> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, unsigned char>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<QChar> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index,  ConvertFunctions::convertVector<T, QChar>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<QString> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, QString>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<bool> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, bool>(vector, &ok, conversionError), orientation);

    return ok;

}

template <typename T>
bool Matrix<T>::insertVector(int index, const QVector<QVariant> &vector, Orientation orientation, QString *conversionError)
{

    bool ok;

    this->insertVectorThatMatchesTypeOfMatrix(index, ConvertFunctions::convertVector<T, QVariant>(vector, &ok, conversionError), orientation);

    return ok;

}


template <typename T>
void Matrix<T>::insertVectorThatMatchesTypeOfMatrix(int index, const QVector<T> &vector, BaseMatrix::Orientation orientation)
{

    if (vector.isEmpty())
        return;

    if (orientation == BaseMatrix::ROW) {

        d_data->d_elements.insert(index, vector);

        ++this->BaseMatrix::d_data->d_rowCount;

        this->BaseMatrix::d_data->d_columnCount = vector.size();

    } else {

        if (this->isEmpty()) {

            d_data->d_elements.reserve(vector.size());

            for(const T& value: vector)
                d_data->d_elements.append(QVector<T>{value});

        } else {

            for(int rowIndex = 0; rowIndex < d_data->d_elements.size(); ++rowIndex)
                d_data->d_elements[rowIndex].insert(index, vector.at(rowIndex));

        }

        ++this->BaseMatrix::d_data->d_columnCount;

        this->BaseMatrix::d_data->d_rowCount = vector.size();

    }

}

template <typename T>
void Matrix<T>::recodeVector(int index, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction)
{

    if (orientation == BaseMatrix::ROW)
        std::for_each(d_data->d_elements[index].begin(), d_data->d_elements[index].end(), recodeFunction);
    else {

       for(QVector<T> &currentRowVector: d_data->d_elements)
           recodeFunction(currentRowVector[index]);

    }

}

template <typename T>
void Matrix<T>::recodeVector(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction)
{

    if (orientation == BaseMatrix::ROW) {

        QVector<T> &currentRowVector(d_data->d_elements[index_orientation]);

        for(const int& index_oppositeOrientation: indexes_oppositeOrientation)
            recodeFunction(currentRowVector[index_oppositeOrientation]);

    } else {

        for(const int &index_oppositeOrientation: indexes_oppositeOrientation)
            recodeFunction(d_data->d_elements[index_oppositeOrientation][index_orientation]);

    }

}

template <typename T>
void Matrix<T>::recodeVectors(const QList<int> &indexes, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction)
{

    for(const int& index: indexes)
        this->recodeVector(index, orientation, recodeFunction);

}

template <typename T>
void Matrix<T>::recodeVectors(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation, std::function<T(T&)> recodeFunction)
{

    for(const int& index_orientation: indexes_orientation)
        this->recodeVector(index_orientation, indexes_oppositeOrientation, orientation, recodeFunction);

}

template <typename T>
void Matrix<T>::removeVector(int index, BaseMatrix::Orientation orientation)
{

    if (orientation == BaseMatrix::ROW)
        d_data->d_elements.removeAt(index);
    else {

        for(QVector<T> &currentRowVector: d_data->d_elements)
            currentRowVector.removeAt(index);

    }

}

template <typename T>
void Matrix<T>::removeVectors(QList<int> indexes, BaseMatrix::Orientation orientation)
{

    std::sort(indexes.begin(), indexes.end(), std::greater<int>());

    for(const int &index: indexes)
        this->removeVector(index, orientation);

}

template <typename T>
void Matrix<T>::setValueAsVariant(int rowIndex, int columnIndex, const QVariant &variant)
{

    T value;

    ConvertFunctions::convert(variant, value);

    d_data->d_elements[rowIndex][columnIndex] = value;

}

template <typename T>
void Matrix<T>::swap(int index1, int index2, BaseMatrix::Orientation orientation)
{

    if (orientation == BaseMatrix::ROW)
        std::swap(d_data->d_elements[index1], d_data->d_elements[index2]);
    else {

        for(QVector<T> &currentRowVector: d_data->d_elements)
            std::swap(currentRowVector[index1], currentRowVector[index2]);

    }

}

template <typename T>
std::vector<std::reference_wrapper<T> > Matrix<T>::stlVectorOfReferences(int index, BaseMatrix::Orientation orientation)
{

    std::vector<std::reference_wrapper<T> > _vector;

    _vector.reserve(this->count(BaseMatrix::switchOrientation(orientation)));

    if (orientation == BaseMatrix::ROW) {

        QVector<T> &currentRowVector(d_data->d_elements[index]);

        for(T &value: currentRowVector)
            _vector.push_back(std::ref(value));

    } else {

       for(QVector<T> &currentRowVector: d_data->d_elements)
            _vector.push_back(std::ref(currentRowVector[index]));

    }

    return _vector;

}

template <typename T>
std::vector<std::reference_wrapper<T> > Matrix<T>::stlVectorOfReferences(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation)
{

    std::vector<std::reference_wrapper<T> > _vector;

    _vector.reserve(indexes_oppositeOrientation.size());

    if (orientation == BaseMatrix::ROW) {

        QVector<T> &currentRowVector(d_data->d_elements[index_orientation]);

        for(const int& index_oppositeOrientation: indexes_oppositeOrientation)
            _vector.push_back(std::ref(currentRowVector[index_oppositeOrientation]));

    } else {

        for(const int &index_oppositeOrientation: indexes_oppositeOrientation)
            _vector.push_back(std::ref(d_data->d_elements[index_oppositeOrientation][index_orientation]));

    }

    return _vector;

}

template <typename T>
std::vector<std::vector<std::reference_wrapper<T> > > Matrix<T>::stlVectorsOfReferences(const QList<int> &indexes, BaseMatrix::Orientation orientation)
{

    std::vector<std::vector<std::reference_wrapper<T> > > _vectors;

    _vectors.reserve(indexes.size());

    for(const int& index: indexes)
        _vectors.push_back(this->stlVectorOfReferences(index, orientation));

    return _vectors;

}

template <typename T>
std::vector<std::vector<std::reference_wrapper<T> > > Matrix<T>::stlVectorsOfReferences(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation)
{

    std::vector<std::vector<std::reference_wrapper<T> > > _vectors;

    _vectors.reserve(indexes_orientation.size());

    for(const int& index_orientation: indexes_orientation)
        _vectors.push_back(this->stlVectorsOfReferences(index_orientation, indexes_oppositeOrientation, orientation));

    return _vectors;

}

template <typename T>
void Matrix<T>::transpose()
{

    MatrixOperations::transpose_inplace_multiThreaded(d_data->d_elements);

}

template <typename T>
QSet<T> Matrix<T>::uniqueValues() const
{

    QSet<T> _uniqueValues;

    for (const QVector<T> &vector : d_data->d_elements) {

        for (const T &value : vector)
            _uniqueValues.insert(value);

    }

    return _uniqueValues;

}

template <typename T>
QSet<QString> Matrix<T>::uniqueValuesOfQString() const
{

    return this->uniqueValuesOf_U<QString>();

}

template <typename T>
QSet<T> Matrix<T>::uniqueValuesInVector(int index, BaseMatrix::Orientation orientation) const
{

    QSet<T> _uniqueValuesInVector;

    for (const T &value: this->vector(index, orientation))
        _uniqueValuesInVector.insert(value);

    return _uniqueValuesInVector;

}

template <typename T>
QSet<T> Matrix<T>::uniqueValuesInVector(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    QSet<T> _uniqueValuesInVector;

    for (const T &value: this->vector(index_orientation, indexes_oppositeOrientation, orientation))
        _uniqueValuesInVector.insert(value);

    return _uniqueValuesInVector;

}

template <typename T>
QSet<T> Matrix<T>::uniqueValuesInVectors(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    QSet<T> _uniqueValuesInVector;

    for (const int &index: indexes)
        _uniqueValuesInVector.unite(this->uniqueValuesInVector(index, orientation));

    return _uniqueValuesInVector;

}

template <typename T>
QSet<T> Matrix<T>::uniqueValuesInVectors(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    QSet<T> _uniqueValuesInVector;

    for (const int &index_orientation: indexes_orientation)
        _uniqueValuesInVector.unite(this->uniqueValuesInVector(index_orientation, indexes_oppositeOrientation, orientation));

    return _uniqueValuesInVector;

}

template <typename T>
T &Matrix<T>::value(int rowIndex, int columnIndex)
{

    return this->operator ()(rowIndex, columnIndex);

}

template <typename T>
const T &Matrix<T>::value(int rowIndex, int columnIndex) const
{

    return this->operator ()(rowIndex, columnIndex);

}

template <typename T>
QVariant Matrix<T>::valueAsVariant(int rowIndex, int columnIndex) const
{

    return QVariant(d_data->d_elements.at(rowIndex).at(columnIndex));

}

template <typename T>
QVector<T> Matrix<T>::vector(int index, BaseMatrix::Orientation orientation) const
{

    if (orientation == BaseMatrix::ROW)
        return this->operator [](index);
    else {

        QVector<T> _vector;

        _vector.reserve(this->count(BaseMatrix::ROW));

        for(const QVector<T> &currentRowVector: d_data->d_elements)
            _vector << currentRowVector.at(index);

        return _vector;

    }

}

template <typename T>
QVector<T> Matrix<T>::vector(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    if (orientation == BaseMatrix::ROW)
        return this->operator ()(index_orientation, indexes_oppositeOrientation);
    else {

        QVector<T> _vector;

        _vector.reserve(indexes_oppositeOrientation.size());

        for(const int &index_oppositeOrientation: indexes_oppositeOrientation)
            _vector << d_data->d_elements.at(index_oppositeOrientation).at(index_orientation);

        return _vector;

    }

}

template <typename T>
QVector<T*> Matrix<T>::vectorOfPointers(int index, BaseMatrix::Orientation orientation)
{

   QVector<T*> _vector;

    _vector.reserve(this->count(BaseMatrix::switchOrientation(orientation)));

    if (orientation == BaseMatrix::ROW) {

        QVector<T> &currentRowVector(d_data->d_elements[index]);

        for(T &value: currentRowVector)
            _vector.push_back(&value);

    } else {

       for(QVector<T> &currentRowVector: d_data->d_elements)
            _vector.push_back(&currentRowVector[index]);

    }

    return _vector;

}

template <typename T>
QVector<T*> Matrix<T>::vectorOfPointers(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation)
{

    QVector<T*> _vector;

    _vector.reserve(indexes_oppositeOrientation.size());

    if (orientation == BaseMatrix::ROW) {

        QVector<T> &currentRowVector(d_data->d_elements[index_orientation]);

        for(const int& index_oppositeOrientation: indexes_oppositeOrientation)
            _vector.push_back(&currentRowVector[index_oppositeOrientation]);

    } else {

        for(const int &index_oppositeOrientation: indexes_oppositeOrientation)
            _vector.push_back(&d_data->d_elements[index_oppositeOrientation][index_orientation]);

    }

    return _vector;

}

template <typename T>
QVector<QVector<T> > Matrix<T>::vectors(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    QVector<QVector<T> > _vectors;

    _vectors.reserve(indexes.size());

    for(const int& index: indexes)
        _vectors << this->vector(index, orientation);

    return _vectors;

}

template <typename T>
QVector<QVector<T> > Matrix<T>::vectors(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    QVector<QVector<T> > _vectors;

    _vectors.reserve(indexes_orientation.size());

    for(const int &index_orientation: indexes_orientation)
        _vectors << this->vector(index_orientation, indexes_oppositeOrientation, orientation);

    return _vectors;

}

template <typename T>
QVector<QVector<T*> > Matrix<T>::vectorsOfPointers(const QList<int> &indexes, BaseMatrix::Orientation orientation)
{

    QVector<QVector<T*> > _vectorsOfPointers;

    _vectorsOfPointers.reserve(indexes.size());

    for(const int& index: indexes)
        _vectorsOfPointers << this->vectorOfPointers(index, orientation);

    return _vectorsOfPointers;

}

template <typename T>
QVector<QVector<T*> > Matrix<T>::vectorsOfPointers(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation)
{

    QVector<QVector<T*> > _vectorsOfPointers;

    _vectorsOfPointers.reserve(indexes_orientation.size());

    for(const int &index_orientation: indexes_orientation)
        _vectorsOfPointers << this->vectorOfPointers(index_orientation, indexes_oppositeOrientation, orientation);

    return _vectorsOfPointers;

}

template <typename T>
template <typename U>
QSet<U> Matrix<T>::uniqueValuesOf_U() const
{

    QSet<U> _uniqueValues;

    for (const T &value : this->uniqueValues()) {

        U convertedValue;

        ConvertFunctions::convert(value, convertedValue);

        _uniqueValues << convertedValue;

    }

    return _uniqueValues;

}

template <typename T>
template <typename U>
QVector<U> Matrix<T>::vectorOf_U(int index, BaseMatrix::Orientation orientation) const
{

    QVector<U> _vector;

    _vector.reserve(this->count(BaseMatrix::switchOrientation(orientation)));

    if (orientation == BaseMatrix::ROW) {

        for(const T &value: d_data->d_elements.at(index)) {

            U convertedValue;

            ConvertFunctions::convert(value, convertedValue);

            _vector << convertedValue;

        }

    } else {

        for(const QVector<T> &currentRowVector: d_data->d_elements) {

            U convertedValue;

            ConvertFunctions::convert(currentRowVector.at(index), convertedValue);

            _vector << convertedValue;

        }

    }

    return _vector;

}

template <typename T>
template <typename U>
QVector<U> Matrix<T>::vectorOf_U(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    QVector<U> _vector;

    _vector.reserve(indexes_oppositeOrientation.size());

    if (orientation == BaseMatrix::ROW) {

        QVector<T> const &currentRowVector(d_data->d_elements.at(index_orientation));

        for(const int& index_oppositeOrientation: indexes_oppositeOrientation) {

            U convertedValue;

            ConvertFunctions::convert(currentRowVector.at(index_oppositeOrientation), convertedValue);

            _vector << convertedValue;

        }

    } else {

        for(const int &index_oppositeOrientation: indexes_oppositeOrientation) {

            U convertedValue;

            ConvertFunctions::convert(d_data->d_elements.at(index_oppositeOrientation).at(index_orientation), convertedValue);

            _vector << convertedValue;

        }

    }

    return _vector;

}

template <typename T>
template <typename U>
QVector<QVector<U> > Matrix<T>::vectorsOf_U(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    QVector<QVector<U> > _vectors;

    _vectors.reserve(indexes.size());

    for(const int& index: indexes)
        _vectors << this->vectorOf_U<U>(index, orientation);

    return _vectors;

}

template <typename T>
template <typename U>
QVector<QVector<U> > Matrix<T>::vectorsOf_U(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    QVector<QVector<U> > _vectors;

    _vectors.reserve(indexes_orientation.size());

    for(const int &index_orientation: indexes_orientation)
        _vectors << this->vectorOf_U<U>(index_orientation, indexes_oppositeOrientation, orientation);

    return _vectors;

}

template <typename T>
QVector<short> Matrix<T>::vectorOfShort(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<short>(index, orientation);

}

template <typename T>
QVector<short> Matrix<T>::vectorOfShort(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<short>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<short> > Matrix<T>::vectorsOfShort(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<short>(indexes, orientation);

}

template <typename T>
QVector<QVector<short> > Matrix<T>::vectorsOfShort(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<short>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<unsigned short> Matrix<T>::vectorOfUnsignedShort(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned short>(index, orientation);

}

template <typename T>
QVector<unsigned short> Matrix<T>::vectorOfUnsignedShort(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned short>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<unsigned short> > Matrix<T>::vectorsOfUnsignedShort(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned short>(indexes, orientation);

}

template <typename T>
QVector<QVector<unsigned short> > Matrix<T>::vectorsOfUnsignedShort(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned short>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<int> Matrix<T>::vectorOfInt(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<int>(index, orientation);

}

template <typename T>
QVector<int> Matrix<T>::vectorOfInt(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<int>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<int> > Matrix<T>::vectorsOfInt(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<int>(indexes, orientation);

}

template <typename T>
QVector<QVector<int> > Matrix<T>::vectorsOfInt(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<int>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<unsigned int> Matrix<T>::vectorOfUnsignedInt(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned int>(index, orientation);

}

template <typename T>
QVector<unsigned int> Matrix<T>::vectorOfUnsignedInt(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned int>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<unsigned int> > Matrix<T>::vectorsOfUnsignedInt(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned int>(indexes, orientation);

}

template <typename T>
QVector<QVector<unsigned int> > Matrix<T>::vectorsOfUnsignedInt(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned int>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<long long> Matrix<T>::vectorOfLongLong(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<long long>(index, orientation);

}

template <typename T>
QVector<long long> Matrix<T>::vectorOfLongLong(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<long long>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<long long> > Matrix<T>::vectorsOfLongLong(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<long long>(indexes, orientation);

}

template <typename T>
QVector<QVector<long long> > Matrix<T>::vectorsOfLongLong(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<long long>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<unsigned long long> Matrix<T>::vectorOfUnsignedLongLong(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned long long>(index, orientation);

}

template <typename T>
QVector<unsigned long long> Matrix<T>::vectorOfUnsignedLongLong(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned long long>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<unsigned long long> > Matrix<T>::vectorsOfUnsignedLongLong(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned long long>(indexes, orientation);

}

template <typename T>
QVector<QVector<unsigned long long> > Matrix<T>::vectorsOfUnsignedLongLong(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned long long>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<float> Matrix<T>::vectorOfFloat(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<float>(index, orientation);

}

template <typename T>
QVector<float> Matrix<T>::vectorOfFloat(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<float>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<float> > Matrix<T>::vectorsOfFloat(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<float>(indexes, orientation);

}

template <typename T>
QVector<QVector<float> > Matrix<T>::vectorsOfFloat(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<float>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<double> Matrix<T>::vectorOfDouble(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<double>(index, orientation);

}

template <typename T>
QVector<double> Matrix<T>::vectorOfDouble(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<double>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<double> > Matrix<T>::vectorsOfDouble(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<double>(indexes, orientation);

}

template <typename T>
QVector<QVector<double> > Matrix<T>::vectorsOfDouble(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<double>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QChar> Matrix<T>::vectorOfQChar(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<QChar>(index, orientation);

}

template <typename T>
QVector<QChar> Matrix<T>::vectorOfQChar(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<QChar>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<QChar> > Matrix<T>::vectorsOfQChar(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<QChar>(indexes, orientation);

}

template <typename T>
QVector<QVector<QChar> > Matrix<T>::vectorsOfQChar(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<QChar>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<unsigned char> Matrix<T>::vectorOfUnsignedChar(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned char>(index, orientation);

}

template <typename T>
QVector<unsigned char> Matrix<T>::vectorOfUnsignedChar(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<unsigned char>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<unsigned char> > Matrix<T>::vectorsOfUnsignedChar(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned char>(indexes, orientation);

}

template <typename T>
QVector<QVector<unsigned char> > Matrix<T>::vectorsOfUnsignedChar(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<unsigned char>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QString> Matrix<T>::vectorOfQString(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<QString>(index, orientation);

}

template <typename T>
QVector<QString> Matrix<T>::vectorOfQString(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<QString>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<QString> > Matrix<T>::vectorsOfQString(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<QString>(indexes, orientation);

}

template <typename T>
QVector<QVector<QString> > Matrix<T>::vectorsOfQString(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<QString>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<bool> Matrix<T>::vectorOfBool(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<bool>(index, orientation);

}

template <typename T>
QVector<bool> Matrix<T>::vectorOfBool(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<bool>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<bool> > Matrix<T>::vectorsOfBool(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<bool>(indexes, orientation);

}

template <typename T>
QVector<QVector<bool> > Matrix<T>::vectorsOfBool(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<bool>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVariant> Matrix<T>::vectorOfQVariant(int index, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<QVariant>(index, orientation);

}

template <typename T>
QVector<QVariant> Matrix<T>::vectorOfQVariant(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorOf_U<QVariant>(index_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QVector<QVector<QVariant> > Matrix<T>::vectorsOfQVariant(const QList<int> &indexes, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<QVariant>(indexes, orientation);

}

template <typename T>
QVector<QVector<QVariant> > Matrix<T>::vectorsOfQVariant(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const
{

    return this->vectorsOf_U<QVariant>(indexes_orientation, indexes_oppositeOrientation, orientation);

}

template <typename T>
QDataStream &Matrix<T>::writeToDataStream(QDataStream &out)
{

    out << QString("_Matrix_begin#");

    out << QString("d_elements") << d_data->d_elements;

    out << QString("d_rowCount") << BaseMatrix::d_data->d_rowCount;

    out << QString("d_columnCount") << BaseMatrix::d_data->d_columnCount;

    out << QString("_Matrix_end#");

    return out;

}

template <typename T>
QDataStream &Matrix<T>::readFromDataStream(QDataStream &in)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Matrix_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_elements"))
            in >> d_data->d_elements;
        else if (label == QString("d_rowCount"))
            in >> BaseMatrix::d_data->d_rowCount;
        else if (label == QString("d_columnCount"))
            in >> BaseMatrix::d_data->d_columnCount;
        else if (label == QString("_Matrix_end#"))
            return in;

    }

    return in;

}

template <typename T>
Matrix<T> &Matrix<T>::operator=(const Matrix<T> &other)
{

    if (this != &other)
        d_data.operator =(other.d_data);

    return *this;

}

template <typename T>
QVector<T> &Matrix<T>::operator[](int rowIndex)
{

    return d_data->d_elements[rowIndex];

}

template <typename T>
const QVector<T> &Matrix<T>::operator[](int rowIndex) const
{

    return d_data->d_elements.at(rowIndex);

}

template <typename T>
T &Matrix<T>::operator()(int rowIndex, int columnIndex)
{

    return d_data->d_elements[rowIndex][columnIndex];

}

template <typename T>
const T &Matrix<T>::operator()(int rowIndex, int columnIndex) const
{

    return d_data->d_elements.at(rowIndex).at(columnIndex);

}

template <typename T>
QVector<T> Matrix<T>::operator()(int rowIndex, const QList<int> &columnIndexes) const
{

    QVector<T> _vector;

    _vector.reserve(columnIndexes.size());

    QVector<T> const &currentRowVector(d_data->d_elements.at(rowIndex));

    for(const int& columnIndex: columnIndexes)
        _vector << currentRowVector.at(columnIndex);

    return _vector;

}

template <typename U>
QDataStream& operator<<(QDataStream& out, const Matrix<U> &matrix)
{

    return matrix.writeToDataStream(out);

}

template <typename U>
QDataStream& operator>>(QDataStream& in, Matrix<U> &matrix)
{

    return matrix.readFromDataStream(in);

}

#endif // MATRIX_H
