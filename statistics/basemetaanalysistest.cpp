#include "basemetaanalysistest.h"

BaseMetaAnalysisTest::BaseMetaAnalysisTest(const QString &nameOfMetaAnalysisTest, const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    d_baseStatisticalTests(baseStatisticalTests),
    d_nameOfMetaAnalysisTest(nameOfMetaAnalysisTest),
    d_pValue(std::numeric_limits<double>::quiet_NaN()),
    d_statistic(std::numeric_limits<double>::quiet_NaN())
{

}

BaseMetaAnalysisTest::~BaseMetaAnalysisTest()
{

}

QVector<QVariant> BaseMetaAnalysisTest::baseStatisticalTestDescriptiveValues()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    return this->calculateBaseStatisticalTestDescriptiveValues();

}

QVector<QVariant> BaseMetaAnalysisTest::metaAnalysisTestDescriptiveValues()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatedPValue();

    return this->calculateMetaAnalysisTestDescriptiveValues();

}

bool BaseMetaAnalysisTest::isSignOfStatisticNegative() const
{

    return std::signbit(d_statistic);

}

double BaseMetaAnalysisTest::minusLog10TransformedPValue()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatedPValue();

    if (std::isnan(d_pValue))
        return std::numeric_limits<double>::quiet_NaN();

    return -std::log10(d_pValue);

}

const QString &BaseMetaAnalysisTest::nameOfMetaAnalysisTest() const
{

    return d_nameOfMetaAnalysisTest;

}

const double &BaseMetaAnalysisTest::pValue()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatedPValue();

    return d_pValue;

}

void BaseMetaAnalysisTest::reset()
{

    d_pValue = std::numeric_limits<double>::quiet_NaN();

    d_statistic = std::numeric_limits<double>::quiet_NaN();

    this->_reset();

}

const double &BaseMetaAnalysisTest::statistic()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    return d_statistic;

}

double BaseMetaAnalysisTest::zTransformedPValue()
{

    if (std::isnan(d_statistic))
        this->calculateStatistic();

    if (std::isnan(d_pValue))
        this->calculatedPValue();

    if (std::isnan(d_pValue))
        return std::numeric_limits<double>::quiet_NaN();

    if (d_pValue == 1.0)
        return std::copysign(0.0, d_statistic);

    boost::math::normal dist(0.0, 1.0);

    return this->isSignOfStatisticNegative() ? boost::math::quantile(dist, d_pValue / 2.0) : -boost::math::quantile(dist, d_pValue / 2.0);

}
