#ifndef MODIFYIDENTIFIERSBASEDONREGEXPWORKERCLASS_H
#define MODIFYIDENTIFIERSBASEDONREGEXPWORKERCLASS_H

#include <QRegExp>

#include "base/baseworkerclass.h"

class ModifyIdentifiersBasedOnRegExpWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString stringToReplaceWith;

        QSharedPointer<BaseDataset> baseDataset;

        BaseMatrix::Orientation orientation;

        QRegExp regularExpression;

        QPair<QStringList, QList<int> > selectedIdentifiersAndIndexes;

    };

    ModifyIdentifiersBasedOnRegExpWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    ModifyIdentifiersBasedOnRegExpWorkerClass::Parameters d_parameters;

};

#endif // MODIFYIDENTIFIERSBASEDONREGEXPWORKERCLASS_H
