#include "projectlistmodel.h"

ProjectListModelItem::ProjectListModelItem(ProjectListModelItem *parentItem, QObject *internalObject) :
    d_checked(false), d_internalObject(internalObject), d_parentItem(parentItem)
{

    if (ProjectList *projectList = dynamic_cast<ProjectList *>(d_internalObject)) {

        for (int i = 0; i < projectList->numberOfProjects(); ++i)
            this->appendChild(new ProjectListModelItem(const_cast<ProjectListModelItem*>(this), projectList->projectAt(i).data()));

    }

    if (Project *project = dynamic_cast<Project *>(d_internalObject)) {

        for (int i = 0; i < project->numberOfDatasets(); ++i)
            this->appendChild(new ProjectListModelItem(const_cast<ProjectListModelItem*>(this), project->baseDatasetAt(i).data()));

    }

}

ProjectListModelItem::~ProjectListModelItem()
{

    qDeleteAll(d_childItems);

}

void ProjectListModelItem::appendChild(ProjectListModelItem *child)
{

    d_childItems.append(child);

}

ProjectListModelItem *ProjectListModelItem::child(int row)
{

    return d_childItems.at(row);

}

int ProjectListModelItem::childCount() const
{

    return d_childItems.count();

}

int ProjectListModelItem::columnCount() const
{

    return 1;

}

QVariant ProjectListModelItem::data(int column) const
{

    if (dynamic_cast<ProjectList *>(d_internalObject)) {

        if (column == 0)
            return "Projects";

    }

    if (Project *project = dynamic_cast<Project *>(d_internalObject)) {

        if (column == 0)
            return project->name();

    }

    if (BaseDataset *baseDataset = dynamic_cast<BaseDataset *>(d_internalObject)) {

        if (column == 0)
            return baseDataset->name();

    }

    return QVariant();

}

void ProjectListModelItem::insertChild(int row, ProjectListModelItem *child)
{

    d_childItems.insert(row, child);

}

QObject *ProjectListModelItem::internalObject()
{

    return d_internalObject;

}

const QObject *ProjectListModelItem::internalObject() const
{

    return d_internalObject;

}

bool ProjectListModelItem::isChecked() const
{

    return d_checked;

}

ProjectListModelItem *ProjectListModelItem::parentItem()
{

    return d_parentItem;

}

void ProjectListModelItem::removeChild(int row)
{

    delete d_childItems[row];

    d_childItems.removeAt(row);

}

void ProjectListModelItem::removeChildren()
{

    qDeleteAll(d_childItems);

    d_childItems.clear();

}

int ProjectListModelItem::row() const
{

    if (d_parentItem)
         return d_parentItem->d_childItems.indexOf(const_cast<ProjectListModelItem*>(this));

     return 0;

}

void ProjectListModelItem::setChecked(bool checked)
{

    d_checked = checked;

}

ProjectListModel::ProjectListModel(QObject *parent, ProjectList *projectList) :
    QAbstractItemModel(parent), d_projectList(projectList)
{

    d_projectList->setParent(this);

    this->connect(d_projectList, SIGNAL(datasetAdded(QSharedPointer<BaseDataset>)), this, SIGNAL(datasetAdded(QSharedPointer<BaseDataset>)));

    this->connect(d_projectList, SIGNAL(datasetNameChanged(QUuid,QString)), this, SIGNAL(datasetNameChanged(QUuid,QString)));

    this->connect(d_projectList, SIGNAL(datasetRemoved(QUuid)), this, SIGNAL(datasetRemoved(QUuid)));

    this->connect(d_projectList, SIGNAL(projectAdded(QSharedPointer<Project>)), this, SIGNAL(projectAdded(QSharedPointer<Project>)));

    this->connect(d_projectList, SIGNAL(projectNameChanged(QUuid,QString)), this, SIGNAL(projectNameChanged(QUuid,QString)));

    this->connect(d_projectList, SIGNAL(projectRemoved(QUuid)), this, SIGNAL(projectRemoved(QUuid)));

    d_rootItem = new ProjectListModelItem(nullptr, d_projectList);

}

ProjectListModel::~ProjectListModel()
{

    delete d_rootItem;

}

int ProjectListModel::columnCount(QModelIndex const &parent) const
{

    Q_UNUSED(parent);

    return 1;

}

QVariant ProjectListModel::data(QModelIndex const &index, int role) const
{

    if (!index.isValid())
        return QVariant();

     ProjectListModelItem *item = static_cast<ProjectListModelItem*>(index.internalPointer());

     if (role == Qt::DisplayRole)
        return item->data(index.column());
     else if ((role == Qt::CheckStateRole) && (index.column() == 0)) {

         if (dynamic_cast<const BaseDataset *>(item->internalObject()))
             return item->isChecked() ? Qt::Checked: Qt::Unchecked;

     }

    return QVariant();

}

Qt::ItemFlags ProjectListModel::flags(QModelIndex const &index) const
{

    if (!index.isValid())
        return Qt::NoItemFlags;

    ProjectListModelItem *item = static_cast<ProjectListModelItem*>(index.internalPointer());

    if (item) {

        if (dynamic_cast<const Project *>(item->internalObject()))
            return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;

        if (dynamic_cast<const BaseDataset *>(item->internalObject()))
            return QAbstractItemModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsUserCheckable;

    }

    return QAbstractItemModel::flags(index);

}

QVariant ProjectListModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    Q_UNUSED(section);

    Q_UNUSED(orientation);

    Q_UNUSED(role);

    return QVariant();

}

QModelIndex ProjectListModel::index(int row, int column, const QModelIndex &parent) const
{

    if (!this->hasIndex(row, column, parent))
        return QModelIndex();

    ProjectListModelItem *parentItem;

    if (!parent.isValid())
        parentItem = d_rootItem;
    else
        parentItem = static_cast<ProjectListModelItem *>(parent.internalPointer());

    ProjectListModelItem *childItem = parentItem->child(row);

    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();

}

bool ProjectListModel::insertProject(int row, Project *project, const QModelIndex &parent)
{

    if (!parent.isValid()) {

        this->beginInsertRows(parent, row, row);

        d_projectList->insertProject(row, project);

        d_rootItem->insertChild(row, new ProjectListModelItem(d_rootItem, project));

        this->endInsertRows();

        return true;

    }

    return false;

}

bool ProjectListModel::insertBaseDataset(int row, BaseDataset *baseDataset, const QModelIndex &parent)
{

    if (!parent.isValid())
        return false;

    ProjectListModelItem *item = static_cast<ProjectListModelItem *>(parent.internalPointer());

    if (Project *project = dynamic_cast<Project *>(item->internalObject())) {

        this->beginInsertRows(parent, row, row);

        project->insertDataset(row, baseDataset);

        item->insertChild(row, new ProjectListModelItem(item, baseDataset));

        this->endInsertRows();

        return true;

    }

    return false;

}

QModelIndex ProjectListModel::parent(const QModelIndex &index) const
{

    if (!index.isValid())
        return QModelIndex();

    ProjectListModelItem *childItem = static_cast<ProjectListModelItem *>(index.internalPointer());

    ProjectListModelItem *parentItem = childItem->parentItem();

    if (parentItem == d_rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);

}

const ProjectList *ProjectListModel::projectList() const
{

    return d_projectList;

}

void ProjectListModel::removeAll()
{

    this->beginResetModel();

    d_rootItem->removeChildren();

    d_projectList->removeAllProjects();

    this->endResetModel();

}

bool ProjectListModel::removeRows(int row, int count, const QModelIndex &parent)
{

    this->beginRemoveRows(parent, row, row + count - 1);

    QList<int> rowIndexesToRemove;

    for (int i = 0; i < count; ++i)
        rowIndexesToRemove << row + i;

    ProjectListModelItem *item = parent.isValid() ? static_cast<ProjectListModelItem *>(parent.internalPointer()) : d_rootItem;

    std::sort(rowIndexesToRemove.begin(), rowIndexesToRemove.end(), std::greater<int>());

    for(const int &rowIndexToRemove: rowIndexesToRemove)
        item->removeChild(rowIndexToRemove);

    if (ProjectList *projectList = dynamic_cast<ProjectList *>(item->internalObject())) {

        projectList->removeProjects(rowIndexesToRemove);

        this->endRemoveRows();

        return true;

    }

    if (Project *project = dynamic_cast<Project *>(item->internalObject())) {

        project->removeDatasets(rowIndexesToRemove);

        this->endRemoveRows();

        return true;

    }

    this->endRemoveRows();

    return false;

}

int ProjectListModel::rowCount(QModelIndex const &parent) const
{

    ProjectListModelItem *parentItem;

    if (!parent.isValid())
        parentItem = d_rootItem;
    else
        parentItem = static_cast<ProjectListModelItem *>(parent.internalPointer());

    return parentItem->childCount();

}

bool ProjectListModel::setData(QModelIndex const &index, QVariant const &value, int role)
{

    ProjectListModelItem *item = static_cast<ProjectListModelItem *>(index.internalPointer());

    if (role == Qt::EditRole) {

        if (Project *project = dynamic_cast<Project *>(item->internalObject())) {

            project->setName(value.toString());

            emit dataChanged(index, index);

            return true;

        }

        if (BaseDataset *baseDataset = dynamic_cast<BaseDataset *>(item->internalObject())) {

            baseDataset->setName(value.toString());

            emit dataChanged(index, index);

            return true;

        }

    } else if (role == Qt::CheckStateRole) {

        item->setChecked(value.toBool());

        emit showSelectedOnly(static_cast<BaseDataset *>(item->internalObject())->universallyUniqueIdentifier(), value.toBool());

        return true;
    }

    return false;

}
