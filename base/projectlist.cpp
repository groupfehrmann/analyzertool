#include "projectlist.h"

ProjectListAuxillaryFunctions::MatchedIdentifiersAndIndexes ProjectListAuxillaryFunctions::matchIdentifiersAndIndexesBetweenDatasets(const QList<QSharedPointer<BaseDataset> > &baseDatasets, const QStringList &optionalValidIdentifiers, BaseMatrix::Orientation orientation, const QList<bool> &selectedOnlyStatusOfDatasets, const QStringList &optionalAnnotationLabelDefiningAlternativeIdentifiers, bool &ok, QString &errorMessage)
{

    QList<BaseMatrix::Orientation> orientations;

    for (int i = 0; i < baseDatasets.size(); ++i)
        orientations << orientation;

    return ProjectListAuxillaryFunctions::matchIdentifiersAndIndexesBetweenDatasets(baseDatasets, optionalValidIdentifiers, orientations, selectedOnlyStatusOfDatasets, optionalAnnotationLabelDefiningAlternativeIdentifiers, ok, errorMessage);

}

ProjectListAuxillaryFunctions::MatchedIdentifiersAndIndexes ProjectListAuxillaryFunctions::matchIdentifiersAndIndexesBetweenDatasets(const QList<QSharedPointer<BaseDataset> > &baseDatasets, const QStringList &optionalValidIdentifiers, const QList<BaseMatrix::Orientation> &orientations, const QList<bool> &selectedOnlyStatusOfDatasets, const QStringList &optionalAnnotationLabelDefiningAlternativeIdentifiers, bool &ok, QString &errorMessage) {

    ok = true;

    errorMessage = QString();

    ProjectListAuxillaryFunctions::MatchedIdentifiersAndIndexes matchedIdentifiersAndIndexes;

    if (baseDatasets.isEmpty())
        return matchedIdentifiersAndIndexes;

    QList<QHash<QString, QList<int> > > listOfIdentifierToIndexes;

    listOfIdentifierToIndexes << ((optionalAnnotationLabelDefiningAlternativeIdentifiers.isEmpty() || optionalAnnotationLabelDefiningAlternativeIdentifiers.first().isEmpty()) ? baseDatasets.first()->header(orientations.first()).identifierToIndexes(selectedOnlyStatusOfDatasets.first()) : baseDatasets.first()->annotationValueToIndexes<QString>(optionalAnnotationLabelDefiningAlternativeIdentifiers.first(), orientations.first(), selectedOnlyStatusOfDatasets.first()));

    QSet<QString> matchedIdentifiers;

    if (optionalValidIdentifiers.isEmpty())
        matchedIdentifiers = QSet<QString>(listOfIdentifierToIndexes.last().keys().begin(), listOfIdentifierToIndexes.last().keys().end());
    else {

        matchedIdentifiers = QSet<QString>(optionalValidIdentifiers.begin(), optionalValidIdentifiers.end());

        matchedIdentifiers.intersect(QSet<QString>(listOfIdentifierToIndexes.last().keys().begin(), listOfIdentifierToIndexes.last().keys().end()));

    }

    QStringList identifiersFromFirstDatasetToMergeInOriginalOrder = ((optionalAnnotationLabelDefiningAlternativeIdentifiers.isEmpty() || optionalAnnotationLabelDefiningAlternativeIdentifiers.first().isEmpty()) ? baseDatasets.first()->header(orientations.first()).identifiers(selectedOnlyStatusOfDatasets.first()) : baseDatasets.first()->annotations(orientations.first()).values_string(baseDatasets.first()->header(orientations.first()).identifiers(selectedOnlyStatusOfDatasets.first()), optionalAnnotationLabelDefiningAlternativeIdentifiers.first()));

    for (int i = 1; i < baseDatasets.size(); ++i) {

        listOfIdentifierToIndexes << ((optionalAnnotationLabelDefiningAlternativeIdentifiers.isEmpty() || optionalAnnotationLabelDefiningAlternativeIdentifiers.at(i).isEmpty()) ? baseDatasets.at(i)->header(orientations.at(i)).identifierToIndexes(selectedOnlyStatusOfDatasets.at(i)) : baseDatasets.at(i)->annotationValueToIndexes<QString>(optionalAnnotationLabelDefiningAlternativeIdentifiers.at(i), orientations.at(i)));

        matchedIdentifiers.intersect(QSet<QString>(listOfIdentifierToIndexes.last().keys().begin(), listOfIdentifierToIndexes.last().keys().end()));

    }

    matchedIdentifiersAndIndexes.baseDatasets = baseDatasets;

    matchedIdentifiersAndIndexes.matchedIndexes.resize(baseDatasets.size());

    for (int i = 0; i < baseDatasets.size(); ++i) {

        QSharedPointer<BaseDataset> baseDataset = baseDatasets.at(i);

        QHash<QString, QList<int> > identifierToIndexes = listOfIdentifierToIndexes.at(i);

        QList<int> &currentIndexes(matchedIdentifiersAndIndexes.matchedIndexes[i]);

        for (const QString &identifierFromFirstDatasetToMergeInOriginalOrder : identifiersFromFirstDatasetToMergeInOriginalOrder) {

            if (matchedIdentifiers.contains(identifierFromFirstDatasetToMergeInOriginalOrder)) {

                QString matchedIdentifier = identifierFromFirstDatasetToMergeInOriginalOrder;

                if (i == 0)
                    matchedIdentifiersAndIndexes.matchedIdentifiers << matchedIdentifier;

                if (identifierToIndexes.value(matchedIdentifier).size() > 1) {

                    if (optionalAnnotationLabelDefiningAlternativeIdentifiers.isEmpty() || optionalAnnotationLabelDefiningAlternativeIdentifiers.first().isEmpty())
                        errorMessage = "detected duplicate identifier \"" + matchedIdentifier + "\" in dataset \"" + baseDataset->parentProject()->name() + " - " + baseDataset->name() + "\" with indexes " + ConvertFunctions::toString(identifierToIndexes.value(matchedIdentifier));
                    else
                        errorMessage = "detected duplicate identifier \"" + matchedIdentifier + "\" in dataset \"" + baseDataset->parentProject()->name() + " - " + baseDataset->name() + "\" with indexes " + ConvertFunctions::toString(identifierToIndexes.value(matchedIdentifier)) + " and with annotation label \"" + optionalAnnotationLabelDefiningAlternativeIdentifiers.at(i) + "\" defining alternative identifiers";

                    ok = false;

                    return ProjectListAuxillaryFunctions::MatchedIdentifiersAndIndexes();

                } else
                    currentIndexes << identifierToIndexes.value(matchedIdentifier).first();

            }

        }

    }

    if (matchedIdentifiersAndIndexes.matchedIdentifiers.isEmpty()) {

        ok = false;

        errorMessage = "could not detect any matching identifiers";

    }

    return matchedIdentifiersAndIndexes;

}

ProjectListData::ProjectListData()
{

}

ProjectListData::ProjectListData(const ProjectListData &other) :
    QSharedData(other), d_uuidToProject(other.d_uuidToProject), d_uuidOfProjects(other.d_uuidOfProjects)
{

}

ProjectListData::~ProjectListData()
{

}


ProjectList::ProjectList(QObject *parent) :
    QObject(parent), d_data(new ProjectListData)
{

}

ProjectList::ProjectList(const ProjectList &other) :
    QObject(other.parent()), d_data(other.d_data)
{

}

ProjectList::~ProjectList()
{

}

void ProjectList::appendProject(Project *project)
{

    this->insertProject(this->numberOfProjects(), project);

}

void ProjectList::insertProject(int index, Project *project)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    project->setParent(0);

    this->connect(project, SIGNAL(datasetNameChanged(QUuid,QString)), this, SIGNAL(datasetNameChanged(QUuid,QString)));

    this->connect(project, SIGNAL(nameChanged(QUuid,QString)), this, SIGNAL(projectNameChanged(QUuid,QString)));

    this->connect(project, SIGNAL(datasetAdded(QSharedPointer<BaseDataset>)), this, SIGNAL(datasetAdded(QSharedPointer<BaseDataset>)));

    this->connect(project, SIGNAL(datasetRemoved(QUuid)), this, SIGNAL(datasetRemoved(QUuid)));

    QSharedPointer<Project> sharedPointerProject(project);

    d_data->d_uuidToProject.insert(project->universallyUniqueIdentifier(), sharedPointerProject);

    d_data->d_uuidOfProjects.insert(index, project->universallyUniqueIdentifier());

    emit this->projectAdded(sharedPointerProject);

    for (int i = 0; i < project->numberOfDatasets(); ++i)
        emit this->datasetAdded(project->baseDatasetAt(i));

}

int ProjectList::numberOfProjects() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToProject.count();

}

const QSharedPointer<Project> ProjectList::projectAt(int index) const
{
    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToProject.value(d_data->d_uuidOfProjects.at(index), QSharedPointer<Project>());

}

const QSharedPointer<Project> ProjectList::projectAt(const QUuid &uuid) const
{
    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToProject.value(uuid, QSharedPointer<Project>());

}

QList<QSharedPointer<Project> > ProjectList::projects() const
{
    QList<QSharedPointer<Project> > _projects;

    for(QSharedPointer<Project> project : d_data->d_uuidToProject)
        _projects << project;

    return _projects;

}

void ProjectList::removeAllProjects()
{

    this->removeProjects(d_data->d_uuidOfProjects);

}

void ProjectList::removeProject(const QUuid &uuid)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidToProject.value(uuid)->removeAllDatasets();

    d_data->d_uuidToProject.remove(uuid);

    d_data->d_uuidOfProjects.removeOne(uuid);

    emit this->projectRemoved(uuid);

}

void ProjectList::removeProject(int index)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    const QUuid &uuid(d_data->d_uuidOfProjects.at(index));

    d_data->d_uuidToProject.value(uuid)->removeAllDatasets();

    d_data->d_uuidToProject.remove(uuid);

    d_data->d_uuidOfProjects.removeAt(index);

    emit this->projectRemoved(uuid);

}

void ProjectList::removeProjects(const QList<QUuid> &uuids)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    for (const QUuid &uuid: uuids) {

        d_data->d_uuidToProject.value(uuid)->removeAllDatasets();

        d_data->d_uuidToProject.remove(uuid);

        d_data->d_uuidOfProjects.removeOne(uuid);

        emit this->projectRemoved(uuid);

    }

}

void ProjectList::removeProjects(const QList<int> &indexes)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QList<int> sortedIndexes = indexes;

    std::sort(sortedIndexes.begin(), sortedIndexes.end(), std::greater<int>());

    for(const int &sortedIndex: sortedIndexes) {

        const QUuid &uuid(d_data->d_uuidOfProjects.at(sortedIndex));

        d_data->d_uuidToProject.value(uuid)->removeAllDatasets();

        d_data->d_uuidToProject.remove(uuid);

        d_data->d_uuidOfProjects.removeAt(sortedIndex);

        emit this->projectRemoved(uuid);

    }

}

ProjectList &ProjectList::operator=(const ProjectList &other)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    if (this != &other)
        d_data.operator=(other.d_data);

    return *this;

}
