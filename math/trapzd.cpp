#include "trapzd.h"

Trapzd::Trapzd(std::function<double (const double &x)> func, const double &a, const double &b) :
    d_func(func), d_a(a), d_b(b), d_n(0)
{

}

double Trapzd::next() {

    d_n++;

    if (d_n == 1) {

        return d_s = 0.5 * (d_b - d_a) * (d_func(d_a) + d_func(d_b));

    } else {

        int  it = 1;

        for (int j = 1; j < d_n - 1; ++j)
            it <<= 1;

        double tnm = it;

        double del = (d_b -d_a) / tnm;

        double x = d_a + 0.5 * del;

        double sum = 0.0;

        for (int j = 0; j < it; ++j, x += del)
            sum += d_func(x);

        d_s = 0.5 *(d_s + (d_b -d_a) * sum / tnm);

        return d_s;
    }
}
