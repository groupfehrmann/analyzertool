#include "selectwithtokensworkerclass.h"

SelectWithTokensWorkerClass::SelectWithTokensWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<SelectWithTokensWorkerClass::Parameters *>(parameters))
{

}

void SelectWithTokensWorkerClass::doWork()
{

    emit processStarted("Select with tokens");

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of identifiers selected: " + QString::number(d_parameters.selectedIdentifiersAndIndexes.first.size()));

    QString str = "apply to : ";

    if (d_parameters.applyToIdentifiers) {

        str += "identifiers";

        if (!d_parameters.applyToAnnotationLabels.isEmpty())
            str += " and ";

    }

    if (!d_parameters.applyToAnnotationLabels.isEmpty()) {

        str += "annotation values with labels : " + ConvertFunctions::toString(d_parameters.applyToAnnotationLabels);

    }

    if (d_parameters.caseSensitivity == Qt::CaseSensitive)
        emit appendLogItem(LogListModel::PARAMETER, "case sensitivity: case sensitive");
    else
        emit appendLogItem(LogListModel::PARAMETER, "case sensitivity: case insensitive");

    if (d_parameters.mode == CONTAINS)
        emit appendLogItem(LogListModel::PARAMETER, "mode: contains");
    else
        emit appendLogItem(LogListModel::PARAMETER, "mode: exact match");

    emit appendLogItem(LogListModel::PARAMETER, "tokens to match:  (" + QString::number(d_parameters.tokensToMatch.size()) + "): " + ConvertFunctions::toString(d_parameters.tokensToMatch));

    emit matrixDataBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    emit annotationsBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    Header &header(d_parameters.baseDataset->header(d_parameters.orientation));

    Annotations &annotations(d_parameters.baseDataset->annotations(d_parameters.orientation));

    emit startProgress(0, "Selecting identifiers", 0, d_parameters.selectedIdentifiersAndIndexes.second.size());

    QList<int> indexesToSelect;

    QSet<QString> tokensThatCouldBeMatched;

    for (int i = 0 ; i < d_parameters.selectedIdentifiersAndIndexes.second.size(); ++i) {

        bool filterSuccessful = false;

        QString currentIdentifier = d_parameters.selectedIdentifiersAndIndexes.first.at(i);

        if (d_parameters.applyToIdentifiers) {

            for (const QString &tokenToMatch : d_parameters.tokensToMatch) {

                if (d_parameters.mode == EXACTMATCH) {

                    if (currentIdentifier == tokenToMatch) {

                        filterSuccessful = true;

                        tokensThatCouldBeMatched.insert(tokenToMatch);

                        break;

                    }

                } else {

                    if (currentIdentifier.contains(tokenToMatch)) {

                        filterSuccessful = true;

                        tokensThatCouldBeMatched.insert(tokenToMatch);

                        break;

                    }

                }

            }

        }

        if (!filterSuccessful) {

            for (const QString &annotationLabel : d_parameters.applyToAnnotationLabels) {

                QString stringToEvaluate = annotations.value(currentIdentifier, annotationLabel).toString();

                for (const QString &tokenToMatch : d_parameters.tokensToMatch) {

                    if (d_parameters.mode == EXACTMATCH) {

                        if (stringToEvaluate == tokenToMatch) {

                            filterSuccessful = true;

                            tokensThatCouldBeMatched.insert(tokenToMatch);

                            break;

                        }

                    } else {

                        if (stringToEvaluate.contains(tokenToMatch)) {

                            filterSuccessful = true;

                            tokensThatCouldBeMatched.insert(tokenToMatch);

                            break;

                        }

                    }

                    if (filterSuccessful)
                        break;

                }

            }

        }

        if (filterSuccessful)
            indexesToSelect << d_parameters.selectedIdentifiersAndIndexes.second.at(i);

        if (QThread::currentThread()->isInterruptionRequested()) {

            emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

            emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    header.deselectAll();

    header.setSelectionStatuses(indexesToSelect, true);

    emit stopProgress(0);

    emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    emit appendLogItem(LogListModel::MESSAGE, "number of identifiers selected: " + QString::number(indexesToSelect.size()));

    QStringList tokensThatCouldNotBeMatched;

    for (const QString & tokenToMatch : d_parameters.tokensToMatch) {

        if (!tokensThatCouldBeMatched.contains(tokenToMatch))
            tokensThatCouldNotBeMatched << tokenToMatch;

    }

    if (!tokensThatCouldNotBeMatched.isEmpty()) {

        std::sort(tokensThatCouldNotBeMatched.begin(), tokensThatCouldNotBeMatched.end());

        emit appendLogItem(LogListModel::WARNING, "the following tokens could not be matched (" + QString::number(tokensThatCouldNotBeMatched.size()) + "): " + ConvertFunctions::toString(tokensThatCouldNotBeMatched));

    }

    QStringList _tokensThatCouldBeMatched = tokensThatCouldBeMatched.values();

    std::sort(_tokensThatCouldBeMatched.begin(), _tokensThatCouldBeMatched.end());

    emit appendLogItem(LogListModel::MESSAGE, "the following tokens could be matched (" + QString::number(_tokensThatCouldBeMatched.size()) + "): " + ConvertFunctions::toString(_tokensThatCouldBeMatched));

    QThread::currentThread()->quit();

}
