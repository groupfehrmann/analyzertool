#include "_fisherexact.h"

double FisherExact::getPartialSumm(QVector<unsigned int> &row, QVector<unsigned int> &sumcolumn, int n)
{
    QVector<int> nominator;

    QVector<int> denominator;

    int a,b,i,k,v;

    for(i = 0; i < n; i++)
    {
        if(2 * row[i] > sumcolumn[i+2])
        {
            a = row[i];
            b = sumcolumn[i + 2] - row[i];
        }
        else
        {
            b = row[i];
            a = sumcolumn[i + 2] - row[i];
        }

        for(k = 1; k <= b; k++)
        {
            nominator.push_back(a + k);
            denominator.push_back(k);
        }
    }

    double result = 1;

    i=0;
    k=0;
    a = nominator.size();
    b = denominator.size();

    while(i < a || k < b)
    {
        v=0;
        while(i < a && (k >= b || result < denominator[k]))
        {
            result*=nominator[i];
            v++;
            i++;
        }

        if(v == 0)
        {
            result = result / denominator[k];
            k++;
        }
    }

    return result;
}

void FisherExact::getNumerator(double &summ, QVector<unsigned int> &row, int rowsum, QVector<unsigned int> &sumcolumn, double threshold, int index, int n, double global_denominator)
{
    for(unsigned int i = 0; (i <= sumcolumn[0] - rowsum && i <= sumcolumn[index+2]); i++)
    {
        row[index] = i;

        if(index == n-1)
        {
            if(rowsum + i == sumcolumn[0])
            {
                double rst = FisherExact::getPartialSumm(row, sumcolumn, n);

                if(threshold >= rst)
                    summ += rst / global_denominator;
            }
        } else {
            FisherExact::getNumerator(summ, row, rowsum + i, sumcolumn, threshold, index + 1, n, global_denominator);
        }
    }
}

double FisherExact::getDenominator(int suma, int sumb )
{
    QVector<int> nominator;
    QVector<int> denominator;

    int a,b,i,k;

    a = suma;
    b = sumb;

    if( a < b )
    {
        a = sumb;
        b = suma;
    }

    for(k = 1; k <= b; k++)
    {
        nominator.push_back(a + k);
        denominator.push_back(k);
    }

    double result = 1;

    i=0;
    k=0;
    suma = nominator.size();
    sumb = denominator.size();

    while(i < suma || k < sumb)
    {
        a=0;

        while( i < suma && (k >= b || result < denominator[k]) )
        {
            result *= nominator[i];
            a++;
            i++;
        }

        if(a == 0)
        {
            result = result / denominator[k];
            k++;
        }
    }

    return result;
}

int FisherExact::desc(const void * v1, const void * v2)
{
    int * data1 = (int *)v1;
    int * data2 = (int *)v2;

    if(data1<data2) return -1;
    if(data1>data2) return 1;

    return 0;
}

#include <QTextStream>

double FisherExact::FisherExact(const QVector<unsigned int> &A, const QVector<unsigned int> &B)
{

    QTextStream out(stdout);

    int i,rowsum, n = A.size();

    QVector<unsigned int> sumcolumn(n + 2);
    sumcolumn[0] = sumcolumn[1] = 0;

    QVector<unsigned int> row(n);

    for(i = 0; i < n; i++)
    {
        row[i] = A[i];
        sumcolumn[0] += A[i];
        sumcolumn[1] += B[i];
        sumcolumn[i + 2] = A[i] + B[i];
    }

    double summ = 0;
    double threshold = FisherExact::getPartialSumm(row, sumcolumn, n);
    double global_denominator = FisherExact::getDenominator(sumcolumn[0], sumcolumn[1]);

    out << threshold << "\t" << global_denominator << Qt::endl;

    i = sumcolumn[0];
    rowsum = sumcolumn[1];
    sumcolumn[0] = sumcolumn[1]=0;

    std::sort(sumcolumn.begin(), sumcolumn.end());
    //SortUtils::sortAscending(sumcolumn);

    if(i > rowsum)
    {
        sumcolumn[0] = rowsum;
        sumcolumn[1]=i;
    } else {
        sumcolumn[1] = rowsum;
        sumcolumn[0] =  i;
    }

    rowsum=0;
    FisherExact::getNumerator(summ, row, rowsum, sumcolumn, threshold, 0, n, global_denominator);

    return summ;
}
