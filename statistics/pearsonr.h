#ifndef PEARSONR_H
#define PEARSONR_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "boost/math/distributions/students_t.hpp"
#include "boost/math/distributions/normal.hpp"

template <typename T>
class PearsonR : public BaseStatisticalTest
{

public:

    PearsonR();

    PearsonR(const QVector<T> &independentVector, QVector<T> &dependentVector, bool pairwiseCompleteMethod = false);

    ~PearsonR();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;


    QVector<T> d_independentVector;

    QVector<T> d_dependentVectorForCalculations;

    QVector<T> *d_dependentVector;

    QVector<T> _dependentVector;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation PearsonR<T>::d_effectSizeTransformation = BaseStatisticalTest::FISHER_R_TO_Z;

template <typename T>
bool PearsonR<T>::d_hasEffectSize = true;

template <typename T>
QStringList PearsonR<T>::d_sampleDescriptiveLabels = {"Count"};

template <typename T>
int PearsonR<T>::d_maximumNumberOfSamples = 1;

template <typename T>
int PearsonR<T>::d_minimumNumberOfSamples = 1;

template <typename T>
int PearsonR<T>::d_minimumNumberOfItemsPerSample = 4;

template <typename T>
QString PearsonR<T>::d_nameOfEffectSize = "Fisher's z' transformation";

template <typename T>
QString PearsonR<T>::d_nameOfTransformedEffectSize = "Fisher's z' transformation";

template <typename T>
QString PearsonR<T>::d_nameOfStatisticalTest = "Pearson R";

template <typename T>
bool PearsonR<T>::d_rankedBased = false;

template <typename T>
QStringList PearsonR<T>::d_testDescriptiveLabels = {"Fisher's z' transformation", "Standard error Fisher's z' transformation", "Pearson R", "Lower bound 95% confidence interval of Pearson R", "Upper bound 95% confidence interval of Pearson R", "Significance (2-tailed)"};

template <typename T>
PearsonR<T>::PearsonR() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_dependentVector(&_dependentVector)
{

}

template <typename T>
PearsonR<T>::PearsonR(const QVector<T> &independentVector, QVector<T> &dependentVector, bool pairwiseCompleteMethod) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_independentVector(independentVector), d_dependentVector(&dependentVector)
{

    if (pairwiseCompleteMethod) {

        d_independentVector.clear();

        for (int i = 0; i < dependentVector.size(); ++i) {

            if (std::isfinite(dependentVector.at(i)) && std::isfinite(independentVector.at(i))) {

                _dependentVector << dependentVector.at(i);

                d_independentVector << independentVector.at(i);

            }

        }

        d_dependentVector = &_dependentVector;

    }

}

template <typename T>
PearsonR<T>::~PearsonR()
{

}

template <typename T>
QVector<QVariant> PearsonR<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    sampleDescriptiveValues << d_independentVector.size();

    return sampleDescriptiveValues;

}

template <typename T>
void PearsonR<T>::calculateStatistic()
{

    double n = d_independentVector.size();

    double ax = 0.0;

    double ay = 0.0;

    for (int i = 0; i < n; ++i) {

        ax += d_independentVector.at(i);

        ay += d_dependentVector->at(i);

    }

    ax /= n;

    ay /= n;

    double syy = 0.0;

    double sxy = 0.0;

    double sxx = 0.0;

    for (int i = 0; i < n; ++i) {

        double xt = d_independentVector.at(i) - ax;

        double yt = d_dependentVector->at(i) - ay;

        sxx += xt * xt;

        syy += yt * yt;

        sxy += xt * yt;

    }

    d_statistic = sxy / std::sqrt(sxx * syy);

    if (std::isinf(d_statistic) || std::isnan(d_statistic))
        d_statistic = 0.0;

    if (d_statistic > 1.0)
        d_statistic = 1.0;
    else if (d_statistic < -1.0)
        d_statistic = -1.0;

}

template <typename T>
void PearsonR<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::students_t dist(d_independentVector.size() - 2.0);

    double value = std::fabs(d_statistic * std::sqrt((d_independentVector.size() - 2.0) / ((1.0 - d_statistic) * (1.0 + d_statistic))));

    if (std::isinf(value))
        value = std::numeric_limits<double>::max();

    if (value == 0.0)
        value = std::numeric_limits<double>::min();

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, value));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void PearsonR<T>::calculateEffectSize()
{

    double value = double(1.0 + d_statistic) / double(1.0 - d_statistic);

    if (std::isinf(value))
        value = std::numeric_limits<double>::max();

    d_effectSize = 0.5 * std::log(value);

}

template <typename T>
void PearsonR<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = (1.0 / std::sqrt(d_independentVector.size() - 3.0));

}

template <typename T>
QVector<QVariant> PearsonR<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    boost::math::normal normalDistribution(0.0, 1.0);

    double w = boost::math::quantile(boost::math::complement(normalDistribution, 0.025)) * d_standardErrorOfEffectSize;

    double rLower = (std::exp(2.0 * (d_effectSize - w)) - 1.0) / (std::exp(2.0 * (d_effectSize - w)) + 1.0);

    double temp = std::exp(2.0 * (d_effectSize + w));

    if (std::isinf(temp) || (temp == std::numeric_limits<double>::max()))
        temp = std::numeric_limits<double>::max() - 1.0;

    double rUpper = (temp - 1.0) / (temp + 1.0);

    descriptiveValues << d_effectSize << d_standardErrorOfEffectSize << d_statistic << rLower << rUpper << d_pValue;

    return descriptiveValues;

}

template <typename T>
void PearsonR<T>::createStructuredMeasurements()
{

}

template <typename T>
void PearsonR<T>::_reset()
{

}

#endif // PEARSONR_H
