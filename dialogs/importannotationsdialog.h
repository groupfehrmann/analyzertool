#ifndef IMPORTANNOTATIONSDIALOG_H
#define IMPORTANNOTATIONSDIALOG_H

#include <QList>
#include <QFile>
#include <QRegExp>

#include "dialogs/basedialog.h"
#include "workerclasses/importannotationsworkerclass.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectfilebuildingblockwidget.h"
#include "widgets/selectdelimiterbuildingblockwidget.h"
#include "widgets/selectheaderlabelsbuildingblockwidget.h"
#include "widgets/selectnumberoflinestoreadfromheadandtailoffilebuildingblockwidget.h"

class ImportAnnotationsDialog : public BaseDialog
{

    Q_OBJECT

public:

    ImportAnnotationsDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString());


    ~ImportAnnotationsDialog();

private:

    SelectFileBuildingBlockWidget *d_selectFileBuildingBlockWidget;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_headerLine;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_firstDataLine;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_lastDataLine;

    SelectDelimiterBuildingBlockWidget *d_selectDelimiterBuildingBlockWidget;

    SelectHeaderLabelsBuildingBlockWidget *d_selectHeaderLabelsBuildingBlockWidget;

    SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget *d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget;

    QStringList d_listOfLineInHead;

    QStringList d_listOfTrimmedLineInHead;

    QStringList d_listOfLineInTail;

    QStringList d_listOfTrimmedLineInTail;

    int d_offSetLineNumber;

    int d_lineNumberOfHeaderLine;

    int d_lineNumberOfFirstDataLine;

    int d_lineNumberOfLastDataLine;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void firstDataLineChanged(int index);

    void headerLineChanged(int index);

    void lastDataLineChanged(int index);

    void pathChanged(const QString &path);

    void updateHeaderLabelsWidget();

};

#endif // IMPORTANNOTATIONSDIALOG_H
