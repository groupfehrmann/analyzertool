#ifndef WELCHT_H
#define WELCHT_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/students_t.hpp"
#include "boost/math/distributions/normal.hpp"

template <typename T>
class WelchT : public BaseStatisticalTest
{

public:

    WelchT();

    WelchT(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~WelchT();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;

    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QPair<double, double> d_meanAndVarianceOfSample1;

    QPair<double, double> d_meanAndVarianceOfSample2;

    double d_meanDifference;

    QVector<T> d_measurements;

    double d_n1;

    double d_n2;

    double d_degreeOfFreedom;

    double d_standardErrorOfMeanDifference;

    QVector<QVector<T> > d_structuredMeasurements;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation WelchT<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool WelchT<T>::d_hasEffectSize = true;

template <typename T>
QStringList WelchT<T>::d_sampleDescriptiveLabels = {"Count", "Mean", "Standard deviation", "Standard error of the mean"};

template <typename T>
int WelchT<T>::d_maximumNumberOfSamples = 2;

template <typename T>
int WelchT<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int WelchT<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString WelchT<T>::d_nameOfEffectSize = "Glass's Delta";

template <typename T>
QString WelchT<T>::d_nameOfTransformedEffectSize = "Glass's Delta";

template <typename T>
QString WelchT<T>::d_nameOfStatisticalTest = "Welch T";

template <typename T>
bool WelchT<T>::d_rankedBased = false;

template <typename T>
QStringList WelchT<T>::d_testDescriptiveLabels = {"Mean difference", "Standard error difference", "Lower bound 95% confidence interval of the difference", "Upper bound 95% confidence interval of the difference", "Point-biserial R", "Eta squared", "Omega squared", "Cohen's D", "Standard error Cohen's D", "Glass's Delta*", "Standard error Glass's Delta*", "Hedges's G", "Standard error Hedges's G", "Student T", "Degree of freedom", "Significance (2-tailed)"};

template <typename T>
WelchT<T>::WelchT() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
WelchT<T>::WelchT(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    this->createStructuredMeasurements();

}

template <typename T>
WelchT<T>::~WelchT()
{

}

template <typename T>
QVector<QVariant> WelchT<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    sampleDescriptiveValues << d_n1 << d_meanAndVarianceOfSample1.first << std::sqrt(d_meanAndVarianceOfSample1.second) << std::sqrt(d_meanAndVarianceOfSample1.second) / std::sqrt(d_n1);

    sampleDescriptiveValues << d_n2 << d_meanAndVarianceOfSample2.first << std::sqrt(d_meanAndVarianceOfSample2.second) << std::sqrt(d_meanAndVarianceOfSample2.second) / std::sqrt(d_n2);

    return sampleDescriptiveValues;

}

template <typename T>
void WelchT<T>::calculateStatistic()
{

    d_n1 = d_structuredMeasurements.at(0).size();

    d_n2 = d_structuredMeasurements.at(1).size();

    d_sampleSize = d_measurements.size();

    d_meanAndVarianceOfSample1 = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(0));

    d_meanAndVarianceOfSample2 = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(1));

    d_degreeOfFreedom = ((d_meanAndVarianceOfSample1.second / d_n1 + d_meanAndVarianceOfSample2.second / d_n2) * (d_meanAndVarianceOfSample1.second / d_n1 + d_meanAndVarianceOfSample2.second / d_n2)) / (((d_meanAndVarianceOfSample1.second / d_n1) * (d_meanAndVarianceOfSample1.second / d_n1)) / (d_n1 - 1.0) + ((d_meanAndVarianceOfSample2.second / d_n2) * (d_meanAndVarianceOfSample2.second / d_n2)) / (d_n2 - 1.0));

    d_meanDifference = d_meanAndVarianceOfSample1.first - d_meanAndVarianceOfSample2.first;

    d_standardErrorOfMeanDifference = std::sqrt(d_meanAndVarianceOfSample1.second / d_n1 + d_meanAndVarianceOfSample2.second / d_n2);

    d_statistic = d_meanDifference / d_standardErrorOfMeanDifference;

}

template <typename T>
void WelchT<T>::calculatePValue()
{

    if (std::isnan(d_statistic) || std::isnan(d_degreeOfFreedom) || (d_degreeOfFreedom == 0)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::students_t students_t_distribution(d_degreeOfFreedom);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(students_t_distribution, std::abs(d_statistic)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void WelchT<T>::calculateEffectSize()
{

    d_effectSize = d_meanDifference / d_meanAndVarianceOfSample2.second;

}

template <typename T>
void WelchT<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::sqrt(((d_n1 + d_n2) / (d_n1 * d_n2)) + ((d_effectSize * d_effectSize) / (2.0 * (d_n2 - 1.0))));

}

template <typename T>
QVector<QVariant> WelchT<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    boost::math::normal normal_distribution(0.0, 1.0);

    double w = boost::math::quantile(boost::math::complement(normal_distribution, 0.025)) * d_standardErrorOfMeanDifference;

    double cohenDEffectSize = d_meanDifference / std::sqrt(((d_n1 - 1.0) * d_meanAndVarianceOfSample1.second + (d_n2 - 1) * d_meanAndVarianceOfSample2.second) / (d_n1 + d_n2 - 2.0));

    double cohenDStandardErrorOfEffectSize = std::sqrt(((d_n1 + d_n2) / (d_n1 * d_n2)) + ((cohenDEffectSize * cohenDEffectSize) / (2.0 * (d_n1 + d_n2 - 2.0))) * ((d_n1 + d_n2) / (d_n1 + d_n2 - 2.0)));

    double hedgesGEffectSize = ((1.0 - (3.0 / (4.0 * (d_n1 + d_n2) - 9.0)))) * cohenDEffectSize;

    double hedgesGStandardErrorOfEffectSize = std::sqrt(((d_n1 + d_n2) / (d_n1 * d_n2)) + ((hedgesGEffectSize * hedgesGEffectSize) / (2.0 * (d_n1 + d_n2))));

    double r_pb = std::sqrt((d_statistic * d_statistic) / ((d_statistic * d_statistic) + d_sampleSize - 2.0));

    double omegaSquared = ((d_statistic * d_statistic) - 1.0) / ((d_statistic * d_statistic) + d_n1 + d_n2 - 1.0);

    double etaSquared = (d_statistic * d_statistic) / ((d_statistic * d_statistic) + d_sampleSize - 2.0);

    descriptiveValues << d_meanDifference << d_standardErrorOfMeanDifference << d_meanDifference - w << d_meanDifference + w << r_pb << etaSquared << omegaSquared << cohenDEffectSize << cohenDStandardErrorOfEffectSize << d_effectSize << d_standardErrorOfEffectSize << hedgesGEffectSize << hedgesGStandardErrorOfEffectSize << d_statistic << d_n1 + d_n2 - 2 << d_pValue;

    return descriptiveValues;

}

template <typename T>
void WelchT<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize(2);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

}

template <typename T>
void WelchT<T>::_reset()
{

    this->createStructuredMeasurements();

}

#endif // WELCHT_H
