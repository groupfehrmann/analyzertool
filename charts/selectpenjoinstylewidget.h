#ifndef SELECTPENJOINSTYLEWIDGET_H
#define SELECTPENJOINSTYLEWIDGET_H

#include <QWidget>
#include <QPen>
#include <QPixmap>
#include <QPainter>
#include <QPointF>

namespace Ui {

class SelectPenJoinStyleWidget;

}

class SelectPenJoinStyleWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectPenJoinStyleWidget(QWidget *parent = 0, Qt::PenJoinStyle defaultPenJoinStyle = Qt::BevelJoin);

    ~SelectPenJoinStyleWidget();

    Qt::PenJoinStyle currentPenJoinStyle();

public slots:

    void setCurrentPenJoinStyle(Qt::PenJoinStyle PenJoinStyle);

private:

    Ui::SelectPenJoinStyleWidget *ui;

    QPixmap createPixmap(Qt::PenJoinStyle PenJoinStyle);

private slots:

    void currentIndexOfComboBoxChanged(int index);

signals:

    void currentPenJoinStyleChanged(Qt::PenJoinStyle PenJoinStyle);

};

#endif // SELECTPENJOINSTYLEWIDGET_H
