#include "selectdirectorybuildingblockwidget.h"
#include "ui_selectdirectorybuildingblockwidget.h"

SelectDirectoryBuildingBlockWidget::SelectDirectoryBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, bool rememberSettings) :
    QWidget(parent), ui(new Ui::SelectDirectoryBuildingBlockWidget), d_rememberSettings(rememberSettings)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/selectDirectoryBuildingBlockWidget");

        if (settings.contains("currentDirectory"))
            ui->lineEdit->setText(settings.value("currentDirectory").toString());

        settings.endGroup();

        this->evaluateDirectory();

    }

    this->connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(pushButton_clicked()));

    this->connect(ui->lineEdit, SIGNAL(textEdited(QString)), this, SLOT(lineEdit_edited(QString)));

}

SelectDirectoryBuildingBlockWidget::~SelectDirectoryBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/selectDirectoryBuildingBlockWidget");

        settings.setValue("currentDirectory", ui->lineEdit->text());

        settings.endGroup();

    }

    delete ui;

}

void SelectDirectoryBuildingBlockWidget::evaluateDirectory()
{


    if (QDir(ui->lineEdit->text()).exists()) {

        ui->lineEdit->setStyleSheet(QString());

        emit directoryChanged(ui->lineEdit->text());

    } else {

        ui->lineEdit->setStyleSheet(QString("color: rgb(255, 0, 0);"));

        emit invalidDirectory();

    }

}

void SelectDirectoryBuildingBlockWidget::lineEdit_edited(const QString &text)
{

    Q_UNUSED(text);

    this->evaluateDirectory();

}

QLabel *SelectDirectoryBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}

QLineEdit *SelectDirectoryBuildingBlockWidget::pointerToInternalLineEdit()
{

    return ui->lineEdit;

}

QPushButton *SelectDirectoryBuildingBlockWidget::pointerToInternalPushButton()
{

    return ui->pushButton;

}

void SelectDirectoryBuildingBlockWidget::pushButton_clicked()
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - Select file");

    fileDialog.setFileMode(QFileDialog::Directory);

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    settings.endGroup();

    if (fileDialog.exec()) {

        ui->lineEdit->setText(fileDialog.directory().absolutePath());

        this->evaluateDirectory();

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/fileDialog");

        settings.setValue("state", fileDialog.saveState());

        settings.setValue("geometry", fileDialog.saveGeometry());

        settings.setValue("lastDirectory", fileDialog.directory().path());

        settings.endGroup();

    } else
        ui->lineEdit->setText(QString());

}
