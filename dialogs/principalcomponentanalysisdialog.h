#ifndef PRINCIPALCOMPONENTANALYSISDIALOG_H
#define PRINCIPALCOMPONENTANALYSISDIALOG_H

#include <QString>
#include <QSharedPointer>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectdirectorybuildingblockwidget.h"
#include "widgets/spinboxbuildingblockwidget.h"
#include "workerclasses/principalcomponentanalysisworkerclass.h"

class PrincipalComponentAnalysisDialog : public BaseDialog
{

    Q_OBJECT

public:

    PrincipalComponentAnalysisDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~PrincipalComponentAnalysisDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_mode;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_numberOfComponentsToExtract;

    SelectDirectoryBuildingBlockWidget *d_selectOutputDirectory;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void selectionOfVariableIdentifiersAndIndexesChanged(const QStringList &selectedItemsIdentifier, const QStringList &notSelectedItemsIdentifier);

};

#endif // PRINCIPALCOMPONENTANALYSISDIALOG_H
