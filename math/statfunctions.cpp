#include "statfunctions.h"

double StatFunctions::gaussianDensity(const double &x)
{

    return 0.5 * M_SQRT1_2 * M_2_SQRTPI * std::exp( - 0.5 * x * x );

    // M_SQRT1_2  -> The reciprocal of the square root of two (also the square root of 1/2).
    // M_2_SQRTPI -> Two times the reciprocal of the square root of pi.

}

double StatFunctions::gaussianKernelDensityEstimator(const double &x, const QVector<double> &empiricalDistribution, const double &bandwidth)
{

    double sum = 0.0;

    for (const double &empiricalValue : empiricalDistribution) {

        sum += gaussianDensity((x - empiricalValue) / bandwidth);

    }

    return sum / (static_cast<double>(empiricalDistribution.size()) * bandwidth);

}

double StatFunctions::qromb(std::function<double (const double &x)> func, const double &a, const double &b, const double &eps)
{

    QVector<double> s(20);

    QVector<double> h(21);

    Poly_interp polint(h, s, 5);

    h[0] = 1.0;

    Trapzd t(func, a, b);

    for (int j = 1; j <= s.size(); j++) {

        s[j-1] = t.next();

        if (j >= 5) {

            double ss = polint.rawinterp(j - 5, 0.0);

            if (std::fabs(polint.dy) <= eps * std::abs(ss))
                return ss;
        }

        h[j] = 0.25 * h.at(j-1);

    }

    return std::numeric_limits<double>::quiet_NaN();

}

double StatFunctions::fac(int n)
{
    double f = 1.0;

    for (int i = n; i > 1; --i)
        f *= double(i);

    return f;
}
