#include "selectwithtokensdialog.h"

SelectWithTokensDialog::SelectWithTokensDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new SelectWithTokensWorkerClass::Parameters;

    this->setupWidgets();

}

SelectWithTokensDialog::~SelectWithTokensDialog()
{

   delete static_cast<SelectWithTokensWorkerClass::Parameters *>(d_parameters);

}

void SelectWithTokensDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_identifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_identifiers", "Select identifiers");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_identifiers, 1, 0);

    d_comboBoxBuildingBlockWidget_mode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_mode", "Select mode", {"Contains", "Exact match"}, {SelectWithTokensWorkerClass::CONTAINS, SelectWithTokensWorkerClass::EXACTMATCH});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_mode, 2, 0);

    d_comboBoxBuildingBlockWidget_caseSensitivity = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_caseSensitivity", "Select case sensitivity", {"Case insensitive", "Case sensitive"}, {Qt::CaseInsensitive, Qt::CaseSensitive});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_caseSensitivity, 3, 0);

    d_comboBoxBuildingBlockWidget_applyToIdentifiers = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_applyToIdentifiers", "Apply to identifiers", {"Enabled", "Disabled"}, {true, false});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_applyToIdentifiers, 4, 0);

    d_itemsSelectorBuildingBlockWidget_ApplyToAnnotationLabels = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_ApplyToAnnotationLabels", "Apply to annotation values with labels");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_ApplyToAnnotationLabels, 5, 0);

    d_textEditBuildingBlockWidget_tokensToMatch = new TextEditBuildingBlockWidget(this, "textEditBuildingBlockWidget_tokensToMatch", "Enter tokens to match");

    d_gridLayout->addWidget(d_textEditBuildingBlockWidget_tokensToMatch, 6, 0);

    d_selectDelimiterBuildingBlockWidget = new SelectDelimiterBuildingBlockWidget(this, "selectDelimiterBuildingBlockWidget");

    d_gridLayout->addWidget(d_selectDelimiterBuildingBlockWidget, 7, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void SelectWithTokensDialog::initializeParameters()
{

    SelectWithTokensWorkerClass::Parameters *parameters = static_cast<SelectWithTokensWorkerClass::Parameters *>(d_parameters);

    parameters->applyToAnnotationLabels = d_itemsSelectorBuildingBlockWidget_ApplyToAnnotationLabels->selectedItemsIdentifierAndNumber().first;

    parameters->applyToIdentifiers = d_comboBoxBuildingBlockWidget_applyToIdentifiers->pointerToInternalComboBox()->currentData().toBool();

    parameters->baseDataset = d_baseDataset;

    parameters->caseSensitivity = static_cast<Qt::CaseSensitivity>(d_comboBoxBuildingBlockWidget_caseSensitivity->pointerToInternalComboBox()->currentData().toInt());

    parameters->mode = static_cast<SelectWithTokensWorkerClass::Mode>(d_comboBoxBuildingBlockWidget_mode->pointerToInternalComboBox()->currentData().toInt());

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->selectedIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_identifiers->selectedItemsIdentifierAndNumber();

    if (static_cast<SelectDelimiterBuildingBlockWidget::DelimiterType>(d_selectDelimiterBuildingBlockWidget->pointerToInternalComboBox()->currentIndex()) == SelectDelimiterBuildingBlockWidget::CUSTUM)
        parameters->tokensToMatch = d_textEditBuildingBlockWidget_tokensToMatch->pointerToInternalTextEdit()->toPlainText().split(d_selectDelimiterBuildingBlockWidget->delimiter(), Qt::SkipEmptyParts, d_selectDelimiterBuildingBlockWidget->pointerToInternalCheckBox()->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive);
    else
        parameters->tokensToMatch = d_textEditBuildingBlockWidget_tokensToMatch->pointerToInternalTextEdit()->toPlainText().split(QRegExp(d_selectDelimiterBuildingBlockWidget->delimiter()), Qt::SkipEmptyParts);

}

void SelectWithTokensDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > identifiersAndIndexes = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_identifiers->setItemsIdentifierAndNumber(identifiersAndIndexes.first, identifiersAndIndexes.second);

    d_itemsSelectorBuildingBlockWidget_ApplyToAnnotationLabels->setItemsIdentifier(d_baseDataset->annotations(orientation).labels());

}
