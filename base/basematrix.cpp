#include "basematrix.h"

BaseMatrixData::BaseMatrixData() :
    d_columnCount(0), d_metaTypeIdentifier(QMetaType::UnknownType), d_rowCount(0)
{

}

BaseMatrixData::BaseMatrixData(const BaseMatrixData &other) :
    QSharedData(other), d_columnCount(other.d_columnCount), d_metaTypeIdentifier(other.d_metaTypeIdentifier), d_rowCount(other.d_rowCount)
{

}

BaseMatrixData::~BaseMatrixData()
{

}

BaseMatrix::Orientation BaseMatrix::switchOrientation(BaseMatrix::Orientation orientation)
{

    return orientation == BaseMatrix::ROW ? BaseMatrix::COLUMN : BaseMatrix::ROW;

}

QString BaseMatrix::orientationToString(BaseMatrix::Orientation orientation)
{

    return orientation == BaseMatrix::ROW ? QString("row") : QString("column");

}

BaseMatrix::BaseMatrix(QObject *parent) :
    QObject(parent), d_data(new BaseMatrixData)
{

}

BaseMatrix::BaseMatrix(const BaseMatrix &other) :
    QObject(other.parent()), d_data(other.d_data)
{

}

BaseMatrix::~BaseMatrix()
{

}

void BaseMatrix::appendVector(BaseMatrix::Orientation orientation)
{

    this->insertVector(this->count(orientation), orientation);

}

void BaseMatrix::appendVectors(int count, BaseMatrix::Orientation orientation)
{

    for (int i = 0; i < count; ++i)
        this->insertVector(this->count(orientation), orientation);

}

bool BaseMatrix::areValidIndexes(int rowIndex, int columnIndex) const
{
    if ((rowIndex < 0) || (rowIndex >= d_data->d_rowCount))
        return false;

    if ((columnIndex < 0) || (columnIndex >= d_data->d_columnCount))
        return false;

    return true;

}

int BaseMatrix::count(BaseMatrix::Orientation orientation) const
{

    return (orientation == BaseMatrix::ROW) ? d_data->d_rowCount : d_data->d_columnCount;

}

bool BaseMatrix::isEmpty() const
{

    return d_data->d_rowCount ? false : true;

}

void BaseMatrix::insertVectors(int index, int count, Orientation orientation)
{

    for (int i = 0; i < count; ++i)
        this->insertVector(index, orientation);

}

int BaseMatrix::metaTypeIdentifier() const
{

    return d_data->d_metaTypeIdentifier;

}

QString BaseMatrix::metaTypeName() const
{

    return QString(QMetaType::typeName(d_data->d_metaTypeIdentifier));

}

void BaseMatrix::prependVector(BaseMatrix::Orientation orientation)
{

    this->insertVector(0, orientation);

}

void BaseMatrix::prependVectors(int count, BaseMatrix::Orientation orientation)
{

    this->insertVectors(0, count, orientation);

}

BaseMatrix &BaseMatrix::operator=(const BaseMatrix &other)
{

    if (this != &other)
        d_data.operator =(other.d_data);

    return *this;

}

QDataStream& operator<<(QDataStream& out, BaseMatrix &baseMatrix)
{

    return baseMatrix.writeToDataStream(out);

}

QDataStream& operator>>(QDataStream& in, BaseMatrix &baseMatrix)
{

    return baseMatrix.readFromDataStream(in);

}
