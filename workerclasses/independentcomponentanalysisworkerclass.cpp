#include "independentcomponentanalysisworkerclass.h"

IndependentComponentAnalysisWorkerClass::IndependentComponentAnalysisWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<IndependentComponentAnalysisWorkerClass::Parameters *>(parameters))
{

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i)
        d_indexesForSubSampling << i;

}

bool IndependentComponentAnalysisWorkerClass::checkInputParameters()
{

    if (!d_parameters.outputDirectory.isEmpty() && !QDir(d_parameters.outputDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.outputDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected");

        return false;

    }

    if (d_parameters.searchTimeConstant == 0) {

        emit appendLogItem(LogListModel::ERROR, "search time constant of zero is not allowed");

        return false;

    }

    return true;

}

void IndependentComponentAnalysisWorkerClass::doWork()
{

    emit processStarted("independent component analysis");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.outputDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Independent component analysis"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "contrast function: " + d_parameters.contrastFunction);

    emit appendLogItem(LogListModel::PARAMETER, "cumulative explained variance threshold that determines the number of components to enter ICA: " + QString::number(static_cast<double>(d_parameters.thresholdCumulativeExplainedVariance)));

    emit appendLogItem(LogListModel::PARAMETER, "maximum number of components to enter ICA: " + QString::number(d_parameters.thresholdMaximumNumberOfComponents));

    emit appendLogItem(LogListModel::PARAMETER, "maximum number of iterations: " + QString::number(d_parameters.maximumNumberOfIterations));

    emit appendLogItem(LogListModel::PARAMETER, "finetune mode: " + (d_parameters.finetuneMode ? QString("enabled") : QString("disabled")));

    emit appendLogItem(LogListModel::PARAMETER, "stabilization mode: " + (d_parameters.stabilizationMode ? QString("enabled") : QString("disabled")));

    emit appendLogItem(LogListModel::PARAMETER, "step size (mu) in stabilization mode: " + QString::number(d_parameters.mu));

    if (d_parameters.searchTimeConstant == -1)
        emit appendLogItem(LogListModel::PARAMETER, "search time constant in stabilization mode: disabled");
    else
        emit appendLogItem(LogListModel::PARAMETER, "search time constant in stabilization mode: " + QString::number(d_parameters.searchTimeConstant));

    emit appendLogItem(LogListModel::PARAMETER, "stopping criteria (epsilon): " + QString::number(d_parameters.epsilon));

    emit appendLogItem(LogListModel::PARAMETER, "proportion of samples to use in an iteration: " + QString::number(d_parameters.proportionOfSamplesToUseInAnIteration));

    emit appendLogItem(LogListModel::PARAMETER, "number of runs to perform: " + QString::number(d_parameters.numberOfRunsToPerform));

    if (d_parameters.numberOfRunsToPerform > 1) {

        emit appendLogItem(LogListModel::PARAMETER, "correlation threshold for clustering independent components from multiple runs : " + QString::number(d_parameters.independentComponentClusteringCorrelationThreshold));

        emit appendLogItem(LogListModel::PARAMETER, "credibility index threshold: " + QString::number(d_parameters.credibilityIndexThreshold));

        emit appendLogItem(LogListModel::PARAMETER, "distance correlation threshold: " + QString::number(d_parameters.distanceCorrelationThreshold));

    }

    if (d_parameters.whiteningMode == COVARIANCE)
        emit appendLogItem(LogListModel::PARAMETER, "whitening mode: covariance");
    else if (d_parameters.whiteningMode == DISTANCECOVARIANCE)
        emit appendLogItem(LogListModel::PARAMETER, "whitening mode: distance covariance");

    if (!d_parameters.outputDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.outputDirectory + "\"");

    if (d_parameters.numberOfRunsToPerform != 1)
        emit appendLogItem(LogListModel::PARAMETER, "multithread mode: \"" + d_parameters.multiThreadMode + "\"");

    // Fetching data
    ////////////////

    emit startProgress(0, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    QVector<QVector<double> > matrix;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        matrix << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    // Centering data
    ////////////////

    emit startProgress(0, "Centering data", 0, matrix.size());

    QFutureWatcher<void> futureWatcher;

    this->connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, [=](int progressValue){ emit updateProgress(0, progressValue);});

    futureWatcher.setFuture(QtConcurrent::map(matrix, VectorOperations::centerToMeanOfZero_inplace));

    futureWatcher.waitForFinished();

    emit stopProgress(0);


    // Calculating covariance or distance covariance matrix
    ////////////////

    if (d_parameters.whiteningMode == COVARIANCE)
        emit startProgress(0, "Calculating covariance matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);
    else if (d_parameters.whiteningMode == DISTANCECOVARIANCE)
        emit startProgress(0, "Calculating distance covariance matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);

    double progressValue;

    this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(0, progressValue);}, Qt::DirectConnection);

    d_timerThread.start();

    if (d_parameters.whiteningMode == COVARIANCE) {

        auto dotProduct_scaledBySizeOfVectorsMinusOne = [] (const QVector<double> &vector1, const QVector<double> &vector2) {

            return VectorOperations::dotProduct(vector1, vector2) / static_cast<long double>(vector1.size() - 1.0);

        };

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, dotProduct_scaledBySizeOfVectorsMinusOne, progressValue);

    } else if (d_parameters.whiteningMode == DISTANCECOVARIANCE) {

        auto dCovFunc = [](const QVector<double> &vector1, const QVector<double> &vector2) {return std::sqrt(FastDistanceCorrelation::fastDistanceCovariance(vector1, vector2, false, false).first());};

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(matrix, dCovFunc, progressValue);

    }

    d_timerThread.quit();

    emit stopProgress(0);


    long double totalExplainedVariance = static_cast<long double>(0.0);

    for (int i = 0; i < matrix.size(); ++i)
        totalExplainedVariance += static_cast<long double>(matrix.at(i).at(i));


    if (d_parameters.whiteningMode == COVARIANCE) {

        emit startProgress(0, "Exporting covariance matrix", 0, 0);

        if (!this->processTableResult("Covariance matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
            return;

        emit stopProgress(0);

    } else if (d_parameters.whiteningMode == DISTANCECOVARIANCE) {

        emit startProgress(0, "Exporting distance covariance matrix", 0, 0);

        if (!this->processTableResult("Distance covariance matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
            return;

        emit stopProgress(0);

    }

    // Performing householder transformation of covariance matrix
    ////////////////

    emit startProgress(0, "Performing Householder transformation", 0, (1.0 / 6.0) * (7.0 * double(matrix.size()) * double(matrix.size()) * double(matrix.size()) - 918.0 * double(matrix.size()) * double(matrix.size()) + 93605.0 * double(matrix.size()) - 182388.0));

    QVector<double> d;

    QVector<double> e;

    this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(0, progressValue);}, Qt::DirectConnection);

    d_timerThread.start();

    MatrixOperations::tred2_multiThreaded(matrix, d, e, progressValue);

    d_timerThread.quit();

    emit stopProgress(0);


    // Calculating eigenvalues and eigenvector of real tridiagonal matrix
    ////////////////

    emit startProgress(0, "Calculating eigenvalues and eigenvectors of real tridiagonal matrix", 0, 0);

    QVector<double> eigenvalues;

    int info = MatrixOperations::eigenvaluesAndEigenVectorsOfRealTridiagonalMatrix(d, e, d_parameters.thresholdMaximumNumberOfComponents, eigenvalues, matrix);

    if (info < 0)
        emit appendLogItem(LogListModel::ERROR, "DSTEGR: " + QString::number(info) + "th parameter had illegal value");
    else if (info == 0)
        emit appendLogItem(LogListModel::MESSAGE, "DSTEGR: Algorithm succeeded");
    else if (info == 1)
        emit appendLogItem(LogListModel::ERROR, "DSTEGR: Algorithm failed to converge");
    else if (info == 2)
        emit appendLogItem(LogListModel::ERROR, "DSTEGR: Inverse iteration failed to converge");

    emit stopProgress(0);

    QVector<QVector<QVariant> > eigenvalueSummary;

    long double sumExplainedVariance = static_cast<long double>(0.0);

    QStringList componentLabels;

    int count = 0;

    int numberOfValidComponents = 0;

    for (const double &eigenvalue : eigenvalues) {

        long double explainedVariance = static_cast<long double>(std::fabs(static_cast<long double>(eigenvalue))) / totalExplainedVariance;

        sumExplainedVariance += explainedVariance;

        eigenvalueSummary << QVector<QVariant> {eigenvalue, static_cast<double>(explainedVariance * static_cast<long double>(100.0)), static_cast<double>(sumExplainedVariance * static_cast<long double>(100.0))};

        if ((eigenvalue > std::numeric_limits<double>::epsilon()) && ((sumExplainedVariance * static_cast<long double>(100.0)) <= d_parameters.thresholdCumulativeExplainedVariance))
            ++numberOfValidComponents;

        componentLabels << "Component " + QString::number(++count);

    }

    if (numberOfValidComponents == 0) {

        emit appendLogItem(LogListModel::ERROR, "the number of components remaining after whitening the data is to low to start ICA");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Exporting eigenvalue summary", 0, 0);

    if (!this->processTableResult("Eigenvalue summary", {"Eigenvalue", "Explained variance", "Cumulative explained variance"}, componentLabels, eigenvalueSummary, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);


    emit startProgress(0, "Exporting eigenvectors", 0, 0);

    if (!this->processTableResult("Eigenvectors", componentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, matrix, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);


    // Removing eigenvectors that do not meet thresholds of eigenvalue > DBL_EPSILON AND cumulative explained variance below user defined threshold
    ////////////////

    emit startProgress(0, "Filtering eigenvectors", 0, matrix.size());

    eigenvalues.resize(numberOfValidComponents);

    for (int i = componentLabels.size(); i > numberOfValidComponents; --i)
        componentLabels.removeLast();

    for (int i = 0; i < matrix.size(); ++i) {

        matrix[i].resize(numberOfValidComponents);

        emit updateProgress(0, i + 1);

    }

    emit appendLogItem(LogListModel::MESSAGE, QString::number(numberOfValidComponents) + " components remained after removing components that did not meet the threshold of (eigenvalue > " + QString::number(DBL_EPSILON) + " AND cumulative explained variance < " + QString::number(static_cast<double>(d_parameters.thresholdCumulativeExplainedVariance)) + "%)");

    emit stopProgress(0);

    // Whitening data
    ////////////////

    emit startProgress(0, "Whitening data", 0, 0);

    QVector<QVector<double> > dewhiteningMatrix = MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(matrix, VectorOperations::applyFunctor_elementWise(eigenvalues, [](const double &value){ return std::sqrt(value);}), [](const double &x, const double &y) {return x * y;});

    QVector<QVector<double> > whiteningMatrix(std::move(MatrixOperations::transpose_inplace_multiThreaded(MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded_horizontal(matrix, VectorOperations::applyFunctor_elementWise(eigenvalues, [](const double &value){ return std::pow(value, -0.5);}), [](double &x, const double &y) {x *= y;}))));

    matrix.clear();

    emit startProgress(1, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        matrix << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        emit updateProgress(1, i + 1);

    }

    emit stopProgress(1);

    emit startProgress(1, "Centering data", 0, matrix.size());

    futureWatcher.disconnect();

    this->connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, [=](int progressValue){ emit updateProgress(1, progressValue);});

    futureWatcher.setFuture(QtConcurrent::map(matrix, VectorOperations::centerToMeanOfZero_inplace));

    futureWatcher.waitForFinished();

    emit stopProgress(1);

    emit startProgress(1, "Multiplying whitening matrix with scaled data", 0, 0);

    QVector<QVector<double> > whitenedMatrix = MatrixOperations::matrix1_x_matrix2_multiThreaded(whiteningMatrix, matrix);

    emit stopProgress(1);

    emit stopProgress(0);

    emit startProgress(0, "Exporting whitening matrix", 0, 0);

    if (!this->processTableResult("whitening matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, componentLabels, whiteningMatrix, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);


    emit startProgress(0, "Exporting dewhitening matrix", 0, 0);

    if (!this->processTableResult("Dewhitening matrix", componentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, dewhiteningMatrix, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);

    emit startProgress(0, "Exporting whitened matrix", 0, 0);

    if (!this->processTableResult("Whitened matrix", d_parameters.selectedItemIdentifiersAndIndexes.first, componentLabels, whitenedMatrix, d_parameters.outputDirectory))
        return;

    emit stopProgress(0);

    int contrastFunction;

    QString contrastFunctionString = d_parameters.contrastFunction;

    if (contrastFunctionString == "Raise to power 3")
        contrastFunction = FICA_NONLIN_POW3;
    else if (contrastFunctionString == "Hyperbolic tangent")
        contrastFunction = FICA_NONLIN_TANH;
    else if (contrastFunctionString == "Gaussian")
        contrastFunction = FICA_NONLIN_GAUSS;
    else if (contrastFunctionString == "Skewness")
        contrastFunction = FICA_NONLIN_SKEW;
    else
        contrastFunction = FICA_NONLIN_GAUSS;

    QVector<QVector<double> > independentComponentsForAllRuns;

    if (d_parameters.numberOfRunsToPerform == 1) {

        emit startProgress(0, "Peforming ICA", 0, 0);

        QVector<QVector<double> > mixingMatrix;

        QVector<QVector<double> > seperatingMatrix;

        bool succesfullConvergence = false;

        while (!succesfullConvergence)
            succesfullConvergence = this->fastICA_multiThreaded(whitenedMatrix, whiteningMatrix, dewhiteningMatrix, mixingMatrix, seperatingMatrix, contrastFunction, d_parameters.maximumNumberOfIterations, d_parameters.finetuneMode, d_parameters.stabilizationMode, d_parameters.mu, d_parameters.epsilon, d_parameters.proportionOfSamplesToUseInAnIteration);

        if (!this->processTableResult("ICA - Mixing matrix", componentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, mixingMatrix, d_parameters.outputDirectory))
            return;

        if (!this->processTableResult("ICA - Seperating matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, componentLabels, seperatingMatrix, d_parameters.outputDirectory))
            return;

        // Fetching data
        ////////////////

        emit startProgress(1, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

        matrix.clear();

        QVector<QVector<double> > matrix;

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

            matrix << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

            emit updateProgress(1, i + 1);

        }

        emit stopProgress(1);

        emit startProgress(1, "Calculating independent components", 0, 0);

        MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(seperatingMatrix, matrix);

        MatrixOperations::transpose_inplace_multiThreaded(seperatingMatrix);

        emit stopProgress(1);

        if (!this->processTableResult("ICA - Independent components", componentLabels, d_parameters.selectedItemIdentifiersAndIndexes.first, seperatingMatrix, d_parameters.outputDirectory))
            return;

        emit stopProgress(0);

    } else {

        // Fetching data
        ////////////////

        emit startProgress(0, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

        matrix.clear();

        QVector<QVector<double> > matrix;

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

            matrix << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

            emit updateProgress(0, i + 1);

        }

        emit stopProgress(0);

        emit startProgress(0, "Peforming ICA runs", 0, d_parameters.numberOfRunsToPerform);

        if (d_parameters.multiThreadMode == "Perform runs simultaneously, each within a single thread") {

            QThread *workerThread = QThread::currentThread();

            QMutex mutex;

            double progressValue = 0;

            auto performICARuns = [&](int blockNumber, const QPair<int, int> &block) {

                QVector<QVector<double> > _independentComponentsForAllRuns;

                for (int i = block.first; i < block.second; ++i) {

                    QVector<QVector<double> > mixingMatrix;

                    QVector<QVector<double> > seperatingMatrix;

                    bool succesfullConvergence = false;

                    while (!succesfullConvergence) {

                        succesfullConvergence = this->fastICA(workerThread, blockNumber + 1, whitenedMatrix, whiteningMatrix, dewhiteningMatrix, mixingMatrix, seperatingMatrix, contrastFunction, d_parameters.maximumNumberOfIterations, d_parameters.finetuneMode, d_parameters.stabilizationMode, d_parameters.mu, d_parameters.epsilon, d_parameters.proportionOfSamplesToUseInAnIteration);

                        if (workerThread->isInterruptionRequested())
                            return _independentComponentsForAllRuns;

                    }

                    this->processTableResult("ICA run " + QString::number(i + 1) + " - Mixing matrix", componentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, mixingMatrix, d_parameters.outputDirectory);

                    this->processTableResult("ICA run " + QString::number(i + 1) + " - Seperating matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, componentLabels, seperatingMatrix, d_parameters.outputDirectory);

                    emit startProgress(blockNumber + 1, "Calculating independent components", 0, 0);

                    MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(seperatingMatrix, matrix);

                    _independentComponentsForAllRuns << seperatingMatrix;

                    MatrixOperations::transpose_inplace_multiThreaded(seperatingMatrix);

                    this->processTableResult("ICA run " + QString::number(i + 1) + " - Independent components", componentLabels, d_parameters.selectedItemIdentifiersAndIndexes.first, seperatingMatrix, d_parameters.outputDirectory);

                    emit stopProgress(blockNumber + 1);

                    mutex.lock();

                    progressValue++;

                    emit updateProgress(0, progressValue);

                    mutex.unlock();

                }

                return _independentComponentsForAllRuns;

            };

            QFutureSynchronizer<QVector<QVector<double> > > futureSynchronizer;

            int blockNumber = 1;

            for (const QPair<int, int> &block : FunctionsForMultiThreading::createIndexBlocksForMultiThreading(d_parameters.numberOfRunsToPerform, QThreadPool::globalInstance()->maxThreadCount())) {

                futureSynchronizer.addFuture(QtConcurrent::run(performICARuns, blockNumber, block));

                ++blockNumber;

            }

            futureSynchronizer.waitForFinished();

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

            for (const QFuture<QVector<QVector<double> > > &future : futureSynchronizer.futures())
                independentComponentsForAllRuns << future.result();

        } else {

            for (int i = 0; i < d_parameters.numberOfRunsToPerform; ++i) {

                QVector<QVector<double> > mixingMatrix;

                QVector<QVector<double> > seperatingMatrix;

                bool succesfullConvergence = false;

                while (!succesfullConvergence) {

                    succesfullConvergence = this->fastICA_multiThreaded(whitenedMatrix, whiteningMatrix, dewhiteningMatrix, mixingMatrix, seperatingMatrix, contrastFunction, d_parameters.maximumNumberOfIterations, d_parameters.finetuneMode, d_parameters.stabilizationMode, d_parameters.mu, d_parameters.epsilon, d_parameters.proportionOfSamplesToUseInAnIteration);

                    if (QThread::currentThread()->isInterruptionRequested()) {

                        QThread::currentThread()->quit();

                        return;

                    }

                }

                if (!this->processTableResult("ICA run " + QString::number(i + 1) + " - Mixing matrix", componentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, mixingMatrix, d_parameters.outputDirectory))
                    return;

                if (!this->processTableResult("ICA run " + QString::number(i + 1) + " - Seperating matrix", d_parameters.selectedVariableIdentifiersAndIndexes.first, componentLabels, seperatingMatrix, d_parameters.outputDirectory))
                    return;

                emit startProgress(1, "Calculating independent components", 0, 0);

                MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(seperatingMatrix, matrix);

                independentComponentsForAllRuns << seperatingMatrix;

                MatrixOperations::transpose_inplace_multiThreaded(seperatingMatrix);

                emit stopProgress(1);

                if (!this->processTableResult("ICA run " + QString::number(i + 1) + " - Independent components", componentLabels, d_parameters.selectedItemIdentifiersAndIndexes.first, seperatingMatrix, d_parameters.outputDirectory))
                    return;

                if (QThread::currentThread()->isInterruptionRequested()) {

                    QThread::currentThread()->quit();

                    return;

                }

                emit updateProgress(0, i + 1);

            }

        }

        emit stopProgress(0);

    }

    if (d_parameters.numberOfRunsToPerform > 1) {

        emit startProgress(0, "Clustering independent components", 0, 0);

        QVector<QVector<double> > independentComponentsConnectionMatrix = independentComponentsForAllRuns;

        // Centering data
        ////////////////

        emit startProgress(1, "Centering independent components", 0, independentComponentsConnectionMatrix.size());

        futureWatcher.disconnect();

        this->connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, [=](int progressValue){ emit updateProgress(1, progressValue);});

        futureWatcher.setFuture(QtConcurrent::map(independentComponentsConnectionMatrix, VectorOperations::centerToMeanOfZero_inplace));

        futureWatcher.waitForFinished();

        emit stopProgress(1);


        // Scaling data
        ////////////////

        emit startProgress(1, "Scaling independent components with l2norm", 0, independentComponentsConnectionMatrix.size());

        futureWatcher.disconnect();

        this->connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, [=](int progressValue){ emit updateProgress(1, progressValue);});

        futureWatcher.setFuture(QtConcurrent::map(independentComponentsConnectionMatrix, VectorOperations::normalizeVector_inplace));

        futureWatcher.waitForFinished();

        emit stopProgress(1);


        // Calculating connection matrix
        ////////////////

        emit startProgress(1, "Calculating independent components connection matrix", 0, (double(independentComponentsConnectionMatrix.size()) * double(independentComponentsConnectionMatrix.size()) - double(independentComponentsConnectionMatrix.size())) / 2.0);

        double progressValue = 0.0;

        this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(1, progressValue);}, Qt::DirectConnection);

        d_timerThread.start();

        std::function<double(const QVector<double> &, const QVector<double> &)> thresholdingFunction = [&](const QVector<double> &vector1, const QVector<double> &vector2) {

            double correlation = static_cast<double>(VectorOperations::dotProduct(vector1, vector2));

            if ((std::abs(correlation) >= d_parameters.independentComponentClusteringCorrelationThreshold))
                return std::abs(correlation) / correlation;
            else
                return 0.0;

        };

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(independentComponentsConnectionMatrix, thresholdingFunction, progressValue);

        d_timerThread.quit();

        emit stopProgress(1);

        // Calculating average independent component per cluster
        ////////////////

        emit startProgress(1, "Calculating consensus independent components", 0, 0);

        QList<int> indexesOfIndependentComponentsConnectionMatrix;

        for (int i = 0; i < independentComponentsConnectionMatrix.size(); ++i)
            indexesOfIndependentComponentsConnectionMatrix << i;

        QStringList consensusIndependentComponentLabels;

        QVector<QVector<double> > consensusIndependentComponentPerCluster;

        QVector<QVector<QVariant> > credibilityIndexSummary;

        while (!indexesOfIndependentComponentsConnectionMatrix.isEmpty()) {

            double currentMaximumNumberOfConnections = 0;

            int currentIndexWithMaximumNumberOfConnections = -1;

            for (const int &rowIndex : indexesOfIndependentComponentsConnectionMatrix) {

                int currentNumberOfConnections = 0;

                const QVector<double> &currentRowVectorOfIndependentComponentsConnectionMatrix(independentComponentsConnectionMatrix.at(rowIndex));

                for (const int &columnIndex : indexesOfIndependentComponentsConnectionMatrix) {

                    const double &currentValue(currentRowVectorOfIndependentComponentsConnectionMatrix.at(columnIndex));

                    if ((std::fabs(currentValue - 1.0) <= std::numeric_limits<double>::epsilon()) || (std::fabs(currentValue + 1.0) <= std::numeric_limits<double>::epsilon()))
                        ++currentNumberOfConnections;

                }

                if (currentNumberOfConnections > currentMaximumNumberOfConnections) {

                    currentMaximumNumberOfConnections = currentNumberOfConnections;

                    currentIndexWithMaximumNumberOfConnections = rowIndex;

                }

            }

            if ((currentMaximumNumberOfConnections / static_cast<double>(d_parameters.numberOfRunsToPerform)) < d_parameters.credibilityIndexThreshold)
                break;

            QVector<double> consensusIndependentComponent(d_parameters.selectedItemIdentifiersAndIndexes.second.size(), 0.0);

            const QVector<double> &currentRowVectorOfIndependentComponentsConnectionMatrix(independentComponentsConnectionMatrix.at(currentIndexWithMaximumNumberOfConnections));

            for (int i = indexesOfIndependentComponentsConnectionMatrix.size() - 1; i >=0; --i) {

                const double &currentValue(currentRowVectorOfIndependentComponentsConnectionMatrix.at(indexesOfIndependentComponentsConnectionMatrix.at(i)));

                if ((std::fabs(currentValue - 1.0) <= std::numeric_limits<double>::epsilon()) || (std::fabs(currentValue + 1.0) <= std::numeric_limits<double>::epsilon())) {

                    QVector<double> temp = independentComponentsForAllRuns.at(indexesOfIndependentComponentsConnectionMatrix.at(i));

                    VectorOperations::applyFunctor_elementWise_inplace(consensusIndependentComponent, temp, [&](double &value1, const double &value2){value1 += (currentValue * value2);});

                    indexesOfIndependentComponentsConnectionMatrix.removeAt(i);

                }

            }

            consensusIndependentComponentPerCluster << VectorOperations::applyFunctor_elementWise_inplace(consensusIndependentComponent, [&](double &value){value /= static_cast<double>(currentMaximumNumberOfConnections);});

            consensusIndependentComponentLabels << "Consensus IC " + QString::number(consensusIndependentComponentLabels.size() + 1);

            credibilityIndexSummary << QVector<QVariant>{currentMaximumNumberOfConnections, (currentMaximumNumberOfConnections / static_cast<double>(d_parameters.numberOfRunsToPerform))};

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

        }

        emit stopProgress(1);

        emit startProgress(1, "Exporting credibility summary", 0, 0);

        if (!this->processTableResult("Credibility summary", {"Number of clustered independent components", "Credibility index"}, consensusIndependentComponentLabels, credibilityIndexSummary, d_parameters.outputDirectory))
            return;

        emit stopProgress(1);

        emit startProgress(1, "Exporting consensus independent components", 0, 0);

        MatrixOperations::transpose_inplace_multiThreaded(consensusIndependentComponentPerCluster);

        if (!this->processTableResult("Consensus independent components", consensusIndependentComponentLabels, d_parameters.selectedItemIdentifiersAndIndexes.first, consensusIndependentComponentPerCluster, d_parameters.outputDirectory))
            return;

        emit stopProgress(1);

        emit stopProgress(0);

        emit startProgress(0, "Calculating correlations between consensus independent components", 0, 0);

        QVector<QVector<double> > correlationMatrixOfConsensusIndependentComponent = MatrixOperations::transpose_multiThreaded(consensusIndependentComponentPerCluster);

        emit startProgress(1, "Calculating distance correlation matrix", 0, (double(matrix.size()) * double(matrix.size()) - double(matrix.size())) / 2.0);

        this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(1, progressValue);}, Qt::DirectConnection);

        d_timerThread.start();

        auto dCorFunc = [](const QVector<double> &vector1, const QVector<double> &vector2) {return FastDistanceCorrelation::fastDistanceCorrelation(vector1, vector2, false);};

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(correlationMatrixOfConsensusIndependentComponent, dCorFunc, progressValue);

        d_timerThread.quit();

        emit stopProgress(1);

        emit startProgress(1, "Exporting correlations between consensus independent components", 0, 0);

        if (!this->processTableResult("Distance correlations between consensus independent component", consensusIndependentComponentLabels, consensusIndependentComponentLabels, correlationMatrixOfConsensusIndependentComponent, d_parameters.outputDirectory))
            return;

        emit stopProgress(1);

        emit startProgress(1, "Detecting dependency between consensus independent components", 0, 0);

        QVector<QVector<QVariant> > summaryOfFilteringResultsBasedOnIndependenceAndCI;

        QSet<QString> uniqueLabelsWinners;

        QList<int> indexesToEvaluate;

        for (int i = 1; i < consensusIndependentComponentLabels.size(); ++i)
            indexesToEvaluate << i;

        for (int i = 0; i < consensusIndependentComponentLabels.size(); ++i) {

            const QVector<double> &currentVector(correlationMatrixOfConsensusIndependentComponent.at(i));

            QStringList labels = {consensusIndependentComponentLabels.at(i)};

            QVector<double> distanceCorrelations = {currentVector.at(i)};

            QVector<double> credibilityIndexes = {credibilityIndexSummary.at(i).at(1).toDouble()};

            QVector<int> originalIndexes = {i};

            QString dummy;

            dummy += "[" + consensusIndependentComponentLabels.at(i) + "; " + QString::number(currentVector.at(i)) + "; " + credibilityIndexSummary.at(i).at(1).toString() + "]";

            for (int j = 0; j < consensusIndependentComponentLabels.size(); ++j) {

                if ((i != j ) && (currentVector.at(j) >= d_parameters.distanceCorrelationThreshold)) {

                    labels << consensusIndependentComponentLabels.at(j);

                    distanceCorrelations << currentVector.at(j);

                    credibilityIndexes << credibilityIndexSummary.at(j).at(1).toDouble();

                    dummy += ", [" + consensusIndependentComponentLabels.at(j) + "; " + QString::number(currentVector.at(j)) + "; " + credibilityIndexSummary.at(j).at(1).toString() + "]";

                    originalIndexes << j;

                }

            }

            QString labelOfWinner = labels.first();

            double credibilityIndexOfWinner = credibilityIndexes.first();

            int indexOfWinner = originalIndexes.first();

            for (int j = 1; j < labels.size(); ++j) {

                if (credibilityIndexes.at(j) > credibilityIndexOfWinner) {

                    credibilityIndexOfWinner = credibilityIndexes.at(j);

                    labelOfWinner = labels.at(j);

                    indexOfWinner = originalIndexes.at(j);


                } else if (std::fabs(credibilityIndexes.at(j) - credibilityIndexOfWinner) <= std::numeric_limits<double>::epsilon()) {

                    if (originalIndexes.at(j) < indexOfWinner) {

                        credibilityIndexOfWinner = credibilityIndexes.at(j);

                        labelOfWinner = labels.at(j);

                        indexOfWinner = originalIndexes.at(j);

                    }

                }

            }

            QVector<QVariant> temp;

            temp << consensusIndependentComponentLabels.at(i) << labelOfWinner << credibilityIndexOfWinner << dummy;

            summaryOfFilteringResultsBasedOnIndependenceAndCI << temp;

            uniqueLabelsWinners.insert(labelOfWinner);

        }

        if (!this->processTableResult("Filtering results based on independency and CI", {"IC label", "IC label of winner", "CI of winner", "Summary [label, dCor, CI"}, consensusIndependentComponentLabels, summaryOfFilteringResultsBasedOnIndependenceAndCI, d_parameters.outputDirectory))
            return;

        emit stopProgress(1);


        QVector<QVector<QVariant> > uniqueWinners;

        QStringList rowLabels;

        for (const QString &winner: uniqueLabelsWinners) {

            QVector<QVariant> temp = {winner};

            uniqueWinners << temp;

            rowLabels << winner;

        }

        if (!this->processTableResult("Final set of sources", {"IC label"}, rowLabels, uniqueWinners, d_parameters.outputDirectory))
            return;

        emit stopProgress(0);

        emit startProgress(0, "Calculating consensus mixing matrix", 0, 0);

        MatrixOperations::transpose_inplace_multiThreaded(consensusIndependentComponentPerCluster);

        QVector<QVector<double> > consensusIndependentComponentScalarProductMatrix = consensusIndependentComponentPerCluster;

        // Calculating consensus independent components scalar product matrix
        ////////////////

        emit startProgress(1, "Calculating consensus independent components scalar product matrix", 0, (double(consensusIndependentComponentScalarProductMatrix.size()) * double(consensusIndependentComponentScalarProductMatrix.size()) - double(consensusIndependentComponentScalarProductMatrix.size())) / 2.0);

        progressValue = 0.0;

        this->connect(d_timer, &QTimer::timeout, this, [=, &progressValue](){ emit updateProgress(1, progressValue);}, Qt::DirectConnection);

        d_timerThread.start();

        MatrixOperations::createSymmetricMatrix_inplace_multiThreaded(consensusIndependentComponentScalarProductMatrix, VectorOperations::dotProduct, progressValue);

        d_timerThread.quit();

        emit stopProgress(1);

        // Calculating inverse of consensus independent components scalar product matrix
        ////////////////

        emit startProgress(1, "Calculating inverse of consensus independent components scalar product matrix", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

        MatrixOperations::mPower_inplace(consensusIndependentComponentScalarProductMatrix, -1.0);

        emit stopProgress(1);

        // Fetching data
        ////////////////

        emit startProgress(1, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

        matrix.clear();

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

            matrix << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

            emit updateProgress(1, i + 1);

        }

        emit stopProgress(1);

        emit startProgress(1, "Matrix multiplication", 0, 0);

        MatrixOperations::transpose_inplace_multiThreaded(matrix);

        MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(consensusIndependentComponentScalarProductMatrix, consensusIndependentComponentPerCluster);

        MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(consensusIndependentComponentScalarProductMatrix, matrix);


        emit stopProgress(1);

        emit startProgress(1, "Exporting consensus mixing matrix", 0, 0);

        MatrixOperations::transpose_inplace_multiThreaded(consensusIndependentComponentScalarProductMatrix);

        if (!this->processTableResult("Consensus mixing matrix", consensusIndependentComponentLabels, d_parameters.selectedVariableIdentifiersAndIndexes.first, consensusIndependentComponentScalarProductMatrix, d_parameters.outputDirectory))
            return;

        emit stopProgress(1);

        emit stopProgress(0);

    }

    QThread::currentThread()->quit();

}

bool IndependentComponentAnalysisWorkerClass::fastICA_multiThreaded(const QVector<QVector<double> > &whitenedMatrix, const QVector<QVector<double> > &whiteningMatrix, const QVector<QVector<double> > &dewhiteningMatrix, QVector<QVector<double> > &mixingMatrix, QVector<QVector<double> > &seperatingMatrix, int contrastFunction, int maximumNumberOfIterations, bool finetuneMode, bool stabilizationMode, double mu, const double &epsilon, double proportionOfSamplesToUseInAnIteration)
{

    int dataCount = whitenedMatrix.isEmpty() ? 0 : whitenedMatrix.at(0).size();

    int gOrig = contrastFunction;

    int gFine = contrastFunction + 1;

    double myyOrig = mu;

    double myyK = 0.01;

    int usedNlinearity = 0;

    double stroke = 0.0;

    int notFine = 1;

    int loong = 0;

    double a1 = 1.0;

    double a2 = 1.0;

    int initialStateMode = FICA_INIT_RAND;

    double minAbsCos = 0.0;

    double minAbsCos2 = 0.0;

    if (proportionOfSamplesToUseInAnIteration * dataCount < 1000)
        proportionOfSamplesToUseInAnIteration = (1000.0 / static_cast<double>(dataCount) < 1.0) ? 1000.0 / static_cast<double>(dataCount) : 1.0;

    if (proportionOfSamplesToUseInAnIteration != 1.0)
        gOrig += 2;

    if ((mu != 1.0) || (stabilizationMode))
        gOrig += 1;

    int fineTuningEnabled = 1;

    if (!finetuneMode) {

        if ((mu != 1.0) || (stabilizationMode))
            gFine = gOrig;
        else
            gFine = gOrig + 1;

        fineTuningEnabled = 0;

    }

    int stabilizationEnabled = stabilizationMode;

    if (!stabilizationMode && mu != 1.0)
        stabilizationEnabled = true;

    usedNlinearity = gOrig;

    QVector<QVector<double> > guess((whiteningMatrix.isEmpty() ? 0 : whiteningMatrix.at(0).size()), QVector<double>(whitenedMatrix.size()));

    MatrixOperations::fillMatrixWithRandomNumbersFromUniformRealDistribution(guess, -0.5, 0.5);

    QVector<QVector<double> > B(whitenedMatrix.size(), QVector<double>(whitenedMatrix.size()));

    if (initialStateMode == 0) {

        emit startProgress(1, "Initializing", 0, 0);

        MatrixOperations::fillMatrixWithRandomNumbersFromUniformRealDistribution(B, -0.5, 0.5);

        MatrixOperations::orthogonalizeMatrix_inplace(B);

        emit stopProgress(1);

    } else
        B = MatrixOperations::matrix1_x_matrix2(whiteningMatrix, guess);

    QVector<QVector<double> > BOld(B.size(), QVector<double>(B.isEmpty() ? 0 : B.at(0).size(), 0.0));

    QVector<QVector<double> > BOld2(B.size(), QVector<double>(B.isEmpty() ? 0 : B.at(0).size(), 0.0));

    QDateTime start = QDateTime::currentDateTime();

    for (int round = 0; round < maximumNumberOfIterations; round++) {

        if (round == 0) {

            if (stabilizationEnabled)
                emit startProgress(1, QString("Iteration ") + QString::number(round + 1) + " - mu: " + QString::number(mu), 0, 0);
            else
                emit startProgress(1, QString("Iteration ") + QString::number(round + 1), 0, 0);

        } else {

            if (stabilizationEnabled)
                emit startProgress(1, QString("Iteration ") + QString::number(round + 1) + " - mu: " + QString::number(mu) + " - convergence change: " + QString::number(1.0 - minAbsCos) + " - average time per iteration: " + this->secondsToTimeString(static_cast<int>(start.secsTo(QDateTime::currentDateTime()) / static_cast<long long>(round))), 0, 0);
            else
                emit startProgress(1, QString("Iteration ") + QString::number(round + 1) + " - convergence change: " + QString::number(1.0 - minAbsCos) + " - average time per iteration: " + this->secondsToTimeString(static_cast<int>(start.secsTo(QDateTime::currentDateTime()) / static_cast<long long>(round))), 0, 0);

        }

        if (round == maximumNumberOfIterations - 1) {

            emit appendLogItem(LogListModel::WARNING, "no convergence reached");

            mixingMatrix = MatrixOperations::matrix1_x_matrix2(dewhiteningMatrix, B);

            MatrixOperations::transpose_inplace(B);

            seperatingMatrix = MatrixOperations::matrix1_x_matrix2(B, whiteningMatrix);

            emit stopProgress(1);

            return false;

        }

        QVector<QVector<double> > BTransposed = MatrixOperations::transpose(B);

        MatrixOperations::matrix1_x_matrix2_inplace(B, MatrixOperations::mPower_inplace(MatrixOperations::createSymmetricMatrix_inplace(BTransposed, VectorOperations::dotProduct), -.5));

        BTransposed = MatrixOperations::transpose(B);

        minAbsCos = MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquareMatrix(MatrixOperations::matrix1_x_matrix2(BTransposed, BOld));

        minAbsCos2 = MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquareMatrix(MatrixOperations::matrix1_x_matrix2(BTransposed, BOld2));

        if (std::fabs(1.0 - minAbsCos) < epsilon) {

            if (fineTuningEnabled && notFine) {

                notFine = 0;

                usedNlinearity = gFine;

                mu = myyK * myyOrig;

                BOld = QVector<QVector<double> >(B.size(), QVector<double>(B.isEmpty() ? 0 : B[0].size(), 0.0));

                BOld2 = QVector<QVector<double> >(B.size(), QVector<double>(B.isEmpty() ? 0 : B[0].size(), 0.0));

            } else {

                emit appendLogItem(LogListModel::MESSAGE, "convergence after " + QString::number(round) + " iterations");

                mixingMatrix = MatrixOperations::matrix1_x_matrix2(dewhiteningMatrix, B);

                emit stopProgress(1);

                break;

            }

        } else if (stabilizationEnabled) {

            if (!static_cast<bool>(stroke) && (std::fabs(1.0 - minAbsCos2) < epsilon)) {

                emit appendLogItem(LogListModel::WARNING, "stroke! Reducing step size by a factor of two");

                stroke = mu;

                mu /= double(2);

                if ((usedNlinearity % 2) == 0)
                    usedNlinearity += 1;

            } else if (static_cast<bool>(stroke)) {

                mu = stroke;

                stroke = 0;

                if ((std::fabs(mu - 1.0) < std::numeric_limits<double>::epsilon()) && ((usedNlinearity % 2) != 0))
                    usedNlinearity -= 1;

            } else if (!loong && (round > maximumNumberOfIterations / 2)) {

                emit appendLogItem(LogListModel::WARNING, "convergence is taking to long, reducing step size by a factor of two");

                loong = 1;

                mu /= double(2);

                if ((usedNlinearity % 2) == 0)
                    usedNlinearity += 1;

            }

            if (d_parameters.searchTimeConstant != -1)
                mu = myyOrig / (1.0 + static_cast<double>(round) / static_cast<double>(d_parameters.searchTimeConstant));

        }

        BOld2 = BOld;

        BOld = B;

        switch (usedNlinearity) {
            case FICA_NONLIN_POW3 : {

                B = MatrixOperations::applyFunctor_elementWise_multiThreaded(MatrixOperations::matrix1_x_matrix2_multiThreaded(whitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(MatrixOperations::transpose_multiThreaded(whitenedMatrix), B, [](const double &value) {return std::pow(value, 3.0);})), B, [&](const double &value1, const double &value2){return (value1 / static_cast<double>(dataCount)) - (3.0 * value2);});

                break;

            }

            case(FICA_NONLIN_POW3 + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(whitenedMatrix), B);

                QVector<QVector<double> > Gpow3 = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [](const double &value){return std::pow(value, 3.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [](const double &value){return std::pow(value, 4.0);}));

                MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(MatrixOperations::transpose_inplace_multiThreaded(Y), Gpow3);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded([&](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise_multiThreaded(Beta, [&](const double &value){ return std::pow(value - (3.0 * dataCount), -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_POW3 + 2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                B = MatrixOperations::applyFunctor_elementWise_multiThreaded(MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(subWhitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B, [](const double &value) {return std::pow(value, 3.0);}), [&](const double &value) {return value / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());}), B, [](const double &value1, const double &value2){return value1 - (3.0 * value2);});

                break;

            }

            case(FICA_NONLIN_POW3 + 3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B);

                QVector<QVector<double> > Gpow3 = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [](const double &value){return std::pow(value, 3.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [](const double &value){return std::pow(value, 4.0);}));

                MatrixOperations::matrix1_x_matrix2_inplace_multiThreaded(MatrixOperations::transpose_inplace_multiThreaded(Y), Gpow3);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded([&](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise_multiThreaded(Beta, [&](const double &value){ return std::pow(value - (3.0 * dataCount), -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case FICA_NONLIN_TANH : {

                QVector<QVector<double> > hypTan = MatrixOperations::matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise_multiThreaded([&](const double &value){return a1 * value;}, MatrixOperations::transpose_multiThreaded(whitenedMatrix), B, [](const double &value){return std::tanh(value);});

                B = MatrixOperations::applyFunctor_elementWise_multiThreaded(MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(whitenedMatrix, hypTan, [&](const double &value){return value / static_cast<double>(dataCount);}), MatrixOperations::applyFunctor_elementWise_multiThreaded_vertical(B, MathDescriptives::sum_vertical_applyFunctorElementWise_multiThreaded(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);}), [&](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(dataCount) * a1;}), [](const double &value1, const double &value2){return value1 - value2;});

                break;

            }

            case(FICA_NONLIN_TANH + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(whitenedMatrix), B);

                QVector<QVector<double> > hypTan = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [&a1](const double &value){return std::tanh(a1 * value);});

                QVector<double> Beta1 = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, hypTan, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise_multiThreaded(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);});

                MatrixOperations::matrix1_x_matrix2_inplace(MatrixOperations::transpose_inplace_multiThreaded(Y), hypTan);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [&a1](const double &value1, const double &value2){ return std::pow(value1 - (a1 * value2), -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_TANH + 2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > hypTan = MatrixOperations::matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise_multiThreaded([&](const double &value){return a1 * value;}, MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B, [](const double &value){return std::tanh(value);});

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(whitenedMatrix, hypTan, [&](const double &value){ return value / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());});

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_vertical(B, MathDescriptives::sum_vertical_applyFunctorElementWise_multiThreaded(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);}), [&](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size()) * a1;}), [](double &value1, const double &value2){ value1 -= value2;});

                break;

            }

            case(FICA_NONLIN_TANH + 3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B);

                QVector<QVector<double> > hypTan = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [&](const double &value){return std::tanh(a1 * value);});

                QVector<double> Beta1 = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, hypTan, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise_multiThreaded(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);});

                Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(Y), hypTan);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded([&](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [&](const double &value1, const double &value2){ return std::pow(value1 - (a1 * value2), -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case FICA_NONLIN_GAUSS : {

                QVector<QVector<double> > U = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(whitenedMatrix), B);

                QVector<QVector<double> > Usquared = MatrixOperations::applyFunctor_elementWise_multiThreaded(U, [](const double &value){return std::pow(value, 2.0);});

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise_multiThreaded(Usquared, [&](const double &value){return std::exp(-a2 * value / 2.0);});

                QVector<QVector<double> > dGauss = MatrixOperations::applyFunctor_elementWise_multiThreaded(Usquared, ex, [&](const double &value1, const double &value2){return 1.0 - (a2 * value1 * value2);});

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(whitenedMatrix, MatrixOperations::applyFunctor_elementWise_multiThreaded(U, ex, [](const double &value1, const double &value2){return value1 * value2;}), [&](const double &value){ return value / static_cast<double>(dataCount);});

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_vertical(B, MathDescriptives::sum_vertical_multiThreaded(dGauss), [&](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(dataCount);}), [](double &value1, const double &value2){ value1 -= value2;});

                break;

            }

            case(FICA_NONLIN_GAUSS + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(whitenedMatrix), B);

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [&](const double &value){return std::exp(-a2 * std::pow(value, 2.0) / 2.0);});

                QVector<QVector<double> > gauss = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, ex, [](const double &value1, const double &value2){return value1 * value2;});

                QVector<double> Beta1 = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, gauss, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise_multiThreaded(Y, ex, [&](const double &value1, const double &value2){return (1.0 - a2 * std::pow(value1, 2.0)) * value2;});

                Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(Y), gauss);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                QVector<QVector<double> > temp = MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded([&](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [&](const double &value1, const double &value2){ return std::pow(value1 - value2, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;});

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, temp, [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_GAUSS + 2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > U = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B);

                QVector<QVector<double> > Usquared = MatrixOperations::applyFunctor_elementWise_multiThreaded(U, [](const double &value){return std::pow(value, 2.0);});

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise_multiThreaded(Usquared, [&](const double &value){return std::exp(-a2 * value / 2.0);});

                QVector<QVector<double> > dGauss = MatrixOperations::applyFunctor_elementWise_multiThreaded(Usquared, ex, [&](const double &value1, const double &value2){return (1.0 - (a2 * value1)) * value2;});

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(subWhitenedMatrix, MatrixOperations::applyFunctor_elementWise_multiThreaded(U, ex, [](const double &value1, const double &value2){return value1 * value2;}), [&](const double &value){ return value / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());});

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_vertical(B, MathDescriptives::sum_vertical_multiThreaded(dGauss), [&](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());}), [](double &value1, const double &value2){ value1 -= value2;});

                break;

            }

            case(FICA_NONLIN_GAUSS + 3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B);

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [&](const double &value){return std::exp(-a2 * std::pow(value, 2.0) / 2.0);});

                QVector<QVector<double> > gauss = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, ex, [](const double &value1, const double &value2){return value1 * value2;});

                QVector<double> Beta1 = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, gauss, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise_multiThreaded(Y, ex, [&](const double &value1, const double &value2){return (1.0 - a2 * std::pow(value1, 2.0)) * value2;});

                Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(Y), gauss);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded([&](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [&](const double &value1, const double &value2){ return std::pow(value1 - value2, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case FICA_NONLIN_SKEW : {

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(whitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(MatrixOperations::transpose_multiThreaded(whitenedMatrix), B, [](const double &value) {return std::pow(value, 2.0);}), [&](const double &value) {return value / static_cast<double>(dataCount);});

                break;

            }

            case(FICA_NONLIN_SKEW + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(whitenedMatrix), B);

                QVector<QVector<double> > Gskew = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [](const double &value){return std::pow(value, 2.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, Gskew, [](const double &value1, const double &value2){return value1 * value2;}));

                Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(Y), Gskew);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded([&](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta, [&](const double &value){ return std::pow(value, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_SKEW+2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(subWhitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise_multiThreaded(MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B, [](const double &value) {return std::pow(value, 2.0);}), [&](const double &value) {return value / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());});

                break;

            }

            case(FICA_NONLIN_SKEW+3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(subWhitenedMatrix), B);

                QVector<QVector<double> > Gskew = MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, [](const double &value){return std::pow(value, 2.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical_multiThreaded(MatrixOperations::applyFunctor_elementWise_multiThreaded(Y, Gskew, [](const double &value1, const double &value2){return value1 * value2;}));

                Y = MatrixOperations::matrix1_x_matrix2_multiThreaded(MatrixOperations::transpose_multiThreaded(Y), Gskew);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace_multiThreaded(B, MatrixOperations::applyFunctor_elementWise_multiThreaded_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise_multiThreaded([&](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta, [&](const double &value){ return std::pow(value, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

        }

        emit stopProgress(1);

        if (QThread::currentThread()->isInterruptionRequested())
            return false;

    }

    seperatingMatrix = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(B), whiteningMatrix);

    return true;

}

bool IndependentComponentAnalysisWorkerClass::fastICA(QThread *workerThread, int progressIdentifier, const QVector<QVector<double> > &whitenedMatrix, const QVector<QVector<double> > &whiteningMatrix, const QVector<QVector<double> > &dewhiteningMatrix, QVector<QVector<double> > &mixingMatrix, QVector<QVector<double> > &seperatingMatrix, int contrastFunction, int maximumNumberOfIterations, bool finetuneMode, bool stabilizationMode, double mu, const double &epsilon, double proportionOfSamplesToUseInAnIteration)
{

    int dataCount = whitenedMatrix.isEmpty() ? 0 : whitenedMatrix.at(0).size();

    int gOrig = contrastFunction;

    int gFine = contrastFunction + 1;

    double myyOrig = mu;

    double myyK = 0.01;

    int usedNlinearity = 0;

    double stroke = 0.0;

    int notFine = 1;

    int loong = 0;

    double a1 = 1.0;

    double a2 = 1.0;

    int initialStateMode = FICA_INIT_RAND;

    double minAbsCos = 0.0;

    double minAbsCos2 = 0.0;

    if (proportionOfSamplesToUseInAnIteration * dataCount < 1000)
        proportionOfSamplesToUseInAnIteration = (1000.0 / static_cast<double>(dataCount) < 1.0) ? 1000.0 / static_cast<double>(dataCount) : 1.0;

    if (proportionOfSamplesToUseInAnIteration != 1.0)
        gOrig += 2;

    if ((mu != 1.0) || (stabilizationMode))
        gOrig += 1;

    int fineTuningEnabled = 1;

    if (!finetuneMode) {

        if ((mu != 1.0) || (stabilizationMode))
            gFine = gOrig;
        else
            gFine = gOrig + 1;

        fineTuningEnabled = 0;

    }

    int stabilizationEnabled = stabilizationMode;

    if (!stabilizationMode && mu != 1.0)
        stabilizationEnabled = true;

    usedNlinearity = gOrig;

    QVector<QVector<double> > guess((whiteningMatrix.isEmpty() ? 0 : whiteningMatrix.at(0).size()), QVector<double>(whitenedMatrix.size()));

    MatrixOperations::fillMatrixWithRandomNumbersFromUniformRealDistribution(guess, -0.5, 0.5);

    QVector<QVector<double> > B(whitenedMatrix.size(), QVector<double>(whitenedMatrix.size()));

    if (initialStateMode == 0) {

        emit startProgress(progressIdentifier, "Initializing", 0, 0);

        MatrixOperations::fillMatrixWithRandomNumbersFromUniformRealDistribution(B, -0.5, 0.5);

        MatrixOperations::orthogonalizeMatrix_inplace(B);

        emit stopProgress(progressIdentifier);

    } else
        B = MatrixOperations::matrix1_x_matrix2(whiteningMatrix, guess);

    QVector<QVector<double> > BOld(B.size(), QVector<double>(B.isEmpty() ? 0 : B.at(0).size(), 0.0));

    QVector<QVector<double> > BOld2(B.size(), QVector<double>(B.isEmpty() ? 0 : B.at(0).size(), 0.0));

    QDateTime start = QDateTime::currentDateTime();

    QTextStream out(stdout);

    for (int round = 0; round < maximumNumberOfIterations; round++) {

        if (round == 0) {

            if (stabilizationEnabled)
                emit startProgress(progressIdentifier, QString("Iteration ") + QString::number(round + 1) + " - mu: " + QString::number(mu), 0, 0);
            else
                emit startProgress(progressIdentifier, QString("Iteration ") + QString::number(round + 1), 0, 0);

        } else {

            if (stabilizationEnabled)
                emit startProgress(progressIdentifier, QString("Iteration ") + QString::number(round + 1) + " - mu: " + QString::number(mu) + " - convergence change: " + QString::number(1.0 - minAbsCos) + " - average time per iteration: " + this->secondsToTimeString(static_cast<int>(start.secsTo(QDateTime::currentDateTime()) / static_cast<long long>(round))), 0, 0);
            else
                emit startProgress(progressIdentifier, QString("Iteration ") + QString::number(round + 1) + " - convergence change: " + QString::number(1.0 - minAbsCos) + " - average time per iteration: " + this->secondsToTimeString(static_cast<int>(start.secsTo(QDateTime::currentDateTime()) / static_cast<long long>(round))), 0, 0);

        }

        if (round == maximumNumberOfIterations - 1) {

            emit appendLogItem(LogListModel::WARNING, "no convergence reached");

            mixingMatrix = MatrixOperations::matrix1_x_matrix2(dewhiteningMatrix, B);

            MatrixOperations::transpose_inplace(B);

            seperatingMatrix = MatrixOperations::matrix1_x_matrix2(B, whiteningMatrix);

            emit stopProgress(progressIdentifier);

            return false;

        }

        QVector<QVector<double> > BTransposed = MatrixOperations::transpose(B);

        MatrixOperations::matrix1_x_matrix2_inplace(B, MatrixOperations::mPower_inplace(MatrixOperations::createSymmetricMatrix_inplace(BTransposed, VectorOperations::dotProduct), -.5));

        BTransposed = MatrixOperations::transpose(B);

        minAbsCos = MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquareMatrix(MatrixOperations::matrix1_x_matrix2(BTransposed, BOld));

        minAbsCos2 = MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquareMatrix(MatrixOperations::matrix1_x_matrix2(BTransposed, BOld2));

        if ((1.0 - minAbsCos) < epsilon) {

            if (fineTuningEnabled && notFine) {

                notFine = 0;

                usedNlinearity = gFine;

                mu = myyK * myyOrig;

                BOld = QVector<QVector<double> >(B.size(), QVector<double>(B.isEmpty() ? 0 : B[0].size(), 0.0));

                BOld2 = QVector<QVector<double> >(B.size(), QVector<double>(B.isEmpty() ? 0 : B[0].size(), 0.0));

            } else {

                emit appendLogItem(LogListModel::MESSAGE, "convergence after " + QString::number(round) + " iterations");

                mixingMatrix = MatrixOperations::matrix1_x_matrix2(dewhiteningMatrix, B);

                emit stopProgress(progressIdentifier);

                break;

            }

        } else if (stabilizationEnabled) {

            if (!static_cast<bool>(stroke) && (std::fabs(1.0 - minAbsCos2) < epsilon)) {

                emit appendLogItem(LogListModel::WARNING, "stroke! Reducing step size by a factor of two");

                stroke = mu;

                mu /= double(2);

                if ((usedNlinearity % 2) == 0)
                    usedNlinearity += 1;

            } else if (static_cast<bool>(stroke)) {

                mu = stroke;

                stroke = 0;

                if ((std::fabs(mu - 1.0) < epsilon) && ((usedNlinearity % 2) != 0))
                    usedNlinearity -= 1;

            } else if (!loong && (round > maximumNumberOfIterations / 2)) {

                emit appendLogItem(LogListModel::WARNING, "convergence is taking to long, reducing step size by a factor of two");

                loong = 1;

                mu /= double(2);

                if ((usedNlinearity % 2) == 0)
                    usedNlinearity += 1;

            }

            if (d_parameters.searchTimeConstant != -1)
                mu = myyOrig / (1.0 + static_cast<double>(round) / static_cast<double>(d_parameters.searchTimeConstant));

        }

        BOld2 = BOld;

        BOld = B;

        switch (usedNlinearity) {

            case FICA_NONLIN_POW3 : {

                B = MatrixOperations::applyFunctor_elementWise(MatrixOperations::matrix1_x_matrix2(whitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(MatrixOperations::transpose(whitenedMatrix), B, [](const double &value) {return std::pow(value, 3.0);})), B, [&dataCount](const double &value1, const double &value2){return (value1 / static_cast<double>(dataCount)) - (3.0 * value2);});

                break;

            }

            case(FICA_NONLIN_POW3 + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(whitenedMatrix), B);

                QVector<QVector<double> > Gpow3 = MatrixOperations::applyFunctor_elementWise(Y, [](const double &value){return std::pow(value, 3.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, [](const double &value){return std::pow(value, 4.0);}));

                MatrixOperations::matrix1_x_matrix2_inplace(MatrixOperations::transpose_inplace(Y), Gpow3);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta, [&dataCount](const double &value){ return std::pow(value - (3.0 * dataCount), -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_POW3 + 2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                B = MatrixOperations::applyFunctor_elementWise(MatrixOperations::matrix1_x_matrix2(subWhitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(MatrixOperations::transpose(subWhitenedMatrix), B, [](const double &value) {return std::pow(value, 3.0);})), B, [&subWhitenedMatrix](const double &value1, const double &value2){return (value1 / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size())) - (3.0 * value2);});

                break;

            }

            case(FICA_NONLIN_POW3 + 3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(subWhitenedMatrix), B);

                QVector<QVector<double> > Gpow3 = MatrixOperations::applyFunctor_elementWise(Y, [](const double &value){return std::pow(value, 3.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, [](const double &value){return std::pow(value, 4.0);}));

                QVector<double> D = VectorOperations::applyFunctor_elementWise(Beta, [&dataCount](const double &value){ return std::pow(value - (3.0 * dataCount), -1.0);});

                MatrixOperations::matrix1_x_matrix2_inplace(MatrixOperations::transpose_inplace(Y), Gpow3);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), D, [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case FICA_NONLIN_TANH : {

                QVector<QVector<double> > hypTan = MatrixOperations::matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise([&a1](const double &value){return a1 * value;}, MatrixOperations::transpose(whitenedMatrix), B, [](const double &value){return std::tanh(value);});

                B = MatrixOperations::applyFunctor_elementWise(MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(whitenedMatrix, hypTan, [&dataCount](const double &value){ return value / static_cast<double>(dataCount);}), MatrixOperations::applyFunctor_elementWise_vertical(B, MathDescriptives::sum_vertical_applyFunctorElementWise(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);}), [&dataCount, &a1](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(dataCount) * a1;}), [](const double &value1, const double &value2){return value1 - value2;});

                break;

            }

            case(FICA_NONLIN_TANH + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(whitenedMatrix), B);

                QVector<QVector<double> > hypTan = MatrixOperations::applyFunctor_elementWise(Y, [&a1](const double &value){return std::tanh(a1 * value);});

                QVector<double> Beta1 = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, hypTan, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);});

                MatrixOperations::matrix1_x_matrix2_inplace(MatrixOperations::transpose_inplace(Y), hypTan);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [&a1](const double &value1, const double &value2){ return std::pow(value1 - (a1 * value2), -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_TANH + 2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > hypTan = MatrixOperations::matrix1_x_matrix2_applyFunctor1ToMatrix1ElementWise_applyFunctor2OnResultingMatrixElementWise([&a1](const double &value){return a1 * value;}, MatrixOperations::transpose(subWhitenedMatrix), B, [](const double &value){return std::tanh(value);});

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(whitenedMatrix, hypTan, [&subWhitenedMatrix](const double &value){ return value / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());});

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_vertical(B, MathDescriptives::sum_vertical_applyFunctorElementWise(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);}), [&subWhitenedMatrix, &a1](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size()) * a1;}), [](double &value1, const double &value2){ value1 -= value2;});

                break;

            }

            case(FICA_NONLIN_TANH + 3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(subWhitenedMatrix), B);

                QVector<QVector<double> > hypTan = MatrixOperations::applyFunctor_elementWise(Y, [&a1](const double &value){return std::tanh(a1 * value);});

                QVector<double> Beta1 = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, hypTan, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise(hypTan, [](const double &value){return 1.0 - std::pow(value, 2.0);});

                Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(Y), hypTan);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [&a1](const double &value1, const double &value2){ return std::pow(value1 - (a1 * value2), -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case FICA_NONLIN_GAUSS : {

                QVector<QVector<double> > U = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(whitenedMatrix), B);

                QVector<QVector<double> > Usquared = MatrixOperations::applyFunctor_elementWise(U, [](const double &value){return std::pow(value, 2.0);});

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise(Usquared, [&a2](const double &value){return std::exp(-a2 * value / 2.0);});

                QVector<QVector<double> > dGauss = MatrixOperations::applyFunctor_elementWise(Usquared, ex, [&a2](const double &value1, const double &value2){return (1.0 - a2 * value1) * value2;});

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(whitenedMatrix, MatrixOperations::applyFunctor_elementWise(U, ex, [](const double &value1, const double &value2){return value1 * value2;}), [&dataCount](const double &value){ return value / static_cast<double>(dataCount);});

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_vertical(B, MathDescriptives::sum_vertical(dGauss), [&dataCount](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(dataCount);}), [](double &value1, const double &value2){ value1 -= value2;});

                break;

            }

            case(FICA_NONLIN_GAUSS + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(whitenedMatrix), B);

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise(Y, [&a2](const double &value){return std::exp(-a2 * std::pow(value, 2.0) / 2.0);});

                QVector<QVector<double> > gauss = MatrixOperations::applyFunctor_elementWise(Y, ex, [](const double &value1, const double &value2){return value1 * value2;});

                QVector<double> Beta1 = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, gauss, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise(Y, ex, [&a2](const double &value1, const double &value2){return (1.0 - a2 * std::pow(value1, 2.0)) * value2;});

                Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(Y), gauss);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [](const double &value1, const double &value2){ return std::pow(value1 - value2, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_GAUSS + 2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > U = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(subWhitenedMatrix), B);

                QVector<QVector<double> > Usquared = MatrixOperations::applyFunctor_elementWise(U, [](const double &value){return std::pow(value, 2.0);});

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise(Usquared, [&a2](const double &value){return std::exp(-a2 * value / 2.0);});

                QVector<QVector<double> > dGauss = MatrixOperations::applyFunctor_elementWise(Usquared, ex, [&a2](const double &value1, const double &value2){return (1.0 - (a2 * value1)) * value2;});

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(subWhitenedMatrix, MatrixOperations::applyFunctor_elementWise(U, ex, [](const double &value1, const double &value2){return value1 * value2;}), [&subWhitenedMatrix](const double &value){ return value / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());});

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_vertical(B, MathDescriptives::sum_vertical(dGauss), [&subWhitenedMatrix](const double &value1, const double &value2){ return (value1 * value2) / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());}), [](double &value1, const double &value2){ value1 -= value2;});

                break;

            }

            case(FICA_NONLIN_GAUSS + 3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(subWhitenedMatrix), B);

                QVector<QVector<double> > ex = MatrixOperations::applyFunctor_elementWise(Y, [&a2](const double &value){return std::exp(-a2 * std::pow(value, 2.0) / 2.0);});

                QVector<QVector<double> > gauss = MatrixOperations::applyFunctor_elementWise(Y, ex, [](const double &value1, const double &value2){return value1 * value2;});

                QVector<double> Beta1 = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, gauss, [](const double &value1, const double &value2){return value1 * value2;}));

                QVector<double> Beta2 = MathDescriptives::sum_vertical_applyFunctorElementWise(Y, ex, [&a2](const double &value1, const double &value2){return (1.0 - a2 * std::pow(value1, 2.0)) * value2;});

                Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(Y), gauss);

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i][i] -= Beta1.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta1, Beta2, [](const double &value1, const double &value2){ return std::pow(value1 - value2, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case FICA_NONLIN_SKEW : {

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(whitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(MatrixOperations::transpose(whitenedMatrix), B, [](const double &value) {return std::pow(value, 2.0);}), [&dataCount](const double &value) {return value / static_cast<double>(dataCount);});

                break;

            }

            case(FICA_NONLIN_SKEW + 1) : {

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(whitenedMatrix), B);

                QVector<QVector<double> > Gskew = MatrixOperations::applyFunctor_elementWise(Y, [](const double &value){return std::pow(value, 2.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, Gskew, [](const double &value1, const double &value2){return value1 * value2;}));

                Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(Y), Gskew);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta, [](const double &value){ return std::pow(value, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

            case(FICA_NONLIN_SKEW+2) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                B = MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(subWhitenedMatrix, MatrixOperations::matrix1_x_matrix2_applyFunctorOnResultingMatrixElementWise(MatrixOperations::transpose(subWhitenedMatrix), B, [](const double &value) {return std::pow(value, 2.0);}), [&subWhitenedMatrix](const double &value) {return value / static_cast<double>(subWhitenedMatrix.isEmpty() ? 0 : subWhitenedMatrix.first().size());});

                break;

            }

            case(FICA_NONLIN_SKEW+3) : {

                std::random_device rd;

                std::mt19937 g(rd());

                std::shuffle(d_indexesForSubSampling.begin(), d_indexesForSubSampling.end(), g);

                QVector<QVector<double> > subWhitenedMatrix = MatrixOperations::subMatrixBasedOnColumnIndexes(whitenedMatrix, d_indexesForSubSampling.mid(0, static_cast<int>(std::floor(static_cast<double>(dataCount) * proportionOfSamplesToUseInAnIteration))));

                QVector<QVector<double> > Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(subWhitenedMatrix), B);

                QVector<QVector<double> > Gskew = MatrixOperations::applyFunctor_elementWise(Y, [](const double &value){return std::pow(value, 2.0);});

                QVector<double> Beta = MathDescriptives::sum_vertical(MatrixOperations::applyFunctor_elementWise(Y, Gskew, [](const double &value1, const double &value2){return value1 * value2;}));

                Y = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(Y), Gskew);

                for (int i = 0; i < Beta.size(); ++i)
                    Y[i][i] -= Beta.at(i);

                MatrixOperations::applyFunctor_elementWise_inplace(B, MatrixOperations::applyFunctor_elementWise_horizontal(MatrixOperations::matrix1_x_matrix2_applyFunctorToMatrix1ElementWise([&mu](const double &value){return mu * value;}, B, Y), VectorOperations::applyFunctor_elementWise(Beta, [](const double &value){ return std::pow(value, -1.0);}), [](const double &value1, const double &value2){return value1 * value2;}), [](double &value1, const double &value2){value1 += value2;});

                break;

            }

        }

        emit stopProgress(progressIdentifier);

        if (workerThread->isInterruptionRequested())
            return false;

    }

    seperatingMatrix = MatrixOperations::matrix1_x_matrix2(MatrixOperations::transpose(B), whiteningMatrix);

    return true;

}

QString IndependentComponentAnalysisWorkerClass::secondsToTimeString(int s)
{
    int days = s / 86400;

    int seconds = s - days * 86400;

    int hours = seconds / 3600;

    seconds -= hours * 3600;

    int minutes = seconds / 60;

    seconds -= minutes * 60;

    QString str;

    if (days)
        str += QString("%1d ").arg(days, 1);

    if (hours >= 1)
        str += QString("%1h ").arg(hours, 2);

    if (minutes >= 1)
        str += QString("%1m ").arg(minutes, 2);

    if (seconds >= 1)
        str += QString("%1s ").arg(seconds, 2);

    if (str.isEmpty())
        str = "< 1s ";

    return str;

}
