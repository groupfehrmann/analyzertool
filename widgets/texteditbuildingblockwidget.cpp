#include "texteditbuildingblockwidget.h"
#include "ui_texteditbuildingblockwidget.h"

TextEditBuildingBlockWidget::TextEditBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, const QString &defaultText) :
    QWidget(parent), ui(new Ui::TextEditBuildingBlockWidget), d_rememberSettings(false)
{

    ui->setupUi(this);

    ui->label->setText(label);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    if (defaultText.isEmpty())
        return;
    else
        d_rememberSettings = true;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/textEditBuildingBlockWidget");

    if (settings.contains("text"))
        ui->textEdit->setText(settings.value("text").toString());

    settings.endGroup();

}

TextEditBuildingBlockWidget::~TextEditBuildingBlockWidget()
{

    if (d_rememberSettings) {

        QSettings settings;

        settings.beginGroup(d_parentIdentifierForSettings + "/textEditBuildingBlockWidget");

        if (ui->textEdit->acceptRichText())
            settings.setValue("text", ui->textEdit->toHtml());
        else
            settings.setValue("text", ui->textEdit->toPlainText());

        settings.endGroup();

    }

    delete ui;

}

QTextEdit *TextEditBuildingBlockWidget::pointerToInternalTextEdit()
{

    return ui->textEdit;

}

QLabel *TextEditBuildingBlockWidget::pointerToInternalLabel()
{

    return ui->label;

}
