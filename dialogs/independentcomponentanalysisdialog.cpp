#include "independentcomponentanalysisdialog.h"

IndependentComponentAnalysisDialog::IndependentComponentAnalysisDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new IndependentComponentAnalysisWorkerClass::Parameters;

    this->setupWidgets();

}

IndependentComponentAnalysisDialog::~IndependentComponentAnalysisDialog()
{

   delete static_cast<IndependentComponentAnalysisWorkerClass::Parameters *>(d_parameters);

}

void IndependentComponentAnalysisDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 1, 1);

    d_comboBoxBuildingBlockWidget_whiteningMode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_whiteningMode", "Select whitening mode", {"Covariance", "Distance covariance"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_whiteningMode, 2, 0);

    d_comboBoxBuildingBlockWidget_contrastFunction = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_contrastFunction", "Select contrast function", {"Raise to power 3", "Hyperbolic tangent", "Gaussian", "Skewness"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_contrastFunction, 2, 1);

    d_doubleSpinBoxBuildingBlockWidget_thresholdCumulativeExplainedVariance = new DoubleSpinBoxBuildingBlockWidget(this, "doubleSpinBoxBuildingBlockWidget_thresholdCumulativeExplainedVariance", "select cumulative explained variance threshold that determines the maximum number of components to enter ICA", true, 4, 0, 100, 100, 0.0001, QString(), "%");

    d_gridLayout->addWidget(d_doubleSpinBoxBuildingBlockWidget_thresholdCumulativeExplainedVariance, 3, 0);

    d_spinBoxBuildingBlockWidget_thresholdMaximumNumberOfComponents = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_thresholdMaximumNumberOfComponents", "Select maximum number of components to enter ICA", true, 10, 1, 100, 1, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_thresholdMaximumNumberOfComponents, 3, 1);

    this->connect(d_itemsSelectorBuildingBlockWidget_variables, SIGNAL(selectionChanged(QStringList,QStringList)), this, SLOT(selectionOfVariableIdentifiersAndIndexesChanged(QStringList,QStringList)));

    d_spinBoxBuildingBlockWidget_maximumIterations = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_maximumIterations", "Select maximum number of iterations", true, 10, 100, 100000, 1000, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_maximumIterations, 4, 0);

    d_comboBoxBuildingBlockWidget_finetuneMode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_finetuneMode", "Finetune mode", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_finetuneMode, 5, 0);

    d_comboBoxBuildingBlockWidget_stabilizationMode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_stabilizationMode", "Stabilization mode", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_stabilizationMode, 6, 0);

    d_doubleSpinBoxBuildingBlockWidget_mu = new DoubleSpinBoxBuildingBlockWidget(this, "doubleSpinBoxBuildingBlockWidget_mu", "Select step size (mu) in stabilization mode", true, 2, 0.01, 1.0, 0.75, 0.01);

    d_gridLayout->addWidget(d_doubleSpinBoxBuildingBlockWidget_mu, 7, 0);

    d_spinBoxBuildingBlockWidget_searchTimeConstant = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_searchTimeConstant", "Select search time constant in stabilization mode (-1 is disabled)", true, 10, -1, 1000, -1, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_searchTimeConstant, 8, 0);

    d_doubleSpinBoxBuildingBlockWidget_epsilon = new DoubleSpinBoxBuildingBlockWidget(this, "doubleSpinBoxBuildingBlockWidget_epsilon", "Select stopping criteria (epsilon)", true, 10, 0.00001, 1.0, 0.00001, 0.00001);

    d_gridLayout->addWidget(d_doubleSpinBoxBuildingBlockWidget_epsilon, 9, 0);

    d_doubleSpinBoxBuildingBlockWidget_proportionOfSamplesToUseInAnIteration = new DoubleSpinBoxBuildingBlockWidget(this, "doubleSpinBoxBuildingBlockWidget_proportionOfSamplesToUseInAnIteration", "Select proportion of samples to use in a interation", true, 2, 0.01, 1.0, 1.0, 0.01);

    d_gridLayout->addWidget(d_doubleSpinBoxBuildingBlockWidget_proportionOfSamplesToUseInAnIteration, 10, 0);

    d_spinBoxBuildingBlockWidget_numberOfRunsToPerform = new SpinBoxBuildingBlockWidget(this, "spinBoxBuildingBlockWidget_numberOfRunsToPerform", "Select number of runs to perform", true, 10, 1, 1000, 25, 1);

    d_gridLayout->addWidget(d_spinBoxBuildingBlockWidget_numberOfRunsToPerform, 11, 0);

    d_doubleSpinBoxBuildingBlockWidget_independentComponentClusteringCorrelationThreshold = new DoubleSpinBoxBuildingBlockWidget(this, "doubleSpinBoxBuildingBlockWidget_independentComponentClusteringCorrelationThreshold", "Select correlation threshold used to cluster independent components from multiple runs", true, 2, 0.01, 1.0, 0.9, 0.01);

    d_gridLayout->addWidget(d_doubleSpinBoxBuildingBlockWidget_independentComponentClusteringCorrelationThreshold, 12, 0);

    d_doubleSpinBoxBuildingBlockWidget_credibilityIndexThreshold = new DoubleSpinBoxBuildingBlockWidget(this, "doubleSpinBoxBuildingBlockWidget_credibilityIndexThreshold", "Select credibility index threshold for valid clusters", true, 2, 0.00, 1.0, 0.5, 0.01);

    d_gridLayout->addWidget(d_doubleSpinBoxBuildingBlockWidget_credibilityIndexThreshold, 13, 0);

    d_doubleSpinBoxBuildingBlockWidget_distanceCorrelationThreshold = new DoubleSpinBoxBuildingBlockWidget(this, "d_doubleSpinBoxBuildingBlockWidget_distanceCorrelationThreshold", "Select distance correlation threshold for concluding independence", true, 2, 0.00, 1.0, 0.5, 0.01);

    d_gridLayout->addWidget(d_doubleSpinBoxBuildingBlockWidget_distanceCorrelationThreshold, 14, 0);

    d_selectOutputDirectory = new SelectDirectoryBuildingBlockWidget(this, "d_selectOutputDirectory", "Select output directory (optional)", true);

    d_gridLayout->addWidget(d_selectOutputDirectory, 15, 0);

    d_comboBoxBuildingBlockWidget_multiThreadMode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_multiThreadMode", "Multithread mode", {"Perform runs simultaneously, each within a single thread", "Perform runs sequentially with multiple threads"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_multiThreadMode, 16, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->connect(d_spinBoxBuildingBlockWidget_numberOfRunsToPerform->pointerToInternalSpinBox(), SIGNAL(valueChanged(int)), this, SLOT(numberOfRunsChanged(int)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

    this->numberOfRunsChanged(d_spinBoxBuildingBlockWidget_numberOfRunsToPerform->pointerToInternalSpinBox()->value());

    this->selectionOfVariableIdentifiersAndIndexesChanged(d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber().first, QStringList());

}

void IndependentComponentAnalysisDialog::initializeParameters()
{

    IndependentComponentAnalysisWorkerClass::Parameters *parameters = static_cast<IndependentComponentAnalysisWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->independentComponentClusteringCorrelationThreshold = d_doubleSpinBoxBuildingBlockWidget_independentComponentClusteringCorrelationThreshold->pointerToInternalDoubleSpinBox()->value();

    parameters->contrastFunction = d_comboBoxBuildingBlockWidget_contrastFunction->pointerToInternalComboBox()->currentText();

    parameters->credibilityIndexThreshold = d_doubleSpinBoxBuildingBlockWidget_credibilityIndexThreshold->pointerToInternalDoubleSpinBox()->value();

    parameters->distanceCorrelationThreshold = d_doubleSpinBoxBuildingBlockWidget_distanceCorrelationThreshold->pointerToInternalDoubleSpinBox()->value();

    parameters->epsilon = d_doubleSpinBoxBuildingBlockWidget_epsilon->pointerToInternalDoubleSpinBox()->value();

    parameters->finetuneMode = (d_comboBoxBuildingBlockWidget_finetuneMode->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->maximumNumberOfIterations = d_spinBoxBuildingBlockWidget_maximumIterations->pointerToInternalSpinBox()->value();

    parameters->mu = d_doubleSpinBoxBuildingBlockWidget_mu->pointerToInternalDoubleSpinBox()->value();

    parameters->multiThreadMode = d_comboBoxBuildingBlockWidget_multiThreadMode->pointerToInternalComboBox()->currentText();

    parameters->numberOfRunsToPerform = d_spinBoxBuildingBlockWidget_numberOfRunsToPerform->pointerToInternalSpinBox()->value();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->proportionOfSamplesToUseInAnIteration = d_doubleSpinBoxBuildingBlockWidget_proportionOfSamplesToUseInAnIteration->pointerToInternalDoubleSpinBox()->value();

    parameters->outputDirectory = d_selectOutputDirectory->pointerToInternalLineEdit()->text();

    parameters->searchTimeConstant = d_spinBoxBuildingBlockWidget_searchTimeConstant->pointerToInternalSpinBox()->value();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->stabilizationMode = (d_comboBoxBuildingBlockWidget_stabilizationMode->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->thresholdCumulativeExplainedVariance = d_doubleSpinBoxBuildingBlockWidget_thresholdCumulativeExplainedVariance->pointerToInternalDoubleSpinBox()->value();

    parameters->thresholdMaximumNumberOfComponents = d_spinBoxBuildingBlockWidget_thresholdMaximumNumberOfComponents->pointerToInternalSpinBox()->value();

    QString currentModeInStringFormat = d_comboBoxBuildingBlockWidget_whiteningMode->pointerToInternalComboBox()->currentText();

    if (currentModeInStringFormat == "Covariance")
        parameters->whiteningMode = IndependentComponentAnalysisWorkerClass::COVARIANCE;
    else if (currentModeInStringFormat == "Distance covariance")
        parameters->whiteningMode = IndependentComponentAnalysisWorkerClass::DISTANCECOVARIANCE;

}

void IndependentComponentAnalysisDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

}

void IndependentComponentAnalysisDialog::numberOfRunsChanged(const int &numberOfRuns)
{

    if (numberOfRuns == 1)
        d_comboBoxBuildingBlockWidget_multiThreadMode->setEnabled(false);
    else
        d_comboBoxBuildingBlockWidget_multiThreadMode->setEnabled(true);

}

void IndependentComponentAnalysisDialog::selectionOfVariableIdentifiersAndIndexesChanged(const QStringList &selectedItemsIdentifier, const QStringList &notSelectedItemsIdentifier)
{

    Q_UNUSED(notSelectedItemsIdentifier)

    d_spinBoxBuildingBlockWidget_thresholdMaximumNumberOfComponents->pointerToInternalSpinBox()->setMaximum(selectedItemsIdentifier.size());

    d_spinBoxBuildingBlockWidget_thresholdMaximumNumberOfComponents->pointerToInternalSpinBox()->setValue(selectedItemsIdentifier.size());

}
