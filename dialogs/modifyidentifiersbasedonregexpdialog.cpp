#include "modifyidentifiersbasedonregexpdialog.h"

ModifyIdentifiersBasedOnRegExpDialog::ModifyIdentifiersBasedOnRegExpDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new ModifyIdentifiersBasedOnRegExpWorkerClass::Parameters;

    this->setupWidgets();

}

ModifyIdentifiersBasedOnRegExpDialog::~ModifyIdentifiersBasedOnRegExpDialog()
{

   delete static_cast<ModifyIdentifiersBasedOnRegExpWorkerClass::Parameters *>(d_parameters);

}

void ModifyIdentifiersBasedOnRegExpDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_identifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_identifiers", "Select identifiers");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_identifiers, 1, 0);

    d_lineEditBuildingBlockWidget_pattern = new LineEditBuildingBlockWidget(this, "lineEditBuildingBlockWidget_pattern", "Regular expression pattern");

    d_gridLayout->addWidget(d_lineEditBuildingBlockWidget_pattern, 2, 0);

    d_comboBoxBuildingBlockWidget_patternSyntax = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_patternSyntax", "Select pattern syntax", {"Regular expression", "Wildcard", "Fixed string"}, {QRegExp::RegExp2, QRegExp::Wildcard, QRegExp::FixedString});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_patternSyntax, 3, 0);

    d_comboBoxBuildingBlockWidget_caseSensitivity = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_caseSensitivity", "Select case sensitivity", {"Case insensitive", "Case sensitive"}, {Qt::CaseInsensitive, Qt::CaseSensitive});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_caseSensitivity, 4, 0);

    d_lineEditBuildingBlockWidget_stringToReplaceWith = new LineEditBuildingBlockWidget(this, "lineEditBuildingBlockWidget_stringToReplaceWith", "String to replace with");

    d_gridLayout->addWidget(d_lineEditBuildingBlockWidget_stringToReplaceWith, 5, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->connect(d_lineEditBuildingBlockWidget_pattern->pointerToInternalLineEdit(), SIGNAL(textChanged(QString)), this, SLOT(lineEditBuildingBlockWidget_pattern_textChanged(QString)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void ModifyIdentifiersBasedOnRegExpDialog::initializeParameters()
{

    ModifyIdentifiersBasedOnRegExpWorkerClass::Parameters *parameters = static_cast<ModifyIdentifiersBasedOnRegExpWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->selectedIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_identifiers->selectedItemsIdentifierAndNumber();

    parameters->regularExpression = QRegExp(d_lineEditBuildingBlockWidget_pattern->pointerToInternalLineEdit()->text(), static_cast<Qt::CaseSensitivity>(d_comboBoxBuildingBlockWidget_caseSensitivity->pointerToInternalComboBox()->currentData().toInt()), static_cast<QRegExp::PatternSyntax>(d_comboBoxBuildingBlockWidget_patternSyntax->pointerToInternalComboBox()->currentData().toInt()));

    parameters->stringToReplaceWith = d_lineEditBuildingBlockWidget_stringToReplaceWith->pointerToInternalLineEdit()->text();

}

void ModifyIdentifiersBasedOnRegExpDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > identifiersAndIndexes = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_identifiers->setItemsIdentifierAndNumber(identifiersAndIndexes.first, identifiersAndIndexes.second);

}

void ModifyIdentifiersBasedOnRegExpDialog::lineEditBuildingBlockWidget_pattern_textChanged(const QString &text)
{

    if (QRegExp(text).isValid())
        d_lineEditBuildingBlockWidget_pattern->pointerToInternalLineEdit()->setStyleSheet(QString());
    else
        d_lineEditBuildingBlockWidget_pattern->pointerToInternalLineEdit()->setStyleSheet(QString("color: rgb(255, 0, 0);"));

}
