#ifndef SELECTMULTIPLEFILESBUILDINGBLOCKWIDGET_H
#define SELECTMULTIPLEFILESBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFile>
#include <QFileDialog>
#include <QPlainTextEdit>

#include "dialogs/basedialog.h"

namespace Ui {

class SelectMultipleFilesBuildingBlockWidget;

}

class SelectMultipleFilesBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectMultipleFilesBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), QFileDialog::AcceptMode = QFileDialog::AcceptOpen, bool rememberSettings = false);

    ~SelectMultipleFilesBuildingBlockWidget();

    QLabel *pointerToInternalLabel();

    QPlainTextEdit *pointerToInternalPlainTextEdit();

    QPushButton *pointerToInternalPushButton();

private:

    Ui::SelectMultipleFilesBuildingBlockWidget *ui;

    QFileDialog::AcceptMode d_acceptMode;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

private slots:

    void pushButton_clicked();

};


#endif // SELECTMULTIPLEFILESBUILDINGBLOCKWIDGET_H
