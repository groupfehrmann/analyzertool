#ifndef GENESETENRICHMENTANALYSISWORKERCLASS_H
#define GENESETENRICHMENTANALYSISWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QVariantList>

#include <tuple>
#include <algorithm>
#include <cmath>

#include "boost/math/distributions/normal.hpp"

#include "base/baseworkerclass.h"
#include "base/basedataset.h"
#include "base/convertfunctions.h"
#include "statistics/studentt.h"
#include "statistics/welcht.h"
#include "statistics/levenef.h"
#include "statistics/mannwhitneyu.h"
#include "statistics/kolmogorovsmirnovz.h"
#include "statistics/kruskalwallischisquared.h"
#include "statistics/pearsonr.h"
#include "statistics/spearmanr.h"
#include "statistics/distancecorrelation.h"
#include "statistics/FisherExactGSEA.h"
#include "math/statfunctions.h"

class GeneSetEnrichmentAnalysisWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QStringList annotationLabelsDefiningSearchableKeys;

        QString annotationLabelUsedToMatch;

        QSharedPointer<BaseDataset> baseDataset;

        QString exportDirectory;

        QStringList geneSetFiles;

        int maximumSizeOfGeneSet;

        int minimumSizeOfGeneSet;

        int numberOfPermutationRounds;

        BaseMatrix::Orientation orientation;

        QString outputFormat;

        bool performPermutations;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

        QString testToDefineEnrichment;

        bool exportGeneSetMemberFiles;

    };

    struct EnrichmentDataElement {

        int geneSetIndex;

        int rankedGenesListIndex;

        int databaseIndex;

        int numberOfGenesInGeneSet;

        double zTransformedPValue;

    };

    GeneSetEnrichmentAnalysisWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

    void updateFileProgress();

private:

    QFile d_file;

    QVector<QStringList> d_databaseToGeneSetIdentifiers;

    QVector<QStringList> d_databaseToGeneSetDescriptions;

    QVector<QVector<QSet<QString> > > d_databaseToGeneSetToGeneSetMembers;

    QStringList d_geneIdentifiers;

    QHash<QString, int> d_geneIdentifierToIndex;

    QStringList d_geneAnnotations;

    QVector<QStringList> d_tokenizedGeneAnnotations;

    GeneSetEnrichmentAnalysisWorkerClass::Parameters d_parameters;

    bool checkInputParameters();

    bool readGeneSetFile(const QString &geneSetFile);

    bool writeGeneSetMembersToFile_tabDelimited(int geneSetIndex, int rankedGenesListIndex, const QSet<QString> &geneSetMembers, const QVector<QPair<int, double> > &sortedRankedGenesListWithOriginalIndex, const QString &path);

    bool writeGeneSetMembersToFile_json(int geneSetIndex, int rankedGenesListIndex, const QSet<QString> &geneSetMembers, const QVector<QPair<int, double> > &sortedRankedGenesListWithOriginalIndex, const QString &path);

    bool writeEnrichmentFromGeneSetPerspectiveToFile_tabDelimited(int geneSetIndex, const QStringList &rankedGenesListIdentifiers, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path);

    bool writeEnrichmentFromGeneSetPerspectiveToFile_json(int geneSetIndex, const QStringList &rankedGenesListIdentifiers, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path);

    bool writeEnrichmentFromRankedGenesListPerspectiveToFile_tabDelimited(int rankedGenesListIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path);

    bool writeEnrichmentFromRankedGenesListPerspectiveToFile_json(int rankedGenesListIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path);

    bool writeGeneSearchableKeysToIndex_tabDelimited(const QString &path);

    bool writeGeneSearchableKeysToIndex_json(const QString &path);

    bool writeGeneRankInfoToFile_tabDelimited(int geneIndex, const QVector<QVector<double> > &transposedRankedGenesLists, const QVector<QVector<int> > &geneIndexToRanks, const QString &path);

    bool writeGeneRankInfoToFile_json(int geneIndex, const QVector<QVector<double> > &transposedRankedGenesLists, const QVector<QVector<int> > &geneIndexToRanks, const QString &path);

    bool writeSortedRankedGenesList_tabDelimited(int rankedGenesListIndex, const QVector<QPair<int, double> > sortedRankedGenesListWithOriginalIndex, const QString &path);

    bool writeSortedRankedGenesList_json(int rankedGenesListIndex, const QVector<QPair<int, double> > sortedRankedGenesListWithOriginalIndex, const QString &path);

    bool writeMatrixWithZTransformedPValues_tabDelimited(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > & enrichmentDataElements, const QString &path, const QStringList &rankedGenesListIdentifiers, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions);

    bool writeMatrixWithZTransformedPValues_json(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > & enrichmentDataElements, const QString &path, const QStringList &rankedGenesListIdentifiers, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions);

    QPair<QVector<double>, QVector<unsigned int> > filterOutNaNs(const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

};

#endif // GENESETENRICHMENTANALYSISWORKERCLASS_H
