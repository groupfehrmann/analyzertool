#include "calculatemodulescoresworkerclass.h"

CalculateModuleScoresWorkerClass::CalculateModuleScoresWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CalculateModuleScoresWorkerClass::Parameters *>(parameters))
{

}

bool CalculateModuleScoresWorkerClass::checkInputParameters()
{

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no variables selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no items selected");

        return false;

    }

    if (QSet<QString>(d_parameters.selectedItemIdentifiersAndIndexes.first.begin(), d_parameters.selectedItemIdentifiersAndIndexes.first.end()).size() != d_parameters.selectedItemIdentifiersAndIndexes.first.size()) {

        emit appendLogItem(LogListModel::ERROR, "duplicate item identifiers are not allowed");

        return false;

    }

    return true;

}

void CalculateModuleScoresWorkerClass::updateFileProgress()
{

    emit updateProgress(1, d_file.pos());

}

bool CalculateModuleScoresWorkerClass::readModules_moduleDefinedByGeneMembershipOnly()
{

    d_file.setFileName(d_parameters.fileDefiningModules);

    if (!d_file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file: \"" + d_parameters.fileDefiningModules + "\"");

        return false;

    }

    QSet<QString> uniqueSetOfItemIdentifiers(d_parameters.selectedItemIdentifiersAndIndexes.first.begin(), d_parameters.selectedItemIdentifiersAndIndexes.first.end());

    emit startProgress(1, "Importing module definitions", 1, d_file.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    int numberOfLinesRead = 0;

    while (!d_file.atEnd()) {

        QStringList tokens = QString(d_file.readLine()).remove(QRegExp("[\r\n]")).split("\t", Qt::SkipEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() < 3) {

            emit appendLogItem(LogListModel::ERROR, "error at line number " + QString::number(numberOfLinesRead) + " in file defining modules \"" + d_parameters.fileDefiningModules + "\", not enough tokens read");

            d_file.close();

            return false;

        }

        QVector<QPair<QString, double> > membersAndWeights;

        QSet<QString> members;

        for (int i = 1; i < tokens.size(); ++i) {

            const QString &token(tokens.at(i));

            if (uniqueSetOfItemIdentifiers.contains(token)) {

                if (!members.contains(token))
                    members.insert(token);
                else
                    emit appendLogItem(LogListModel::WARNING, "check module definition in file \"" + d_parameters.fileDefiningModules + "\" at line " + QString::number(numberOfLinesRead) + ", module with identifier \"" +  tokens.at(0) + "\" conatins duplicate members with identifier \"" + token + "\"");

                membersAndWeights << QPair<QString, double>(token, 1.0);

            } else
                emit appendLogItem(LogListModel::WARNING, "member \"" + token + "\" of module \"" + tokens.at(0) + "\" is not present in data set");

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            d_file.close();

            return false;

        }

        if (!membersAndWeights.isEmpty()) {

            d_moduleIdentifiers << tokens.at(0);

            d_moduleToMembersAndWeights << membersAndWeights;

            emit appendLogItem(LogListModel::MESSAGE, "the score for module \"" + tokens.at(0) + "\" will be calculated with the following mapped members (n=" + QString::number(members.size()) + "): " + ConvertFunctions::toString(members.values()));

        } else
            emit appendLogItem(LogListModel::WARNING, "module \"" + tokens.at(0) + "\" has no members that could be mapped to the data set and there for omitted from further analysis");

    }

    d_file.close();

    emit stopProgress(1);

    emit appendLogItem(LogListModel::MESSAGE, "imported " + QString::number(d_moduleToMembersAndWeights.size()) + " modules from \"" + d_parameters.fileDefiningModules + "\"");

    return true;

}

bool CalculateModuleScoresWorkerClass::readModules_moduleDefinedByGeneMembershipAndWeight()
{

    d_file.setFileName(d_parameters.fileDefiningModules);

    if (!d_file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file: \"" + d_parameters.fileDefiningModules + "\"");

        return false;

    }

    QSet<QString> uniqueSetOfItemIdentifiers(d_parameters.selectedItemIdentifiersAndIndexes.first.begin(), d_parameters.selectedItemIdentifiersAndIndexes.first.end());

    emit startProgress(1, "Importing module definitions", 1, d_file.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    int numberOfLinesRead = 0;

    while (!d_file.atEnd()) {

        QStringList tokens = QString(d_file.readLine()).remove(QRegExp("[\r\n]")).split("\t", Qt::SkipEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() < 3) {

            emit appendLogItem(LogListModel::ERROR, "error at line number " + QString::number(numberOfLinesRead) + " in file defining modules \"" + d_parameters.fileDefiningModules + "\", not enough tokens read");

            d_file.close();

            return false;

        }

        QVector<QPair<QString, double> > membersAndWeights;

        QSet<QString> members;

        int i = 1;

        while ((i + 1) < tokens.size()) {

            const QString &token(tokens.at(i));

            if (uniqueSetOfItemIdentifiers.contains(token)) {

                if (!members.contains(token))
                    members.insert(token);
                else
                    emit appendLogItem(LogListModel::WARNING, "check module definition in file \"" + d_parameters.fileDefiningModules + "\" at line " + QString::number(numberOfLinesRead) + ", module with identifier \"" +  tokens.at(0) + "\" conatins duplicate members with identifier \"" + token + "\"");

                membersAndWeights << QPair<QString, double>(token, tokens.at(i + 1).toDouble());

            } else
                emit appendLogItem(LogListModel::WARNING, "member \"" + token + "\" of module \"" + tokens.at(0) + "\" is not present in data set");

            i = i + 2;

        }

        if (i != tokens.size()) {

            emit appendLogItem(LogListModel::ERROR, "error at line number " + QString::number(numberOfLinesRead) + " in file defining modules \"" + d_parameters.fileDefiningModules + "\", probably mismatch between number of member identifiers and weights");

            d_file.close();

            return false;

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            d_file.close();

            return false;

        }

        if (!membersAndWeights.isEmpty()) {

            d_moduleIdentifiers << tokens.at(0);

            d_moduleToMembersAndWeights << membersAndWeights;

            emit appendLogItem(LogListModel::MESSAGE, "the score for module \"" + tokens.at(0) + "\" will be calculated with the following mapped members (n=" + QString::number(members.size()) + "): " + ConvertFunctions::toString(members.values()));

        } else
            emit appendLogItem(LogListModel::WARNING, "module \"" + tokens.at(0) + "\" has no members that could be mapped to the data set and there for omitted from further analysis");

    }

    d_file.close();

    emit stopProgress(1);

    emit appendLogItem(LogListModel::MESSAGE, "imported " + QString::number(d_moduleToMembersAndWeights.size()) + " modules from \"" + d_parameters.fileDefiningModules + "\"");

    return true;

}

bool CalculateModuleScoresWorkerClass::readModules_moduleDefinedByWeightAndGeneMembership()
{

    d_file.setFileName(d_parameters.fileDefiningModules);

    if (!d_file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file: \"" + d_parameters.fileDefiningModules + "\"");

        return false;

    }

    QSet<QString> uniqueSetOfItemIdentifiers(d_parameters.selectedItemIdentifiersAndIndexes.first.begin(), d_parameters.selectedItemIdentifiersAndIndexes.first.end());

    emit startProgress(1, "Importing module definitions", 1, d_file.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    int numberOfLinesRead = 0;

    while (!d_file.atEnd()) {

        QStringList tokens = QString(d_file.readLine()).remove(QRegExp("[\r\n]")).split("\t", Qt::SkipEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() < 3) {

            emit appendLogItem(LogListModel::ERROR, "error at line number " + QString::number(numberOfLinesRead) + " in file defining modules \"" + d_parameters.fileDefiningModules + "\", not enough tokens read");

            d_file.close();

            return false;

        }

        QVector<QPair<QString, double> > membersAndWeights;

        QSet<QString> members;

        int i = 1;

        while ((i + 1) < tokens.size()) {

            const QString &token(tokens.at(i + 1));

            if (uniqueSetOfItemIdentifiers.contains(token)) {

                if (!members.contains(token))
                    members.insert(token);
                else
                    emit appendLogItem(LogListModel::WARNING, "check module definition in file \"" + d_parameters.fileDefiningModules + "\" at line " + QString::number(numberOfLinesRead) + ", module with identifier \"" +  tokens.at(0) + "\" conatins duplicate members with identifier \"" + token + "\"");

                membersAndWeights << QPair<QString, double>(token, tokens.at(i).toDouble());

            } else
                emit appendLogItem(LogListModel::WARNING, "member \"" + token + "\" of module \"" + tokens.at(0) + "\" is not present in data set");

            i = i + 2;

        }

        if (i != tokens.size()) {

            emit appendLogItem(LogListModel::ERROR, "error at line number " + QString::number(numberOfLinesRead) + " in file defining modules \"" + d_parameters.fileDefiningModules + "\", probably mismatch between number of member identifiers and weights");

            d_file.close();

            return false;

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            d_file.close();

            return false;

        }

        if (!membersAndWeights.isEmpty()) {

            d_moduleIdentifiers << tokens.at(0);

            d_moduleToMembersAndWeights << membersAndWeights;

            emit appendLogItem(LogListModel::MESSAGE, "the score for module \"" + tokens.at(0) + "\" will be calculated with the following mapped members (n=" + QString::number(members.size()) + "): " + ConvertFunctions::toString(members.values()));

        } else
            emit appendLogItem(LogListModel::WARNING, "module \"" + tokens.at(0) + "\" has no members that could be mapped to the data set and there for omitted from further analysis");

    }

    d_file.close();

    emit stopProgress(1);

    emit appendLogItem(LogListModel::MESSAGE, "imported " + QString::number(d_moduleToMembersAndWeights.size()) + " modules from \"" + d_parameters.fileDefiningModules + "\"");

    return true;

}

void CalculateModuleScoresWorkerClass::doWork()
{
    emit processStarted("calculate module scores");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Calculate module scores"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "file defining modules: \"" + d_parameters.fileDefiningModules + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "format of file defining modules: " + d_parameters.formatOfFileDefiningModules);

    if (!d_parameters.exportDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    if (d_parameters.formatOfFileDefiningModules == "module defined by gene membership only")
        this->readModules_moduleDefinedByGeneMembershipOnly();
    else if (d_parameters.formatOfFileDefiningModules == "module defined by gene membership and weight")
        this->readModules_moduleDefinedByGeneMembershipAndWeight();
    else if (d_parameters.formatOfFileDefiningModules == "module defined by weight and gene membership")
        this->readModules_moduleDefinedByWeightAndGeneMembership();

    emit startProgress(0, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    QHash<QString, int> itemIdentifierToIndex;

    for (int i = 0; i< d_parameters.selectedItemIdentifiersAndIndexes.first.size(); ++i)
        itemIdentifierToIndex.insert(d_parameters.selectedItemIdentifiersAndIndexes.first.at(i), i);

    QVector<QVector<double> > matrix;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        matrix << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    QVector<QVector<QVariant> > moduleScoreMatrix;

    emit startProgress(0, "Processing samples", 0, d_parameters.selectedVariableIdentifiersAndIndexes.second.size());

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        const QVector<double> &currentDataVector(matrix.at(i));

        emit startProgress(1, "Processing modules", 0, d_moduleToMembersAndWeights.size());

        QVector<QVariant> moduleScoreVector;

        moduleScoreVector = d_parameters.baseDataset->annotationValues(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelsDefiningAnnotationValuesForVariables, d_parameters.orientation).toVector();

        for (int j = 0; j < d_moduleToMembersAndWeights.size(); ++j) {

            double score = 0.0;

            double totalWeight = 0.0;

            for (const QPair<QString, double> &memberAndWeight: d_moduleToMembersAndWeights.at(j)) {

                score += currentDataVector.at(itemIdentifierToIndex.value(memberAndWeight.first)) * memberAndWeight.second;

                totalWeight += std::fabs(memberAndWeight.second);

            }

            moduleScoreVector << (score / totalWeight);

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(1, j + 1);

        }

        moduleScoreMatrix << moduleScoreVector;

        emit stopProgress(1);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    if (!this->processTableResult("Module scores", QStringList() << d_parameters.annotationLabelsDefiningAnnotationValuesForVariables << d_moduleIdentifiers, d_parameters.selectedVariableIdentifiersAndIndexes.first, moduleScoreMatrix, QString()))
        return;

    QThread::currentThread()->quit();

}
