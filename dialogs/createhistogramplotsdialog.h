#ifndef CREATEHISTOGRAMPLOTSDIALOG_H
#define CREATEHISTOGRAMPLOTSDIALOG_H

#include <QString>
#include <QSharedPointer>

#include "base/basematrix.h"
#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/plotexportparametersbuildingblockwidget.h"
#include "widgets/selectfilebuildingblockwidget.h"
#include "widgets/spinboxbuildingblockwidget.h"
#include "workerclasses/createhistogramplotsworkerclass.h"

class CreateHistogramPlotsDialog : public BaseDialog
{

    Q_OBJECT

public:

    CreateHistogramPlotsDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~CreateHistogramPlotsDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_subsetIdentifiers;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_scaleToFrequencies;

    SpinBoxBuildingBlockWidget *d_spinBoxBuildingBlockWidget_numberOfBins;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_plotMode;

    PlotExportParametersBuildingBlockWidget *d_plotExportParametersBuildingBlockWidget;

    SelectFileBuildingBlockWidget *d_selectFileBuildingBlockWidget_templatePlot;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

    void annotationLabelDefiningSubsetChanged(const QString &currentAnnotationLabel);

};

#endif // CREATEHISTOGRAMPLOTSDIALOG_H
