#ifndef OPENPROJECTWORKERCLASS_H
#define OPENPROJECTWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QFile>

#include "base/baseworkerclass.h"
#include "base/project.h"

class OpenProjectWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

    };

    OpenProjectWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    OpenProjectWorkerClass::Parameters d_parameters;

    QFile d_fileIn;

private slots:

    void updateFileProgress();
};

#endif // OPENPROJECTWORKERCLASS_H
