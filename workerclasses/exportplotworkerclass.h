#ifndef EXPORTPLOTWORKERCLASS_H
#define EXPORTPLOTWORKERCLASS_H

#include <QString>
#include <QSharedPointer>
#include <QFile>
#include <QFileInfo>
#include <QChartView>
#include <QImage>
#include <QPainter>
#include <QSizeF>
#include <QRectF>
#include <QImageWriter>
#include <QPrinter>

#include "base/baseworkerclass.h"
#include "base/plotresultitem.h"
#include "charts/exportplottofile.h"

class ExportPlotWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

        QChartView *chartViewToSave;

        int heightInMillimeters;

        int widthInMillimeters;

        int resolutionInDPI;

    };

    ExportPlotWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    ExportPlotWorkerClass::Parameters d_parameters;

};

#endif // EXPORTPLOTWORKERCLASS_H
