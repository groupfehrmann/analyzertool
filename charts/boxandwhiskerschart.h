#ifndef BOXANDWHISKERSCHART_H
#define BOXANDWHISKERSCHART_H

#include <QObject>
#include <QApplication>
#include <QChart>
#include <QStringList>
#include <QEventLoop>
#include <QPair>
#include <QBoxSet>
#include <QBoxPlotSeries>
#include <QScatterSeries>
#include <QValueAxis>
#include <QBarCategoryAxis>

#include "charts/exportplottofile.h"
#include "math/mathdescriptives.h"
#include "charts/basechart.h"

using namespace QtCharts;

class BoxAndWhiskersChart : public BaseChart
{

    Q_OBJECT

public:

    explicit BoxAndWhiskersChart(QObject *parent = nullptr, const QString &title = QString(), const QStringList &variableIdentifiers = QStringList(), const QStringList &subsetIdentifiers = QStringList(), const QVector<QVector<QVector<double> > > &data = QVector<QVector<QVector<double> > >());

    explicit BoxAndWhiskersChart(QObject *parent = nullptr, const QString &title = QString(), const QVector<double> &data = QVector<double>());

signals:

    void initializeChart(const QString &title, const QStringList &variableIdentifiers, const QStringList &subsetIdentifiers, const QVector<QVector<QVector<double> > > &data);

private slots:

    void _initializeChart(const QString &title, const QStringList &variableIdentifiers, const QStringList &subsetIdentifiers, const QVector<QVector<QVector<double> > > &data);

};

#endif // BOXANDWHISKERSCHART_H
