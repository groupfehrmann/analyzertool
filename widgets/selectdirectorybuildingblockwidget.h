#ifndef SELECTDIRECTORYBUILDINGBLOCKWIDGET_H
#define SELECTDIRECTORYBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QDir>
#include <QFileDialog>

#include "dialogs/basedialog.h"

namespace Ui {

class SelectDirectoryBuildingBlockWidget;

}

class SelectDirectoryBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectDirectoryBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), bool rememberSettings = false);

    ~SelectDirectoryBuildingBlockWidget();

    QLabel *pointerToInternalLabel();

    QLineEdit *pointerToInternalLineEdit();

    QPushButton *pointerToInternalPushButton();

private:

    Ui::SelectDirectoryBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

    void evaluateDirectory();

private slots:

    void lineEdit_edited(const QString &text);

    void pushButton_clicked();


signals:

    void directoryChanged(const QString &path);

    void invalidDirectory();
};

#endif // SELECTDIRECTORYBUILDINGBLOCKWIDGET_H
