#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(datasetAdded(QSharedPointer<BaseDataset>)), ui->stackedWidget_viewer, SLOT(addDataset(QSharedPointer<BaseDataset>)));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(datasetNameChanged(QUuid,QString)), this, SLOT(nameOfCurrentSelectedDatasetChanged()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(datasetRemoved(QUuid)), ui->stackedWidget_viewer, SLOT(removeWidget(QUuid)));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(datasetSelected()), this, SLOT(datasetSelected()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(itemActivated(QUuid)), ui->stackedWidget_viewer, SLOT(setCurrentWidget(QUuid)));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(resultItemAdded(QSharedPointer<BaseResultItem>)), ui->stackedWidget_viewer, SLOT(addResultItem(QSharedPointer<BaseResultItem>)));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(resultItemNameChanged(QUuid,QString)), this, SLOT(nameOfCurrentSelectedResultItemChanged()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(resultItemRemoved(QUuid)), ui->stackedWidget_viewer, SLOT(removeWidget(QUuid)));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(resultItemSelected()), this, SLOT(resultItemSelected()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(resultNameChanged(QUuid,QString)), this, SLOT(nameOfCurrentSelectedResultChanged()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(resultSelected()), this, SLOT(resultSelected()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(projectSelected()), this, SLOT(projectSelected()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(projectNameChanged(QUuid,QString)), this, SLOT(nameOfCurrentSelectedProjectChanged()));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(showSelectedOnly(QUuid,bool)), ui->stackedWidget_viewer, SLOT(showSelectedOnly(QUuid,bool)));

    this->connect(ui->projectsAndResultsOverviewWidget, SIGNAL(noValidItemSelected()), this, SLOT(noValidItemSelectedInProjectsAndResultsOverviewWidget()));

    this->connect(&d_processControler, SIGNAL(addToRecentlyOpenedFiles(QString,QString)), this, SLOT(addToRecentlyOpenedFiles(QString,QString)));

    this->connect(&d_processControler, SIGNAL(annotationsBeginChange(QUuid,BaseMatrix::Orientation)), ui->stackedWidget_viewer, SLOT(annotationsBeginChange(QUuid,BaseMatrix::Orientation)));

    this->connect(&d_processControler, SIGNAL(annotationsEndChange(QUuid,BaseMatrix::Orientation)), ui->stackedWidget_viewer, SLOT(annotationsEndChange(QUuid,BaseMatrix::Orientation)));

    this->connect(&d_processControler, SIGNAL(appendLogItem(LogListModel::Type,QString)), ui->logWidget, SLOT(appendLogItem(LogListModel::Type,QString)));

    this->connect(&d_processControler, SIGNAL(datasetAvailable(BaseDataset*)), ui->projectsAndResultsOverviewWidget, SLOT(addDataset(BaseDataset*)));

    this->connect(&d_processControler, SIGNAL(datasetBeginChange(QUuid)), ui->stackedWidget_viewer, SLOT(datasetBeginChange(QUuid)));

    this->connect(&d_processControler, SIGNAL(datasetEndChange(QUuid)), ui->stackedWidget_viewer, SLOT(datasetEndChange(QUuid)));

    this->connect(&d_processControler, SIGNAL(matrixDataBeginChange(QUuid)), ui->stackedWidget_viewer, SLOT(matrixDataBeginChange(QUuid)));

    this->connect(&d_processControler, SIGNAL(matrixDataEndChange(QUuid)), ui->stackedWidget_viewer, SLOT(matrixDataEndChange(QUuid)));

    this->connect(&d_processControler, SIGNAL(processStarted(QString)), ui->progressWidget, SLOT(startProcess(QString)));

    this->connect(&d_processControler, SIGNAL(processStopped()), ui->progressWidget, SLOT(stopProcess()));

    this->connect(&d_processControler, SIGNAL(projectAvailable(Project*)), ui->projectsAndResultsOverviewWidget, SLOT(addProject(Project*)));

    this->connect(&d_processControler, SIGNAL(resultAvailable(Result*)), ui->projectsAndResultsOverviewWidget, SLOT(addResult(Result*)));

    this->connect(&d_processControler, SIGNAL(resultItemAvailable(BaseResultItem*)), ui->projectsAndResultsOverviewWidget, SLOT(addResultItem(BaseResultItem*)));

    this->connect(&d_processControler, SIGNAL(startProgress(int,QString,double,double)), ui->progressWidget, SLOT(startProgress(int,QString,double,double)));

    this->connect(&d_processControler, SIGNAL(stopProgress(int)), ui->progressWidget, SLOT(stopProgress(int)));

    this->connect(&d_processControler, SIGNAL(updateProgress(int,double)), ui->progressWidget, SLOT(updateProgress(int,double)));

    this->connect(ui->progressWidget, SIGNAL(cancelWorkerThread()), &d_processControler, SLOT(cancelWorkerThread()));

    this->connect(ui->progressWidget, SIGNAL(appendLogItem(LogListModel::Type,QString)), ui->logWidget, SLOT(appendLogItem(LogListModel::Type,QString)));

    this->connect(ui->stackedWidget_viewer, SIGNAL(appendLogItem(LogListModel::Type,QString)), ui->logWidget, SLOT(appendLogItem(LogListModel::Type,QString)));

    this->setWindowTitle(QCoreApplication::applicationName());

    this->readApplicationSettings();

    this->connectActionsInMenuToSlots();

    this->projectSelected();

    this->createRecentlyOpenedActions();

    this->updateRecentlyOpenedActions();

}

MainWindow::~MainWindow()
{

    this->writeApplicationSettings();

    delete ui;

}

void MainWindow::addToRecentlyOpenedFiles(const QString &name, const QString &path)
{

    for (int i = d_pathOfRecentlyOpenedFiles.size() - 1; i >= 0; --i) {

        if (d_pathOfRecentlyOpenedFiles.at(i) == path) {

            d_nameOfRecentlyOpenedFiles.removeAt(i);

            d_pathOfRecentlyOpenedFiles.removeAt(i);

        }
    }

    d_nameOfRecentlyOpenedFiles.prepend(name);

    d_pathOfRecentlyOpenedFiles.prepend(path);

    this->updateRecentlyOpenedActions();

}

void MainWindow::clearRecentlyOpenedFiles()
{

    d_nameOfRecentlyOpenedFiles.clear();

    d_pathOfRecentlyOpenedFiles.clear();

    this->updateRecentlyOpenedActions();

}

void MainWindow::close()
{

    ui->projectsAndResultsOverviewWidget->removeCurrentSelected();

}

void MainWindow::closeAll()
{

    ui->projectsAndResultsOverviewWidget->removeAll();

}

void MainWindow::connectActionsInMenuToSlots()
{

    this->connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(open()));

    this->connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(save()));

    this->connect(ui->actionClose, SIGNAL(triggered()), this, SLOT(close()));

    this->connect(ui->actionClose_all, SIGNAL(triggered()), this, SLOT(closeAll()));

    this->connect(ui->actionData_matrix_from_a_single_file, SIGNAL(triggered()), this, SLOT(importDataMatrixFromSingleFile()));

    this->connect(ui->actionTab_delimited_text_file, SIGNAL(triggered()), this, SLOT(exportToTabDelimitedTextFile()));

    this->connect(ui->actionJSON_file, SIGNAL(triggered()), this, SLOT(exportToJSONFile()));

    this->connect(ui->actionPlot_to_image_file, SIGNAL(triggered()), this, SLOT(exportPlotToImageFile()));

    this->connect(ui->actionColumn_annotations, SIGNAL(triggered()), this, SLOT(importColumnAnnotations()));

    this->connect(ui->actionRow_annotations, SIGNAL(triggered()), this, SLOT(importRowAnnotations()));

    this->connect(ui->actionTranspose, SIGNAL(triggered()), this, SLOT(transpose()));

    this->connect(ui->actionMerge_datasets, SIGNAL(triggered()), this, SLOT(mergeDatasets()));

    this->connect(ui->actionSplitIdentifiers, SIGNAL(triggered()), this, SLOT(splitIdentifiers()));

    this->connect(ui->actionModify_with_regular_expression, SIGNAL(triggered()), this, SLOT(modifyIdentifiersBasedOnRegExp()));

    this->connect(ui->actionDeduplicateIdentifiers, SIGNAL(triggered()), this, SLOT(deduplicateIdentifiers()));

    this->connect(ui->actionReplace_with_annotation_value, SIGNAL(triggered()), this, SLOT(replaceIdentifierWithAnnotationValue()));

    this->connect(ui->actionTransformDataMatrix, SIGNAL(triggered()), this, SLOT(transformDataMatrix()));

    this->connect(ui->actionSelect_With_regular_expression, SIGNAL(triggered()), this, SLOT(selectWithRegularExpression()));

    this->connect(ui->actionSelect_With_tokens, SIGNAL(triggered()), this, SLOT(selectWithTokens()));

    this->connect(ui->actionEdit_plot_appearance, SIGNAL(triggered()), this, SLOT(editPlotAppearance()));

    this->connect(ui->actionVertical_bar_plots, SIGNAL(triggered()), this, SLOT(createVerticalBarPlots()));

    this->connect(ui->actionVertical_percentage_bar_plots, SIGNAL(triggered()), this, SLOT(createVerticalPercentBarPlots()));

    this->connect(ui->actionVertical_stacked_bar_plots, SIGNAL(triggered()), this, SLOT(createVerticalStackedBarPlots()));

    this->connect(ui->actionHorizontal_bar_plots, SIGNAL(triggered()), this, SLOT(createHorizontalBarPlots()));

    this->connect(ui->actionHorizontal_percentage_bar_plots, SIGNAL(triggered()), this, SLOT(createHorizontalPercentBarPlots()));

    this->connect(ui->actionHorizontal_stacked_bar_plots, SIGNAL(triggered()), this, SLOT(createHorizontalStackedBarPlots()));

    this->connect(ui->actionGenomic_mapping_rank_on_x_axis_Homo_Sapiens_plots, SIGNAL(triggered()), this, SLOT(createScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiens()));

    this->connect(ui->actionGenomic_mapping_on_x_axis_Homo_Sapiens_plots, SIGNAL(triggered()), this, SLOT(createScatterPlotsWithGenomicMappingOnXAxisHomoSapiens()));

    this->connect(ui->actionHistogram_plots, SIGNAL(triggered()), this, SLOT(createHistogramPlots()));

    this->connect(ui->actionBox_and_whiskers_plots, SIGNAL(triggered()), this, SLOT(createBoxAndWhiskersPlots()));

    this->connect(ui->actionRank_on_x_axis_plots, SIGNAL(triggered()), this, SLOT(createScatterPlotsWithRankOnXAxis()));

    this->connect(ui->actionDescriptives, SIGNAL(triggered()), this, SLOT(calculateDescriptives()));

    this->connect(ui->actionModule_scores, SIGNAL(triggered()), this, SLOT(calculateModuleScores()));

    this->connect(ui->actionIndependent_component_analysis, SIGNAL(triggered()), this, SLOT(independentComponentAnalysis()));

    this->connect(ui->actionPrincipal_component_analysis, SIGNAL(triggered()), this, SLOT(principalComponentAnalysis()));

    this->connect(ui->actionCompare_samples_continuous, SIGNAL(triggered()), this, SLOT(compareSamplesContinuous()));

    this->connect(ui->actionPie_plots, SIGNAL(triggered()), this, SLOT(createPiePlots()));

    this->connect(ui->actionCreate_MD5_hash_sum_for_files, SIGNAL(triggered()), this, SLOT(createMD5HashSumForFiles()));

    this->connect(ui->actionCreate_Gene_Network, SIGNAL(triggered()), this, SLOT(createGeneNetwork()));

    this->connect(ui->actionCalculate_distance_matrix, SIGNAL(triggered()), this, SLOT(calculateDistanceMatrix()));

    this->connect(ui->actionGene_set_enrichment_analysis, SIGNAL(triggered()), this, SLOT(geneSetEnrichmentAnalysis()));

}

void MainWindow::createBoxAndWhiskersPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create box and whiskers plots, no dataset selected", 5000);

        return;

    }

    CreateBoxAndWhiskersPlotDialog createBoxAndWhiskersPlotDialog(this, "Create box and whiskers plots", "createHistogramPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createBoxAndWhiskersPlotDialog.exec())
        return;

    d_processControler.startProcess(new CreateBoxAndWhiskersPlotWorkerClass(nullptr, createBoxAndWhiskersPlotDialog.parameters()));

}

void MainWindow::createHistogramPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create histogram plots, no dataset selected", 5000);

        return;

    }

    CreateHistogramPlotsDialog createHistogramPlotsDialog(this, "Create histogram plots", "createHistogramPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createHistogramPlotsDialog.exec())
        return;

    d_processControler.startProcess(new CreateHistogramPlotsWorkerClass(nullptr, createHistogramPlotsDialog.parameters()));

}

void MainWindow::createPiePlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create pie plots, no dataset selected", 5000);

        return;

    }

    CreatePiePlotsDialog createPiePlotsDialog(this, "Create pie plots", "createPiePlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createPiePlotsDialog.exec())
        return;

    d_processControler.startProcess(new CreatePiePlotsWorkerClass(nullptr, createPiePlotsDialog.parameters()));

}

void MainWindow::createHorizontalBarPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create horizontal bar plots, no dataset selected", 5000);

        return;

    }

    CreateBarPlotsDialog createBarPlotsDialog(this, "Create horizontal bar plots", "createHorizontalBarPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createBarPlotsDialog.exec())
        return;

    CreateBarPlotsWorkerClass::Parameters *parameters = static_cast<CreateBarPlotsWorkerClass::Parameters *>(createBarPlotsDialog.parameters());

    parameters->seriesType = QAbstractSeries::SeriesTypeHorizontalBar;

    d_processControler.startProcess(new CreateBarPlotsWorkerClass(nullptr, createBarPlotsDialog.parameters()));

}

void MainWindow::createHorizontalPercentBarPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create horizontal percent bar plots, no dataset selected", 5000);

        return;

    }

    CreateBarPlotsDialog createBarPlotsDialog(this, "Create horizontal percentage bar plots", "createHorizontalPercentageBarPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createBarPlotsDialog.exec())
        return;

    CreateBarPlotsWorkerClass::Parameters *parameters = static_cast<CreateBarPlotsWorkerClass::Parameters *>(createBarPlotsDialog.parameters());

    parameters->seriesType = QAbstractSeries::SeriesTypeHorizontalPercentBar;

    d_processControler.startProcess(new CreateBarPlotsWorkerClass(nullptr, createBarPlotsDialog.parameters()));

}

void MainWindow::createHorizontalStackedBarPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create horizontal stacked bar plots, no dataset selected", 5000);

        return;

    }

    CreateBarPlotsDialog createBarPlotsDialog(this, "Create horizontal stacked bar plots", "createHorizontalStackedBarPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createBarPlotsDialog.exec())
        return;

    CreateBarPlotsWorkerClass::Parameters *parameters = static_cast<CreateBarPlotsWorkerClass::Parameters *>(createBarPlotsDialog.parameters());

    parameters->seriesType = QAbstractSeries::SeriesTypeHorizontalStackedBar;

    d_processControler.startProcess(new CreateBarPlotsWorkerClass(nullptr, createBarPlotsDialog.parameters()));

}

void MainWindow::createRecentlyOpenedActions()
{

    for (int i = 0; i < 10; ++i) {

        d_recentlyOpenedActions.append(new QAction(this));

        d_recentlyOpenedActions.last()->setVisible(false);

        this->connect(d_recentlyOpenedActions.last(), SIGNAL(triggered()), this, SLOT(openRecentlyOpenedFile()));

        ui->menuRecently_opened->addAction(d_recentlyOpenedActions.last());
    }

    ui->menuRecently_opened->addSeparator();

    d_clearRecentlyOpenedFilesAction = new QAction("Clear recent projects", this);

    this->connect(d_clearRecentlyOpenedFilesAction, SIGNAL(triggered()), this, SLOT(clearRecentlyOpenedFiles()));

    ui->menuRecently_opened->addAction(d_clearRecentlyOpenedFilesAction);

}

void MainWindow::createScatterPlotsWithRankOnXAxis()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create scatter plots with ranke on x-axis, no dataset selected", 5000);

        return;

    }

    CreateScatterPlotsWithRankOnXAxisDialog createScatterPlotsWithRankOnXAxisDialog(this, "Create scatter plots with rank on x-axis", "createScatterPlotsWithRankOnXAxisDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createScatterPlotsWithRankOnXAxisDialog.exec())
        return;

    d_processControler.startProcess(new CreateScatterPlotsWithRankOnXAxisWorkerClass(nullptr, createScatterPlotsWithRankOnXAxisDialog.parameters()));

}

void MainWindow::createScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiens()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create scatter plots with genomic mapping rank on x-axis (Homo Sapiens), no dataset selected", 5000);

        return;

    }

    CreateScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensDialog createScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensDialog(this, "Create scatter plots with genomic mapping rank on x-axis (Homo Sapiens)", "createScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensDialog.exec())
        return;

    d_processControler.startProcess(new CreateScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensWorkerClass(nullptr, createScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiensDialog.parameters()));

}

void MainWindow::createScatterPlotsWithGenomicMappingOnXAxisHomoSapiens()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create scatter plots with genomic mapping on x-axis (Homo Sapiens), no dataset selected", 5000);

        return;

    }

    CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog createScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog(this, "Create scatter plots with genomic mapping on x-axis (Homo Sapiens)", "createScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog.exec())
        return;

    d_processControler.startProcess(new CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass(nullptr, createScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog.parameters()));

}

void MainWindow::createVerticalBarPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create vertical bar plots, no dataset selected", 5000);

        return;

    }

    CreateBarPlotsDialog createBarPlotsDialog(this, "Create vertical bar plots", "createVerticalBarPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createBarPlotsDialog.exec())
        return;

    CreateBarPlotsWorkerClass::Parameters *parameters = static_cast<CreateBarPlotsWorkerClass::Parameters *>(createBarPlotsDialog.parameters());

    parameters->seriesType = QAbstractSeries::SeriesTypeBar;

    d_processControler.startProcess(new CreateBarPlotsWorkerClass(nullptr, createBarPlotsDialog.parameters()));

}

void MainWindow::createVerticalPercentBarPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create vertical percent bar plots, no dataset selected", 5000);

        return;

    }

    CreateBarPlotsDialog createBarPlotsDialog(this, "Create vertical percentage bar plots", "createVerticalPercentageBarPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createBarPlotsDialog.exec())
        return;

    CreateBarPlotsWorkerClass::Parameters *parameters = static_cast<CreateBarPlotsWorkerClass::Parameters *>(createBarPlotsDialog.parameters());

    parameters->seriesType = QAbstractSeries::SeriesTypePercentBar;

    d_processControler.startProcess(new CreateBarPlotsWorkerClass(nullptr, createBarPlotsDialog.parameters()));

}

void MainWindow::createVerticalStackedBarPlots()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not create vertical stacked bar plots, no dataset selected", 5000);

        return;

    }

    CreateBarPlotsDialog createBarPlotsDialog(this, "Create vertical stacked bar plots", "createVerticalStackedBarPlotsDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createBarPlotsDialog.exec())
        return;

    CreateBarPlotsWorkerClass::Parameters *parameters = static_cast<CreateBarPlotsWorkerClass::Parameters *>(createBarPlotsDialog.parameters());

    parameters->seriesType = QAbstractSeries::SeriesTypeStackedBar;

    d_processControler.startProcess(new CreateBarPlotsWorkerClass(nullptr, createBarPlotsDialog.parameters()));

}

void MainWindow::datasetSelected()
{

    ui->actionOpen->setEnabled(true);

    ui->actionSave->setEnabled(true);

    ui->actionClose->setText("Close dataset \"" + ui->projectsAndResultsOverviewWidget->currentDataset()->name() + "\"");

    ui->actionClose->setEnabled(true);

    ui->actionData_matrix_from_a_single_file->setEnabled(true);

    ui->actionRow_annotations->setEnabled(true);

    ui->actionColumn_annotations->setEnabled(true);

    ui->actionTab_delimited_text_file->setEnabled(true);

    ui->actionJSON_file->setEnabled(true);

    ui->actionTranspose->setEnabled(true);

    ui->actionSplitIdentifiers->setEnabled(true);

    ui->actionModify_with_regular_expression->setEnabled(true);

    ui->actionDeduplicateIdentifiers->setEnabled(true);

    ui->actionReplace_with_annotation_value->setEnabled(true);

    ui->actionTransformDataMatrix->setEnabled(true);

    ui->actionSelect_With_regular_expression->setEnabled(true);

    ui->actionSelect_With_tokens->setEnabled(true);

    ui->actionPlot_to_image_file->setEnabled(false);

    ui->actionEdit_plot_appearance->setEnabled(false);

    ui->actionVertical_bar_plots->setEnabled(true);

    ui->actionVertical_percentage_bar_plots->setEnabled(true);

    ui->actionVertical_stacked_bar_plots->setEnabled(true);

    ui->actionHorizontal_bar_plots->setEnabled(true);

    ui->actionHorizontal_percentage_bar_plots->setEnabled(true);

    ui->actionHorizontal_stacked_bar_plots->setEnabled(true);

    ui->actionHistogram_plots->setEnabled(true);

    ui->actionBox_and_whiskers_plots->setEnabled(true);

    ui->actionPie_plots->setEnabled(true);

    ui->actionGenomic_mapping_rank_on_x_axis_Homo_Sapiens_plots->setEnabled(true);

    ui->actionGenomic_mapping_on_x_axis_Homo_Sapiens_plots->setEnabled(true);

    ui->actionRank_on_x_axis_plots->setEnabled(true);

    ui->actionIndependent_component_analysis->setEnabled(true);

    ui->actionPrincipal_component_analysis->setEnabled(true);

    ui->actionCompare_samples_continuous->setEnabled(true);

    ui->actionDescriptives->setEnabled(true);

    ui->actionModule_scores->setEnabled(true);

    ui->actionCreate_Gene_Network->setEnabled(true);

}

void MainWindow::editPlotAppearance()
{

    if (ui->projectsAndResultsOverviewWidget->currentSelectedItemType() == ProjectsAndResultsOverviewWidget::PLOTRESULTITEM) {

        EditPlotAppearanceDialog editPlotAppearanceDialog(nullptr, ui->stackedWidget_viewer->currentChartView()->chart());

        editPlotAppearanceDialog.exec();

    }

}

void MainWindow::exportPlotToImageFile()
{

    if (ui->stackedWidget_viewer->currentChartView()) {

        SelectFileAndPlotParametersDialog selectFileAndPlotParametersDialog;

        selectFileAndPlotParametersDialog.setWidthOfPlot(static_cast<int>(ui->stackedWidget_viewer->currentChartView()->rect().width() / static_cast<double>(selectFileAndPlotParametersDialog.dpiOfPlot()) * 25.4));

        selectFileAndPlotParametersDialog.setHeightOfPlot(static_cast<int>(ui->stackedWidget_viewer->currentChartView()->rect().height() / static_cast<double>(selectFileAndPlotParametersDialog.dpiOfPlot()) * 25.4));

        if (!selectFileAndPlotParametersDialog.exec())
            return;

        ExportPlotWorkerClass::Parameters parameters;

        parameters.pathToFile = selectFileAndPlotParametersDialog.pathOfPlot();

        parameters.heightInMillimeters = selectFileAndPlotParametersDialog.heigthOfPlot();

        parameters.widthInMillimeters = selectFileAndPlotParametersDialog.widthOfPlot();

        parameters.resolutionInDPI = selectFileAndPlotParametersDialog.dpiOfPlot();

        if (parameters.pathToFile.isEmpty())
            return;

        parameters.chartViewToSave = ui->stackedWidget_viewer->currentChartView();

        d_processControler.startProcess(new ExportPlotWorkerClass(nullptr, &parameters));

    }

}

void MainWindow::exportToTabDelimitedTextFile()
{

    if (ui->stackedWidget_viewer->currentTableModel()) {

        ExportToTabDelimitedTextFileWorkerClass::Parameters parameters;

        parameters.pathToFile = this->selectFile(QFileDialog::AcceptSave, QString(), QString(), {"Text file (*.txt)"});

        if (parameters.pathToFile.isEmpty())
            return;

        if (ui->projectsAndResultsOverviewWidget->currentSelectedItemType() == ProjectsAndResultsOverviewWidget::DATASET) {

            switch (ui->stackedWidget_viewer->indexOfSelectedTabInDatasetWidget()) {

                case 0 : parameters.descriptionOfObjectToExport = "data matrix of dataset \'" + ui->projectsAndResultsOverviewWidget->currentDataset()->name() + "\'"; break;

                case 1 : parameters.descriptionOfObjectToExport = "row annotations of dataset \'" + ui->projectsAndResultsOverviewWidget->currentDataset()->name() + "\'"; break;

                case 2 : parameters.descriptionOfObjectToExport = "column annotations of dataset \'" + ui->projectsAndResultsOverviewWidget->currentDataset()->name() + "\'"; break;

            }

        } else if (ui->projectsAndResultsOverviewWidget->currentSelectedItemType() == ProjectsAndResultsOverviewWidget::TABLERESULTITEM)
            parameters.descriptionOfObjectToExport = "table item \'" + ui->projectsAndResultsOverviewWidget->currentResultItem()->name() + "\' of result \'" + ui->projectsAndResultsOverviewWidget->currentResult()->name() + "\'";

        parameters.abstractItemModel = ui->stackedWidget_viewer->currentTableModel();

        d_processControler.startProcess(new ExportToTabDelimitedTextFileWorkerClass(nullptr, &parameters));

    }

}

void MainWindow::exportToJSONFile()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not export to JSON file, no dataset selected", 5000);

        return;

    }

    ExportToJSONFileDialog exportToJSONFileDialog(this, "Export to JSON file", "exportToJSONFileDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!exportToJSONFileDialog.exec())
        return;

    d_processControler.startProcess(new ExportToJSONFileWorkerClass(nullptr, exportToJSONFileDialog.parameters()));

}

void MainWindow::importColumnAnnotations()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not import column annotations, no dataset selected", 5000);

        return;

    }

    ImportAnnotationsDialog importColumnAnnotationsDialog(this, "Import column annotations", "importColumnAnnotationsDialog");

    if (!importColumnAnnotationsDialog.exec())
        return;

    ImportAnnotationsWorkerClass::Parameters *parameters = static_cast<ImportAnnotationsWorkerClass::Parameters *>(importColumnAnnotationsDialog.parameters());

    parameters->orientation = BaseMatrix::COLUMN;

    parameters->baseDataset = ui->projectsAndResultsOverviewWidget->currentDataset();

    d_processControler.startProcess(new ImportAnnotationsWorkerClass(nullptr, parameters));

}

void MainWindow::importDataMatrixFromSingleFile()
{

    ImportDatasetFromSingleFileDialog importDatasetFromSingleFileDialog(this, "Import dataset from single file", "importDatasetFromSingleFileDialog");

    if (!importDatasetFromSingleFileDialog.exec())
        return;

    d_processControler.startProcess(new ImportDatasetFromSingleFileWorkerClass(nullptr, importDatasetFromSingleFileDialog.parameters()));

}

void MainWindow::importRowAnnotations()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not import row annotations, no dataset selected", 5000);

        return;

    }

    ImportAnnotationsDialog importRowAnnotationsDialog(this, "Import row annotations", "importRowAnnotationsDialog");

    if (!importRowAnnotationsDialog.exec())
        return;

    ImportAnnotationsWorkerClass::Parameters *parameters = static_cast<ImportAnnotationsWorkerClass::Parameters *>(importRowAnnotationsDialog.parameters());

    parameters->orientation = BaseMatrix::ROW;

    parameters->baseDataset = ui->projectsAndResultsOverviewWidget->currentDataset();

    d_processControler.startProcess(new ImportAnnotationsWorkerClass(nullptr, parameters));

}

void MainWindow::independentComponentAnalysis()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not perform independent componenet analysis, no dataset selected", 5000);

        return;

    }

    IndependentComponentAnalysisDialog independentComponentAnalysisDialog(this, "Independent component analysis", "independentComponentAnalysisDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!independentComponentAnalysisDialog.exec())
        return;

    d_processControler.startProcess(new IndependentComponentAnalysisWorkerClass(nullptr, independentComponentAnalysisDialog.parameters()));

}

void MainWindow::principalComponentAnalysis()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not perform principal componenet analysis, no dataset selected", 5000);

        return;

    }

    PrincipalComponentAnalysisDialog principalComponentAnalysisDialog(this, "Principal component analysis", "principalComponentAnalysisDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!principalComponentAnalysisDialog.exec())
        return;

    d_processControler.startProcess(new PrincipalComponentAnalysisWorkerClass(nullptr, principalComponentAnalysisDialog.parameters()));

}

void MainWindow::calculateDescriptives()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not calculate descriptives, no dataset selected", 5000);

        return;

    }

    CalculateDescriptivesDialog calculateDescriptivesDialog(this, "Calculate descriptives", "calculateDescriptivesDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!calculateDescriptivesDialog.exec())
        return;

    d_processControler.startProcess(new CalculateDescriptivesWorkerClass(nullptr, calculateDescriptivesDialog.parameters()));

}

void MainWindow::calculateModuleScores()
 {

     if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

         ui->statusBar->showMessage("could not calculate module scores, no dataset selected", 5000);

         return;

     }

     CalculateModuleScoresDialog calculateModuleScoresDialog(this, "Calculate module scores", "calculateModuleScoresDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

     if (!calculateModuleScoresDialog.exec())
         return;

     d_processControler.startProcess(new CalculateModuleScoresWorkerClass(nullptr, calculateModuleScoresDialog.parameters()));

 }

void MainWindow::compareSamplesContinuous()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not perform samples comparison (continuous), no dataset selected", 5000);

        return;

    }

    CompareSamplesContinuousDialog compareSamplesContinuousDialog(this, "Compare samples (continuous)", "compareSamplesContinuousDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!compareSamplesContinuousDialog.exec())
        return;

    CompareSamplesWorkerClass::Parameters *parameters = static_cast<CompareSamplesWorkerClass::Parameters *>(compareSamplesContinuousDialog.parameters());

    parameters->comparisonDescription = "Samples comparison (continuous)";

    d_processControler.startProcess(new CompareSamplesWorkerClass(nullptr, compareSamplesContinuousDialog.parameters()));

}

void MainWindow::createMD5HashSumForFiles()
{

    CreateMD5HashSumForFilesDialog createMD5HashSumForFilesDialog(this, "Create MD5 hash sum for files", "createMD5HashSumForFilesDialog");

    if (!createMD5HashSumForFilesDialog.exec())
        return;

    d_processControler.startProcess(new CreateMD5HashSumForFilesWorkerClass(nullptr, createMD5HashSumForFilesDialog.parameters()));

}

void MainWindow::createGeneNetwork()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not construct gene network, no dataset selected", 5000);

        return;

    }

    CreateGeneNetworkDialog createGeneNetworkDialog(this, "Create gene network", "createGeneNetworkDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!createGeneNetworkDialog.exec())
        return;

    d_processControler.startProcess(new CreateGeneNetworkWorkerClass(nullptr, createGeneNetworkDialog.parameters()));

}

void MainWindow::geneSetEnrichmentAnalysis()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not perform gene set enrichment analysis, no dataset selected", 5000);

        return;

    }

    GeneSetEnrichmentAnalysisDialog geneSetEnrichmentAnalysisDialog(this, "Perform gene set enrichment analysis", "geneSetEnrichmentAnalysisDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!geneSetEnrichmentAnalysisDialog.exec())
        return;

    d_processControler.startProcess(new GeneSetEnrichmentAnalysisWorkerClass(nullptr, geneSetEnrichmentAnalysisDialog.parameters()));

}

void MainWindow::mergeDatasets()
{

    MergeDatasetsDialog mergeDatasetsDialog(this, "Merge datasets", "mergeDatasetsDialog",  ui->projectsAndResultsOverviewWidget->projectListModel());

    if (!mergeDatasetsDialog.exec())
        return;

    d_processControler.startProcess(new MergeDatasetsWorkerClass(nullptr, mergeDatasetsDialog.parameters()));

}

void MainWindow::modifyIdentifiersBasedOnRegExp()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not modify identifiers with regular expression, no dataset selected", 5000);

        return;

    }

    ModifyIdentifiersBasedOnRegExpDialog modifyIdentifiersBasedOnRegExpDialog(this, "Modify identifiers based on regular expression", "modifyIdentifiersBasedOnRegExpDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!modifyIdentifiersBasedOnRegExpDialog.exec())
        return;

    d_processControler.startProcess(new ModifyIdentifiersBasedOnRegExpWorkerClass(nullptr, modifyIdentifiersBasedOnRegExpDialog.parameters()));

}

void MainWindow::deduplicateIdentifiers()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not deduplicate identifiers , no dataset selected", 5000);

        return;

    }

    DeduplicateIdentifiersDialog deduplicateIdentifiersDialog(this, "Deduplicate identifiers ", "deduplicateIdentifiersDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!deduplicateIdentifiersDialog.exec())
        return;

    d_processControler.startProcess(new DeduplicateIdentifiersWorkerClass(nullptr, deduplicateIdentifiersDialog.parameters()));

}

void MainWindow::nameOfCurrentSelectedProjectChanged()
{

    ui->actionClose->setText("Close project \"" + ui->projectsAndResultsOverviewWidget->currentProject()->name() + "\"");

}

void MainWindow::nameOfCurrentSelectedDatasetChanged()
{

    ui->actionClose->setText("Close dataset \"" + ui->projectsAndResultsOverviewWidget->currentDataset()->name() + "\"");

}

void MainWindow::nameOfCurrentSelectedResultChanged()
{

    ui->actionClose->setText("Close result \"" + ui->projectsAndResultsOverviewWidget->currentResult()->name() + "\"");

}

void MainWindow::nameOfCurrentSelectedResultItemChanged()
{

    ui->actionClose->setText("Close result item \"" + ui->projectsAndResultsOverviewWidget->currentResultItem()->name() + "\"");

}

void MainWindow::noValidItemSelectedInProjectsAndResultsOverviewWidget()
{

    ui->actionOpen->setEnabled(true);

    ui->actionSave->setEnabled(false);

    ui->actionClose->setText("Close");

    ui->actionClose->setEnabled(false);

    ui->actionData_matrix_from_a_single_file->setEnabled(true);

    ui->actionRow_annotations->setEnabled(false);

    ui->actionColumn_annotations->setEnabled(false);

    ui->actionTab_delimited_text_file->setEnabled(false);

    ui->actionJSON_file->setEnabled(false);

    ui->actionTranspose->setEnabled(false);

    ui->actionSplitIdentifiers->setEnabled(false);

    ui->actionModify_with_regular_expression->setEnabled(false);

    ui->actionDeduplicateIdentifiers->setEnabled(false);

    ui->actionReplace_with_annotation_value->setEnabled(false);

    ui->actionTransformDataMatrix->setEnabled(false);

    ui->actionSelect_With_regular_expression->setEnabled(false);

    ui->actionSelect_With_tokens->setEnabled(false);

    ui->actionPlot_to_image_file->setEnabled(false);

    ui->actionEdit_plot_appearance->setEnabled(false);

    ui->actionVertical_bar_plots->setEnabled(false);

    ui->actionVertical_percentage_bar_plots->setEnabled(false);

    ui->actionVertical_stacked_bar_plots->setEnabled(false);

    ui->actionHorizontal_bar_plots->setEnabled(false);

    ui->actionHorizontal_percentage_bar_plots->setEnabled(false);

    ui->actionHorizontal_stacked_bar_plots->setEnabled(false);

    ui->actionHistogram_plots->setEnabled(false);

    ui->actionPie_plots->setEnabled(false);

    ui->actionBox_and_whiskers_plots->setEnabled(false);

    ui->actionGenomic_mapping_rank_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionGenomic_mapping_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionRank_on_x_axis_plots->setEnabled(false);

    ui->actionIndependent_component_analysis->setEnabled(false);

    ui->actionPrincipal_component_analysis->setEnabled(false);

    ui->actionCompare_samples_continuous->setEnabled(false);

    ui->actionDescriptives->setEnabled(false);

    ui->actionModule_scores->setEnabled(false);

    ui->actionCreate_Gene_Network->setEnabled(false);

}

void MainWindow::open()
{

    QString pathToFile = this->selectFile(QFileDialog::AcceptOpen, QString(), QString(), {"AnalyzerTool files (*.pfile *.dfile *.rfile *.plot)"});

    if (pathToFile.isEmpty())
        return;

    this->open(pathToFile);

}

void MainWindow::open(const QString &pathToFile)
{

    QFile fileIn(pathToFile);

    if (!fileIn.open(QIODevice::ReadOnly)) {

        ui->logWidget->appendLogItem(LogListModel::ERROR, "could not open file \"" + pathToFile + "\"");

        return;

    }

    QDataStream out(&fileIn);

    QString label;

    out >> label;

    fileIn.close();

    if (label == QString("_Project_begin#")) {

        OpenProjectWorkerClass::Parameters parameters;

        parameters.pathToFile = pathToFile;

        d_processControler.startProcess(new OpenProjectWorkerClass(nullptr, &parameters));

    } else if (label == QString("_Dataset_begin#")) {

        OpenDatasetWorkerClass::Parameters parameters;

        parameters.pathToFile = pathToFile;

        d_processControler.startProcess(new OpenDatasetWorkerClass(nullptr, &parameters));

    } else if (label == QString("_Plot_begin#")) {

        OpenPlotWorkerClass::Parameters parameters;

        parameters.pathToFile = pathToFile;

        d_processControler.startProcess(new OpenPlotWorkerClass(nullptr, &parameters));

    }

}

void MainWindow::openRecentlyOpenedFile()
{

    this->open(qobject_cast<QAction *>(sender())->property("path").toString());

}

void MainWindow::projectSelected()
{

    ui->actionOpen->setEnabled(true);

    ui->actionSave->setEnabled(true);

    ui->actionClose->setText("Close project \"" + ui->projectsAndResultsOverviewWidget->currentProject()->name() + "\"");

    ui->actionClose->setEnabled(true);

    ui->actionData_matrix_from_a_single_file->setEnabled(true);

    ui->actionRow_annotations->setEnabled(false);

    ui->actionColumn_annotations->setEnabled(false);

    ui->actionTab_delimited_text_file->setEnabled(false);

    ui->actionJSON_file->setEnabled(false);

    ui->actionTranspose->setEnabled(false);

    ui->actionSplitIdentifiers->setEnabled(false);

    ui->actionModify_with_regular_expression->setEnabled(false);

    ui->actionDeduplicateIdentifiers->setEnabled(false);

    ui->actionReplace_with_annotation_value->setEnabled(false);

    ui->actionTransformDataMatrix->setEnabled(false);

    ui->actionSelect_With_regular_expression->setEnabled(false);

    ui->actionSelect_With_tokens->setEnabled(false);

    ui->actionPlot_to_image_file->setEnabled(false);

    ui->actionEdit_plot_appearance->setEnabled(false);

    ui->actionVertical_bar_plots->setEnabled(false);

    ui->actionVertical_percentage_bar_plots->setEnabled(false);

    ui->actionVertical_stacked_bar_plots->setEnabled(false);

    ui->actionHorizontal_bar_plots->setEnabled(false);

    ui->actionHorizontal_percentage_bar_plots->setEnabled(false);

    ui->actionHorizontal_stacked_bar_plots->setEnabled(false);

    ui->actionHistogram_plots->setEnabled(false);

    ui->actionBox_and_whiskers_plots->setEnabled(false);

    ui->actionPie_plots->setEnabled(false);

    ui->actionGenomic_mapping_rank_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionGenomic_mapping_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionRank_on_x_axis_plots->setEnabled(false);

    ui->actionIndependent_component_analysis->setEnabled(false);

    ui->actionPrincipal_component_analysis->setEnabled(false);

    ui->actionCompare_samples_continuous->setEnabled(false);

    ui->actionDescriptives->setEnabled(false);

    ui->actionModule_scores->setEnabled(false);

    ui->actionCreate_Gene_Network->setEnabled(false);

}

void MainWindow::readApplicationSettings()
{

    QSettings settings;

    settings.beginGroup("mainWindow");

    if (settings.contains("state"))
        this->restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        this->restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("splitter_horizontal/state"))
        ui->splitter_horizontal->restoreState(settings.value("splitter_horizontal/state").toByteArray());

    if (settings.contains("splitter_vertical/state"))
        ui->splitter_vertical->restoreState(settings.value("splitter_vertical/state").toByteArray());

    if (settings.contains("tabWidget_mainWindowBottom/currentIndex"))
        ui->tabWidget_mainWindowBottom->setCurrentIndex(settings.value("tabWidget_mainWindowBottom/currentIndex").toInt());

    if (settings.contains("nameOfRecentlyOpenedFiles") && settings.contains("pathOfRecentlyOpenedFiles")) {

        d_nameOfRecentlyOpenedFiles = settings.value("nameOfRecentlyOpenedFiles").toStringList();

        d_pathOfRecentlyOpenedFiles = settings.value("pathOfRecentlyOpenedFiles").toStringList();

    }

    settings.endGroup();

}

void MainWindow::replaceIdentifierWithAnnotationValue()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not replace identifier with annotation value, no dataset selected", 5000);

        return;

    }

    ReplaceIdentifiersDialog replaceIdentifiersDialog(this, "Replace identifier with annotation value", "replaceIdentifiersDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!replaceIdentifiersDialog.exec())
        return;

    d_processControler.startProcess(new ReplaceIdentifiersWorkerClass(nullptr, replaceIdentifiersDialog.parameters()));

}

void MainWindow::resultSelected()
{

    ui->actionOpen->setEnabled(true);

    ui->actionSave->setEnabled(true);

    ui->actionClose->setText("Close result \"" + ui->projectsAndResultsOverviewWidget->currentResult()->name() + "\"");

    ui->actionClose->setEnabled(true);

    ui->actionData_matrix_from_a_single_file->setEnabled(false);

    ui->actionRow_annotations->setEnabled(false);

    ui->actionColumn_annotations->setEnabled(false);

    ui->actionTab_delimited_text_file->setEnabled(false);

    ui->actionJSON_file->setEnabled(false);

    ui->actionTranspose->setEnabled(false);

    ui->actionSplitIdentifiers->setEnabled(false);

    ui->actionModify_with_regular_expression->setEnabled(false);

    ui->actionDeduplicateIdentifiers->setEnabled(false);

    ui->actionReplace_with_annotation_value->setEnabled(false);

    ui->actionTransformDataMatrix->setEnabled(false);

    ui->actionSelect_With_regular_expression->setEnabled(false);

    ui->actionSelect_With_tokens->setEnabled(false);

    ui->actionEdit_plot_appearance->setEnabled(false);

    ui->actionVertical_bar_plots->setEnabled(false);

    ui->actionVertical_percentage_bar_plots->setEnabled(false);

    ui->actionVertical_stacked_bar_plots->setEnabled(false);

    ui->actionHorizontal_bar_plots->setEnabled(false);

    ui->actionHorizontal_percentage_bar_plots->setEnabled(false);

    ui->actionHorizontal_stacked_bar_plots->setEnabled(false);

    ui->actionHistogram_plots->setEnabled(false);

    ui->actionBox_and_whiskers_plots->setEnabled(false);

    ui->actionPie_plots->setEnabled(false);

    ui->actionGenomic_mapping_rank_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionGenomic_mapping_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionRank_on_x_axis_plots->setEnabled(false);

    ui->actionIndependent_component_analysis->setEnabled(false);

    ui->actionPrincipal_component_analysis->setEnabled(false);

    ui->actionCompare_samples_continuous->setEnabled(false);

    ui->actionDescriptives->setEnabled(false);

    ui->actionModule_scores->setEnabled(false);

    ui->actionCreate_Gene_Network->setEnabled(false);

}

void MainWindow::resultItemSelected()
{

    ui->actionOpen->setEnabled(true);

    ui->actionSave->setEnabled(true);

    ui->actionClose->setText("Close result item \"" + ui->projectsAndResultsOverviewWidget->currentResultItem()->name() + "\"");

    ui->actionClose->setEnabled(true);

    ui->actionData_matrix_from_a_single_file->setEnabled(false);

    ui->actionRow_annotations->setEnabled(false);

    ui->actionColumn_annotations->setEnabled(false);

    if (ui->stackedWidget_viewer->currentTableModel())
        ui->actionTab_delimited_text_file->setEnabled(true);
    else
        ui->actionTab_delimited_text_file->setEnabled(false);

    ui->actionJSON_file->setEnabled(false);

    ui->actionTranspose->setEnabled(false);

    ui->actionSplitIdentifiers->setEnabled(false);

    ui->actionModify_with_regular_expression->setEnabled(false);

    ui->actionDeduplicateIdentifiers->setEnabled(false);

    ui->actionReplace_with_annotation_value->setEnabled(false);

    ui->actionTransformDataMatrix->setEnabled(false);

    ui->actionSelect_With_regular_expression->setEnabled(false);

    ui->actionSelect_With_tokens->setEnabled(false);

    if (ui->stackedWidget_viewer->currentChartView()) {

        ui->actionEdit_plot_appearance->setEnabled(true);

        ui->actionPlot_to_image_file->setEnabled(true);

    } else {

        ui->actionEdit_plot_appearance->setEnabled(false);

        ui->actionPlot_to_image_file->setEnabled(false);

    }

    ui->actionVertical_bar_plots->setEnabled(false);

    ui->actionVertical_percentage_bar_plots->setEnabled(false);

    ui->actionVertical_stacked_bar_plots->setEnabled(false);

    ui->actionHorizontal_bar_plots->setEnabled(false);

    ui->actionHorizontal_percentage_bar_plots->setEnabled(false);

    ui->actionHorizontal_stacked_bar_plots->setEnabled(false);

    ui->actionHistogram_plots->setEnabled(false);

    ui->actionPie_plots->setEnabled(false);

    ui->actionBox_and_whiskers_plots->setEnabled(false);

    ui->actionGenomic_mapping_rank_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionGenomic_mapping_on_x_axis_Homo_Sapiens_plots->setEnabled(false);

    ui->actionRank_on_x_axis_plots->setEnabled(false);

    ui->actionIndependent_component_analysis->setEnabled(false);

    ui->actionPrincipal_component_analysis->setEnabled(false);

    ui->actionCompare_samples_continuous->setEnabled(false);

    ui->actionDescriptives->setEnabled(false);

    ui->actionModule_scores->setEnabled(false);

    ui->actionCreate_Gene_Network->setEnabled(false);

}

void MainWindow::selectWithRegularExpression()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not select with regular expression, no dataset selected", 5000);

        return;

    }

    SelectWithRegularExpressionDialog selectWithRegularExpressionDialog(this, "Select with regular expression", "selectWithRegularExpressionDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!selectWithRegularExpressionDialog.exec())
        return;

    d_processControler.startProcess(new SelectWithRegularExpressionWorkerClass(nullptr, selectWithRegularExpressionDialog.parameters()));

}

void MainWindow::selectWithTokens()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not select with tokens, no dataset selected", 5000);

        return;

    }

    SelectWithTokensDialog selectWithTokensDialog(this, "Select with tokens", "selectWithTokensDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!selectWithTokensDialog.exec())
        return;

    d_processControler.startProcess(new SelectWithTokensWorkerClass(nullptr, selectWithTokensDialog.parameters()));

}

void MainWindow::save()
{

    if (ui->projectsAndResultsOverviewWidget->currentSelectedItemType() == ProjectsAndResultsOverviewWidget::INVALID)
        return;

    if (ui->projectsAndResultsOverviewWidget->currentSelectedItemType() == ProjectsAndResultsOverviewWidget::PROJECT) {

        SaveProjectWorkerClass::Parameters parameters;

        parameters.pathToFile = this->selectFile(QFileDialog::AcceptSave, QString(), ui->projectsAndResultsOverviewWidget->currentProject()->name(), {"project file (*.pfile)"}, "pfile");

        if (parameters.pathToFile.isEmpty())
            return;

        parameters.projectToSave = ui->projectsAndResultsOverviewWidget->currentProject();

        d_processControler.startProcess(new SaveProjectWorkerClass(nullptr, &parameters));

    }

    if (ui->projectsAndResultsOverviewWidget->currentSelectedItemType() == ProjectsAndResultsOverviewWidget::DATASET) {

        SaveDatasetWorkerClass::Parameters parameters;

        parameters.pathToFile = this->selectFile(QFileDialog::AcceptSave, QString(), ui->projectsAndResultsOverviewWidget->currentDataset()->name(), {"dataset file (*.dfile)"}, "dfile");

        if (parameters.pathToFile.isEmpty())
            return;

        parameters.datasetToSave = ui->projectsAndResultsOverviewWidget->currentDataset();

        d_processControler.startProcess(new SaveDatasetWorkerClass(nullptr, &parameters));

    }

    if (ui->projectsAndResultsOverviewWidget->currentSelectedItemType() == ProjectsAndResultsOverviewWidget::PLOTRESULTITEM) {

        SavePlotWorkerClass::Parameters parameters;

        parameters.pathToFile = this->selectFile(QFileDialog::AcceptSave, QString(), QString(), {"plot file (*.plot)"}, "plot");

        if (parameters.pathToFile.isEmpty())
            return;

        parameters.chartViewToSave = ui->stackedWidget_viewer->currentChartView();

        d_processControler.startProcess(new SavePlotWorkerClass(nullptr, &parameters));

    }

}

QString MainWindow::selectFile(QFileDialog::AcceptMode acceptMode, const QString &directory, const QString &suggestedFileName, const QStringList &filters, const QString &defaultSuffix) const
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - Select file");

    fileDialog.setAcceptMode(acceptMode);

    if (!defaultSuffix.isEmpty())
        fileDialog.setDefaultSuffix(defaultSuffix);

    QSettings settings;

    settings.beginGroup("MainWindow/fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (!directory.isEmpty())
        fileDialog.setDirectory(directory);
    else if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    if (!suggestedFileName.isEmpty())
        fileDialog.selectFile(suggestedFileName);

    if (!filters.isEmpty())
        fileDialog.setNameFilters(filters);


    settings.endGroup();

    if (fileDialog.exec()) {

        QSettings settings;

        settings.beginGroup("MainWindow/fileDialog");

        settings.setValue("state", fileDialog.saveState());

        settings.setValue("geometry", fileDialog.saveGeometry());

        settings.setValue("lastDirectory", fileDialog.directory().path());

        settings.endGroup();

        return fileDialog.selectedFiles().first();
    }

    return QString();

}

void MainWindow::splitIdentifiers()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not split identifiers, no dataset selected", 5000);

        return;

    }

    SplitIdentifiersDialog splitIdentifiersDialog(this, "Split identifiers", "splitIdentifiersDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!splitIdentifiersDialog.exec())
        return;

    d_processControler.startProcess(new SplitIdentifiersWorkerClass(nullptr, splitIdentifiersDialog.parameters()));

}

void MainWindow::transformDataMatrix()
{

    if (!ui->projectsAndResultsOverviewWidget->currentDataset()) {

        ui->statusBar->showMessage("could not transform data matrix, no dataset selected", 5000);

        return;

    }

    TransformDataMatrixDialog transformDataMatrixDialog(this, "Transform data matrix", "transformDataMatrixDialog", ui->projectsAndResultsOverviewWidget->currentDataset(), ui->projectsAndResultsOverviewWidget->currentSelectedOnly());

    if (!transformDataMatrixDialog.exec())
        return;

    d_processControler.startProcess(new TransformDataMatrixWorkerClass(nullptr, transformDataMatrixDialog.parameters()));

}

void MainWindow::calculateDistanceMatrix()
{

    CalculateDistanceMatrixDialog calculateDistanceMatrixDialog(this, "Calculate distance matrix", "calculateDistanceMatrixDialog",  ui->projectsAndResultsOverviewWidget->projectListModel());

    if (!calculateDistanceMatrixDialog.exec())
        return;

    d_processControler.startProcess(new CalculateDistanceMatrixWorkerClass(nullptr, calculateDistanceMatrixDialog.parameters()));

}

void MainWindow::transpose()
{

    QSharedPointer<BaseDataset> baseDataset = ui->projectsAndResultsOverviewWidget->currentDataset();

    if (!baseDataset) {

        ui->statusBar->showMessage("could not transpose, no dataset selected", 5000);

        return;

    }

    TransposeDatasetWorkerClass::Parameters parameters;

    parameters.datasetToTranspose = baseDataset;

    d_processControler.startProcess(new TransposeDatasetWorkerClass(nullptr, &parameters));

}

void MainWindow::updateRecentlyOpenedActions()
{

    QMutableListIterator<QString> it(d_pathOfRecentlyOpenedFiles);

    while (it.hasNext()) {

        it.next();

        if (!QFile::exists(it.value()))
            it.remove();

    }

    for (int i = 0; i < 10; ++i) {

        if (i < d_pathOfRecentlyOpenedFiles.count()) {

            d_recentlyOpenedActions[i]->setText("\"" + d_nameOfRecentlyOpenedFiles.at(i) + "\" - \"" + d_pathOfRecentlyOpenedFiles.at(i) + "\"");

            d_recentlyOpenedActions[i]->setProperty("path", d_pathOfRecentlyOpenedFiles.at(i));

            d_recentlyOpenedActions[i]->setVisible(true);

        } else
            d_recentlyOpenedActions[i]->setVisible(false);

    }

}

void MainWindow::writeApplicationSettings() const
{

    QSettings settings;

    settings.beginGroup("mainWindow");

    settings.setValue("state", this->saveState());

    settings.setValue("geometry", this->saveGeometry());

    settings.setValue("splitter_horizontal/state", ui->splitter_horizontal->saveState());

    settings.setValue("splitter_vertical/state", ui->splitter_vertical->saveState());

    settings.setValue("tabWidget_mainWindowBottom/currentIndex", ui->tabWidget_mainWindowBottom->currentIndex());

    settings.setValue("nameOfRecentlyOpenedFiles", d_nameOfRecentlyOpenedFiles);

    settings.setValue("pathOfRecentlyOpenedFiles", d_pathOfRecentlyOpenedFiles);

    settings.endGroup();

}
