#include "setbrushparameterstreewidgetitem.h"

SetBrushParametersTreeWidgetItem::SetBrushParametersTreeWidgetItem(QTreeWidget *parent, const QString &label, const QBrush &defaultBrush) :
    QObject(), QTreeWidgetItem(parent), d_brush(defaultBrush)
{

    this->initialize(label, defaultBrush);

}

SetBrushParametersTreeWidgetItem::SetBrushParametersTreeWidgetItem(QTreeWidgetItem *parent, const QString &label, const QBrush &defaultBrush) :
   QObject(), QTreeWidgetItem(parent), d_brush(defaultBrush)
{

    this->initialize(label, defaultBrush);

}

SetBrushParametersTreeWidgetItem::~SetBrushParametersTreeWidgetItem()
{

}

QBrush SetBrushParametersTreeWidgetItem::currentBrush()
{

    return d_brush;

}

void SetBrushParametersTreeWidgetItem::initialize(const QString &label, const QBrush &brush)
{

    this->setText(0, label);


    d_selectColorWidget = new SelectColorWidget(nullptr, brush.color());

    this->connect(d_selectColorWidget, SIGNAL(colorSelected(QColor)), this, SLOT(setColor(QColor)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Color"}), 1, d_selectColorWidget);



    d_selectBrushStyleWidget = new SelectBrushStyleWidget(nullptr, brush.style());

    this->connect(d_selectBrushStyleWidget, SIGNAL(currentBrushStyleChanged(Qt::BrushStyle)), this, SLOT(setStyle(Qt::BrushStyle)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Style"}), 1, d_selectBrushStyleWidget);

}

void SetBrushParametersTreeWidgetItem::setColor(const QColor &color)
{

    d_brush.setColor(color);

    emit currentBrushChanged(d_brush);

}

void SetBrushParametersTreeWidgetItem::setCurrentBrush(const QBrush &brush)
{

    d_brush = brush;

    d_selectColorWidget->setCurrentColor(brush.color());

    d_selectBrushStyleWidget->setCurrentBrushStyle(brush.style());

    emit currentBrushChanged(d_brush);

}

void SetBrushParametersTreeWidgetItem::setStyle(Qt::BrushStyle style)
{

    d_brush.setStyle(style);

    emit currentBrushChanged(d_brush);

}
