#include "fishersmethod.h"

QStringList FishersMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "-LN( Significance (2-tailed) )", "Sample size"};

QStringList FishersMethod::d_metaAnalysisTestDescriptiveLabels = {"Chi-squared", "Degree of freedom", "Significance (2-tailed)"};

QString FishersMethod::d_nameOfMetaAnalysisTest = "Fisher's method";


FishersMethod::FishersMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

FishersMethod::~FishersMethod()
{

}

QStringList FishersMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList FishersMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> FishersMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << -1.0 * std::log(baseStatisticalTest->pValue()) << baseStatisticalTest->sampleSize();

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> FishersMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_baseStatisticalTests.size() * 2 << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void FishersMethod::calculateStatistic()
{

    double chiSquared = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        chiSquared += -1.0 * std::log(baseStatisticalTest->pValue() / 2.0);

    d_statistic = std::fabs(-2.0 * chiSquared);

}

void FishersMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_baseStatisticalTests.size() * 2.0);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

}

void FishersMethod::_reset()
{

}
