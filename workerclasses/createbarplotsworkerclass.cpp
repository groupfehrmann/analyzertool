#include "createbarplotsworkerclass.h"

CreateBarPlotsWorkerClass::CreateBarPlotsWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CreateBarPlotsWorkerClass::Parameters *>(parameters))
{

}

void CreateBarPlotsWorkerClass::doWork()
{

    emit processStarted("create bar plots");

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Bar plots"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    if (!d_parameters.annotationLabelDefiningAlternativeBarSetLabels.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining alternative bar set labels: \"" + d_parameters.annotationLabelDefiningAlternativeBarSetLabels + "\"");

    if (!d_parameters.annotationLabelDefiningAlternativeBarCategoryLabels.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining alternative bar category labels: \"" + d_parameters.annotationLabelDefiningAlternativeBarCategoryLabels + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "plot mode: " + d_parameters.plotMode);

    if (!d_parameters.exportDirectory.isEmpty()) {

        if (d_parameters.widthOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale width of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "width of plot: " + QString::number(d_parameters.widthOfPlot));

        if (d_parameters.heightOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale height of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "height of plot: " + QString::number(d_parameters.heightOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "dpi of plot: " + QString::number(d_parameters.dpiOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "export type of plot: " + d_parameters.exportTypeOfPlot);

        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    }

    if (!d_parameters.pathToTemplatePlot.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "template plot selected: \"" + d_parameters.pathToTemplatePlot + "\"");

        emit startProgress(0, "Opening template plot", 0, 0);

        bool error = false;

        QString errorMessage;

        OpenPlotFromFile openPlotFromFile(nullptr, d_parameters.pathToTemplatePlot, &error, &errorMessage);

        if (error)
            emit appendLogItem(LogListModel::ERROR, errorMessage);
        else
            d_templatePlot = openPlotFromFile.chart();

        emit stopProgress(0);

    }

    QString fileExtension = d_parameters.exportTypeOfPlot.split(".").at(1);

    fileExtension.remove(")");

    switch (d_parameters.seriesType) {

        case QAbstractSeries::SeriesTypeBar : emit appendLogItem(LogListModel::PARAMETER, "bar series type: vertical bar"); break;

        case QAbstractSeries::SeriesTypeStackedBar : emit appendLogItem(LogListModel::PARAMETER, "bar series type: vertical stacked bar"); break;

        case QAbstractSeries::SeriesTypePercentBar : emit appendLogItem(LogListModel::PARAMETER, "bar series type: vertical percent bar"); break;

        case QAbstractSeries::SeriesTypeHorizontalBar : emit appendLogItem(LogListModel::PARAMETER, "bar series type: horizontal bar"); break;

        case QAbstractSeries::SeriesTypeHorizontalStackedBar : emit appendLogItem(LogListModel::PARAMETER, "bar series type: horizontal stacked bar"); break;

        case QAbstractSeries::SeriesTypeHorizontalPercentBar : emit appendLogItem(LogListModel::PARAMETER, "bar series type: horizontal percent bar"); break;

        default: break;
    }

    if (d_parameters.plotMode == "All bar sets in single plot") {

        emit startProgress(0, "Creating plot", 0, 0);

        QSharedPointer<BarChart> barChart(new BarChart(nullptr, "Bar plot", d_parameters.annotationLabelDefiningAlternativeBarCategoryLabels.isEmpty() ? d_parameters.barCategoryLabels.first : d_parameters.baseDataset->annotations(d_parameters.orientation).values_string(d_parameters.barCategoryLabels.first, d_parameters.annotationLabelDefiningAlternativeBarCategoryLabels), d_parameters.annotationLabelDefiningAlternativeBarSetLabels.isEmpty() ? d_parameters.barSetLabels.first : d_parameters.baseDataset->annotations(d_parameters.orientation).values_string(d_parameters.barSetLabels.first, d_parameters.annotationLabelDefiningAlternativeBarSetLabels), d_parameters.baseDataset->baseMatrix()->vectorsOfDouble(d_parameters.barSetLabels.second, d_parameters.barCategoryLabels.second, d_parameters.orientation), d_parameters.seriesType));

        if (d_templatePlot)
            CopyChartParameters(nullptr, barChart->chart().data(), d_templatePlot.data());

        if (d_parameters.exportDirectory.isEmpty())
            emit resultItemAvailable(new PlotResultItem(nullptr, "Bar plot", barChart->chart()));
        else {

            bool error;

            QString errorMessage;

            ExportPlotToFile(nullptr, barChart->chart().data(), d_parameters.exportDirectory + "/barChart" + "." + fileExtension, d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

            if (error)
                emit appendLogItem(LogListModel::ERROR, errorMessage);

        }

        emit stopProgress(0);

    } else {

        emit startProgress(0, "Creating bar plots", 0, d_parameters.barSetLabels.first.size());

        for (int i = 0; i < d_parameters.barSetLabels.first.size(); ++i) {

            QSharedPointer<BarChart> barChart(new BarChart(nullptr, d_parameters.barSetLabels.first.at(i), d_parameters.annotationLabelDefiningAlternativeBarCategoryLabels.isEmpty() ? d_parameters.barCategoryLabels.first : d_parameters.baseDataset->annotations(d_parameters.orientation).values_string(d_parameters.barCategoryLabels.first, d_parameters.annotationLabelDefiningAlternativeBarCategoryLabels), d_parameters.annotationLabelDefiningAlternativeBarSetLabels.isEmpty() ? d_parameters.barSetLabels.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.barSetLabels.first.at(i), d_parameters.annotationLabelDefiningAlternativeBarSetLabels).toString(), d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.barSetLabels.second.at(i), d_parameters.barCategoryLabels.second, d_parameters.orientation), d_parameters.seriesType));

            if (d_templatePlot)
                CopyChartParameters(nullptr, barChart->chart().data(), d_templatePlot.data());

            if (d_parameters.exportDirectory.isEmpty())
                emit resultItemAvailable(new PlotResultItem(nullptr, d_parameters.barSetLabels.first.at(i), barChart->chart()));
            else {

                bool error;

                QString errorMessage;

                ExportPlotToFile(nullptr, barChart->chart().data(), d_parameters.exportDirectory + "/barChart_" + d_parameters.barSetLabels.first.at(i).simplified() + "." + fileExtension , d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

                if (error)
                    emit appendLogItem(LogListModel::ERROR, errorMessage);

            }

            if (QThread::currentThread()->isInterruptionRequested()) {

                barChart.clear();

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(0, i + 1);

        }

        emit stopProgress(0);

    }

    QThread::currentThread()->quit();

}
