#include "mainwindow.h"

#include <QApplication>
#include <QTextBlock>

#include "math/vectoroperations.h"

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);

    qRegisterMetaType<LogListModel::Type>("LogListModel::Type");

    qRegisterMetaType<BaseMatrix::Orientation>("BaseMatrix::Orientation");

    qRegisterMetaType<QVector<int> >("Vector::int");

    qRegisterMetaType<QVector<unsigned int> >("Vector::uint");

    qRegisterMetaType<QVector<double> >("Vector::double");

    qRegisterMetaType<QVector<QVector<int> > >("VectorOfVector::int");

    qRegisterMetaType<QVector<QVector<unsigned int> > >("VectorOfVector::uint");

    qRegisterMetaType<QVector<QVector<double> > >("VectorOfVector::double");

    qRegisterMetaType<QVector<QVector<QVector<double> > > >("VectorOfVectorOfVector::double");

    qRegisterMetaType<QAbstractSeries::SeriesType>("QAbstractSeries::SeriesType");

    qRegisterMetaType<QTextBlock>("QTextBlock");

    qRegisterMetaTypeStreamOperators<QPair<QStringList, QList<int> > >("pairOfQStringListAndIntList");

    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);

    QCoreApplication::setOrganizationName("Rudolf S.N. Fehrmann");

    QCoreApplication::setOrganizationDomain("www.rudolffehrmann.nl");

    QCoreApplication::setApplicationName("AnalyzerTool 5");

    QCoreApplication::setApplicationVersion("February 22th, 2021");

    MainWindow w;

    w.show();

    return a.exec();

}
