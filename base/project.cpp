#include "project.h"

ProjectData::ProjectData() :
    d_name("New project")
{

}

ProjectData::ProjectData(const ProjectData &other) :
    QSharedData(other), d_uuidToBaseDataset(other.d_uuidToBaseDataset), d_uuidOfBaseDatasets(other.d_uuidOfBaseDatasets), d_name(other.d_name)
{

}

ProjectData::~ProjectData()
{

}

Project::Project(QObject *parent) :
    QObject(parent), d_data(new ProjectData), d_uuid(QUuid::createUuid())
{

}

Project::Project(const Project &other) :
    QObject(other.parent()), d_data(other.d_data), d_uuid(QUuid::createUuid())
{

}

Project::~Project()
{

}

void Project::appendDataset(BaseDataset *baseDataset)
{

    this->insertDataset(this->numberOfDatasets(), baseDataset);

}

const QSharedPointer<BaseDataset> Project::baseDatasetAt(int index) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToBaseDataset.value(d_data->d_uuidOfBaseDatasets.at(index), QSharedPointer<BaseDataset>());

}

const QSharedPointer<BaseDataset> Project::baseDatasetAt(const QUuid &uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToBaseDataset.value(uuid, QSharedPointer<BaseDataset>());

}

void Project::insertDataset(int index, BaseDataset *baseDataset)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    baseDataset->setParent(0);

    this->connect(baseDataset, SIGNAL(nameChanged(QUuid,QString)), this, SIGNAL(datasetNameChanged(QUuid,QString)));

    QSharedPointer<BaseDataset> sharedPointerBaseDataset(baseDataset);

    d_data->d_uuidToBaseDataset.insert(baseDataset->universallyUniqueIdentifier(), sharedPointerBaseDataset);

    d_data->d_uuidOfBaseDatasets.insert(index, baseDataset->universallyUniqueIdentifier());

    baseDataset->setParentProject(this);

    emit this->datasetAdded(sharedPointerBaseDataset);

}

QList<QSharedPointer<BaseDataset> > Project::baseDatasets()
{
    QList<QSharedPointer<BaseDataset> > _baseDatasets;

    for (const QUuid &uuid : d_data->d_uuidOfBaseDatasets)
        _baseDatasets << d_data->d_uuidToBaseDataset.value(uuid);

    return _baseDatasets;

}

const QString &Project::name() const
{

    return d_data->d_name;

}

int Project::numberOfDatasets() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidToBaseDataset.count();

}

void Project::removeAllDatasets()
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidToBaseDataset.clear();

    for (const QUuid &uuid: d_data->d_uuidOfBaseDatasets)
        emit this->datasetRemoved(uuid);

    d_data->d_uuidOfBaseDatasets.clear();

}

void Project::removeDataset(const QUuid &uuid)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidToBaseDataset.remove(uuid);

    d_data->d_uuidOfBaseDatasets.removeOne(uuid);

    emit this->datasetRemoved(uuid);

}

void Project::removeDatasets(const QList<QUuid> &uuids)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    for (const QUuid &uuid: uuids) {

        d_data->d_uuidToBaseDataset.remove(uuid);

        d_data->d_uuidOfBaseDatasets.removeOne(uuid);

        emit this->datasetRemoved(uuid);

    }

}

void Project::removeDataset(int index)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    const QUuid &uuid(d_data->d_uuidOfBaseDatasets.at(index));

    d_data->d_uuidToBaseDataset.remove(uuid);

    d_data->d_uuidOfBaseDatasets.removeAt(index);

    emit this->datasetRemoved(uuid);

}

void Project::removeDatasets(const QList<int> &indexes)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QList<int> sortedIndexes = indexes;

    std::sort(sortedIndexes.begin(), sortedIndexes.end(), std::greater<int>());

    for(const int &sortedIndex: sortedIndexes) {

        const QUuid &uuid(d_data->d_uuidOfBaseDatasets.at(sortedIndex));

        d_data->d_uuidToBaseDataset.remove(uuid);

        d_data->d_uuidOfBaseDatasets.removeAt(sortedIndex);

        emit this->datasetRemoved(uuid);

    }

}

void Project::setName(const QString &name)
{

    d_data->d_name = name;

    emit this->nameChanged(d_uuid, name);

}

const QUuid &Project::universallyUniqueIdentifier() const
{

    return d_uuid;

}

Project &Project::operator=(const Project &other)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    if (this != &other)
        d_data.operator=(other.d_data);

    return *this;

}

QDataStream& operator<<(QDataStream& out, const Project &project)
{

    out << QString("_Project_begin#");

    out << QString("d_uuidOfBaseDatasets") << project.d_data->d_uuidToBaseDataset.size();

    QHashIterator<QUuid, QSharedPointer<BaseDataset> > it(project.d_data->d_uuidToBaseDataset);

    while (it.hasNext())
        out << *it.next().value().data();

    out << QString("d_name") << project.d_data->d_name;

    out << QString("_Project_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, Project &project)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Project_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_uuidOfBaseDatasets")) {

            int numberOfDatasets;

            in >> numberOfDatasets;

            for (int i = 0; i < numberOfDatasets; ++i) {

                BaseDataset *baseDataset = new BaseDataset();

                in >> *baseDataset;

                project.appendDataset(baseDataset);

            }

        } else if (label == QString("d_name"))
            in >> project.d_data->d_name;
        else if (label == QString("_Project_end#"))
            return in;

    }

    return in;

}
