#ifndef EXPORTTOFILEFUNCTIONS_H
#define EXPORTTOFILEFUNCTIONS_H

#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QVector>

namespace ExportToFileFunctions {

    template <typename T>
    bool exportToFile_tabDelimited(const QString &path, const QStringList &columnLabels, const QStringList &rowLabels, QVector<QVector<T> > matrix, QString &errorMessage)
    {

        QFile fileIn(path);

        if (!fileIn.open(QIODevice::WriteOnly | QIODevice::Text)) {

            errorMessage = "could not open file \"" + path + "\" : " + fileIn.errorString();

            return false;

        }

        QTextStream out(&fileIn);

        for (const QString &columnLabel : columnLabels)
            out << "\t" << columnLabel;

        out << Qt::endl;

        for (int i = 0; i < rowLabels.size(); ++i) {

            const QVector<T> &rowVector(matrix.at(i));

            out << rowLabels.at(i);

            for (int j = 0; j < columnLabels.size(); ++j)
                out << "\t" << QVariant(rowVector.at(j)).toString();

            out << Qt::endl;

        }

        return true;

    }

}

#endif // EXPORTTOFILEFUNCTIONS_H
