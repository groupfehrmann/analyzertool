#ifndef IMPORTDATAMATRIXFROMSINGLEFILEDIALOG_H
#define IMPORTDATAMATRIXFROMSINGLEFILEDIALOG_H

#include <QFile>
#include <QList>
#include <QRegExp>

#include "dialogs/basedialog.h"
#include "workerclasses/importdatamatrixfromsinglefileworkerclass.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/selectfilebuildingblockwidget.h"
#include "widgets/selectdelimiterbuildingblockwidget.h"
#include "widgets/selectheaderlabelsbuildingblockwidget.h"
#include "widgets/selectnumberoflinestoreadfromheadandtailoffilebuildingblockwidget.h"

class ImportDatasetFromSingleFileDialog : public BaseDialog
{

    Q_OBJECT

public:

    ImportDatasetFromSingleFileDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString());


    ~ImportDatasetFromSingleFileDialog();

private:

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_datasetDataType;

    SelectFileBuildingBlockWidget *d_selectFileBuildingBlockWidget;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_natureOfFirstLabelInHeader;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_headerLine;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_firstDataLine;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_lastDataLine;

    SelectDelimiterBuildingBlockWidget *d_selectDelimiterBuildingBlockWidget;

    SelectHeaderLabelsBuildingBlockWidget *d_selectHeaderLabelsBuildingBlockWidget;

    SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget *d_selectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget;

    bool d_headerLineHasLabelForRowIdentifiers;

    QStringList d_listOfLineInHead;

    QStringList d_listOfTrimmedLineInHead;

    QStringList d_listOfLineInTail;

    QStringList d_listOfTrimmedLineInTail;

    int d_offSetLineNumber;

    int d_lineNumberOfHeaderLine;

    int d_lineNumberOfFirstDataLine;

    int d_lineNumberOfLastDataLine;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void natureOfFirstLabelInHeaderChanged(int index);

    void firstDataLineChanged(int index);

    void headerLineChanged(int index);

    void lastDataLineChanged(int index);

    void pathChanged(const QString &path);

    void updateHeaderLabelsWidget();

};

#endif // IMPORTDATAMATRIXFROMSINGLEFILEDIALOG_H
