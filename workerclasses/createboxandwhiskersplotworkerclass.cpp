#include "createboxandwhiskersplotworkerclass.h"

CreateBoxAndWhiskersPlotWorkerClass::CreateBoxAndWhiskersPlotWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CreateBoxAndWhiskersPlotWorkerClass::Parameters *>(parameters))
{

}

void CreateBoxAndWhiskersPlotWorkerClass::doWork()
{

    emit processStarted("create box and whiskers plots");

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Box and whiskers plots"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    if (!d_parameters.annotationLabelDefiningSubsets.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining subsets: \"" + d_parameters.annotationLabelDefiningSubsets + "\"");

        emit appendLogItem(LogListModel::PARAMETER, "subset identifiers: " + ConvertFunctions::toString(d_parameters.subsetIdentifiers));

    }

    if (!d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining alternative variable label: " + d_parameters.annotationLabelDefiningAlternativeVariableIdentifier);

    emit appendLogItem(LogListModel::PARAMETER, "plot mode: " + d_parameters.plotMode);

    if (!d_parameters.exportDirectory.isEmpty()) {

        if (d_parameters.widthOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale width of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "width of plot: " + QString::number(d_parameters.widthOfPlot));

        if (d_parameters.heightOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale height of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "height of plot: " + QString::number(d_parameters.heightOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "dpi of plot: " + QString::number(d_parameters.dpiOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "export type of plot: " + d_parameters.exportTypeOfPlot);

        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    }

    if (!d_parameters.pathToTemplatePlot.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "template plot selected: \"" + d_parameters.pathToTemplatePlot + "\"");

        emit startProgress(0, "Opening template plot", 0, 0);

        bool error = false;

        QString errorMessage;

        OpenPlotFromFile openPlotFromFile(nullptr, d_parameters.pathToTemplatePlot, &error, &errorMessage);

        if (error)
            emit appendLogItem(LogListModel::ERROR, errorMessage);
        else
            d_templatePlot = openPlotFromFile.chart();

        emit stopProgress(0);

    }

    emit startProgress(0, "Collecting data", 0, 0);

    QHash<QString, QList<int> > subsetIdentifierToIndexes = d_parameters.baseDataset->annotationValueToIndexes<QString>(d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.annotationLabelDefiningSubsets, QSet<QString>(d_parameters.subsetIdentifiers.begin(), d_parameters.subsetIdentifiers.end()), BaseMatrix::switchOrientation(d_parameters.orientation));

    if (d_parameters.annotationLabelDefiningSubsets.isEmpty())
        d_parameters.subsetIdentifiers << QString();
    else {

        for (int i = d_parameters.subsetIdentifiers.size() - 1; i >=0; --i) {

            if (!subsetIdentifierToIndexes.contains(d_parameters.subsetIdentifiers.at(i)))
                d_parameters.subsetIdentifiers.removeOne(d_parameters.subsetIdentifiers.at(i));

        }

    }

    emit stopProgress(0);

    QString fileExtension = d_parameters.exportTypeOfPlot.split(".").at(1);

    fileExtension.remove(")");

    if (d_parameters.plotMode == "All variables in single plot") {

        emit startProgress(0, "Creating box and whiskers plot", 0, 0);

        QVector<QVector<QVector<double> > > data;

        QStringList variableIdentifiers;

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i) {

            variableIdentifiers << (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString());

            QVector<QVector<double> > subsetVectors;

            for (const QString &subsetIdentifier : d_parameters.subsetIdentifiers)
                subsetVectors << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), subsetIdentifierToIndexes.value(subsetIdentifier), d_parameters.orientation);

            data << subsetVectors;

        }

        QSharedPointer<BoxAndWhiskersChart> boxAndWhiskersChart(new BoxAndWhiskersChart(nullptr, QString(), variableIdentifiers, d_parameters.subsetIdentifiers, data));

        if (d_templatePlot)
            CopyChartParameters(nullptr, boxAndWhiskersChart->chart().data(), d_templatePlot.data());

        if (d_parameters.exportDirectory.isEmpty())
            emit resultItemAvailable(new PlotResultItem(nullptr, "box and whiskers plot", boxAndWhiskersChart->chart()));
        else {

            bool error;

            QString errorMessage;

            ExportPlotToFile(nullptr, boxAndWhiskersChart->chart().data(), d_parameters.exportDirectory + "/boxAndWhiskesPlot." + fileExtension , d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

            if (error)
               emit appendLogItem(LogListModel::ERROR, errorMessage);

        }

        emit stopProgress(0);

    } else {

        emit startProgress(0, "Creating box and whisker plots", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i) {

            QVector<QVector<double> > subsetVectors;

            for (const QString &subsetIdentifier : d_parameters.subsetIdentifiers)
                subsetVectors << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), subsetIdentifierToIndexes.value(subsetIdentifier), d_parameters.orientation);

            QString variableIdentifier = (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString());

            QSharedPointer<BoxAndWhiskersChart> boxAndWhiskersChart(new BoxAndWhiskersChart(nullptr, variableIdentifier, {variableIdentifier}, d_parameters.subsetIdentifiers, QVector<QVector<QVector<double> > >() << subsetVectors));

            if (d_templatePlot)
                CopyChartParameters(nullptr, boxAndWhiskersChart->chart().data(), d_templatePlot.data());

            if (d_parameters.exportDirectory.isEmpty())
                emit resultItemAvailable(new PlotResultItem(nullptr, "box and whiskers plot - " + (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()), boxAndWhiskersChart->chart()));
            else {

                bool error;

                QString errorMessage;

                ExportPlotToFile(nullptr, boxAndWhiskersChart->chart().data(), d_parameters.exportDirectory + "/boxAndWhiskersPlot_" + (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()).simplified() + "." + fileExtension , d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

                if (error)
                    emit appendLogItem(LogListModel::ERROR, errorMessage);

            }

            if (QThread::currentThread()->isInterruptionRequested()) {

                boxAndWhiskersChart.clear();

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(0, i + 1);

        }

        emit stopProgress(0);

    }

    QThread::currentThread()->quit();

}
