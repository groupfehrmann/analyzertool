#include "setmarginsparameterstreewidgetitem.h"

SetMarginsParametersTreeWidgetItem::SetMarginsParametersTreeWidgetItem(QTreeWidget *parent, const QString &label, const QMargins &defaultMargins) :
    QObject(), QTreeWidgetItem(parent)
{

    this->initialize(label, defaultMargins);

}

SetMarginsParametersTreeWidgetItem::SetMarginsParametersTreeWidgetItem(QTreeWidgetItem *parent, const QString &label, const QMargins &defaultMargins) :
   QObject(), QTreeWidgetItem(parent)
{

    this->initialize(label, defaultMargins);

}

SetMarginsParametersTreeWidgetItem::~SetMarginsParametersTreeWidgetItem()
{

}

QMargins SetMarginsParametersTreeWidgetItem::currentMargins()
{

    return d_margins;

}

void SetMarginsParametersTreeWidgetItem::initialize(const QString &label, const QMargins &margins)
{

    this->setText(0, label);


    d_spinBoxBottomMarginWidget = new QSpinBox;

    d_spinBoxBottomMarginWidget->setValue(margins.bottom());

    this->connect(d_spinBoxBottomMarginWidget, SIGNAL(valueChanged(int)), this, SLOT(setBottomMargin(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Bottom"}), 1, d_spinBoxBottomMarginWidget);


    d_spinBoxLeftMarginWidget = new QSpinBox;

    d_spinBoxLeftMarginWidget->setValue(margins.left());

    this->connect(d_spinBoxLeftMarginWidget, SIGNAL(valueChanged(int)), this, SLOT(setLeftMargin(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Left"}), 1, d_spinBoxLeftMarginWidget);


    d_spinBoxRightMarginWidget = new QSpinBox;

    d_spinBoxRightMarginWidget->setValue(margins.right());

    this->connect(d_spinBoxRightMarginWidget, SIGNAL(valueChanged(int)), this, SLOT(setRightMargin(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Right"}), 1, d_spinBoxRightMarginWidget);


    d_spinBoxTopMarginWidget = new QSpinBox;

    d_spinBoxTopMarginWidget->setValue(margins.top());

    this->connect(d_spinBoxTopMarginWidget, SIGNAL(valueChanged(int)), this, SLOT(setTopMargin(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Top"}), 1, d_spinBoxTopMarginWidget);

}


void SetMarginsParametersTreeWidgetItem::setCurrentMargins(const QMargins &margins)
{

    d_spinBoxLeftMarginWidget->setValue(margins.left());

    d_spinBoxTopMarginWidget->setValue(margins.top());

    d_spinBoxRightMarginWidget->setValue(margins.right());

    d_spinBoxBottomMarginWidget->setValue(margins.bottom());

    emit currentMarginsChanged(d_margins);

}

void SetMarginsParametersTreeWidgetItem::setLeftMargin(int leftMargin)
{

    d_margins.setLeft(leftMargin);

    emit currentMarginsChanged(d_margins);

}

void SetMarginsParametersTreeWidgetItem::setTopMargin(int topMargin)
{

    d_margins.setTop(topMargin);

    emit currentMarginsChanged(d_margins);

}

void SetMarginsParametersTreeWidgetItem::setRightMargin(int rightMargin)
{

    d_margins.setRight(rightMargin);

    emit currentMarginsChanged(d_margins);

}

void SetMarginsParametersTreeWidgetItem::setBottomMargin(int bottomMargin)
{

    d_margins.setBottom(bottomMargin);

    emit currentMarginsChanged(d_margins);

}
