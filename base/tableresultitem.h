#ifndef TABLERESULTITEM_H
#define TABLERESULTITEM_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QSharedPointer>

#include "base/baseresultitem.h"

class TableResultItem : public BaseResultItem
{

    Q_OBJECT

public:

    TableResultItem(QObject *parent = nullptr, const QString &name = "Table result item");

    void appendHorizontalHeaderLabel(const QString &label);

    void appendHorizontalHeaderLabels(const QStringList &labels);

    void appendVerticalHeaderLabel(const QString &label);

    void appendVerticalHeaderLabels(const QStringList &labels);

    void appendRow(const QString &verticalHeaderLabel, const QVector<QVariant> &data);

    void appendRow(const QVector<QVariant> &data);

    void appendColumn(const QString &horizontalHeaderLabel, const QVector<QVariant> &data);

    void appendColumn(const QVector<QVariant> &data);

    const QVector<QVector<QVariant> > &data() const;

    const QStringList &horizontalHeaderLabels() const;

    const QStringList &verticalHeaderLabels() const;

    template <typename T>
    void setTableData(const QStringList &horizontalHeaderLabels, const QStringList &verticalHeaderLabels, const QVector<QVector<T> > &data);

private:

    QStringList d_horizontalHeaderLabels;

    QStringList d_verticalHeaderLabels;

    QVector<QVector<QVariant> > d_data;

};

template <typename T>
void TableResultItem::setTableData(const QStringList &horizontalHeaderLabels, const QStringList &verticalHeaderLabels, const QVector<QVector<T> > &data)
{

    d_horizontalHeaderLabels = horizontalHeaderLabels;

    d_verticalHeaderLabels = verticalHeaderLabels;

    for (const QVector<T> &vector : data) {

        QVector<QVariant> convertedVector;

        for (const T &value : vector)
            convertedVector << value;

        d_data << convertedVector;

    }

}

template <>
void TableResultItem::setTableData(const QStringList &horizontalHeaderLabels, const QStringList &verticalHeaderLabels, const QVector<QVector<QVariant> > &data);

class TableResultItemSortFilterProxyModel : public QSortFilterProxyModel
{

    Q_OBJECT

public:

    explicit TableResultItemSortFilterProxyModel(QObject *parent = nullptr);

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

    bool filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const;

};

class TableResultItemModel : public QAbstractTableModel
{

    Q_OBJECT

public:

    TableResultItemModel(QObject *parent = nullptr, const QSharedPointer<TableResultItem> &tableResultItem = QSharedPointer<TableResultItem>());

    int columnCount(QModelIndex const &parent = QModelIndex()) const;

    QVariant data(QModelIndex const &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(QModelIndex const &index) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    int rowCount(QModelIndex const &parent = QModelIndex()) const;

private:

    QSharedPointer<TableResultItem> d_tableResultItem;

};

#endif // TABLERESULTITEM_H
