#include "scatterchart_rankonxaxis.h"

ScatterChart_RankOnXAxis::ScatterChart_RankOnXAxis(QObject *parent, const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis, QChart::ChartType chartType) :
    BaseChart(parent), d_chartType(chartType)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)), SLOT(_initializeChart(QString,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)));

    emit initializeChart(title, subsetIdentifiers, ranks_xAxis, values_yAxis);

    d_eventLoop.exec();

}

ScatterChart_RankOnXAxis::ScatterChart_RankOnXAxis(QObject *parent, const QString &title, const QVector<int> &ranks_xAxis, const QVector<double> &values_yAxis, QChart::ChartType chartType) :
    BaseChart(parent), d_chartType(chartType)
{

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)), SLOT(_initializeChart(QString,QStringList,QVector<QVector<int> >,QVector<QVector<double> >)));

    emit initializeChart(title, {QString()}, QVector<QVector<int> >() << ranks_xAxis, QVector<QVector<double> >() << values_yAxis);

    d_eventLoop.exec();

}

void ScatterChart_RankOnXAxis::_initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<int> > &ranks_xAxis, const QVector<QVector<double> > &values_yAxis)
{
    QChart *chart;

    if (d_chartType == QChart::ChartTypePolar)
        chart = new QPolarChart;
    else
        chart = new QChart;

    chart->setTitle(title);


    QValueAxis *xAxis = new QValueAxis;

    xAxis->setGridLineVisible(false);

    xAxis->setVisible(false);


    QValueAxis *yAxis = new QValueAxis;


    if (d_chartType == QChart::ChartTypePolar) {

        static_cast<QPolarChart *>(chart)->addAxis(xAxis, QPolarChart::PolarOrientationAngular);

        static_cast<QPolarChart *>(chart)->addAxis(yAxis, QPolarChart::PolarOrientationRadial);

    } else {

        chart->addAxis(xAxis, Qt::AlignBottom);

        chart->addAxis(yAxis, Qt::AlignLeft);

    }


    if (values_yAxis.size() == 1)
        chart->legend()->setVisible(false);


    double yMinimumValue = std::numeric_limits<double>::max();

    double yMaximumValue = std::numeric_limits<double>::lowest();


    double xMinimumValue = std::numeric_limits<double>::max();

    double xMaximumValue = std::numeric_limits<double>::lowest();


    for (int i = 0; i < subsetIdentifiers.size(); ++i) {

        QScatterSeries *scatterSeries = new QScatterSeries;

        if (!subsetIdentifiers.at(i).isEmpty())
            scatterSeries->setName(subsetIdentifiers.at(i));


        const QVector<int> &current_ranks_xAxis(ranks_xAxis.at(i));

        const QVector<double> &current_values_yAxis(values_yAxis.at(i));


        for (int j = 0; j < current_ranks_xAxis.size(); ++j) {

            double current_value_yAxis = current_values_yAxis.at(j);

            double current_rank_xAxis = current_ranks_xAxis.at(j);


            scatterSeries->append(current_rank_xAxis, current_value_yAxis);


            yMinimumValue = std::min(yMinimumValue, current_value_yAxis);

            yMaximumValue = std::max(yMaximumValue, current_value_yAxis);

            xMinimumValue = std::min(xMinimumValue, current_rank_xAxis);

            xMaximumValue = std::max(xMaximumValue, current_rank_xAxis);

        }

        chart->addSeries(scatterSeries);

        scatterSeries->attachAxis(xAxis);

        scatterSeries->attachAxis(yAxis);

        scatterSeries->setMarkerSize(10);

        scatterSeries->setBorderColor(Qt::transparent);

        scatterSeries->setOpacity(0.5);

    }

    yAxis->setRange(yMinimumValue, yMaximumValue);

    yAxis->applyNiceNumbers();

    xAxis->setRange(xMinimumValue - 1, xMaximumValue + 1);


    d_chart = QSharedPointer<QChart>(chart);

    emit finishedInitializationOfChart();

}
