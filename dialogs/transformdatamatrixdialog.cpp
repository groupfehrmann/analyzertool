#include "transformdatamatrixdialog.h"

TransformDataMatrixDialog::TransformDataMatrixDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new TransformDataMatrixWorkerClass::Parameters;

    this->setupWidgets();

}

TransformDataMatrixDialog::~TransformDataMatrixDialog()
{

   delete static_cast<TransformDataMatrixWorkerClass::Parameters *>(d_parameters);

}

void TransformDataMatrixDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 2, 0);

    d_comboBoxBuildingBlockWidget_processOnlyValidNumbers = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_processOnlyValidNumbers", "Process valid number only", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_processOnlyValidNumbers, 3, 0);

    d_transformSequenceBuildingBlockWidget = new TransformSequenceBuildingBlockWidget(this, "transformSequenceBuildingBlockWidget");

    d_gridLayout->addWidget(d_transformSequenceBuildingBlockWidget, 4, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

}

void TransformDataMatrixDialog::initializeParameters()
{

    TransformDataMatrixWorkerClass::Parameters *parameters = static_cast<TransformDataMatrixWorkerClass::Parameters *>(d_parameters);

    parameters->baseDataset = d_baseDataset;

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->processOnlyValidNumbers = (d_comboBoxBuildingBlockWidget_processOnlyValidNumbers->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->formatedTransformFunctionList = d_transformSequenceBuildingBlockWidget->formatedTransformFunctionList();

    parameters->functionDescriptionsWithParametersReplacement = d_transformSequenceBuildingBlockWidget->functionDescriptionsWithParametersReplacement();

}

void TransformDataMatrixDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

}
