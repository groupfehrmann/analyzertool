#ifndef CREATEMD5HASHSUMFORFILESWORKERCLASS_H
#define CREATEMD5HASHSUMFORFILESWORKERCLASS_H

#include <QString>
#include <QCryptographicHash>
#include <QHash>
#include <QFile>
#include <QSet>

#include "base/baseworkerclass.h"

class CreateMD5HashSumForFilesWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        bool copyUniqueFilesToExportDirectory;

        QString exportDirectory;

        QString importDirectory;

        QString patternToFilterValidFiles;

    };

    CreateMD5HashSumForFilesWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CreateMD5HashSumForFilesWorkerClass::Parameters d_parameters;

    QStringList getPathsToValidFilesInDirectory(const QString &importDirectory, const QString &patternToFilterValidFiles);

};

#endif // CREATEMD5HASHSUMFORFILESWORKERCLASS_H
