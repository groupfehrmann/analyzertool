#ifndef TRAPZD_H
#define TRAPZD_H

#include <cmath>
#include <functional>

class Trapzd
{

public:

    Trapzd(std::function<double (const double &x)> func, const double &a, const double &b);

    double next();

private:

    std::function<double (const double &x)> d_func;

    double d_a;

    double d_b;

    int d_n;

    double d_s;
};

#endif // TRAPZD_H
