#ifndef SETPENPARAMETERSTREEWIDGETITEM_H
#define SETPENPARAMETERSTREEWIDGETITEM_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QPen>
#include <QSpinBox>

#include "charts/selectpenstylewidget.h"
#include "charts/selectpencapstylewidget.h"
#include "charts/selectpenjoinstylewidget.h"
#include "charts/selectcolorwidget.h"

class SetPenParametersTreeWidgetItem : public QObject, QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetPenParametersTreeWidgetItem(QTreeWidget *parent = nullptr, const QString &label = "Pen", const QPen &defaultPen = QPen());

    explicit SetPenParametersTreeWidgetItem(QTreeWidgetItem *parent = nullptr, const QString &label = "Pen", const QPen &defaultPen = QPen());

    ~SetPenParametersTreeWidgetItem();

    QPen currentPen();

public slots:

    void setCurrentPen(const QPen &pen);

private:

    void initialize(const QString &label, const QPen &initialPen);

    QPen d_pen;

    SelectColorWidget *d_selectColorWidget;

    SelectPenCapStyleWidget *d_selectPenCapStyleWidget;

    SelectPenJoinStyleWidget *d_selectPenJoinStyleWidget;

    SelectPenStyleWidget *d_selectPenStyleWidget;

    QSpinBox *d_spinBoxWidthWidget;

private slots:

    void setCapStyle(Qt::PenCapStyle capStyle);

    void setColor(const QColor &color);

    void setJoinStyle(Qt::PenJoinStyle joinStyle);

    void setStyle(Qt::PenStyle style);

    void setWidth(int width);

signals:

    void currentPenChanged(const QPen &pen);

};

#endif // SETPENPARAMETERSTREEWIDGETITEM_H
