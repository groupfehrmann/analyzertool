#ifndef IMPORTDATASETFROMSINGLEFILEWORKERCLASS_H
#define IMPORTDATASETFROMSINGLEFILEWORKERCLASS_H

#include <QPair>
#include <QString>
#include <QFile>

#include "base/basedataset.h"
#include "base/dataset.h"
#include "base/baseworkerclass.h"
#include "widgets/selectdelimiterbuildingblockwidget.h"

class ImportDatasetFromSingleFileWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    enum FirstLabelInHeaderMode {

        REPRESENTCOLUMNWITHROWIDENTIFIERS = 0,

        REPRESENTFIRSTCOLUMNWITHDATA = 1

    };


    struct Parameters {

        QString datasetDataType;

        QString pathToFile;

        FirstLabelInHeaderMode firstLabelInHeaderMode;

        int lineNumberHeaderLine;

        int lineNumberFirstDataLine;

        int lineNumberLastDataLine;

        SelectDelimiterBuildingBlockWidget::DelimiterType delimiterType;

        QString custumDelimiter;

        Qt::CaseSensitivity caseSensitivityForCustumDelimiter;

        QPair<int, QString> indexAndLabelForColumnDefiningRowIdentifiers;

        QPair<int, QString> indexAndLabelForColumnDefiningFirstDataColumn;

        QPair<int, QString> indexAndLabelForColumnDefiningSecondDataColumn;

    };

    ImportDatasetFromSingleFileWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    ImportDatasetFromSingleFileWorkerClass::Parameters d_parameters;

    BaseDataset *d_baseDataset;

    QFile d_fileIn;

    void cancel();

private slots:

    void updateFileProgress();

};

#endif // IMPORTDATASETFROMSINGLEFILEWORKERCLASS_H
