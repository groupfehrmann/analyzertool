#ifndef HISTOGRAMCHART_H
#define HISTOGRAMCHART_H

#include <QObject>
#include <QApplication>
#include <QChart>
#include <QStringList>
#include <QEventLoop>
#include <QPair>
#include <QBarSet>
#include <QBarSeries>
#include <QCategoryAxis>
#include <QValueAxis>
#include <QBarCategoryAxis>

#include "charts/exportplottofile.h"
#include "math/mathdescriptives.h"
#include "charts/basechart.h"

using namespace QtCharts;

class HistogramChart : public BaseChart
{

    Q_OBJECT

public:

    explicit HistogramChart(QObject *parent = nullptr, const QString &title = QString(), const QStringList &subsetIdentifiers = QStringList(), const QVector<QVector<double> > &valueVectors = QVector<QVector<double> >(), int numberOfBins = 25, const double &lowerBound = std::numeric_limits<double>::lowest(), const double &upperBound = std::numeric_limits<double>::max(), bool scaleToFrequencies = false);

    explicit HistogramChart(QObject *parent = nullptr, const QString &title = QString(), const QVector<double> &valueVector = QVector<double>(), int numberOfBins = 25, const double &lowerBound = std::numeric_limits<double>::lowest(), const double &upperBound = std::numeric_limits<double>::max(), bool scaleToFrequencies = false);

signals:

    void initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &valueVectors, int numberOfBins, const double &lowerBound, const double &upperBound, bool scaleToFrequencies);

private slots:

    void _initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QVector<QVector<double> > &valueVectors, int numberOfBins, const double &lowerBound, const double &upperBound, bool scaleToFrequencies);

};

#endif // HISTOGRAMCHART_H
