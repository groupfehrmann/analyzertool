#include "tableresultitem.h"

TableResultItem::TableResultItem(QObject *parent, const QString &name) :
    BaseResultItem(parent, name)
{

    BaseResultItem::d_data->d_type = BaseResultItem::TABLE;

}

void TableResultItem::appendHorizontalHeaderLabel(const QString &label)
{

    d_horizontalHeaderLabels << label;

}

void TableResultItem::appendHorizontalHeaderLabels(const QStringList &labels)
{

    d_horizontalHeaderLabels << labels;

}

void TableResultItem::appendVerticalHeaderLabel(const QString &label)
{

    d_verticalHeaderLabels << label;

}

void TableResultItem::appendVerticalHeaderLabels(const QStringList &labels)
{

    d_verticalHeaderLabels << labels;

}

void TableResultItem::appendRow(const QString &verticalHeaderLabel, const QVector<QVariant> &data)
{

    d_verticalHeaderLabels << verticalHeaderLabel;

    d_data << data;

}

void TableResultItem::appendRow(const QVector<QVariant> &data)
{

    this->appendRow(QString(), data);

}

void TableResultItem::appendColumn(const QString &horizontalHeaderLabel, const QVector<QVariant> &data)
{

    d_horizontalHeaderLabels << horizontalHeaderLabel;

    for (int i = 0; i < d_data.size(); ++i)
        d_data[i] << data.at(i);

}

void TableResultItem::appendColumn(const QVector<QVariant> &data)
{

    this->appendColumn(QString(), data);

}

const QVector<QVector<QVariant> > &TableResultItem::data() const
{

    return d_data;

}

const QStringList &TableResultItem::horizontalHeaderLabels() const
{

    return d_horizontalHeaderLabels;

}

const QStringList &TableResultItem::verticalHeaderLabels() const
{

    return d_verticalHeaderLabels;

}

template <>
void TableResultItem::setTableData(const QStringList &horizontalHeaderLabels, const QStringList &verticalHeaderLabels, const QVector<QVector<QVariant> > &data)
{

    d_horizontalHeaderLabels = horizontalHeaderLabels;

    d_verticalHeaderLabels = verticalHeaderLabels;

    d_data = data;

}

TableResultItemSortFilterProxyModel::TableResultItemSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{

}

bool TableResultItemSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    Q_UNUSED(sourceRow);

    return true;

}

bool TableResultItemSortFilterProxyModel::filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    Q_UNUSED(sourceColumn);

    return true;

}

TableResultItemModel::TableResultItemModel(QObject *parent, const QSharedPointer<TableResultItem> &tableResultItem) :
    QAbstractTableModel(parent), d_tableResultItem(tableResultItem)
{

}

int TableResultItemModel::columnCount(QModelIndex const &parent) const
{

    Q_UNUSED(parent);

    return d_tableResultItem->horizontalHeaderLabels().size();

}

QVariant TableResultItemModel::data(QModelIndex const &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    switch (role) {

        case Qt::DisplayRole:
            return QVariant(d_tableResultItem->data().at(index.row()).at(index.column()));
        case Qt::StatusTipRole:
            return "row index " + QString::number(index.row()) + ", column index " + QString::number(index.column())  + " - \"" + d_tableResultItem->verticalHeaderLabels().at(index.row()) + "\", \"" + d_tableResultItem->horizontalHeaderLabels().at(index.column()) + "\"";

        default:
            return QVariant();

    }

}

Qt::ItemFlags TableResultItemModel::flags(QModelIndex const &index) const
{

    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);

}

QVariant TableResultItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    switch (role) {

        case Qt::DisplayRole:
            return (orientation == Qt::Vertical) ? d_tableResultItem->verticalHeaderLabels().at(section) : d_tableResultItem->horizontalHeaderLabels().at(section);

        case Qt::EditRole:
            return (orientation == Qt::Vertical) ? d_tableResultItem->verticalHeaderLabels().at(section) : d_tableResultItem->horizontalHeaderLabels().at(section);

        case Qt::StatusTipRole:
            return "index " + QString::number(section);

        default:
            return QVariant();

    }

}

int TableResultItemModel::rowCount(QModelIndex const &parent) const
{

    Q_UNUSED(parent);

    return d_tableResultItem->verticalHeaderLabels().size();

}
