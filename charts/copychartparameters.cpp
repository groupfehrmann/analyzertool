#include "copychartparameters.h"

void CopyParameterSettingsFunctions::copyParameterSettings(QAbstractAxis &abstractAxisTo, const QAbstractAxis &abstractAxisFrom) {

    abstractAxisTo.setGridLinePen(abstractAxisFrom.gridLinePen());

    abstractAxisTo.setGridLineVisible(abstractAxisFrom.isGridLineVisible());

    abstractAxisTo.setLabelsAngle(abstractAxisFrom.labelsAngle());

    abstractAxisTo.setLabelsBrush(abstractAxisFrom.labelsBrush());

    abstractAxisTo.setLabelsFont(abstractAxisFrom.labelsFont());

    abstractAxisTo.setLabelsVisible(abstractAxisFrom.labelsVisible());

    abstractAxisTo.setLinePen(abstractAxisFrom.linePen());

    abstractAxisTo.setLineVisible(abstractAxisFrom.isLineVisible());

    abstractAxisTo.setMinorGridLinePen(abstractAxisFrom.minorGridLinePen());

    abstractAxisTo.setMinorGridLineVisible(abstractAxisFrom.isMinorGridLineVisible());

    abstractAxisTo.setReverse(abstractAxisFrom.isReverse());

    abstractAxisTo.setShadesBrush(abstractAxisFrom.shadesBrush());

    abstractAxisTo.setShadesPen(abstractAxisFrom.shadesPen());

    abstractAxisTo.setShadesVisible(abstractAxisFrom.shadesVisible());

    abstractAxisTo.setTitleBrush(abstractAxisFrom.titleBrush());

    abstractAxisTo.setTitleFont(abstractAxisFrom.titleFont());

    abstractAxisTo.setTitleText(abstractAxisFrom.titleText());

    abstractAxisTo.setTitleVisible(abstractAxisFrom.isTitleVisible());

    abstractAxisTo.setVisible(abstractAxisFrom.isVisible());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QBarCategoryAxis &barCategoryAxisTo, const QBarCategoryAxis &barCategoryAxisFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractAxis &>(barCategoryAxisTo), static_cast<const QAbstractAxis &>(barCategoryAxisFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QDateTimeAxis &dateTimeAxisTo, const QDateTimeAxis &dateTimeAxisFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractAxis &>(dateTimeAxisTo), static_cast<const QAbstractAxis &>(dateTimeAxisFrom));

    dateTimeAxisTo.setFormat(dateTimeAxisFrom.format());

    dateTimeAxisTo.setMax(dateTimeAxisFrom.max());

    dateTimeAxisTo.setMin(dateTimeAxisFrom.min());

    dateTimeAxisTo.setTickCount(dateTimeAxisFrom.tickCount());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QLogValueAxis &logValueAxisTo, const QLogValueAxis &logValueAxisFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractAxis &>(logValueAxisTo), static_cast<const QAbstractAxis &>(logValueAxisFrom));

    logValueAxisTo.setBase(logValueAxisFrom.base());

    logValueAxisTo.setLabelFormat(logValueAxisFrom.labelFormat());

    logValueAxisTo.setMax(logValueAxisFrom.max());

    logValueAxisTo.setMin(logValueAxisFrom.min());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QValueAxis &valueAxisTo, const QValueAxis &valueAxisFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractAxis &>(valueAxisTo), static_cast<const QAbstractAxis &>(valueAxisFrom));

    valueAxisTo.setLabelFormat(valueAxisTo.labelFormat());

    valueAxisTo.setMax(valueAxisFrom.max());

    valueAxisTo.setMin(valueAxisFrom.min());

    valueAxisTo.setMinorTickCount(valueAxisFrom.minorTickCount());

    valueAxisTo.setTickCount(valueAxisFrom.tickCount());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QCategoryAxis &categoryAxisTo, const QCategoryAxis &categoryAxisFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QValueAxis &>(categoryAxisTo), static_cast<const QValueAxis &>(categoryAxisFrom));

    categoryAxisTo.setLabelsPosition(categoryAxisFrom.labelsPosition());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QAbstractSeries &abstractSeriesTo, const QAbstractSeries &abstractSeriesFrom) {

    abstractSeriesTo.setOpacity(abstractSeriesFrom.opacity());

    abstractSeriesTo.setUseOpenGL(abstractSeriesTo.useOpenGL());

    abstractSeriesTo.setVisible(abstractSeriesFrom.isVisible());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QAbstractBarSeries &abstractBarSeriesTo, const QAbstractBarSeries &abstractBarSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractSeries &>(abstractBarSeriesTo), static_cast<const QAbstractSeries &>(abstractBarSeriesFrom));

    for (int i = 0; i < abstractBarSeriesFrom.count(); ++i) {

        if (i < abstractBarSeriesTo.count())
            CopyParameterSettingsFunctions::copyParameterSettings(*abstractBarSeriesTo.barSets()[i], *abstractBarSeriesFrom.barSets().at(i));

    }

    abstractBarSeriesTo.setBarWidth(abstractBarSeriesFrom.barWidth());

    abstractBarSeriesTo.setLabelsAngle(abstractBarSeriesFrom.labelsAngle());

    abstractBarSeriesTo.setLabelsFormat(abstractBarSeriesFrom.labelsFormat());

    abstractBarSeriesTo.setLabelsPosition(abstractBarSeriesFrom.labelsPosition());

    abstractBarSeriesTo.setLabelsVisible(abstractBarSeriesFrom.isLabelsVisible());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QBarSeries &barSeriesTo, const QBarSeries &barSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractBarSeries &>(barSeriesTo), static_cast<const QAbstractBarSeries &>(barSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QHorizontalBarSeries &horizontalBarSeriesTo, const QHorizontalBarSeries &horizontalBarSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractBarSeries &>(horizontalBarSeriesTo), static_cast<const QAbstractBarSeries &>(horizontalBarSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QHorizontalPercentBarSeries &horizontalPercentBarSeriesTo, const QHorizontalPercentBarSeries &horizontalPercentBarSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractBarSeries &>(horizontalPercentBarSeriesTo), static_cast<const QAbstractBarSeries &>(horizontalPercentBarSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QHorizontalStackedBarSeries &horizontalStackedBarSeriesTo, const QHorizontalStackedBarSeries &horizontalStackedBarSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractBarSeries &>(horizontalStackedBarSeriesTo), static_cast<const QAbstractBarSeries &>(horizontalStackedBarSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QPercentBarSeries &percentBarSeriesTo, const QPercentBarSeries &percentBarSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractBarSeries &>(percentBarSeriesTo), static_cast<const QAbstractBarSeries &>(percentBarSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QStackedBarSeries &stackedBarSeriesTo, const QStackedBarSeries &stackedBarSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractBarSeries &>(stackedBarSeriesTo), static_cast<const QAbstractBarSeries &>(stackedBarSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QAreaSeries &areaSeriesTo, const QAreaSeries &areaSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractSeries &>(areaSeriesTo), static_cast<const QAbstractSeries &>(areaSeriesTo));

    areaSeriesTo.setBrush(areaSeriesFrom.brush());

    if (areaSeriesTo.lowerSeries() && areaSeriesFrom.lowerSeries())
        CopyParameterSettingsFunctions::copyParameterSettings(*areaSeriesTo.lowerSeries(), *areaSeriesFrom.lowerSeries());

    areaSeriesTo.setPen(areaSeriesFrom.pen());

    areaSeriesTo.setPointLabelsClipping(areaSeriesFrom.pointLabelsClipping());

    areaSeriesTo.setPointLabelsFont(areaSeriesFrom.pointLabelsFont());

    areaSeriesTo.setPointLabelsFormat(areaSeriesFrom.pointLabelsFormat());

    areaSeriesTo.setPointLabelsVisible(areaSeriesFrom.pointLabelsVisible());

    areaSeriesTo.setPointsVisible(areaSeriesFrom.pointsVisible());

    if (areaSeriesTo.upperSeries() && areaSeriesFrom.upperSeries())
        CopyParameterSettingsFunctions::copyParameterSettings(*areaSeriesTo.upperSeries(), *areaSeriesFrom.upperSeries());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QBoxPlotSeries &boxPlotSeriesTo, const QBoxPlotSeries &boxPlotSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractSeries &>(boxPlotSeriesTo), static_cast<const QAbstractSeries &>(boxPlotSeriesFrom));

    for (int i = 0; i < boxPlotSeriesFrom.count(); ++i) {

        if (i < boxPlotSeriesTo.count())
            CopyParameterSettingsFunctions::copyParameterSettings(*boxPlotSeriesTo.boxSets()[i], *boxPlotSeriesFrom.boxSets().at(i));

    }

    boxPlotSeriesTo.setBoxOutlineVisible(const_cast<QBoxPlotSeries &>(boxPlotSeriesFrom).boxOutlineVisible());

    boxPlotSeriesTo.setBoxWidth(const_cast<QBoxPlotSeries &>(boxPlotSeriesFrom).boxWidth());

    boxPlotSeriesTo.setBrush(boxPlotSeriesFrom.brush());

    boxPlotSeriesTo.setPen(boxPlotSeriesFrom.pen());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QPieSeries &pieSeriesTo, const QPieSeries &pieSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractSeries &>(pieSeriesTo), static_cast<const QAbstractSeries &>(pieSeriesFrom));

    pieSeriesTo.setHoleSize(pieSeriesFrom.holeSize());

    pieSeriesTo.setPieEndAngle(pieSeriesFrom.pieEndAngle());

    pieSeriesTo.setPieSize(pieSeriesFrom.pieSize());

    pieSeriesTo.setPieStartAngle(pieSeriesFrom.pieStartAngle());

    for (int i = 0; i < pieSeriesFrom.count(); ++i) {

        if (i < pieSeriesTo.count())
            CopyParameterSettingsFunctions::copyParameterSettings(*pieSeriesTo.slices()[i], *pieSeriesFrom.slices().at(i));

    }

}

void CopyParameterSettingsFunctions::copyParameterSettings(QXYSeries &xySeriesTo, const QXYSeries &xySeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QAbstractSeries &>(xySeriesTo), static_cast<const QAbstractSeries &>(xySeriesFrom));

    xySeriesTo.setBrush(xySeriesFrom.brush());

    xySeriesTo.setPen(xySeriesFrom.pen());

    xySeriesTo.setPointLabelsClipping(xySeriesFrom.pointLabelsClipping());

    xySeriesTo.setPointLabelsFont(xySeriesFrom.pointLabelsFont());

    xySeriesTo.setPointLabelsFont(xySeriesFrom.pointLabelsFormat());

    xySeriesTo.setPointLabelsFormat(xySeriesFrom.pointLabelsFormat());

    xySeriesTo.setPointLabelsVisible(xySeriesFrom.pointLabelsVisible());

    xySeriesTo.setPointsVisible(xySeriesFrom.pointsVisible());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QLineSeries &lineSeriesTo, const QLineSeries &lineSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QXYSeries &>(lineSeriesTo), static_cast<const QXYSeries &>(lineSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QSplineSeries &splineSeriesTo, const QSplineSeries &splineSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QLineSeries &>(splineSeriesTo), static_cast<const QLineSeries &>(splineSeriesFrom));

}

void CopyParameterSettingsFunctions::copyParameterSettings(QScatterSeries &scatterSeriesTo, const QScatterSeries &scatterSeriesFrom) {

    CopyParameterSettingsFunctions::copyParameterSettings(static_cast<QXYSeries &>(scatterSeriesTo), static_cast<const QXYSeries &>(scatterSeriesFrom));

    scatterSeriesTo.setMarkerShape(scatterSeriesFrom.markerShape());

    scatterSeriesTo.setMarkerSize(scatterSeriesFrom.markerSize());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QCandlestickSeries &candlestickSeriesTo, const QCandlestickSeries &candlestickSeriesFrom)
{

    Q_UNUSED(candlestickSeriesTo);

    Q_UNUSED(candlestickSeriesFrom);

}

void CopyParameterSettingsFunctions::copyParameterSettings(QBarSet &barSetTo, const QBarSet &barSetFrom) {

    barSetTo.setBrush(barSetFrom.brush());

    barSetTo.setLabel(barSetFrom.label());

    barSetTo.setLabelColor(const_cast<QBarSet &>(barSetFrom).labelColor());

    barSetTo.setLabelFont(barSetFrom.labelFont());

    barSetTo.setPen(barSetFrom.pen());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QBoxSet &boxSetTo, const QBoxSet &boxSetFrom) {

    boxSetTo.setBrush(boxSetFrom.brush());

    boxSetTo.setLabel(boxSetFrom.label());

    boxSetTo.setPen(boxSetFrom.pen());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QPieSlice &pieSliceTo, const QPieSlice &pieSliceFrom) {

    pieSliceTo.setBorderWidth(const_cast<QPieSlice &>(pieSliceFrom).borderWidth());

    pieSliceTo.setBrush(pieSliceFrom.brush());

    pieSliceTo.setExplodeDistanceFactor(pieSliceFrom.explodeDistanceFactor());

    pieSliceTo.setExploded(pieSliceFrom.isExploded());

    pieSliceTo.setLabel(pieSliceFrom.label());

    pieSliceTo.setLabelArmLengthFactor(pieSliceFrom.labelArmLengthFactor());

    pieSliceTo.setLabelBrush(pieSliceFrom.labelBrush());

    pieSliceTo.setLabelFont(pieSliceFrom.labelFont());

    pieSliceTo.setLabelPosition(const_cast<QPieSlice &>(pieSliceFrom).labelPosition());

    pieSliceTo.setLabelVisible(pieSliceFrom.isLabelVisible());

    pieSliceTo.setPen(pieSliceFrom.pen());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QChart &chartTo, const QChart &chartFrom) {

    chartTo.setAnimationDuration(chartFrom.animationDuration());

    chartTo.setAnimationEasingCurve(chartFrom.animationEasingCurve());

    chartTo.setAnimationOptions(chartFrom.animationOptions());

    for (int i = 0; i < chartFrom.axes().count(); ++i) {

        if (i < chartTo.axes().count()) {

            switch(chartFrom.axes().at(i)->type()) {

                case QAbstractAxis::AxisTypeValue : if (chartTo.axes().at(i)->type() == QAbstractAxis::AxisTypeValue) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QValueAxis *>(chartTo.axes()[i]), *static_cast<const QValueAxis *>(chartFrom.axes().at(i))); break;

                case QAbstractAxis::AxisTypeBarCategory : if (chartTo.axes().at(i)->type() == QAbstractAxis::AxisTypeBarCategory) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QBarCategoryAxis *>(chartTo.axes()[i]), *static_cast<const QBarCategoryAxis *>(chartFrom.axes().at(i))); break;

                case QAbstractAxis::AxisTypeCategory : if (chartTo.axes().at(i)->type() == QAbstractAxis::AxisTypeCategory) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QCategoryAxis *>(chartTo.axes()[i]), *static_cast<const QCategoryAxis *>(chartFrom.axes().at(i))); break;

                case QAbstractAxis::AxisTypeDateTime : if (chartTo.axes().at(i)->type() == QAbstractAxis::AxisTypeDateTime) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QDateTimeAxis *>(chartTo.axes()[i]), *static_cast<const QDateTimeAxis *>(chartFrom.axes().at(i))); break;

                case QAbstractAxis::AxisTypeLogValue : if (chartTo.axes().at(i)->type() == QAbstractAxis::AxisTypeLogValue) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QLogValueAxis *>(chartTo.axes()[i]), *static_cast<const QLogValueAxis *>(chartFrom.axes().at(i))); break;

                case QAbstractAxis::AxisTypeNoAxis : break;

            }

        }

    }

    chartTo.setBackgroundBrush(chartFrom.backgroundBrush());

    chartTo.setBackgroundPen(chartFrom.backgroundPen());

    chartTo.setBackgroundRoundness(chartFrom.backgroundRoundness());

    chartTo.setBackgroundVisible(chartFrom.isBackgroundVisible());

    chartTo.setDropShadowEnabled(chartFrom.isDropShadowEnabled());

    CopyParameterSettingsFunctions::copyParameterSettings(*chartTo.legend(), *chartFrom.legend());

    chartTo.setMargins(chartFrom.margins());

    chartTo.setPlotAreaBackgroundBrush(chartFrom.plotAreaBackgroundBrush());

    chartTo.setPlotAreaBackgroundPen(chartFrom.plotAreaBackgroundPen());

    for (int i = 0; i < chartFrom.series().count(); ++i) {

        if (i < chartTo.series().count()) {

            switch(chartFrom.series().at(i)->type()) {

                case QAbstractSeries::SeriesTypeLine : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeLine) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QLineSeries *>(chartTo.series()[i]), *static_cast<const QLineSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeArea : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeArea) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QAreaSeries *>(chartTo.series()[i]), *static_cast<const QAreaSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeBar : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeBar) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QBarSeries *>(chartTo.series()[i]), *static_cast<const QBarSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeStackedBar : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeStackedBar) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QStackedBarSeries *>(chartTo.series()[i]), *static_cast<const QStackedBarSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypePercentBar : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypePercentBar) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QPercentBarSeries *>(chartTo.series()[i]), *static_cast<const QPercentBarSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypePie : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypePie) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QPieSeries *>(chartTo.series()[i]), *static_cast<const QPieSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeScatter : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeScatter) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QScatterSeries *>(chartTo.series()[i]), *static_cast<const QScatterSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeSpline : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeSpline) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QSplineSeries *>(chartTo.series()[i]), *static_cast<const QSplineSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeHorizontalBar : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeHorizontalBar) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QHorizontalBarSeries *>(chartTo.series()[i]), *static_cast<const QHorizontalBarSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeHorizontalStackedBar : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeHorizontalStackedBar) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QHorizontalStackedBarSeries *>(chartTo.series()[i]), *static_cast<const QHorizontalStackedBarSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeHorizontalPercentBar : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeHorizontalPercentBar) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QHorizontalPercentBarSeries *>(chartTo.series()[i]), *static_cast<const QHorizontalPercentBarSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeBoxPlot : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeBoxPlot) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QBoxPlotSeries *>(chartTo.series()[i]), *static_cast<const QBoxPlotSeries *>(chartFrom.series().at(i))); break;

                case QAbstractSeries::SeriesTypeCandlestick : if (chartTo.series().at(i)->type() == QAbstractSeries::SeriesTypeBoxPlot) CopyParameterSettingsFunctions::copyParameterSettings(*static_cast<QCandlestickSeries *>(chartTo.series()[i]), *static_cast<const QCandlestickSeries *>(chartFrom.series().at(i))); break;

            }

        }

    }

    chartTo.setTitleFont(chartFrom.titleFont());

    chartTo.setTitleBrush(chartFrom.titleBrush());

}

void CopyParameterSettingsFunctions::copyParameterSettings(QLegend &legendTo, const QLegend &legendFrom) {

    legendTo.setAlignment(legendFrom.alignment());

    legendTo.setBackgroundVisible(legendFrom.isBackgroundVisible());

    legendTo.setBrush(legendFrom.brush());

    legendTo.setFont(legendFrom.font());

    legendTo.setBorderColor(const_cast<QLegend &>(legendFrom).borderColor());

    legendTo.setPen(legendFrom.pen());

    legendTo.setReverseMarkers(const_cast<QLegend &>(legendFrom).reverseMarkers());

    legendTo.setShowToolTips(legendFrom.showToolTips());

    legendTo.setVisible(legendFrom.isVisible());

}

CopyChartParameters::CopyChartParameters(QObject *parent, QChart *chartTo, QChart *chartFrom) :
    QObject(parent)
{

    this->moveToThread(QApplication::instance()->thread());

    this->connect(this, SIGNAL(startCopy(QChart*,QChart*)), this, SLOT(_startCopy(QChart*,QChart*)));

    this->connect(this, SIGNAL(finishedCopy()), &d_eventLoop, SLOT(quit()));

    emit startCopy(chartTo, chartFrom);

    d_eventLoop.exec();

}

void CopyChartParameters::_startCopy(QChart *chartTo, QChart *chartFrom)
{

    CopyParameterSettingsFunctions::copyParameterSettings(*chartTo, *chartFrom);

    emit finishedCopy();

}
