#include "basedataset.h"

BaseDatasetData::BaseDatasetData() :
    d_name("New dataset"), d_parentProject(nullptr)
{

}

BaseDatasetData::BaseDatasetData(const BaseDatasetData &other) :
    QSharedData(other), d_verticalHeader(other.d_verticalHeader), d_horizontalHeader(other.d_horizontalHeader), d_baseMatrix(other.d_baseMatrix), d_columnAnnotations(other.d_columnAnnotations), d_rowAnnotations(other.d_rowAnnotations), d_name(other.d_name), d_parentProject(other.d_parentProject)
{

}

BaseDatasetData::~BaseDatasetData()
{

}

BaseDataset::BaseDataset(QObject *parent) :
    QObject(parent), d_data(new BaseDatasetData), d_uuid(QUuid::createUuid())
{

}

BaseDataset::BaseDataset(const BaseDataset &other) :
    QObject(other.parent()), d_data(other.d_data), d_uuid(QUuid::createUuid())
{

}

BaseDataset::~BaseDataset()
{

}

void BaseDataset::appendVector(const QString &identifier, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).append(identifier);

    d_data->d_baseMatrix->appendVector(orientation);

}

void BaseDataset::appendVectors(const QStringList &identifiers, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).append(identifiers);

    d_data->d_baseMatrix->appendVectors(identifiers.size(), orientation);

}

Annotations &BaseDataset::annotations(BaseMatrix::Orientation orientation)
{

    return (orientation == BaseMatrix::ROW) ? d_data->d_rowAnnotations : d_data->d_columnAnnotations;

}

const Annotations &BaseDataset::annotations(BaseMatrix::Orientation orientation) const
{

    return (orientation == BaseMatrix::ROW) ? d_data->d_rowAnnotations : d_data->d_columnAnnotations;

}

BaseMatrix *BaseDataset::baseMatrix()
{

    return d_data->d_baseMatrix;

}

const BaseMatrix *BaseDataset::baseMatrix() const
{

    return d_data->d_baseMatrix;

}

const Header &BaseDataset::header(BaseMatrix::Orientation orientation) const
{

    return (orientation == BaseMatrix::ROW) ? d_data->d_verticalHeader : d_data->d_horizontalHeader;

}

Header &BaseDataset::header(BaseMatrix::Orientation orientation)
{

    return (orientation == BaseMatrix::ROW) ? d_data->d_verticalHeader : d_data->d_horizontalHeader;

}

template <>
QMap<int, QVariant> BaseDataset::indexToAnnotationValue(const QString &annotationLabel, const QVariant &annotationValue, BaseMatrix::Orientation orientation, bool selectedOnly) const {

    const Header &header(this->header(orientation));

    const Annotations &annotations(this->annotations(orientation));

    QMap<int, QVariant> _indexToValue;

    for (const int &index: header.indexes(selectedOnly)) {

        if (annotations.value(header.identifier(index), annotationLabel) == annotationValue)
            _indexToValue.insert(index, annotationValue);

    }

    return _indexToValue;

}

const QVariant BaseDataset::annotationValue(int index, const QString &label, BaseMatrix::Orientation orientation) const
{

    return this->annotations(orientation).value(this->header(orientation).identifier(index), label);

}

const QVariant BaseDataset::annotationValue(int indexOfIdentifier, int indexOfAnnotationLabel, BaseMatrix::Orientation orientation) const
{

    return this->annotations(orientation).value(this->header(orientation).identifier(indexOfIdentifier), this->annotations(orientation).label(indexOfAnnotationLabel));

}

QList<QVariant> BaseDataset::annotationValues(int index, const QStringList &annotationLabels, BaseMatrix::Orientation orientation) const
{

    return this->annotations(orientation).values(this->header(orientation).identifier(index), annotationLabels);

}

QList<QVariant> BaseDataset::annotationValues(const QList<int> &indexes, const QString &annotationLabel, BaseMatrix::Orientation orientation) const
{

    return this->annotations(orientation).values(this->header(orientation).identifiers(indexes), annotationLabel);

}

void BaseDataset::insertVector(int index, const QString &identifier, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).insert(index, identifier);

    d_data->d_baseMatrix->insertVector(index, orientation);

}

void BaseDataset::insertVectors(int index, const QStringList &identifiers, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).insert(index, identifiers);

    d_data->d_baseMatrix->insertVectors(index, identifiers.size(), orientation);

}

const QString &BaseDataset::name() const
{

    return d_data->d_name;

}

const Project *BaseDataset::parentProject() const
{

    return d_data->d_parentProject;

}

void BaseDataset::prependVector(const QString &identifier, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).prepend(identifier);

    d_data->d_baseMatrix->prependVector(orientation);

}

void BaseDataset::prependVectors(const QStringList &identifiers, BaseMatrix::Orientation orientation)
{

    this->BaseDataset::header(orientation).prepend(identifiers);

    d_data->d_baseMatrix->prependVectors(identifiers.size(), orientation);

}

void BaseDataset::removeVector(int index, BaseMatrix::Orientation orientation)
{

    Header &header(this->header(orientation));

    const QString &identifier(header.identifier(index));

    header.remove(index);

    d_data->d_baseMatrix->removeVector(index, orientation);

    if (!header.contains(identifier))
        this->annotations(orientation).removeIdentifier(identifier);

}

void BaseDataset::removeVectors(const QList<int> &indexes, BaseMatrix::Orientation orientation)
{

    Header &header(this->header(orientation));

    Annotations &annotations(this->annotations(orientation));

    const QStringList &identifiers(header.identifiers(indexes));

    header.remove(indexes);

    d_data->d_baseMatrix->removeVectors(indexes, orientation);

    for (const QString &identifier: identifiers) {

        if (!header.contains(identifier))
            annotations.removeIdentifier(identifier);

    }

}

void BaseDataset::removeVectors(const QString &identifier, BaseMatrix::Orientation orientation, bool selectedOnly)
{

    Header &header(this->header(orientation));

    d_data->d_baseMatrix->removeVectors(header.indexes(identifier, selectedOnly), orientation);

    header.remove(identifier, selectedOnly);

    if (!header.contains(identifier))
        this->annotations(orientation).removeIdentifier(identifier);

}

void BaseDataset::removeVectors(const QStringList &identifiers, BaseMatrix::Orientation orientation, bool selectedOnly)
{

    Header &header(this->header(orientation));

    Annotations &annotations(this->annotations(orientation));

    d_data->d_baseMatrix->removeVectors(this->header(orientation).indexesInContinuousList(identifiers, selectedOnly), orientation);

    header.remove(identifiers, selectedOnly);

    for (const QString &identifier: identifiers) {

        if (!header.contains(identifier))
            annotations.removeIdentifier(identifier);

    }

}

void BaseDataset::setName(const QString &name)
{

    d_data->d_name = name;

    emit this->nameChanged(d_uuid, name);

}

void BaseDataset::setParentProject(Project *parentProject)
{

    d_data->d_parentProject = parentProject;

}

void BaseDataset::transpose()
{

    std::swap(d_data->d_horizontalHeader, d_data->d_verticalHeader);

    std::swap(d_data->d_rowAnnotations, d_data->d_columnAnnotations);

    d_data->d_baseMatrix->transpose();

}

void BaseDataset::swap(int index1, int index2, BaseMatrix::Orientation orientation, bool update)
{

    this->BaseDataset::header(orientation).swap(index1, index2, update);

    d_data->d_baseMatrix->swap(index1, index2, orientation);

}

const QUuid &BaseDataset::universallyUniqueIdentifier() const
{

    return d_uuid;

}

BaseDataset &BaseDataset::operator=(const BaseDataset &other)
{

    if (this != &other)
        d_data.operator=(other.d_data);

    return *this;

}

QDataStream& operator<<(QDataStream& out, BaseDataset &baseDataset)
{

    out << QString("_Dataset_begin#");

    out << QString("d_baseMatrix") << baseDataset.d_data->d_baseMatrix->metaTypeIdentifier() << *baseDataset.d_data->d_baseMatrix;

    out << QString("d_verticalHeader") << baseDataset.d_data->d_verticalHeader;

    out << QString("d_horizontalHeader") << baseDataset.d_data->d_horizontalHeader;

    out << QString("d_columnAnnotations") << baseDataset.d_data->d_columnAnnotations;

    out << QString("d_rowAnnotations") << baseDataset.d_data->d_rowAnnotations;

    out << QString("d_name") << baseDataset.d_data->d_name;

    out << QString("_Dataset_end#");

    return out;

}

QDataStream& operator>>(QDataStream& in, BaseDataset &baseDataset)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Dataset_begin#")) {

        in.device()->seek(pos);

        return in;

    }

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_baseMatrix")) {

            int metaTypeIdentifier;

            in >> metaTypeIdentifier;

            switch (metaTypeIdentifier) {

                case QMetaType::Short: baseDataset.d_data->d_baseMatrix = new Matrix<short>(); break;

                case QMetaType::UShort: baseDataset.d_data->d_baseMatrix = new Matrix<unsigned short>(); break;

                case QMetaType::Int: baseDataset.d_data->d_baseMatrix = new Matrix<int>(); break;

                case QMetaType::UInt: baseDataset.d_data->d_baseMatrix = new Matrix<unsigned int>(); break;

                case QMetaType::LongLong: baseDataset.d_data->d_baseMatrix = new Matrix<long long>(); break;

                case QMetaType::ULongLong: baseDataset.d_data->d_baseMatrix = new Matrix<unsigned long long>(); break;

                case QMetaType::Float: baseDataset.d_data->d_baseMatrix = new Matrix<float>(); break;

                case QMetaType::Double: baseDataset.d_data->d_baseMatrix = new Matrix<double>(); break;

                case QMetaType::UChar: baseDataset.d_data->d_baseMatrix = new Matrix<unsigned char>(); break;

                case QMetaType::QChar: baseDataset.d_data->d_baseMatrix = new Matrix<QChar>(); break;

                case QMetaType::QString: baseDataset.d_data->d_baseMatrix = new Matrix<QString>(); break;

                case QMetaType::Bool: baseDataset.d_data->d_baseMatrix = new Matrix<bool>(); break;

            }

            in >> *baseDataset.d_data->d_baseMatrix;

        } else if (label == QString("d_verticalHeader"))
            in >> baseDataset.d_data->d_verticalHeader;
        else if (label == QString("d_horizontalHeader"))
            in >> baseDataset.d_data->d_horizontalHeader;
        else if (label == QString("d_columnAnnotations"))
            in >> baseDataset.d_data->d_columnAnnotations;
        else if (label == QString("d_rowAnnotations"))
            in >> baseDataset.d_data->d_rowAnnotations;
        else if (label == QString("d_name"))
            in >> baseDataset.d_data->d_name;
        else if (label == QString("_Dataset_end#"))
            return in;

    }

    return in;

}
