#include "stouffersmethod.h"

QStringList StouffersMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "Z-transformed ( Significance (2-tailed) )", "Sample size"};

QStringList StouffersMethod::d_metaAnalysisTestDescriptiveLabels = {"Z", "Significance (2-tailed)"};

QString StouffersMethod::d_nameOfMetaAnalysisTest = "Stouffer's method";


StouffersMethod::StouffersMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

StouffersMethod::~StouffersMethod()
{

}

QStringList StouffersMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList StouffersMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> StouffersMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    boost::math::normal dist(0.0, 1.0);

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << -1.0 * boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0) << baseStatisticalTest->sampleSize();

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> StouffersMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void StouffersMethod::calculateStatistic()
{

    boost::math::normal dist(0.0, 1.0);

    d_statistic = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        d_statistic += -1.0 *boost::math::quantile(dist, (baseStatisticalTest->pValue()) / 2.0);

    d_statistic /= std::sqrt(d_baseStatisticalTests.size());

}

void StouffersMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, std::fabs(d_statistic)));

}

void StouffersMethod::_reset()
{

}
