#ifndef SELECTCOLORWIDGET_H
#define SELECTCOLORWIDGET_H

#include <QWidget>
#include <QColorDialog>
#include <QColor>
#include <QPalette>

namespace Ui {

class SelectColorWidget;

}

class SelectColorWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectColorWidget(QWidget *parent = 0, const QColor &currentColor = Qt::white);

    ~SelectColorWidget();

    QColor currentColor() const;

public slots:

    void setCurrentColor(const QColor &color);

private:

    Ui::SelectColorWidget *ui;

    QColorDialog *d_colorDialog;

    QColor d_currentColor;

    void setColorInfo(const QColor &color);

private slots:

    void toolButton_triggered();

signals:

    void colorSelected(const QColor &color);

    void currentColorChanged(const QColor &color);

};

#endif // SELECTCOLORWIDGET_H
