#include "fisherstrendmethod.h"

QStringList FishersTrendMethod::d_baseStatisticalTestDescriptiveLabels = {"Significance (2-tailed)", "-LN( Significance (2-tailed) )", "Sign of statistic", "Sample size"};

QStringList FishersTrendMethod::d_metaAnalysisTestDescriptiveLabels = {"Chi-squared", "Degree of freedom", "Significance (2-tailed)"};

QString FishersTrendMethod::d_nameOfMetaAnalysisTest = "Fisher's trend method";


FishersTrendMethod::FishersTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests) :
    BaseMetaAnalysisTest(d_nameOfMetaAnalysisTest, baseStatisticalTests)
{

}

FishersTrendMethod::~FishersTrendMethod()
{

}

QStringList FishersTrendMethod::baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const
{

    QStringList _baseStatisticalTestDescriptiveLabels;

    for (const QString &subsetIdentifier : subsetIdentifiers) {

        for (const QString &baseStatisticalTestDescriptiveLabel : d_baseStatisticalTestDescriptiveLabels)
            _baseStatisticalTestDescriptiveLabels << baseStatisticalTestDescriptiveLabel + " \"" + subsetIdentifier + "\"";

    }

    return _baseStatisticalTestDescriptiveLabels;

}

const QStringList FishersTrendMethod::metaAnalysisTestDescriptiveLabels() const
{

    return d_metaAnalysisTestDescriptiveLabels;

}

QVector<QVariant> FishersTrendMethod::calculateBaseStatisticalTestDescriptiveValues()
{

    QVector<QVariant> baseStatisticalTestDescriptiveValues;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests)
        baseStatisticalTestDescriptiveValues << baseStatisticalTest->pValue() << -1.0 * std::log(baseStatisticalTest->pValue()) << (baseStatisticalTest->isSignOfStatisticNegative() ? "-" : "+") << baseStatisticalTest->sampleSize();

    return baseStatisticalTestDescriptiveValues;

}

QVector<QVariant> FishersTrendMethod::calculateMetaAnalysisTestDescriptiveValues()
{

    QVector<QVariant> metaAnalysisTestDescriptiveValues;

    metaAnalysisTestDescriptiveValues << d_statistic << d_baseStatisticalTests.size() * 2 << d_pValue;

    return metaAnalysisTestDescriptiveValues;

}

void FishersTrendMethod::calculateStatistic()
{

    double chiSquared = 0.0;

    for (const QSharedPointer<BaseStatisticalTest> & baseStatisticalTest : d_baseStatisticalTests) {

        if (baseStatisticalTest->isSignOfStatisticNegative())
            chiSquared -= -1.0 * std::log(baseStatisticalTest->pValue() / 2.0);
        else
            chiSquared += -1.0 * std::log(baseStatisticalTest->pValue() / 2.0);

    }

    d_statistic = std::fabs(-2.0 * chiSquared);

}

void FishersTrendMethod::calculatedPValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_baseStatisticalTests.size() * 2.0);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

}

void FishersTrendMethod::_reset()
{

}
