#include "setpiesliceparameterswidget.h"

SetPieSliceParametersWidget::SetPieSliceParametersWidget(QTreeWidget *parent, const QString &label, QList<QPieSlice *> pieSlices) :
    QObject(), QTreeWidgetItem(parent), d_pieSlices(pieSlices), d_initialized(false)
{

    if (!pieSlices.isEmpty()) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetPieSliceParametersWidget::SetPieSliceParametersWidget(QTreeWidgetItem *parent, const QString &label, QList<QPieSlice *> pieSlices) :
   QObject(), QTreeWidgetItem(parent), d_pieSlices(pieSlices), d_initialized(false)
{

    if (!pieSlices.isEmpty()) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetPieSliceParametersWidget::~SetPieSliceParametersWidget()
{

}

void SetPieSliceParametersWidget::initialize(const QString &label)
{

    this->setText(0, label);


    if (d_pieSlices.size() == 1) {

        d_setLabelWidget = new QLineEdit(d_pieSlices.first()->label());

        this->connect(d_setLabelWidget, SIGNAL(textEdited(QString)), this, SLOT(setLabel(QString)));

        this->treeWidget()->setItemWidget(this, 1, d_setLabelWidget);

    }

    d_setBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(this, "Brush", d_pieSlices.first()->brush());

    this->connect(d_setBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setBrush(QBrush)));


    d_setPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(this, "Pen", d_pieSlices.first()->pen());

    this->connect(d_setPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setPen(QPen)));


    d_selectExplodeWidget = new QCheckBox;

    d_selectExplodeWidget->setChecked(d_pieSlices.first()->isExploded());

    this->connect(d_selectExplodeWidget, SIGNAL(toggled(bool)), this, SLOT(setExploded(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Exploded"}), 1, d_selectExplodeWidget);


    d_selectExplodeDistanceFactorWidget = new QDoubleSpinBox;

    d_selectExplodeDistanceFactorWidget->setRange(0, 1.0);

    d_selectExplodeDistanceFactorWidget->setDecimals(2);

    d_selectExplodeDistanceFactorWidget->setSingleStep(0.01);

    d_selectExplodeDistanceFactorWidget->setValue(d_pieSlices.first()->explodeDistanceFactor());

    this->connect(d_selectExplodeDistanceFactorWidget, SIGNAL(valueChanged(double)), this, SLOT(setExplodeDistanceFactor(double)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Explode distance factor"}), 1, d_selectExplodeDistanceFactorWidget);


    QTreeWidgetItem *labelTreeWidgetItem = new QTreeWidgetItem(this, {"Label"});


    d_setLabelArmLengthFactor = new QDoubleSpinBox;

    d_setLabelArmLengthFactor->setRange(0, 1.0);

    d_setLabelArmLengthFactor->setDecimals(2);

    d_setLabelArmLengthFactor->setSingleStep(0.01);

    d_setLabelArmLengthFactor->setValue(d_pieSlices.first()->labelArmLengthFactor());

    this->connect(d_setLabelArmLengthFactor, SIGNAL(valueChanged(double)), this, SLOT(setLabelArmLengthFactor(double)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelTreeWidgetItem, {"Arm length factor"}), 1, d_setLabelArmLengthFactor);


    d_selectLabelColorWidget = new SelectColorWidget(nullptr, d_pieSlices.first()->labelColor());

    this->connect(d_selectLabelColorWidget, SIGNAL(colorSelected(QColor)), this, SLOT(setLabelColor(QColor)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelTreeWidgetItem, {"Color"}), 1, d_selectLabelColorWidget);


    d_selectLabelFontWidget = new SelectFontWidget(nullptr, d_pieSlices.first()->labelFont());

    this->connect(d_selectLabelFontWidget, SIGNAL(currentFontChanged(QFont)), this, SLOT(setLabelFont(QFont)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelTreeWidgetItem, {"Font"}), 1, d_selectLabelFontWidget);


    d_selectLabeLPositionWidget = new QComboBox();

    d_selectLabeLPositionWidget->addItem("Label is outside the slice with an arm", QPieSlice::LabelOutside);

    d_selectLabeLPositionWidget->addItem("Label is centered inside the slice and laid out horizontally", QPieSlice::LabelInsideHorizontal);

    d_selectLabeLPositionWidget->addItem("Label is centered inside the slice and rotated to be parallel to the tangential of the slice's arc", QPieSlice::LabelInsideTangential);

    d_selectLabeLPositionWidget->addItem("Label is centered inside the slice rotated to be parallel to the normal of the slice's arc", QPieSlice::LabelInsideNormal);

    d_selectLabeLPositionWidget->setCurrentIndex(d_pieSlices.first()->labelPosition());

    this->connect(d_selectLabeLPositionWidget, SIGNAL(currentIndexChanged(int)), this, SLOT(setLabelPosition(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelTreeWidgetItem, {"Position"}), 1, d_selectLabeLPositionWidget);


    d_selectLabelVisibleWidget = new QCheckBox;

    d_selectLabelVisibleWidget->setChecked(d_pieSlices.first()->isLabelVisible());

    this->connect(d_selectLabelVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setLabelVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelTreeWidgetItem, {"Visible"}), 1, d_selectLabelVisibleWidget);

}

void SetPieSliceParametersWidget::setBrush(const QBrush &brush)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setBrush(brush);

}

void SetPieSliceParametersWidget::setExplodeDistanceFactor(const double &factor)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setExplodeDistanceFactor(factor);

}

void SetPieSliceParametersWidget::setExploded(bool exploded)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setExploded(exploded);

}

void SetPieSliceParametersWidget::setLabel(const QString label)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setLabel(label);

}

void SetPieSliceParametersWidget::setLabelArmLengthFactor(const double &factor)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setLabelArmLengthFactor(factor);

}

void SetPieSliceParametersWidget::setLabelColor(QColor color)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setLabelColor(color);

}

void SetPieSliceParametersWidget::setLabelFont(const QFont &font)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setLabelFont(font);

}

void SetPieSliceParametersWidget::setLabelPosition(int position)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setLabelPosition(static_cast<QPieSlice::LabelPosition>(position));
}

void SetPieSliceParametersWidget::setLabelVisible(bool visible)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setLabelVisible(visible);

}

void SetPieSliceParametersWidget::setPen(const QPen &pen)
{

    for (QPieSlice *pieSlice : d_pieSlices)
        pieSlice->setPen(pen);

}

void SetPieSliceParametersWidget::setPieSlice(const QString &label, QPieSlice *pieSlice)
{

    d_pieSlices.first() = pieSlice;

    qDeleteAll(this->takeChildren());

    this->initialize(label);

    d_selectExplodeDistanceFactorWidget->setValue(pieSlice->explodeDistanceFactor());

    d_setBrushParametersTreeWidgetItem->setCurrentBrush(pieSlice->brush());

    d_selectExplodeWidget->setChecked(pieSlice->isExploded());

    d_setLabelWidget->setText(pieSlice->label());

    d_setLabelArmLengthFactor->setValue(pieSlice->labelArmLengthFactor());

    d_setLabelWidget->setText(pieSlice->label());

    d_selectLabelColorWidget->setCurrentColor(pieSlice->labelColor());

    d_selectLabelFontWidget->setCurrentFont(pieSlice->labelFont());

    d_selectLabeLPositionWidget->setCurrentIndex(static_cast<int>(pieSlice->labelPosition()));

    d_selectLabelVisibleWidget->setChecked(pieSlice->isLabelVisible());

    d_setPenParametersTreeWidgetItem->setCurrentPen(pieSlice->pen());

}

void SetPieSliceParametersWidget::updateSettings()
{

    if (d_pieSlices.isEmpty())
        return;

    d_selectExplodeDistanceFactorWidget->setValue(d_pieSlices.first()->explodeDistanceFactor());

    d_setBrushParametersTreeWidgetItem->setCurrentBrush(d_pieSlices.first()->brush());

    d_selectExplodeWidget->setChecked(d_pieSlices.first()->isExploded());

    d_setLabelWidget->setText(d_pieSlices.first()->label());

    d_setLabelArmLengthFactor->setValue(d_pieSlices.first()->labelArmLengthFactor());

    d_setLabelWidget->setText(d_pieSlices.first()->label());

    d_selectLabelColorWidget->setCurrentColor(d_pieSlices.first()->labelColor());

    d_selectLabelFontWidget->setCurrentFont(d_pieSlices.first()->labelFont());

    d_selectLabeLPositionWidget->setCurrentIndex(static_cast<int>(d_pieSlices.first()->labelPosition()));

    d_selectLabelVisibleWidget->setChecked(d_pieSlices.first()->isLabelVisible());

    d_setPenParametersTreeWidgetItem->setCurrentPen(d_pieSlices.first()->pen());

}
