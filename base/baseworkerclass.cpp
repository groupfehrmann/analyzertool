#include "baseworkerclass.h"

BaseWorkerClass::BaseWorkerClass(QObject *parent) :
    QObject(parent)
{

    d_timer = new QTimer();

    d_timer->setInterval(500);

    this->connect(&d_timerThread, SIGNAL(started()), d_timer, SLOT(start()));

    this->connect(&d_timerThread, SIGNAL(finished()), d_timer, SLOT(stop()));

    d_timer->moveToThread(&d_timerThread);

}

BaseWorkerClass::~BaseWorkerClass()
{

    d_timerThread.quit();

    d_timerThread.wait();

    delete d_timer;

}

