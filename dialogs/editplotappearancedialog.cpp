#include "editplotappearancedialog.h"
#include "ui_editplotappearancedialog.h"

EditPlotAppearanceDialog::EditPlotAppearanceDialog(QWidget *parent, QChart *chart) :
    QDialog(parent),
    ui(new Ui::EditPlotAppearanceDialog)
{

    ui->setupUi(this);

    this->setWindowTitle(QCoreApplication::applicationName() + " - Edit plot appearance");

    ui->treeWidget->addTopLevelItem(new SetChartParametersTreeWidgetItem(ui->treeWidget, chart->title(), chart));

    QSettings settings;

    settings.beginGroup("editPlotAppearanceDialog");

    if (settings.contains("geometry"))
        this->restoreGeometry(settings.value("geometry").toByteArray());

    settings.endGroup();

    this->connect(ui->toolButton_expandAll, SIGNAL(clicked(bool)), ui->treeWidget, SLOT(expandAll()));

    this->connect(ui->toolButton_collapsAll, SIGNAL(clicked(bool)), ui->treeWidget, SLOT(collapseAll()));

}

EditPlotAppearanceDialog::~EditPlotAppearanceDialog()
{

    QSettings settings;

    settings.beginGroup("editPlotAppearanceDialog");

    settings.setValue("geometry", this->saveGeometry());

    settings.endGroup();

    delete ui;

}
