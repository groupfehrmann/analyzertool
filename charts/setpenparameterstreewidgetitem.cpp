#include "setpenparameterstreewidgetitem.h"

SetPenParametersTreeWidgetItem::SetPenParametersTreeWidgetItem(QTreeWidget *parent, const QString &label, const QPen &defaultPen) :
    QObject(), QTreeWidgetItem(parent), d_pen(defaultPen)
{

    this->initialize(label, defaultPen);

}

SetPenParametersTreeWidgetItem::SetPenParametersTreeWidgetItem(QTreeWidgetItem *parent, const QString &label, const QPen &defaultPen) :
   QObject(), QTreeWidgetItem(parent), d_pen(defaultPen)
{

    this->initialize(label, defaultPen);

}

SetPenParametersTreeWidgetItem::~SetPenParametersTreeWidgetItem()
{

}

QPen SetPenParametersTreeWidgetItem::currentPen()
{

    return d_pen;

}

void SetPenParametersTreeWidgetItem::initialize(const QString &label, const QPen &pen)
{

    this->setText(0, label);


    d_selectPenCapStyleWidget = new SelectPenCapStyleWidget(nullptr, pen.capStyle());

    this->connect(d_selectPenCapStyleWidget, SIGNAL(currentPenCapStyleChanged(Qt::PenCapStyle)), this, SLOT(setCapStyle(Qt::PenCapStyle)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Cap style"}), 1, d_selectPenCapStyleWidget);


    d_selectColorWidget = new SelectColorWidget(nullptr, pen.color());

    this->connect(d_selectColorWidget, SIGNAL(colorSelected(QColor)), this, SLOT(setColor(QColor)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Color"}), 1, d_selectColorWidget);


    d_selectPenJoinStyleWidget = new SelectPenJoinStyleWidget(nullptr, pen.joinStyle());

    this->connect(d_selectPenJoinStyleWidget, SIGNAL(currentPenJoinStyleChanged(Qt::PenJoinStyle)), this, SLOT(setJoinStyle(Qt::PenJoinStyle)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Join style"}), 1, d_selectPenJoinStyleWidget);


    d_selectPenStyleWidget = new SelectPenStyleWidget(nullptr, pen.style());

    this->connect(d_selectPenStyleWidget, SIGNAL(currentPenStyleChanged(Qt::PenStyle)), this, SLOT(setStyle(Qt::PenStyle)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Style"}), 1, d_selectPenStyleWidget);


    d_spinBoxWidthWidget = new QSpinBox;

    d_spinBoxWidthWidget->setValue(pen.width());

    this->connect(d_spinBoxWidthWidget, SIGNAL(valueChanged(int)), this, SLOT(setWidth(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Width"}), 1, d_spinBoxWidthWidget);

}

void SetPenParametersTreeWidgetItem::setColor(const QColor &color)
{

    d_pen.setColor(color);

    emit currentPenChanged(d_pen);

}

void SetPenParametersTreeWidgetItem::setCapStyle(Qt::PenCapStyle capStyle)
{

    d_pen.setCapStyle(capStyle);

    emit currentPenChanged(d_pen);

}

void SetPenParametersTreeWidgetItem::setCurrentPen(const QPen &pen)
{

    d_pen = pen;

    d_spinBoxWidthWidget->setValue(pen.width());

    d_selectColorWidget->setCurrentColor(pen.color());

    d_selectPenStyleWidget->setCurrentPenStyle(pen.style());

    d_selectPenCapStyleWidget->setCurrentPenCapStyle(pen.capStyle());

    d_selectPenJoinStyleWidget->setCurrentPenJoinStyle(pen.joinStyle());

    emit currentPenChanged(d_pen);

}


void SetPenParametersTreeWidgetItem::setJoinStyle(Qt::PenJoinStyle joinStyle)
{

    d_pen.setJoinStyle(joinStyle);

    emit currentPenChanged(d_pen);

}

void SetPenParametersTreeWidgetItem::setStyle(Qt::PenStyle style)
{

    d_pen.setStyle(style);

    emit currentPenChanged(d_pen);

}

void SetPenParametersTreeWidgetItem::setWidth(int width)
{

    d_pen.setWidth(width);

    emit currentPenChanged(d_pen);

}
