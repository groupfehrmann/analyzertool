#ifndef SETPIESLICEPARAMETERSWIDGET_H
#define SETPIESLICEPARAMETERSWIDGET_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QPieSlice>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QComboBox>
#include <QList>
#include <QPieSlice>

#include "charts/setbrushparameterstreewidgetitem.h"
#include "charts/setpenparameterstreewidgetitem.h"
#include "charts/selectfontwidget.h"
#include "charts/selectcolorwidget.h"

using namespace QtCharts;

class SetPieSliceParametersWidget : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetPieSliceParametersWidget(QTreeWidget *parent = nullptr, const QString &label = "Pie slice", QList<QPieSlice *> pieSlices = QList<QPieSlice *>());

    explicit SetPieSliceParametersWidget(QTreeWidgetItem *parent = nullptr, const QString &label = "Pie slice", QList<QPieSlice *> pieSlices = QList<QPieSlice *>());

    ~SetPieSliceParametersWidget();

public slots:

    void setPieSlice(const QString &label, QPieSlice *pieSlice);

    void updateSettings();

private:

    void initialize(const QString &label);

    QList<QPieSlice *> d_pieSlices;

    bool d_initialized;

    QDoubleSpinBox *d_selectExplodeDistanceFactorWidget;

    SetBrushParametersTreeWidgetItem *d_setBrushParametersTreeWidgetItem;

    QCheckBox *d_selectExplodeWidget;

    QLineEdit *d_setLabelWidget;

    QDoubleSpinBox *d_setLabelArmLengthFactor;

    SelectColorWidget *d_selectLabelColorWidget;

    SelectFontWidget *d_selectLabelFontWidget;

    QComboBox *d_selectLabeLPositionWidget;

    QCheckBox *d_selectLabelVisibleWidget;

    SetPenParametersTreeWidgetItem *d_setPenParametersTreeWidgetItem;

private slots:

    void setBrush(const QBrush &brush);

    void setExplodeDistanceFactor(const double &factor);

    void setExploded(bool exploded);

    void setLabel(const QString label);

    void setLabelArmLengthFactor(const double &factor);

    void setLabelColor(QColor color);

    void setLabelFont(const QFont &font);

    void setLabelPosition(int position);

    void setLabelVisible(bool visible);

    void setPen(const QPen &pen);

};

#endif // SETPIESLICEPARAMETERSWIDGET_H
