#ifndef SETBRUSHPARAMETERSTREEWIDGETITEM_H
#define SETBRUSHPARAMETERSTREEWIDGETITEM_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QBrush>

#include "charts/selectcolorwidget.h"
#include "charts/selectbrushstylewidget.h"

class SetBrushParametersTreeWidgetItem : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetBrushParametersTreeWidgetItem(QTreeWidget *parent = nullptr, const QString &label = "Brush", const QBrush &defaultBrush = QBrush());

    explicit SetBrushParametersTreeWidgetItem(QTreeWidgetItem *parent = nullptr, const QString &label = "Brush", const QBrush &defaultBrush = QBrush());

    ~SetBrushParametersTreeWidgetItem();

    QBrush currentBrush();

public slots:

    void setCurrentBrush(const QBrush &brush);

private:

    void initialize(const QString &label, const QBrush &brush);

    QBrush d_brush;

    SelectColorWidget *d_selectColorWidget;

    SelectBrushStyleWidget *d_selectBrushStyleWidget;

private slots:

    void setColor(const QColor &color);

    void setStyle(Qt::BrushStyle style);

signals:

    void currentBrushChanged(const QBrush &brush);

};

#endif // SETBRUSHPARAMETERSTREEWIDGETITEM_H
