#include "createscatterplotswithgenomicmappingonxaxishomosapiensworkerclass.h"

unsigned int CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::d_numberOfBasepairsPerAutosomalChromosome[24] = {249250621, 243199373, 198022430, 191154276, 180915260, 171115067, 159138663, 146364022, 141213431, 135534747, 135006516, 133851895, 115169878, 107349540, 102531392, 90354753, 81195210, 78077248, 59128983, 63025520, 48129895, 51304566, 155270560, 59373566};

unsigned int CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::d_numberOfBasepairsPerAutosomalChromosome_cumulative[25] = {0, 249250621, 492449994, 690472424, 881626700, 1062541960, 1233657027, 1392795690, 1539159712, 1680373143, 1815907890, 1950914406, 2084766301, 2199936179, 2307285719, 2409817111, 2500171864, 2581367074, 2659444322, 2718573305, 2781598825, 2829728720, 2881033286, 3036303846, 3095677412};

CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::Parameters *>(parameters))
{

}

void CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::doWork()
{

    emit processStarted("create scatter plots with genomic mapping on x-axis (Homo Sapiens)");

    if (!d_parameters.exportDirectory.isEmpty() && !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Scatter plots with genomic mapping on x-axis (Homo Sapiens)"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of variables selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of items selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "annotation label defining chromosome position: \"" + d_parameters.annotationLabelDefiningChromosomePosition + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "annotation label defining basepair position: \"" + d_parameters.annotationLabelDefiningBasepairPosition + "\"");

    if (!d_parameters.annotationLabelDefiningSubsets.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining subsets: \"" + d_parameters.annotationLabelDefiningSubsets + "\"");

        emit appendLogItem(LogListModel::PARAMETER, "subset identifiers: " + ConvertFunctions::toString(d_parameters.subsetIdentifiers));

    }

    if (!d_parameters.annotationLabelDefiningSubsets.isEmpty() || d_parameters.plotMode == "All variables in single plot")
        emit appendLogItem(LogListModel::PARAMETER, "seperate Y-axis for each subset/variable: " + (d_parameters.seperateYAxisForEachSubset ? QString("enabled") : QString("disabled")));

    if (!d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "annotation label defining title: \"" + d_parameters.annotationLabelDefiningAlternativeVariableIdentifier + "\"");

    if (!d_parameters.prefixStringToUseInPlotName.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "prefix string to use in plot name: \"" + d_parameters.prefixStringToUseInPlotName + "\"");

    if (d_parameters.chartType == QChart::ChartTypeCartesian)
        emit appendLogItem(LogListModel::PARAMETER, "chart type: cartesian");
    else
        emit appendLogItem(LogListModel::PARAMETER, "chart type: polar");

    emit appendLogItem(LogListModel::PARAMETER, "plot mode: " + d_parameters.plotMode);

    if (!d_parameters.exportDirectory.isEmpty()) {

        if (d_parameters.widthOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale width of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "width of plot: " + QString::number(d_parameters.widthOfPlot));

        if (d_parameters.heightOfPlot == -1)
            emit appendLogItem(LogListModel::PARAMETER, "autoscale height of plot");
        else
            emit appendLogItem(LogListModel::PARAMETER, "height of plot: " + QString::number(d_parameters.heightOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "dpi of plot: " + QString::number(d_parameters.dpiOfPlot));

        emit appendLogItem(LogListModel::PARAMETER, "export type of plot: " + d_parameters.exportTypeOfPlot);

        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    }

    if (!d_parameters.pathToTemplatePlot.isEmpty()) {

        emit appendLogItem(LogListModel::PARAMETER, "template plot selected: \"" + d_parameters.pathToTemplatePlot + "\"");

        emit startProgress(0, "Opening template plot", 0, 0);

        bool error = false;

        QString errorMessage;

        OpenPlotFromFile openPlotFromFile(nullptr, d_parameters.pathToTemplatePlot, &error, &errorMessage);

        if (error)
            emit appendLogItem(LogListModel::ERROR, errorMessage);
        else
            d_templatePlot = openPlotFromFile.chart();

        emit stopProgress(0);

    }

    emit startProgress(0, "Obtaining genomic mappings", 0, 0);

    QList<QPair<int, std::tuple<QString, int, double, double> > > listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition = ConvertFunctions::toListOfPairedIndexWithTupleOfValues(d_parameters.baseDataset->indexToAnnotationValues<QString, int, double, double>(d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.annotationLabelDefiningSubsets, QSet<QString>(d_parameters.subsetIdentifiers.begin(), d_parameters.subsetIdentifiers.end()), d_parameters.annotationLabelDefiningChromosomePosition, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}, d_parameters.annotationLabelDefiningBasepairPosition, QSet<double>(), QString(), QSet<double>(), BaseMatrix::switchOrientation(d_parameters.orientation)));

    std::function<bool(const QPair<int, std::tuple<QString, int, double, double> > &element1, const QPair<int, std::tuple<QString, int, double, double> > &element2) > compareFunction = [](const QPair<int, std::tuple<QString, int, double, double> > &element1, const QPair<int, std::tuple<QString, int, double, double> > &element2) {

        int chromosomePosition1 = std::get<1>(element1.second);

        int chromosomePosition2 = std::get<1>(element2.second);

        if (chromosomePosition1 < chromosomePosition2)
            return true;
        else if (chromosomePosition1 > chromosomePosition2)
            return false;
        else if (std::get<2>(element1.second) < std::get<2>(element2.second))
            return true;
        else
            return false;

        };

    std::sort(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition.begin(), listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition.end(), compareFunction);

    QVector<int> rankedChromosomeMappingForAllItems;

    QVector<double> rankedCumulativeBasepairMappingForAllItems;

    for (int i = 0; i < listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition.size(); ++i) {

       int currentChromosomePosition = std::get<1>(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition[i].second);

       rankedChromosomeMappingForAllItems << currentChromosomePosition;

       std::get<3>(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition[i].second) = d_numberOfBasepairsPerAutosomalChromosome_cumulative[currentChromosomePosition - 1] + std::get<2>(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition[i].second);

       rankedCumulativeBasepairMappingForAllItems << std::get<3>(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition[i].second);

    }

    double minimumBasepair = std::get<3>(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition.first().second);

    double maximumBasepair = std::get<3>(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition.last().second);

    QHash<QString, std::tuple<QList<int>, QVector<int>, QVector<double>, QVector<double> > > subsetIdentifierTo_indexes_chromosomePosition_basePairPosition_cumulativeBasepairPosition = ConvertFunctions::toHashWithFirstValueOfTupleAsKeyAndPlaceIndexInFirstSequenceOfTuple<QString, int, double, double>(listOfPairedIndexWith_subsetIdentifier_chromosomePosition_basepairPosition_cumulativeBasepairPosition);

    if (d_parameters.annotationLabelDefiningSubsets.isEmpty())
        d_parameters.subsetIdentifiers << QString();
    else {

        for (int i = d_parameters.subsetIdentifiers.size() - 1; i >=0; --i) {

            if (!subsetIdentifierTo_indexes_chromosomePosition_basePairPosition_cumulativeBasepairPosition.contains(d_parameters.subsetIdentifiers.at(i)))
                d_parameters.subsetIdentifiers.removeOne(d_parameters.subsetIdentifiers.at(i));

        }

    }

    emit stopProgress(0);

    QString fileExtension = d_parameters.exportTypeOfPlot.split(".").at(1);

    fileExtension.remove(")");

    if (d_parameters.plotMode == "All variables in single plot") {

        emit startProgress(0, "Creating plot", 0, 0);

        QVector<QVector<double> > xVectors;

        QVector<QVector<double> > yVectors;

        QStringList seriesLabels;

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i) {

            for (const QString &subsetIdentifier : d_parameters.subsetIdentifiers) {

                if (subsetIdentifier.isEmpty())
                    seriesLabels << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i);
                else
                    seriesLabels << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) + " - " + subsetIdentifier;


                xVectors << std::get<3>(subsetIdentifierTo_indexes_chromosomePosition_basePairPosition_cumulativeBasepairPosition.value(subsetIdentifier));

                yVectors << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), std::get<0>(subsetIdentifierTo_indexes_chromosomePosition_basePairPosition_cumulativeBasepairPosition.value(subsetIdentifier)), d_parameters.orientation);

            }

        }

        QSharedPointer<ScatterChart_GenomicMappingOnXAxis_HomoSapiens> scatterChart_GenomicMappingOnXAxis_HomoSapiens(new ScatterChart_GenomicMappingOnXAxis_HomoSapiens(nullptr, "scatter plot - genomic mapping on x-axis - Homo Sapiens", rankedChromosomeMappingForAllItems, rankedCumulativeBasepairMappingForAllItems, minimumBasepair, maximumBasepair, seriesLabels, xVectors, yVectors, d_parameters.chartType, d_parameters.seperateYAxisForEachSubset));

        if (d_templatePlot)
            CopyChartParameters(nullptr, scatterChart_GenomicMappingOnXAxis_HomoSapiens->chart().data(), d_templatePlot.data());

        if (d_parameters.exportDirectory.isEmpty())
            emit resultItemAvailable(new PlotResultItem(nullptr, "scatter plot - genomic mapping on x-axis - Homo Sapiens", scatterChart_GenomicMappingOnXAxis_HomoSapiens->chart()));
        else {

            bool error;

            QString errorMessage;

            ExportPlotToFile(nullptr, scatterChart_GenomicMappingOnXAxis_HomoSapiens->chart().data(), d_parameters.exportDirectory + "/scatterChart_GenomicMappingOnXAxis_HomoSapiens." + fileExtension , d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

            if (error)
               emit appendLogItem(LogListModel::ERROR, errorMessage);

        }

        emit stopProgress(0);

    } else {

        emit startProgress(0, "Creating scatter plots", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

        for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i) {

            QVector<QVector<double> > xVectors;

            QVector<QVector<double> > yVectors;

            for (const QString &subsetIdentifier : d_parameters.subsetIdentifiers) {

                xVectors << std::get<3>(subsetIdentifierTo_indexes_chromosomePosition_basePairPosition_cumulativeBasepairPosition.value(subsetIdentifier));

                yVectors << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), std::get<0>(subsetIdentifierTo_indexes_chromosomePosition_basePairPosition_cumulativeBasepairPosition.value(subsetIdentifier)), d_parameters.orientation);

            }

            QSharedPointer<ScatterChart_GenomicMappingOnXAxis_HomoSapiens> scatterChart_GenomicMappingOnXAxis_HomoSapiens(new ScatterChart_GenomicMappingOnXAxis_HomoSapiens(nullptr, (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()), rankedChromosomeMappingForAllItems, rankedCumulativeBasepairMappingForAllItems, minimumBasepair, maximumBasepair, d_parameters.subsetIdentifiers, xVectors, yVectors, d_parameters.chartType, d_parameters.seperateYAxisForEachSubset));

            if (d_templatePlot)
                CopyChartParameters(nullptr, scatterChart_GenomicMappingOnXAxis_HomoSapiens->chart().data(), d_templatePlot.data());

            if (d_parameters.exportDirectory.isEmpty())
                emit resultItemAvailable(new PlotResultItem(nullptr, (d_parameters.prefixStringToUseInPlotName.isEmpty() ? "scatter plot - genomic mapping on x-axis - Homo Sapiens - " : d_parameters.prefixStringToUseInPlotName) + (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()), scatterChart_GenomicMappingOnXAxis_HomoSapiens->chart()));
            else {

                bool error;

                QString errorMessage;

                ExportPlotToFile(nullptr, scatterChart_GenomicMappingOnXAxis_HomoSapiens->chart().data(), d_parameters.exportDirectory + "/" + (d_parameters.prefixStringToUseInPlotName.isEmpty() ? "scatter plot - genomic mapping on x-axis - Homo Sapiens - " : d_parameters.prefixStringToUseInPlotName) + (d_parameters.annotationLabelDefiningAlternativeVariableIdentifier.isEmpty() ? d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) : d_parameters.baseDataset->annotations(d_parameters.orientation).value(d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i), d_parameters.annotationLabelDefiningAlternativeVariableIdentifier).toString()).simplified() + "." + fileExtension , d_parameters.widthOfPlot, d_parameters.heightOfPlot, d_parameters.dpiOfPlot, &error, &errorMessage);

                if (error)
                    emit appendLogItem(LogListModel::ERROR, errorMessage);

            }

            if (QThread::currentThread()->isInterruptionRequested()) {

                scatterChart_GenomicMappingOnXAxis_HomoSapiens.clear();

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(0, i + 1);

        }

        emit stopProgress(0);

    }

    QThread::currentThread()->quit();

}
