#ifndef SELECTWITHREGULAREXPRESSIONWORKERCLASS_H
#define SELECTWITHREGULAREXPRESSIONWORKERCLASS_H

#include <QString>
#include <QRegExp>

#include "base/baseworkerclass.h"

class SelectWithRegularExpressionWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    enum Mode{

        OPTIONAL = 0,

        REQUIRED = 1

    };

    struct Filter {

        Mode mode;

        QString annotationLabel;

        QRegExp regularExpression;

    };

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        BaseMatrix::Orientation orientation;

        QList<Filter> filters;

        QPair<QStringList, QList<int> > selectedIdentifiersAndIndexes;

    };

    SelectWithRegularExpressionWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    SelectWithRegularExpressionWorkerClass::Parameters d_parameters;

};

#endif // SELECTWITHREGULAREXPRESSIONWORKERCLASS_H
