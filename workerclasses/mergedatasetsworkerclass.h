#ifndef MERGEDATASETSWORKERCLASS_H
#define MERGEDATASETSWORKERCLASS_H

#include <QString>
#include <QList>
#include <QSharedPointer>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"
#include "base/basematrix.h"
#include "base/projectlist.h"
#include "base/convertfunctions.h"

class MergeDatasetsWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        bool createUniqueIdentifiers;

        bool createUniqueAnnotationLabels;

        QList<QSharedPointer<BaseDataset> > datasetsToMerge;

        bool mergeColumnAnnotations;

        QString mergedDatasetDataType;

        bool mergeRowAnnotations;

        BaseMatrix::Orientation orientationToMerge;

        QList<bool> selectedOnlyStatusOfDatasetsToMerge;

    };

    MergeDatasetsWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    MergeDatasetsWorkerClass::Parameters d_parameters;

    BaseDataset *d_baseDataset;

};

#endif // MERGEDATASETSWORKERCLASS_H
