#include "createscatterplotswithgenomicmappingonxaxishomosapiensdialog.h"

CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog::CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::Parameters;

    this->setupWidgets();

}

CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog::~CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog()
{

   delete static_cast<CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::Parameters *>(d_parameters);

}

void CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 2, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningChromosomeMapping = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningChromosomeMapping", "Select annotation label defining chromosome mapping");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningChromosomeMapping, 3, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningBasepairMapping = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningBasepairMapping", "Select annotation label defining basepair mapping");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningBasepairMapping, 4, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets", "Select annotation label defining subsets (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets, 5, 0);

    d_itemsSelectorBuildingBlockWidget_subsetIdentifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_subsetIdentifiers", "Select subset identifiers (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_subsetIdentifiers, 6, 0);

    d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_seperateYAxisForEachSubset", "Create seperate Y-axis for each subset", {"Enabled", "Disabled"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset, 7, 0);

    d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset->setEnabled(false);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier", "Select annotation label defining title (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier, 8, 0);

    d_comboBoxBuildingBlockWidget_chartType = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_chartType", "Select chart type", {"Cartesian", "Polar"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_chartType, 9, 0);

    d_comboBoxBuildingBlockWidget_plotMode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_plotMode", "Select plot mode", {"All variables in single plot", "Each variable in seperate plot"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_plotMode, 10, 0);

    d_plotExportParametersBuildingBlockWidget = new PlotExportParametersBuildingBlockWidget(this, "plotExportParametersBuildingBlockWidget");

    d_gridLayout->addWidget(d_plotExportParametersBuildingBlockWidget, 11, 0);

    d_lineEditBuildingBlockWidget_prefixStringToUseInPlotName = new LineEditBuildingBlockWidget(this, "lineEditBuildingBlockWidget_prefixStringToUseInPlotName", "Prefix string to use in plot name (optionale)");

    d_gridLayout->addWidget(d_lineEditBuildingBlockWidget_prefixStringToUseInPlotName, 12, 0);

    d_selectFileBuildingBlockWidget_templatePlot = new SelectFileBuildingBlockWidget(this, "selectFileBuildingBlockWidget_templatePlot", "Select template plot (optional)", QFileDialog::AcceptOpen, true);

    d_gridLayout->addWidget(d_selectFileBuildingBlockWidget_templatePlot, 13, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

    this->connect(d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox(), SIGNAL(currentTextChanged(QString)), this, SLOT(annotationLabelDefiningSubsetChanged(QString)));

    this->connect(d_comboBoxBuildingBlockWidget_plotMode->pointerToInternalComboBox(), SIGNAL(currentTextChanged(QString)), this, SLOT(plotModeChanged(QString)));

    this->plotModeChanged(d_comboBoxBuildingBlockWidget_plotMode->pointerToInternalComboBox()->currentText());

}

void CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog::initializeParameters()
{

    CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::Parameters *parameters = static_cast<CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensWorkerClass::Parameters *>(d_parameters);

    parameters->annotationLabelDefiningAlternativeVariableIdentifier = d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningBasepairPosition = d_comboBoxBuildingBlockWidget_annotationLabelDefiningBasepairMapping->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningChromosomePosition = d_comboBoxBuildingBlockWidget_annotationLabelDefiningChromosomeMapping->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningSubsets = d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->currentText();

    parameters->baseDataset = d_baseDataset;

    parameters->chartType = (d_comboBoxBuildingBlockWidget_chartType->pointerToInternalComboBox()->currentText() == "Cartesian") ? QChart::ChartTypeCartesian : QChart::ChartTypePolar;

    parameters->dpiOfPlot = d_plotExportParametersBuildingBlockWidget->dpiOfPlot();

    parameters->exportDirectory = d_plotExportParametersBuildingBlockWidget->exportDirectory();

    parameters->exportTypeOfPlot = d_plotExportParametersBuildingBlockWidget->typeOfPlot();

    parameters->heightOfPlot = d_plotExportParametersBuildingBlockWidget->heightOfPlot();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->pathToTemplatePlot = d_selectFileBuildingBlockWidget_templatePlot->pointerToInternalLineEdit()->text();

    parameters->plotMode = d_comboBoxBuildingBlockWidget_plotMode->pointerToInternalComboBox()->currentText();

    parameters->prefixStringToUseInPlotName = d_lineEditBuildingBlockWidget_prefixStringToUseInPlotName->pointerToInternalLineEdit()->text();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->seperateYAxisForEachSubset = (d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset->pointerToInternalComboBox()->currentText() == "Enabled") ? true : false;

    parameters->subsetIdentifiers = d_itemsSelectorBuildingBlockWidget_subsetIdentifiers->selectedItemsIdentifierAndNumber().first;

    parameters->widthOfPlot = d_plotExportParametersBuildingBlockWidget->widthOfPlot();

}

void CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningBasepairMapping->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningBasepairMapping->pointerToInternalComboBox()->addItems(d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningChromosomeMapping->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningChromosomeMapping->pointerToInternalComboBox()->addItems(d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(orientation).labels());

}

void CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog::annotationLabelDefiningSubsetChanged(const QString &currentAnnotationLabel)
{

    if (currentAnnotationLabel.isEmpty() && (d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset->pointerToInternalComboBox()->currentText() != "All variables in single plot")) {

        d_itemsSelectorBuildingBlockWidget_subsetIdentifiers->clear();

        d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset->setEnabled(false);

        return;

    }

    QStringList subsetIdentifiers = d_baseDataset->uniqueAnnotationValues<QString>(currentAnnotationLabel, BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget->selectedOrientation()), d_selectedOnly).values();

    subsetIdentifiers.removeOne(QString());

    std::sort(subsetIdentifiers.begin(), subsetIdentifiers.end());

    d_itemsSelectorBuildingBlockWidget_subsetIdentifiers->setItemsIdentifier(subsetIdentifiers);

    d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset->setEnabled(true);

}

void CreateScatterPlotsWithGenomicMappingOnXAxisHomoSapiensDialog::plotModeChanged(const QString &plotMode)
{

    if (d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->currentText().isEmpty())
        d_comboBoxBuildingBlockWidget_seperateYAxisForEachSubset->setEnabled(plotMode == "All variables in single plot");

}
