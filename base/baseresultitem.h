#ifndef BASERESULTITEM_H
#define BASERESULTITEM_H

#include <QObject>
#include <QSharedDataPointer>
#include <QSharedData>
#include <QUuid>
#include <QDataStream>

class BaseResultItemData : public QSharedData
{

public:

    BaseResultItemData();

    BaseResultItemData(const BaseResultItemData &other);

    ~BaseResultItemData();

    QString d_name;

    int d_type;

};

class BaseResultItem : public QObject
{

    Q_OBJECT

public:

    enum Type {

        TABLE = 0,

        PLOT = 1,

        TEXT = 2

    };

    BaseResultItem(QObject *parent = nullptr, const QString &name = "Result item");

    BaseResultItem(const BaseResultItem &other);

    ~BaseResultItem();

    const QString &name() const;

    BaseResultItem::Type type() const;

    const QUuid &universallyUniqueIdentifier() const;

    BaseResultItem &operator=(const BaseResultItem &other);

    friend QDataStream& operator<<(QDataStream& out, BaseResultItem &baseResultItem);

    friend QDataStream& operator>>(QDataStream& in, BaseResultItem &baseResultItem);

protected:

    QSharedDataPointer<BaseResultItemData> d_data;

    QUuid d_uuid;

public slots:

    void setName(const QString &name);

signals:

    void nameChanged(const QUuid &uuid, const QString &name);

};

#endif // BASERESULTITEM_H
