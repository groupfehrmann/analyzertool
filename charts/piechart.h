#ifndef PIECHART_H
#define PIECHART_H

#include <QObject>
#include <QApplication>
#include <QChart>
#include <QPieSeries>
#include <QPieSlice>
#include <QStringList>
#include <QVector>
#include <QEventLoop>
#include <QHash>
#include <QColor>
#include <QLegendMarker>
#include <QPieLegendMarker>

#include <tuple>

#include "charts/exportplottofile.h"
#include "charts/basechart.h"

using namespace QtCharts;

class MainSlice : public QPieSlice
 {

    Q_OBJECT

public:

    MainSlice(QPieSeries *breakdownSeries, QObject *parent = nullptr);

    QPieSeries *breakdownSeries() const;

    void setName(QString name);

    QString name() const;

private:

    QPieSeries *d_breakdownSeries;

    QString d_name;

};


class PieChart : public BaseChart
{

    Q_OBJECT

public:

    explicit PieChart(QObject *parent = nullptr, const QString &title = QString(), const QStringList &pieSliceSubsetIdentifiers = QStringList(), const QStringList &pieSliceLabels = QStringList(), const QVector<double> &pieSliceValues = QVector<double>());

private:

    QList<QColor> d_defaultColors;

signals:

    void initializeChart(const QString &title, const QStringList &pieSliceSubsetIdentifiers, const QStringList &pieSliceLabels, const QVector<double> &pieSliceValues);

private slots:

    void _initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QStringList &pieSliceLabels, const QVector<double>  &pieSliceValues);

};

#endif // PIECHART_H
