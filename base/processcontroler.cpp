#include "processcontroler.h"

ProcessControler::ProcessControler(QObject *parent) :
    QObject(parent)
{

    d_workerThread = new QThread(this);

}

ProcessControler::~ProcessControler()
{

}

void ProcessControler::cancelWorkerThread()
{

    d_workerThread->requestInterruption();

}

void ProcessControler::deleteCurrentBaseWorkerClass()
{

    this->disconnect(d_currentBaseWorkerClass, nullptr, nullptr, nullptr);

    this->disconnect(d_workerThread, nullptr, nullptr, nullptr);

    delete d_currentBaseWorkerClass;

}

bool ProcessControler::processRunning() const
{

    return d_workerThread->isRunning();

}

void ProcessControler::startProcess(BaseWorkerClass *baseWorkerClass)
{

    d_currentBaseWorkerClass = baseWorkerClass;

    d_currentBaseWorkerClass->setParent(nullptr);

    if (d_workerThread->isRunning()) {

        emit appendLogItem(LogListModel::ERROR, "there is allready a process running");

        delete d_currentBaseWorkerClass;

        return;

    }

    this->connect(d_currentBaseWorkerClass, SIGNAL(addToRecentlyOpenedFiles(QString,QString)), this, SIGNAL(addToRecentlyOpenedFiles(QString,QString)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(annotationsBeginChange(QUuid,BaseMatrix::Orientation)), this, SIGNAL(annotationsBeginChange(QUuid,BaseMatrix::Orientation)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(annotationsEndChange(QUuid,BaseMatrix::Orientation)), this, SIGNAL(annotationsEndChange(QUuid,BaseMatrix::Orientation)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(appendLogItem(LogListModel::Type,QString)), this, SIGNAL(appendLogItem(LogListModel::Type,QString)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(datasetAvailable(BaseDataset*)), this, SIGNAL(datasetAvailable(BaseDataset*)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(datasetBeginChange(QUuid)), this, SIGNAL(datasetBeginChange(QUuid)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(datasetEndChange(QUuid)), this, SIGNAL(datasetEndChange(QUuid)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(matrixDataBeginChange(QUuid)), this, SIGNAL(matrixDataBeginChange(QUuid)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(matrixDataEndChange(QUuid)), this, SIGNAL(matrixDataEndChange(QUuid)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(processStarted(QString)), this, SIGNAL(processStarted(QString)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(processStopped()), this, SIGNAL(processStopped()));

    this->connect(d_currentBaseWorkerClass, SIGNAL(projectAvailable(Project*)), this, SIGNAL(projectAvailable(Project*)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(resultAvailable(Result*)), this, SIGNAL(resultAvailable(Result*)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(resultItemAvailable(BaseResultItem*)), this, SIGNAL(resultItemAvailable(BaseResultItem*)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(startProgress(int,QString,double,double)), this, SIGNAL(startProgress(int,QString,double,double)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(stopProgress(int)), this, SIGNAL(stopProgress(int)));

    this->connect(d_currentBaseWorkerClass, SIGNAL(updateProgress(int,double)), this, SIGNAL(updateProgress(int,double)));

    this->connect(d_workerThread, SIGNAL(finished()), this, SIGNAL(processStopped()));

    this->connect(d_workerThread, SIGNAL(finished()), this, SLOT(deleteCurrentBaseWorkerClass()));

    this->connect(d_workerThread, SIGNAL(started()), d_currentBaseWorkerClass, SLOT(doWork()));

    d_currentBaseWorkerClass->moveToThread(d_workerThread);

    d_workerThread->start();

}
