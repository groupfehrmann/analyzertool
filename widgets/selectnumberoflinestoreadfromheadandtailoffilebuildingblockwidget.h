#ifndef SELECTNUMBEROFLINESTOREADFROMHEADANDTAILOFFILEBUILDINGBLOCKWIDGET_H
#define SELECTNUMBEROFLINESTOREADFROMHEADANDTAILOFFILEBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QSpinBox>
#include <QSettings>

#include "dialogs/basedialog.h"

namespace Ui {

class SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget;

}

class SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString());

    ~SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget();

    QSpinBox *pointerToInternalSpinBoxNumberOfLinesFromHeadOfFile();

    QSpinBox *pointerToInternalSpinBoxNumberOfLinesFromTailOfFile();

private:

    Ui::SelectNumberOfLinesToReadFromHeadAndTailOfFileBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

};

#endif // SELECTNUMBEROFLINESTOREADFROMHEADANDTAILOFFILEBUILDINGBLOCKWIDGET_H
