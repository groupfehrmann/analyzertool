#ifndef HEADER_H
#define HEADER_H

#include <QObject>
#include <QSharedDataPointer>
#include <QList>
#include <QMap>
#include <QHash>
#include <QStringList>
#include <QPair>
#include <QSet>
#include <QDataStream>
#include <QRandomGenerator>

#include <functional>
#include <algorithm>

class HeaderData : public QSharedData
{

public:

    HeaderData();

    HeaderData(const HeaderData &other);

    ~HeaderData();

    QStringList d_identifiers;

    QList<int> d_indexes;

    QList<bool> d_selectionStatus;

    QHash<QString, QList<int> > d_identifierToIndexes;

    QHash<QString, QList<int> > d_identifierToIndexes_selectedOnly;

    QMap<int, QString> d_indexToIdentifier_selectedOnly;

};

class Header : QObject
{

    Q_OBJECT

    friend class BaseDataset;

    template<typename T> friend class Dataset;

    friend class ImportDatasetFromSingleFileWorkerClass;

    friend class MergeDatasetsWorkerClass;

public:

    static QString randomIdentifier(int lengthOfRandomIdentifier);

    static QStringList randomIdentifiers(int lengthOfRandomIdentifier, int numberOfRandomIdentifiers);

    Header(QObject *parent = nullptr);

    Header(const Header &other);

    ~Header();

    bool areIndexesValid(const QList<int> &indexes) const;

    bool contains(const QString &identifier) const;

    int count(bool selectedOnly = false) const;

    void deselectAll();

    const QString &identifier(int index) const;

    const QHash<QString, QList<int> > &identifierToIndexes(bool selectedOnly = false) const;

    QStringList identifiers(bool selectedOnly = false) const;

    QStringList identifiers(const QList<int> &indexes) const;

    QPair<QStringList, QList<int> > identifiersAndIndexes(bool selectedOnly = false) const;

    QList<int> indexes(bool selectedOnly = false) const;

    QList<int> indexes(const QString &identifier, bool selectedOnly = false) const;

    QList<QList<int> > indexes(const QStringList &identifiers, bool selectedOnly = false) const;

    QList<int> indexesInContinuousList(const QStringList &identifiers, bool selectedOnly = false) const;

    bool isSelected(int index) const;

    void renameIdentifier(const QString &oldIdentifier, const QString &newIdentifier, bool selectedOnly = false);

    void select(int index);

    void select(const QList<int> &indexes);

    void selectAll();

    const QList<bool> &selectionStatuses() const;

    void setIdentifier(int index, const QString &identifier);

    void setSelectionStatus(int index, bool selectionStatus);

    void setSelectionStatuses(const QString &identifier, bool selectionStatus);

    void setSelectionStatuses(const QList<int> &indexes, bool selectionStatus);

    void setSelectionStatuses(const QStringList &identifiers, bool selectionStatus);

    void splitIdentifiers(const QString &delimiter, int indexOfTokenToKeep, bool selectedOnly = false, Qt::SplitBehavior splitBehavior = Qt::KeepEmptyParts, Qt::CaseSensitivity caseSensitivity = Qt::CaseSensitive);

    void swap(int index1, int index2, bool update = true);

    void toggleSelection(int index);

    QSet<QString> uniqueIdentifiers(bool selectedOnly = false) const;

    QSet<QString> uniqueIdentifiers(const QList<int> &indexes) const;

    Header &operator=(const Header &other);

    friend QDataStream& operator<<(QDataStream& out, const Header &header);

    friend QDataStream& operator>>(QDataStream& in, Header &header);

private:

    QSharedDataPointer<HeaderData> d_data;

    void append(const QString &identifier);

    void append(const QStringList &identifiers);

    void insert(int index, const QString &identifier);

    void insert(int index, const QStringList &identifiers);

    void prepend(const QString &identifier);

    void prepend(const QStringList &identifiers);

    void remove(int index);

    void remove(QList<int> indexes);

    void remove(const QString &identifier, bool selectedOnly = false);

    void remove(const QStringList &identifiers, bool selectedOnly = false);

    void updateInternalHashesAndMaps();

};

Q_DECLARE_TYPEINFO(Header, Q_MOVABLE_TYPE);

#endif // HEADER_H
