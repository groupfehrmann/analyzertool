#include "setaxisparameterstreewidgetitem.h"

SetAxisParametersTreeWidgetItem::SetAxisParametersTreeWidgetItem(QTreeWidget *parent, const QString &label, QAbstractAxis *axis) :
    QObject(), QTreeWidgetItem(parent), d_axis(axis), d_initialized(false)
{

    if (axis) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetAxisParametersTreeWidgetItem::SetAxisParametersTreeWidgetItem(QTreeWidgetItem *parent, const QString &label, QAbstractAxis *axis) :
   QObject(), QTreeWidgetItem(parent), d_axis(axis), d_initialized(false)
{

    if (axis) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetAxisParametersTreeWidgetItem::~SetAxisParametersTreeWidgetItem()
{

}

void SetAxisParametersTreeWidgetItem::initialize(const QString &label)
{

    this->setText(0, label);

    if (d_axis->type() == QAbstractAxis::AxisTypeLogValue) {

        d_selectBaseWidget = new QSpinBox;

        d_selectBaseWidget->setRange(2, std::numeric_limits<int>::max());

        d_selectBaseWidget->setValue(static_cast<QLogValueAxis *>(d_axis)->base());

        this->connect(d_selectBaseWidget, SIGNAL(valueChanged(int)), this, SLOT(setBase(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Base"}), 1, d_selectBaseWidget);

    }

    QTreeWidgetItem *gridlineTreeWidgetItem = new QTreeWidgetItem(this, {"Grid line"});


    QTreeWidgetItem *majorGridlineTreeWidgetItem = new QTreeWidgetItem(gridlineTreeWidgetItem, {"Major"});


    d_setGridLinePenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(majorGridlineTreeWidgetItem, "Pen", d_axis->gridLinePen());

    this->connect(d_setGridLinePenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(SetGridLinePen(QPen)));


    d_selectGridLineVisibleWidget = new QCheckBox;

    d_selectGridLineVisibleWidget->setChecked(d_axis->isGridLineVisible());

    this->connect(d_selectGridLineVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setGridLineVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(majorGridlineTreeWidgetItem, {"Visible"}), 1, d_selectGridLineVisibleWidget);


    QTreeWidgetItem *minorGridlineTreeWidgetItem = new QTreeWidgetItem(gridlineTreeWidgetItem, {"Minor"});

    d_setMinorGridLinePenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(minorGridlineTreeWidgetItem, "Pen", d_axis->minorGridLinePen());

    this->connect(d_setMinorGridLinePenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setMinorGridLinePen(QPen)));


    d_selectMinorGridLineVisibleWidget = new QCheckBox;

    d_selectMinorGridLineVisibleWidget->setChecked(d_axis->isMinorGridLineVisible());

    this->connect(d_selectMinorGridLineVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setMinorGridLineVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(minorGridlineTreeWidgetItem, {"Visible"}), 1, d_selectMinorGridLineVisibleWidget);


    QTreeWidgetItem *labelsTreeWidgetItem = new QTreeWidgetItem(this, {"Labels"});


    d_selectLabelsAngleWidget = new QSpinBox;

    d_selectLabelsAngleWidget->setRange(-360, 360);

    d_selectLabelsAngleWidget->setValue(d_axis->labelsAngle());

    this->connect(d_selectLabelsAngleWidget, SIGNAL(valueChanged(int)), this, SLOT(setLabelsAngle(int)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Angle"}), 1, d_selectLabelsAngleWidget);


    d_setLabelsBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(labelsTreeWidgetItem, "Brush", d_axis->labelsBrush());

    this->connect(d_setLabelsBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setLabelsBrush(QBrush)));


    d_selectLabelsFontWidget = new SelectFontWidget(nullptr, d_axis->labelsFont());

    this->connect(d_selectLabelsFontWidget, SIGNAL(currentFontChanged(QFont)), this, SLOT(setLabelsFont(QFont)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Font"}), 1, d_selectLabelsFontWidget);


    if (d_axis->type() == QAbstractAxis::AxisTypeValue) {

        d_selectLabelFormatWidget = new QLineEdit(static_cast<QValueAxis *>(d_axis)->labelFormat());

        this->connect(d_selectLabelFormatWidget, SIGNAL(textEdited(QString)), this, SLOT(setLabelFormat(QString)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Format"}), 1, d_selectLabelFormatWidget);

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeLogValue) {

        d_selectLabelFormatWidget = new QLineEdit(static_cast<QLogValueAxis *>(d_axis)->labelFormat());

        this->connect(d_selectLabelFormatWidget, SIGNAL(textEdited(QString)), this, SLOT(setLabelFormat(QString)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Format"}), 1, d_selectLabelFormatWidget);

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeCategory) {

        d_selectAxisLabelsPositionWidget = new QComboBox();

        d_selectAxisLabelsPositionWidget->addItem("Labels are centered to category", QCategoryAxis::AxisLabelsPositionCenter);

        d_selectAxisLabelsPositionWidget->addItem("Labels are positioned to the high end limit of the category", QCategoryAxis::AxisLabelsPositionOnValue);

        d_selectAxisLabelsPositionWidget->setCurrentIndex(static_cast<QCategoryAxis *>(d_axis)->labelsPosition());

        this->connect(d_selectAxisLabelsPositionWidget, SIGNAL(currentIndexChanged(int)), this, SLOT(setLabelsPosition(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Position"}), 1, d_selectAxisLabelsPositionWidget);

    }

    d_selectLabelsVisibleWidget = new QCheckBox;

    d_selectLabelsVisibleWidget->setChecked(d_axis->labelsVisible());

    this->connect(d_selectLabelsVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setLabelsVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(labelsTreeWidgetItem, {"Visible"}), 1, d_selectLabelsVisibleWidget);


    QTreeWidgetItem *lineTreeWidgetItem = new QTreeWidgetItem(this, {"Line"});


    d_setLinePenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(lineTreeWidgetItem, "Pen", d_axis->linePen());

    this->connect(d_setLinePenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setLinePen(QPen)));


    d_selectLineVisibleWidget = new QCheckBox;

    d_selectLineVisibleWidget->setChecked(d_axis->isLineVisible());

    this->connect(d_selectLineVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setLineVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(lineTreeWidgetItem, {"Visible"}), 1, d_selectLineVisibleWidget);


    if (d_axis->type() == QAbstractAxis::AxisTypeValue) {

        d_selectMaxWidget = new QDoubleSpinBox;

        d_selectMaxWidget->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());

        d_selectMaxWidget->setValue(static_cast<QValueAxis *>(d_axis)->max());

        this->connect(d_selectMaxWidget, SIGNAL(valueChanged(double)), this, SLOT(setMax(qreal)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Maximum"}), 1, d_selectMaxWidget);

        d_selectMaxWidget = new QDoubleSpinBox;

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeDateTime) {

        d_selectMaxDateWidget = new QDateTimeEdit(static_cast<QDateTimeAxis *>(d_axis)->max());

        this->connect(d_selectMaxDateWidget, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(setMax(QDateTime)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Maximum"}), 1, d_selectMaxDateWidget);

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeLogValue) {

        d_selectMaxWidget = new QDoubleSpinBox;

        d_selectMaxWidget->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());

        d_selectMaxWidget->setValue(static_cast<QLogValueAxis *>(d_axis)->max());

        this->connect(d_selectMaxWidget, SIGNAL(valueChanged(double)), this, SLOT(setMax(qreal)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Maximum"}), 1, d_selectMaxWidget);

        d_selectMaxWidget = new QDoubleSpinBox;

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeDateTime) {

        d_selectTickCountWidget = new QSpinBox;

        d_selectTickCountWidget->setRange(2, std::numeric_limits<int>::max());

        d_selectTickCountWidget->setValue(static_cast<QDateTimeAxis *>(d_axis)->tickCount());

        this->connect(d_selectTickCountWidget, SIGNAL(valueChanged(int)), this, SLOT(setTickCount(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Major tick count"}), 1, d_selectTickCountWidget);

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeValue) {

        d_selectMinWidget = new QDoubleSpinBox;

        d_selectMinWidget->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());

        d_selectMinWidget->setValue(static_cast<QValueAxis *>(d_axis)->min());

        this->connect(d_selectMinWidget, SIGNAL(valueChanged(double)), this, SLOT(setMin(qreal)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Minimum"}), 1, d_selectMinWidget);

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeDateTime) {

        d_selectMinDateWidget = new QDateTimeEdit(static_cast<QDateTimeAxis *>(d_axis)->min());

        this->connect(d_selectMinDateWidget, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(setMin(QDateTime)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Minimum"}), 1, d_selectMinDateWidget);

    }

    if (d_axis->type() == QAbstractAxis::AxisTypeLogValue) {

        d_selectMinWidget->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());

        d_selectMinWidget->setValue(static_cast<QLogValueAxis *>(d_axis)->max());

        this->connect(d_selectMinWidget, SIGNAL(valueChanged(double)), this, SLOT(setMin(qreal)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(this, {"Minimum"}), 1, d_selectMinWidget);

    }

    QTreeWidgetItem *shadesTreeWidgetItem = new QTreeWidgetItem(this, {"Shades"});


    d_setShadesBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(shadesTreeWidgetItem, "Brush", d_axis->titleBrush());

    this->connect(d_setShadesBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setShadesBrush(QBrush)));


    d_setShadesPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(shadesTreeWidgetItem, "Pen", d_axis->shadesPen());

    this->connect(d_setShadesPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setShadesPen(QPen)));


    d_selectShadesVisibleWidget = new QCheckBox;

    d_selectShadesVisibleWidget->setChecked(d_axis->shadesVisible());

    this->connect(d_selectShadesVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setShadesVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(shadesTreeWidgetItem, {"Visible"}), 1, d_selectShadesVisibleWidget);


    if (d_axis->type() == QAbstractAxis::AxisTypeValue) {

        QTreeWidgetItem *ticksTreeWidgetItem = new QTreeWidgetItem(this, {"Ticks"});


        d_selectTickCountWidget = new QSpinBox;

        d_selectTickCountWidget->setRange(2, std::numeric_limits<int>::max());

        d_selectTickCountWidget->setValue(static_cast<QValueAxis *>(d_axis)->tickCount());

        this->connect(d_selectTickCountWidget, SIGNAL(valueChanged(int)), this, SLOT(setTickCount(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(ticksTreeWidgetItem, {"Major count"}), 1, d_selectTickCountWidget);


        d_selectMinorTickCountWidget = new QSpinBox;

        d_selectMinorTickCountWidget->setRange(0, std::numeric_limits<int>::max());

        d_selectMinorTickCountWidget->setValue(static_cast<QValueAxis *>(d_axis)->minorTickCount());

        this->connect(d_selectMinorTickCountWidget, SIGNAL(valueChanged(int)), this, SLOT(setMinorTickCount(int)));

        this->treeWidget()->setItemWidget(new QTreeWidgetItem(ticksTreeWidgetItem, {"Minor count"}), 1, d_selectMinorTickCountWidget);

    }


    QTreeWidgetItem *titleTreeWidgetItem = new QTreeWidgetItem(this, {"Title"});

    d_setTitleBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(titleTreeWidgetItem, "Brush", d_axis->titleBrush());

    this->connect(d_setTitleBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setTitleBrush(QBrush)));


    d_selectTitleFontWidget = new SelectFontWidget(nullptr, d_axis->titleFont());

    this->connect(d_selectTitleFontWidget, SIGNAL(currentFontChanged(QFont)), this, SLOT(setTitleFont(QFont)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(titleTreeWidgetItem, {"Font"}), 1, d_selectTitleFontWidget);


    d_selectTitleWidget = new QLineEdit(d_axis->titleText());

    this->connect(d_selectTitleWidget, SIGNAL(textEdited(QString)), this, SLOT(setTitleText(QString)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(titleTreeWidgetItem, {"Text"}), 1, d_selectTitleWidget);

}

void SetAxisParametersTreeWidgetItem::SetGridLinePen(const QPen &pen)
{

    d_axis->setGridLinePen(pen);

}

void SetAxisParametersTreeWidgetItem::setGridLineVisible(bool visible)
{

    d_axis->setGridLineVisible(visible);

}

void SetAxisParametersTreeWidgetItem::setMinorGridLinePen(const QPen &pen)
{

    d_axis->setMinorGridLinePen(pen);

}

void SetAxisParametersTreeWidgetItem::setMinorGridLineVisible(bool visible)
{

    d_axis->setMinorGridLineVisible(visible);

}

void SetAxisParametersTreeWidgetItem::setLabelsAngle(int angle)
{

    d_axis->setLabelsAngle(angle);

}

void SetAxisParametersTreeWidgetItem::setLabelsBrush(const QBrush &brush)
{

    d_axis->setLabelsBrush(brush);

}

void SetAxisParametersTreeWidgetItem::setLabelsFont(const QFont &font)
{

    d_axis->setLabelsFont(font);

}

void SetAxisParametersTreeWidgetItem::setLabelsVisible(bool visible)
{

    d_axis->setLabelsVisible(visible);

}

void SetAxisParametersTreeWidgetItem::setLinePen(const QPen &pen)
{

    d_axis->setLinePen(pen);

}

void SetAxisParametersTreeWidgetItem::setLineVisible(bool visible)
{

    d_axis->setLineVisible(visible);

}

void SetAxisParametersTreeWidgetItem::setShadesBrush(const QBrush &brush)
{

    d_axis->setShadesBrush(brush);

}

void SetAxisParametersTreeWidgetItem::setShadesPen(const QPen &pen)
{

    d_axis->setShadesPen(pen);

}

void SetAxisParametersTreeWidgetItem::setShadesVisible(bool visible)
{

    d_axis->setShadesVisible(visible);

}

void SetAxisParametersTreeWidgetItem::setTitleBrush(const QBrush &brush)
{

    d_axis->setTitleBrush(brush);

}

void SetAxisParametersTreeWidgetItem::setTitleFont(const QFont &font)
{

    d_axis->setTitleFont(font);

}

void SetAxisParametersTreeWidgetItem::setTitleText(const QString &title)
{

    d_axis->setTitleText(title);

}

void SetAxisParametersTreeWidgetItem::setTitleVisible(bool visible)
{

    d_axis->setTitleVisible(visible);

}

void SetAxisParametersTreeWidgetItem::setLabelFormat(const QString &format)
{

    switch (d_axis->type()) {

        case QAbstractAxis::AxisTypeValue: static_cast<QValueAxis *>(d_axis)->setLabelFormat(format); break;

        case QAbstractAxis::AxisTypeLogValue: static_cast<QLogValueAxis *>(d_axis)->setLabelFormat(format); break;

        default: break;

    }

}

void SetAxisParametersTreeWidgetItem::setMax(qreal max)
{

    switch (d_axis->type()) {

        case QAbstractAxis::AxisTypeValue: static_cast<QValueAxis *>(d_axis)->setMax(max); break;

        case QAbstractAxis::AxisTypeLogValue: static_cast<QLogValueAxis *>(d_axis)->setMax(max); break;

        default: break;

    }

}

void SetAxisParametersTreeWidgetItem::setMin(qreal min)
{

    switch (d_axis->type()) {

        case QAbstractAxis::AxisTypeValue: static_cast<QValueAxis *>(d_axis)->setMin(min); break;

        case QAbstractAxis::AxisTypeLogValue: static_cast<QLogValueAxis *>(d_axis)->setMin(min); break;

        default: break;

    }

}

void SetAxisParametersTreeWidgetItem::setMinorTickCount(int count)
{

    switch (d_axis->type()) {

        case QAbstractAxis::AxisTypeValue: static_cast<QValueAxis *>(d_axis)->setMinorTickCount(count); break;

        default: break;

    }

}

void SetAxisParametersTreeWidgetItem::setTickCount(int count)
{

    switch (d_axis->type()) {

        case QAbstractAxis::AxisTypeValue: static_cast<QValueAxis *>(d_axis)->setTickCount(count); break;

        case QAbstractAxis::AxisTypeDateTime: static_cast<QValueAxis *>(d_axis)->setTickCount(count); break;

        default: break;

    }

}

void SetAxisParametersTreeWidgetItem::setLabelsPosition(int index)
{

    static_cast<QCategoryAxis *>(d_axis)->setLabelsPosition(static_cast<QCategoryAxis::AxisLabelsPosition>(index));

}

void SetAxisParametersTreeWidgetItem::setMax(const QDateTime &max)
{

    static_cast<QDateTimeAxis *>(d_axis)->setMax(max);

}

void SetAxisParametersTreeWidgetItem::setMin(const QDateTime &min)
{

    static_cast<QDateTimeAxis *>(d_axis)->setMin(min);

}

void SetAxisParametersTreeWidgetItem::setBase(int base)
{

    static_cast<QLogValueAxis *>(d_axis)->setBase(base);

}

void SetAxisParametersTreeWidgetItem::setAxis(const QString &label, QAbstractAxis *axis)
{

    d_axis = axis;

    if (!d_initialized)
        this->initialize(label);
    else {

        qDeleteAll(this->takeChildren());

        this->initialize(label);

    }

    d_initialized = true;

}
