#ifndef BARCHART_H
#define BARCHART_H

#include <QObject>
#include <QApplication>
#include <QChart>
#include <QBarSeries>
#include <QStackedBarSeries>
#include <QPercentBarSeries>
#include <QHorizontalBarSeries>
#include <QHorizontalStackedBarSeries>
#include <QHorizontalPercentBarSeries>
#include <QBarCategoryAxis>
#include <QBarSet>
#include <QValueAxis>
#include <QStringList>
#include <QEventLoop>

#include "charts/exportplottofile.h"
#include "charts/basechart.h"

using namespace QtCharts;

class BarChart : public BaseChart
{

    Q_OBJECT

public:

    explicit BarChart(QObject *parent = nullptr, const QString &title = QString(), const QStringList &barCategoryLabels = QStringList(), const QStringList &barSetLabels = QStringList(), const QVector<QVector<double> > &values = QVector<QVector<double> >(), QAbstractSeries::SeriesType seriesType = QAbstractSeries::SeriesTypeBar);

    explicit BarChart(QObject *parent = nullptr, const QString &title = QString(), const QStringList &barCategoryLabels = QStringList(), const QString &barSetLabel = QString(), const QVector<double> &values = QVector<double>(), QAbstractSeries::SeriesType seriesType = QAbstractSeries::SeriesTypeBar);

signals:

    void initializeChart(const QString &title, const QStringList &barCategoryLabels, const QStringList &barSetLabels, const QVector<QVector<double> > &values, QAbstractSeries::SeriesType seriesType);

private slots:

    void _initializeChart(const QString &title, const QStringList &barCategoryLabels, const QStringList &barSetLabels, const QVector<QVector<double> > &values, QAbstractSeries::SeriesType seriesType);

};

#endif // BARCHART_H
