#ifndef GENERICINVERSEMETHODWITHFIXEDEFFECTMODEL_H
#define GENERICINVERSEMETHODWITHFIXEDEFFECTMODEL_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include <cmath>
#include <limits>

#include "boost/math/distributions/normal.hpp"
#include "boost/math/distributions/chi_squared.hpp"

#include "statistics/basemetaanalysistest.h"

class GenericInverseMethodWithFixedEffectModel : public BaseMetaAnalysisTest
{

public:

    GenericInverseMethodWithFixedEffectModel(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    ~GenericInverseMethodWithFixedEffectModel();

    QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const;

    const QStringList metaAnalysisTestDescriptiveLabels() const;

protected:

    void _reset();

    static QString d_nameOfMetaAnalysisTest;

    double d_sumOfInverseVarianceWeights;

    double d_sumOfInverseVarianceWeightedEffectSizes;

    double d_sumOfInverseVarianceWeightedSquaredEffectSizes;

    double d_meanEffectSize;

    double d_standardErrorEffectSize;

    QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues();

    QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues();

    void calculateStatistic();

    void calculatedPValue();

};

#endif // GENERICINVERSEMETHODWITHFIXEDEFFECTMODEL_H
