#include "openprojectworkerclass.h"

OpenProjectWorkerClass::OpenProjectWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<OpenProjectWorkerClass::Parameters *>(parameters))
{

}

void OpenProjectWorkerClass::doWork()
{

    emit processStarted("Open project");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    d_fileIn.setFileName(d_parameters.pathToFile);

    if (!d_fileIn.open(QIODevice::ReadOnly)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\" -> " + d_fileIn.errorString());

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Opening project", 0, d_fileIn.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    QDataStream in(&d_fileIn);

    Project *project = new Project();

    in >> *project;

    emit stopProgress(0);

    emit appendLogItem(LogListModel::MESSAGE, "project name: \"" + project->name() + "\"");

    emit appendLogItem(LogListModel::MESSAGE, "number of datasets present: " + QString::number(project->numberOfDatasets()));

    for (int i = 0; i < project->numberOfDatasets(); ++i) {

        emit appendLogItem(LogListModel::MESSAGE, "dataset " + QString::number(i + 1) + ": \"" + project->baseDatasetAt(i)->name() + "\"");

        emit appendLogItem(LogListModel::MESSAGE, "\"" + project->baseDatasetAt(i)->name() + "\" -> number of rows: " + QString::number(project->baseDatasetAt(i)->header(BaseMatrix::ROW).count()));

        emit appendLogItem(LogListModel::MESSAGE, "\"" + project->baseDatasetAt(i)->name() + "\" -> number of columns: " + QString::number(project->baseDatasetAt(i)->header(BaseMatrix::COLUMN).count()));

    }

    emit projectAvailable(project);

    emit addToRecentlyOpenedFiles(project->name(), d_parameters.pathToFile);

    QThread::currentThread()->quit();

}

void OpenProjectWorkerClass::updateFileProgress()
{

    emit updateProgress(0, d_fileIn.pos());

}
