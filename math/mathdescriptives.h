#ifndef MATHDESCRIPTIVES_H
#define MATHDESCRIPTIVES_H

#include <QVector>
#include <QPair>
#include <QtConcurrent>
#include <QFutureSynchronizer>

#ifdef Q_OS_MAC
#include <Accelerate/Accelerate.h>
#endif

#include <tuple>
#include <cmath>
#include <algorithm>
#include <functional>
#include <utility>
#include <bitset>


#include "math/functionsformultithreading.h"
#include "vectoroperations.h"
#include "boost/math/distributions/normal.hpp"

namespace MathDescriptives {

    double sum(const QVector<double> &vector);

    double sumOfAbsoluteValues(const QVector<double> &vector);

    double sumOfSquaredValues(const QVector<double> &vector);

    double mean(const QVector<double> &vector);

    double meanOfAbsoluteValues(const QVector<double> &vector);

    double meanOfSquaredValues(const QVector<double> &vector);

    double variance(const QVector<double> &vector);

    double standardDeviation(const QVector<double> &vector);

    QPair<double, double> meanAndStandardDeviation(const QVector<double> &vector);

    QPair<double, double> meanAndVariance(const QVector<double> &vector);

    double l2Norm(const QVector<double> &vector);

    QVector<double> sum_vertical(const QVector<QVector<double> > &matrix);

    QVector<double> sum_vertical_multiThreaded(const QVector<QVector<double> > &matrix);

    template <typename Functor>
    QVector<double> sum_vertical_applyFunctorElementWise(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        if (matrix.isEmpty())
            return QVector<double>();

        QVector<long double> _sum_vertical(matrix.first().size(), static_cast<long double>(0.0));

        for (const QVector<double> &vector : matrix) {

            for (int i = 0; i < vector.size(); ++i)
                _sum_vertical[i] += functor(vector.at(i));

        }

        QVector<double> result(_sum_vertical.size());

        for (int i = 0; i < result.size(); ++i)
            result[i] = static_cast<double>(_sum_vertical.at(i));

        return result;

    }

    template <typename Functor>
    QVector<double> sum_vertical_applyFunctorElementWise_multiThreaded(const QVector<QVector<double> > &matrix, const Functor &functor)
    {

        if (matrix.isEmpty())
            return QVector<double>();

        QMutex mutex;

        QVector<double> _sum_vertical(matrix.first().size(), 0.0);

        auto mapFunction = [&](const QPair<int, int> &block) {

            QVector<long double> _sum_vertical_intermediateResult(matrix.first().size(), static_cast<long double>(0.0));

            for (int i = block.first; i < block.second; ++i) {

                const QVector<double> &currentRowVectorOfMatrix(matrix.at(i));

                for (int j = 0; j < currentRowVectorOfMatrix.size(); ++j)
                    _sum_vertical_intermediateResult[j] += functor(currentRowVectorOfMatrix.at(j));

            }

            mutex.lock();

            for (int i = 0; i < _sum_vertical.size(); ++i)
                _sum_vertical[i] += _sum_vertical_intermediateResult.at(i);

            mutex.unlock();

        };

        QFutureSynchronizer<void> futureSynchronizer;

        for (const QPair<int, int> &block : FunctionsForMultiThreading::createIndexBlocksForMultiThreading(matrix.size(), QThreadPool::globalInstance()->maxThreadCount()))
            futureSynchronizer.addFuture(QtConcurrent::run(mapFunction, block));

        futureSynchronizer.waitForFinished();

        return _sum_vertical;

    }

    template <typename Functor>
    QVector<double> sum_vertical_applyFunctorElementWise(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor) {

        if (matrix1.isEmpty())
            return QVector<double>();

        QVector<long double> _sum_vertical(matrix1.first().size(), static_cast<long double>(0.0));

        for (int i = 0; i < matrix1.size(); ++i) {

            const QVector<double> &currentRowVectorOfMatrix1(matrix1.at(i));

            const QVector<double> &currentRowVectorOfMatrix2(matrix2.at(i));

            for (int j = 0; j < currentRowVectorOfMatrix1.size(); ++j)
                _sum_vertical[j] += functor(currentRowVectorOfMatrix1.at(j), currentRowVectorOfMatrix2.at(j));

        }

        QVector<double> result(_sum_vertical.size());

        for (int i = 0; i < result.size(); ++i)
            result[i] = static_cast<double>(_sum_vertical.at(i));

        return result;

    }

    template <typename Functor>
    QVector<double> sum_vertical_applyFunctorElementWise_multiThreaded(const QVector<QVector<double> > &matrix1, const QVector<QVector<double> > &matrix2, const Functor &functor) {

        if (matrix1.isEmpty())
            return QVector<double>();

        QMutex mutex;

        QVector<double> _sum_vertical(matrix1.first().size(), 0.0);

        auto mapFunction = [&](const QPair<int, int> &block) {

            QVector<long double> sums_intermediateResult(matrix1.first().size(), static_cast<long double>(0.0));

            for (int i = block.first; i < block.second; ++i) {

                const QVector<double> &currentRowVectorOfMatrix1(matrix1.at(i));

                const QVector<double> &currentRowVectorOfMatrix2(matrix2.at(i));

                for (int j = 0; j < currentRowVectorOfMatrix1.size(); ++j)
                    sums_intermediateResult[j] += functor(currentRowVectorOfMatrix1.at(j), currentRowVectorOfMatrix2.at(j));

            }

            mutex.lock();

            for (int i = 0; i < _sum_vertical.size(); ++i)
                _sum_vertical[i] += sums_intermediateResult.at(i);

            mutex.unlock();

        };

        QFutureSynchronizer<void> futureSynchronizer;

        for (const QPair<int, int> &block : FunctionsForMultiThreading::createIndexBlocksForMultiThreading(matrix1.size(), QThreadPool::globalInstance()->maxThreadCount()))
            futureSynchronizer.addFuture(QtConcurrent::run(mapFunction, block));

        futureSynchronizer.waitForFinished();

        return _sum_vertical;

    }

    double minimum(const QVector<double> &vector);

    double minimumOfAbsoluteValues(const QVector<double> &vector);

    double maximum(const QVector<double> &vector);

    double maximumOfAbsoluteValues(const QVector<double> &vector);

    double minimumAbsoluteValueOnDiagonalOfSquareMatrix(const QVector<QVector<double> > &squareMatrix);

    QPair<double, double> minimumAndMaximum(const QVector<double> &vector);

    QPair<double, double> minimumAndMaximum(const QVector<QVector<double> > &matrix);

    template <typename T>
    double percentileOfPresortedVector(const QVector<T> &sortedVector, const double &percentile) {

        if (sortedVector.isEmpty())
            return std::numeric_limits<double>::quiet_NaN();

        if (sortedVector.size() == 1)
            return sortedVector.first();

        long double integerPart;

        long double fractionalPart = std::modf(static_cast<double>(sortedVector.size() - 1) * percentile, &integerPart);

        return static_cast<double>(sortedVector.at(int(integerPart))) + fractionalPart * (sortedVector.at(int(integerPart) + 1) - sortedVector.at(int(integerPart)));

    }

    double median(QVector<double> vector);

    template <typename T>
    QVector<long double> percentilesOfPresortedVector(const QVector<T> &sortedVector, const QVector<double> &percentiles) {

        QVector<long double> _percentilesOfPresortedVector;

        _percentilesOfPresortedVector.reserve(percentiles.size());

        for (const double percentile : percentiles)
            _percentilesOfPresortedVector << MathDescriptives::percentileOfPresortedVector(sortedVector, percentile);

        return _percentilesOfPresortedVector;

    }


    template <typename T>
    bool containsNanOrInf(const QVector<T> &vector) {

        for (const T &value : vector) {

            if (std::isnan(value) || std::isinf(value))
                return true;

        }

        return false;

    }

    QVector<std::tuple<double, double, double> > histogram_count_lowerBound_upperBound(QVector<double> vector, const double &lowerBound, const double &upperBound, int numberOfBins);

    QVector<std::tuple<double, double, double> > histogram_frequency_lowerBound_upperBound(const QVector<double> &vector, const double &lowerBound, const double &upperBound, int numberOfBins);

    double OneSampleWilcoxonSignedRankMinusLog10PValue(const QVector<double> &vector, const double &offset);

    template <typename T>
    QVector<unsigned long long int> counts(const QVector<T> &vector, const QVector<T> &valuesToCount) {

        QHash<T, unsigned long long int> countData;

        for (const T &valueToCount : valuesToCount)
            countData.insert(valueToCount, 0);

        for (const T &value : vector) {

            if (countData.contains(value))
                countData[value]++;

        }

        QVector<unsigned long long int> _counts;

        for (const T &valueToCount : valuesToCount)
            _counts << countData.value(valueToCount);

        return _counts;

    }

    template <typename T>
    QVector<double> percentages(const QVector<T> &vector, const QVector<T> &valuesToCount) {

        QVector<unsigned long long int> counts = MathDescriptives::counts(vector, valuesToCount);

        double totalCount = 0.0;

        for (const unsigned long long int &count : counts)
            totalCount += count;

        QVector<double> _percentages;

        for (const unsigned long long int &count : counts)
            _percentages << static_cast<double>(count) / totalCount;

        return _percentages;

    }

}

#endif // MATHDESCRIPTIVES_H
