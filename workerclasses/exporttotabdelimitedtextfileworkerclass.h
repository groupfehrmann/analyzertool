#ifndef EXPORTTOTABDELIMITEDTEXTFILEWORKERCLASS_H
#define EXPORTTOTABDELIMITEDTEXTFILEWORKERCLASS_H

#include <QString>
#include <QFile>
#include <QAbstractItemModel>
#include <QTextStream>

#include "base/baseworkerclass.h"

class ExportToTabDelimitedTextFileWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QString pathToFile;

        QString descriptionOfObjectToExport;

        QAbstractItemModel *abstractItemModel;

    };

    ExportToTabDelimitedTextFileWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    ExportToTabDelimitedTextFileWorkerClass::Parameters d_parameters;

};

#endif // EXPORTTOTABDELIMITEDTEXTFILEWORKERCLASS_H
