#include "setchartparameterstreewidgetitem.h"

SetChartParametersTreeWidgetItem::SetChartParametersTreeWidgetItem(QTreeWidget *parent, const QString &label, QChart *chart) :
    QObject(), QTreeWidgetItem(parent), d_chart(chart), d_initialized(false)
{

    if (chart) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetChartParametersTreeWidgetItem::SetChartParametersTreeWidgetItem(QTreeWidgetItem *parent, const QString &label, QChart *chart) :
   QObject(), QTreeWidgetItem(parent), d_chart(chart), d_initialized(false)
{

    if (chart) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetChartParametersTreeWidgetItem::~SetChartParametersTreeWidgetItem()
{

}

void SetChartParametersTreeWidgetItem::initialize(const QString &label)
{

    this->setText(0, label);

    QTreeWidgetItem *axesTreeWidgetItem;

    if (d_chart->chartType() == QChart::ChartTypeCartesian) {

        if (!d_chart->axes().isEmpty()) {

            axesTreeWidgetItem = new QTreeWidgetItem(this, {"Axes"});

            for (QAbstractAxis *axis : d_chart->axes()) {

                QString alignment;

                switch (axis->alignment()) {

                    case Qt::AlignLeft : alignment = "Left"; break;

                    case Qt::AlignRight : alignment = "Right"; break;

                    case Qt::AlignBottom : alignment = "Bottom"; break;

                    case Qt::AlignTop : alignment = "Top"; break;

                    default : break;

                }

                QString orientation = (axis->orientation() == Qt::Horizontal) ? " horizontal" : " vertical";

                QString type;

                switch(axis->type()) {

                    case QAbstractAxis::AxisTypeValue : type = " value axis"; break;

                    case QAbstractAxis::AxisTypeBarCategory : type = " bar category axis"; break;

                    case QAbstractAxis::AxisTypeCategory : type = " category axis"; break;

                    case QAbstractAxis::AxisTypeDateTime : type = " date/time axis"; break;

                    case QAbstractAxis::AxisTypeLogValue : type = " log value axis"; break;

                    case QAbstractAxis::AxisTypeNoAxis : ;

                }

                new SetAxisParametersTreeWidgetItem(axesTreeWidgetItem, alignment + orientation + type, axis);

            }

        }

    } else if (d_chart->chartType() == QChart::ChartTypePolar) {

        QPolarChart *polarChart = static_cast<QPolarChart *>(d_chart);

        if (!polarChart->axes().isEmpty()) {

            axesTreeWidgetItem = new QTreeWidgetItem(this, {"Axes"});

            for (QAbstractAxis *axis : polarChart->axes()) {

                QString orientation;

                switch (polarChart->axisPolarOrientation(axis)) {

                    case QPolarChart::PolarOrientationRadial : orientation = "Radial"; break;

                    case QPolarChart::PolarOrientationAngular : orientation = "Angular"; break;

                    default : break;

                }

                QString type;

                switch(axis->type()) {

                    case QAbstractAxis::AxisTypeValue : type = " value axis"; break;

                    case QAbstractAxis::AxisTypeBarCategory : type = " bar category axis"; break;

                    case QAbstractAxis::AxisTypeCategory : type = " category axis"; break;

                    case QAbstractAxis::AxisTypeDateTime : type = " date/time axis"; break;

                    case QAbstractAxis::AxisTypeLogValue : type = " log value axis"; break;

                    case QAbstractAxis::AxisTypeNoAxis : ;

                }

                new SetAxisParametersTreeWidgetItem(axesTreeWidgetItem, orientation + orientation + type, axis);

            }

        }

    }

    QTreeWidgetItem *backgroundTreeWidgetItem = new QTreeWidgetItem(this, {"Background"});


    d_setBackgroundBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(backgroundTreeWidgetItem, "Brush", d_chart->backgroundBrush());

    this->connect(d_setBackgroundBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setBackgroundBrush(QBrush)));


    d_selectDropShadowEnabledWidget = new QCheckBox;

    d_selectDropShadowEnabledWidget->setChecked(d_chart->isDropShadowEnabled());

    this->connect(d_selectDropShadowEnabledWidget, SIGNAL(toggled(bool)), this, SLOT(setDropShadowEnabled(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(backgroundTreeWidgetItem, {"Drop shadow enable"}), 1, d_selectDropShadowEnabledWidget);


    d_setBackgroundPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(backgroundTreeWidgetItem, "Pen", d_chart->backgroundPen());

    this->connect(d_setBackgroundPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setBackgroundPen(QPen)));


    d_selectBackgroundRoundnessWidget = new QDoubleSpinBox;

    d_selectBackgroundRoundnessWidget->setValue(d_chart->backgroundRoundness());

    this->connect(d_selectBackgroundRoundnessWidget, SIGNAL(valueChanged(double)), this, SLOT(setBackgroundRoundness(qreal)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(backgroundTreeWidgetItem, {"Roundness"}), 1, d_selectBackgroundRoundnessWidget);


    d_selectBackgroundVisibleWidget = new QCheckBox;

    d_selectBackgroundVisibleWidget->setChecked(d_chart->isBackgroundVisible());

    this->connect(d_selectBackgroundVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setBackgroundVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(backgroundTreeWidgetItem, {"Visible"}), 1, d_selectBackgroundVisibleWidget);


    d_setLegendParametersTreeWidgetItem = new SetLegendParametersTreeWidgetItem(this, "Legend", d_chart->legend());


    d_setMarginsParametersTreeWidgetItem = new SetMarginsParametersTreeWidgetItem(this, "Margins", d_chart->margins());

    this->connect(d_setMarginsParametersTreeWidgetItem, SIGNAL(currentMarginsChanged(QMargins)), this, SLOT(setMargins(QMargins)));


    QTreeWidgetItem *plotAreaBackgroundTreeWidgetItem = new QTreeWidgetItem(this, {"Plot area background"});


    d_setPlotAreaBackgroundBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(plotAreaBackgroundTreeWidgetItem, "Brush", d_chart->plotAreaBackgroundBrush());

    this->connect(d_setPlotAreaBackgroundBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setPlotAreaBackgroundBrush(QBrush)));


    d_setPlotAreaBackgroundPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(plotAreaBackgroundTreeWidgetItem, "Pen", d_chart->plotAreaBackgroundPen());

    this->connect(d_setPlotAreaBackgroundPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setPlotAreaBackgroundPen(QPen)));


    d_selectPlotAreaBackgroundVisibleWidget = new QCheckBox;

    d_selectPlotAreaBackgroundVisibleWidget->setChecked(d_chart->isPlotAreaBackgroundVisible());

    this->connect(d_selectPlotAreaBackgroundVisibleWidget, SIGNAL(toggled(bool)), this, SLOT(setPlotAreaBackgroundVisible(bool)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(plotAreaBackgroundTreeWidgetItem, {"Visible"}), 1, d_selectPlotAreaBackgroundVisibleWidget);


    QTreeWidgetItem *seriesTreeWidgetItem = new QTreeWidgetItem(this, {"Series"});

    for (int i = 0; i < d_chart->series().size(); ++i)
        new SetSeriesParametersTreeWidgetItem(seriesTreeWidgetItem, "Serie " + QString::number(i + 1), d_chart->series().at(i));


    QTreeWidgetItem *titleTreeWidgetItem = new QTreeWidgetItem(this, {"Title"});


    d_selectTitleWidget = new QLineEdit(d_chart->title());

    this->connect(d_selectTitleWidget, SIGNAL(textEdited(QString)), this, SLOT(setTitle(QString)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(titleTreeWidgetItem, {"Text"}), 1, d_selectTitleWidget);


    d_setTitleBackgroundBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(titleTreeWidgetItem, "Brush", d_chart->titleBrush());

    this->connect(d_setTitleBackgroundBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setTitleBrush(QBrush)));


    d_selectTitleFontWidget = new SelectFontWidget(nullptr, d_chart->titleFont());

    this->connect(d_selectTitleFontWidget, SIGNAL(currentFontChanged(QFont)), this, SLOT(setTitleFont(QFont)));

    this->treeWidget()->setItemWidget(new QTreeWidgetItem(titleTreeWidgetItem, {"Font"}), 1, d_selectTitleFontWidget);

}

void SetChartParametersTreeWidgetItem::setBackgroundBrush(const QBrush &brush)
{

    d_chart->setBackgroundBrush(brush);

}

void SetChartParametersTreeWidgetItem::setBackgroundPen(const QPen &pen)
{

    d_chart->setBackgroundPen(pen);

}

void SetChartParametersTreeWidgetItem::setBackgroundRoundness(qreal diameter)
{

    d_chart->setBackgroundRoundness(diameter);

}

void SetChartParametersTreeWidgetItem::setBackgroundVisible(bool visible)
{

    d_chart->setBackgroundVisible(visible);

}

void SetChartParametersTreeWidgetItem::setChart(const QString &label, QChart *chart)
{

    d_chart = chart;

    if (!d_initialized)
        this->initialize(label);
    else {

        qDeleteAll(this->takeChildren());

        this->initialize(label);

    }

    d_initialized = true;

}

void SetChartParametersTreeWidgetItem::setDropShadowEnabled(bool enabled)
{

    d_chart->setDropShadowEnabled(enabled);

}

void SetChartParametersTreeWidgetItem::setMargins(const QMargins &margins)
{

    d_chart->setMargins(margins);

}

void SetChartParametersTreeWidgetItem::setPlotAreaBackgroundBrush(const QBrush &brush)
{

    d_chart->setPlotAreaBackgroundBrush(brush);

}

void SetChartParametersTreeWidgetItem::setPlotAreaBackgroundPen(const QPen &pen)
{

    d_chart->setPlotAreaBackgroundPen(pen);

}

void SetChartParametersTreeWidgetItem::setPlotAreaBackgroundVisible(bool visible)
{

    d_chart->setPlotAreaBackgroundVisible(visible);

}

void SetChartParametersTreeWidgetItem::setTitle(const QString &title)
{

    d_chart->setTitle(title);

}

void SetChartParametersTreeWidgetItem::setTitleBrush(const QBrush &brush)
{

    d_chart->setTitleBrush(brush);

}

void SetChartParametersTreeWidgetItem::setTitleFont(const QFont &font)
{

    d_chart->setTitleFont(font);

}
