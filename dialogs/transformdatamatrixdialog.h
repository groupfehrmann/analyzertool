#ifndef TRANSFORMDATAMATRIXDIALOG_H
#define TRANSFORMDATAMATRIXDIALOG_H

#include <QString>
#include <QSharedPointer>

#include "base/basedataset.h"
#include "dialogs/basedialog.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/comboboxbuildingblockwidget.h"
#include "widgets/transformsequencebuildingblockwidget.h"
#include "workerclasses/transformdatamatrixworkerclass.h"

class TransformDataMatrixDialog : public BaseDialog
{

    Q_OBJECT

public:

    TransformDataMatrixDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);

    ~TransformDataMatrixDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_variables;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_items;

    ComboBoxBuildingBlockWidget *d_comboBoxBuildingBlockWidget_processOnlyValidNumbers;

    TransformSequenceBuildingBlockWidget *d_transformSequenceBuildingBlockWidget;

    bool d_selectedOnly;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // TRANSFORMDATAMATRIXDIALOG_H
