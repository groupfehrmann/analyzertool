#ifndef SETSERIESPARAMETERSTREEWIDGETITEM_H
#define SETSERIESPARAMETERSTREEWIDGETITEM_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QAbstractSeries>
#include <QAbstractBarSeries>
#include <QXYSeries>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QBarSet>
#include <QScatterSeries>
#include <QBoxPlotSeries>
#include <QBoxSet>
#include <QPieSeries>

#include "charts/setbarsetparameterswidget.h"
#include "charts/setboxsetparameterswidget.h"
#include "charts/setpiesliceparameterswidget.h"
#include "charts/setbrushparameterstreewidgetitem.h"
#include "charts/setpenparameterstreewidgetitem.h"
#include "charts/selectfontwidget.h"

using namespace QtCharts;

class SetSeriesParametersTreeWidgetItem : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetSeriesParametersTreeWidgetItem(QTreeWidget *parent = nullptr, const QString &label = "Series", QAbstractSeries *series = nullptr);

    explicit SetSeriesParametersTreeWidgetItem(QTreeWidgetItem *parent = nullptr, const QString &label = "Series", QAbstractSeries *series = nullptr);

public slots:

    void setSeries(const QString &label, QAbstractSeries *series);

private:

    void initialize(const QString &label);

    QAbstractSeries *d_series;

    bool d_initialized;

    QDoubleSpinBox *d_selectOpacityWidget;

    QDoubleSpinBox *d_selectBarWidthWidget;

    QSpinBox *d_selectLabelsAngleWidget;

    QLineEdit *d_selectLabelsFormatWidget;

    QComboBox *d_selectLabelsPositionWidget;

    QCheckBox *d_selectLabelsVisibleWidget;

    QLineEdit *d_setNameWidget;

    SetBrushParametersTreeWidgetItem *d_setBrushParametersTreeWidgetItem;

    SetPenParametersTreeWidgetItem *d_setPenParametersTreeWidgetItem;

    QCheckBox *d_selectPointLabelsClippingWidget;

    SelectColorWidget *d_selectPointLabelsColorWidget;

    SelectFontWidget *d_selectPointLabelsFontWidget;

    QLineEdit *d_selectPointLabelsFormatWidget;

    QCheckBox *d_selectPointLabelsVisibleWidget;

    QComboBox *d_selectMarkerShapeWidget;

    QDoubleSpinBox *d_selectMarkerSizeWidget;

    QCheckBox *d_selectBoxOutlineVisibleWidget;

    QDoubleSpinBox *d_selectBoxWidthWidget;

    QDoubleSpinBox *d_selectHoleSizeWidget;

    QDoubleSpinBox *d_selectHorizontalPositionWidget;

    QDoubleSpinBox *d_selectPieEndAngleWidget;

    QDoubleSpinBox *d_selectPieSizeWidget;

    QDoubleSpinBox *d_selectPieStartAngleWidget;

    QDoubleSpinBox *d_selectVerticalPositionWidget;

private slots:

    void setOpacity(const double &opacity);

    void setBarWidth(const double &width);

    void setLabelsAngle(int angle);

    void setLabelsFormat(const QString &format);

    void setLabelsPosition(int index);

    void setLabelsVisible(bool visible);

    void setName(const QString &name);

    void setBrush(const QBrush &brush);

    void setPen(const QPen &pen);

    void setPointLabelsClipping(bool enabled);

    void setPointLabelsColor(const QColor &color);

    void setPointLabelsFont(const QFont &font);

    void setPointLabelsFormat(const QString &format);

    void setPointLabelsVisible(bool visible);

    void setMarkerShape(int shape);

    void setMarkerSize(const double &size);

    void setBoxOutlineVisible(bool visible);

    void setBoxWidth(const double &width);

    void setHoleSize(const double &holeSize);

    void setHorizontalPosition(const double &relativePosition);

    void setPieEndAngle(const double &angle);

    void setPieSize(const double &relativeSize);

    void setPieStartAngle(const double &startAngle);

    void setVerticalPosition(const double &relativePosition);

};

#endif // SETSERIESPARAMETERSTREEWIDGETITEM_H
