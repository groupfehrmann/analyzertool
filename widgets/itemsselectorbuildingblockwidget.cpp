#include "itemsselectorbuildingblockwidget.h"
#include "ui_itemsselectorbuildingblockwidget.h"

ItemSelectorModel::ItemSelectorModel(QObject *parent, const QString &communicationIdentifier) :
    QAbstractListModel(parent), d_communicationIdentifier(communicationIdentifier)
{

}

void ItemSelectorModel::clear()
{

    this->beginResetModel();

    d_itemsIdentifierAndNumber.first.clear();

    d_itemsIdentifierAndNumber.second.clear();

    this->endResetModel();

}

QVariant ItemSelectorModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
        return d_itemsIdentifierAndNumber.first.at(index.row());
    if (role == Qt::UserRole)
        return d_itemsIdentifierAndNumber.second.at(index.row());
    else
        return QVariant();

}

bool ItemSelectorModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{

    if (action == Qt::IgnoreAction)
        return true;

    if (!data->hasFormat("itemsIdentifierAndNumber_" + d_communicationIdentifier))
        return false;

    if (column > 0)
        return false;

    int beginRow;

    if (row != -1)
        beginRow = row;
    else if (parent.isValid())
        beginRow = parent.row();
    else
        beginRow = this->rowCount(QModelIndex());

    QByteArray encodedData = data->data("itemsIdentifierAndNumber_" + d_communicationIdentifier);

    QDataStream stream(&encodedData, QIODevice::ReadOnly);

    int rows;

    stream >> rows;

    this->beginInsertRows(QModelIndex(), beginRow, beginRow + rows - 1);

    while (!stream.atEnd()) {

        QString identifier;

        int number;

        stream >> identifier;

        stream >> number;

        d_itemsIdentifierAndNumber.first.insert(beginRow, identifier);

        d_itemsIdentifierAndNumber.second.insert(beginRow, number);

        ++beginRow;

    }

    this->endInsertRows();

    return true;

}

Qt::ItemFlags ItemSelectorModel::flags(const QModelIndex &index) const
{

    Qt::ItemFlags defaultFlags = this->QAbstractItemModel::flags(index);

    if (index.isValid())
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
    else
        return Qt::ItemIsDropEnabled | Qt::ItemIsSelectable;

}

QMimeData *ItemSelectorModel::mimeData(const QModelIndexList &indexes) const
{

    QMimeData *mimeData = new QMimeData();

    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    stream << indexes.size();

    for (const QModelIndex &index : indexes)
        stream << data(index, Qt::DisplayRole).toString() << data(index, Qt::UserRole).toInt();

    mimeData->setData("itemsIdentifierAndNumber_" + d_communicationIdentifier, encodedData);

    return mimeData;

}

const QPair<QStringList, QList<int> > &ItemSelectorModel::itemsIdentifierAndNumber()
{

    return d_itemsIdentifierAndNumber;

}

QStringList ItemSelectorModel::mimeTypes() const
{

    QStringList types;

    types << "itemsIdentifierAndNumber_" + d_communicationIdentifier;

    return types;

}

bool ItemSelectorModel::removeRows(int row, int count, const QModelIndex &parent)
{

    Q_UNUSED(parent);

    this->beginRemoveRows(parent, row, row + count - 1);

    for (int i = row + count - 1; i >= row; --i) {

        d_itemsIdentifierAndNumber.first.removeAt(i);

        d_itemsIdentifierAndNumber.second.removeAt(i);

    }

    this->endRemoveRows();

    return true;

}

int ItemSelectorModel::rowCount(const QModelIndex &parent) const
{

    if (parent.isValid())
        return 0;
    else
        return d_itemsIdentifierAndNumber.first.count();

}

void ItemSelectorModel::setItemsIdentifierAndNumber(const QStringList &identifiers, const QList<int> &numbers)
{

    this->beginResetModel();

    d_itemsIdentifierAndNumber.first = identifiers;

    d_itemsIdentifierAndNumber.second = numbers;

    this->endResetModel();

}

void ItemSelectorModel::setItemsIdentifier(const QStringList &identifiers)
{

    QList<int> numbers;

    numbers.reserve(identifiers.size());

    for (int i = 0; i < identifiers.size(); ++i)
        numbers << i;

    this->setItemsIdentifierAndNumber(identifiers, numbers);

}

Qt::DropActions ItemSelectorModel::supportedDropActions() const
{

    return Qt::MoveAction;

}

ItemsSelectorBuildingBlockWidget::ItemsSelectorBuildingBlockWidget(BaseDialog *parent, const QString &widgetId, const QString &label, const QStringList &itemsIdentifier, const QList<int> &itemsNumber) :
    QWidget(parent), ui(new Ui::ItemsSelectorBuildingBlockWidget)
{

    ui->setupUi(this);

    ui->label->setText(label);

    if (parent)
        d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    ui->splitter->setStretchFactor(0, 0);

    ui->splitter->setStretchFactor(1, 1);

    d_itemSelectorModel_notSelected = new ItemSelectorModel(this, widgetId);

    d_itemSelectorModel_selected = new ItemSelectorModel(this, widgetId);

    QSortFilterProxyModel *sortFilterProxyModel = new QSortFilterProxyModel(this);

    sortFilterProxyModel->setSourceModel(d_itemSelectorModel_notSelected);

    ui->listView_notSelected->setModel(sortFilterProxyModel);

    ui->listView_selected->setModel(d_itemSelectorModel_selected);

    this->connect(ui->lineEdit_filter, SIGNAL(textChanged(QString)), sortFilterProxyModel, SLOT(setFilterWildcard(QString)));

    if (itemsIdentifier.isEmpty())
        d_rememberSettings = false;
    else
        d_rememberSettings = true;

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/itemsSelectorBuildingBlockWidget");

    if (settings.contains("splitter/state"))
        ui->splitter->restoreState(settings.value("splitter/state").toByteArray());

    if (d_rememberSettings) {

        if (settings.contains("itemsNotSelected") && settings.contains("itemsSelected")) {

            d_itemSelectorModel_notSelected->setItemsIdentifierAndNumber(settings.value("itemsNotSelected").value<QPair<QStringList, QList<int> > >().first, settings.value("itemsNotSelected").value<QPair<QStringList, QList<int> > >().second);

            d_itemSelectorModel_selected->setItemsIdentifierAndNumber(settings.value("itemsSelected").value<QPair<QStringList, QList<int> > >().first, settings.value("itemsSelected").value<QPair<QStringList, QList<int> > >().second);

        } else {

            if (itemsNumber.isEmpty())
                this->setItemsIdentifier(itemsIdentifier);
            else
                this->setItemsIdentifierAndNumber(itemsIdentifier, itemsNumber);

        }

    }

    settings.endGroup();

    this->connect(d_itemSelectorModel_selected, &ItemSelectorModel::rowsInserted, this, [=](){emit this->selectionChanged(d_itemSelectorModel_selected->itemsIdentifierAndNumber().first, d_itemSelectorModel_notSelected->itemsIdentifierAndNumber().first);});

    this->connect(d_itemSelectorModel_selected, &ItemSelectorModel::rowsRemoved, this, [=](){emit this->selectionChanged(d_itemSelectorModel_selected->itemsIdentifierAndNumber().first, d_itemSelectorModel_notSelected->itemsIdentifierAndNumber().first);});

    this->connect(d_itemSelectorModel_selected, &ItemSelectorModel::rowsMoved, this, [=](){emit this->selectionChanged(d_itemSelectorModel_selected->itemsIdentifierAndNumber().first, d_itemSelectorModel_notSelected->itemsIdentifierAndNumber().first);});

}

ItemsSelectorBuildingBlockWidget::~ItemsSelectorBuildingBlockWidget()
{

    QSettings settings;

    settings.beginGroup(d_parentIdentifierForSettings + "/itemsSelectorBuildingBlockWidget");

    settings.setValue("splitter/state", ui->splitter->saveState());

    if (d_rememberSettings) {

        settings.setValue("itemsNotSelected", QVariant::fromValue(d_itemSelectorModel_notSelected->itemsIdentifierAndNumber()));

        settings.setValue("itemsSelected", QVariant::fromValue(d_itemSelectorModel_selected->itemsIdentifierAndNumber()));

    }

    settings.endGroup();

    delete ui;

}

void ItemsSelectorBuildingBlockWidget::clear()
{

    d_itemSelectorModel_notSelected->clear();

    d_itemSelectorModel_selected->clear();

}

const QPair<QStringList, QList<int> > &ItemsSelectorBuildingBlockWidget::selectedItemsIdentifierAndNumber()
{

    return d_itemSelectorModel_selected->itemsIdentifierAndNumber();

}

void ItemsSelectorBuildingBlockWidget::setItemsIdentifier(const QStringList &identifiers)
{

    d_itemSelectorModel_notSelected->setItemsIdentifier(identifiers);

    d_itemSelectorModel_selected->setItemsIdentifierAndNumber(QStringList(), QList<int>());

}

void ItemsSelectorBuildingBlockWidget::setItemsIdentifierAndNumber(const QPair<QStringList, QList<int> > &itemsIdentifierAndNumber)
{

    d_itemSelectorModel_notSelected->setItemsIdentifierAndNumber(itemsIdentifierAndNumber.first, itemsIdentifierAndNumber.second);

    d_itemSelectorModel_selected->setItemsIdentifierAndNumber(QStringList(), QList<int>());

}

void ItemsSelectorBuildingBlockWidget::setItemsIdentifierAndNumber(const QStringList &identifiers, const QList<int> &numbers)
{

    d_itemSelectorModel_notSelected->setItemsIdentifierAndNumber(identifiers, numbers);

    d_itemSelectorModel_selected->setItemsIdentifierAndNumber(QStringList(), QList<int>());

}
