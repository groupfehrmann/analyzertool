#include "deduplicateidentifiersworkerclass.h"

DeduplicateIdentifiersWorkerClass::DeduplicateIdentifiersWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<DeduplicateIdentifiersWorkerClass::Parameters *>(parameters))
{

}

void DeduplicateIdentifiersWorkerClass::doWork()
{
    emit processStarted("Deduplicate identifiers");

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of identifiers selected: " + QString::number(d_parameters.selectedIdentifiersAndIndexes.first.size()));

    switch (d_parameters.mode) {

        case KEEPRANDOM: emit appendLogItem(LogListModel::PARAMETER, "mode: keep random identifier"); break;

        case KEEPFIRST: emit appendLogItem(LogListModel::PARAMETER, "mode: keep first identifier"); break;

        case KEEPLAST: emit appendLogItem(LogListModel::PARAMETER, "mode: keep last identifier"); break;

        default: break;

    }

    emit startProgress(0, "Detecting duplicate identifiers", 0, d_parameters.selectedIdentifiersAndIndexes.second.size());

    QHash<QString, QList<int> > identifierToIndexes;

    for (int i = 0; i < d_parameters.selectedIdentifiersAndIndexes.first.size(); ++i) {

        identifierToIndexes[d_parameters.selectedIdentifiersAndIndexes.first.at(i)] << d_parameters.selectedIdentifiersAndIndexes.second.at(i);

        if (QThread::currentThread()->isInterruptionRequested()) {


            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Determining which duplicate identifiers to remove", 0, identifierToIndexes.size());

    int counter = 0;

    QVector<QVector<QVariant> > logData;

    QStringList logDataVerticalHeader;

    QHashIterator<QString, QList<int> > it(identifierToIndexes);

    QList<int> indexesToRemove;

    while (it.hasNext()) {

        it.next();

        ++counter;

        if (it.value().size() > 1) {

            logDataVerticalHeader << QString::number(logData.size() + 1);

            std::sort(identifierToIndexes[it.key()].begin(), identifierToIndexes[it.key()].end());

            QVector<QVariant> _logData;

            _logData << it.key() << ConvertFunctions::toString(it.value());

            if (d_parameters.mode == KEEPFIRST) {

                _logData << identifierToIndexes[it.key()].first();

                identifierToIndexes[it.key()].removeFirst();

            } else if (d_parameters.mode == KEEPLAST) {

                _logData << identifierToIndexes[it.key()].last();

                identifierToIndexes[it.key()].removeLast();

            } else {

                std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());

                std::uniform_real_distribution<double> distribution(0, it.value().size());

                int randomIndex = distribution(generator);

                _logData << identifierToIndexes[it.key()].at(randomIndex);

                identifierToIndexes[it.key()].removeAt(randomIndex);

            }

            indexesToRemove << identifierToIndexes[it.key()];

            logData << _logData;

            if (QThread::currentThread()->isInterruptionRequested()) {

                QThread::currentThread()->quit();

                return;

            }

            emit updateProgress(0, counter);

        }

    }

    emit matrixDataBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    emit annotationsBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    emit startProgress(0, "Removing duplicates", 0, 0);

    d_parameters.baseDataset->removeVectors(indexesToRemove, d_parameters.orientation);

    emit stopProgress(0);

    emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    if (!this->processTableResult("Deduplicate identifiers log", {"identifier", "mapped indexes", "index kept"}, logDataVerticalHeader, logData, QString()))
        return;

    QThread::currentThread()->quit();

}
