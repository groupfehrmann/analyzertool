#ifndef SELECTPENSTYLEWIDGET_H
#define SELECTPENSTYLEWIDGET_H

#include <QWidget>
#include <QPen>
#include <QPixmap>
#include <QPainter>

namespace Ui {

class SelectPenStyleWidget;

}

class SelectPenStyleWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectPenStyleWidget(QWidget *parent = 0, Qt::PenStyle defaultPenStyle = Qt::SolidLine);

    ~SelectPenStyleWidget();

    Qt::PenStyle currentPenStyle();

public slots:

    void setCurrentPenStyle(Qt::PenStyle penStyle);

private:

    Ui::SelectPenStyleWidget *ui;

    QPixmap createPixmap(Qt::PenStyle penStyle);

private slots:

    void currentIndexOfComboBoxChanged(int index);

signals:

    void currentPenStyleChanged(Qt::PenStyle penStyle);

};

#endif // SELECTPENSTYLEWIDGET_H
