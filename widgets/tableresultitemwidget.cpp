#include "tableresultitemwidget.h"
#include "ui_tableresultitemwidget.h"

TableResultItemWidget::TableResultItemWidget(QWidget *parent, QSharedPointer<TableResultItem> tableResultItem) :
    QWidget(parent), ui(new Ui::TableResultItemWidget), d_tableResultItem(tableResultItem)
{

    ui->setupUi(this);


    TableResultItemSortFilterProxyModel *tableResultItemSortFilterProxyModel = new TableResultItemSortFilterProxyModel(this);

    TableResultItemModel *tableResultItemModel = new TableResultItemModel(this, d_tableResultItem);

    tableResultItemSortFilterProxyModel->setSourceModel(tableResultItemModel);

    ui->tableView->setModel(tableResultItemSortFilterProxyModel);

}

TableResultItemWidget::~TableResultItemWidget()
{
    delete ui;
}

QAbstractItemModel *TableResultItemWidget::currentTableModel()
{

    return ui->tableView->model();

}

QTableView *TableResultItemWidget::currentTableView()
{

    return ui->tableView;

}

