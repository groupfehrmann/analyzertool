#ifndef STOUFFERSTRENDMETHOD_H
#define STOUFFERSTRENDMETHOD_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QSharedPointer>

#include <cmath>
#include <limits>

#include "boost/math/distributions/normal.hpp"

#include "statistics/basemetaanalysistest.h"

class StouffersTrendMethod : public BaseMetaAnalysisTest
{

public:

    StouffersTrendMethod(const QVector<QSharedPointer<BaseStatisticalTest> > &baseStatisticalTests);

    ~StouffersTrendMethod();

    QStringList baseStatisticalTestDescriptiveLabels(const QStringList &subsetIdentifiers) const;

    const QStringList metaAnalysisTestDescriptiveLabels() const;

protected:

    void _reset();

    static QStringList d_baseStatisticalTestDescriptiveLabels;

    static QStringList d_metaAnalysisTestDescriptiveLabels;

    static QString d_nameOfMetaAnalysisTest;

    QVector<QVariant> calculateBaseStatisticalTestDescriptiveValues();

    QVector<QVariant> calculateMetaAnalysisTestDescriptiveValues();

    void calculateStatistic();

    void calculatedPValue();

};

#endif // STOUFFERSTRENDMETHOD_H
