#include "genesetenrichmentanalysisworkerclass.h"

GeneSetEnrichmentAnalysisWorkerClass::GeneSetEnrichmentAnalysisWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<GeneSetEnrichmentAnalysisWorkerClass::Parameters *>(parameters))
{

}

bool GeneSetEnrichmentAnalysisWorkerClass::checkInputParameters()
{

    if (d_parameters.exportDirectory.isEmpty() || !QDir(d_parameters.exportDirectory).exists()) {

        emit appendLogItem(LogListModel::ERROR, "no valid export directory : \"" + d_parameters.exportDirectory + "\"");

        return false;

    }

    if (d_parameters.selectedVariableIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no ranked gene lists selected");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no probes/genes selected");

        return false;

    }

    if (d_parameters.geneSetFiles.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "no gene set definitions files selected");

        return false;

    }

    for (const QString &geneSetFile : d_parameters.geneSetFiles) {

        if (!QFile(geneSetFile).exists()) {

            emit appendLogItem(LogListModel::ERROR, "no valid gene set definition file : \"" + geneSetFile + "\"");

            return false;

        }

    }

    if (d_parameters.minimumSizeOfGeneSet > d_parameters.maximumSizeOfGeneSet) {

        emit appendLogItem(LogListModel::ERROR, "minimum number of genes in a gene set can not be higher than the maximum number of genes");

        return false;

    }

    if (QSet<QString>(d_parameters.selectedItemIdentifiersAndIndexes.first.begin(), d_parameters.selectedItemIdentifiersAndIndexes.first.end()).size() != d_parameters.selectedItemIdentifiersAndIndexes.first.size()) {

        emit appendLogItem(LogListModel::ERROR, "duplicate probe/gene identifiers are not allowed");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.size() < d_parameters.minimumSizeOfGeneSet * 2) {

        emit appendLogItem(LogListModel::ERROR, "number of probe/gene identifiers should be at least a factor 2 higher than the minimum number of genes in a gene set");

        return false;

    }

    if (d_parameters.selectedItemIdentifiersAndIndexes.first.size() < 4) {

        emit appendLogItem(LogListModel::ERROR, "number of probe/gene identifiers should be at least 4");

        return false;

    }

    QList<QString> annotationValues;

    d_parameters.baseDataset->annotationValues(d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.annotationLabelUsedToMatch, BaseMatrix::switchOrientation(d_parameters.orientation), annotationValues);

    if (annotationValues.size() != QSet<QString>(annotationValues.begin(), annotationValues.end()).size()) {

        emit appendLogItem(LogListModel::ERROR, "detected duplicate identifiers for annotation label used to match between geneset members and components");

        QHash<QString, int> identifierToCounter;

        for (const QString &annotationValue : annotationValues)
            identifierToCounter[annotationValue]++;

        for (const QString &annotationValue : annotationValues) {

            if (identifierToCounter.value(annotationValue) != 1)
                emit appendLogItem(LogListModel::ERROR, annotationValue + " -> " + QString::number(identifierToCounter.value(annotationValue)));

        }

        return false;

    }

    if (d_parameters.annotationLabelsDefiningSearchableKeys.isEmpty()) {

        emit appendLogItem(LogListModel::ERROR, "At least one annotation label is required to create searchable keys");

        return false;

    }

    return true;

}

void GeneSetEnrichmentAnalysisWorkerClass::doWork()
{

    emit processStarted("gene set enrichment analysis");

    if (!this->checkInputParameters()) {

        QThread::currentThread()->quit();

        return;

    }

    if (d_parameters.exportDirectory.isEmpty())
        emit resultAvailable(new Result(nullptr, "Generate gene network"));

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of ranked gene lists selected: " + QString::number(d_parameters.selectedVariableIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "number of probes/genes selected: " + QString::number(d_parameters.selectedItemIdentifiersAndIndexes.first.size()));

    for (const QString &geneSetfile : d_parameters.geneSetFiles)
        emit appendLogItem(LogListModel::PARAMETER, "gene set definitions file selected: \"" + geneSetfile + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "annotation label used to match between members of genesets and the ranked gene lists data: \"" + d_parameters.annotationLabelUsedToMatch + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "test to define enrichment: \"" + d_parameters.testToDefineEnrichment + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "minimum number of genes in a gene set definition: " + QString::number(d_parameters.minimumSizeOfGeneSet));

    emit appendLogItem(LogListModel::PARAMETER, "maximum number of genes in a gene set: " + QString::number(d_parameters.maximumSizeOfGeneSet));

    emit appendLogItem(LogListModel::PARAMETER, "permutation mode: " + (d_parameters.performPermutations ? QString("enabled") : QString("disabled")));

    if (d_parameters.performPermutations)
        emit appendLogItem(LogListModel::PARAMETER, "number of permutation rounds: " + QString::number(d_parameters.numberOfPermutationRounds));

    emit appendLogItem(LogListModel::PARAMETER, "output format: \"" + d_parameters.outputFormat + "\"");

    if (!d_parameters.exportDirectory.isEmpty())
        emit appendLogItem(LogListModel::PARAMETER, "export directory: \"" + d_parameters.exportDirectory + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "export geneset member files: " + (d_parameters.exportGeneSetMemberFiles ? QString("enabled") : QString("disabled")));


    emit startProgress(0, "Constructing probe/gene identifier to index mapping", 0, d_parameters.selectedItemIdentifiersAndIndexes.second.size());

    for (int i = 0; i < d_parameters.selectedItemIdentifiersAndIndexes.second.size(); ++i) {

        QString geneIdentifier = d_parameters.baseDataset->annotationValue(d_parameters.selectedItemIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelUsedToMatch, BaseMatrix::switchOrientation(d_parameters.orientation)).toString();

        d_geneIdentifiers << geneIdentifier;

        d_geneIdentifierToIndex.insert(geneIdentifier, i);

        QList<QVariant> annotationValues = d_parameters.baseDataset->annotationValues(d_parameters.selectedItemIdentifiersAndIndexes.second.at(i), d_parameters.annotationLabelsDefiningSearchableKeys, BaseMatrix::switchOrientation(d_parameters.orientation));

        QString temp = annotationValues.first().toString();

        QStringList tokenizedAnnotations;

        tokenizedAnnotations << annotationValues.first().toString().remove('"');

        for (int j = 1; j < annotationValues.size(); ++j) {

            temp += " \\\\\\\\\\\\ " + annotationValues.at(j).toString();

            tokenizedAnnotations << annotationValues.at(j).toString().remove('"');
        }

        temp.remove('"');

        d_geneAnnotations.append(temp);

        d_tokenizedGeneAnnotations.append(tokenizedAnnotations);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    if (d_parameters.outputFormat == "Tab-delimited")
        this->writeGeneSearchableKeysToIndex_tabDelimited(d_parameters.exportDirectory);
    else if (d_parameters.outputFormat == "JSON")
        this->writeGeneSearchableKeysToIndex_json(d_parameters.exportDirectory);

    emit startProgress(0, "Importing gene set definitions files", 0, d_parameters.geneSetFiles.size());

    QFile fileOut;

    if (d_parameters.outputFormat == "Tab-delimited")
        fileOut.setFileName(d_parameters.exportDirectory + "/dataBaseToIndex.txt");
    else if (d_parameters.outputFormat == "JSON")
        fileOut.setFileName(d_parameters.exportDirectory + "/dataBaseToIndex.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        if (d_parameters.outputFormat == "Tab-delimited")
            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/dataBaseToIndex.txt\"");
        else if (d_parameters.outputFormat == "JSON")
            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/dataBaseToIndex.json\"");

        QThread::currentThread()->quit();

        return;

    }

    QTextStream out(&fileOut);

    bool first = true;

    for (int i = 0; i < d_parameters.geneSetFiles.size(); ++i) {

        if (!this->readGeneSetFile(d_parameters.geneSetFiles.at(i))) {

            QThread::currentThread()->quit();

            return;

        }

        QString prettyPrintFileName = QFileInfo(d_parameters.geneSetFiles.at(i)).baseName();

        if (d_parameters.outputFormat == "Tab-delimited")
            out << prettyPrintFileName << "\t" << i + 1 << Qt::endl;
        else if (d_parameters.outputFormat == "JSON") {

            if (first) {

                out << "[" << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << i + 1 << "," << Qt::endl << "\t\t\"label\":\"" << prettyPrintFileName << "\"" << Qt::endl << "\t}";

                first = false;

            } else
                out << "," << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << i + 1 << "," << Qt::endl << "\t\t\"label\":\"" << prettyPrintFileName << "\"" << Qt::endl << "\t}";


        }

        emit updateProgress(0, i + 1);

    }

    if (d_parameters.outputFormat == "JSON")
        out << Qt::endl << "]";

    fileOut.close();

    emit stopProgress(0);

    emit startProgress(0, "Exporting ranked genes list identifier to index", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    QFile fileOut2;

    if (d_parameters.outputFormat == "Tab-delimited")
        fileOut2.setFileName(d_parameters.exportDirectory + "/rankedGenesListIdentifierToIndex.txt");
    else if (d_parameters.outputFormat == "JSON")
        fileOut2.setFileName(d_parameters.exportDirectory + "/rankedGenesListIdentifierToIndex.json");

    if (!fileOut2.open(QIODevice::WriteOnly | QIODevice::Text)) {

        if (d_parameters.outputFormat == "Tab-delimited")
            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/rankedGenesListIdentifierToIndex.txt\"");
        else if (d_parameters.outputFormat == "JSON")
            emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.exportDirectory + "/rankedGenesListIdentifierToIndex.json\"");

        QThread::currentThread()->quit();

        return;

    }

    QTextStream out2(&fileOut2);

    first = true;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++i) {

        if (d_parameters.outputFormat == "Tab-delimited")
            out2 << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) << "\t" << i + 1 << Qt::endl;
        else if (d_parameters.outputFormat == "JSON") {

            if (first) {

                out2 << "[" << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << i + 1 << "," << Qt::endl << "\t\t\"label\":\"" << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) << "\"" << Qt::endl << "\t}";

                first = false;

            } else
                out2 << "," << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << i + 1 << "," << Qt::endl << "\t\t\"label\":\"" << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(i) << "\"" << Qt::endl << "\t}";


        }

        emit updateProgress(0, i + 1);

    }

    if (d_parameters.outputFormat == "JSON")
        out2 << Qt::endl << "]";

    fileOut2.close();

    emit stopProgress(0);

    emit startProgress(0, "Fetching data", 0, d_parameters.selectedVariableIdentifiersAndIndexes.first.size());

    QVector<QVector<double> > rankedGenesLists;

    for (int i = 0; i < d_parameters.selectedVariableIdentifiersAndIndexes.second.size(); ++i) {

        rankedGenesLists << d_parameters.baseDataset->baseMatrix()->vectorOfDouble(d_parameters.selectedVariableIdentifiersAndIndexes.second.at(i), d_parameters.selectedItemIdentifiersAndIndexes.second, d_parameters.orientation);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Sorting ranked genes lists", 0, rankedGenesLists.size());

    QVector<QVector<QPair<int, double> > > sortedRankedGenesListsWithOriginalIndex;
            
    QVector<QVector<int> > geneIndexToRanks(d_parameters.selectedItemIdentifiersAndIndexes.second.size(), QVector<int>(rankedGenesLists.size(), std::numeric_limits<int>::quiet_NaN()));

    for (int i = 0; i < rankedGenesLists.size(); ++i) {

        const QVector<double> &currentRankedGenesList(rankedGenesLists.at(i));

        QVector<QPair<int, double> > temp;

        temp.reserve(currentRankedGenesList.size());

        for (int j = 0; j < currentRankedGenesList.size(); ++j) {

            const double &currentValue(currentRankedGenesList.at(j));

            if (!std::isnan(currentValue))
                temp.append(QPair<int, double>(j, currentRankedGenesList.at(j)));

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        std::sort(temp.begin(), temp.end(), [](const QPair<int, double> &element1, const QPair<int, double> &element2){ return element1.second > element2.second;});

        if (d_parameters.outputFormat == "Tab-delimited")
            this->writeSortedRankedGenesList_tabDelimited(i, temp, d_parameters.exportDirectory);
        else if (d_parameters.outputFormat == "JSON")
            this->writeSortedRankedGenesList_json(i, temp, d_parameters.exportDirectory);

        sortedRankedGenesListsWithOriginalIndex.append(temp);

        for (int j = 0; j < temp.size(); ++j)
            geneIndexToRanks[temp.at(j).first][i] = j + 1;
            
        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Exporting gene rank info", 0, rankedGenesLists.size());

    MatrixOperations::transpose_inplace(rankedGenesLists);

    for (int i = 0; i < rankedGenesLists.size(); ++i) {

        if (d_parameters.outputFormat == "Tab-delimited")
            this->writeGeneRankInfoToFile_tabDelimited(i, rankedGenesLists, geneIndexToRanks, d_parameters.exportDirectory);
        else if (d_parameters.outputFormat == "JSON")
            this->writeGeneRankInfoToFile_json(i, rankedGenesLists, geneIndexToRanks, d_parameters.exportDirectory);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }
    emit stopProgress(0);

    emit startProgress(0, "Transposing", 0, 0);

    MatrixOperations::transpose_inplace(rankedGenesLists);

    emit stopProgress(0);

    std::function<double (const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers)> enrichmentFunction;

    if (d_parameters.performPermutations) {

        if (d_parameters.testToDefineEnrichment == "Student T")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); StudentT<double> studentT(filtered.first, filtered.second); return studentT.statistic();};
        else if (d_parameters.testToDefineEnrichment == "Welch T")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); WelchT<double> welchT(filtered.first, filtered.second); return welchT.statistic();};
        else if (d_parameters.testToDefineEnrichment == "Mann-Whitney U")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); MannWhitneyU<double> mannWhitneyU(filtered.first, filtered.second); return mannWhitneyU.statistic();};
        else if (d_parameters.testToDefineEnrichment == "Kolmogorov-Smirnov Z")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); KolmogorovsmirnovZ<double> kolmogorovsmirnovZ(filtered.first, filtered.second); return kolmogorovsmirnovZ.statistic();};
        else if (d_parameters.testToDefineEnrichment == "Fisher exact")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); FisherExactGSEA<double> fisherExactGSEA1(filtered.first, filtered.second, 3.0); FisherExactGSEA<double> fisherExactGSEA2(filtered.first, filtered.second, -3.0); return ((fisherExactGSEA1.statistic() > fisherExactGSEA2.statistic()) ? fisherExactGSEA1.statistic() : fisherExactGSEA2.statistic());};

    } else {

        if (d_parameters.testToDefineEnrichment == "Student T")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); StudentT<double> studentT(filtered.first, filtered.second); return studentT.zTransformedPValue();};
        else if (d_parameters.testToDefineEnrichment == "Welch T")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); WelchT<double> welchT(filtered.first, filtered.second); return welchT.zTransformedPValue();};
        else if (d_parameters.testToDefineEnrichment == "Mann-Whitney U")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); MannWhitneyU<double> mannWhitneyU(filtered.first, filtered.second); return mannWhitneyU.zTransformedPValue();};
        else if (d_parameters.testToDefineEnrichment == "Kolmogorov-Smirnov Z")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); KolmogorovsmirnovZ<double> kolmogorovsmirnovZ(filtered.first, filtered.second); return kolmogorovsmirnovZ.zTransformedPValue();};
        else if (d_parameters.testToDefineEnrichment == "Fisher exact")
            enrichmentFunction = [&](const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) {QPair<QVector<double>, QVector<unsigned int> > filtered = filterOutNaNs(measurements, itemSampleCodedIdentifiers); FisherExactGSEA<double> fisherExactGSEA1(filtered.first, filtered.second, 3.0); FisherExactGSEA<double> fisherExactGSEA2(filtered.first, filtered.second, -3.0); return ((std::fabs(fisherExactGSEA1.zTransformedPValue()) > std::fabs(fisherExactGSEA2.zTransformedPValue())) ? fisherExactGSEA1.zTransformedPValue() : -fisherExactGSEA2.zTransformedPValue());};

    }

    QThread *mainThreadOfWorkerClass = QThread::currentThread();

    QHash<int, QVector<QVector<double> > > geneSetSizeToRankedGenesListToEmpiricalNullDistribution;

    if (d_parameters.performPermutations) {

        std::random_device rd;

        std::mt19937 g(rd());

        double progressValue = 0.0;

        QMutex mutex;

        auto constructRankedGenesListEmpiricalNullDistribution = [&](const int &sizeOfGeneset) {

            QVector<QVector<double> > rankedGenesListToEmpiricalNullDistribution;

            rankedGenesListToEmpiricalNullDistribution.reserve(d_parameters.numberOfPermutationRounds);

            QVector<unsigned int> itemSampleCodedIdentifiers(d_geneIdentifierToIndex.size(), 1);

            for (int k = 0; k < sizeOfGeneset; ++k)
                itemSampleCodedIdentifiers[k] = 0;

            for (int permutationRound = 0; permutationRound < d_parameters.numberOfPermutationRounds; ++permutationRound) {

                mutex.lock();

                std::shuffle(itemSampleCodedIdentifiers.begin(), itemSampleCodedIdentifiers.end(), g);

                mutex.unlock();

                QVector<double> permutationEnrichmentVector;

                for (const QVector<double> &rankedGenesList : rankedGenesLists) {

                    permutationEnrichmentVector << std::fabs(enrichmentFunction(rankedGenesList, itemSampleCodedIdentifiers));

                    if (mainThreadOfWorkerClass->isInterruptionRequested())
                        return rankedGenesListToEmpiricalNullDistribution;

                }

                rankedGenesListToEmpiricalNullDistribution.append(permutationEnrichmentVector);

                mutex.lock();

                progressValue += 1.0;

                mutex.unlock();
            }

            MatrixOperations::transpose_inplace(rankedGenesListToEmpiricalNullDistribution);

            for (QVector<double> &empiricalNullDistribution : rankedGenesListToEmpiricalNullDistribution)
                std::sort(empiricalNullDistribution.begin(), empiricalNullDistribution.end(), std::greater<double>());

            return rankedGenesListToEmpiricalNullDistribution;

        };

        QFuture<QVector<QVector<double > > > future1 = QtConcurrent::mapped(FunctionsForMultiThreading::createSequenceOfIndexes(d_parameters.maximumSizeOfGeneSet - d_parameters.minimumSizeOfGeneSet + 1, d_parameters.minimumSizeOfGeneSet, 1), std::function<QVector<QVector<double> >(const int &)>(constructRankedGenesListEmpiricalNullDistribution));

        this->monitorFutureAndWaitForFinish(future1, 0, "Constructing empirical null distributions", progressValue, 0.0, (d_parameters.maximumSizeOfGeneSet - d_parameters.minimumSizeOfGeneSet + 1) * d_parameters.numberOfPermutationRounds);

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        for (int j = 0; j < future1.resultCount(); ++j)
            geneSetSizeToRankedGenesListToEmpiricalNullDistribution[j + d_parameters.minimumSizeOfGeneSet] = future1.resultAt(j);

    }

    emit startProgress(0, "Processing gene set databases", 0, d_parameters.geneSetFiles.size());

    for (int i = 0; i < d_parameters.geneSetFiles.size(); ++i) {

        QDir(d_parameters.exportDirectory).mkdir("dataBase_" + QString::number(i + 1));

        QString currentDirectory = d_parameters.exportDirectory + "/" + "dataBase_" + QString::number(i + 1);

        QStringList geneSetIdentifiers = d_databaseToGeneSetIdentifiers.at(i);

        const QStringList &geneSetDescriptions(d_databaseToGeneSetDescriptions.at(i));

        const QVector<QSet<QString> > &geneSetToGeneSetMembers(d_databaseToGeneSetToGeneSetMembers.at(i));


        auto calculateEnrichment = [&](const int &indexOfGeneSet) {

            const QSet<QString> &geneSetMembers(geneSetToGeneSetMembers.at(indexOfGeneSet));

            QVector<unsigned int> itemSampleCodedIdentifiers(d_geneIdentifierToIndex.size(), 1);

            for (const QString &geneSetMember : geneSetMembers)
                itemSampleCodedIdentifiers[d_geneIdentifierToIndex.value(geneSetMember)] = 0;

            QVector<EnrichmentDataElement> enrichmentDataElements;

            for (int j = 0; j < rankedGenesLists.size(); ++j) {

                const QVector<double> &currentRankedGenesList(rankedGenesLists.at(j));

                if (d_parameters.exportGeneSetMemberFiles) {

                    if (d_parameters.outputFormat == "Tab-delimited")
                        this->writeGeneSetMembersToFile_tabDelimited(indexOfGeneSet, j, geneSetMembers, sortedRankedGenesListsWithOriginalIndex.at(j), currentDirectory);
                    else if (d_parameters.outputFormat == "JSON")
                        this->writeGeneSetMembersToFile_json(indexOfGeneSet, j, geneSetMembers, sortedRankedGenesListsWithOriginalIndex.at(j), currentDirectory);

                }

                EnrichmentDataElement enrichmentDataElement;

                enrichmentDataElement.geneSetIndex = indexOfGeneSet;

                enrichmentDataElement.rankedGenesListIndex = j;

                enrichmentDataElement.databaseIndex = i;

                enrichmentDataElement.numberOfGenesInGeneSet = geneSetMembers.size();

                if (d_parameters.performPermutations) {

                    const QVector<double> &empiricalNullDistribution(geneSetSizeToRankedGenesListToEmpiricalNullDistribution.value(geneSetMembers.size()).at(j));

                    double temp = std::fabs(enrichmentFunction(currentRankedGenesList, itemSampleCodedIdentifiers));

                    int counter = 0;

                    while (empiricalNullDistribution.at(counter) < temp)
                        ++counter;

                    enrichmentDataElement.zTransformedPValue = static_cast<double>(counter) / d_parameters.numberOfPermutationRounds;

                } else
                    enrichmentDataElement.zTransformedPValue = enrichmentFunction(rankedGenesLists.at(j), itemSampleCodedIdentifiers);

                enrichmentDataElements.append(enrichmentDataElement);

            }

            if (mainThreadOfWorkerClass->isInterruptionRequested())
                return enrichmentDataElements;


            QVector<EnrichmentDataElement *> enrichmentDataElementsPointers;

            for (int j = 0; j < enrichmentDataElements.size(); ++j)
                enrichmentDataElementsPointers.append(&enrichmentDataElements[j]);

            std::sort(enrichmentDataElementsPointers.begin(), enrichmentDataElementsPointers.end(), [](EnrichmentDataElement *element1, EnrichmentDataElement *element2){ return std::fabs(element1->zTransformedPValue) > std::fabs(element2->zTransformedPValue);});

            if (d_parameters.outputFormat == "Tab-delimited")
                this->writeEnrichmentFromGeneSetPerspectiveToFile_tabDelimited(indexOfGeneSet, d_parameters.selectedVariableIdentifiersAndIndexes.first, enrichmentDataElementsPointers, currentDirectory);
            else if (d_parameters.outputFormat == "JSON")
                this->writeEnrichmentFromGeneSetPerspectiveToFile_json(indexOfGeneSet, d_parameters.selectedVariableIdentifiersAndIndexes.first, enrichmentDataElementsPointers, currentDirectory);

            return enrichmentDataElements;

        };

        QFuture<QVector<EnrichmentDataElement> > future2 = QtConcurrent::mapped(FunctionsForMultiThreading::createSequenceOfIndexes(geneSetIdentifiers.size(), 0, 1), std::function<QVector<EnrichmentDataElement>(const int &)>(calculateEnrichment));

        this->monitorFutureAndWaitForFinish(future2, 1, "Calculating enrichment");

        if (QThread::currentThread()->isInterruptionRequested()) {

            QThread::currentThread()->quit();

            return;

        }

        QVector<QVector<EnrichmentDataElement> > results;

        for (int j = 0; j < future2.resultCount(); ++j)
            results << future2.resultAt(j);

        if (d_parameters.outputFormat == "Tab-delimited")
            this->writeMatrixWithZTransformedPValues_tabDelimited(i, results, d_parameters.exportDirectory, d_parameters.selectedVariableIdentifiersAndIndexes.first, geneSetIdentifiers, geneSetDescriptions);
        else if (d_parameters.outputFormat == "JSON")
            this->writeMatrixWithZTransformedPValues_json(i, results, d_parameters.exportDirectory, d_parameters.selectedVariableIdentifiersAndIndexes.first, geneSetIdentifiers, geneSetDescriptions);

        QFile fileOut3;

        if (d_parameters.outputFormat == "Tab-delimited")
            fileOut3.setFileName(currentDirectory + "/geneSetToIndex.txt");
        else if (d_parameters.outputFormat == "JSON")
            fileOut3.setFileName(currentDirectory + "/geneSetToIndex.json");

        if (!fileOut3.open(QIODevice::WriteOnly | QIODevice::Text)) {

            if (d_parameters.outputFormat == "Tab-delimited")
                emit appendLogItem(LogListModel::ERROR, "could not open file \"" + currentDirectory + "/geneSetToIndex.txt\"");
            else if (d_parameters.outputFormat == "JSON")
                emit appendLogItem(LogListModel::ERROR, "could not open file \"" + currentDirectory + "/geneSetToIndex.json\"");

            QThread::currentThread()->quit();

            return;

        }

        QTextStream out3(&fileOut3);

        bool first = true;

        for (int j = 0; j < geneSetIdentifiers.size(); ++j) {

            if (d_parameters.outputFormat == "Tab-delimited")
                out3 << geneSetIdentifiers.at(j) << " \\\\\\ " << geneSetDescriptions.at(j) << "\t" << j << Qt::endl;
            else if (d_parameters.outputFormat == "JSON") {

                if (first) {

                    out3 << "[" << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << j + 1 << "," << Qt::endl << "\t\t\"label\":\"" << geneSetIdentifiers.at(j) << " \\\\\\\\\\\\ " << geneSetDescriptions.at(j) << "\"" << Qt::endl << "\t}";

                    first = false;

                } else
                    out3 << "," << Qt::endl << "\t{" << Qt::endl << "\t\t\"value\":" << j + 1 << "," << Qt::endl << "\t\t\"label\":\"" << geneSetIdentifiers.at(j) << " \\\\\\\\\\\\ " << geneSetDescriptions.at(j) << "\"" << Qt::endl << "\t}";

            }

        }

        if (d_parameters.outputFormat == "JSON")
            out3 << Qt::endl << "]";

        fileOut3.close();

        MatrixOperations::transpose_inplace_multiThreaded(results);

        for (int j = 0; j < d_parameters.selectedVariableIdentifiersAndIndexes.first.size(); ++j) {

            QVector<EnrichmentDataElement> &result(results[j]);

            std::sort(result.begin(), result.end(), [](EnrichmentDataElement element1, EnrichmentDataElement element2){ return std::fabs(element1.zTransformedPValue) > std::fabs(element2.zTransformedPValue);});

            if (d_parameters.outputFormat == "Tab-delimited")
                this->writeEnrichmentFromRankedGenesListPerspectiveToFile_tabDelimited(j, geneSetIdentifiers, geneSetDescriptions, result, currentDirectory);
            else if (d_parameters.outputFormat == "JSON")
                this->writeEnrichmentFromRankedGenesListPerspectiveToFile_json(j, geneSetIdentifiers, geneSetDescriptions, result, currentDirectory);

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    QThread::currentThread()->quit();

}

bool GeneSetEnrichmentAnalysisWorkerClass::readGeneSetFile(const QString &geneSetFile)
{

    d_file.setFileName(geneSetFile);

    if (!d_file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file: \"" + geneSetFile + "\"");

        return false;

    }

    emit startProgress(1, "Importing gene set file \"" + geneSetFile + "\"", 1, d_file.size());

    this->connect(d_timer, SIGNAL(timeout()), this, SLOT(updateFileProgress()), Qt::DirectConnection);

    d_timerThread.start();

    int numberOfLinesRead = 0;

    QSet<QString> geneSetIdentifiersSet;

    QStringList geneSetIdentifiers;

    QStringList geneSetDescriptions;

    QVector<QSet<QString> > geneSetToGeneSetMembers;

    while (!d_file.atEnd()) {

        QStringList tokens = QString(d_file.readLine()).remove(QRegExp("[\r\n]")).split("\t", Qt::SkipEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() < 3) {

            emit appendLogItem(LogListModel::ERROR, "error at line number " + QString::number(numberOfLinesRead) + " in gene set file \"" + geneSetFile + "\", not enough tokens read");

            d_file.close();

            return false;

        }

        QString geneSetIdentifier = tokens.at(0);

        QString geneSetDescription = tokens.at(1);

        QSet<QString> geneSetMembers;

        for (int i = 2; i < tokens.size(); ++i) {

            if (d_geneIdentifierToIndex.contains(tokens.at(i))) {

                if (!geneSetMembers.contains(tokens.at(i)))
                    geneSetMembers.insert(tokens.at(i));
                else
                    emit appendLogItem(LogListModel::WARNING, "check gene set definitions in file \"" + geneSetFile + "\" at line " + QString::number(numberOfLinesRead) + ", gene set with identifier \"" + geneSetIdentifier + "\" conatins duplicate members with identifier \"" + tokens.at(i) + "\"");

            }

        }

        if ((geneSetMembers.size() >= d_parameters.minimumSizeOfGeneSet) && (geneSetMembers.size() <= d_parameters.maximumSizeOfGeneSet)) {

            if (geneSetIdentifiersSet.contains(geneSetIdentifier)) {

                emit appendLogItem(LogListModel::WARNING, "duplicate gene set identifier \"" + geneSetIdentifier + "\" in gene set file \"" + geneSetFile + "\"");

                d_file.close();

                return false;

            }

            geneSetIdentifiers.append(geneSetIdentifier);

            geneSetDescriptions.append(geneSetDescription.replace('/', "\/"));

            geneSetToGeneSetMembers.append(geneSetMembers);

        } else
            emit appendLogItem(LogListModel::WARNING, "exluded gene set from gene set file \"" + geneSetFile + "\", identifier \"" + geneSetIdentifier + "\", description \"" + geneSetDescription + "\", number of members " + QString::number(geneSetMembers.size()) + ", members " + ConvertFunctions::toString(geneSetMembers.values()));

        if (QThread::currentThread()->isInterruptionRequested()) {

            d_file.close();

            return false;

        }

    }

    d_file.close();

    d_databaseToGeneSetIdentifiers.append(geneSetIdentifiers);

    d_databaseToGeneSetDescriptions.append(geneSetDescriptions);

    d_databaseToGeneSetToGeneSetMembers.append(geneSetToGeneSetMembers);

    emit stopProgress(1);

    emit appendLogItem(LogListModel::MESSAGE, "imported " + QString::number(geneSetIdentifiers.size()) + " gene sets from \"" + geneSetFile + "\"");

    return true;

}

void GeneSetEnrichmentAnalysisWorkerClass::updateFileProgress()
{

    emit updateProgress(1, d_file.pos());

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeEnrichmentFromGeneSetPerspectiveToFile_tabDelimited(int geneSetIndex, const QStringList &rankedGenesListIdentifiers, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/enrichmentGeneSet_" + QString::number(geneSetIndex + 1) + ".txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/enrichmentGeneSet_" + QString::number(geneSetIndex + 1) + ".txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "Ranked genes list identifier\t" << "Z-score" << Qt::endl;

    for (EnrichmentDataElement *enrichmentDataElement : enrichmentDataElements) {

        if (!std::isnan(enrichmentDataElement->zTransformedPValue))
            out << rankedGenesListIdentifiers.at(enrichmentDataElement->rankedGenesListIndex) << "\t" << enrichmentDataElement->zTransformedPValue << Qt::endl;

    }

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeEnrichmentFromGeneSetPerspectiveToFile_json(int geneSetIndex, const QStringList &rankedGenesListIdentifiers, QVector<EnrichmentDataElement *> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/enrichmentGeneSet_" + QString::number(geneSetIndex + 1) + ".json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/enrichmentGeneSet_" + QString::number(geneSetIndex + 1) + ".json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (EnrichmentDataElement *enrichmentDataElement : enrichmentDataElements) {

        if (!std::isnan(enrichmentDataElement->zTransformedPValue)) {

            if (first) {

                out << "\t{\"id\":\"" << rankedGenesListIdentifiers.at(enrichmentDataElement->rankedGenesListIndex) << "\",\"zTransformedPValue\":" << enrichmentDataElement->zTransformedPValue << ",\"geneSetIndex\":" << enrichmentDataElement->geneSetIndex + 1 << ",\"rankedGenesListIndex\":" << enrichmentDataElement->rankedGenesListIndex + 1 << "}";

                first = false;

            } else
                out << "," << Qt::endl << "\t{\"id\":\"" << rankedGenesListIdentifiers.at(enrichmentDataElement->rankedGenesListIndex) << "\",\"zTransformedPValue\":" << enrichmentDataElement->zTransformedPValue << ",\"geneSetIndex\":" << enrichmentDataElement->geneSetIndex + 1 << ",\"rankedGenesListIndex\":" << enrichmentDataElement->rankedGenesListIndex + 1 << "}";

        }

    }

    out << Qt::endl << "]}";

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeEnrichmentFromRankedGenesListPerspectiveToFile_tabDelimited(int rankedGenesListIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/enrichmentRankedGenesList_" + QString::number(rankedGenesListIndex + 1) + ".txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/enrichmentRankedGenesList_" + QString::number(rankedGenesListIndex + 1) + ".txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "Gene set identifier\t" << "Gene set description\t" << "Number of genes in gene set\t" << "Z-score" << Qt::endl;

    for (EnrichmentDataElement enrichmentDataElement : enrichmentDataElements) {

        if (!std::isnan(enrichmentDataElement.zTransformedPValue))
            out << geneSetIdentifiers.at(enrichmentDataElement.geneSetIndex) << "\t" << geneSetDescriptions.at(enrichmentDataElement.geneSetIndex) << "\t" << enrichmentDataElement.numberOfGenesInGeneSet << "\t" << enrichmentDataElement.zTransformedPValue << Qt::endl;

    }

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeEnrichmentFromRankedGenesListPerspectiveToFile_json(int rankedGenesListIndex, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions, QVector<EnrichmentDataElement> enrichmentDataElements, const QString &path)
{

    QFile fileOut(path + "/enrichmentRankedGenesList_" + QString::number(rankedGenesListIndex + 1) + ".json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/enrichmentRankedGenesList_" + QString::number(rankedGenesListIndex + 1) + ".json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (EnrichmentDataElement enrichmentDataElement : enrichmentDataElements) {

        if (!std::isnan(enrichmentDataElement.zTransformedPValue)) {

            if (first) {

                out << "\t{\"id\":\"" << geneSetIdentifiers.at(enrichmentDataElement.geneSetIndex) << "\",\"description\":\"" << geneSetDescriptions.at(enrichmentDataElement.geneSetIndex) << "\",\"nGenes\":" << enrichmentDataElement.numberOfGenesInGeneSet << ",\"zTransformedPValue\":" << enrichmentDataElement.zTransformedPValue << ",\"geneSetIndex\":" << enrichmentDataElement.geneSetIndex + 1 << ",\"rankedGenesListIndex\":" << enrichmentDataElement.rankedGenesListIndex + 1 << "}";

                first = false;

            } else
                out << "," << Qt::endl << "\t{\"id\":\"" << geneSetIdentifiers.at(enrichmentDataElement.geneSetIndex) << "\",\"description\":\"" << geneSetDescriptions.at(enrichmentDataElement.geneSetIndex) << "\",\"nGenes\":" << enrichmentDataElement.numberOfGenesInGeneSet << ",\"zTransformedPValue\":" << enrichmentDataElement.zTransformedPValue << ",\"geneSetIndex\":" << enrichmentDataElement.geneSetIndex + 1 << ",\"rankedGenesListIndex\":" << enrichmentDataElement.rankedGenesListIndex + 1 << "}";

        }

    }

    out << Qt::endl << "]}";

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeGeneSearchableKeysToIndex_tabDelimited(const QString &path)
{

    QFile fileOut(path + "/geneToIndex.txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneToIndex.txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    for (int i = 0; i < d_geneAnnotations.size(); ++i)
        out << d_geneAnnotations.at(i) << "\t" << i + 1 << Qt::endl;


    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeGeneSearchableKeysToIndex_json(const QString &path)
{

    QFile fileOut(path + "/geneToIndex.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneToIndex.json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "[";

    out << Qt::endl << "\t" << "{" << Qt::endl;

    out << "\t\t" << "\"value\":" << 1 << "," << Qt::endl;

    out << "\t\t" << "\"label\":\"";

    out << d_geneAnnotations.first();

    out << "\"" << Qt::endl;

    out << "\t}";

    for (int i = 1; i < d_geneAnnotations.size(); ++i) {

        out << "," << Qt::endl << "\t" << "{" << Qt::endl;

        out << "\t\t" << "\"value\":" << i + 1 << "," << Qt::endl;

        out << "\t\t" << "\"label\":\"";

        out << d_geneAnnotations.at(i);

        out << "\"" << Qt::endl;

        out << "\t}";

    }

    out << Qt::endl << "]";

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeGeneSetMembersToFile_tabDelimited(int geneSetIndex, int rankedGenesListIndex, const QSet<QString> &geneSetMembers, const QVector<QPair<int, double> > &sortedRankedGenesListWithOriginalIndex, const QString &path)
{

    QFile fileOut(path + "/geneSet_" + QString::number(geneSetIndex + 1) + "_rankedGenesList_" + QString::number(rankedGenesListIndex + 1) + "_geneSetMembersAndValues.txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneSet_" + QString::number(geneSetIndex + 1) + "_rankedGenesList_" + QString::number(rankedGenesListIndex + 1) + "_geneSetMembersAndValues.txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "Gene identifiers\t"  << "Metric\t" << "Rank from top\t" << "Percentile from top\t" << "Rank from bottom\t" << "Percentile from bottom" << Qt::endl;

    for (int i = 0; i < sortedRankedGenesListWithOriginalIndex.size(); ++i) {

        const QPair<int, double> &currentElement(sortedRankedGenesListWithOriginalIndex.at(i));

        int index = currentElement.first;

        double value = currentElement.second;

        if (geneSetMembers.contains(d_geneIdentifiers.at(index)))
            out << d_geneAnnotations.at(index) << "\t" << value << "\t" << i + 1 << "\t" << static_cast<double>(i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << "\t" << sortedRankedGenesListWithOriginalIndex.size() - i + 1 << "\t" << static_cast<double>(sortedRankedGenesListWithOriginalIndex.size() - i + 1) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << Qt::endl;

    }

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeGeneSetMembersToFile_json(int geneSetIndex, int rankedGenesListIndex, const QSet<QString> &geneSetMembers, const QVector<QPair<int, double> > &sortedRankedGenesListWithOriginalIndex, const QString &path)
{

    QFile fileOut(path + "/geneSet_" + QString::number(geneSetIndex + 1) + "_rankedGenesList_" + QString::number(rankedGenesListIndex + 1) + "_geneSetMembersAndValues.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneSet_" + QString::number(geneSetIndex + 1) + "_rankedGenesList_" + QString::number(rankedGenesListIndex + 1) + "_geneSetMembersAndValues.json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (int i = 0; i < sortedRankedGenesListWithOriginalIndex.size(); ++i) {

        const QPair<int, double> &currentElement(sortedRankedGenesListWithOriginalIndex.at(i));

        int index = currentElement.first;

        double value = currentElement.second;

        if (geneSetMembers.contains(d_geneIdentifiers.at(index))) {

            if (first) {

                out << "\t{\"id\":\"" << d_geneAnnotations.at(index) << "\",\"value\":" << value << ",\"rankFromTop\":" << i + 1 << ",\"percentileFromTop\":" << static_cast<double>(i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"rankFromBottom\":" << sortedRankedGenesListWithOriginalIndex.size() - i + 1 << ",\"percentileFromBottom\":" << static_cast<double>(sortedRankedGenesListWithOriginalIndex.size() - i + 1) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"geneIndex\":" << index << "}";

                first = false;

            } else
                out << "," << Qt::endl << "\t{\"id\":\"" << d_geneAnnotations.at(index) << "\",\"value\":" <<  value << ",\"rankFromTop\":" << i + 1 << ",\"percentileFromTop\":" << static_cast<double>(i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"rankFromBottom\":" << sortedRankedGenesListWithOriginalIndex.size() - i + 1 << ",\"percentileFromBottom\":" << static_cast<double>(sortedRankedGenesListWithOriginalIndex.size() - i + 1) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"geneIndex\":" << index << "}";

        }

    }

    out << Qt::endl << "]}";

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeGeneRankInfoToFile_tabDelimited(int geneIndex, const QVector<QVector<double> > &transposedRankedGenesLists, const QVector<QVector<int> > &geneIndexToRanks, const QString &path)
{

    QFile fileOut(path + "/geneRankInfo_" + QString::number(geneIndex + 1) + ".txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneRankInfo_" + QString::number(geneIndex + 1) + ".txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "Ranked genes list identifier\t" << "Metric\t" << "Rank from top\t" << "Percentile from top\t" << "Rank from bottom\t" << "Percentile from bottom" << Qt::endl;

    const QVector<double> &currentRowInTransposedRankedGenesLists(transposedRankedGenesLists.at(geneIndex));

    QVector<QPair<int, double> > sortedMetricsWithOriginalIndex;

    for (int i = 0; i < currentRowInTransposedRankedGenesLists.size(); ++i)
        sortedMetricsWithOriginalIndex.append(QPair<int, double>(i, currentRowInTransposedRankedGenesLists.at(i)));

    std::sort(sortedMetricsWithOriginalIndex.begin(), sortedMetricsWithOriginalIndex.end(), [](const QPair<int, double> &element1, const QPair<int, double> &element2){ return element1.second > element2.second;});

    for (int i = 0; i < sortedMetricsWithOriginalIndex.size(); ++i) {

        int index = sortedMetricsWithOriginalIndex.at(i).first;

        double currentRank = static_cast<double>(geneIndexToRanks.at(geneIndex).at(index));

        if (!std::isnan(currentRowInTransposedRankedGenesLists.at(index)))
            out << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(index) << "\t" << currentRowInTransposedRankedGenesLists.at(index) << "\t" << currentRank << "\t" << currentRank / static_cast<double>(transposedRankedGenesLists.size()) << "\t" << transposedRankedGenesLists.size() - currentRank + 1 << "\t" << (transposedRankedGenesLists.size() - currentRank + 1) / static_cast<double>(transposedRankedGenesLists.size()) << Qt::endl;

    }

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeGeneRankInfoToFile_json(int geneIndex, const QVector<QVector<double> > &transposedRankedGenesLists, const QVector<QVector<int> > &geneIndexToRanks, const QString &path)
{

    QFile fileOut(path + "/geneRankInfo_" + QString::number(geneIndex + 1) + ".json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneRankInfo_" + QString::number(geneIndex + 1) + ".json\"");

        return false;

    }

    const QVector<double> &currentRowInTransposedRankedGenesLists(transposedRankedGenesLists.at(geneIndex));

    QVector<QPair<int, double> > sortedMetricsWithOriginalIndex;

    for (int i = 0; i < currentRowInTransposedRankedGenesLists.size(); ++i)
        sortedMetricsWithOriginalIndex.append(QPair<int, double>(i, currentRowInTransposedRankedGenesLists.at(i)));

    std::sort(sortedMetricsWithOriginalIndex.begin(), sortedMetricsWithOriginalIndex.end(), [](const QPair<int, double> &element1, const QPair<int, double> &element2){ return element1.second > element2.second;});

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (int i = 0; i < currentRowInTransposedRankedGenesLists.size(); ++i) {

        int index = sortedMetricsWithOriginalIndex.at(i).first;

        double currentRank = static_cast<double>(geneIndexToRanks.at(geneIndex).at(index));

        if (!std::isnan(currentRowInTransposedRankedGenesLists.at(index))) {

            if (first) {

                out << "\t{\"id\":\"" << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(index) << "\",\"value\":" << currentRowInTransposedRankedGenesLists.at(index) << ",\"rankFromTop\":" << currentRank << ",\"percentileFromTop\":" << currentRank / static_cast<double>(transposedRankedGenesLists.size()) << ",\"rankFromBottom\":" << transposedRankedGenesLists.size() - currentRank + 1<< ",\"percentileFromBottom\":" << (transposedRankedGenesLists.size() - currentRank + 1) / static_cast<double>(transposedRankedGenesLists.size()) << ",\"rankedGenesListIndex\":" << index + 1 << "}";

                first = false;

            } else
                out << "," << Qt::endl << "\t{\"id\":\"" << d_parameters.selectedVariableIdentifiersAndIndexes.first.at(index) << "\",\"value\":" <<  currentRowInTransposedRankedGenesLists.at(index) << ",\"rankFromTop\":" << currentRank << ",\"percentileFromTop\":" << currentRank / static_cast<double>(transposedRankedGenesLists.size()) << ",\"rankFromBottom\":" << transposedRankedGenesLists.size() - currentRank + 1 << ",\"percentileFromBottom\":" << (transposedRankedGenesLists.size() - currentRank + 1) / static_cast<double>(transposedRankedGenesLists.size()) << ",\"rankedGenesListIndex\":" << index + 1 <<"}";

        }

    }

    out << Qt::endl << "]}";

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeSortedRankedGenesList_tabDelimited(int rankedGenesListIndex, const QVector<QPair<int, double> > sortedRankedGenesListWithOriginalIndex, const QString &path)
{

    QFile fileOut(path + "/sortedRankedGeneList_" + QString::number(rankedGenesListIndex + 1) + ".txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneRankInfo_" + QString::number(rankedGenesListIndex + 1) + ".txt\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "Gene identifiers\t"  << "Metric\t" << "Rank from top\t" << "Percentile from top\t" << "Rank from bottom\t" << "Percentile from bottom" << Qt::endl;

    for (int i = 0; i < sortedRankedGenesListWithOriginalIndex.size(); ++i) {

        const QPair<int, double> &currentElement(sortedRankedGenesListWithOriginalIndex.at(i));

        int index = currentElement.first;

        double value = currentElement.second;

        out << d_geneAnnotations.at(index) << "\t" << value << "\t" << i + 1 << "\t" << static_cast<double>(i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << "\t" << sortedRankedGenesListWithOriginalIndex.size() - i << "\t" << static_cast<double>(sortedRankedGenesListWithOriginalIndex.size() - i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << Qt::endl;

    }

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeSortedRankedGenesList_json(int rankedGenesListIndex, const QVector<QPair<int, double> > sortedRankedGenesListWithOriginalIndex, const QString &path)
{

    QFile fileOut(path + "/sortedRankedGeneList_" + QString::number(rankedGenesListIndex + 1) + ".json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/geneRankInfo_" + QString::number(rankedGenesListIndex + 1) + ".json\"");

        return false;

    }

    QTextStream out(&fileOut);

    out << "{\"rows\": [" << Qt::endl;

    bool first = true;

    for (int i = 0; i < sortedRankedGenesListWithOriginalIndex.size(); ++i) {

        const QPair<int, double> &currentElement(sortedRankedGenesListWithOriginalIndex.at(i));

        int index = currentElement.first;

        double value = currentElement.second;

        if (first) {

            out << "\t{\"id\":\"" << d_geneAnnotations.at(index) << "\",\"value\":" << value << ",\"rankFromTop\":" << i + 1 << ",\"percentileFromTop\":" << static_cast<double>(i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"rankFromBottom\":" << sortedRankedGenesListWithOriginalIndex.size() - i<< ",\"percentileFromBottom\":" << static_cast<double>(sortedRankedGenesListWithOriginalIndex.size() - i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"geneIndex\":" << index + 1 << "}";

            first = false;

        } else
            out << "," << Qt::endl << "\t{\"id\":\"" << d_geneAnnotations.at(index) << "\",\"value\":" <<  value << ",\"rankFromTop\":" << i + 1 << ",\"percentileFromTop\":" << static_cast<double>(i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"rankFromBottom\":" << sortedRankedGenesListWithOriginalIndex.size() - i << ",\"percentileFromBottom\":" << static_cast<double>(sortedRankedGenesListWithOriginalIndex.size() - i) / static_cast<double>(sortedRankedGenesListWithOriginalIndex.size()) << ",\"geneIndex\":" << index + 1 << "}";

    }

    out << Qt::endl << "]}";

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeMatrixWithZTransformedPValues_tabDelimited(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > & enrichmentDataElements, const QString &path, const QStringList &rankedGenesListIdentifiers, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions)
{

    QFile fileOut(path + "/" + QString::number(databaseIndex + 1) + "_zTransformedPValues.txt");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/" + QString::number(databaseIndex + 1) + "_zTransformedPValues.txt");

        return false;

    }

    QTextStream out(&fileOut);

    out << "GeneSet identifier" << "\t" << "GeneSet description";

    for (const QString &rankedGenesListIdentifier : rankedGenesListIdentifiers)
        out << "\t" << rankedGenesListIdentifier;

    out << Qt::endl;

    for (int i = 0; i < geneSetIdentifiers.size(); ++i)
        out << geneSetIdentifiers.at(i) << "\t" << geneSetDescriptions.at(i) << Qt::endl;

    for (int i = 0; i < enrichmentDataElements.size(); ++i) {

        out << geneSetIdentifiers.at(i) << "\t" << geneSetDescriptions.at(i);

        const QVector<EnrichmentDataElement> &temp(enrichmentDataElements.at(i));

        for (int j = 0; j < temp.size(); ++j) {

            const double &value(temp.at(j).zTransformedPValue);

            if (std::isnan(value))
                out << "\t" << 0.0;
            else
                out << "\t" << temp.at(j).zTransformedPValue;

        }

        out << Qt::endl;
    }

    fileOut.close();

    return true;

}

bool GeneSetEnrichmentAnalysisWorkerClass::writeMatrixWithZTransformedPValues_json(int databaseIndex, const QVector<QVector<EnrichmentDataElement> > & enrichmentDataElements, const QString &path, const QStringList &rankedGenesListIdentifiers, const QStringList &geneSetIdentifiers, const QStringList &geneSetDescriptions)
{

    Q_UNUSED(geneSetDescriptions)

    QFile fileOut(path + "/" + QString::number(databaseIndex + 1) + "_zTransformedPValues.json");

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + path + "/" + QString::number(databaseIndex + 1) + "_zTransformedPValues.json");

        return false;

    }

    QTextStream out(&fileOut);

    out << "{" << Qt::endl << "\t" << "\"columnLabels\": [" << Qt::endl;

    bool first = true;

    for (const QString &rankedGenesListIdentifier : rankedGenesListIdentifiers) {

        if (first) {

            out << "\t\t\"" << rankedGenesListIdentifier << "\"";

            first = false;

        } else
            out << ", \"" << rankedGenesListIdentifier << "\"";

    }

    out << Qt::endl << "]," << Qt::endl;

    out << "\t" << "\"rowLabels\": [" << Qt::endl;

    first = true;

    for (const QString &geneSetIdentifier : geneSetIdentifiers) {

        if (first) {

            out << "\t\t\"" << geneSetIdentifier << "\"";

            first = false;

        } else
            out << ", \"" << geneSetIdentifier << "\"";

    }

    out << Qt::endl << "]," << Qt::endl;

    out << "\t\"data\": [";

    first = true;

    for (int i = 0; i < enrichmentDataElements.size(); ++i) {

        const QVector<EnrichmentDataElement> &temp(enrichmentDataElements.at(i));

        for (int j = 0; j < temp.size(); ++j) {

            double value = temp.at(j).zTransformedPValue;

            if (std::isnan(value))
                value = 0.0;
            else
                value = temp.at(j).zTransformedPValue;


            if (first) {

                out << Qt::endl << "\t\t{\"row\":" << i << ",\"column\":" << j << ",\"value\":" << value << "}";

                first = false;

            } else
                out << "," << Qt::endl << "\t\t{\"row\":" << i << ",\"column\":" << j << ",\"value\":" << value << "}";

        }

    }

    out << Qt::endl << "\t]" << Qt::endl << "}";

    fileOut.close();

    return true;

}

QPair<QVector<double>, QVector<unsigned int> > GeneSetEnrichmentAnalysisWorkerClass::filterOutNaNs(const QVector<double> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers)
{

    QVector<double> filteredMeasurements;

    QVector<unsigned int> filteredItemSampleCodedIdentifiers;

    for (int i = 0; i < measurements.size(); ++i) {

        const double &value(measurements.at(i));

        if (!std::isnan(value)) {

            filteredMeasurements.append(value);

            filteredItemSampleCodedIdentifiers.append(itemSampleCodedIdentifiers.at(i));

        }

    }

    return QPair<QVector<double>, QVector<unsigned int> >(filteredMeasurements, filteredItemSampleCodedIdentifiers);

}
