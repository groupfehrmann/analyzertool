#ifndef PROJECT_H
#define PROJECT_H

#include <QObject>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QHash>
#include <QSharedPointer>
#include <QUuid>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>

#include "base/basedataset.h"
#include "base/dataset.h"

class ProjectData : public QSharedData
{

public:

    ProjectData();

    ProjectData(const ProjectData &other);

    ~ProjectData();

    QHash<QUuid, QSharedPointer<BaseDataset> > d_uuidToBaseDataset;

    QList<QUuid> d_uuidOfBaseDatasets;

    QString d_name;

};

class Project : public QObject
{

    Q_OBJECT

public:

    Project(QObject *parent = nullptr);

    Project(const Project &other);

    ~Project();

    const QSharedPointer<BaseDataset> baseDatasetAt(const QUuid &uuid) const;

    const QSharedPointer<BaseDataset> baseDatasetAt(int index) const;

    QList<QSharedPointer<BaseDataset> > baseDatasets();

    const QString &name() const;

    int numberOfDatasets() const;

    const QUuid &universallyUniqueIdentifier() const;

    Project &operator=(const Project &other);

    friend QDataStream& operator<<(QDataStream& out, const Project &project);

    friend QDataStream& operator>>(QDataStream& in, Project &project);

private:

    QSharedDataPointer<ProjectData> d_data;

    mutable QReadWriteLock d_readWriteLock;

    QUuid d_uuid;

public slots:

    void appendDataset(BaseDataset *baseDataset);

    void insertDataset(int index, BaseDataset *baseDataset);

    void removeAllDatasets();

    void removeDataset(const QUuid &uuid);

    void removeDatasets(const QList<QUuid> &uuids);

    void removeDataset(int index);

    void removeDatasets(const QList<int> &indexes);

    void setName(const QString &name);

signals:

    void datasetRemoved(const QUuid &uuid);

    void datasetAdded(const QSharedPointer<BaseDataset> &baseDataset);

    void datasetNameChanged(const QUuid &uuid, const QString &name);

    void nameChanged(const QUuid &uuid, const QString &name);

};

Q_DECLARE_TYPEINFO(Project, Q_MOVABLE_TYPE);

#endif // PROJECT_H
