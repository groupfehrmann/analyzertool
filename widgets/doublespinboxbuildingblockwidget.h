#ifndef DOUBLESPINBOXBUILDINGBLOCKWIDGET_H
#define DOUBLESPINBOXBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QStringList>
#include <QSettings>
#include <QString>

#include "dialogs/basedialog.h"

namespace Ui {

class DoubleSpinBoxBuildingBlockWidget;

}

class DoubleSpinBoxBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit DoubleSpinBoxBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), bool rememberSettings = false, int decimals = 10, double mimumum = 0, double maximum = 100, double value = 1, double singleStep = 1, const QString &prefix = QString(), const QString &suffix = QString());

    ~DoubleSpinBoxBuildingBlockWidget();

    QDoubleSpinBox *pointerToInternalDoubleSpinBox();

    QLabel *pointerToInternalLabel();

private:

    Ui::DoubleSpinBoxBuildingBlockWidget *ui;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

};


#endif // DOUBLESPINBOXBUILDINGBLOCKWIDGET_H
