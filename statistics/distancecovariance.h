#ifndef DISTANCECOVARIANCE_H
#define DISTANCECOVARIANCE_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "math/statfunctions.h"
#include "math/fastdistancecorrelation.h"

template <typename T>
class DistanceCovariance : public BaseStatisticalTest
{

public:

    DistanceCovariance();

    explicit DistanceCovariance(const QVector<T> &independentVector, QVector<T> &dependentVector, bool pairwiseCompleteMethod = false);

    explicit DistanceCovariance(const QVector<T> &independentVector, QVector<T> &dependentVector, int numberOfPermutations, bool pairwiseCompleteMethod = false);

    ~DistanceCovariance();

    double distanceCovarianceValue();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;


    QVector<T> d_independentVector;

    QVector<T> *d_dependentVector;

    QVector<T> _dependentVector;

    int d_numberOfPermutations;

    QVector<double> d_intermediateResults;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation DistanceCovariance<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool DistanceCovariance<T>::d_hasEffectSize = false;

template <typename T>
QStringList DistanceCovariance<T>::d_sampleDescriptiveLabels = {"Count"};

template <typename T>
int DistanceCovariance<T>::d_maximumNumberOfSamples = 1;

template <typename T>
int DistanceCovariance<T>::d_minimumNumberOfSamples = 1;

template <typename T>
int DistanceCovariance<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString DistanceCovariance<T>::d_nameOfEffectSize = QString();

template <typename T>
QString DistanceCovariance<T>::d_nameOfTransformedEffectSize = QString();

template <typename T>
QString DistanceCovariance<T>::d_nameOfStatisticalTest = "Distance covariance";

template <typename T>
bool DistanceCovariance<T>::d_rankedBased = false;

template <typename T>
QStringList DistanceCovariance<T>::d_testDescriptiveLabels = {"Covariance R", "Statistic", "Significance"};

template <typename T>
DistanceCovariance<T>::DistanceCovariance() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_dependentVector(&_dependentVector)
{

}

template <typename T>
DistanceCovariance<T>::DistanceCovariance(const QVector<T> &independentVector, QVector<T> &dependentVector, bool pairwiseCompleteMethod) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_independentVector(independentVector), d_dependentVector(&dependentVector), d_numberOfPermutations(1000)
{

    if (pairwiseCompleteMethod) {

        d_independentVector.clear();

        for (int i = 0; i < dependentVector.size(); ++i) {

            if (std::isfinite(dependentVector.at(i)) && std::isfinite(independentVector.at(i))) {

                _dependentVector << dependentVector.at(i);

                d_independentVector << independentVector.at(i);

            }

        }

        d_dependentVector = &_dependentVector;

    }

}

template <typename T>
DistanceCovariance<T>::DistanceCovariance(const QVector<T> &independentVector, QVector<T> &dependentVector, int numberOfPermutations, bool pairwiseCompleteMethod) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_independentVector(independentVector), d_dependentVector(&dependentVector), d_numberOfPermutations(numberOfPermutations)
{

    if (pairwiseCompleteMethod) {

        d_independentVector.clear();

        for (int i = 0; i < dependentVector.size(); ++i) {

            if (!std::isnan(dependentVector.at(i)) && !std::isnan(independentVector.at(i))) {

                _dependentVector << dependentVector.at(i);

                d_independentVector << independentVector.at(i);

            }

        }

        d_dependentVector = &_dependentVector;

    }

}

template <typename T>
DistanceCovariance<T>::~DistanceCovariance()
{

}

template <typename T>
double DistanceCovariance<T>::distanceCovarianceValue()
{

    this->statistic();

    return std::sqrt(d_intermediateResults.at(0));

}

template <typename T>
QVector<QVariant> DistanceCovariance<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    sampleDescriptiveValues << d_independentVector.size();

    return sampleDescriptiveValues;

}

template <typename T>
void DistanceCovariance<T>::calculateStatistic()
{

    d_intermediateResults = FastDistanceCorrelation::fastDistanceCovariance(d_independentVector, *d_dependentVector, false, true);

    d_statistic = static_cast<double>(d_independentVector.size() * d_intermediateResults.at(0));

}

template <typename T>
void DistanceCovariance<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    QVector<double> empiricalNullDistribution(d_numberOfPermutations);

    QVector<double> copyOfDependentVector = *d_dependentVector;

    std::random_device rd;

    std::mt19937 g(rd());

    for (int i = 0; i < d_numberOfPermutations; ++i) {

        std::shuffle(copyOfDependentVector.begin(), copyOfDependentVector.end(), g);

        while (copyOfDependentVector == *d_dependentVector)
            std::shuffle(copyOfDependentVector.begin(), copyOfDependentVector.end(), g);

        empiricalNullDistribution[i] = std::sqrt(FastDistanceCorrelation::fastDistanceCovariance(d_independentVector, copyOfDependentVector, false, false).first());

    }

    d_pValue = StatFunctions::qromb([&](const double &value) {return StatFunctions::gaussianKernelDensityEstimator(value, empiricalNullDistribution, 0.01);}, this->distanceCovarianceValue(), 1.0, 1.0e-5);

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void DistanceCovariance<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void DistanceCovariance<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> DistanceCovariance<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << this->distanceCovarianceValue() << d_statistic << d_pValue;

    return descriptiveValues;

}

template <typename T>
void DistanceCovariance<T>::createStructuredMeasurements()
{

}

template <typename T>
void DistanceCovariance<T>::_reset()
{

}

#endif // DISTANCECOVARIANCE_H
