#include "openplotfromfile.h"

OpenPlotFromFile::OpenPlotFromFile(QObject *parent, const QString &path, bool *error, QString *errorMessage) :
    QObject(parent), d_error(error), d_errorMessage(errorMessage)
{

    this->moveToThread(QApplication::instance()->thread());

    this->connect(this, SIGNAL(startOpeningPlotFromFile(QString)), this, SLOT(_startOpeningPlotFromFile(QString)));

    this->connect(this, SIGNAL(finishedOpeningPlotFromFile()), &d_eventLoop, SLOT(quit()));

    emit startOpeningPlotFromFile(path);

    d_eventLoop.exec();

}

const QSharedPointer<QChart> &OpenPlotFromFile::chart()
{

    return d_chart;

}

void OpenPlotFromFile::_startOpeningPlotFromFile(const QString &path)
{

    d_chart = QSharedPointer<QChart>(new QChart);

    QFile fileIn;

    fileIn.setFileName(path);

    if (!fileIn.open(QIODevice::ReadOnly)) {

        *d_errorMessage = "could not open file \"" + path + "\"";

        *d_error = true;

        QThread::currentThread()->quit();

        return;

    }

    QDataStream in(&fileIn);

    QString label;

    in >> label; // _Plot_begin#

    in >> *d_chart;

    emit finishedOpeningPlotFromFile();

}
