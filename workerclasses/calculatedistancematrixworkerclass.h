#ifndef CALCULATEDISTANCEMATRIXWORKERCLASS_H
#define CALCULATEDISTANCEMATRIXWORKERCLASS_H

#include <QString>
#include <QList>
#include <QSharedPointer>
#include <QHash>

#include "base/baseworkerclass.h"
#include "base/basedataset.h"
#include "base/basematrix.h"
#include "base/projectlist.h"
#include "statistics/pearsonr.h"
#include "statistics/spearmanr.h"
#include "statistics/distancecorrelation.h"
#include "statistics/distancecovariance.h"

class CalculateDistanceMatrixWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QSharedPointer<BaseDataset> dataset1;

        QSharedPointer<BaseDataset> dataset2;

        BaseMatrix::Orientation orientationDataset1;

        BaseMatrix::Orientation orientationDataset2;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexesDataset1;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexesDataset1;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexesDataset2;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexesDataset2;

        QString distanceFunction;

        bool calculatePValues;

        int numberOfPermutations;

        QString exportDirectory;

    };

    CalculateDistanceMatrixWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    CalculateDistanceMatrixWorkerClass::Parameters d_parameters;

    BaseDataset *d_baseDataset;

    bool checkInputParameters();

};

#endif // CALCULATEDISTANCEMATRIXWORKERCLASS_H
