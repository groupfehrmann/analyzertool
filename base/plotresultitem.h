#ifndef PLOTRESULTITEM_H
#define PLOTRESULTITEM_H

#include <QObject>
#include <QString>
#include <QChart>
#include <QSharedPointer>

#include "base/baseresultitem.h"

using namespace QtCharts;

class PlotResultItem : public BaseResultItem
{

    Q_OBJECT

public:

    explicit PlotResultItem(QObject *parent = nullptr, const QString &name = "Plot result item", const QSharedPointer<QChart> & chart = QSharedPointer<QChart>());

    void setChart(const QSharedPointer<QChart> &chart);

    const QSharedPointer<QChart> &chart();

private:

    QSharedPointer<QChart> d_chart;

};

#endif // PLOTRESULTITEM_H
