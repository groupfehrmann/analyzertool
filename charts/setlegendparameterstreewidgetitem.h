#ifndef SETLEGENDPARAMETERSTREEWIDGETITEM_H
#define SETLEGENDPARAMETERSTREEWIDGETITEM_H

#include <QObject>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QLegend>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>

#include "charts/setbrushparameterstreewidgetitem.h"
#include "charts/setpenparameterstreewidgetitem.h"
#include "charts/selectfontwidget.h"
#include "charts/selectcolorwidget.h"

using namespace QtCharts;

class SetLegendParametersTreeWidgetItem : public QObject, public QTreeWidgetItem
{

    Q_OBJECT

public:

    explicit SetLegendParametersTreeWidgetItem(QTreeWidget *parent = nullptr, const QString &label = "Legend", QLegend *legend = nullptr);

    explicit SetLegendParametersTreeWidgetItem(QTreeWidgetItem *parent = nullptr, const QString &label = "Legend", QLegend *legend = nullptr);

    ~SetLegendParametersTreeWidgetItem();

public slots:

    void setLegend(const QString &label, QLegend *legend);

private:

    void initialize(const QString &label);

    QLegend *d_legend;

    bool d_initialized;

    QComboBox *d_selectAlignmentWidget;

    QCheckBox *d_selectBackgroundVisibleWidget;

    SelectColorWidget *d_selectBorderColorWidget;

    SetBrushParametersTreeWidgetItem *d_setBrushParametersTreeWidgetItem;

    SelectFontWidget *d_selectFontWidget;

    SetBrushParametersTreeWidgetItem *d_setLabelBrushParametersTreeWidgetItem;

    SetPenParametersTreeWidgetItem *d_setPenParametersTreeWidgetItem;

    QCheckBox *d_selectReverseMarkersWidget;

    QCheckBox *d_selectShowToolTipsWidget;

    QCheckBox *d_selectVisibleWidget;

private slots:

    void setAlignment(int index);

    void setBackgroundVisible(bool visible);

    void setBorderColor(QColor color);

    void setBrush(const QBrush &brush);

    void setFont(const QFont &font);

    void setLabelBrush(const QBrush &brush);

    void setPen(const QPen &pen);

    void setReverseMarkers(bool reverse);

    void setShowToolTips(bool show);

    void setVisible(bool visible);

};

#endif // SETLEGENDPARAMETERSTREEWIDGETITEM_H
