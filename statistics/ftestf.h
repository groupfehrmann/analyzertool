#ifndef FTESTF_H
#define FTESTF_H

#include <QString>
#include <QVector>
#include <QStringList>
#include <QVariant>

#include "statistics/basestatisticaltest.h"
#include "math/mathdescriptives.h"
#include "boost/math/distributions/fisher_f.hpp"

template <typename T>
class FTestF : public BaseStatisticalTest
{

public:

    FTestF();

    FTestF(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers);

    ~FTestF();

private:

    static BaseStatisticalTest::EffectSizeTransformation d_effectSizeTransformation;

    static bool d_hasEffectSize;

    static QStringList d_sampleDescriptiveLabels;

    static int d_maximumNumberOfSamples;

    static int d_minimumNumberOfItemsPerSample;

    static int d_minimumNumberOfSamples;

    static QString d_nameOfEffectSize;

    static QString d_nameOfStatisticalTest;

    static QString d_nameOfTransformedEffectSize;

    static bool d_rankedBased;

    static QStringList d_testDescriptiveLabels;



    QVector<unsigned int> &d_itemSampleCodedIdentifiers;

    QVector<unsigned int> _itemSampleCodedIdentifiers;

    QVector<T> d_measurements;

    QVector<QVector<T> > d_structuredMeasurements;

    QVector<QPair<double, double> > d_sampleMeanAndVariance;

    double d_df1;

    double d_df2;

    void _reset();

    QVector<QVariant> calculateSamplesDescriptivesValues();

    void calculateStatistic();

    QVector<QVariant> calculateTestDescriptivesValues();

    void calculateEffectSize();

    void calculatePValue();

    void calculateStandardErrorOfEffectSize();

    void createStructuredMeasurements();

};

template <typename T>
BaseStatisticalTest::EffectSizeTransformation FTestF<T>::d_effectSizeTransformation = BaseStatisticalTest::NOTRANSFORMATION;

template <typename T>
bool FTestF<T>::d_hasEffectSize = false;

template <typename T>
QStringList FTestF<T>::d_sampleDescriptiveLabels = {"Count", "Mean", "Variance"};

template <typename T>
int FTestF<T>::d_maximumNumberOfSamples = 2;

template <typename T>
int FTestF<T>::d_minimumNumberOfSamples = 2;

template <typename T>
int FTestF<T>::d_minimumNumberOfItemsPerSample = 2;

template <typename T>
QString FTestF<T>::d_nameOfEffectSize = "NA";

template <typename T>
QString FTestF<T>::d_nameOfTransformedEffectSize = "NA";

template <typename T>
QString FTestF<T>::d_nameOfStatisticalTest = "F-test F";

template <typename T>
bool FTestF<T>::d_rankedBased = false;

template <typename T>
QStringList FTestF<T>::d_testDescriptiveLabels = {"F-test F", "Degree of freedom 1", "Degree of freedom 2", "Significance (2-tailed)"};

template <typename T>
FTestF<T>::FTestF() :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(_itemSampleCodedIdentifiers)
{

}

template <typename T>
FTestF<T>::FTestF(const QVector<T> &measurements, QVector<unsigned int> &itemSampleCodedIdentifiers) :
    BaseStatisticalTest(d_nameOfStatisticalTest, d_testDescriptiveLabels, d_sampleDescriptiveLabels, d_rankedBased, d_nameOfEffectSize, d_effectSizeTransformation, d_hasEffectSize, d_nameOfTransformedEffectSize, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_minimumNumberOfSamples), d_itemSampleCodedIdentifiers(itemSampleCodedIdentifiers), d_measurements(measurements)
{

    this->createStructuredMeasurements();

}

template <typename T>
FTestF<T>::~FTestF()
{

}

template <typename T>
QVector<QVariant> FTestF<T>::calculateSamplesDescriptivesValues()
{

    QVector<QVariant> sampleDescriptiveValues;

    for (int i = 0; i < d_structuredMeasurements.size(); ++i)
        sampleDescriptiveValues << d_structuredMeasurements.at(i).size() << d_sampleMeanAndVariance.at(i).first << d_sampleMeanAndVariance.at(i).second;

    return sampleDescriptiveValues;

}

template <typename T>
void FTestF<T>::calculateStatistic()
{

    d_sampleMeanAndVariance[0] = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(0));

    d_sampleMeanAndVariance[1] = MathDescriptives::meanAndVariance(d_structuredMeasurements.at(1));

    double var1 = d_sampleMeanAndVariance.at(0).second;

    double var2 = d_sampleMeanAndVariance.at(2).second;

    if (var1 > var2) {

        d_statistic = var1 / var2;

        d_df1 = d_structuredMeasurements.at(0).size() - 1.0;

        d_df2 = d_structuredMeasurements.at(1).size() - 1.0;

    } else {

        d_statistic = var2 / var1;

        d_df1 = d_structuredMeasurements.at(1).size() - 1.0;

        d_df2 = d_structuredMeasurements.at(0).size() - 1.0;
    }

}

template <typename T>
void FTestF<T>::calculatePValue()
{

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::fisher_f dist(d_df1, d_df2);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();

}

template <typename T>
void FTestF<T>::calculateEffectSize()
{

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
void FTestF<T>::calculateStandardErrorOfEffectSize()
{

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}

template <typename T>
QVector<QVariant> FTestF<T>::calculateTestDescriptivesValues()
{

    QVector<QVariant> descriptiveValues;

    descriptiveValues << d_statistic << d_df1 << d_df2 << d_pValue;

    return descriptiveValues;

}

template <typename T>
void FTestF<T>::createStructuredMeasurements()
{

    d_structuredMeasurements.clear();

    d_structuredMeasurements.resize((*std::max_element(d_itemSampleCodedIdentifiers.begin(), d_itemSampleCodedIdentifiers.end())) + 1);

    for (int i = 0; i < d_measurements.size(); ++i)
        d_structuredMeasurements[d_itemSampleCodedIdentifiers.at(i)] << d_measurements.at(i);

    d_sampleMeanAndVariance.resize(d_structuredMeasurements.size());
}

template <typename T>
void FTestF<T>::_reset()
{

    this->createStructuredMeasurements();

}


#endif // FTESTF_H
