#ifndef ORIENTATIONBUILDINGBLOCKWIDGET_H
#define ORIENTATIONBUILDINGBLOCKWIDGET_H

#include <QWidget>
#include <QCheckBox>
#include <QButtonGroup>

#include "base/basematrix.h"
#include "dialogs/basedialog.h"

namespace Ui {

class OrientationBuildingBlockWidget;

}

class OrientationBuildingBlockWidget : public QWidget
{

    Q_OBJECT

public:

    explicit OrientationBuildingBlockWidget(BaseDialog *parent = nullptr, const QString &widgetId = QString(), const QString &label = QString(), bool rememberSettings = true);

    ~OrientationBuildingBlockWidget();

    BaseMatrix::Orientation selectedOrientation() const;

private:

    Ui::OrientationBuildingBlockWidget *ui;

    QButtonGroup d_buttonGroup;

    QString d_parentIdentifierForSettings;

    bool d_rememberSettings;

private slots:

    void checkBox_row_toggled(bool toggled);

signals:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // ORIENTATIONBUILDINGBLOCKWIDGET_H
