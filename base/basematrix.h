#ifndef BASEMATRIX_H
#define BASEMATRIX_H

#include <QObject>
#include <QMetaType>
#include <QtGlobal>
#include <QSharedDataPointer>
#include <QSharedData>
#include <QVariant>

class BaseMatrixData : public QSharedData
{

public:

    BaseMatrixData();

    BaseMatrixData(const BaseMatrixData &other);

    ~BaseMatrixData();

    int d_columnCount;

    int d_metaTypeIdentifier;

    int d_rowCount;

};

class BaseMatrix : public QObject
{

    Q_OBJECT

public:

    enum Orientation {

        ROW = 0,

        COLUMN = 1

    };

    static BaseMatrix::Orientation switchOrientation(BaseMatrix::Orientation orientation);

    static QString orientationToString(BaseMatrix::Orientation orientation);

    BaseMatrix(QObject *parent = nullptr);

    BaseMatrix(const BaseMatrix &other);

    ~BaseMatrix();

    void appendVector(BaseMatrix::Orientation orientation);

    template <typename T>
    bool appendVector(const QVector<T> &vector, Orientation orientation, QString *errorMessage = nullptr);

    void appendVectors(int count, BaseMatrix::Orientation orientation);

    template <typename T>
    bool appendVectors(const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation, QString *errorMessage = nullptr);

    bool areValidIndexes(int rowIndex, int columnIndex) const;

    int count(BaseMatrix::Orientation orientation) const;

    virtual void insertVector(int index, Orientation orientation) = 0;

    virtual bool insertVector(int index, const QVector<short> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<unsigned short> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<int> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<unsigned int> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<long long> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<unsigned long long> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<float> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<double> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<unsigned char> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<QChar> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<QString> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    virtual bool insertVector(int index, const QVector<bool> &vector, Orientation orientation, QString *errorMessage = nullptr) = 0;

    void insertVectors(int index, int count, Orientation orientation);

    template <typename T>
    bool insertVectors(int index, const QVector<QVector<T> > &vectors, Orientation orientation, QString *errorMessage = nullptr);

    bool isEmpty() const;

    int metaTypeIdentifier() const;

    QString metaTypeName() const;

    void prependVector(BaseMatrix::Orientation orientation);

    template <typename T>
    bool prependVector(const QVector<T> &vector, Orientation orientation, QString *errorMessage = nullptr);

    void prependVectors(int count, BaseMatrix::Orientation orientation);

    template <typename T>
    bool prependVectors(const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation, QString *errorMessage = nullptr);

    virtual void removeVector(int index, BaseMatrix::Orientation orientation) = 0;

    virtual void removeVectors(QList<int> indexes, BaseMatrix::Orientation orientation) = 0;

    virtual void swap(int index1, int index2, BaseMatrix::Orientation orientation) = 0;

    virtual void setValueAsVariant(int rowIndex, int columnIndex, const QVariant &variant) = 0;

    virtual void transpose() = 0;

    virtual QSet<QString> uniqueValuesOfQString() const = 0;

    virtual QVariant valueAsVariant(int rowIndex, int columnIndex) const = 0;

    virtual QVector<short> vectorOfShort(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<short> vectorOfShort(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<short> > vectorsOfShort(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<short> > vectorsOfShort(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned short> vectorOfUnsignedShort(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned short> vectorOfUnsignedShort(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned short> > vectorsOfUnsignedShort(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned short> > vectorsOfUnsignedShort(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<int> vectorOfInt(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<int> vectorOfInt(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<int> > vectorsOfInt(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<int> > vectorsOfInt(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned int> vectorOfUnsignedInt(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned int> vectorOfUnsignedInt(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned int> > vectorsOfUnsignedInt(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned int> > vectorsOfUnsignedInt(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<long long> vectorOfLongLong(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<long long> vectorOfLongLong(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<long long> > vectorsOfLongLong(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<long long> > vectorsOfLongLong(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned long long> vectorOfUnsignedLongLong(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned long long> vectorOfUnsignedLongLong(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned long long> > vectorsOfUnsignedLongLong(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned long long> > vectorsOfUnsignedLongLong(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<float> vectorOfFloat(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<float> vectorOfFloat(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<float> > vectorsOfFloat(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<float> > vectorsOfFloat(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<double> vectorOfDouble(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<double> vectorOfDouble(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<double> > vectorsOfDouble(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<double> > vectorsOfDouble(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QChar> vectorOfQChar(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QChar> vectorOfQChar(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<QChar> > vectorsOfQChar(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<QChar> > vectorsOfQChar(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned char> vectorOfUnsignedChar(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<unsigned char> vectorOfUnsignedChar(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned char> > vectorsOfUnsignedChar(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<unsigned char> > vectorsOfUnsignedChar(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QString> vectorOfQString(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QString> vectorOfQString(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<QString> > vectorsOfQString(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<QString> > vectorsOfQString(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<bool> vectorOfBool(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<bool> vectorOfBool(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<bool> > vectorsOfBool(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<bool> > vectorsOfBool(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVariant> vectorOfQVariant(int index, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVariant> vectorOfQVariant(int index_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<QVariant> > vectorsOfQVariant(const QList<int> &indexes, BaseMatrix::Orientation orientation) const = 0;

    virtual QVector<QVector<QVariant> > vectorsOfQVariant(const QList<int> &indexes_orientation, const QList<int> &indexes_oppositeOrientation, BaseMatrix::Orientation orientation) const = 0;

    virtual QDataStream &writeToDataStream(QDataStream &out) = 0;

    virtual QDataStream &readFromDataStream(QDataStream &in) = 0;

    BaseMatrix &operator=(const BaseMatrix &other);

    friend QDataStream& operator<<(QDataStream& out, BaseMatrix &baseMatrix);

    friend QDataStream& operator>>(QDataStream& in, BaseMatrix &matrix);

protected:

    QSharedDataPointer<BaseMatrixData> d_data;

};

Q_DECLARE_TYPEINFO(BaseMatrix, Q_MOVABLE_TYPE);

template <typename T>
bool BaseMatrix::appendVector(const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage)
{

    return this->insertVector(this->count(orientation), vector, orientation, errorMessage);

}

template <typename T>
bool BaseMatrix::appendVectors(const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation, QString *errorMessage)
{

    if (vectors.isEmpty())
        return false;

    for (const QVector<T> &vector: vectors) {

        if (!this->appendVector(vector, orientation, errorMessage))
            return false;

    }

    return true;

}

template <typename T>
bool BaseMatrix::insertVectors(int index, const QVector<QVector<T> > &vectors, Orientation orientation, QString *errorMessage)
{

    if (vectors.isEmpty())
        return false;

    for (int i = vectors.size() - 1; i >= 0; --i) {

        if (!this->insertVector(index, vectors.at(i), orientation, errorMessage))
            return false;

    }

    return true;

}

template <typename T>
bool BaseMatrix::prependVector(const QVector<T> &vector, BaseMatrix::Orientation orientation, QString *errorMessage)
{

    return this->insertVector(0, vector, orientation, errorMessage);

}

template <typename T>
bool BaseMatrix::prependVectors(const QVector<QVector<T> > &vectors, BaseMatrix::Orientation orientation, QString *errorMessage)
{

    return this->insertVectors(0, vectors, orientation, errorMessage);

}

#endif // BASEMATRIX_H
