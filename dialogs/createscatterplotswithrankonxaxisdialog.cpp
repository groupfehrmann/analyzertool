#include "createscatterplotswithrankonxaxisdialog.h"

CreateScatterPlotsWithRankOnXAxisDialog::CreateScatterPlotsWithRankOnXAxisDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogIdentifierForSettings, QSharedPointer<BaseDataset> baseDataset, bool selectedOnly) :
    BaseDialog(parent, dialogTitle, dialogIdentifierForSettings), d_baseDataset(baseDataset), d_selectedOnly(selectedOnly)
{

    d_parameters = new CreateScatterPlotsWithRankOnXAxisWorkerClass::Parameters;

    this->setupWidgets();

}

CreateScatterPlotsWithRankOnXAxisDialog::~CreateScatterPlotsWithRankOnXAxisDialog()
{

   delete static_cast<CreateScatterPlotsWithRankOnXAxisWorkerClass::Parameters *>(d_parameters);

}

void CreateScatterPlotsWithRankOnXAxisDialog::initializeWidgets()
{

    d_orientationBuildingBlockWidget = new OrientationBuildingBlockWidget(this, QString(), "Select orientation");

    d_gridLayout->addWidget(d_orientationBuildingBlockWidget, 0, 0);

    d_itemsSelectorBuildingBlockWidget_variables = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_variables", "Select variables");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_variables, 1, 0);

    d_itemsSelectorBuildingBlockWidget_items = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_items", "Select items");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_items, 2, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningRank = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningRank", "Select annotation label defining rank");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningRank, 3, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets", "Select annotation label defining subsets (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets, 4, 0);

    d_itemsSelectorBuildingBlockWidget_subsetIdentifiers = new ItemsSelectorBuildingBlockWidget(this, "itemsSelectorBuildingBlockWidget_subsetIdentifiers", "Select subset identifiers (optional)");

    d_gridLayout->addWidget(d_itemsSelectorBuildingBlockWidget_subsetIdentifiers, 5, 0);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier", "Select annotation label defining title (optional)");

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier, 6, 0);

    d_comboBoxBuildingBlockWidget_chartType = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_chartType", "Select chart type", {"Cartesian", "Polar"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_chartType, 7, 0);

    d_comboBoxBuildingBlockWidget_plotMode = new ComboBoxBuildingBlockWidget(this, "comboBoxBuildingBlockWidget_plotMode", "Select plot mode", {"All variables in single plot", "Each variable in seperate plot"});

    d_gridLayout->addWidget(d_comboBoxBuildingBlockWidget_plotMode, 8, 0);

    d_plotExportParametersBuildingBlockWidget = new PlotExportParametersBuildingBlockWidget(this, "plotExportParametersBuildingBlockWidget");

    d_gridLayout->addWidget(d_plotExportParametersBuildingBlockWidget, 9, 0);

    d_selectFileBuildingBlockWidget_templatePlot = new SelectFileBuildingBlockWidget(this, "selectFileBuildingBlockWidget_templatePlot", "Select template plot (optional)", QFileDialog::AcceptOpen, true);

    d_gridLayout->addWidget(d_selectFileBuildingBlockWidget_templatePlot, 10, 0);

    this->connect(d_orientationBuildingBlockWidget, SIGNAL(orientationChanged(BaseMatrix::Orientation)), this, SLOT(orientationChanged(BaseMatrix::Orientation)));

    this->orientationChanged(d_orientationBuildingBlockWidget->selectedOrientation());

    this->connect(d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox(), SIGNAL(currentTextChanged(QString)), this, SLOT(annotationLabelDefiningSubsetChanged(QString)));

}

void CreateScatterPlotsWithRankOnXAxisDialog::initializeParameters()
{

    CreateScatterPlotsWithRankOnXAxisWorkerClass::Parameters *parameters = static_cast<CreateScatterPlotsWithRankOnXAxisWorkerClass::Parameters *>(d_parameters);

    parameters->annotationLabelDefiningAlternativeVariableIdentifier = d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningRank = d_comboBoxBuildingBlockWidget_annotationLabelDefiningRank->pointerToInternalComboBox()->currentText();

    parameters->annotationLabelDefiningSubsets = d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->currentText();

    parameters->baseDataset = d_baseDataset;

    parameters->chartType = (d_comboBoxBuildingBlockWidget_chartType->pointerToInternalComboBox()->currentText() == "Cartesian") ? QChart::ChartTypeCartesian : QChart::ChartTypePolar;

    parameters->dpiOfPlot = d_plotExportParametersBuildingBlockWidget->dpiOfPlot();

    parameters->exportDirectory = d_plotExportParametersBuildingBlockWidget->exportDirectory();

    parameters->exportTypeOfPlot = d_plotExportParametersBuildingBlockWidget->typeOfPlot();

    parameters->heightOfPlot = d_plotExportParametersBuildingBlockWidget->heightOfPlot();

    parameters->orientation = d_orientationBuildingBlockWidget->selectedOrientation();

    parameters->pathToTemplatePlot = d_selectFileBuildingBlockWidget_templatePlot->pointerToInternalLineEdit()->text();

    parameters->plotMode = d_comboBoxBuildingBlockWidget_plotMode->pointerToInternalComboBox()->currentText();

    parameters->selectedItemIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_items->selectedItemsIdentifierAndNumber();

    parameters->selectedVariableIdentifiersAndIndexes = d_itemsSelectorBuildingBlockWidget_variables->selectedItemsIdentifierAndNumber();

    parameters->subsetIdentifiers = d_itemsSelectorBuildingBlockWidget_subsetIdentifiers->selectedItemsIdentifierAndNumber().first;

    parameters->widthOfPlot = d_plotExportParametersBuildingBlockWidget->widthOfPlot();

}

void CreateScatterPlotsWithRankOnXAxisDialog::orientationChanged(BaseMatrix::Orientation orientation)
{

    QPair<QStringList, QList<int> > variablesIdentifierAndIndex = d_baseDataset->header(orientation).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_variables->setItemsIdentifierAndNumber(variablesIdentifierAndIndex.first, variablesIdentifierAndIndex.second);

    QPair<QStringList, QList<int> > itemsIdentifierAndIndex = d_baseDataset->header(BaseMatrix::switchOrientation(orientation)).identifiersAndIndexes(d_selectedOnly);

    d_itemsSelectorBuildingBlockWidget_items->setItemsIdentifierAndNumber(itemsIdentifierAndIndex.first, itemsIdentifierAndIndex.second);

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningRank->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningRank->pointerToInternalComboBox()->addItems(d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningSubsets->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(BaseMatrix::switchOrientation(orientation)).labels());

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->clear();

    d_comboBoxBuildingBlockWidget_annotationLabelDefiningAlternativeVariableIdentifier->pointerToInternalComboBox()->addItems(QStringList() << QString() << d_baseDataset->annotations(orientation).labels());

}

void CreateScatterPlotsWithRankOnXAxisDialog::annotationLabelDefiningSubsetChanged(const QString &currentAnnotationLabel)
{

    if (currentAnnotationLabel.isEmpty()) {

        d_itemsSelectorBuildingBlockWidget_subsetIdentifiers->clear();

        return;

    }

    QStringList subsetIdentifiers = d_baseDataset->uniqueAnnotationValues<QString>(currentAnnotationLabel, BaseMatrix::switchOrientation(d_orientationBuildingBlockWidget->selectedOrientation()), d_selectedOnly).values();

    subsetIdentifiers.removeOne(QString());

    std::sort(subsetIdentifiers.begin(), subsetIdentifiers.end());

    d_itemsSelectorBuildingBlockWidget_subsetIdentifiers->setItemsIdentifier(subsetIdentifiers);

}
