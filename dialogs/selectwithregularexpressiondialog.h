#ifndef SELECTWITHREGULAREXPRESSIONDIALOG_H
#define SELECTWITHREGULAREXPRESSIONDIALOG_H

#include "dialogs/basedialog.h"
#include "workerclasses/selectwithregularexpressionworkerclass.h"
#include "widgets/orientationbuildingblockwidget.h"
#include "widgets/itemsselectorbuildingblockwidget.h"
#include "widgets/selectregularexpressionfiltersbuildingblockwidget.h"

class SelectWithRegularExpressionDialog : public BaseDialog
{

    Q_OBJECT

public:

    SelectWithRegularExpressionDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogIdentifierForSettings = QString(), QSharedPointer<BaseDataset> baseDataset = QSharedPointer<BaseDataset>(), bool selectedOnly = false);


    ~SelectWithRegularExpressionDialog();

private:

    QSharedPointer<BaseDataset> d_baseDataset;

    bool d_selectedOnly;

    OrientationBuildingBlockWidget *d_orientationBuildingBlockWidget;

    ItemsSelectorBuildingBlockWidget *d_itemsSelectorBuildingBlockWidget_identifiers;

    SelectRegularExpressionFiltersBuildingBlockWidget *d_selectRegularExpressionFiltersBuildingBlockWidget;

    void initializeWidgets();

    void initializeParameters();

private slots:

    void orientationChanged(BaseMatrix::Orientation orientation);

};

#endif // SELECTWITHREGULAREXPRESSIONDIALOG_H
