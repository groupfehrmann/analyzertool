#include "selectheaderlabelsbuildingblockwidget.h"
#include "ui_selectheaderlabelsbuildingblockwidget.h"

SelectHeaderLabelsBuildingBlockWidget::SelectHeaderLabelsBuildingBlockWidget(BaseDialog *parent, const QString &widgetId) :
    QWidget(parent), ui(new Ui::SelectHeaderLabelsBuildingBlockWidget)
{

    ui->setupUi(this);

    d_parentIdentifierForSettings = parent->identifierForSettings() + "_" + widgetId;

    this->connect(ui->comboBox_labelDefiningRowIdentifiers, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxLabelDefiningRowIdentifiersCurrentIndexChanged(int)));

    this->connect(ui->comboBox_labelDefiningFirstDataColumn, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxLabelDefiningFirstDataColumnCurrentIndexChanged(int)));

}

SelectHeaderLabelsBuildingBlockWidget::~SelectHeaderLabelsBuildingBlockWidget()
{

    delete ui;

}

void SelectHeaderLabelsBuildingBlockWidget::comboBoxLabelDefiningRowIdentifiersCurrentIndexChanged(int index)
{

    ui->comboBox_labelDefiningFirstDataColumn->clear();

    if (index == -1)
        return;

    d_indexOfLabelDefiningRowIdentifiers = index;

    ui->comboBox_labelDefiningFirstDataColumn->addItems(d_tokensOfHeaderLine.mid(d_indexOfLabelDefiningRowIdentifiers + 1));

}

void SelectHeaderLabelsBuildingBlockWidget::comboBoxLabelDefiningFirstDataColumnCurrentIndexChanged(int index)
{

    ui->comboBox_labelDefiningSecondDataColumn->clear();

    if (index == -1)
        return;

    d_indexOfLabelDefiningFirstDataColumn = d_indexOfLabelDefiningRowIdentifiers + index + 1;

    ui->comboBox_labelDefiningSecondDataColumn->addItem(QString());

    ui->comboBox_labelDefiningSecondDataColumn->addItems(d_tokensOfHeaderLine.mid(d_indexOfLabelDefiningFirstDataColumn + 1));

    if (ui->comboBox_labelDefiningSecondDataColumn->count() > 1)
        ui->comboBox_labelDefiningSecondDataColumn->setCurrentIndex(1);

}

QPair<int, QString> SelectHeaderLabelsBuildingBlockWidget::indexAndLabelForColumnDefiningRowIdentifiers()
{

    return QPair<int, QString>(d_indexOfLabelDefiningRowIdentifiers, ui->comboBox_labelDefiningRowIdentifiers->currentText());

}

QPair<int, QString> SelectHeaderLabelsBuildingBlockWidget::indexAndLabelForColumnDefiningFirstDataColumn()
{

    return QPair<int, QString>(d_indexOfLabelDefiningFirstDataColumn, ui->comboBox_labelDefiningFirstDataColumn->currentText());

}

QPair<int, QString> SelectHeaderLabelsBuildingBlockWidget::indexAndLabelForColumnDefiningSecondDataColumn()
{

    if (ui->comboBox_labelDefiningSecondDataColumn->currentIndex() != 0)
        return QPair<int, QString>(d_indexOfLabelDefiningFirstDataColumn + ui->comboBox_labelDefiningSecondDataColumn->currentIndex(), ui->comboBox_labelDefiningSecondDataColumn->currentText());
    else
        return QPair<int, QString>(-1, QString());

}

void SelectHeaderLabelsBuildingBlockWidget::headerLineChanged(const QStringList &tokensOfHeaderLine)
{

    d_tokensOfHeaderLine = tokensOfHeaderLine;

    ui->comboBox_labelDefiningRowIdentifiers->clear();

    ui->comboBox_labelDefiningRowIdentifiers->addItems(tokensOfHeaderLine);

}

QComboBox *SelectHeaderLabelsBuildingBlockWidget::pointerToInternalComboBoxLabelDefiningRowIdentifiers()
{

    return ui->comboBox_labelDefiningRowIdentifiers;

}

QComboBox *SelectHeaderLabelsBuildingBlockWidget::pointerToInternalComboBoxLabelDefiningFirstDataColumn()
{

    return ui->comboBox_labelDefiningFirstDataColumn;

}

QComboBox *SelectHeaderLabelsBuildingBlockWidget::pointerToInternalComboBoxLabelDefiningSecondDataColumn()
{

    return ui->comboBox_labelDefiningSecondDataColumn;

}
