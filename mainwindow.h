#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSharedPointer>
#include <QSettings>
#include <QFileDialog>
#include <QFile>
#include <QStringList>

#include "base/projectlist.h"
#include "base/projectlistmodel.h"
#include "base/resultlist.h"
#include "base/resultlistmodel.h"
#include "base/loglistmodel.h"
#include "base/processcontroler.h"
#include "dialogs/importdatamatrixfromsinglefiledialog.h"
#include "dialogs/importannotationsdialog.h"
#include "dialogs/selectfileandplotparametersdialog.h"
#include "dialogs/editplotappearancedialog.h"
#include "dialogs/createbarplotsdialog.h"
#include "dialogs/createscatterplotswithgenomicmappingrankonxaxishomosapiensdialog.h"
#include "dialogs/createscatterplotswithgenomicmappingonxaxishomosapiensdialog.h"
#include "dialogs/createhistogramplotsdialog.h"
#include "dialogs/createboxandwhiskersplotdialog.h"
#include "dialogs/createscatterplotswithrankonxaxisdialog.h"
#include "dialogs/mergedatasetsdialog.h"
#include "dialogs/independentcomponentanalysisdialog.h"
#include "dialogs/splitidentifiersdialog.h"
#include "dialogs/transformdatamatrixdialog.h"
#include "dialogs/comparesamplescontinuousdialog.h"
#include "dialogs/modifyidentifiersbasedonregexpdialog.h"
#include "dialogs/selectwithregularexpressiondialog.h"
#include "dialogs/selectwithtokensdialog.h"
#include "dialogs/createpieplotsdialog.h"
#include "dialogs/deduplicateidentifiersdialog.h"
#include "dialogs/createmd5hashsumforfilesdialog.h"
#include "dialogs/calculatedescriptivesdialog.h"
#include "dialogs/principalcomponentanalysisdialog.h"
#include "dialogs/creategenenetworkdialog.h"
#include "dialogs/calculatemodulescoresdialog.h"
#include "dialogs/replaceidentifiersdialog.h"
#include "dialogs/calculatedistancematrixdialog.h"
#include "dialogs/genesetenrichmentanalysisdialog.h"
#include "dialogs/exporttojsonfiledialog.h"
#include "workerclasses/importdatamatrixfromsinglefileworkerclass.h"
#include "workerclasses/saveprojectworkerclass.h"
#include "workerclasses/savedatasetworkerclass.h"
#include "workerclasses/openprojectworkerclass.h"
#include "workerclasses/opendatasetworkerclass.h"
#include "workerclasses/exporttotabdelimitedtextfileworkerclass.h"
#include "workerclasses/importannotationsworkerclass.h"
#include "workerclasses/saveplotworkerclass.h"
#include "workerclasses/exportplotworkerclass.h"
#include "workerclasses/createbarplotsworkerclass.h"
#include "workerclasses/createscatterplotswithgenomicmappingrankonxaxishomosapiensworkerclass.h"
#include "workerclasses/createscatterplotswithgenomicmappingonxaxishomosapiensworkerclass.h"
#include "workerclasses/transposedatasetworkerclass.h"
#include "workerclasses/openplotworkerclass.h"
#include "workerclasses/createhistogramplotsworkerclass.h"
#include "workerclasses/createboxandwhiskersplotworkerclass.h"
#include "workerclasses/createscatterplotswithrankonxaxisworkerclass.h"
#include "workerclasses/mergedatasetsworkerclass.h"
#include "workerclasses/independentcomponentanalysisworkerclass.h"
#include "workerclasses/splitidentifiersworkerclass.h"
#include "workerclasses/transformdatamatrixworkerclass.h"
#include "workerclasses/comparesamplesworkerclass.h"
#include "workerclasses/modifyidentifiersbasedonregexpworkerclass.h"
#include "workerclasses/selectwithregularexpressionworkerclass.h"
#include "workerclasses/selectwithtokensworkerclass.h"
#include "workerclasses/createpieplotsworkerclass.h"
#include "workerclasses/deduplicateidentifiersworkerclass.h"
#include "workerclasses/createmd5hashsumforfilesworkerclass.h"
#include "workerclasses/calculatedescriptivesworkerclass.h"
#include "workerclasses/principalcomponentanalysisworkerclass.h"
#include "workerclasses/calculatemodulescoresworkerclass.h"
#include "workerclasses/replaceidentifiersworkerclass.h"
#include "workerclasses/calculatedistancematrixworkerclass.h"
#include "workerclasses/genesetenrichmentanalysisworkerclass.h"
#include "workerclasses/exporttojsonfileworkerclass.h"

namespace Ui {

    class MainWindow;

}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

private:

    Ui::MainWindow *ui;

    QAction *d_clearRecentlyOpenedFilesAction;

    ProcessControler d_processControler;

    QStringList d_nameOfRecentlyOpenedFiles;

    QStringList d_pathOfRecentlyOpenedFiles;

    QList<QAction *> d_recentlyOpenedActions;

    void connectActionsInMenuToSlots();

    void createRecentlyOpenedActions();

    void open(const QString &pathToFile);

    void readApplicationSettings();

    QString selectFile(QFileDialog::AcceptMode acceptMode, const QString &directory = QString(), const QString &suggestedFileName = QString(), const QStringList &filters = QStringList(), const QString &defaultSuffix = QString()) const;

    void updateRecentlyOpenedActions();

    void writeApplicationSettings() const;

private slots:

    void addToRecentlyOpenedFiles(const QString &name, const QString &path);

    void clearRecentlyOpenedFiles();

    void close();

    void closeAll();

    void createBoxAndWhiskersPlots();

    void createGeneNetwork();

    void createHistogramPlots();

    void createMD5HashSumForFiles();

    void createPiePlots();

    void createHorizontalBarPlots();

    void createHorizontalPercentBarPlots();

    void createHorizontalStackedBarPlots();

    void createScatterPlotsWithGenomicMappingRankOnXAxisHomoSapiens();

    void createScatterPlotsWithGenomicMappingOnXAxisHomoSapiens();

    void createScatterPlotsWithRankOnXAxis();

    void createVerticalBarPlots();

    void createVerticalPercentBarPlots();

    void createVerticalStackedBarPlots();

    void datasetSelected();

    void deduplicateIdentifiers();

    void editPlotAppearance();

    void exportPlotToImageFile();

    void exportToTabDelimitedTextFile();

    void exportToJSONFile();

    void importColumnAnnotations();

    void importDataMatrixFromSingleFile();

    void importRowAnnotations();

    void calculateDescriptives();

    void calculateModuleScores();

    void independentComponentAnalysis();

    void principalComponentAnalysis();

    void compareSamplesContinuous();

    void open();

    void openRecentlyOpenedFile();

    void projectSelected();

    void mergeDatasets();

    void modifyIdentifiersBasedOnRegExp();

    void nameOfCurrentSelectedDatasetChanged();

    void nameOfCurrentSelectedProjectChanged();

    void nameOfCurrentSelectedResultChanged();

    void nameOfCurrentSelectedResultItemChanged();

    void noValidItemSelectedInProjectsAndResultsOverviewWidget();

    void replaceIdentifierWithAnnotationValue();

    void resultSelected();

    void resultItemSelected();

    void selectWithRegularExpression();

    void selectWithTokens();

    void transformDataMatrix();

    void transpose();

    void save();

    void splitIdentifiers();

    void calculateDistanceMatrix();

    void geneSetEnrichmentAnalysis();

};

#endif // MAINWINDOW_H
