#include "piechart.h"

MainSlice::MainSlice(QPieSeries *breakdownSeries, QObject *parent) :
    QPieSlice(parent), d_breakdownSeries(breakdownSeries)
{

}

QPieSeries *MainSlice::breakdownSeries() const
{

    return d_breakdownSeries;

}

void MainSlice::setName(QString name)
{
    d_name = name;

}

QString MainSlice::name() const
{

    return d_name;
}

PieChart::PieChart(QObject *parent, const QString &title, const QStringList &pieSliceSubsetIdentifiers, const QStringList &pieSliceLabels, const QVector<double> &pieSliceValues) :
    BaseChart(parent)
{

    d_defaultColors << QColor(86, 169, 246);
    d_defaultColors << QColor(114, 190, 72);
    d_defaultColors << QColor(244, 210, 62);
    d_defaultColors << QColor(241, 143, 46);
    d_defaultColors << QColor(234, 94, 91);
    d_defaultColors << QColor(178, 110, 223);
    d_defaultColors << QColor(16, 103, 189);
    d_defaultColors << QColor(17, 135, 48);
    d_defaultColors << QColor(219, 188, 55);
    d_defaultColors << QColor(220, 106, 35);
    d_defaultColors << QColor(198, 40, 23);
    d_defaultColors << QColor(118, 66, 153);
    d_defaultColors << QColor(26, 80, 132);
    d_defaultColors << QColor(16, 92, 29);
    d_defaultColors << QColor(194, 150, 43);
    d_defaultColors << QColor(188, 91, 29);
    d_defaultColors << QColor(132, 17, 10);
    d_defaultColors << QColor(94, 53, 122);
    d_defaultColors << QColor(2, 37, 80);
    d_defaultColors << QColor(9, 63, 12);
    d_defaultColors << QColor(162, 116, 32);
    d_defaultColors << QColor(222, 135, 56);
    d_defaultColors << QColor(145, 70, 19);
    d_defaultColors << QColor(86, 7, 9);
    d_defaultColors << QColor(59, 33, 76);

    this->connect(this, SIGNAL(initializeChart(QString,QStringList,QStringList,QVector<double>)), this, SLOT(_initializeChart(QString,QStringList,QStringList,QVector<double>)));

    emit initializeChart(title, pieSliceSubsetIdentifiers, pieSliceLabels, pieSliceValues);

    d_eventLoop.exec();

}

void PieChart::_initializeChart(const QString &title, const QStringList &subsetIdentifiers, const QStringList &pieSliceLabels, const QVector<double> &pieSliceValues)
{

    d_chart = QSharedPointer<QChart>(new QChart);

    d_chart->legend()->setVisible(false);

    if (subsetIdentifiers.isEmpty()) {

        QPieSeries *series = new QPieSeries();

        for (int i = 0; i < pieSliceLabels.size(); ++i) {

            QPieSlice *slice = new QPieSlice(pieSliceLabels.at(i), pieSliceValues.at(i));

            series->append(slice);

        }

        d_chart->addSeries(series);

    } else {

        QHash<QString, int> subsetIdentifierToIndex;

        QStringList uniqueSubsetIdentifiers;

        for (const QString &subsetIdentifier : subsetIdentifiers) {

            if (!subsetIdentifierToIndex.contains(subsetIdentifier)) {

                uniqueSubsetIdentifiers << subsetIdentifier;

                subsetIdentifierToIndex.insert(subsetIdentifier, subsetIdentifierToIndex.size());

            }

        }

        QHash<QString, QPieSeries *> subsetIdentifierToPieSeries;

        for (const QString &key : subsetIdentifierToIndex.keys()) {

            QPieSeries *pieSeries = new QPieSeries;

            pieSeries->setName(key);

            subsetIdentifierToPieSeries[key] = pieSeries;

        }

        for (int i = 0; i < subsetIdentifiers.size(); ++i)
            subsetIdentifierToPieSeries[subsetIdentifiers.at(i)]->append(pieSliceLabels.at(i), pieSliceValues.at(i));

        QPieSeries *mainPieSeries = new QPieSeries;

        mainPieSeries->setName("Inner pie (subgroups)");

        mainPieSeries->setPieSize(0.4);

        d_chart->addSeries(mainPieSeries);

        int indexOfDefaultColor = 0;

        for (const QString &subsetIdentifier : uniqueSubsetIdentifiers) {

            QPieSeries *breakdownSeries = subsetIdentifierToPieSeries[subsetIdentifier];

            MainSlice *mainSlice = new MainSlice(breakdownSeries);

            mainSlice->setValue(breakdownSeries->sum());

            mainSlice->setLabel(breakdownSeries->name());

            mainSlice->setLabelVisible(true);

            mainSlice->setLabelColor(Qt::white);

            mainSlice->setLabelPosition(QPieSlice::LabelInsideNormal);

            QColor color = d_defaultColors.at(indexOfDefaultColor);

            indexOfDefaultColor++;

            mainSlice->setColor(color);

            mainPieSeries->append(mainSlice);

            breakdownSeries->setPieSize(0.6);

            breakdownSeries->setHoleSize(0.45);

            breakdownSeries->setLabelsVisible();

            foreach (QPieSlice *slice, breakdownSeries->slices()) {

                color = color.lighter(110);

                slice->setBrush(color);

            }

            d_chart->addSeries(breakdownSeries);

        }

        qreal angle = 0;

        for (QPieSlice *mainSlice : mainPieSeries->slices()) {

            for (QLegendMarker *pieMarker : d_chart->legend()->markers(mainPieSeries))
                pieMarker->setVisible(false);

            QPieSeries *breakdownSeries = qobject_cast<MainSlice *>(mainSlice)->breakdownSeries();

            breakdownSeries->setPieStartAngle(angle);

            angle += mainSlice->percentage() * 360.0;

            breakdownSeries->setPieEndAngle(angle);

            for (QLegendMarker *marker : d_chart->legend()->markers(breakdownSeries)) {

                QPieLegendMarker *pieMarker = qobject_cast<QPieLegendMarker *>(marker);

                pieMarker->setLabel(QString("%1 %2%").arg(pieMarker->slice()->label()).arg(pieMarker->slice()->percentage() * 100, 0, 'f', 2));
            }

        }

    }

    d_chart->setTitle(title);

    emit finishedInitializationOfChart();

}
