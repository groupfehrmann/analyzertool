#include "exportplotworkerclass.h"

ExportPlotWorkerClass::ExportPlotWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<ExportPlotWorkerClass::Parameters *>(parameters))
{

}

void ExportPlotWorkerClass::doWork()
{
    emit processStarted("Export plot");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    emit appendLogItem(LogListModel::PARAMETER, "plot to export: \"" + d_parameters.chartViewToSave->chart()->title() +"\"");

    emit appendLogItem(LogListModel::PARAMETER, "height in millimeters: " + QString::number(d_parameters.heightInMillimeters));

    emit appendLogItem(LogListModel::PARAMETER, "width in millimeters: " + QString::number(d_parameters.widthInMillimeters));

    emit appendLogItem(LogListModel::PARAMETER, "resolution in dotch per inch: " + QString::number(d_parameters.resolutionInDPI));

    emit startProgress(0, "Exporting plot", 0, 0);

    bool error;

    QString errorMessage;

    ExportPlotToFile(nullptr, d_parameters.chartViewToSave, d_parameters.pathToFile, d_parameters.widthInMillimeters, d_parameters.heightInMillimeters, d_parameters.resolutionInDPI, &error, &errorMessage);

    if (error)
        emit appendLogItem(LogListModel::ERROR, errorMessage);

    emit stopProgress(0);

    QThread::currentThread()->quit();

}
