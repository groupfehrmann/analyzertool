#ifndef PRINCIPALCOMPONENTANALYSISWORKERCLASS_H
#define PRINCIPALCOMPONENTANALYSISWORKERCLASS_H


#include <QString>
#include <QList>
#include <QSharedPointer>
#include <QtConcurrent>
#include <QFutureWatcher>
#include <QDateTime>

#ifdef Q_OS_MAC
#include <Accelerate/Accelerate.h>
#endif

#include <cmath>
#include <functional>
#include <utility>
#include <cfloat>
#include <random>
#include <algorithm>

#include "base/baseworkerclass.h"
#include "math/vectoroperations.h"
#include "math/matrixoperations.h"
#include "math/mathdescriptives.h"
#include "math/fastdistancecorrelation.h"

class PrincipalComponentAnalysisWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    enum Mode{

        COVARIANCE = 0,

        CORRELATION = 1,

        DISTANCECOVARIANCE = 2,

        DISTANCECORRELATION = 3

    };

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        Mode mode;

        int numberOfComponentsToExtract;

        BaseMatrix::Orientation orientation;

        QString outputDirectory;

        QPair<QStringList, QList<int> > selectedItemIdentifiersAndIndexes;

        QPair<QStringList, QList<int> > selectedVariableIdentifiersAndIndexes;

    };

    PrincipalComponentAnalysisWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    PrincipalComponentAnalysisWorkerClass::Parameters d_parameters;

    bool checkInputParameters();


};

#endif // PRINCIPALCOMPONENTANALYSISWORKERCLASS_H
