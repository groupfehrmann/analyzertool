#include "savedatasetworkerclass.h"

SaveDatasetWorkerClass::SaveDatasetWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<SaveDatasetWorkerClass::Parameters *>(parameters))
{

}

void SaveDatasetWorkerClass::doWork()
{
    emit processStarted("Save dataset");

    emit appendLogItem(LogListModel::PARAMETER, "path to file: \"" + d_parameters.pathToFile +"\"");

    emit appendLogItem(LogListModel::PARAMETER, "dataset to save: \"" + d_parameters.datasetToSave->name() +"\"");

    QFile fileIn(d_parameters.pathToFile);

    if (!fileIn.open(QIODevice::WriteOnly)) {

        emit appendLogItem(LogListModel::ERROR, "could not open file \"" + d_parameters.pathToFile + "\"");

        QThread::currentThread()->quit();

        return;

    }

    emit startProgress(0, "Saving dataset", 0, 0);

    QDataStream out(&fileIn);

    out << *d_parameters.datasetToSave.data();

    emit stopProgress(0);

    QThread::currentThread()->quit();

}
