#include "splitidentifiersworkerclass.h"

SplitIdentifiersWorkerClass::SplitIdentifiersWorkerClass(QObject *parent, void *parameters) :
    BaseWorkerClass(parent), d_parameters(*static_cast<SplitIdentifiersWorkerClass::Parameters *>(parameters))
{

}

void SplitIdentifiersWorkerClass::doWork()
{
    emit processStarted("Split identifiers");

    emit appendLogItem(LogListModel::PARAMETER, "dataset: \"" + d_parameters.baseDataset->name() + "\"");

    emit appendLogItem(LogListModel::PARAMETER, "orientation: " + BaseMatrix::orientationToString(d_parameters.orientation));

    emit appendLogItem(LogListModel::PARAMETER, "number of identifiers selected: " + QString::number(d_parameters.selectedIdentifiersAndIndexes.first.size()));

    emit appendLogItem(LogListModel::PARAMETER, "delimiter type: \"" + SelectDelimiterBuildingBlockWidget::delimiterTypeToString(d_parameters.delimiterType) + "\"");

    if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
        emit appendLogItem(LogListModel::PARAMETER, "custum delimiter \"" + d_parameters.custumDelimiter + "\", case sensitive: " + (d_parameters.caseSensitivityForCustumDelimiter == Qt::CaseSensitive ? "true" : "false"));

    emit appendLogItem(LogListModel::PARAMETER, "number of token to keep: " + QString::number(d_parameters.numberOfTokenToKeep));

    emit matrixDataBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    emit annotationsBeginChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    Header &header(d_parameters.baseDataset->header(d_parameters.orientation));

    Annotations &annotations(d_parameters.baseDataset->annotations(d_parameters.orientation));

    emit startProgress(0, "Splitting identifiers", 0, d_parameters.selectedIdentifiersAndIndexes.second.size());

    for (int i = 0 ; i < d_parameters.selectedIdentifiersAndIndexes.second.size(); ++i) {

        QStringList tokens;

        if (d_parameters.delimiterType == SelectDelimiterBuildingBlockWidget::CUSTUM)
            tokens = d_parameters.selectedIdentifiersAndIndexes.first.at(i).split(d_parameters.custumDelimiter, Qt::KeepEmptyParts, d_parameters.caseSensitivityForCustumDelimiter);
        else
            tokens = d_parameters.selectedIdentifiersAndIndexes.first.at(i).split(QRegExp(d_parameters.custumDelimiter), Qt::KeepEmptyParts);

        if (d_parameters.numberOfTokenToKeep <= tokens.size())
            header.setIdentifier(d_parameters.selectedIdentifiersAndIndexes.second.at(i), tokens.at(d_parameters.numberOfTokenToKeep - 1));
        else
            emit appendLogItem(LogListModel::WARNING, "requested token (" + QString::number(d_parameters.numberOfTokenToKeep) + ") not available after splitting identifier \"" + d_parameters.selectedIdentifiersAndIndexes.first.at(i) +"\" at index " + QString::number(d_parameters.selectedIdentifiersAndIndexes.second.at(i))); 

        if (!annotations.labels().isEmpty()) {

            for (const QString &label : annotations.labels()) {

                QVariant annotationValue = annotations.value(d_parameters.selectedIdentifiersAndIndexes.first.at(i), label);

                if (!annotationValue.isNull()) {

                    if (!annotations.appendValue(tokens.at(d_parameters.numberOfTokenToKeep - 1), label, annotationValue))
                        emit appendLogItem(LogListModel::WARNING, "discordant annotation value for identifier \"" + tokens.at(d_parameters.numberOfTokenToKeep - 1) + "\", label \"" + label + "\", value to set \"" + annotationValue.toString() + "\", current value \"" + annotations.value(tokens.at(d_parameters.numberOfTokenToKeep - 1), label).toString() + "\"");

                }

            }

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

            emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

            QThread::currentThread()->quit();

            return;

        }

        emit updateProgress(0, i + 1);

    }

    emit stopProgress(0);

    emit startProgress(0, "Cleaning up annotations", 0, annotations.identifiers().size());

    int progressCounter = 0;

    for (const QString &identifier : annotations.identifiers().values()) {

        if (!header.contains(identifier))
            annotations.removeIdentifier(identifier);

        emit updateProgress(0, ++progressCounter);

    }

    emit stopProgress(0);

    emit annotationsEndChange(d_parameters.baseDataset->universallyUniqueIdentifier(), d_parameters.orientation);

    emit matrixDataEndChange(d_parameters.baseDataset->universallyUniqueIdentifier());

    QThread::currentThread()->quit();

}
