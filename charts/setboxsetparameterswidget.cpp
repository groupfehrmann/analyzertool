#include "setboxsetparameterswidget.h"

SetBoxSetParametersWidget::SetBoxSetParametersWidget(QTreeWidget *parent, const QString &label, QBoxSet *boxSet) :
    QObject(), QTreeWidgetItem(parent), d_boxSet(boxSet), d_initialized(false)
{

    if (boxSet) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetBoxSetParametersWidget::SetBoxSetParametersWidget(QTreeWidgetItem *parent, const QString &label, QBoxSet *boxSet) :
   QObject(), QTreeWidgetItem(parent), d_boxSet(boxSet), d_initialized(false)
{

    if (boxSet) {

        this->initialize(label);

        d_initialized = true;

    }

}

SetBoxSetParametersWidget::~SetBoxSetParametersWidget()
{

}

void SetBoxSetParametersWidget::initialize(const QString &label)
{

    this->setText(0, label);


    d_setLabelWidget = new QLineEdit(d_boxSet->label());

    this->connect(d_setLabelWidget, SIGNAL(textEdited(QString)), this, SLOT(setLabel(QString)));

    this->treeWidget()->setItemWidget(this, 1, d_setLabelWidget);


    d_setBrushParametersTreeWidgetItem = new SetBrushParametersTreeWidgetItem(this, "Brush", d_boxSet->brush());

    this->connect(d_setBrushParametersTreeWidgetItem, SIGNAL(currentBrushChanged(QBrush)), this, SLOT(setBrush(QBrush)));


    d_setPenParametersTreeWidgetItem = new SetPenParametersTreeWidgetItem(this, "Pen", d_boxSet->pen());

    this->connect(d_setPenParametersTreeWidgetItem, SIGNAL(currentPenChanged(QPen)), this, SLOT(setPen(QPen)));

}

void SetBoxSetParametersWidget::setBrush(const QBrush &brush)
{

    d_boxSet->setBrush(brush);

}

void SetBoxSetParametersWidget::setLabel(const QString label)
{

    d_boxSet->setLabel(label);

}


void SetBoxSetParametersWidget::setPen(const QPen &pen)
{

    d_boxSet->setPen(pen);

}

void SetBoxSetParametersWidget::setBoxSet(const QString &label, QBoxSet *boxSet)
{

    d_boxSet = boxSet;

    qDeleteAll(this->takeChildren());

    this->initialize(label);

    d_setBrushParametersTreeWidgetItem->setCurrentBrush(boxSet->brush());

    d_setLabelWidget->setText(boxSet->label());

    d_setPenParametersTreeWidgetItem->setCurrentPen(boxSet->pen());

}
