#ifndef SPLITIDENTIFIERSWORKERCLASS_H
#define SPLITIDENTIFIERSWORKERCLASS_H

#include "base/baseworkerclass.h"
#include "widgets/selectdelimiterbuildingblockwidget.h"

class SplitIdentifiersWorkerClass : public BaseWorkerClass
{

    Q_OBJECT

public:

    struct Parameters {

        QSharedPointer<BaseDataset> baseDataset;

        SelectDelimiterBuildingBlockWidget::DelimiterType delimiterType;

        QString custumDelimiter;

        Qt::CaseSensitivity caseSensitivityForCustumDelimiter;

        int numberOfTokenToKeep;

        BaseMatrix::Orientation orientation;

        QPair<QStringList, QList<int> > selectedIdentifiersAndIndexes;

    };

    SplitIdentifiersWorkerClass(QObject *parent = nullptr, void *parameters = nullptr);

public slots:

    void doWork();

private:

    SplitIdentifiersWorkerClass::Parameters d_parameters;

};

#endif // SPLITIDENTIFIERSWORKERCLASS_H
