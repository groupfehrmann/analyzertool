#include "textresultitem.h"

TextResultItem::TextResultItem(QObject *parent, const QString &name) :
    BaseResultItem(parent, name)
{

    BaseResultItem::d_data->d_type = BaseResultItem::TEXT;

}

void TextResultItem::setText(const QString &text)
{

    d_text = text;

}

const QString &TextResultItem::text() const
{

    return d_text;

}
